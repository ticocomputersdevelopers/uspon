<?php
class Articles{
	public $count;
	//Constructor
	public function __construct(pgsql $con) {
        $this->con = $con;
		//Default vrednosti
		$this->grupa_pr_id = -1;
    }

    //Treba da napisemo i destructor, najverovatnije sa NULL - ISPROBATI!!!
    public function __destruct(){
    	$this->con = NULL;
    }

    //Vraca listu proizvoda kroz array
    public function articlesList(){
    	//Grupa
        if($_SESSION['grupa_pr_id']<>"-1"){
			$sub_groups = new Misc($this->con);
			$sub_groups_list = $sub_groups->miscGetSubgroups($_SESSION['grupa_pr_id']);	
			$uslov_kategorija = " AND r.grupa_pr_id IN(".$sub_groups_list.")  AND ".$_SESSION['grupa_pr_id']."=$1 ";								          
        }else{
            $uslov_kategorija = " AND -1=$1"; 
        };

        if($_SESSION['proizvodjac_id']<>""){
            if($_SESSION['proizvodjac_id']<>"-1"){
                $uslov_proizvodjac = " AND r.proizvodjac_id IN(".$_SESSION['proizvodjac_id'].") ";
            }else{
                $uslov_proizvodjac = " ";    
            }            
        }else{
            $uslov_proizvodjac = " "; 
        };

        if($_SESSION['vrednosti']<>""){
            if($_SESSION['vrednosti']<>"-1"){
                $uslov_vrednosti = " AND r.roba_id IN(SELECT roba_id FROM web_roba_karakteristike WHERE grupa_pr_vrednost_id IN (".$_SESSION['vrednosti'].")) ";
            }else{
                $uslov_vrednosti = " ";    
            }            
        }else{
            $uslov_vrednosti = " "; 
        };

        if(isset($_GET['pretraga'])){
        	$pretraga = new Misc($this->con);
        	$uslov_pretraga = $pretraga->miscSearch($_GET['pretraga']);	
        }else{
        	$uslov_pretraga = " ";
        }

        if($_SESSION['articles_current_min_value']<>"0"){
            $uslov_cena_od = " AND r.".$_SESSION['vrsta_cene']." >= ".$_SESSION['articles_current_min_value'];
        }else{
            $uslov_cena_od = " "; 
        }

        if($_SESSION['articles_current_max_value']<>"0"){
            $uslov_cena_do = " AND r.".$_SESSION['vrsta_cene']." <= ".$_SESSION['articles_current_max_value'];
        }else{
            $uslov_cena_do = " "; 
        }
        

        if(isset($_SESSION['sort_lista_robe'])){
            switch($_SESSION['sort_lista_robe']){
    		  //Cena opadajuce
    		  case "price_desc":
    			     $uslov_order = " ORDER BY r.web_cena DESC ";
    		  break;
    		  //Cena rastuce
    		  case "price_asc":
    			$uslov_order = " ORDER BY r.web_cena ASC ";
    		  break;
    		  //Najnoviji
    		  case "newest":
    			$uslov_order = " ORDER BY r.roba_id DESC ";
    		  break;
    		  //Default
    		  default:
    			$uslov_order = " ORDER BY r.web_cena ASC ";
    		  break;
    	   }            
        }else{
    	   //Default
            $uslov_order = " ORDER BY r.web_cena ASC "; 
        };


		if(isset($_SESSION['max_lista_robe'])){
			$uslov_limit = " LIMIT ".$_SESSION['max_lista_robe'];
		}else{
			$uslov_limit = " ";
		}

		if(!isset($_SESSION['page_lista_robe'])){
			$uslov_offset = " OFFSET 0 ";	
		}else{
			$uslov_offset = " OFFSET ".(($_SESSION['page_lista_robe']-1)*$_SESSION['max_lista_robe']);
		}			

		if($_SESSION['akcija']=="1"){
			$uslov_akcija = " AND r.akcija_flag_primeni = 1 ";
			$uslov_limit = " LIMIT ".$_SESSION['max_specials'];
		}else{
			$uslov_akcija = " ";	
			$uslov_limit = " LIMIT ".$_SESSION['max_lista_robe'];
		}

		if($_SESSION['bez_opisa']=="1"){$uslov_opis = "";}else{$uslov_opis = " AND r.web_opis <> '' ";};
		if($_SESSION['bez_karakteristika']=='1'){$uslov_karakteristike = "";}else{$uslov_karakteristike = " AND karakt_exist(r.roba_id, r.web_flag_karakteristike) = 'DA' ";};
		if($_SESSION['bez_webcene']=='1'){$uslov_web_cena = "";}else{$uslov_web_cena = " AND (r.web_cena <> 0.00) AND (r.web_cena IS NOT NULL) ";};
		if($_SESSION['bez_slike']=='1'){$uslov_slika = "";}else{$uslov_slika = " AND (ws.web_slika_id IS NOT NULL AND ws.flag_prikazi = 1) ";};

    	$sql = "SELECT r.roba_id,r.model,r.akcija_popust,r.akcijska_cena,r.naziv_web,r.".$_SESSION['vrsta_cene'].",ws.web_slika_id,ws.slika,ws.putanja,ws.alt, "
		."CASE WHEN r.grupa_pr_id IN(SELECT gp.grupa_pr_id FROM grupa_pr gp, connectby('grupa_pr', 'grupa_pr_id', 'parrent_grupa_pr_id', 'sifra', '1', 0)"//Kada je grupa u distri
        ."AS t(id int, p_id int, lv int, pos int) WHERE gp.grupa_pr_id = id) THEN "
           ."CASE "
              ."WHEN l.lager = 0 THEN '0' " 
              ."WHEN l.lager IS NULL THEN '0' "
              ."WHEN l.lager <= 10 THEN CAST (l.lager AS varchar(6)) "
              ."WHEN l.lager > 10 THEN '> 10' "
           ."END "
        
        ."ELSE "//Kada je grupa van distro 
           ."CASE "
              ."WHEN l.lager > 1 THEN 'Na stanju' "
           ."END "
        ." END AS lager_izveden "
        ." FROM roba r "
        ." LEFT JOIN (SELECT roba_id,web_slika_id,flag_prikazi,putanja,alt,regexp_replace(putanja, '.+/', '') AS slika FROM web_slika WHERE akcija = 1 GROUP BY roba_id, web_slika_id,flag_prikazi,putanja,alt) ws ON r.roba_id = ws.roba_id "
		." LEFT JOIN (SELECT (sum(kolicina) - sum(rezervisano)) AS lager, roba_id FROM lager WHERE orgj_id IN (".$_SESSION['magacin'].")AND poslovna_godina_id = ".$_SESSION['godina']." GROUP BY roba_id) l ON l.roba_id = r.roba_id "
		." WHERE r.roba_id <> -1 AND r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND (l.lager > r.web_minimalna_kolicina OR r.web_flag_obavezan_prikaz = 1) " 
		.$uslov_opis
		.$uslov_karakteristike
		.$uslov_web_cena
		.$uslov_slika
        .$uslov_kategorija
        .$uslov_proizvodjac
        .$uslov_pretraga
        .$uslov_akcija
        .$uslov_vrednosti;
		//echo $sql;
        $uslovi_array = array($_SESSION['grupa_pr_id']);
        $this->con->query($sql,"artikli_count",array($_SESSION['grupa_pr_id']));
        $this->count = $this->con->numRows();
        //Brisem sesiju za grupu i proizvodjaca;
        //$_SESSION['grupa_pr_id'] = '-1';
        //$_SESSION['proizvodjac_id'] = '-1';//jana zakomentarisala
        //Min i max vrednosti
        $first_ret = array();
        while($rowCart = $this->con->fetchArray()){
            $first_ret[]=$rowCart;
        };
        $numbers = array_map(function($first_ret) {return $first_ret[$_SESSION['vrsta_cene']];}, $first_ret);
        $_SESSION['articles_min_value'] = (int)min($numbers);
        $_SESSION['articles_max_value'] = (int)max($numbers);

        //Pustam drugi upit prema cene,order,limit i offset 
        $sql = $sql.$uslov_cena_od.$uslov_cena_do.$uslov_order.$uslov_limit.$uslov_offset;
        //echo $sql;
        $this->con->query($sql,"artikli_lista",array($_SESSION['grupa_pr_id']));    
        //Storniraj pretragu posle ovoga
        $_SESSION['search_lista_robe'] = "";
        $second_ret = array();
        while($rowCart = $this->con->fetchArray()){
            if($rowCart['lager_izveden'] !=''){
                $second_ret[]=$rowCart; 
            }           
        };

        return $second_ret;     

    }
	
	//vraca grupa_id za prosledjeni roba_id
    public function articlesGroupId($roba_id){
        $sql='SELECT grupa_pr_id FROM roba WHERE roba_id=$1';
        $this->con->query($sql,"grupa_proizvoda",array($roba_id));
        $res=$this->con->fetchResult('grupa_pr_id',0);
        return $res;
    }

    //Vraca preporucene proizvode
    public function articlesRecomended($roba_id,$grupa_pr_id,$limit){
        if($_SESSION['bez_opisa']=="1"){$uslov_opis = "";}else{$uslov_opis = " AND r.web_opis <> '' ";};
        if($_SESSION['bez_karakteristika']=='1'){$uslov_karakteristike = "";}else{$uslov_karakteristike = " AND karakt_exist(r.roba_id, r.web_flag_karakteristike) = 'DA' ";};
        if($_SESSION['bez_webcene']=='1'){$uslov_web_cena = "";}else{$uslov_web_cena = " AND (r.web_cena <> 0.00) AND (r.web_cena IS NOT NULL) ";};
        if($_SESSION['bez_slike']=='1'){$uslov_slika = "";}else{$uslov_slika = " AND (ws.web_slika_id IS NOT NULL AND ws.flag_prikazi = 1) ";};

        $sub_groups = new Misc($this->con);
        $sub_groups_list = $sub_groups->miscGetSubgroups($grupa_pr_id); 

        $sql = "SELECT r.roba_id,r.akcija_popust,r.akcijska_cena,r.naziv_web,r.".$_SESSION['vrsta_cene']." AS cena,ws.web_slika_id,ws.slika,ws.putanja,ws.alt FROM roba r "
        ." LEFT JOIN (SELECT roba_id,web_slika_id,flag_prikazi,putanja,alt,regexp_replace(putanja, '.+/', '') AS slika FROM web_slika WHERE akcija = 1 GROUP BY roba_id, web_slika_id,flag_prikazi,putanja,alt) ws ON r.roba_id = ws.roba_id "
        ." LEFT JOIN (SELECT (sum(kolicina) - sum(rezervisano)) AS lager, roba_id FROM lager WHERE orgj_id IN (".$_SESSION['magacin'].")AND poslovna_godina_id = ".$_SESSION['godina']." GROUP BY roba_id) l ON l.roba_id = r.roba_id "
        ." WHERE r.roba_id <> -1 AND r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND (l.lager > r.web_minimalna_kolicina OR r.web_flag_obavezan_prikaz = 1) "
        //." AND r.roba_id <> ".$roba_id
        ." AND r.grupa_pr_id IN(".$sub_groups_list.") "
        .$uslov_opis
        .$uslov_karakteristike
        .$uslov_web_cena
        .$uslov_slika
        ." LIMIT ".$limit;

        $this->con->query($sql,"artikli_recomended",array());   
        $ret = array();
        while($rowCart = $this->con->fetchArray()){
            $ret[]=$rowCart;
        };
        return $ret;
    }

    //Vraca detaljno proizvod
    public function articlesItem($roba_id){
    	if($roba_id==""){
    		$this->product_exist = false;
    	}else{
    		$sql = "SELECT r.roba_id, r.naziv_web,r.akcijska_cena,r.akcija_popust, r.".$_SESSION['vrsta_cene'].", r.web_opis, r.web_flag_karakteristike, g.grupa FROM roba r INNER JOIN grupa_pr g ON r.grupa_pr_id=g.grupa_pr_id WHERE r.roba_id = $1";
        	$this->con->query($sql,"artikal",array($roba_id));
        	if($this->con->numRows() > 0){
            	$this->product_exist = true;
            	$this->roba_id = $this->con->fetchResult('roba_id',0);
            	$this->naziv_web = $this->con->fetchResult('naziv_web',0);
            	$this->cena = $this->con->fetchResult($_SESSION['vrsta_cene'],0);
            	$this->web_opis = $this->con->fetchResult('web_opis',0);
            	$this->grupa = $this->con->fetchResult('grupa',0);
                $this->akcijska_cena = $this->con->fetchResult('akcijska_cena',0);
                $this->akcija_popust = $this->con->fetchResult('akcija_popust',0);
            	$this->web_karakteristike = $this->articlesSpecs($this->con->fetchResult('roba_id',0),$this->con->fetchResult('web_flag_karakteristike',0));         	
        	}else{
        		$this->product_exist = false;
        	}
    	}        
    }

    //Vraca slike
    public function articlesItemImages($roba_id,$akcija){
        $sql = "SELECT roba_id,web_slika_id,flag_prikazi,putanja,alt,regexp_replace(putanja, '.+/', '') AS slika FROM web_slika WHERE roba_id = $1 AND akcija = $2";
        $this->con->query($sql,"slike_artikal",array($roba_id,$akcija));   
        $ret = array();
        while($rowCart = $this->con->fetchArray()){
            $ret[]=$rowCart;
        };
        return $ret;
    }
    

    //Prikazuje karakteristike za artikal u zavisnosti od flag-a
    public function articlesSpecs($roba_id,$web_flag_karakteristike){
    	switch($web_flag_karakteristike){
            //HTML
            case "0":
            	$sql = "SELECT web_karakteristike FROM roba WHERE roba_id = $1";
            	$this->con->query($sql,"web_karakteristike",array($roba_id));
            	return $this->con->fetchResult('web_karakteristike',0);
            break;
            //Generisane
            case "1":
            	$tabela_begin = '<table class="tabela-karakteristike-generisana" cellspacing="0" cellpadding="0">';
				$celije = '';
				$tabela_end = '</table>';
				$sql = "SELECT wrk.roba_id,gpn.naziv,gpn.css AS css_naziv,wrk.vrednost,wrk.vrednost_css AS css_vrednost FROM web_roba_karakteristike wrk "
				."INNER JOIN grupa_pr_naziv gpn ON wrk.grupa_pr_naziv_id=gpn.grupa_pr_naziv_id WHERE wrk.roba_id = $1 ";
				$this->con->query($sql,"web_karakteristike",array($roba_id));
				echo $sql;
				while($rowKarakteristike = $this->con->fetchObject()){
					$celije = $celije. '<tr><td class="celija-karakteristike-generisana-naziv">'.$rowKarakteristike->naziv.'</td><td class="celija-karakteristike-generisana-vrednost">'.$rowKarakteristike->vrednost.'</td></tr>';	
				}
				return $tabela_begin.$celije.$tabela_end;
            break;
            //Generisane od dobavljaca
            case "2":
            	$tabela_begin = '<table class="tabela-karakteristike-generisana" cellspacing="0" cellpadding="0">';
				$celije = '';
				$tabela_end = '</table>';
				$sql = "SELECT karakteristika_naziv, karakteristika_vrednost FROM dobavljac_cenovnik_karakteristike WHERE roba_id = $1 ";
				$this->con->query($sql,"web_karakteristike",array($roba_id));
				while($rowKarakteristike = $this->con->fetchObject()){
					$celije = $celije. '<tr><td class="celija-karakteristike-generisana-naziv">'.$rowKarakteristike->karakteristika_naziv.'</td><td class="celija-karakteristike-generisana-vrednost">'.$rowKarakteristike->karakteristika_vrednost.'</td></tr>';	
				}
				return $tabela_begin.$celije.$tabela_end;
            break;
            default:
            	$sql = "SELECT web_karakteristike FROM roba WHERE roba_id = $1";
            	$this->con->query($sql,"web_karakteristike",array($roba_id));
            	return $this->con->fetchResult('web_karakteristike',0);
            break;
        }
    }
	
 /*   //Prikazuje paginaciju
    public function articlesPagination(){
   		//Prvo proveravamo da li uopste ima proizvoda
   		if($this->count > 0){
   			$max_page_number = ceil($this->count/$_SESSION['max_lista_robe']);
			//Ako je prva stranica
			if($_SESSION['page_lista_robe'] == "1"){
				echo '<li><a href="#" id="1"><<</a></li>';
				echo '<li><a href="#" id="1"><</a></li>';
				echo '<li><a href="#" id="1">1</a></li>';	
				//Ako postoji druga stranica
				if($max_page_number > 1){
					echo '<li><a href="#" id="2">2</a></li>';
				}
				//Ako postoji treca stranica
				if($max_page_number >= 3){
					echo '<li><a href="#" id="3">3</a></li>';
					echo '<li><a href="#" id="3">></a></li>';
					echo '<li><a href="#" id="'.$max_page_number.'">>></a></li>';
				}
			}else{
				echo '<li><a href="" id="1">&laquo;</a></li>';
				//Ako trenutna stranica jos nije dostignuta
				if($_SESSION['page_lista_robe'] < $max_page_number){
					echo '<li><a href="#" id="'.($_SESSION['page_lista_robe']-1).'"><</a></li>';
					echo '<li><a href="#" id="'.($_SESSION['page_lista_robe']-1).'">'.($_SESSION['page_lista_robe']-1) .'</a></li>';
					echo '<li><a href="#" id="'.$_SESSION['page_lista_robe'].'">'.$_SESSION['page_lista_robe'] .'</a></li>';
					echo '<li><a href="#" id="'.($_SESSION['page_lista_robe']+1).'">'.($_SESSION['page_lista_robe']+1) .'</a></li>';
					echo '<li><a href="#" id="'.($_SESSION['page_lista_robe']+1).'">></a></li>';
					echo '<li><a href="#" id="'.$max_page_number.'">&raquo;</a></li>';	
				//Stigao je do zadnje strane
				}else{
					echo '<li><a href="#" id="'.($_SESSION['page_lista_robe']-1).'"><</a></li>';
					echo '<li><a href="#" id="'.($_SESSION['page_lista_robe']-1).'">'.($_SESSION['page_lista_robe']-1) .'</a></li>';
					echo '<li><a href="#" id="'.$_SESSION['page_lista_robe'].'">'.$_SESSION['page_lista_robe'] .'</a></li>';
				}
			}
   		}	
    }*/
	    //Prikazuje paginaciju
    public function articlesPagination($br_paginacija = 5){
        //Prvo proveravamo da li uopste ima proizvoda
        if($this->count > 0){
            $max_page_number = ceil($this->count/$_SESSION['max_lista_robe']);
            if($max_page_number<$br_paginacija){
                $pag_step = floor($max_page_number/2);
				$dopuna=$max_page_number;
            }
            else{
				$pag_step=floor($br_paginacija/2);
				$dopuna=$br_paginacija;
			}
           // echo "MAXPAGE:".$max_page_number." PageStep:".$pag_step." Pagination broj:".$br_paginacija;
            $curent_page='';
            
            if($max_page_number>1){

                //Ako je prva stranica
                if($_SESSION['page_lista_robe'] == "1"){
                    echo '<li><a class="pag-current" href="#" id="1">1</a></li>';
                    if($max_page_number >= $br_paginacija){

                        for($i=2;$i<=$br_paginacija;$i++){
                            echo '<li><a href="#" id="'.$i.'">'.$i.'</a></li>';
                        }
                        echo '<li><a href="#" id="2">></a></li>';
                        echo '<li><a href="#" id="'.$max_page_number.'">>></a></li>';
                    }else{
                        for($i=2;$i<=$max_page_number;$i++){
                            echo '<li><a href="#" id="'.$i.'">'.$i.'</a></li>';
                        }
                    }
                }
            
                //Ako se nalazi na stranicama vecim od polovine prikaza u paginacija
                elseif(($_SESSION['page_lista_robe'] > $pag_step) and ($_SESSION['page_lista_robe'] < $max_page_number)){

                    echo '<li><a href="#" id="1"><<</a></li>';
                    echo '<li><a href="#" id="'.($_SESSION['page_lista_robe']-1).'"><</a></li>';

                    for($i=$pag_step;$i>0;$i--){
                        echo '<li><a href="#" id="'.($_SESSION['page_lista_robe']-$i).'">'.($_SESSION['page_lista_robe']-$i).'</a></li>';
                    }
                    echo '<li><a class="pag-current" href="#" id="'.$_SESSION['page_lista_robe'].'">'.$_SESSION['page_lista_robe'].'</a></li>';
                    for($i=1;$i<=$pag_step;$i++){
                        if(($_SESSION['page_lista_robe']+$i)<=$max_page_number){
                            echo '<li><a href="#" id="'.($_SESSION['page_lista_robe']+$i).'">'.($_SESSION['page_lista_robe']+$i).'</a></li>';
                        }
                    }
                    echo '<li><a href="#" id="'.($_SESSION['page_lista_robe']+1).'">></a></li>';
                    echo '<li><a href="#" id="'.$max_page_number.'">>></a></li>';
                }    
                
                //Ako se nalazi na manjim stranicama od polovine    
                elseif(($_SESSION['page_lista_robe'] <= $pag_step) and ($_SESSION['page_lista_robe'] < $max_page_number)) {    
                    for($i=1;$i<=$dopuna;$i++){
                        if($i==$_SESSION['page_lista_robe']){
                            echo '<li><a class="pag-current" href="#" id="'.$i.'">'.$i.'</a></li>';
                        }
                        else{
                            echo '<li><a href="#" id="'.$i.'">'.$i.'</a></li>';
                        }            
                    }
                  
                    echo '<li><a href="#" id="'.($_SESSION['page_lista_robe']+1).'">></a></li>';
                    echo '<li><a href="#" id="'.$max_page_number.'">>></a></li>';
                }
                
                //Ako se nalazi na zadnjoj stranici u paginaciji
                elseif($_SESSION['page_lista_robe'] == $max_page_number) {
                    echo '<li><a href="#" id="1"><<</a></li>';
                    echo '<li><a href="#" id="'.($_SESSION['page_lista_robe']-1).'"><</a></li>';
                    for($i=$dopuna-1;$i>0;$i--){
                        echo '<li><a href="#" id="'.($max_page_number-$i).'">'.($max_page_number-$i).'</a></li>';          
                    }
                    echo  '<li><a class="pag-current" href="#" id="'.$max_page_number.'">'.$max_page_number.'</a></li>';
                }
            }    
        }   
    } 


	
    //Prikazuje limit
    public function articlesLimit(){
   		echo '<select name="" id="article-limit">';
   		switch($_SESSION['max_lista_robe']){
   			case "20":
   				echo '<option selected value="20">20</option>';
        		echo '<option value="28">28</option>';
        		echo '<option value="36">36</option>';
   			break;
   			case "28":
   				echo '<option value="20">20</option>';
        		echo '<option selected value="28">28</option>';
        		echo '<option value="36">36</option>';
   			break;
   			case "36":
   				echo '<option value="20">20</option>';
        		echo '<option value="28">28</option>';
        		echo '<option selected value="36">36</option>';
   			break;
   			default:
   				echo '<option value="20">20</option>';
        		echo '<option value="28">28</option>';
        		echo '<option selected value="36">36</option>';
   			break;
   		}        
		echo '</select>';
    }

    //Prikazuje order
    public function articlesOrder(){
    	echo '<select name="" id="article-sort">';
    	switch($_SESSION['sort_lista_robe']){
    		case "price_desc":
    			echo '<option selected value="price_desc">Cena - opadajuce</option>';
        		echo '<option value="price_asc">Cena - rastuce</option>';
        		echo '<option value="newest">Najnoviji</option>';
    		break;
    		case "price_asc":
    			echo '<option value="price_desc">Cena - opadajuce</option>';
        		echo '<option selected value="price_asc">Cena - rastuce</option>';
        		echo '<option value="newest">Najnoviji</option>';
    		break;
    		case "newest":
    			echo '<option value="price_desc">Cena - opadajuce</option>';
        		echo '<option value="price_asc">Cena - rastuce</option>';
        		echo '<option selected value="newest">Najnoviji</option>';
    		break;
    		default:
    			echo '<option selected value="price_desc">Cena - opadajuce</option>';
        		echo '<option value="price_asc">Cena - rastuce</option>';
        		echo '<option value="newest">Najnoviji</option>';
    		break;
    	}
        
        echo '</select>';
    }


    //Prikazuje valutu
    public function articlesValuta(){                        
        echo '<select name="" id="combo-currency-change">';
        switch($_SESSION['valuta']){
            case "1":
                echo '<option id="1" value="1" selected >RSD</option>';
                echo '<option id="2" value="2" >EUR</option>';
            break;
            case "2":
                echo '<option id="1" value="1">RSD</option>';
                echo '<option id="2" value="2" selected>EUR</option>';
            break;
            default:
                echo '<option id="1" value="1" selected >RSD</option>';
                echo '<option id="2" value="2" >EUR</option>';
            break;
        }        
        echo '</select>';
    }

    //Vraca proizvodjace
    public function articlesManufacturer(){
        $sql = "SELECT p.proizvodjac_id, p.naziv FROM proizvodjac p WHERE p.proizvodjac_id <> -1 ORDER BY p.naziv ASC";
        $this->con->query($sql,"proizvodjaci",array());   
        $ret = array();
        $proizvodjaci_current = explode(",",$_SESSION['proizvodjac_id']);
        while($rowCart = $this->con->fetchArray()){
            $ret[]=$rowCart;
        };
        foreach ($ret as $proizvodjac){
            if (in_array($proizvodjac['proizvodjac_id'], $proizvodjaci_current)) {
                echo '<label><input type="checkbox" name="option[]" value="'.$proizvodjac['proizvodjac_id'].'" checked/>'.$proizvodjac['naziv'].'</label>';
            }else{
                echo '<label><input type="checkbox" name="option[]" value="'.$proizvodjac['proizvodjac_id'].'"/>'.$proizvodjac['naziv'].'</label>';
            }
        }
    }

    //Vraca nazive karakteristika
    public function articlesSpecsName(){
        $sql = "SELECT * FROM grupa_pr_naziv WHERE grupa_pr_id = $1";
        $this->con->query($sql,"grupa_pr_naziv",array($_SESSION['grupa_pr_id']));   
        $ret = array();
        while($rowCart = $this->con->fetchArray()){
            $ret[]=$rowCart;
        };
        return $ret;
    }
	
	//Vraca vrednosti karakteristika
    public function articlesSpecsValues($grupa_pr_naziv_id){
        $sql = "SELECT * FROM grupa_pr_vrednost WHERE grupa_pr_naziv_id = $1 ORDER BY naziv ASC";
        $this->con->query($sql,"vrednosti_".$grupa_pr_naziv_id,array($grupa_pr_naziv_id));   
        $ret = array();
        $vrednosti_current = explode(",",$_SESSION['vrednosti']);
        while($rowCart = $this->con->fetchArray()){
            $ret[]=$rowCart;
        };

        foreach ($ret as $vrednosti){
        	if (in_array($vrednosti['grupa_pr_vrednost_id'], $vrednosti_current)) {
                echo '<label><input type="checkbox" name="option[]" value="'.$vrednosti['grupa_pr_vrednost_id'].'" checked/>'.$vrednosti['naziv'].'</label>';
            }else{
                echo '<label><input type="checkbox" name="option[]" value="'.$vrednosti['grupa_pr_vrednost_id'].'"/>'.$vrednosti['naziv'].'</label>';
            }
        	
        }
    }

    //Vraca encoded sliku
    public function articlesImagesBase64($putanja){
        //Ako je internet explorer manje ili 8, ne radi
        if(preg_match('/(?i)msie [2-8]/',$_SERVER['HTTP_USER_AGENT'])){
            return $putanja;
        }else{
            $result = 'data:image/' . pathinfo($putanja, PATHINFO_EXTENSION) . ';base64,' . base64_encode(file_get_contents($putanja));
            return $result;
        }      
    }


    //Pravi link na product info strani za kategoriju
    public function articlesCategoryLink($roba_id){
        $url = new Misc($this->con);
        //Uzimamo grupe
        $sql = "WITH RECURSIVE breadcrumb(grupa_pr_id, grupa, parrent_grupa_pr_id) AS ( "
        ."SELECT grupa_pr_id, grupa, parrent_grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = $1) "
        ."UNION "
        ."SELECT grupa_pr.grupa_pr_id, grupa_pr.grupa, grupa_pr.parrent_grupa_pr_id FROM grupa_pr, breadcrumb WHERE breadcrumb.parrent_grupa_pr_id = grupa_pr.grupa_pr_id) "
        ."SELECT * FROM breadcrumb WHERE grupa_pr_id NOT IN(-1,0) "
        ."ORDER BY grupa_pr_id ASC";
        $this->con->query($sql,"category_link".$roba_id,array($roba_id));

        $products = '/products/';
        $slash = '';
        while($rowGrupe = $this->con->fetchObject()){
            $products .= $slash . $url->miscFormatUrlKey($rowGrupe->grupa);
            $slash = '/';
        };
        return $products;
    }


    //Vraca naziv kategorije prema id
    public function articlesGroupName(){
        if($_SESSION['grupa_pr_id']<>"-1"){
            $sql = "SELECT grupa FROM grupa_pr WHERE grupa_pr_id = $1";
            $this->con->query($sql, "category_name",array($_SESSION['grupa_pr_id']));
            return $this->con->fetchResult('grupa',0);                                        
        }else{
            return 'Artikli'; 
        };
    }

    //Vraca ukljucene filtere
    public function articlesFiltersReturn($tip){
        $ret = array();
        if($tip=='filter_proizvodjac'){
            if($_SESSION['proizvodjac_id']<>"" and $_SESSION['proizvodjac_id']<>"-1"){
                $sql = "SELECT * FROM proizvodjac WHERE proizvodjac_id IN(".$_SESSION['proizvodjac_id'].") ";
                $this->con->query($sql,$tip,array());   
                while($rowCart = $this->con->fetchArray()){
                    $ret[]=$rowCart;
                };
            }
        }else{
            if($_SESSION['vrednosti']<>"" and $_SESSION['vrednosti']<>"-1"){
                $sql = "SELECT * FROM grupa_pr_vrednost WHERE grupa_pr_vrednost_id IN(".$_SESSION['vrednosti'].") ";
                $this->con->query($sql,$tip,array());   
                $ret = array();
                while($rowCart = $this->con->fetchArray()){
                    $ret[]=$rowCart;
                };     
            }       
        }
        return $ret;
        
    }


    //Vraca sve kategorije sa nadredjenim kategorijma kome pripada taj artikal
    public function articlesCategoriesRecursive($roba_id){
        $url = new Misc($this->con);
        $sql = "WITH RECURSIVE breadcrumb(grupa_pr_id, grupa, parrent_grupa_pr_id) AS (
          SELECT grupa_pr_id, grupa, parrent_grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = $1)
          UNION
            SELECT grupa_pr.grupa_pr_id, grupa_pr.grupa, grupa_pr.parrent_grupa_pr_id FROM grupa_pr, breadcrumb
            WHERE breadcrumb.parrent_grupa_pr_id = grupa_pr.grupa_pr_id )
        SELECT * FROM breadcrumb WHERE grupa_pr_id NOT IN(-1,0) 
        ORDER BY parrent_grupa_pr_id ASC";
        $this->con->query($sql,"category_recursive",array($roba_id));

        $groups = '';
        $separator = '';
        while($rowGrupe = $this->con->fetchObject()){
            $groups .= $separator . $url->miscFormatUrlKey($rowGrupe->grupa_pr_id);
            $separator = ',';
        };
        return $groups;
        
    }

    //Vraca formatiranu cenu sa ispisom valute
    public function articlesPriceFormat($cena,$valuta_show = true){
        //Dinar
        if($_SESSION['valuta']=="1"){ 
            $valuta = ' din.';          
            if($cena!="0"){            
                $iznos = number_format($cena, 2, ',', '.');
            }else{
                $iznos = '0.00';
            }
        //Euro
        }else{
            $valuta = ' eur.'; 
            if($cena!="0"){            
                $iznos = number_format($cena/$_SESSION['kurs'], 2, ',', '.');
            }else{
                $iznos = '0.00';
            }
        }
        if($valuta_show == true){
            return $iznos.$valuta;
        }else{
            return $iznos;
        }
    }

	//Vraca glavnu sliku
	public function articlesImages($velicina,$putanja,$slika,$encoded = false){
		$images = new Images();
    	switch($velicina){
    		case 'big':
                //Ako putanja nije prazna
                if($putanja!=''){
                    //Ako postoji big
                    if(file_exists($putanja)){
                        $result = $putanja;
                    }else{
                        $result = 'images/products/big/noimage.jpg';
                    }
                }else{
                    $result = 'images/products/big/noimage.jpg';
                }   			
            break;
            case "standard":
                //Ako putanja nije prazna
                if($putanja!=''){
                     //Ako postoji standard
                    if(file_exists('images/products/standard/'.$slika)){
                        $result = 'images/products/standard/'.$slika;
                    }else{
                        //Ako ne postoji, kreiracemo je od velike ako postoji
                        if(file_exists($putanja)){
                            $images->load('./images/products/big/'.$slika);
                            $images->resizeToWidth($_SESSION['img_sirina_standard']);
                            $images->save('./images/products/standard/'.$slika);
                            //Ako je napravio sliku
                            if(file_exists('images/products/standard/'.$slika)){
                                $result = 'images/products/standard/'.$slika;   
                            }else{
                                $result = 'images/products/standard/noimage.jpg';
                            }                  
                        }else{
                            $result = 'images/products/standard/noimage.jpg';
                        }
                    }
                }else{
                    $result = 'images/products/standard/noimage.jpg';    
                }               
            break;
			case 'medium':
                //Ako putanja nije prazna
                if($putanja!=''){
                     //Ako postoji medium
                    if(file_exists('images/products/medium/'.$slika)==true){
                        $result = 'images/products/medium/'.$slika;
                    }else{
                        //Ako ne postoji, kreiracemo je od velike ako postoji
                        if(file_exists($putanja)){
                            $images->load('./images/products/big/'.$slika);
                            $images->resizeToWidth($_SESSION['img_sirina_medium']);
                            $images->save('./images/products/medium/'.$slika);
                            //Ako je napravio sliku
                            if(file_exists('images/products/medium/'.$slika)){
                                $result = 'images/products/medium/'.$slika; 
                            }else{
                                $result = 'images/products/medium/noimage.jpg';
                            }                  
                        }else{
                            $result = 'images/products/medium/noimage.jpg';
                        }
                    }
                }else{
                    $result = 'images/products/medium/noimage.jpg';
                }               
            break;
			case 'small':
                //Ako putanja nije prazna
                if($putanja!=''){
                    //Ako postoji small
                    if(file_exists('images/products/small/'.$slika)){
                        $result = 'images/products/small/'.$slika;
                    }else{
                        //Ako ne postoji, kreiracemo je od velike ako postoji
                        if(file_exists($putanja)){
                            $images->load('./images/products/big/'.$slika);
                            $images->resizeToWidth($_SESSION['img_sirina_small']);
                            $images->save('./images/products/small/'.$slika);
                            //Ako je napravio sliku
                            if(file_exists('images/products/small/'.$slika)){
                                $result = 'images/products/small/'.$slika;  
                            }else{
                                $result = 'images/products/small/noimage.jpg';
                            }                  
                        }else{
                            $result = 'images/products/small/noimage.jpg';
                        }
                    }    
                }else{
                    $result = 'images/products/small/noimage.jpg';
                }               
            break;
        }         
        //Ako je enkodovana
        if($encoded == true){
            return $this->articlesImagesBase64($result);    
        }else{
           return '/'.$result; 
        }
        
   }
  
}


?>