<?php
class Sessions{

	//Constructor
	public function __construct(pgsql $con) {
        $this->con=$con;
    }

    //Treba da napisemo i destructor, najverovatnije sa NULL - ISPROBATI!!!
    public function __destruct(){
    	$this->con = NULL;
    }

	//Proverava da li je slog u options 'B2C-RESETUJ SESIJE' podesen na 1, ako jeste resetuje sve sesije
	public function sessionResetAll(){
		$sql = "SELECT int_data FROM options WHERE options_id = 1330";
		$this->con->query($sql,"reset_session",array());
		if($this->con->fetchResult('int_data',0)=="1"){
			unset($_SESSION['godina']);
			unset($_SESSION['magacin']);
			unset($_SESSION['kurs']);
			unset($_SESSION['template']);
			unset($_SESSION['boja']);
			unset($_SESSION['modules_loaded']);
			unset($_SESSION['options_loaded']);
			unset($_SESSION['company_loaded']);
			unset($_SESSION['smtp_loaded']);
		}
		//Vracam vrednost na 0
		$sql = "UPDATE options SET int_data = 0 WHERE options_id = 1330";
		$this->con->query($sql,"reset_sesion_status",array());
	}
	
    //Biram poslovnu godinu
	public function sessionGodina(){
		if(!isset($_SESSION['godina'])){
			$sql = "SELECT godina FROM poslovna_godina WHERE status = 0";
			$this->con->query($sql,"poslovna_godina",array());
			$_SESSION['godina'] = $this->con->fetchResult('godina',0);
		}		
	}

	 //Biram magacine
	public function sessionMagacini(){
		if(!isset($_SESSION['magacin'])){
			$sql = "SELECT orgj_id FROM orgj WHERE orgj_id <> -1 AND b2c=1";
			$this->con->query($sql,"orgj",array());
			$zarez = '';
			$magacin = '';
			if ($this->con->numRows() > 0){
				while($rowMagacin = $this->con->fetchObject()){
					$magacin .= $zarez . $rowMagacin->orgj_id;
					$zarez = ', ';
				}
			}else{
				$magacin = '-1';
			}
			$_SESSION['magacin'] = $magacin;
		}		
	}	

	//Postavljam kurs
	public function sessionKurs(){
		if(!isset($_SESSION['kurs'])){
			$sql = "SELECT kl.datum, kl.ziralni, v.valuta_sl, v.valuta_naziv FROM kursna_lista kl, valuta v WHERE kl.valuta_id = v.valuta_id ORDER BY kl.datum DESC LIMIT 1";
			$this->con->query($sql,"kurs",array());
			$_SESSION['kurs'] = 0;
			if($this->con->numRows() > 0){ 
				$_SESSION['kurs'] = $this->con->fetchResult('ziralni',0);
			}
		}
	}

	//Postavljam izabrani template i boja
	public function sessionTemplate(){
		if(!isset($_SESSION['template'])){
			$sql = "SELECT wt.putanja AS template, wts.putanja AS boja FROM web_templates wt INNER JOIN web_templates_schemas wts ON wt.web_templates_id=wts.web_templates_id "
			."WHERE wt.selected = 1 AND wts.selected = 1 ";
			$this->con->query($sql,"template",array());
			$_SESSION['template'] = '';
			$_SESSION['boja'] = '';
			if($this->con->numRows() > 0){ 
				$_SESSION['template'] = $this->con->fetchResult('template',0);
				$_SESSION['boja'] = $this->con->fetchResult('boja',0);
			}
		}
	}

	//Namesta sesiju za module. Kada se u tabeli za options ili web_options promeni neki parametar, treba napraviti jedan slog u bazi 
	//koji bi se updatevao da znamo da su moduli promenjeni, kada to znamo onda radimo unset sesije modules_loaded
	public function sessionModules(){
		if(!isset($_SESSION['modules_loaded'])){
			
			//Uzimam sve module
			$sql = "SELECT web_options_id,int_data FROM web_options WHERE sekcija_id = 1 ORDER BY web_options_id ASC";
			$this->con->query($sql,"modules",array());

			$moduli = '';
			$zarez = '';
			while($rowModules = $this->con->fetchObject()){
            	$moduli .= $zarez . $rowModules->web_options_id.'|'.$rowModules->int_data;
            	$zarez = ',';
        	};
        	$_SESSION['modules'] = $moduli;
			//Ucitane su sesije
			$_SESSION['modules_loaded'] = 'ok';
		}
	}
	
	//Podesavam ostale sesije iz options i web_options tabele
	public function sessionOptions(){
		if(!isset($_SESSION['options_loaded'])){		
			//Da li korisnik mora da uradi potvrdu registracije
			$_SESSION['potvrda_registracije'] = $this->sessionGetWebOptions('int_data','100','potvrda_registracije');
			//Maksimalan broj artikala na prvoj stranici
			$_SESSION['max_specials'] = $this->sessionGetWebOptions('int_data','101','max_specials');			
			//Sortiranje kategorija
			$_SESSION['sortiranja_kategorija'] = $this->sessionGetOptions('str_data','1301','sortiranja_kategorija');
			//Dozvoljen prikaz bez opisa
			$_SESSION['bez_opisa'] = $this->sessionGetOptions('int_data','1305','bez_opisa');
			//Dozvoljen prikaz bez karakteristika
			$_SESSION['bez_karakteristika'] = $this->sessionGetOptions('int_data','1306','bez_karakteristika');
			//Dozvoljen prikaz bez web cene
			$_SESSION['bez_webcene'] = $this->sessionGetOptions('int_data','1307','bez_webcene');
			//Dozvoljen prikaz bez slike
			$_SESSION['bez_slike'] = $this->sessionGetOptions('int_data','1308','bez_slike');
			//Maksimalna sirina slike
			$_SESSION['max_image_width'] = $this->sessionGetOptions('int_data','1314','max_image_width');
			//Prodavnica aktivna
			$_SESSION['prodavnica_aktivna'] = $this->sessionGetOptions('int_data','1317','prodavnica_aktivna');
			//Sirina slike za standard
			$_SESSION['img_sirina_standard'] = $this->sessionGetOptions('int_data','1331','img_sirina_standard');
			//Sirina slike za medium
			$_SESSION['img_sirina_medium'] = $this->sessionGetOptions('int_data','1332','img_sirina_medium');
			//Sirina slike za standard
			$_SESSION['img_sirina_small'] = $this->sessionGetOptions('int_data','1333','img_sirina_small');
			//Vrsta cene koje ce se koristiti na sajtu
			$_SESSION['vrsta_cene'] = $this->sessionGetOptions('str_data','1334','vrsta_cene');
			//Default limit za listu robe
			$_SESSION['max_lista_robe'] = "20";
			//Default sortiranje za listu robe
			$_SESSION['sort_lista_robe'] = "price_asc";
			//Default broj trenutne stranice za paginaciju
			$_SESSION['page_lista_robe'] = "1";
			//Default pretraga za listu robe
			$_SESSION['search_lista_robe'] = "";
			//Default vrednosti
			$_SESSION['vrednosti'] = "-1";
			//Default akcija
			$_SESSION['akcija'] = "";
			//Default vrednosti za cene slider
			$_SESSION['articles_min_value'] = "";
        	$_SESSION['articles_max_value'] = "";
        	$_SESSION['articles_current_min_value'] = "0";
        	$_SESSION['articles_current_max_value'] = "1000000";			
			//Prikaz artikla u gridu ili tube look
			$_SESSION['prikaz'] = "grid";
			//Valuta za prikaz
			$_SESSION['valuta'] = "1"; //1-Dinar, 2-Euro
			

			//Ucitane su sesije
			$_SESSION['options_loaded'] = 'ok';
		}		
	}

	//Podesavam sesije za informacije o firmi
	public function sessionCompany(){
		if(!isset($_SESSION['company_loaded'])){
			$sql = "SELECT * FROM preduzece";
			$this->con->query($sql,"preduzece",array());
			$_SESSION['company_naziv'] = $this->con->fetchResult('naziv',0);
			$_SESSION['company_adresa'] = $this->con->fetchResult('adresa',0);
			$_SESSION['company_telefon'] = $this->con->fetchResult('telefon',0);
			$_SESSION['company_fax'] = $this->con->fetchResult('fax',0);
			$_SESSION['company_pib'] = $this->con->fetchResult('pib',0);
			$_SESSION['company_delatnost_sifra'] = $this->con->fetchResult('delatnost_sifra',0);
			$_SESSION['company_ziro'] = $this->con->fetchResult('ziro',0);
			$_SESSION['company_email'] = $this->con->fetchResult('email',0);
			$_SESSION['company_web'] = $this->con->fetchResult('web',0);

			//Ucitane su sesije
			$_SESSION['company_loaded'] = 'ok';
		}		
	}

	//Vraca smtp podesavanja za server
	public function sessionSMTP(){
		if(!isset($_SESSION['smtp_loaded'])){
			$sql = "SELECT * FROM web_b2c_newsletter_options WHERE web_b2c_newsletter_options_id = 1";
			$this->con->query($sql,"web_b2c_newsletter_options",array());
			$_SESSION['smtp_server'] = $this->con->fetchResult('server',0);
			$_SESSION['smtp_port'] = $this->con->fetchResult('port',0);
			$_SESSION['smtp_username'] = $this->con->fetchResult('username',0);
			$_SESSION['smtp_pass'] = $this->con->fetchResult('pass',0);
			$_SESSION['smtp_name_from'] = $this->con->fetchResult('name_from',0);
			$_SESSION['smtp_mail_from'] = $this->con->fetchResult('mail_from',0);

			//Ucitane su sesije
			$_SESSION['smtp_loaded'] = 'ok';
		}	
	}
	
	//Postavlja default vrednosti sesija za listu robe
	public function sessionArticles(){
		if(!isset($_SESSION['articles_loaded'])){			
			$_SESSION['grupa_pr_id'] = "-1";
			$_SESSION['proizvodjac_id'] = "-1";

			//Ucitane su sesije
			$_SESSION['articles_loaded'] = 'ok';
		}	
	}
	//Vraca vrednost iz options tabele
	private function sessionGetOptions($data_type,$options_id,$naziv_upita){
		$sql = "SELECT ".$data_type." FROM options WHERE options_id = $1";
		$this->con->query($sql,$naziv_upita,array($options_id));
		$data = $this->con->fetchResult($data_type,0);
		return $data;
	}
	
	//Vraca vrednost iz web_options tabele
	private function sessionGetWebOptions($data_type,$options_id,$naziv_upita){
		$sql = "SELECT ".$data_type." FROM web_options WHERE web_options_id = $1";
		$this->con->query($sql,$naziv_upita,array($options_id));
		$data = $this->con->fetchResult($data_type,0);
		return $data;
	}
}


?>