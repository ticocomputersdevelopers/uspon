<?php
class ProductInfo{

	public $roba_id,$grupa_pr_id,$product_exist;

	//Constructor
	public function __construct(pgsql $con) {
        $this->con = $con;
    }

    //Treba da napisemo i destructor, najverovatnije sa NULL - ISPROBATI!!!
    public function __destruct(){
    	$this->con = NULL;
    }


    //Vracam informacije o artiklu
    public function infoProduct(){
    	//Proveram parametre
    	if($_SESSION['bez_opisa']=="1"){$parametar_opis = "";}else{$parametar_opis = " AND r.web_opis <> '' ";};
		if($_SESSION['bez_karakteristika']=="1"){$parametar_karakteristike = "";}else{$parametar_karakteristike = " AND karakt_exist(r.roba_id, r.web_flag_karakteristike) = 'DA' ";};
		if($_SESSION['bez_webcene']=="1"){$parametar_web_cena = "";}else{$parametar_web_cena = " AND (r.web_cena <> 0.00) AND (r.web_cena IS NOT NULL) ";};
		if($_SESSION['bez_slike']=="1"){$parametar_slika = "";}else{$parametar_slika = " AND (ws.web_slika_id IS NOT NULL AND ws.flag_prikazi = 1) ";};


		//Za magacine ne mozemo da koristimo prepared statment zato sto ne podrzava IN()
    	$sql = " SELECT r.roba_id, r.roba_flag_cene_id,r.valuta_id,r.naziv_web, r.web_cena ,r.mpcena, r.web_opis,r.grupa_pr_id, g.grupa, r.web_karakteristike,r.web_flag_karakteristike, r.web_opis, r.akcija_stara_cena, r.akcija_naslov , ws.web_slika_id AS slika,  " 
	    ." r.akcija_popust, l.lager, r.akcija_flag_primeni, r.tip_cene "
		." FROM roba r "
		." INNER JOIN grupa_pr g ON r.grupa_pr_id = g.grupa_pr_id "
		." INNER JOIN tarifna_grupa tg ON r.tarifna_grupa_id = tg.tarifna_grupa_id "
		." LEFT JOIN (SELECT roba_id,web_slika_id,flag_prikazi FROM web_slika WHERE akcija = 1 GROUP BY roba_id, web_slika_id,flag_prikazi) ws ON r.roba_id = ws.roba_id "
		." LEFT JOIN (SELECT (sum(kolicina) - sum(rezervisano)) AS lager, roba_id FROM lager WHERE orgj_id IN (".$_SESSION['magacin'].")AND poslovna_godina_id = $1 GROUP BY roba_id) l ON l.roba_id = r.roba_id "
		." WHERE r.roba_id <> -1 AND r.roba_id = $2 AND r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND (l.lager > r.web_minimalna_kolicina OR r.web_flag_obavezan_prikaz = 1) "
		. $parametar_opis
		. $parametar_karakteristike
		. $parametar_web_cena
		. $parametar_slika;
		$this->con->query($sql,"product_info",array($_SESSION['godina'],$this->roba_id));

		//Funkcije mora da se pozivaju tek kad zavrsimo sa ostalim vrednostima
		if($this->con->numRows() > 0){
			$this->product_exist = $this->con->numRows();
			$this->naziv_web = $this->con->fetchResult('naziv_web',0);
			$this->grupa_pr_id = $this->con->fetchResult('grupa_pr_id',0);			
			$this->lager = $this->con->fetchResult('lager',0);
			$this->mpcena = $this->con->fetchResult('mpcena',0);
			$this->web_cena = $this->con->fetchResult('web_cena',0);
			$this->opis = $this->con->fetchResult('web_opis',0);
			$this->web_karakteristike = $this->con->fetchResult('web_karakteristike',0);
			$this->web_flag_karakteristike = $this->con->fetchResult('web_flag_karakteristike',0);
			$this->breadcrumb_group = $this->infoProductBreadCrumb();
			$this->karakteristike = $this->infoProductKarakteristike($this->web_flag_karakteristike,$this->web_karakteristike);
		}
		//echo $sql;
    }

    //Prikazuje pripadnost grupi sa parrent grupama proizvoda
    public function infoProductBreadCrumb(){
    	$result = '';
    	$sql = "WITH RECURSIVE breadcrumb(grupa_pr_id, grupa, parrent_grupa_pr_id) AS ( "
		."SELECT grupa_pr_id, grupa, parrent_grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = $1 "
		."UNION "
		."SELECT grupa_pr.grupa_pr_id, grupa_pr.grupa, grupa_pr.parrent_grupa_pr_id FROM grupa_pr, breadcrumb WHERE breadcrumb.parrent_grupa_pr_id = grupa_pr.grupa_pr_id) "
		."SELECT * FROM breadcrumb WHERE grupa_pr_id NOT IN(-1,0) "
		."UNION "
		."SELECT 0 AS grupa_pr_id,'Pocetna' AS grupa,-1 AS parrent_grupa_pr_id ORDER BY parrent_grupa_pr_id ASC";
		$this->con->query($sql,"breadcrumb_group",array($this->grupa_pr_id));

		while($rowGrupe = $this->con->fetchObject()){
			$result .= '<li>'.$rowGrupe->grupa.'</li>';
		}
		return $result;
    }


    //Vraca karakteristike u odnosu na flag za njih
    public function infoProductKarakteristike($web_flag_karakteristike,$web_karakteristike){
    	//return $web_karakteristike;
    	switch($web_flag_karakteristike){
    		//HTML
    		case "0":
    			return $web_karakteristike;
    		break;
    		//Generisane
    		case "1":
    			$tabela_begin = '<table class="tabela-karakteristike-generisana" cellspacing="0" cellpadding="0">';
				$celije = '';
				$tabela_end = '</table>';
				$sql = "SELECT wrk.roba_id,gpn.naziv,gpn.css AS css_naziv,wrk.vrednost,wrk.vrednost_css AS css_vrednost FROM web_roba_karakteristike wrk "
						."INNER JOIN grupa_pr_naziv gpn ON wrk.grupa_pr_naziv_id=gpn.grupa_pr_naziv_id WHERE wrk.roba_id = $1 ";
				$this->con->query($sql,"specs_product_info_roba",array($this->roba_id));
				while($rowKarakteristike = $this->con->fetchObject()){
					$celije = $celije. '<tr><td class="celija-karakteristike-generisana-naziv">'.$rowKarakteristike->naziv.'</td><td class="celija-karakteristike-generisana-vrednost">'.$rowKarakteristike->vrednost.'</td></tr>';	
				}
				return $tabela_begin.$celije.$tabela_end;
    		break;
    		//Generisane od dobavljaca
    		case "2":
    			$tabela_begin = '<table class="tabela-karakteristike-generisana" cellspacing="0" cellpadding="0">';
				$celije = '';
				$tabela_end = '</table>';
				$sql = "SELECT karakteristika_naziv, karakteristika_vrednost FROM dobavljac_cenovnik_karakteristike WHERE roba_id = $1 ";
				$this->con->query($sql,"specs_product_info_dobavljac",array($this->roba_id));
				while($rowKarakteristike = $this->con->fetchObject()){
					$celije = $celije. '<tr><td class="celija-karakteristike-generisana-naziv">'.$rowKarakteristike->karakteristika_naziv.'</td><td class="celija-karakteristike-generisana-vrednost">'.$rowKarakteristike->karakteristika_vrednost.'</td></tr>';	
				}
				return $tabela_begin.$celije.$tabela_end;
    		break;
    		default:
    			return '';
    		break;
    	}
    }
}


?>