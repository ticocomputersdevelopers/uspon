<?php
class Cart{

	//Constructor
	public function __construct(pgsql $con) {
        $this->con = $con;
    }

    //Treba da napisemo i destructor, najverovatnije sa NULL - ISPROBATI!!!
    public function __destruct(){
    	$this->con = NULL;
    }

    //Glavna akcija za korpu
	public function cartAdd(){
		$this->rezultat = '-1';

		//Vracam informacije za artikal
		$sql = "SELECT * FROM roba WHERE roba_id = $1";
		$this->con->query($sql,"artikal_za_korpu",array($this->roba_id));
		if($this->con->fetchResult('akcija_popust',0)==0){
			$this->jm_cena = $this->con->fetchResult($_SESSION['vrsta_cene'],0);
		}else{
			$this->jm_cena = $this->con->fetchResult('akcijska_cena',0);
		}
		
		$this->tarifna_grupa_id = $this->con->fetchResult('tarifna_grupa_id',0);
		$this->racunska_cena_nc = $this->con->fetchResult('racunska_cena_nc',0);

		
		//Prvo da vidimo da li je podesena sesija web_b2c_korpa_id
		if(isset($_SESSION['web_b2c_korpa_id'])){  
			//Ako je podesena, onda radimo proveru da li taj artikal vec postoji za tu korpu
			$sql = "SELECT * FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_id = $1 AND roba_id = $2 ";
			$this->con->query($sql,"web_b2c_korpa_stavka_provera",array($_SESSION['web_b2c_korpa_id'],$this->roba_id));
			//Ako je pronadjen taj proizvod za tu korpu
			if ($this->con->numRows() > 0){
				//Updetuj korpu
				$sql = "UPDATE web_b2c_korpa_stavka SET kolicina = kolicina+$1 WHERE web_b2c_korpa_id = $2 AND roba_id = $3 ";
				$this->con->query($sql,"web_b2c_korpa_update_kolicina",array($this->kolicina,$_SESSION['web_b2c_korpa_id'],$this->roba_id));
				if($this->con->affectedRows()>0){
					$this->rezultat = '0';
				}
			}else{
				//Vracam sledeci broj stavke
				$sql = "SELECT CAST(CASE WHEN MAX(broj_stavke)+1 IS NULL THEN 0 ELSE MAX(broj_stavke)+1 END AS numeric(20)) AS broj_stavke FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_id = $1 ";
				$this->con->query($sql,"artikal_broj_stavke",array($_SESSION['web_b2c_korpa_id']));
				$this->broj_stavke = $this->con->fetchResult('broj_stavke',0);

				//Insertujem stavku korpe
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "web_b2c_korpa_id,";		$sVrednosti .= "$1,";
				$sPolja .= "broj_stavke,";			$sVrednosti .= "$2,";
				$sPolja .= "roba_id,";				$sVrednosti .= "$3,";
				$sPolja .= "kolicina,";				$sVrednosti .= "$4,";
				$sPolja .= "jm_cena,";				$sVrednosti .= "$5,";
				$sPolja .= "tarifna_grupa_id,";		$sVrednosti .= "$6,";
				$sPolja .= "racunska_cena_nc";		$sVrednosti .= "$7";
				$sql = "INSERT INTO web_b2c_korpa_stavka (" . $sPolja . ") VALUES (" . $sVrednosti . ")";	
				$this->con->query($sql,"web_b2c_korpa_stavka",array($_SESSION['web_b2c_korpa_id'],$this->broj_stavke,$this->roba_id,$this->kolicina,$this->jm_cena,
				$this->tarifna_grupa_id,$this->racunska_cena_nc));			
				if($this->con->affectedRows()>0){
					$this->rezultat = '0';
				}
			};					
		}else{
			//Ako kupac nije ulogovan, dodeljujem mu sesiju ali bez insertovanja u tabelu web_kupac
			$sekvence = new Misc($this->con);

			if(!isset($_SESSION['web_kupac_id'])){
				$_SESSION['web_kupac_id'] = $sekvence->miscGetSequence("web_kupac_web_kupac_id_seq");
			}; 
			//Podesavam sesiju
			$_SESSION['web_b2c_korpa_id'] = $sekvence->miscGetSequence("web_b2c_korpa_web_b2c_korpa_id_seq");

			//Insertujem korpu
			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "web_b2c_korpa_id,";		$sVrednosti .= "$1,";
			$sPolja .= "web_kupac_id,";			$sVrednosti .= "$2,";
			$sPolja .= "datum";					$sVrednosti .= "$3";
			$sql = "INSERT INTO web_b2c_korpa (" . $sPolja . ") VALUES (" . $sVrednosti . ")";	
			$this->con->query($sql,"web_b2c_korpa",array($_SESSION['web_b2c_korpa_id'],$_SESSION['web_kupac_id'],date("Y-m-d")));
			if($this->con->affectedRows()>0){
				//Insertujem stavku korpe
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "web_b2c_korpa_id,";		$sVrednosti .= "$1,";
				$sPolja .= "broj_stavke,";			$sVrednosti .= "$2,";
				$sPolja .= "roba_id,";				$sVrednosti .= "$3,";
				$sPolja .= "kolicina,";				$sVrednosti .= "$4,";
				$sPolja .= "jm_cena,";				$sVrednosti .= "$5,";
				$sPolja .= "tarifna_grupa_id,";		$sVrednosti .= "$6,";
				$sPolja .= "racunska_cena_nc";		$sVrednosti .= "$7";
				$sql = "INSERT INTO web_b2c_korpa_stavka (" . $sPolja . ") VALUES (" . $sVrednosti . ")";	
				$this->con->query($sql,"web_b2c_korpa_stavka",array($_SESSION['web_b2c_korpa_id'],1,$this->roba_id,$this->kolicina,$this->jm_cena,
				$this->tarifna_grupa_id,$this->racunska_cena_nc));	
				if($this->con->affectedRows()>0){
					$this->rezultat = '0';
				}
			}					
		}			

		//Vracam i ukupnu kolicinu
		$this->kolicina = $this->cartAmount();
	}

	//Menja kolicinu
	public function cartUpdate(){
		$sql = "UPDATE web_b2c_korpa_stavka SET kolicina = $1 WHERE web_b2c_korpa_id = $2 AND roba_id = $3 ";
		$this->con->query($sql,"web_b2c_korpa_stavka_kolicina",array($this->kolicina,$_SESSION['web_b2c_korpa_id'],$this->roba_id));
		if($this->con->affectedRows()>0){
			$this->rezultat = '0';
		}
	}

	//Brise stavku iz korpe
	public function cartDelete(){
		$sql = "DELETE FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_id = $1 AND roba_id = $2 ";
		$this->con->query($sql,"web_b2c_korpa_stavka",array($_SESSION['web_b2c_korpa_id'],$this->roba_id));
		if($this->con->affectedRows()>0){
			$this->rezultat = '0';
		}
	}

	//Brise sve stavke iz korpe
	public function cartDeleteAll(){
		$sql = "DELETE FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_id = $1";
		$this->con->query($sql,"web_b2c_korpa_stavka",array($_SESSION['web_b2c_korpa_id']));
		if($this->con->affectedRows()>0){
			$this->rezultat = '0';
		}
	}

	//Vraca broj artikala u korpi
	public function cartAmount(){
		if(isset($_SESSION['web_b2c_korpa_id'])){
			$sql = "SELECT COUNT(web_b2c_korpa_stavka_id) AS broj FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_id = $1"; 
			$this->con->query($sql,"korpa_kolicina_header",array($_SESSION['web_b2c_korpa_id']));
			return $this->con->fetchResult('broj',0);
		}else{
			return '0';
		}
		
	}

    //Vraca sadrzaj korpe kroz array
    public function cartContain(){
    	$sql = "SELECT r.roba_id,wbk.jm_cena ,r.valuta_id,r.roba_flag_cene_id,r.naziv_web, wbk.kolicina, wbk.jm_cena*wbk.kolicina,ws.web_slika_id,ws.slika,ws.putanja,ws.alt  AS ukupno FROM web_b2c_korpa_stavka wbk "
    	."INNER JOIN roba r ON wbk.roba_id=r.roba_id "
    	."LEFT JOIN (SELECT roba_id,web_slika_id,flag_prikazi,putanja,alt,regexp_replace(putanja, '.+/', '') AS slika FROM web_slika WHERE akcija = 1 GROUP BY roba_id, web_slika_id,flag_prikazi,putanja,alt) ws ON r.roba_id = ws.roba_id "
    	."WHERE wbk.web_b2c_korpa_id = $1 AND wbk.from_conf = 0 ORDER BY broj_stavke ASC";
    	$this->con->query($sql,"korpa",array($_SESSION['web_b2c_korpa_id']));
    	$ret = array();
    	while($rowCart = $this->con->fetchArray()){
    		$ret[]=$rowCart;
    	};
    	return $ret;   	
    }

    //Vraca ukupan iznos korpe
    public function cartSum(){
    	$sql = "SELECT SUM(wbk.jm_cena*wbk.kolicina) AS ukupna_suma FROM web_b2c_korpa_stavka wbk INNER JOIN roba r ON wbk.roba_id=r.roba_id WHERE wbk.web_b2c_korpa_id = $1 AND r.valuta_id = 1 ";
    	$this->con->query($sql,"korpa_sum",array($_SESSION['web_b2c_korpa_id']));		
    	return $this->con->fetchResult('ukupna_suma',0); 
    }

    public function cartArticleList(){
    	$sql = "SELECT wb2c.*,r.naziv_web FROM web_b2c_korpa_stavka wb2c LEFT JOIN roba r ON r.roba_id=wb2c.roba_id WHERE wb2c.web_b2c_korpa_id = $1 ORDER BY web_b2c_korpa_stavka_id DESC";
    	$this->con->query($sql,"artice-cart-list",array($_SESSION['web_b2c_korpa_id']));
    	$ret = array();
    	while($rowCartArticle = $this->con->fetchArray()){
    		$ret[]=$rowCartArticle;
    	};
    	return $ret;  

    }
}


?>