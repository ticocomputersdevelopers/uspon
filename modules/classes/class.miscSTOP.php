<?php
class Misc{

	//Constructor
	public function __construct(pgsql $con) {
        $this->con=$con;
    }

    //Treba da napisemo i destructor, najverovatnije sa NULL - ISPROBATI!!!
    public function __destruct(){
    	$this->con = NULL;
    }

	//Detektuje browser
	public function miscBrowserDetect(){
    	if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)){
        	return true;
    	}else{
        	return false;
    	}
	}

    //Vraca naziv stranice
    public function miscPageDetect(){
		$currentFile = $_SERVER["PHP_SELF"];
		$parts = Explode('/', $currentFile);
		$stranica = $parts[count($parts) - 1];
		return $stranica;
	}

	//Skracuje string za odredjenu velicinu
	public function miscShortString($string, $max_length){
    	if(strlen($string) > $max_length){
    		$string = substr($string, 0, $max_length);
    		$pos = strrpos($string, " ");
    		if($pos === false){
    			return substr($string, 0, $max_length)."...";
    		}
    		return substr($string, 0, $pos)."...";
    	}else{
    		return $string;
    	}
	} 

	//Vraca ip adresu korisnika
	public function miscGetIpAdress(){
		$ip;
		if(getenv("HTTP_CLIENT_IP")){
			$ip = getenv("HTTP_CLIENT_IP");
		}
		else if(getenv("HTTP_X_FORWARDED_FOR")){
			$ip = getenv("HTTP_X_FORWARDED_FOR");
		}
		else if(getenv("REMOTE_ADDR")){
			$ip = getenv("REMOTE_ADDR");
		}
		else{
			$ip = "UNKNOWN";
		}
		return $ip;
	} 

	//Ekvivalent Delphi funkciji QuotedStr
	public function miscQuotedStr($string){
		$string = str_replace(chr(047), "", $string);
		$string = str_replace(chr(057), "", $string);
		$string = str_replace(chr(134), "", $string);
		$string = chr(047).$string.chr(047);
		return $string;
	}

	//Funkcija za enkripciju mailova i lozinki
	public function miscEncrypt($string){
		$key = 'opIaod3392iiu';
		return base64_encode(mcrypt_cbc(MCRYPT_RIJNDAEL_128,$key,$string,MCRYPT_ENCRYPT,''));
	}

	//Funkcija za dekripciju mailova i lozinki
	public function miscDecrypt($string){
		$key = 'opIaod3392iiu';
		$string = base64_decode($string);
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128,$key,$string,MCRYPT_MODE_CBC));
	}

	//Vraca sledeci id iz baze prema sekvenci
	public function miscGetSequence($sequenceName) {
		$sql = "SELECT nextval(' ". $sequenceName ."') as next";
		$this->con->query($sql,$sequenceName,array());
		$data = $this->con->fetchResult('next',0);
		return $data;
	}

	//Vraca mesto prema datom id-u
	public function miscGetPlace($mesto_id){
		if($mesto_id==""){
			$mesto_id = "-1";
		};	
		$sql ="SELECT ptt||' '||mesto AS naziv FROM mesto WHERE mesto_id = $1";
		$this->con->query($sql,"mesto",array($mesto_id));
		if($pg_conn->numRows()>0){
			$naziv = $pg_conn->fetchResult('naziv',0);
		}else{
			$naziv = '';
		}
		return $naziv;
	}

	//Vraca sve podgrupe vezane za grupa_pr_id koji prosledjujemo
	public function miscGetSubgroups($grupa_pr_id){
		$sql = "SELECT gp.grupa_pr_id, gp.sifra, gp.grupa FROM grupa_pr gp, connectby('grupa_pr', 'grupa_pr_id', 'parrent_grupa_pr_id', 'sifra', $1, 0) "
		."AS t(id int, p_id int, lv int, pos int) WHERE gp.grupa_pr_id = id ";
		$this->con->query($sql,"podgrupe",array($grupa_pr_id));
		$zarez = '';
		$grupe = '';
		if ($this->con->numRows() > 0){ 
			while($rowGrupe = $this->con->fetchObject()){
				$grupe .= $zarez . $rowGrupe->grupa_pr_id;
				$zarez = ', ';
			}
		}else{ 
			$grupe = "";
		}
		return $grupe;
	}

	//Pretraga artikla
	public function miscSearch($kljucne_reci){
		// vadim nedozvoljene reci iz kljucnih reci
		$str_to_replace = array('"'=>'',"\n"=>" ","'"=>"","select"=>"","update"=>"","insert"=>"","from"=>"","("=>"",")"=>"","["=>"","]"=>"","{"=>"","}"=>"","delete"=>"");
    	$kljucne_reci = strtr($kljucne_reci, $str_to_replace);
		//menjam uslov za dsRoba na osnovu unetih reci.Moze biti maksimalno do 5 kljucnih reci i sada ih postavljam na prazan string
		$rec1 = "";
		$rec2 = "";
		$rec3 = "";
		$rec4 = "";
		$rec5 = "";
		$sUslov = "";
    	//preuzimam kljucne reci i razbijam ih na delove
    	$sVrednost = trim($kljucne_reci);
    	if($sVrednost == ""){ 
			$sUslov = ""; 
		}else{
    		for($i = 1; $i <= 5; $i++){
          		$iPos = strpos($sVrednost, " "); // separator izmedju razlicitih kljucnih reci je space
				if(is_numeric($iPos)){
              		switch($i){
                		case 1: $rec1 = substr($sVrednost, 0, $iPos);
                		case 2: $rec2 = substr($sVrednost, 0, $iPos);
                		case 3: $rec3 = substr($sVrednost, 0, $iPos);
                		case 4: $rec4 = substr($sVrednost, 0, $iPos);
                		case 5: $rec5 = substr($sVrednost, 0, $iPos);
              		}
              		$sVrednost = substr($sVrednost, - (strlen($sVrednost) - ($iPos + 1)));
          		}else{
              		switch($i){
						case 1: $rec1 = $sVrednost;
						case 2: $rec2 = $sVrednost;
						case 3: $rec3 = $sVrednost;
						case 4: $rec4 = $sVrednost;
						case 5: $rec5 = $sVrednost;
					}
              		$sVrednost = "";
				}
        	}

        	$zamena = new Misc($this->con);
        	//formiram uslov za pretragu
        	if($rec1 != ""){ $sUslov .= " AND (r.naziv_web ILIKE '%" . $zamena->miscStrToUpperSerbian($rec1) . "%' OR r.naziv_web ILIKE '%" . $zamena->miscStrToLowerSerbian($rec1) . "%')"; }
        	if($rec2 != ""){ $sUslov .= " AND (r.naziv_web ILIKE '%" . $zamena->miscStrToUpperSerbian($rec2) . "%' OR r.naziv_web ILIKE '%" . $zamena->miscStrToLowerSerbian($rec2) . "%')"; }
        	if($rec3 != ""){ $sUslov .= " AND (r.naziv_web ILIKE '%" . $zamena->miscStrToUpperSerbian($rec3) . "%' OR r.naziv_web ILIKE '%" . $zamena->miscStrToLowerSerbian($rec3) . "%')"; }
        	if($rec4 != ""){ $sUslov .= " AND (r.naziv_web ILIKE '%" . $zamena->miscStrToUpperSerbian($rec4) . "%' OR r.naziv_web ILIKE '%" . $zamena->miscStrToLowerSerbian($rec4) . "%')"; }
        	if($rec5 != ""){ $sUslov .= " AND (r.naziv_web ILIKE '%" . $zamena->miscStrToUpperSerbian($rec5) . "%' OR r.naziv_web ILIKE '%" . $zamena->miscStrToLowerSerbian($rec5) . "%')"; }
		}
		return $sUslov;
	}

	//Menja nasa slova iz mala u velika
	private function miscStrToUpperSerbian($string) {
    	return strtr(mb_strtoupper($string, "utf-8"), array("�" => "�","�" => "�","�" => "�","�" => "�","�" => "�"));
	} 
	//Menja nasa slova iz velika u mala
	private function miscStrToLowerSerbian($string) {
    	return strtr(mb_strtolower($string, "utf-8"), array("�" => "�","�" => "�","�" => "�","�" => "�","�" => "�"));
	}

	//Ispisuje combobox sa nacinima isporuke iz baze
	public function miscShowShipmentWays(){
		// uzimam nacine isporuka iz baze
		$sql = "SELECT naziv, web_nacin_isporuke_id FROM web_nacin_isporuke WHERE selected = 1 ORDER BY web_nacin_isporuke_id ASC";
		$this->con->query($sql,"nacin_isporuke",array());
		$ret = array();
    	while($rowIsporuka = $this->con->fetchArray()){
    		$ret[]=$rowIsporuka;
    	};
    	return $ret;        
	}

	//Ispisuje combobox sa nacinima placanja iz baze
	public function miscShowPaymentWays(){
		// uzimam nacine placanja iz baze
		$sql = "SELECT naziv, web_nacin_placanja_id FROM web_nacin_placanja WHERE selected = 1 ORDER BY web_nacin_placanja_id ASC";
		$this->con->query($sql,"nacin_placanja",array());
		$ret = array();
    	while($rowPlacanja = $this->con->fetchArray()){
    		$ret[]=$rowPlacanja;
    	};
    	return $ret; 
	}

	//Pravi array iz tabele mesta
	public function miscShowPlaces(){
		$sql = "SELECT * FROM mesto";
		$this->con->query($sql,"mesta",array());
		$ret = array();
    	while($rowMesta = $this->con->fetchArray()){
    		$ret[]=$rowMesta;
    	};
    	return $ret;
	}

	//Pravi array iz za meni grupe
	public function miscShowGrupe($grupa_pr_id){
		$sql = "SELECT grupa_pr_id, parrent_grupa_pr_id, grupa FROM grupa_pr WHERE parrent_grupa_pr_id = $1 AND web_b2c_prikazi = 1 ".$_SESSION['sortiranja_kategorija'];
		$this->con->query($sql,$grupa_pr_id,array($grupa_pr_id));
		$ret = array();
    	while($rowMesta = $this->con->fetchArray()){
    		$ret[]=$rowMesta;
    	};
    	return $ret;
	}

	//Pravi valid url za rewrite
	public function miscFormatUrlKey($str){
    	$urlKey = preg_replace('#[^0-9a-z�ƊЎ���]+#i', '-', $str);
    	$urlKey = mb_strtolower($urlKey);
    	$urlKey = trim($urlKey, '-');
    	return $urlKey;
	}

	public function miscBreadcrumbsOld($separator = ' &raquo; ', $home = '') {
    	$path = array_filter(explode('/', parse_url(strtr(urldecode($_SERVER['REQUEST_URI']), array("č" => "�","ć" => "�","�" => "�","đ" => "�","��" => "�")), PHP_URL_PATH)));
    	$base_url = substr($_SERVER['SERVER_PROTOCOL'], 0, strpos($_SERVER['SERVER_PROTOCOL'], '/')) . '://' . $_SERVER['HTTP_HOST'] . '/';
    	$breadcrumbs = array("<a href=\"$base_url\">$home</a>");
    	$tmp = array_keys($path);
    	$last = end($tmp);
    	unset($tmp);
    	foreach ($path as $x => $crumb){
        	$title = ucwords(str_replace(array('.php', '_'), array('', ' '), $crumb));
			if ($x == 1){
	        	$breadcrumbs[]  = "<a href=\"$base_url$crumb\">$title</a>";
			}elseif($x > 1 && $x < $last){
				$tmp = "<li><a class=b-kategorija href=\"$base_url";
				for($i = 1; $i <= $x; $i++){
					$tmp .= $path[$i] . '/';
				}
                $tmp .= "\">$title</a></li>";
				$breadcrumbs[] = $tmp;
				unset($tmp);
        	}else{
                $breadcrumbs[] = "<li class=trenutna-lokacija>".$title."</li>";
			}
    	}
    	unset($breadcrumbs[0],$breadcrumbs[1]);
    	return implode($separator, $breadcrumbs);
	}	

	//Izvlaci substring izmedju 2 stringa
	public function miscBetweenString($string, $start, $end){
 		$string = " ".$string;
 		$pos = strpos($string,$start);
 		if ($pos == 0) return "";
 		$pos += strlen($start);
 		$len = strpos($string,$end,$pos) - $pos;
 		return substr($string,$pos,$len);
	}

	//Breadcrumbs funkcija za linkove.Ako se dodaje nova stranica u rewrite, i ovde takodje treba
	public function miscBreadcrumbs(){
		$page_name = str_replace('page_name=', '', $_SERVER['QUERY_STRING']);
		$pocetna = '<li><a class="b-kategorija" href="/">Naslovna strana</a></li>';
		switch($page_name){
			case "":
				$link = '';
			break;
			case "pocetna":
				$link = '';
			break;
			case "register":
				$link = ' &raquo; <li><a class=b-kategorija href="/registracija">Registracija</a></li>';
			break;
			case "user":
				$link = ' &raquo; <li><a class=b-kategorija href="/korisnik">Korisnik</a></li>';
			break;
			case "o-nama":
				$link = ' &raquo; <li><a class=b-kategorija href="/o-nama">O nama</a></li>';
			break;
			case "kontakt":
				$link = ' &raquo; <li><a class=b-kategorija href="/kontakt">Kontakt</a></li>';
			break;
			case "nacin-kupovine":
				$link = ' &raquo; <li><a class=b-kategorija href="/nacin-kupovine">Na�in kupovine</a></li>';
			break;
			case "korpa":
				$link = ' &raquo; <li><a class=b-kategorija href="/cart">Korpa</a></li>';
			break;
			case "narudzbina":
				$link = ' &raquo; <li><a class=b-kategorija href="/payment">Narud�bina</a></li>';
			break;
			case "products":
				$link = ' &raquo; '. $this->miscBreadcrumbsOld();
			break;
		}
		//Ako je proizvod detaljni
		if(isset($_GET['roba_id']) and is_numeric($_GET['roba_id'])){
			//Ili cemo da vratimo pun naziv proizvoda ili rewrite-ovan naziv
			$sql = "SELECT naziv_web FROM roba WHERE roba_id = $1";
			$this->con->query($sql,"breadcrumb_roba",array($_GET['roba_id']));
			$link = ' &raquo; <li><a class="b-kategorija" href="'.htmlspecialchars($_SERVER['REQUEST_URI']).'">'.$this->con->fetchResult('naziv_web',0).'</a></li>';
			//$link = ' &raquo; <a href="'.htmlspecialchars($_SERVER['REQUEST_URI']).'">'.$this->miscBetweenString($_SERVER['REQUEST_URI'], 'product/', '/').'</a>';	
			return $pocetna.$link;
		}else{
			return $pocetna.$link;	
		}		
	}



	//Vraca sledeci broj dokumenta iz web_b2c_narudzbina
	public function miscGetNextDocumentNumber($web_nalog){
		// uzimam prefiks dokumenta
		$sql = "SELECT sifra_vd FROM vrsta_dokumenta WHERE vrsta_dokumenta_id = $1";	
		$this->con->query($sql,"vrsta_dokumenta_sifra",array($web_nalog));
		if($this->con->numRows() == 1){
			$prefix = $this->con->fetchResult('sifra_vd',0);
		}else{
			$prefix = "NN";
		}

		$sql = "SELECT MAX(broj_dokumenta) AS mx FROM web_b2c_narudzbina";
		$this->con->query($sql,"vrsta_dokumenta_broj",array());
		if($this->con->fetchResult('mx',0)==""){
			$broj = '1';
		}else{		
			$broj =str_replace($prefix, "", $this->con->fetchResult('mx',0));
			$broj++;
			$l=strlen($broj);
			for($i=$l; $i<5; $i++){
				$broj = '0'.$broj;
			}
		
		}
		return $prefix.$broj;	
	}

	//Vraca encoded sliku
    public function miscImagesBase64($putanja){
    	//Ako je internet explorer manje ili 8, ne radi
    	if(preg_match('/(?i)msie [2-8]/',$_SERVER['HTTP_USER_AGENT'])){
    		return $putanja;
		}else{
    		$result = 'data:image/' . pathinfo($putanja, PATHINFO_EXTENSION) . ';base64,' . base64_encode(file_get_contents($putanja));
        	return $result;
		}       
    }

	//Slike za slideshow
	public function miscSlideshow(){
		$sql = "SELECT * FROM web_b2c_slike_slideshow WHERE flag_prikazi = 1";
		$this->con->query($sql,"slike_slideshow",array());
		while($rowSlike = $this->con->fetchArray()){
            $ret[]=$rowSlike;
        };
        return $ret;
	}

	public function miscBackground(){
		$sql = "SELECT * FROM options WHERE options_id = 1339";
		$this->con->query($sql,"slika_background",array());
		if($this->con->fetchResult('str_data',0)!=''){
			echo '<img src="'.$putanja.'" class="">';
		}		
	}

	//Vraca slike koje trebamo da preuzmemo od dobavljaca za import
	public function miscImagesSupplier($selektovani){
		$sql = "SELECT dc.roba_id, dcs.putanja, dcs.akcija FROM dobavljac_cenovnik dc INNER JOIN dobavljac_cenovnik_slike dcs ON "
		."dc.sifra_kod_dobavljaca=dcs.sifra_kod_dobavljaca WHERE dc.dobavljac_cenovnik_id IN(".$selektovani.") "
		."ORDER BY dcs.dobavljac_cenovnik_slike_id ASC ";
		$this->con->query($sql,"vrati_slike_dobavljac",array());
		while($rowSlikeDobavljac = $this->con->fetchArray()){
            $ret[]=$rowSlikeDobavljac;
        };
        return $ret;
	}


	//Korisnici-posetioci sajta salju pitanje sa kontakt forme kao i sa artikla ako imaju neko pitanje vezano za njega
	public function miscContactForm(){
		$mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = $_SESSION['smtp_server'];
        $mail->SMTPAuth = true;
        $mail->Username = $_SESSION['smtp_username'];
        $mail->Password = $_SESSION['smtp_pass']; 
        $mail->From = $this->contact_email;
        $mail->FromName = $this->contact_name;
        $mail->addAddress($_SESSION['smtp_mail_from']);
        $mail->addReplyTo($this->contact_email, 'Pitanje sa Vase prodavnice');
        $mail->isHTML(true);
        $mail->Subject = 'Pitanje sa Vase prodavnice';
        //Ako salje sa kontakt forme
        if($this->contact_forma == true){
        	$mail->Body = 'Imate novo pitanje sa kontakt forme Vase prodavnice.Pitanje je postavio '.$this->contact_name.'.Pitanje glasi:<br><br>'.$this->contact_question.'';
        //Ako salje sa artikla
        }else{
        	$artikal = '<a href="'.$_SERVER["SERVER_NAME"].'/product_info/probaartikal">'.$_SERVER["SERVER_NAME"].'/product_info/probaartikal</a>';
        	$mail->Body = 'Imate novo pitanje vezano za artikal '.$artikal.' sa Vase prodavnice.Pitanje je postavio '.$this->contact_name.'.Pitanje glasi:<br><br>'.$this->contact_question.'';
        }
        
        if(!$mail->send()){
            $this->contact_rezultat = '1';
        }else{
            $this->contact_rezultat = '0';
        }
	}

	//Podesava title, description i keywords za stranicu u zavisnosti od strane
	public function miscSEO(){
		if(isset($_GET['page_name'])){
			switch($_GET['page_name']){
				case "pocetna":		
					$this->title = 'Pocetna';
					$this->description = '';
					$this->keywords = '';
				break;
				case "o-nama":
					$this->title = 'O nama';
					$this->description = '';
					$this->keywords = '';
				break;
				case "kontakt":
					$this->title = 'Kontakt';
					$this->description = '';
					$this->keywords = '';
				break;
				case "nacin-kupovine":
					$this->title = 'Nacin kupovine';
					$this->description = '';
					$this->keywords = '';
				break;
				case "products":	
					$this->title = 'Proizvodi';
					$this->description = '';
					$this->keywords = '';	
				break;
				case "korpa":	
					$this->title = 'Korpa';
					$this->description = '';
					$this->keywords = '';	
				break;
				case "product":
					if(isset($_GET['roba_id']) and is_numeric($_GET['roba_id'])){
						$sql = "SELECT naziv_web, description, keywords FROM roba WHERE roba_id = $1";
						$this->con->query($sql,"artikal_seo", array($_GET['roba_id']));
						$this->title = $this->con->fetchResult('naziv_web',0);
						$this->description = $this->con->fetchResult('description',0);
						$this->keywords = $this->con->fetchResult('keywords',0);
					}else{
						$this->title = 'Izabrani proizvod nije pronadjen';
						$this->description = '';
						$this->keywords = '';
					}					
				break;
				case "search":		
					$this->title = 'Proizvodi';
					$this->description = '';
					$this->keywords = '';	
				break;
			}
		}else{
			$this->title = 'Pocetna';
			$this->description = '';
			$this->keywords = '';
		}
	}



}


?>