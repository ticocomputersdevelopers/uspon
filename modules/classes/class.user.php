<?php
class User{

    public $web_kupac_id,$kod;

	//Constructor
	public function __construct(pgsql $con) {
        $this->con = $con;
        //Default vrednosti
        $this->web_kupac_id = -1;
        $this->kod = '';
        $this->lozinka = '';
        $this->ime = '';
        $this->adresa = '';
        $this->telefon = '';
        $this->email = '';
        $this->flag_potvrda = 0;
        $this->partner_id = -1;
        $this->prezime = '';
        $this->flag_prima_poruke = 0;
        $this->flag_vrsta_kupca = 0;
        $this->telefon_mobilni = '';
        $this->naziv = '';
        $this->fax = '';
        $this->mesto_id = -1;
        $this->pib = '';
        $this->status_registracije = 0;

    }

    //Treba da napisemo i destructor, najverovatnije sa NULL - ISPROBATI!!!
    public function __destruct(){
    	$this->con = NULL;
    }

    //Dodaje novog korisnika
    public function userAdd(){       
        $misc = new Misc($this->con);
        //Uzimam sledeci web_kupac_id za dalju identifikaciju
        $this->web_kupac_id = $misc->miscGetSequence("web_kupac_web_kupac_id_seq");
           
        $sPolja = '';
        $sVrednosti = '';
        $sPolja .= " web_kupac_id,";            $sVrednosti .= " $1,";
        $sPolja .= " kod,";                     $sVrednosti .= " $2,";
        $sPolja .= " lozinka,";                 $sVrednosti .= " $3,";
        $sPolja .= " ime,";                     $sVrednosti .= " $4,";
        $sPolja .= " adresa,";                  $sVrednosti .= " $5,";
        $sPolja .= " telefon,";                 $sVrednosti .= " $6,";
        $sPolja .= " email,";                   $sVrednosti .= " $7,";
        $sPolja .= " flag_potvrda,";            $sVrednosti .= " $8,";
        $sPolja .= " partner_id,";              $sVrednosti .= " $9,";
        $sPolja .= " prezime,";                 $sVrednosti .= " $10,";
        $sPolja .= " flag_prima_poruke,";       $sVrednosti .= " $11,";
        $sPolja .= " flag_vrsta_kupca,";        $sVrednosti .= " $12,";
        $sPolja .= " telefon_mobilni,";         $sVrednosti .= " $13,";
        $sPolja .= " naziv,";                   $sVrednosti .= " $14,";
        $sPolja .= " fax,";                     $sVrednosti .= " $15,";
        $sPolja .= " mesto_id,";                $sVrednosti .= " $16,";
        $sPolja .= " pib,";                     $sVrednosti .= " $17,";
        $sPolja .= " status_registracije";      $sVrednosti .= " $18";
        $sql = "INSERT INTO web_kupac (" . $sPolja . ") VALUES (" . $sVrednosti . ")";

        /*
        0 - Uspesno uneseno, korisnik ne mora da potvrdi
        1 - Uspesno uneseno, korisnik mora da se potvrdi preko mail adrese       
        2 - Nesupeo unos, ta mail adresa vec postoji      
        */
        
        //Proveri pre inserta da li taj korisnik vec postoji u bazi kao registrovan
        if($this->status_registracije==1){
            $this->kod = $this->userCodeGenerate();
            if($this->userExist() > 1){
                $this->user_rezultat = '2';
            }else{
                 $this->con->query($sql,"web_kupac_add_registred",array($this->web_kupac_id,$this->kod,$misc->miscEncrypt($this->lozinka),$this->ime,$this->adresa,$this->telefon,
                 $misc->miscEncrypt($this->email),$this->flag_potvrda,$this->partner_id,$this->prezime,$this->flag_prima_poruke,$this->flag_vrsta_kupca,
                 $this->telefon_mobilni,$this->naziv,$this->fax,$this->mesto_id,$this->pib,$this->status_registracije));   
                //Ako je ukljucena potvrda registracije
                if($_SESSION['potvrda_registracije']=="1"){
                    $this->userSendConfrimMail();         
                    $this->user_rezultat = '1';   
                }else{
                    $_SESSION['web_kupac_id'] = $this->web_kupac_id;
                    $_SESSION['flag_vrsta_kupca'] = $this->flag_vrsta_kupca; 
                    $this->userLogin();
                    $this->user_rezultat = '0';
                }               
            }
        }else{            
            $this->con->query($sql,"web_kupac_add_unregistred",array($this->web_kupac_id,$this->kod,$misc->miscEncrypt($this->lozinka),$this->ime,$this->adresa,$this->telefon,
            $misc->miscEncrypt($this->email),$this->flag_potvrda,$this->partner_id,$this->prezime,$this->flag_prima_poruke,$this->flag_vrsta_kupca,
            $this->telefon_mobilni,$this->naziv,$this->fax,$this->mesto_id,$this->pib,$this->status_registracije)); 
            $_SESSION['web_kupac_id'] = $this->web_kupac_id;
            $_SESSION['flag_vrsta_kupca'] = $this->flag_vrsta_kupca; 
            $this->user_rezultat = '0';
        }       
    }

    //Menja podatke za korisnika
    public function userUpdate(){
        $misc = new Misc($this->con);
    	$sql = "UPDATE web_kupac SET lozinka=$1, ime=$2, adresa=$3, telefon=$4, prezime=$5, flag_prima_poruke=$6, telefon_mobilni=$7, naziv=$8,fax=$9, mesto_id=$10, pib=$11 WHERE web_kupac_id = $12";
    	$this->con->query($sql,"web_kupac_update",array($misc->miscEncrypt($this->lozinka),$this->ime,$this->adresa,$this->telefon,$this->prezime,$this->flag_prima_poruke,$this->telefon_mobilni,
    	$this->naziv,$this->fax,$this->mesto_id,$this->pib,$_SESSION['web_kupac_id']));
    	if($this->con->affectedRows() > 0){
    		$this->user_rezultat = '0';
    	}else{
    		$this->user_rezultat = '1';
    	}
    }

    //Korisnik se loguje
    public function userLogin(){		
		$misc = new Misc($this->con);
		$sql = "SELECT web_kupac_id,lozinka,email, flag_potvrda, flag_vrsta_kupca, kupac_popust FROM web_kupac WHERE lozinka = $1 AND email = $2 AND status_registracije = 1";
		$this->con->query($sql,"web_kupac_login",array($misc->miscEncrypt($this->lozinka),$misc->miscEncrypt($this->email)));

        /*
        0 - Korisnik postoji i uradio je potvrdu preko maila(ako je ukljucena)
        1 - Korisnik postoji ali nije uradio potvrdu preko maila       
        2 - Korisnik ne postoji     
        */

		if($this->con->numRows() > 0){ // pronadjen je korisnik
			//Ako je ukljucena opcija da registracija mora da se potvrdi preko mail adrese
			if($_SESSION['potvrda_registracije']=="1"){
				//Ako je uradio potvrdu mail-om
				if($this->con->fetchResult('flag_potvrda',0)==1){
					$_SESSION['ulogovan'] = "1";
					$_SESSION['web_kupac_id'] = $this->con->fetchResult('web_kupac_id',0);
					$_SESSION['flag_vrsta_kupca'] = $this->con->fetchResult('flag_vrsta_kupca',0);
					$_SESSION['kupac_popust'] = $this->con->fetchResult('kupac_popust',0);
					$this->login_rezultat = '0';
				}else{
					$this->login_rezultat = '1';
				}
			}else{
				$_SESSION['ulogovan'] = "1";
				$_SESSION['web_kupac_id'] = $this->con->fetchResult('web_kupac_id',0);
				$_SESSION['flag_vrsta_kupca'] = $this->con->fetchResult('flag_vrsta_kupca',0);
				$_SESSION['kupac_popust'] = $this->con->fetchResult('kupac_popust',0);
				$this->login_rezultat = '0';
			}			
			//Ako je korisnik vec ubacio neke artikle u korpu, radimo update za web_b2c_korpu
			if(isset($_SESSION['web_b2c_korpa_id'])){
				$sql = "UPDATE web_b2c_korpa SET web_kupac_id = $1 WHERE web_b2c_korpa_id = $2 ";
				$this->con->query($sql,"web_kupac_login_update_korpa",array($_SESSION['web_kupac_id'],$_SESSION['web_b2c_korpa_id']));
			}
		}else{
			$this->login_rezultat = '2';
		};

    }



    //Korisnik se odjavljuje
    public function userLogout(){
    	unset($_SESSION['ulogovan']);
		unset($_SESSION['web_kupac_id']);
		unset($_SESSION['flag_vrsta_kupca']);
		unset($_SESSION['web_b2c_korpa_id']);
    }


    //Proverava da li postoji korisnik
    private function userExist(){     
        $sql = "SELECT web_kupac_id FROM web_kupac WHERE email = $1 AND status_registracije = 1 ";
        $this->con->query($sql,"user_exist",array($this->email));
        return $this->con->numRows();
    }

    //Vraca podatke o korisniku
    public function userInfo(){
    	$misc = new Misc($this->con);

    	$sql = "SELECT wk.*, m.mesto FROM web_kupac wk INNER JOIN mesto m ON wk.mesto_id=m.mesto_id WHERE web_kupac_id = $1";
    	$this->con->query($sql,"web_kupac_info",array($_SESSION['web_kupac_id']));
    	if($this->con->numRows() > 0){
    		$this->lozinka = $misc->miscDecrypt($this->con->fetchResult('lozinka',0));
        	$this->ime = $this->con->fetchResult('ime',0);
        	$this->adresa = $this->con->fetchResult('adresa',0);
        	$this->telefon = $this->con->fetchResult('telefon',0);
        	$this->email = $misc->miscDecrypt($this->con->fetchResult('email',0));
        	$this->prezime = $this->con->fetchResult('prezime',0);
        	$this->flag_vrsta_kupca = $this->con->fetchResult('flag_vrsta_kupca',0);
        	$this->telefon_mobilni = $this->con->fetchResult('telefon_mobilni',0);
        	$this->naziv = $this->con->fetchResult('naziv',0);
        	$this->fax = $this->con->fetchResult('fax',0);
        	$this->mesto_id = $this->con->fetchResult('mesto_id',0);
        	$this->pib = $this->con->fetchResult('pib',0);
            $this->mesto = $this->con->fetchResult('mesto',0);
        	return '0';
    	}else{
            $this->lozinka = '';
            $this->ime = '';
            $this->adresa = '';
            $this->telefon = '';
            $this->email = '';
            $this->prezime = '';
            $this->flag_vrsta_kupca = '';
            $this->telefon_mobilni = '';
            $this->naziv = '';
            $this->fax = '';
            $this->mesto_id = '';
            $this->pib = '';
            $this->mesto = '';
    		return '1';
    	}
    	
    }



    //Korisnik zavrsava registraciju putem linka iz maila
    public function userConfirmRegister(){
    	$sql = "SELECT web_kupac_id FROM web_kupac WHERE web_kupac_id = $1 AND kod = $2 AND status_registracije = 1 ";
		$this->con->query($sql,"web_kupac_register_response",array($this->web_kupac_id,$this->kod));
		//Nadjen je korisnik i kod
		if ($this->con->numRows() > 0){
			//Menjamo flag_potvrda u 1
			$sql = "UPDATE web_kupac SET flag_potvrda = 1, kod = NULL WHERE web_kupac_id = $1 ";
			$this->con->query($sql,"web_kupac_register_update",array($this->web_kupac_id));			
			$this->user_confirm_result = '0';
		//Neispravan kod ili web_kupac_id
		}else{
			$this->user_confirm_result = '1';
		};
    }

    //Korisnik je zaboravio sifru.Ovde treba radi predostroznosti da kada uputi zahtev za to da mu resetujemo sifru na neku random
    public function userForgotPass(){
    	$misc = new Misc($this->con);
    	$sql = "SELECT web_kupac_id, lozinka, email FROM web_kupac WHERE email = $1 AND status_registracije = 1";
		$this->con->query($sql,"web_kupac_forgot",array($misc->miscEncrypt($this->email)));
		//Nadjen je korisnik
		if ($this->con->numRows() > 0){
			//Restujem sifru za korisnika
			$lozinka = $this->userRandomPassword();
			$sql = "UPDATE web_kupac SET lozinka = $1 WHERE email = $2 AND status_registracije = 1";
			$this->con->query($sql,"web_kupac_forgot_update",array($misc->miscEncrypt($lozinka),$misc->miscEncrypt($this->email)));
			if($this->con->affectedRows() > 0){
				//Saljem mu mail sa novom lozinkom
				$mail = new PHPMailer();
        		$mail->isSMTP();
        		$mail->Host = $_SESSION['smtp_server'];
        		$mail->SMTPAuth = true;
        		$mail->Username = $_SESSION['smtp_username'];
        		$mail->Password = $_SESSION['smtp_pass']; 
        		$mail->From = $_SESSION['smtp_mail_from'];
        		$mail->FromName = $_SESSION['smtp_name_from'];
        		$mail->addAddress($this->email);
        		$mail->addReplyTo($_SESSION['smtp_name_from'], 'Potvrda registracije');
        		$mail->isHTML(true);
        		$mail->Subject = 'Promena lozinke';
        		$mail->Body = 'Vi ili neko sa Vasom e-mail adresom '.$this->email.' je uputio zahtev sa  <a href="'.$_SERVER["SERVER_NAME"].'/">'.$_SERVER["SERVER_NAME"].'/</a> u vezi sa zaboravljenom lozinkom.Vasa lozinka je resetovana i glasi ovako:<br><br>'.$lozinka.'';
        		if(!$mail->send()){
            		$this->user_forgot_result = '1';
        		}else{
            		$this->user_forgot_result = '0';
        		}
			}
		}else{
			$this->user_forgot_result = '2';
		}
    }

    //Provera da li korisnik trenutno ima nesto u korpi
    public function userCheckCart(){
        if(isset($_SESSION['web_b2c_korpa_id'])){ 
            $sql = "SELECT COUNT(web_b2c_korpa_stavka_id) FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_id = $1 ";
            $this->con->query($sql,"user_check_cart",array($_SESSION['web_b2c_korpa_id']));
            $this->check_cart_rezultat = $this->con->fetchResult('count',0);
        }else{
            $this->check_cart_rezultat = 0;
        }
    }



    //Korisnik se prijavljuje na newsletter
    public function userNewsletterSignIn(){
    	$misc = new Misc($this->con);
    	//Prvo proveravamo da li taj e-mail vec postoji u bazi i da li je aktivan za newsletter
		$sql = "SELECT * FROM web_b2c_newsletter WHERE email = $1 AND aktivan = 1" ;
		$this->con->query($sql,"web_b2c_newsletter_check",array($misc->miscEncrypt($this->email)));
		if($this->con->numRows() > 0){
			//Vec je prijavljen za newsletter
			return '-1';
		}else{
			//Generisem kod za konfirmaciju
			$kod = $this->userCodeGenerateNewsletter();		
			$sql = "INSERT INTO web_b2c_newsletter(kod,email,aktivan) VALUES($1,$2,$3)";
			$this->con->query($sql,"web_b2c_newsletter_insert",array($kod,$misc->miscEncrypt($this->email),1));
			if($this->con->affectedRows() > 0){
				return '0';
			}else{
				return '-2';
			};			
		};
    }

    //Korisnik se odjavljuje sa newsletter-a
    public function userNewsletterSignOut(){
    	$misc = new Misc($this->con);
    	//Radimo update u web_b2c_newsletter za aktivan
    	$sql = "UPDATE web_b2c_newsletter SET aktivan = 0 WHERE kod = $1 AND email = $2";
    	$this->con->query($sql,"web_b2c_newsletter_remove",array($this->kod,$misc->miscEncrypt($this->email)));
    	if($this->con->affectedRows() > 0){
			return '0';
		}else{
			return '-1';
		};
    }

    private function userSendConfrimMail(){
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = $_SESSION['smtp_server'];
        $mail->SMTPAuth = true;
        $mail->Username = $_SESSION['smtp_username'];
        $mail->Password = $_SESSION['smtp_pass']; 
        $mail->From = $_SESSION['smtp_mail_from'];
        $mail->FromName = $_SESSION['smtp_name_from'];
        $mail->addAddress($this->email);
        $mail->addReplyTo($_SESSION['smtp_name_from'], 'Potvrda registracije');
        $mail->isHTML(true);
        $mail->Subject = 'Potvrda registracije';
        $mail->Body = 'Da biste zavrsili Vasu registraciju, potrebno je da kliknete na link <a href="'.$_SERVER["SERVER_NAME"].'/confirm/kod/'.$this->kod.'/id/'.$this->web_kupac_id.'">'.$_SERVER["SERVER_NAME"].'/confirm/kod/'.$this->kod.'/id/'.$this->web_kupac_id.'</a>';
        if(!$mail->send()){
            return '-1';
        }else{
            return '0';
        }
    }

    //Generise kod(MD5) za potvrdu korisnika
    private function userCodeGenerate(){
        //Random
        $strKey = md5(microtime());
        //Proveri da vec ne postoji taj
        $sql = "SELECT web_kupac_id FROM web_kupac WHERE kod = $1 ";
        $this->con->query($sql,"web_kupac_code_generate",array($strKey));    
        if($this->con->numRows() > 0){
            //Kod vec postoji
            return userCodeGenerate();
        }else{
            //Kod je ok
            return $strKey;
        }
    }


    //Generise kod(MD5) za newsletter
    private function userCodeGenerateNewsletter(){
        //Random
        $strKey = md5(microtime());
        //Proveri da vec ne postoji taj
        $sql = "SELECT web_b2c_newsletter_id FROM web_b2c_newsletter WHERE kod = $1 ";
        $this->con->query($sql,"web_b2c_newsletter_code_generate",array($strKey));    
        if($this->con->numRows() > 0){
            //Kod vec postoji
            return userCodeGenerateNewsletter();
        }else{
            //Kod je ok
            return $strKey;
        }
    }

    //Generise random string za novu lozinku
    private function userRandomPassword($length = 10){
    	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$randomString = '';
    	for ($i = 0; $i < $length; $i++) {
        	$randomString .= $characters[rand(0, strlen($characters) - 1)];
    	}
    	return $randomString;
	}

}


?>