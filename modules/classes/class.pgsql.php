<?php
class pgsql {
	private $linkid; // PostgreSQL link identifier
	private $host; // PostgreSQL server host
	private $user; // PostgreSQL user
	private $passwd; // PostgreSQL password
	private $db; // PostgreSQL database
	private $port; // PostgreSQL port
	private $result; // Query result
	
	/* Class constructor. Initializes the $host, $user, $passwd and $db fields. */
	function __construct($host, $db, $user, $passwd, $port) {
		$this->host = $host;
		$this->user = $user;
		$this->passwd = $passwd;
		$this->db = $db;
		$this->port = $port;
	}
	/* Class destructor */
	function __destruct() {
		$this->disconnect();
	}
	/* Connects to the PostgreSQL Database */
	public function connect(){
		try{
			$this->linkid = @pg_connect("host=$this->host port=$this->port dbname=$this->db user=$this->user password=$this->passwd");
			if (! $this->linkid) throw new Exception("Could not connect to PostgreSQL server.");
		}
		catch (Exception $e) {
			die($e->getMessage());
		}
	}
	
	/* Execute database query.Ovo je stara funkcija koja nija imala prepared statment i vise je necemo koristiti
	public function query($query){
		try{
			$this->result = @pg_query($this->linkid,$query);
			if(! $this->result) throw new Exception("The database query failed.");
		}
		catch (Exception $e){
			echo $e->getMessage();
		}
		return $this->result;
	}*/

	/* Execute database query.Nova funkcija koja koristi prepared statments.Ovo nam je osnovna zastita protiv SQL injections */
	public function query($query,$name,$array){
		try{
			$this->result = @pg_prepare($this->linkid,$name,$query);
			$this->result = @pg_execute($this->linkid,$name,$array);
			if(! $this->result) throw new Exception("The database query failed.");
		}
		catch (Exception $e){
			echo $e->getMessage();
		}
		return $this->result;
	}


	/* Determine total rows affected by query. */
	public function affectedRows(){
		$count = @pg_affected_rows($this->result);
		return $count;
	}
	/* Determine total rows returned by query */
	public function numRows(){
		$count = @pg_num_rows($this->result);
		return $count;
	}
	/* Return query result row as an object. */
	public function fetchObject(){
		$row = @pg_fetch_object($this->result);
		return $row;
	}
	/* Return query result row as an indexed array. */
	public function fetchRow($num){
		$row = @pg_fetch_row($this->result,$num);
		return $row;
	}
	/* Return field value in given row */
	public function fetchResult($field,$row){
		$vrednost = @pg_fetch_result($this->result,$row,$field);
		return $vrednost;
	}
	/* Return query result row as an associated array. */
	public function fetchArray(){
		$row = @pg_fetch_array($this->result);
		return $row;
	}
	/* Return the number of fields in a result set */
	public function numberFields() {
		$count = @pg_num_fields($this->result);
		return $count;
	}
	/* Return a field name given an integer offset. */
	public function fieldName($offset){
		$field = @pg_field_name($this->result, $offset);
		return $field;
	}
	/* Free the recordset */
	public function freeResult(){
		@pg_free_result($this->result);
	}
	/* Return last error */
	public function lastError(){
		$txtError = @pg_last_error($this->linkid);
		return $txtError;
	}
	/* Closing connection */
	public function disconnect(){
		@pg_close($this->linkid);
	}
}
?>