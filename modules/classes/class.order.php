<?php
class Order{

	//Constructor
	public function __construct(pgsql $con) {
        $this->con = $con;
        //Default vrednosti
        $this->web_b2c_narudzbina_id = -1;
        $this->web_kupac_id = -1;
        $this->orgj_id = -1;
        $this->poslovna_godina_id = -1;
        $this->vrsta_dokumenta_id = -1;
        $this->broj_dokumenta = '';
        $this->datum_dokumenta = '';
        $this->valuta_id = -1;
        $this->kurs = 0.00;
        $this->web_nacin_placanja_id = -1;
        $this->web_nacin_isporuke_id = -1;
        $this->iznos = 0.00;
        $this->prihvaceno = 0;
        $this->stornirano = 0;
        $this->realizovano = 0;
        $this->napomena = '';
        $this->ip_adresa = '';
        $this->payment_id = '';
        $this->trans_id = '';
        $this->post_date = '';
        $this->result_code = '';
        $this->auth_code = '';
    }

    //Treba da napisemo i destructor, najverovatnije sa NULL - ISPROBATI!!!
    public function __destruct(){
    	$this->con = NULL;
    }

    //Dodaje novu narudzbinu
    public function orderAdd(){
        $order_infos = new Misc($this->con);

        //Insertujem narudzbinu
        $this->web_b2c_narudzbina_id = $order_infos->miscGetSequence("web_b2c_narudzbina_web_b2c_narudzbina_id_seq");
        $this->broj_dokumenta = $order_infos->miscGetNextDocumentNumber("501");
        $this->ip_adresa = $order_infos->miscGetIpAdress();
                
        $sPolja = '';
        $sVrednosti = '';
        $sPolja .= " web_b2c_narudzbina_id,";       $sVrednosti .= " $1,";
        $sPolja .= " web_kupac_id,";                $sVrednosti .= " $2,";
        $sPolja .= " orgj_id,";                     $sVrednosti .= " $3,";
        $sPolja .= " poslovna_godina_id,";          $sVrednosti .= " $4,";
        $sPolja .= " vrsta_dokumenta_id,";          $sVrednosti .= " $5,";
        $sPolja .= " broj_dokumenta,";              $sVrednosti .= " $6,";
        $sPolja .= " datum_dokumenta,";             $sVrednosti .= " $7,";
        $sPolja .= " valuta_id,";                   $sVrednosti .= " $8,";
        $sPolja .= " kurs,";                        $sVrednosti .= " $9,";
        $sPolja .= " web_nacin_placanja_id,";       $sVrednosti .= " $10,";
        $sPolja .= " web_nacin_isporuke_id,";       $sVrednosti .= " $11,";
        $sPolja .= " iznos,";                       $sVrednosti .= " $12,";
        $sPolja .= " prihvaceno,";                  $sVrednosti .= " $13,";
        $sPolja .= " stornirano,";                  $sVrednosti .= " $14,";
        $sPolja .= " realizovano,";                 $sVrednosti .= " $15,";
        $sPolja .= " napomena,";                    $sVrednosti .= " $16,";
        $sPolja .= " ip_adresa,";                   $sVrednosti .= " $17,";
        $sPolja .= " payment_id,";                  $sVrednosti .= " $18,";
        $sPolja .= " trans_id,";                    $sVrednosti .= " $19,";
        $sPolja .= " post_date,";                   $sVrednosti .= " $20,";
        $sPolja .= " result_code,";                 $sVrednosti .= " $21,";
        $sPolja .= " auth_code";                    $sVrednosti .= " $22";
        
        
        $sql = "INSERT INTO web_b2c_narudzbina (" . $sPolja . ") VALUES (" . $sVrednosti . ")";
        $this->con->query($sql,"web_b2c_narudzbina_add",array($this->web_b2c_narudzbina_id,$_SESSION['web_kupac_id'],-1,$_SESSION['godina'],501,
        $this->broj_dokumenta,date("d-m-Y"),1,$_SESSION['kurs'],$this->web_nacin_placanja_id,$this->web_nacin_isporuke_id,0,0,
        0,0,$this->napomena,$this->ip_adresa,$this->payment_id,$this->trans_id,$this->post_date,$this->result_code,$this->auth_code)); 

        if($this->con->affectedRows() > 0){
            $_SESSION['web_b2c_narudzbina_id'] = $this->web_b2c_narudzbina_id;
            $_SESSION['narudzbina_mail_posalji'] = 'send';
        }

        $this->orderCartToOrder();  
        
        if ($this->con->affectedRows() > 0){
            //Razdvajam prema nacinu placanja
            switch($this->web_nacin_placanja_id){
                //Pouzecem
                case "1":
                    $this->order_result = '0';
                break;
                //Uplatnicom
                case "2":
                    $this->order_result = '0';
                break;
                //Platnom karticom
                case "3":
                    $this->order_result = '0';
                break;
                //PLATIMO servisom
                case "4":
                    $this->order_result = '0';
                break;
            }
        }else{
           $this->order_result = '1';
        }
    }

    //Kopira iz web_b2c_korpa_stavka u web_b2c_narudzbina_stavka
    private function orderCartToOrder(){
        //Uzimam stavke iz web_b2c_korpa_stavka
        $sql = "SELECT * FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_id = $1 ";
        $this->con->query($sql,"web_cart_to_order",array($_SESSION['web_b2c_korpa_id']));
    
        if($this->con->numRows() > 0){
            $sql = "INSERT INTO web_b2c_narudzbina_stavka(web_b2c_narudzbina_id, broj_stavke, roba_id, kolicina, jm_cena, tarifna_grupa_id,racunska_cena_nc) "
            ."SELECT  $1, web_b2c_korpa_stavka.broj_stavke, web_b2c_korpa_stavka.roba_id, web_b2c_korpa_stavka.kolicina, " 
            ."web_b2c_korpa_stavka.jm_cena, web_b2c_korpa_stavka.tarifna_grupa_id, web_b2c_korpa_stavka.racunska_cena_nc  "
            ."FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_stavka.web_b2c_korpa_id = $2 ";
            $this->con->query($sql,"web_cart_to_order_insert",array($this->web_b2c_narudzbina_id,$_SESSION['web_b2c_korpa_id']));
        };
    
        //Ako je uspeo unos, skini sesiju
        if ($this->con->affectedRows() > 0){
            unset($_SESSION['web_b2c_korpa_id']);
        } 
    }

    //Vraca informacije o narudzbini
    public function orderShowOrder(){
        $sql = "SELECT wbn.*,(SELECT wni.naziv FROM web_nacin_isporuke wni WHERE wbn.web_nacin_isporuke_id=wni.web_nacin_isporuke_id)AS nacin_isporuke,
        (SELECT wnp.naziv FROM web_nacin_placanja wnp WHERE wbn.web_nacin_placanja_id=wnp.web_nacin_placanja_id)AS nacin_placanja,
        CAST(CASE WHEN wbn.stornirano = 0 THEN 'DA' ELSE 'NE' END AS varchar(6)) AS narudzbina_uspesna FROM web_b2c_narudzbina wbn
        WHERE wbn.web_b2c_narudzbina_id = $1";
        $this->con->query($sql, "web_show_order",array($_SESSION['web_b2c_narudzbina_id']));
        $ret = array();
        while($rowNarudzbina = $this->con->fetchArray()){
            $ret[]=$rowNarudzbina;
        };
        return $ret;
    }

    //Vraca sadrzaj narudzbine kroz array
    public function orderContain(){
        $sql = "SELECT wbs.roba_id,r.naziv_web,g.grupa,to_char((wbs.jm_cena/(tg.porez/100+1)),'FM99999999.99') AS cena_bez_pdv,wbs.jm_cena-wbs.jm_cena/(tg.porez/100+1) AS pdv_iznos,tg.porez, "
        ."wbs.kolicina,round(wbs.jm_cena*wbs.kolicina,2) AS ukupno FROM web_b2c_narudzbina_stavka wbs INNER JOIN roba r ON wbs.roba_id=r.roba_id "
        ."INNER JOIN grupa_pr g ON r.grupa_pr_id=g.grupa_pr_id INNER JOIN tarifna_grupa tg ON r.tarifna_grupa_id=tg.tarifna_grupa_id "
        ."WHERE wbs.web_b2c_narudzbina_id = $1 ";
        $this->con->query($sql,"web_b2c_narudzbina_stavke",array($_SESSION['web_b2c_narudzbina_id']));       
        $ret = array();
        while($rowNarudzbina = $this->con->fetchArray()){
            $ret[]=$rowNarudzbina;
        };
        return $ret;    
    }

    //Vraca ukupan iznos korpe
    public function orderSum(){
        $sql = "SELECT SUM(wbs.jm_cena*wbs.kolicina) AS ukupna_suma FROM web_b2c_narudzbina_stavka wbs INNER JOIN roba r ON wbs.roba_id=r.roba_id WHERE wbs.web_b2c_narudzbina_id = $1 AND r.valuta_id = 1 ";
        $this->con->query($sql,"web_b2c_narudzbina_ukupno",array($_SESSION['web_b2c_narudzbina_id']));       
        return $this->con->fetchResult('ukupna_suma',0);
    }

    //Salje kopiju narudzbine korisniku na mail
    public function orderSendMail($email,$body){
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->Host = $_SESSION['smtp_server'];
        $mail->SMTPAuth = true;
        $mail->Username = $_SESSION['smtp_username'];
        $mail->Password = $_SESSION['smtp_pass']; 
        $mail->From = $_SESSION['smtp_mail_from'];
        $mail->FromName = $_SESSION['smtp_name_from'];
        $mail->addAddress($email);
        $mail->addReplyTo($_SESSION['smtp_name_from'], 'Narudzbina sa '.$_SESSION['company_naziv'].' webshop-a');
        $mail->isHTML(true);
        $mail->Subject = 'Narudzbina sa '.$_SESSION['company_naziv'].' webshop-a';
        $mail->Body = $body;
        if(!$mail->send()){
            return '-1';
        }else{
            return '0';
        }    
    }
}


?>