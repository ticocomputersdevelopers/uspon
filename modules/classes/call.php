<?php
header("Content-type: text/html; charset=windows-1250");
require_once("all.php");
if(isset($_POST['action'])){
	switch($_POST['action']){
		case "set_session":
			$_SESSION[$_POST['naziv']] = $_POST['value'];
		break;
		case "get_session":
			echo $_SESSION[$_POST['naziv']];
		break;
		case "cart_add":
			$cart = new Cart($pgConn);
			$cart->roba_id = $_POST['roba_id'];
			$cart->kolicina = $_POST['kolicina'];
			$cart->cartAdd();
			echo $cart->rezultat.'|'.$cart->kolicina;			
		break;
		case "cart_update":
			$cart = new Cart($pgConn);
			$cart->roba_id = $_POST['roba_id'];
			$cart->kolicina = $_POST['kolicina'];
			$cart->cartUpdate();
			echo $cart->rezultat;			
		break;
		case "cart_delete":
			$cart = new Cart($pgConn);
			$cart->roba_id = $_POST['roba_id'];
			$cart->cartDelete();
			echo $cart->rezultat;			
		break;
		case "cart_delete_all":
			$cart = new Cart($pgConn);
			$cart->cartDeleteAll();
			echo $cart->rezultat;			
		break;
		case "cart_order":
			//Ako je neulogovan korisnik, dodacemo ga u bazu
			if($_SESSION['ulogovan']==''){
				$user = new User($pgConn);
				//Zeli registraciju
				if($_POST['registration']==1){
					$user->lozinka = $_POST['password'];
					$user->status_registracije = 1;
					$user->flag_potvrda = 1;
				}
				$user->ime = iconv('UTF-8', 'WINDOWS-1250',$_POST['name']);
				$user->adresa = iconv('UTF-8', 'WINDOWS-1250',$_POST['address']);
				$user->telefon = $_POST['phone'];
				$user->email = $_POST['email'];
				$user->flag_potvrda = 0;
				$user->partner_id = -1;
				$user->prezime = iconv('UTF-8', 'WINDOWS-1250',$_POST['surname']);
				$user->flag_prima_poruke = $_POST['newsletter'];
				$user->telefon_mobilni = $_POST['mobile'];
				$user->naziv = iconv('UTF-8', 'WINDOWS-1250',$_POST['company']);
				$user->mesto_id = $_POST['place'];
				$user->pib = $_POST['pib'];		
				$user->status_registracije = 0;	
				$user->flag_vrsta_kupca = $_POST['vrsta_kupca'];	
				$user->userAdd();
			}
			//echo $_POST['pib'].''.$_POST['company'];
			//Unosimo i narudzbinu
			$order = new Order($pgConn);
			$order->web_nacin_placanja_id = $_POST['payment'];
			$order->web_nacin_isporuke_id = $_POST['shipment'];
			$order->napomena = iconv('UTF-8', 'WINDOWS-1250',$_POST['notice']);;
			$order->orderAdd();
			echo $order->order_result;				
		break;
		case "contact_send":
			$contact = new Misc($pgConn);
			$contact->contact_forma = true;
			$contact->contact_email = $_POST['email'];
			$contact->contact_name = iconv('UTF-8', 'WINDOWS-1250',$_POST['name']);
			$contact->contact_question = iconv('UTF-8', 'WINDOWS-1250',$_POST['question']);
			$contact->miscContactForm();
			echo $contact->contact_rezultat;
		break;
		case "user_register_add":
			$user = new User($pgConn);
			$user->lozinka = $_POST['password'];
			$user->ime = iconv('UTF-8', 'WINDOWS-1250',$_POST['name']);
			$user->adresa = iconv('UTF-8', 'WINDOWS-1250',$_POST['address']);
			$user->telefon = $_POST['phone'];
			$user->email = $_POST['email'];
			$user->flag_potvrda = 0;
			$user->partner_id = -1;
			$user->prezime = iconv('UTF-8', 'WINDOWS-1250',$_POST['surname']);
			$user->flag_prima_poruke = 0;
			$user->flag_vrsta_kupca = $_POST['vrsta_kupca'];
			$user->telefon_mobilni = $_POST['mobile'];
			$user->naziv = iconv('UTF-8', 'WINDOWS-1250',$_POST['company']);
			$user->mesto_id = $_POST['place'];
			$user->pib = $_POST['pib'];
			$user->status_registracije = 1;
			$user->userAdd();
			echo $user->user_rezultat;
		break;
		case "user_register_update":
			$user = new User($pgConn);
			$user->lozinka = $_POST['password'];
			$user->ime = iconv('UTF-8', 'WINDOWS-1250',$_POST['name']);
			$user->adresa = iconv('UTF-8', 'WINDOWS-1250',$_POST['address']);
			$user->telefon = $_POST['phone'];
			$user->email = $_POST['email'];
			$user->flag_potvrda = 0;
			$user->partner_id = -1;
			$user->prezime = iconv('UTF-8', 'WINDOWS-1250',$_POST['surname']);
			$user->flag_prima_poruke = 0;
			$user->flag_vrsta_kupca = $_POST['vrsta_kupca'];
			$user->telefon_mobilni = $_POST['mobile'];
			$user->naziv = iconv('UTF-8', 'WINDOWS-1250',$_POST['company']);
			$user->mesto_id = $_POST['place'];
			$user->pib = $_POST['pib'];
			$user->status_registracije = 1;
			$user->userUpdate();
			echo $user->user_rezultat;
		break;
		case "user_login":
			$user = new User($pgConn);
			$user->lozinka = $_POST['password'];
			$user->email = $_POST['email'];
			$user->userLogin();
			echo $user->login_rezultat;
		break;
		case "user_check_cart":
			$user = new User($pgConn);
			$user->userCheckCart();
			echo $user->check_cart_rezultat;
		break;
		case "user_logout":
			$user = new User($pgConn);
			$user->userLogout();
		break;
		case "user_forgot":
			$user = new User($pgConn);
			$user->email = $_POST['email'];
			$user->userForgotPass();
			echo $user->user_forgot_result;
		break;
		case "comment_add":
			$comment = new Comments($pgConn);
			$comment->roba_id = $_POST['roba_id'];
			$comment->ime_osobe = iconv('UTF-8', 'WINDOWS-1250',$_POST['ime_osobe']);
			$comment->pitanje = iconv('UTF-8', 'WINDOWS-1250',$_POST['pitanje']);
			$comment->commentsAdd();
			echo $comment->comment_rezultat;
		break;
	}
	
}
?>