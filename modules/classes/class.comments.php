<?php
class Comments{

	//Constructor
	public function __construct(pgsql $con) {
        $this->con = $con;
    }

    //Treba da napisemo i destructor, najverovatnije sa NULL - ISPROBATI!!!
    public function __destruct(){
    	$this->con = NULL;
    }

    //Dodaj komentar
	public function commentsAdd(){
		$misc = new Misc($this->con);
		$sPolja = '';
		$sVrednosti = '';
		$sPolja .= "roba_id,";		$sVrednosti .= "$1,";
		$sPolja .= "ime_osobe,";	$sVrednosti .= "$2,";
		$sPolja .= "ip_adresa,";	$sVrednosti .= "$3,";
		$sPolja .= "pitanje,";		$sVrednosti .= "$4,";
		$sPolja .= "datum";			$sVrednosti .= "$5";
		$sql = "INSERT INTO web_b2c_komentari (" . $sPolja . ") VALUES (" . $sVrednosti . ")";	
		$this->con->query($sql,"web_b2c_komentari_unos",array($this->roba_id,$this->ime_osobe,$misc->miscGetIpAdress(),$this->pitanje,date("Y-m-d")));
		if($this->con->affectedRows() > 0){
            $this->comment_rezultat = '0';
        }else{
            $this->comment_rezultat = '1';
        }
	}


    //Vraca sadrzaj korpe kroz array
    public function commentsShow(){
    	$sql = "SELECT * FROM web_b2c_komentari WHERE roba_id = $1 AND komentar_odobren = 1";
    	$this->con->query($sql,"web_b2c_komentari_odogovor",array($this->roba_id));		
    	$ret = array();
    	while($rowComments = $this->con->fetchArray()){
    		$ret[]=$rowComments;
    	};
    	return $ret;   	
    }
}


?>