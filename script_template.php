<?php
require_once("modules/classes/all.php");
$misc = new Misc($pgConn);
if(isset($_POST['action'])){ $action = $_POST['action']; } else { $action = "-1"; } // Akcija koja se primenjuje
if(isset($_POST['naziv_template'])){ $naziv_template = $_POST['naziv_template']; } else { $naziv_template = "unknown"; } // Naziv templata
if(isset($_POST['naziv_fajla'])){ $naziv_fajla = $_POST['naziv_fajla']; } else { $naziv_fajla = "unknown"; } // Naziv fajla

//U zavisnosti od akcije
switch($action){
	//Upload fajla
	case "0":
		$rezultat = '-1';
		if (isset($_FILES['file']) && $_FILES['file']['error'] == UPLOAD_ERR_OK){
  			$target = './templates/'.$_FILES['file']['name'];
  			$result = move_uploaded_file($_FILES['file']['tmp_name'], $target);
			rename('./templates/'.$_FILES['file']['name'], './templates/'.$naziv_fajla);
			
  			if ($result){
    			$rezultat = '0';//Iskopiran fajl
  			}else{
  				$rezultat = '1';//Ne mogu da iskopiram fajl
  			}
		}else{
			$rezultat = '2';//Fajl nije poslat
		}
		echo $rezultat;
	break;

	//Izvrsavanje
	case "1":
		$rezultat = '-1';
		//Podesavanje vrednosti
		$web_templates_id = $misc->miscGetSequence('web_templates_web_templates_id_seq');
		$putanja_template = '/templates/'.$naziv_template.'/structure.css';

		//Raspakivanje fajla
		$rar_file = rar_open('templates/'.$naziv_fajla) or die("1");
		$entries = rar_list($rar_file);
		foreach ($entries as $entry) {
    		$entry->extract('templates/'.$naziv_template.'/');
		}
		rar_close($rar_file);
		
		//Unos u bazu za template
		$sql = "INSERT INTO web_templates(web_templates_id, naziv, putanja)VALUES ($1, $2, $3)";
		$pgConn->query($sql,"web_templates",array($web_templates_id,$naziv_template,$putanja_template));

		//Unos u bazu za color
		if($pgConn->affectedRows() > 0){
			//Prebroj koliko fajlova ima u color
			$files = array_diff(scandir('templates/'.$naziv_template.'/color/'), array('..', '.'));
			foreach($files as $file) {
				$sql = "INSERT INTO web_templates_schemas(web_templates_id, naziv, putanja)VALUES ($1, $2, $3);";
				$pgConn->query($sql,"web_templates_schemas",array($web_templates_id,basename($file, ".css"),'/templates/'.$naziv_template.'/color/'.$file));
				$rezultat = '0';
			}	
		}else{
			$rezultat = '2';
		}

		//Setovanje za template
		$sql = "UPDATE web_templates SET selected = 0";
		$pgConn->query($sql,"web_templates_update_off",array());
		$sql = "UPDATE web_templates SET selected = 1 WHERE web_templates_id = $1";
		$pgConn->query($sql,"web_templates_update_on",array($web_templates_id));
		$sql = "UPDATE web_templates_schemas SET selected = 1 WHERE web_templates_id = $1 AND web_templates_schemas_id = (SELECT web_templates_schemas_id FROM web_templates_schemas WHERE web_templates_id = $1 LIMIT 1)";
		$pgConn->query($sql,"web_templates_schemas_update_on",array($web_templates_id));
		//Signal za reset sesija
		$sql = "UPDATE options SET int_data = 1 WHERE options_id = 1330";
		$pgConn->query($sql,"reset_sesion_status",array());
		
		//Brisi fajl
		unlink('templates/'.$naziv_fajla);

		echo $rezultat;
	break;
}



















?>