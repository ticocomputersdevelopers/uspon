
ALL REQUESTS REQUIRED BASIC AUTH, credentials exists for each user of the admin panel

Base API domain is shop domain.

//CATEGORIES
category object:
	id                          integer | exists in db             // used to identify existing items, not used for creating new item
	external_code               text    | max:255                  // can be used to identify existing items
	name                        text    | required | max:200
	parent_id                   integer | exists in db             // use parent_id or parent_external_code
	parent_external_code        text    | max:255 | exists in db
	show                  		integer in(0,1)
	show_b2b                  	integer in(0,1)
	image_path                  text
	sort                        integer
	childs                      array of category objects          

endpionts:
	GET:/api/v1/categories
	
	POST:/api/v1/categories-save
		request:
			array of objects:
				id
				external_code
				name
				parent_id
				parent_external_code
				show
				show_b2b

	DELETE:/api/v1/categories-delete
		request:
			array of objects:                                                   //id or external_code
				id                  integer | exists in db    
				external_code       text    | max:255 


//ARTICLES
article object:
	id                            integer | exists in db             // used to identify existing items, not used for creating new item
	external_code                 text    | max:255                  // can be used to identify existing items
	name                          text    | required | max:300      
	web_name                      text    | required | max:300
	category_id                   integer | exists in db             // category_id or category_external_code
	category_external_code        max:255 | exists in db
	brand_name                    text    | max:100
	active                        integer in(0,1)
	show                          integer in(0,1)
	show_b2b                      integer in(0,1)
	locked                        integer in(0,1)
	type_name                     text    | max:200
	vat_value                     numeric | max_digits:7
	measure_name                  text    | max:30
	html_description              max:5000
	basic_b2c_price               numeric | max_digits:15
	basic_b2b_price               numeric | max_digits:15
	web_price                     numeric | max_digits:15
	retail_price                  numeric | max_digits:15
	sale_price                    numeric | max_digits:15
	web_margin                    numeric | max_digits:7
	retail_margin                 numeric | max_digits:7
	tags_list                     text    | max:500
	images                        array of objects:
							    		id           	integer | exists in db     // used to identify existing items, not used for creating new item
								    	image_file   	mimes:jpg,png,jpeg,gif | max:10MB
								    	active       	integer in(0,1)
					    				default      	integer in(0,1)
	stock                         array of objects:
								    	stockroom_name      text    | required | max:100 | in(RetailWarehouse,WholesaleWarehouse)
								    	quantity            required| numeric  | max_digits:7
								    	reserved            numeric|digits_between:1,7

endpionts:
	GET:/api/v1/articles
		url query:
        	page                  integer | min:1
        	limit                 integer | max:1000
        	id                    integer | exists in db
        	category_id           integer | exists in db
        	brand_id              integer | exists in db
            name                  text    | max:300
            external_codes        array   			          // example: ?external_codes[]=code1&external_codes[]=code2

	POST:/api/v1/articles-save
		request:
			array of objects:
				id
				external_code
				name   
				web_name
				category_id
				category_external_code
				brand_name
				active
				show
				show_b2b
				locked
				type_name
				vat_value
				measure_name
				html_description
				basic_b2c_price
				basic_b2b_price5
				web_price
				retail_price
				sale_price
				web_margin
				retail_margin
				images
				stock

	DELETE:/api/v1/articles-delete
		request:
			array of objects:                                                         //id or external_code
				id                    integer | exists in db
				external_code         text    | max:255 


//PARTNERS
partner object:
	id                          integer | exists in db             // used to identify existing items, not used for creating new item
	external_code               text    | max:255                  // can be used to identify existing items
	name                        text    | required | max:255 
	address                     text    | max:255
	city                        text    | max:200 
	phone                       text    | max:255 
	emai                        email   | max:255 
	tax_id                      integer | min_digits:9 | max_digits:9
	company_registration_number integer | min_digits:8 | max_digits:8
	bank_account_number         trxt    | max:255
	rebate                      integer | max_digits:9    

endpionts:
	GET:/api/v1/partners
		url query:
        	page                         integer | min:1
        	limit                        integer | max:1000
        	id                           integer | exists in db
        	name                         text    | max:300
        	address                      text    | max:255
        	city                         text    | max:200
        	email                        text    | max:100
        	tax_id                       integer | min_digits:9 | max_digits:9
        	company_registration_number  integer | min_digits:8 | max_digits:8
        	external_code                text    | max:255

	POST:/api/v1/partners-save
		request:
			array of objects:
				id                			integer | exists in db             // used to identify existing items, not used for creating new item
				external_code     			text    | max:255                  // can be used to identify existing items
	    		name              			text    | required | max:255
	        	address           			text    | max:255
	        	city              			text    | max:200
	        	phone             			text    | max:255
	        	email                       		email   | max:100
	        	tax_id                      		integer | min_digits:9 | max_digits:9
	        	company_registration_number 		integer | min_digits:8 | max_digits:8
	        	bank_account_number         		text    | max:255
	        	rebate                      		numeric | max_digits:7

	DELETE:/api/v1/partners-delete
		request:
			array of objects:                                                //id or external_code
				id                  integer | exists in db
				external_code       text    | max:255 


//PARTNER CARDS
partner card object:
	id                           integer    | exists in db              // used to identify existing items, not used for creating new item
	external_code                text       | max:255                   // can be used to identify existing items
	partner_id                   integer    | required | exists in db   // use partner_id or partner_external_code
	partner_external_code        text       | max:255  | exists in db
	document_kind                text       | required | max:200
	document_date                date:Y-m-d | required
	incoming_date                date:Y-m-d
	description                  text       | max:300
	debit                        numeric    | max_digits:9
	credit                       numeric    | max_digits:9

endpionts:
	POST:/api/v1/partner-cards-save
		request:
			array of objects:
				id
				external_code
				partner_id
				partner_external_code
		    	document_kind
		    	document_date
		    	incoming_date
		    	description
		    	debit
		    	credit

	DELETE:/api/v1/partner-cards-delete
		request:
			array of objects:                                                         //id or external_code
				id                    integer | exists in db
				external_code         text    | max:255



//B2C ORDERS
b2c order object:
	id                            integer | exists in db             // used to identify existing items, not used for creating new item
    document_number               text    | max:20 | exists in db    // can be used to identify existing items, autocrated
    customer_id                   integer | required | exists in db
    date                          date:Y-m-d H:i:s | required
    status                        in(NEW,ACCEPTED,REALIZED,CANCELED)
    note                          text    | max:2000
	items                         array of objects:
			                        id             integer | exists in db
			                        sort           integer | max:1000
			                        article_id     integer | required | exists in db
			                        quantity       numeric | required | max_digits:7
			                        vat_id         integer | required | exists in db
			                        basic_price    numeric | max_digits:15
			                        unit_price     numeric | max_digits:15

endpionts:
	GET:/api/v1/b2c-orders
		url query:
        	page                  integer | min:1
        	limit                 integer | max:1000
        	id                    integer | exists in db
        	document_number       text    | max:20
        	customer_id           integer | exists in db
        	customer_external_code text    | max:255 | exists in db
        	status                in(NEW,ACCEPTED,REALIZED,CANCELED)
        	date_from             date:Y-m-d H:i:s

	POST:/api/v1/b2c-orders-save
		request:
			array of objects:
				id
				document_number
		    	customer_id
		    	date
		    	status
		    	note
				items

	DELETE:/api/v1/b2c-orders-delete
		request:
			array of objects:                                                         //id or document_number
				id                    integer | exists in db
				document_number       text    | max:20

	DELETE:/api/v1/b2c-order-items-delete
		request:
			array of objects:
				id                    integer | exists in db




//B2B ORDERS
b2b order object:
	id                            integer | exists in db             // used to identify existing items, not used for creating new item
    document_number               text    | max:20 | exists in db    // can be used to identify existing items, autocrated
    partner_id                    integer | required | exists in db
    date                          date:Y-m-d H:i:s | required
    status                        in(NEW,ACCEPTED,REALIZED,CANCELED)
    note                          text    | max:2000
	items                         array of objects:
			                        id             integer | exists in db
			                        sort           integer | max:1000
			                        article_id     integer | required | exists in db
			                        quantity       numeric | required | max_digits:7
			                        vat_id         integer | required | exists in db
			                        basic_price    numeric | max_digits:15
			                        unit_price     numeric | max_digits:15

endpionts:
	GET:/api/v1/b2b-orders
		url query:
        	page                  integer | min:1
        	limit                 integer | max:1000
        	id                    integer | exists in db
        	document_number       text    | max:20
        	partner_id            integer | exists in db
        	partner_external_code text    | max:255 | exists in db
        	status                in(NEW,ACCEPTED,REALIZED,CANCELED)
        	date_from             date:Y-m-d 

	POST:/api/v1/b2b-orders-save
		request:
			array of objects:
				id
				document_number
		    	partner_id
		    	date
		    	status
		    	note
				items

	DELETE:/api/v1/b2b-orders-delete
		request:
			array of objects:                                                         //id or document_number
				id                    integer | exists in db
				document_number       text    | max:20

	DELETE:/api/v1/b2b-order-items-delete
		request:
			array of objects:
				id                    integer | exists in db
