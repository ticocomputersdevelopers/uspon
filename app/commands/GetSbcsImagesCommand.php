<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
// use Intervention\Image\Facades\Image;


class GetSbcsImagesCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'get:sbcs:images';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Resize images.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$sbcsConn = DB::connection('sbcs');
		$pulsConn = DB::connection('puls');
		$sirina_big = $pulsConn->table('options')->where('options_id',1331)->pluck('int_data');

		$sbcs_images = $sbcsConn->select("SELECT roba_id, slika FROM web_slika WHERE roba_id IS NOT NULL");
		foreach ($sbcs_images as $key => $sbcs_image) {
			$roba_id = $pulsConn->table('roba')->where('id_is',$sbcs_image->roba_id)->pluck('roba_id');
			if(!is_null($roba_id)){

                    $slika_id = $pulsConn->select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
                    $name = $slika_id.'.jpg';
                    $putanja = 'images/products/big/'.$name;

                    if(self::saveImageFile($putanja,$sbcs_image->slika,$sirina_big)){
	                    $insert_data = array(
	                        'web_slika_id'=>intval($slika_id),
	                        'roba_id'=> $roba_id,
	                        'akcija'=> $key==0 ? 1 : 0, 
	                        'flag_prikazi'=>1,
	                        'putanja'=>$putanja
	                        );
                    	$pulsConn->table('web_slika')->insert($insert_data);
	                }

 

			}
		}

		$this->info('Finish.');
	}

	public static function saveImageFile($putanja,$content,$sirina_big=600){
		try {
			$stream_content = stream_get_contents($content);
			$img_file = fopen($putanja,"wb");
			fwrite($img_file, $stream_content);
			fclose($img_file);

			if(filesize($putanja) <= 1000000){
				// Image::make($putanja)->resize($sirina_big, null, function ($constraint) { $constraint->aspectRatio(); })->save();	
			}
			return true;
		} catch (Exception $e) {
			File::delete($putanja);
			return false;
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('transfer_domain', InputArgument::OPTIONAL, 'Image path on root.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}