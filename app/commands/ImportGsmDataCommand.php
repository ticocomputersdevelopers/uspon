<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;



class ImportGsmDataCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'import:gsm:data';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import gsm data.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$sirina_big = DB::table('options')->where('options_id',1331)->pluck('int_data');
		$roba = DB::table('roba')->select('roba_id','id_is')->whereNotNull('id_is')->where('id_is','!=','')->get();

		foreach($roba as $row){
			$opis_path = base_path().'/gsm_data/opis/'.trim($row->id_is).'.txt';
			if(File::exists($opis_path)){
				$opis_array = explode("\n", file_get_contents($opis_path));

				$opis = $this->convert(implode('<br>',$opis_array));
				DB::table('roba')->where('roba_id',$row->roba_id)->update(array('web_opis'=>$opis));
			}
		}

		foreach($roba as $row){
			$image_path = base_path().'/gsm_data/slike/'.trim($row->id_is).'.jpg';

			if(File::exists($image_path)){
                $slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
                $name = $slika_id.'.jpg';
                $putanja = base_path().'/images/products/big/'.$name;

                $image_path = str_replace('/var/www/html/wbp/','',$image_path);
                if(self::saveImageFile($putanja,$image_path,$sirina_big)){
                    $insert_data = array(
                        'web_slika_id'=>intval($slika_id),
                        'roba_id'=> $row->roba_id,
                        'akcija'=> 1, 
                        'flag_prikazi'=>1,
                        'putanja'=>$putanja
                        );
                	DB::table('web_slika')->insert($insert_data);
                }

			}
		}
	}

	protected function convert($string){
		// $string = pg_escape_string($string);
		$string = iconv("WINDOWS-1250", "UTF-8//TRANSLIT//IGNORE",$string);
		return $string;
	}

	protected function saveImageFile($destination,$source,$sirina_big=600){
		try {
			File::copy($source,$destination);

			// if(filesize($putanja) <= 1000000){
				// Image::make($destination)->resize($sirina_big, null, function ($constraint) { $constraint->aspectRatio(); })->save();	
			// }
			return true;
		} catch (Exception $e) {
			File::delete($destination);
			return false;
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('path', InputArgument::OPTIONAL, 'Image path on root.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}