<?php
ini_set('memory_limit', '-1');

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;



class TransferDetailsStasaCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'transfer:articles:details';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'TransferDetailsStasaCommand';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$file_path = "artikli.dat";

		$subject = file_get_contents($file_path);
		$subject = preg_replace("/(^(?![0-9]{1,6}\|RSD))\n/im","$1",$subject);
		$rows = explode("\n",$subject);

		$products = array_filter($rows,function($row){
			return count(explode('|',$row)) == 10;
		});

       
        foreach($products as $product) {

            $sifra_logik = trim($worksheet->getCell('A'.$row)->getValue());
            $sifra_old = trim($worksheet->getCell('B'.$row)->getValue());

            $article_old = $transfer_pgsql->select("SELECT roba_id, naziv_web, web_opis, web_karakteristike FROM roba WHERE sifra_d = '1310".trim($sifra_old)."'");
            $article = DB::select("SELECT roba_id FROM roba WHERE id_is = '".trim($sifra_logik)."'");
            if(isset($article_old[0]) && isset($article[0])){
            	      	
				DB::table('roba')->where('roba_id',$article[0]->roba_id)->update(array('naziv_web' => $article_old[0]->naziv_web,'web_opis'=>$article_old[0]->web_opis, 'web_karakteristike'=>$article_old[0]->web_karakteristike,'sifra_is'=>trim($sifra_old)));

				if(count(DB::table('web_slika')->where('roba_id',$article[0]->roba_id)->get()) == 0){
					$slike = $transfer_pgsql->select("SELECT akcija, putanja FROM web_slika WHERE roba_id = ".$article[0]->roba_id."");		
		            foreach($slike as $slika){
		                try { 
		                    $slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
		                    $name = $slika_id.'.jpg';
		                    $destination = 'images/products/big/'.$name;
		                    $url = 'http://nijansa.rs/'.$slika->putanja;
		                    $akcija = $slika->akcija;
                    	
	                        $content = @file_get_contents($url);
	                        file_put_contents($destination,$content);

		                    $insert_data = array(
		                        'web_slika_id'=>intval($slika_id),
		                        'roba_id'=> $article[0]->roba_id,
		                        'akcija'=>$akcija, 
		                        'flag_prikazi'=>1,
		                        'putanja'=>'images/products/big/'.$name
		                        );

		                    DB::table('web_slika')->insert($insert_data);
		 
		                }
		                catch (Exception $e) {
		                }
		            }

				}

            }

        }

		$this->info('Finish.');
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('transfer_domain', InputArgument::OPTIONAL, 'Image path on root.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}