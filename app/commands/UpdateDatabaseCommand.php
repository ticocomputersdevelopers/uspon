<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpdateDatabaseCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'update:db';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		DB::statement("create or replace function addcol(schemaname varchar, tablename varchar, colname varchar, coltype varchar)
returns varchar 
language 'plpgsql'
as 
$$
declare 
    col_name varchar ;
begin 
      execute 'select column_name from information_schema.columns  where  table_schema = ' ||
      quote_literal(schemaname)||' and table_name='|| quote_literal(tablename) || '   and    column_name= '|| quote_literal(colname)    
      into   col_name ;   

      raise info  ' the val : % ', col_name;
      if(col_name is null ) then 
          col_name := colname;
          execute 'alter table ' ||schemaname|| '.'|| tablename || ' add column '|| colname || '  ' || coltype; 
      else
           col_name := colname ||' Already exist';
      end if;
return col_name;
end;
$$");
		DB::statement("create or replace function dropcol(schemaname varchar, tablename varchar, colname varchar)
returns varchar 
language 'plpgsql'
as 
$$
declare 
    col_name varchar ;
begin 
      execute 'select column_name from information_schema.columns  where  table_schema = ' ||
      quote_literal(schemaname)||' and table_name='|| quote_literal(tablename) || '   and    column_name= '|| quote_literal(colname)    
      into   col_name ;   

      raise info  ' the val : % ', col_name;
      if(col_name is null ) then 
           col_name := colname ||' Not exist';
      else
          col_name := colname;
          execute 'alter table ' ||schemaname|| '.'|| tablename || ' drop column '|| colname; 
      end if;
return col_name;
end;
$$");
		DB::statement("create or replace function addtable(schemaname varchar, tablename varchar, body varchar)
returns varchar 
language 'plpgsql'
as 
$$
declare 
    tab_name varchar;
begin 
      execute 'select table_name from information_schema.tables  where  table_schema = ' ||
      quote_literal(schemaname)||' and table_name='|| quote_literal(tablename)    
      into   tab_name;   

      raise info  ' the val : % ', tab_name;
      if(tab_name is null ) then 
          tab_name := tablename;
          execute 'create table ' ||schemaname|| '.'|| tablename || ' ('|| body ||') with (OIDS=TRUE)';
      else
           tab_name := tablename ||' Already exist';
      end if;
return tab_name;
end;
$$");

DB::statement("CREATE OR REPLACE FUNCTION addconstraint (t_name text, c_name text, constraint_sql text)
  RETURNS void
AS
$$
  begin
    -- Look for our constraint
    if not exists (SELECT * FROM information_schema.constraint_column_usage where constraint_name = c_name) 
    	then
        execute 'ALTER TABLE ' || t_name || ' ADD CONSTRAINT ' || c_name || ' ' || constraint_sql;
    end if;
end;
$$
LANGUAGE plpgsql VOLATILE;");

	}

	/**
	 * Execute the console command. 
	 *
	 * @return mixed
	 */
	public function fire()
	{
		DB::statement("SELECT addcol('public','prodavnica_tema','shop','integer DEFAULT 1')");
		//teme
		DB::statement("INSERT INTO prodavnica_tema (prodavnica_tema_id,naziv,putanja,aktivna,shop) SELECT * FROM (VALUES 
	    	(6,'Avatar','avatar/',1,1),
	    	(7,'Clothes','clothes/',1,1),
	    	(8,'Shoes','shoes/',1,1),
	    	(9,'Prezentacija Opšta','landingPage/',1,0),
	    	(10,'Furniture','furniture/',1,1),
	    	(11,'Prezentacija 2','landingPage_v2/',1,0),
	    	(12,'Fashion','fashion/',1,1)) 
	    	redovi(prodavnica_tema_id,naziv,putanja,aktivna,shop) 
	        WHERE prodavnica_tema_id NOT IN (SELECT prodavnica_tema_id FROM prodavnica_tema)");

	    DB::statement("INSERT INTO prodavnica_stil (prodavnica_stil_id,prodavnica_tema_id,naziv,putanja,aktivna,izabrana,zakljucana) SELECT * FROM (VALUES 
	    	(13,6,'Light','light/',1,0,0), (14,6,'Light Green','light-green/',1,0,0), 
	    	(15,6,'Light Orange','light-orange/',1,0,0), (16,7,'Light','light/',1,0,0), 
	    	(17,8,'Light','light/',1,0,0), (18,8,'Sportska oprema','sportska_oprema/',1,0,0), 
	    	(19,9,'Light','light/',1,0,0), (20,10,'Light','light/',1,0,0), (21,11,'Blue','blue/',1,0,0), (22,12,'Light','light/',1,0,0))
	    	redovi(prodavnica_stil_id,prodavnica_tema_id,naziv,putanja,aktivna,izabrana,zakljucana) 
	        WHERE prodavnica_stil_id NOT IN (SELECT prodavnica_stil_id FROM prodavnica_stil)");

	    //promena tipa u grupa_pr
	    DB::statement("ALTER TABLE grupa_pr ALTER COLUMN rabat TYPE numeric");
	    //promena tipa u proizvodjac
	    DB::statement("ALTER TABLE proizvodjac ALTER COLUMN rabat TYPE numeric");

	    //web_b2c_seo
	    DB::statement("SELECT addcol('public','web_b2c_seo','grupa_pr_id','integer DEFAULT -1')");
 		DB::statement("SELECT addcol('public','web_b2c_seo','tekst','character varying (300)')");
 		DB::statement("SELECT addcol('public','web_b2c_seo','parrent_id','integer DEFAULT 0')");
	    // DB::statement("INSERT INTO web_b2c_seo (naziv_stranice,title,menu_top,header_menu,footer,b2b_header,b2b_footer,disable,tip_artikla_id) SELECT * FROM (VALUES 
	    // 	('o-nama','O nama',0,0,0,0,0,0,-1)
	    // 	) redovi(naziv_stranice,title,menu_top,header_menu,footer,b2b_header,b2b_footer,disable,tip_artikla_id) 
	    //     WHERE naziv_stranice NOT IN (SELECT naziv_stranice FROM web_b2c_seo)");

		// add table baneri jezik
	    DB::statement("SELECT addtable('public','baneri_jezik','baneri_id integer NOT NULL,jezik_id integer NOT NULL,naslov character varying(100),nadnaslov character varying(100),sadrzaj text,naslov_dugme character varying(100), CONSTRAINT baneri_jezik_pkey PRIMARY KEY (baneri_id, jezik_id), CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	    //vezani_artikli
	    DB::statement("SELECT addcol('public','vezani_artikli','cena','numeric(22,2)')");


		//preduzece
		DB::statement("SELECT addcol('public','preduzece','instagram','character varying(255)')");
		DB::statement("SELECT addcol('public','preduzece','linkedin','character varying(255)')");
		DB::statement("SELECT addcol('public','preduzece','youtube','character varying(255)')");
		DB::statement("SELECT addcol('public','preduzece','potpis','text')");
		DB::statement("ALTER TABLE preduzece ALTER COLUMN delatnost_sifra TYPE character varying(255)");
		DB::statement("ALTER TABLE preduzece ALTER COLUMN naziv TYPE character varying(255)");

		//posta_slanje
		DB::statement("SELECT addcol('public','posta_slanje','difolt','smallint DEFAULT 0')");
		DB::statement("SELECT addcol('public','posta_slanje','api_aktivna','smallint DEFAULT 0')");
		DB::statement("SELECT addcol('public','posta_slanje','nasa_sifra','character varying(255)')");
		
		DB::statement("DELETE FROM options WHERE options_id IN (1340,3014,3015,3016,3017,3027,3031)");

	    //options
	    DB::statement("INSERT INTO options (options_id,preduzece_id,naziv,str_data,int_data) SELECT * FROM (VALUES (3023,1,'Intesa banking Client ID',null,0),
	    	(3014,1,'Automatika - Menjanje flaga na web-u u zavisnosti od cene',null,0),
	        (3024,1,'Intesa banking Store Key',null,0),
	        (3025,1,'Uključen limit za artikle',null,1),
	        (3026,1,'AOP limit za artikle',null,100),
	        (3028,1,'Web cena kao osnovna B2B cena',null,1),
	        (3029,1,'Skraćeni admin',null,1),
	        (3030,1,'Brzo umatičenje artikala',null,0),
	        (3031,1,'Pakovanje artikla',null,0),
	        (3032,1,'Kombinacija rabata - rabat IS kategoriaja',null,0),
	        (3033,1,'Web Import - update naziva',null,0),
	        (3034,1,'Korišćenje kategorije kupca za formiranje cena na B2B-u',null,0),
	        (3035,1,'Slanje mail-a kupcu posle obrade narudžbine',null,0),
	        (3036,1,'Facebook pixel',null,0),
	        (3037,1,'Google Cloud API key',null,0),
	        (3038,1,'Korišćenje google translator-a',null,0),
	        (3039,1,'Garancija',null,0),
	        (3040,1,'RMA modul',null,0),
	        (3041,1,'RMA uslužni servis',null,1),
	        (3042,1,'Default Korišćenje definisanih marži',null,0),
	        (3043,1,'RMA prikaz cene rezervnih delova',null,1),
	        (3044,1,'Admin B2B options excel cenovnik',null,0),
	        (3050,1,'Dokumenti - Računi',null,0),
	        (3055,1,'Elasticsearch',null,0),
			(3056,1,'Elasticsearch cron update',null,1),
	        (3057,1,'B2C-BEZ KOLICINE',null,1),
	        (3058,1,'LoadBEE',null,0),
	        (3059,1,'Flixmedia',null,0),
	        (3060,1,'Drip',null,0),
	        (3061,1,'Neaktivna prodavnica',null,0),
	        (3062,1,'Pitch Print',null,0),
	        (3063,1,'CRM',null,0),
	        (3064,1,'Custom CSS',null,0)
	        ) redovi(options_id,preduzece_id,naziv,str_data,int_data) 
	        WHERE options_id NOT IN (SELECT options_id FROM options)");
	    //web options
		DB::statement("INSERT INTO web_options (web_options_id,sekcija_id,naziv,str_data,int_data,bool_data,aplikacija_id) SELECT * FROM (VALUES 
			(98,1,'Prikaz akcije na početnoj strani',null,1,TRUE,100),
			(99,1,'Prikaz blogova na početnoj',null,1,TRUE,100),
			(150,1,'Troskovi isporuke',null,0,TRUE,100),
			(151,1,'Prikaz najpopularnijih proizvoda na B2B-u',null,1,TRUE,100),
			(152,1,'Zaokruživanje cena',null,0,TRUE,100),
			(153,1,'Prikaz gen. karateristika kod liste artikla',null,0,TRUE,100),
			(100,1,'Korisnik mora da uradi potvrdu registracije',null,0,TRUE,100),
			(101,1,'Broj proizvoda na akciji',null,1,TRUE,100),
			(200,1,'Prikaz sifre,id,sifre_d ili seo',null,1,TRUE,100),
			(201,1,'Vrsta sifre webu',null,1,TRUE,100),
			(203,1,'Prikaz sifre',null,0,TRUE,100),
			(204,1,'Kurs na B2C-u',null,0,TRUE,100),
			(205,1,'Kurs na B2B-u',null,0,TRUE,100),
			(206,1,'Artikli akcije i tipova na početnoj, bez ajax-a u slick-u',null,1,TRUE,100),
			(207,1,'Cene opadajući redosled',null,0,TRUE,100),
			(208,1,'Najnoviji, najpopularniji, najprodavaniji na početnoj',null,1,TRUE,100),
			(209,1,'Zaokruživanje cena, broj decimala',null,0,TRUE,100),
			(210,1,'Pracenje lagera B2B',null,1,TRUE,100),
			(310,1,'Dodatne slike',null,0,TRUE,100),
			(311,1,'PDF u narudzbini',null,1,TRUE,100),
			(312,1,'Akcija tajmer',null,0,TRUE,100),
			(313,1,'Kupovina na rate',null,0,TRUE,100),
			(314,1,'Koriscenje bodova',null,0,TRUE,100),
			(315,1,'Maksimalni broj bodova',null,0,TRUE,100),
			(316,1,'Rok trajanja bodova (u danima)',null,0,TRUE,100),
			(317,1,'Vrednost boda',null,0,TRUE,100),
			(318,1,'Koriscenje vaučera',null,0,TRUE,100),
			(319,1,'Ankete',null,0,TRUE,100),
			(320,1,'Kupovina na gramažu',null,0,TRUE,100),
			(321,1,'Podešavanja header-a, puna širina',null,0,TRUE,100),
			(322,1,'Podešavanja footer-a, puna širina',null,0,TRUE,100),
			(323,1,'Cookies',null,0,TRUE,100)
			) redovi (web_options_id,sekcija_id,naziv,str_data,int_data,bool_data,aplikacija_id) WHERE web_options_id NOT IN (SELECT web_options_id FROM web_options)");
	    
	    //update export
	    DB::statement("INSERT INTO export (export_id,naziv,vrednost_kolone,dozvoljen) SELECT * FROM (VALUES (1,'IT Svet','it-svet',0),(2,'Pametno.rs','pametno-rs',0),(3,'Shopmania','shopmania',0),(4,'Magento','magento',0),(5,'Woocommerce','woocommerce',0),(6,'Shopify','shopify',0),(7,'Eponuda','eponuda',0),(8,'Kupindo','kupindo',0),(9,'CeneRS','ceners',1)) redovi(export_id,naziv,vrednost_kolone,dozvoljen) 
	        WHERE export_id NOT IN (SELECT export_id FROM export)");

	    //dodavanje dve kolone u export
		DB::statement("SELECT addcol('public','export','clientid','character varying')");
		DB::statement("SELECT addcol('public','export','clientsecret','character varying')");

	    DB::statement("SELECT addtable('public','export_grupe','export_grupe_id bigserial NOT NULL, grupa_id bigint NOT NULL, naziv character varying(255), parent_id bigint, grupa_pr_ids character varying(255), export_id integer NOT NULL, CONSTRAINT export_grupe_pkey PRIMARY KEY (export_grupe_id)')");
	    DB::statement("DROP TABLE IF EXISTS shopmania_grupe");

	   	DB::statement("SELECT addcol('public','roba','visina','numeric(20,4)')");
	    DB::statement("SELECT addcol('public','roba','sirina','numeric(20,4)')");
	    DB::statement("SELECT addcol('public','roba','duzina','numeric(20,4)')");
	    DB::statement("SELECT addcol('public','roba','bruto_masa','numeric(20,4)')");
	    DB::statement("SELECT addcol('public','roba','jm_mase','integer DEFAULT -1')");


	    DB::statement("SELECT dropcol('public','roba','narudzbina_status_id')");

	    // DB::statement("INSERT INTO narudzbina_status (narudzbina_status_id,naziv,selected) SELECT * FROM (VALUES 
	    // 	(1,'Bez statusa',1)) 
	    // 	redovi(narudzbina_status_id,naziv,selected) 
	    //     WHERE narudzbina_status_id NOT IN (SELECT narudzbina_status_id FROM narudzbina_status)");

		DB::statement("SELECT addcol('public','grupa_pr','sifra_is','character varying(255)')");
		DB::statement("SELECT addcol('public','grupa_pr','id_is','character varying(255)')");
		DB::statement("SELECT addcol('public','grupa_pr','pozadinska_slika','character varying(255)')");

		DB::statement("SELECT addcol('public','proizvodjac','id_is','character varying(255)')");
		DB::statement("SELECT addcol('public','web_slika','id_is','character varying(255)')");



		//dodaj id_kategorije u partner tabelu
		DB::statement("SELECT addcol('public','partner','id_kategorije','integer DEFAULT NULL')");


/*****************************************************************************************************************/
		// B2C baneri
		DB::statement("SELECT addcol('public','baneri','aktivan','integer DEFAULT 1')");
		DB::statement("SELECT addcol('public','baneri','datum_od','date')");
		DB::statement("SELECT addcol('public','baneri','datum_do','date')");
		DB::statement("SELECT addcol('public','baneri','link2','character varying(255)')");

		//B2B baneri
		DB::statement("SELECT addtable('public','baneri_b2b','baneri_id bigserial, img text, link text, redni_broj integer,naziv character (255), tip_prikaza integer NOT NULL, web_b2b_seo_id integer')");
		// B2B baneri
		DB::statement("SELECT addcol('public','baneri_b2b','aktivan','integer DEFAULT 1')");
		DB::statement("SELECT addcol('public','baneri_b2b','datum_od','date')");
		DB::statement("SELECT addcol('public','baneri_b2b','datum_do','date')");
		DB::statement("SELECT addcol('public','baneri_b2b','link2','character varying(255)')");
/*****************************************************************************************************************/
		// B2B Vesti
		DB::statement("SELECT addtable('public','web_vest_b2b','web_vest_b2b_id bigserial, datum date, naslov character varying(200), aktuelno smallint DEFAULT 1,slika character varying (300), rbr integer')");
		DB::statement("SELECT addtable('public','web_vest_b2b_jezik','web_vest_b2b_id bigint NOT NULL, jezik_id integer, naslov character varying(200), sadrzaj text, title character varying (100), description character varying(255), keywords character varying(255)')");
		// b2b seo
		DB::statement("SELECT addtable('public','web_b2b_seo','web_b2b_seo_id bigserial, naziv_stranice character varying(300), title character varying(300), description text, keywords text, og_title character varying(300), og_description character varying(300), og_image character varying(300), og_type character varying(300), og_url character varying(300), og_site_name character varying(300), twitter_card character varying(300), twitter_title character varying(300), twitter_description character varying(300), twitter_url character varying(300), twitter_image character varying(300), twitter_creator character varying(300), flag_page integer, rb_strane integer, web_content text, seo_title character varying(255), flag_b2b_show integer, tip_artikla_id integer DEFAULT -1, disable integer DEFAULT 0, menu_top integer DEFAULT 1, header_menu integer DEFAULT 1, footer integer DEFAULT 1, b2b_header integer DEFAULT 1, b2b_footer integer DEFAULT 1, grupa_pr_id integer DEFAULT -1')"); 
		DB::statement("SELECT addtable('public','web_b2b_seo_jezik','web_b2b_seo_id integer NOT NULL, jezik_id integer NOT NULL, sadrzaj text, title character varying(100), description character varying(255), keywords character varying(255)')");	

/*****************************************************************************************************************/	    
		//informacioni sistem
	    DB::statement("SELECT addtable('public','informacioni_sistem','informacioni_sistem_id bigserial NOT NULL, naziv character varying(255), sifra character varying(255), aktivan smallint DEFAULT 0, izabran smallint DEFAULT 0, CONSTRAINT informacioni_sistem_pkey PRIMARY KEY (informacioni_sistem_id)')");
	    
	    DB::statement("UPDATE informacioni_sistem SET naziv = 'Roaming', sifra = 'roaming' WHERE informacioni_sistem_id = 6");
	    DB::statement("INSERT INTO informacioni_sistem (informacioni_sistem_id, naziv, sifra, aktivan, izabran) SELECT * FROM (VALUES (1, 'Excel fajl', 'excel', 1, 0),(2, 'Wings', 'wings', 1, 0),(3, 'Calculus', 'calculus', 1, 0),(4, 'Infograf', 'infograf', 1, 0),(5, 'Logik', 'logik', 1, 0),(6, 'Roaming', 'roaming', 1, 0),(7, 'SBCS', 'sbcs', 1, 0),(8, 'Skala', 'skala', 1, 0),(9, 'GSM', 'gsm', 1, 0),(10, 'Promobi', 'promobi', 1, 0),(11, 'NSSport', 'nssport', 1, 0),(12, 'Xml', 'xml', 1, 0),(13, 'SoftCom', 'softcom', 1, 0),(14, 'Panteon', 'panteon', 1, 0),(15, 'TimCode', 'timcode', 1, 0),(16, 'Navigator', 'navigator', 1, 0),(17, 'SpSoft', 'spsoft', 1, 0)) redovi(informacioni_sistem_id, naziv, sifra, aktivan, izabran) WHERE informacioni_sistem_id NOT IN (SELECT informacioni_sistem_id FROM informacioni_sistem)");
	    DB::statement("SELECT addcol('public','informacioni_sistem','api_url','character varying(255)')");
	    DB::statement("SELECT addcol('public','informacioni_sistem','portal','character varying(255)')");
	    DB::statement("SELECT addcol('public','informacioni_sistem','username','character varying(255)')");
	    DB::statement("SELECT addcol('public','informacioni_sistem','password','character varying(255)')");
	    DB::statement("SELECT addcol('public','informacioni_sistem','b2b_magacin','character varying(255)')");
	    DB::statement("SELECT addcol('public','informacioni_sistem','b2c_magacin','character varying(255)')");
	    DB::statement("SELECT addcol('public','informacioni_sistem','mp_kupac_id_is','character varying(255)')");


	    DB::statement("SELECT addtable('public','narudzbina_opstina','narudzbina_opstina_id bigserial NOT NULL,code integer NOT NULL,naziv character varying(255),ptt character varying(255), CONSTRAINT narudzbina_opstina_unique UNIQUE (code), CONSTRAINT narudzbina_opstina_id_pkey PRIMARY KEY (narudzbina_opstina_id, code)')");

	    DB::statement("SELECT addtable('public','narudzbina_mesto','narudzbina_mesto_id bigserial NOT NULL,code integer NOT NULL,narudzbina_opstina_code integer NOT NULL,naziv character varying(100),ptt character varying(255), CONSTRAINT narudzbina_mesto_unique UNIQUE (code), CONSTRAINT narudzbina_mesto_pkey PRIMARY KEY (narudzbina_mesto_id, code), CONSTRAINT narudzbina_opstina_code_fkey FOREIGN KEY (narudzbina_opstina_code) REFERENCES public.narudzbina_opstina (code) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	    DB::statement("SELECT addtable('public','narudzbina_ulica','narudzbina_ulica_id bigserial NOT NULL,code integer NOT NULL,narudzbina_mesto_code integer NOT NULL,naziv character varying(100),ptt character varying(255), CONSTRAINT narudzbina_ulica_unique UNIQUE (code,narudzbina_ulica_id), CONSTRAINT narudzbina_ulica_pkey PRIMARY KEY (narudzbina_ulica_id, code), CONSTRAINT narudzbina_mesto_code_fkey FOREIGN KEY (narudzbina_mesto_code) REFERENCES public.narudzbina_mesto (code) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

		//web_kupac
		DB::statement("SELECT addcol('public','web_kupac','ulica_id','integer')");
		DB::statement("SELECT addcol('public','web_kupac','broj','character varying(255)')");
		DB::statement("SELECT addcol('public','web_kupac','posta_br','integer')");

		// DB::statement("ALTER TABLE web_kupac ADD CONSTRAINT web_kupac_ulica_id_fkey FOREIGN KEY (ulica_id) REFERENCES public.narudzbina_ulica (narudzbina_ulica_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE");

	    DB::statement("SELECT addtable('public','posta_status','posta_status_id bigserial NOT NULL,code character varying(100),name character varying(100),description character varying(255), posta_slanje_id integer NOT NULL, CONSTRAINT posta_status_pkey PRIMARY KEY (posta_status_id), CONSTRAINT posta_slanje_id_fkey FOREIGN KEY (posta_slanje_id) REFERENCES public.posta_slanje (posta_slanje_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

/*******************************************************************************/
	    DB::statement("SELECT addtable('public','partner_grupa','partner_grupa_id bigserial NOT NULL, partner_id integer NOT NULL,grupa_pr_id integer NOT NULL,grupa character varying(255),web_marza numeric(20,4) DEFAULT 0,mp_marza numeric(20,4) DEFAULT 0, CONSTRAINT partner_grupa_pkey PRIMARY KEY (partner_grupa_id), CONSTRAINT partner_grupa_partner_id_fkey FOREIGN KEY (partner_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT partner_grupa_grupa_pr_id_fkey FOREIGN KEY (grupa_pr_id) REFERENCES public.grupa_pr (grupa_pr_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
	    DB::statement("SELECT addtable('public','partner_proizvodjac','partner_proizvodjac_id bigserial NOT NULL, partner_id integer NOT NULL,proizvodjac_id integer NOT NULL,proizvodjac character varying(255), CONSTRAINT partner_proizvodjac_pkey PRIMARY KEY (partner_proizvodjac_id), CONSTRAINT partner_proizvodjac_partner_id_fkey FOREIGN KEY (partner_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT partner_proizvodjac_proizvodjac_id_fkey FOREIGN KEY (proizvodjac_id) REFERENCES public.proizvodjac (proizvodjac_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	    //tabela za kategorije
	    DB::statement("SELECT addtable('public','partner_kategorija','id_kategorije bigserial NOT NULL, naziv character varying(255), rabat integer, active integer DEFAULT 0, CONSTRAINT partner_kategorija_pkey PRIMARY KEY (id_kategorije)')");

	    DB::statement("SELECT addtable('public','nivo_pristupa_proizvodjac','partner_id integer NOT NULL, proizvodjac_id integer NOT NULL, unactive integer DEFAULT 1, grupa_pr_id integer NOT NULL')");
        DB::statement("SELECT addtable('public','nivo_pristupa_grupa','partner_id integer NOT NULL, grupa_pr_id integer NOT NULL, unactive integer DEFAULT 1')");
        DB::statement("SELECT addtable('public','nivo_pristupa_artikal','partner_id integer NOT NULL, roba_id integer NOT NULL, grupa_pr_id integer DEFAULT -1, unactive integer DEFAULT 1')");
        DB::statement("SELECT addtable('public','nivo_pristupa_lager','partner_id integer NOT NULL, orgj_id integer NOT NULL, unactive integer DEFAULT 1')");
	    DB::statement("ALTER TABLE partner_kategorija ALTER COLUMN rabat TYPE numeric");

	    //tabele za log
	    DB::statement("SELECT addtable('public','log_akcija','log_akcija_id bigserial NOT NULL, sifra character varying(255), naziv character varying(255), CONSTRAINT log_akcija_pkey PRIMARY KEY (log_akcija_id)')");

	    DB::statement("SELECT addtable('public','log_korisnik','korisnik_id integer, log_akcija_id integer, datum timestamp, ids text, naziv_proizvoda text DEFAULT NULL, CONSTRAINT FK_imenik FOREIGN KEY (korisnik_id) REFERENCES imenik(imenik_id), CONSTRAINT FK_log_akcija FOREIGN KEY (log_akcija_id) REFERENCES log_akcija(log_akcija_id)')");

	    DB::statement("SELECT addtable('public','log_b2b_akcija','log_b2b_akcija_id bigserial NOT NULL, sifra character varying(255), naziv character varying(255), CONSTRAINT log_b2b_akcija_pkey PRIMARY KEY (log_b2b_akcija_id)')");

	    DB::statement("SELECT addtable('public','log_b2b_partner','partner_id integer, log_b2b_akcija_id integer, datum timestamp, ids text, naziv_proizvoda text DEFAULT NULL, CONSTRAINT FK_partner FOREIGN KEY (partner_id) REFERENCES partner(partner_id), CONSTRAINT FK_log_akcija FOREIGN KEY (log_b2b_akcija_id) REFERENCES log_b2b_akcija(log_b2b_akcija_id)')");

/*******************************************************************************/

	    DB::statement("INSERT INTO posta_status (code, name, description, posta_slanje_id) SELECT * FROM (VALUES 
	    	('-2', 'Obrisana pošiljka', 'Obrisana pošiljka', 3),
	    	('-1', 'Storno isporuke', 'Storno isporuke', 3),
	    	('0', 'Kreirana pošiljka', 'Priprema za dostavu', 3),
	    	('1', 'Pošiljka isporučena', 'Pošiljka je isporučena', 3),
	    	('3', 'Pošiljka preuzeta', 'Pošiljka preuzeta', 3),
	    	('5', 'Odbijena pošiljka', 'Pošiljka je odbijena od strane primaoca', 3),
	    	('6', 'Nema nikog', 'Pokušana isporuka, nema nikoga na adresi', 3),
	    	('7', 'Na odmoru', 'Pokušana isporuka, primalac je na godišnjem odmoru', 3),
	    	('8', 'Netačna adresa', 'Pokušana isporuka, netačna je adresa primaoca', 3),
	    	('9', 'Nema novac', 'Pokušana isporuka, primalac nema novac', 3),
	    	('10', 'Neodgovarajuća sadržina', 'Sadržaj pošiljke nije odgovarajući', 3),
	    	('11', 'Oštećena pošiljka', 'Pošiljka je oštećena-reklamacioni postupak', 3),
	    	('12', 'Primalac odložio', 'Isporuka odložena u dogovoru sa primaocem', 3),
	    	('20', 'Pošiljka vraćena pošiljaocu', 'Pošiljka vraćena pošiljaocu', 3),
	    	('21', 'Kreiran povrat', 'Pošiljka se vraća pošiljaocu', 3),
	    	('22', 'Ukinut povrat', 'Ukinut povrat pošiljke', 3),
	    	('105', 'Odbijena pošiljka', 'Pošiljka je odbijena od strane primaoca', 3),
	    	('106', 'Nema nikog', 'Ponovni pokušaj isporuke, nema nikoga na adresi', 3),
	    	('107', 'Na odmoru', 'Ponovni pokušaj isporuke, primalac je na odmoru', 3),
	    	('108', 'Netačna adresa', 'Ponovni pokušaj isporuke, netačna adresa primaoca', 3),
	    	('109', 'Nema novac', 'Ponovni pokušaj isporuke, primalac nema novac', 3),
	    	('110', 'Neodgovarajuća sadržina', 'Sadržaj pošiljke nije odgovarajući', 3),
	    	('111', 'Oštećena pošiljka', 'Pošiljka je oštećena-reklamacioni postupak', 3),
	    	('112', 'Primalac odložio', 'Isporuka odložena u dogovoru sa primaocem', 3)
	    	) redovi(code, name, description, posta_slanje_id) 
	        WHERE NOT EXISTS (SELECT * FROM posta_status)");

	    DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posta_zahtev','integer NOT NULL DEFAULT 0')");
	    DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posta_reference_id','character varying(255)')");
	    DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posta_response_data','character varying(500)')");

		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posiljalac_id','integer DEFAULT 1')");
		DB::statement("SELECT addconstraint('web_b2c_narudzbina_stavka','web_b2c_narudzbina_stavka_posiljalac_id_fkey','FOREIGN KEY (posiljalac_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE SET NULL ON DELETE SET NULL')");

		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posta_slanje_id','integer')");
		DB::statement("SELECT addconstraint('web_b2c_narudzbina_stavka','web_b2c_narudzbina_stavka_posta_slanje_id_fkey','FOREIGN KEY (posta_slanje_id) REFERENCES public.posta_slanje (posta_slanje_id) MATCH SIMPLE ON UPDATE SET NULL ON DELETE SET NULL')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posta_status_id','integer')");
		DB::statement("SELECT addconstraint('web_b2c_narudzbina_stavka','web_b2c_narudzbina_stavka_posta_status_id_fkey','FOREIGN KEY (posta_status_id) REFERENCES public.posta_status (posta_status_id) MATCH SIMPLE ON UPDATE SET NULL ON DELETE SET NULL')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','ulica_id','integer')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','broj','character varying(255)')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','broj_paketa','numeric(20,4) DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','web_nacin_placanja_id','integer')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','transport_placa','integer DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','cena_otkupa','numeric(20,4) DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','racun_otkupa','character varying(255)')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','otkup_prima','integer DEFAULT -1')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','vrednost','numeric(20,4) DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','masa','numeric(20,4) DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','odgovor','integer DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','napomena','character varying(255)')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','opis','character varying(255)')");

		DB::statement("SELECT addcol('public','web_b2c_narudzbina','popust','numeric(20,2) DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2c_narudzbina','komentar','character varying')");

		DB::statement("SELECT addcol('public','preduzece','ulica_id','integer')");
		DB::statement("SELECT addcol('public','preduzece','broj','character varying(255)')");
		DB::statement("SELECT addcol('public','preduzece','kontakt_osoba','character varying(255)')");
		
		DB::statement("SELECT addcol('public','partner','ulica_id','integer')");
		DB::statement("SELECT addcol('public','partner','broj','character varying(255)')");
		DB::statement("SELECT addcol('public','partner','kontakt_osoba','character varying(255)')");
		DB::statement("ALTER TABLE partner ALTER COLUMN naziv TYPE character varying(255)");
		DB::statement("ALTER TABLE partner ALTER COLUMN login TYPE character varying(255)");
		DB::statement("ALTER TABLE partner ALTER COLUMN password TYPE character varying(255)");
		DB::statement("ALTER TABLE partner ALTER COLUMN racun TYPE character varying(255)");
		DB::statement("ALTER TABLE partner ALTER COLUMN adresa TYPE character varying(255)");
		DB::statement("ALTER TABLE partner ALTER COLUMN telefon TYPE character varying(255)");

	    DB::statement("INSERT INTO export_grupe (export_grupe_id, grupa_id, naziv, export_id) SELECT * FROM (VALUES 
	    	(1,18,'Desktop računari',8), (2,2725,'Bicikli',8), (3,1981,'Prirodne nauke',8), (4,2729,'Dodatna oprema',8), (5,2733,'Rezervni delovi',8), (6,2013,'Hrišćanstvo',8), (7,4353,'LiceUlice',8), (8,2053,'Majice i košulje',8), (9,518,'Programi',8), (10,1031,'Srbija i ex-YU',8), (11,524,'Antropologija',8), (12,1036,'Automobili',8), (13,528,'Bušilice',8), (14,1809,'Kućišta, maske i fioke za laptop',8), (15,1044,'Ručni satovi',8), (16,534,'Sklopke',8), (17,1050,'Etno predmeti',8), (18,1056,'Jugoslavija 1918-41',8), (19,545,'Setovi',8), (20,1313,'Pribor za zavarivanje i lemljenje',8), (21,291,'Ploče',8), (22,297,'Nakit',8), (23,301,'Šeširi i kačketi',8), (24,1070,'CEPT i Evropa',8), (25,1077,'Azija',8), (26,1349,'Pneumatski pištolji i creva',8), (27,1097,'Srbija i YU 1876-1960',8), (28,2889,'Sport',8), (29,1102,'Srbija i YU 1868-1945',8), (30,1118,'Ulje',8), (31,1127,'Srbija i ex-YU do 1945',8), (32,1143,'Delovi',8), (33,1153,'Punjači za laptop',8), (34,901,'Elektronika',8), (35,136,'Mobilni telefoni',8), (36,4237,'Bade mantil',8), (37,1685,'Futrole',8), (38,675,'Romani',8), (39,973,'Gelovi',8), (40,976,'Ukrasi',8), (41,2001,'Katalozi',8), (42,1493,'Politikin Zabavnik',8), (43,3029,'Akcija / Avantura',8), (44,1245,'Burgije',8), (45,232,'Automehanika',8), (46,1012,'Figure',8), (47,1017,'Srbija i ex-YU',8), (48,2041,'Decoupage predmeti',8), (49,2553,'Sadnice',8), (50,506,'Router i switch',8), (51,1277,'Voltmetri, ampermetri i termometri',8), (52,766,'Senke i olovke za oči',8), (53,2541,'Seme',8), (54,3593,'Stari nakit',8), (55,1024,'Gradovi',8), (56,2305,'Sanitarije',8), (57,517,'Operativni sistemi i drajveri',8), (58,1032,'Inostranstvo',8), (59,1289,'Ključevi',8), (60,1545,'Bonelli stripovi',8), (61,269,'Maske',8), (62,1039,'Lutke',8), (63,1043,'Džepni satovi',8), (64,533,'Aku alati',8), (65,1813,'Šarke za laptop',8), (66,2325,'Oprema za pse',8), (67,535,'Prekidači i utičnice',8), (68,1305,'Aparati za zavarivanje',8), (69,28,'Optički uređaji',8), (70,2589,'Za pripremu hrane',8), (71,288,'Audio diskovi',8), (72,1057,'Jugoslavija 1944-60',8), (73,1063,'Rusija',8), (74,1833,'Konzole',8), (75,2349,'Vrata',8), (76,2613,'Kamere i kompleti',8), (77,1078,'Afrika',8), (78,1597,'Felne i ratkapne',8), (79,2365,'Lampe',8), (80,2621,'Fiskalne kase',8), (81,72,'Klima uređaji i ventilatori',8), (82,1353,'Pumpe, dizalice i prese',8), (83,1098,'Srbija i YU 1961-danas',8), (84,1103,'Srbija i YU 1946-danas',8), (85,1111,'LCD',8), (86,1113,'Fotografije, grafike i crteži',8), (87,2905,'Životinje',8), (88,1117,'Kombinovane tehnike',8), (89,1128,'Srbija i ex-YU 1945-60',8), (90,108,'Patike',8), (91,2925,'Domaća zimnica',8), (92,110,'Cipele',8), (93,370,'Torbe, torbice i novčanici',8), (94,117,'Mirisi',8), (95,2421,'Šerpe, lonci, tiganji i džezve',8), (96,1142,'Oprema',8), (97,1917,'Kardio sprave',8), (98,132,'Fotoaparati i kamere',8), (99,1925,'Pidžame i bade mantili',8), (100,2449,'Ukrasi',8), (101,4241,'Benkica',8), (102,153,'Audio komponente',8), (103,2713,'Nakit i ukrasi za kosu',8), (104,419,'Arheologija',8), (105,2477,'Posteljine',8), (106,431,'Žičani instrumenti',8), (107,2737,'Sportska obuća',8), (108,441,'Televizori',8), (109,1721,'Brushalteri i setovi',8), (110,1213,'Stolarske mašine',8), (111,2749,'Štapovi za pecanje',8), (112,1473,'Laptopovi',8), (113,1989,'Osnovna škola',8), (114,1481,'Baterije za laptop',8), (115,971,'Četkice',8), (116,975,'Nalepnice',8), (117,2005,'Brošure',8), (118,2521,'Zavesa',8), (119,987,'Košulje',8), (120,1501,'Sport i hobi',8), (121,990,'Haljine',8), (122,1249,'Glodala i noževi',8), (123,2017,'Priče',8), (124,2273,'Garniture, ležajevi i fotelje',8), (125,482,'Majice',8), (126,233,'Autoelektrika',8), (127,2793,'Roleri i rolšue',8), (128,491,'Haljine',8), (129,1011,'Posuđe',8), (130,2037,'Decoupage oprema',8), (131,1273,'Mikrometri i pomična merila',8), (132,1018,'Inostranstvo',8), (133,507,'Mrežni kablovi',8), (134,763,'Sjajevi, olovke i karmini',8), (135,3069,'Biografija',8), (136,1281,'Vage',8), (137,771,'Lakovi',8), (138,267,'SIM kartice (brojevi)',8), (139,1805,'Konektori i kablovi',8), (140,2573,'Lukovice i rizomi',8), (141,1038,'Makete',8), (142,529,'Brusilice',8), (143,1045,'Zidni satovi',8), (144,1048,'Narodne nošnje',8), (145,1051,'Gobleni',8), (146,1309,'Lemilice',8), (147,543,'Klešta',8), (148,289,'Audio kasete',8), (149,1058,'Jugoslavija 1961-80',8), (150,298,'Satovi i oprema',8), (151,1068,'Bugarska',8), (152,1073,'Australija',8), (153,1329,'Spojnice',8), (154,1090,'Bonovi, obveznice i menice',8), (155,1096,'Evropa',8), (156,1099,'Antika i Srednji vek',8), (157,1116,'Akvarel, akril i tempera',8), (158,2909,'Crtani, film i junaci',8), (159,1120,'Ikone',8), (160,1129,'Srbija i ex-YU 1961-80',8), (161,1393,'Poljoprivredne mašine',8), (162,3193,'Etnografija',8), (163,1149,'Torbe, rančevi i futrole',8), (164,897,'Građevinski materijal',8), (165,904,'Ostalo',8), (166,140,'Baterije',8), (167,2959,'Touch screen (bez ekrana)',8), (168,4245,'Bermude',8), (169,2967,'Digitroni',8), (170,674,'Bajke',8), (171,970,'Tipse',8), (172,977,'Šabloni',8), (173,1241,'Brusni materijali',8), (174,2009,'Uputstva',8), (175,1505,'Muzika i film',8), (176,227,'Do 50 ccm',8), (177,238,'Enterijer',8), (178,1010,'Lampe',8), (179,499,'Suknje',8), (180,505,'Wireless kartice',8), (181,3065,'Drama',8), (182,1019,'Ostalo',8), (183,764,'Puderi i rumenila',8), (184,2199,'Miljei',8), (185,2533,'Ukrasno cveće',8), (186,2877,'Sport',8), (187,2185,'Ukrasi',8), (188,2545,'Saksije i žardinjere',8), (189,1025,'Kompanije',8), (190,2561,'Preparati',8), (191,1541,'Alan Ford',8), (192,1801,'Inverteri za laptop',8), (193,2313,'Slavine i tuševi',8), (194,1035,'Disney',8), (195,525,'Etnologija',8), (196,1297,'Noževi i sečiva',8), (197,787,'Nalepnice i folije',8), (198,532,'Testere',8), (199,789,'Budizam',8), (200,25,'Tastature',8), (201,793,'Mitologija',8), (202,1049,'Ćilimi i tkanja',8), (203,2329,'Oprema za mačke',8), (204,284,'Filmovi',8), (205,29,'Grafičke kartice',8), (206,2593,'Za termičku obradu hrane',8), (207,1059,'Jugoslavija 1981-91',8), (208,1065,'Mađarska',8), (209,299,'Naočare i oprema',8), (210,1325,'Ventili i manometri',8), (211,1837,'Kontroleri',8), (212,2353,'Prozori',8), (213,1076,'Severna Amerika',8), (214,1589,'Stoni satovi',8), (215,2617,'Ostala sigurnosna oprema',8), (216,1089,'Pribor i literatura',8), (217,1601,'Xenon kompleti i sijalice',8), (218,2625,'Ostala oprema',8), (219,1093,'Azija',8), (220,74,'Frižideri i zamrzivači',8), (221,2637,'Optički instrumenti',8), (222,2893,'Životinje',8), (223,2385,'Sijalice',8), (224,1106,'Nemačka',8), (225,1115,'Pastel, kolaž i vitraž',8), (226,97,'Obuća',8), (227,1121,'Skulpture i maske',8), (228,2913,'Ostalo',8), (229,105,'Cipele',8), (230,1130,'Srbija i ex-YU od 1981',8), (231,111,'Čizme',8), (232,2929,'Domaći džem, pekmez i marmelada',8), (233,2429,'Posude za čuvanje hrane i pića',8), (234,1921,'Sprave za vežbe snage',8), (235,1929,'Gaće',8), (236,2453,'Satovi',8), (237,4249,'Bluza',8), (238,154,'Slušalice i mikrofoni',8), (239,157,'Video i DVD plejeri',8), (240,2717,'Kape, rukavice i šalovi',8), (241,426,'Duvački instrumenti',8), (242,2481,'Prekrivači, jorgani i ćebad',8), (243,2741,'Dresovi i sportska odeća',8), (244,1725,'Spavaćice',8), (245,2753,'Mašinice',8), (246,1993,'Srednja škola',8), (247,972,'Lampe',8), (248,978,'Ostalo',8), (249,979,'Ostalo',8), (250,1753,'Flat kablovi',8), (251,988,'Mantili',8), (252,2269,'Stolovi, stolice i klupe',8), (253,2525,'Oprema',8), (254,3037,'Epska fantastika',8), (255,223,'Car Hifi',8), (256,991,'Mantili',8), (257,480,'Košulje',8), (258,228,'Do 250 ccm',8), (259,1253,'Ležajevi',8), (260,1509,'Nauka i kultura',8), (261,2025,'Basne',8), (262,234,'Autolimarija',8), (263,2797,'Skejt',8), (264,1013,'Ostalo',8), (265,1269,'Multimetri',8), (266,508,'Modemi',8), (267,765,'Maskare i eyelineri',8), (268,2897,'Crtani, filmovi i junaci',8), (269,1037,'Avioni',8), (270,1293,'Ureznice i nareznice',8), (271,271,'Kablovi',8), (272,1041,'Kinder i PEZ figurice',8), (273,531,'Šlajferice',8), (274,790,'Hinduizam',8), (275,1046,'Ostalo',8), (276,1052,'Ostalo',8), (277,1066,'Poljska',8), (278,300,'Ukrasi za kosu',8), (279,1074,'Južna Amerika',8), (280,66,'Medicinske nauke',8), (281,1091,'Žetoni i čekovi',8), (282,1094,'Amerika i Australija',8), (283,1357,'Klipovi, cilindri i razvodnici',8), (284,1105,'Mađarska',8), (285,1361,'Libele i metri',8), (286,1119,'Ostalo',8), (287,1123,'Reprodukcije',8), (288,1131,'Evropa',8), (289,119,'Za kosu',8), (290,3197,'Filologija',8), (291,129,'Monitori',8), (292,898,'Za termotehniku',8), (293,2193,'Nameštaj',8), (294,4253,'Bodi',8), (295,1973,'Pidžame',8), (296,967,'Ostalo',8), (297,974,'Ostalo',8), (298,1757,'Zvučnici i mikrofoni',8), (299,229,'Do 750 ccm',8), (300,2021,'Pesme',8), (301,1261,'Pribor za stolarske mašine',8), (302,239,'Prateća oprema',8), (303,1777,'Ekrani za laptop',8), (304,498,'Pantalone',8), (305,3061,'Epska poezija',8), (306,504,'Mrežne karte i adapteri',8), (307,1533,'Časopisi za žene',8), (308,1829,'Motorna ulja',8), (309,2901,'Ostalo',8), (310,1026,'Politika',8), (311,1285,'Ostalo',8), (312,2309,'Za čišćenje i higijenu',8), (313,519,'Igre',8), (314,265,'Zvučnici',8), (315,272,'Tastature',8), (316,1040,'Ostalo',8), (317,2833,'Američki stripovi',8), (318,1821,'Kamere, zvučnici i mikrofoni',8), (319,30,'Hard diskovi i SSD',8), (320,544,'Šrafcigeri',8), (321,2341,'Oprema za akvaristiku',8), (322,2597,'Za pripremu kafe i čaja',8), (323,1064,'Čehoslovačka',8), (324,1321,'Kompresori',8), (325,1075,'Srednja Amerika',8), (326,2357,'Oprema',8), (327,2873,'Naočare za decu i oprema',8), (328,62,'Filozofija',8), (329,1092,'Ostalo',8), (330,1605,'LED trake i sijalice',8), (331,2373,'Lusteri',8), (332,2629,'Risiveri i tjuneri',8), (333,1095,'Afrika',8), (334,2641,'Oprema',8), (335,1107,'Italija',8), (336,2645,'Igre',8), (337,1377,'Za baštu',8), (338,1122,'Ramovi i oprema',8), (339,106,'Čizme',8), (340,1132,'Amerika i Australija',8), (341,116,'Lična higijena',8), (342,1653,'Lektire',8), (343,2933,'Domaće slatko',8), (344,128,'Sandale',8), (345,1417,'Kaiševi',8), (346,2441,'Servisi i tanjiri',8), (347,2701,'Nega i pribor',8), (348,142,'Zaštitne folije',8), (349,403,'Tehničke nauke',8), (350,151,'Šporeti, ploče i rerne',8), (351,2457,'Ogledala',8), (352,4257,'Čarape',8), (353,2211,'Ostalo',8), (354,676,'Slikovnice',8), (355,1189,'Udarne bušilice i čekići',8), (356,2215,'Miševi',8), (357,937,'Rezervni delovi i materijal',8), (358,1449,'Farmerke',8), (359,428,'Instrumenti sa dirkama',8), (360,2485,'Jastuci',8), (361,1729,'Korseti',8), (362,3017,'Erotski roman',8), (363,2261,'Ormari, vitrine, komode',8), (364,989,'Pantalone',8), (365,2781,'Najloni (strune)',8), (366,992,'Košulje',8), (367,481,'Jakne',8), (368,230,'Preko 750 ccm',8), (369,236,'Signalizacija',8), (370,241,'Čamci i brodovi',8), (371,1265,'Ostalo',8), (372,2801,'Delovi za rolere i skejt',8), (373,1529,'Časopisi za muškarce',8), (374,509,'Antene',8), (375,1861,'Kape, šalovi i rukavice',8), (376,2917,'Grafičke kartice',8), (377,934,'Rezervni delovi i materijal',8), (378,530,'Glodalice',8), (379,1301,'Čekići i stege',8), (380,791,'Islam',8), (381,1825,'Mrežne kartice i Bluetooth',8), (382,1060,'Nemačka',8), (383,1067,'Rumunija',8), (384,1333,'Ostalo',8), (385,1079,'Ostalo',8), (386,1857,'Kaputi i mantili',8), (387,1104,'Bugarska',8), (388,1365,'Za električne mašine',8), (389,1124,'Ostalo',8), (390,1381,'Agregati',8), (391,365,'Istorija i teorija književnosti i jezika',8), (392,1133,'Azija i Afrika',8), (393,1389,'Za građevinarstvo',8), (394,2161,'Gume',8), (395,1401,'Setovi za negu tela',8), (396,2169,'Predškolski uzrast',8), (397,1413,'Marame i ešarpe',8), (398,906,'Ostalo',8), (399,3213,'Geografija',8), (400,1761,'Kamere',8), (401,3045,'Horor',8), (402,486,'Trenerke',8), (403,490,'Džemperi',8), (404,2029,'Bojanke',8), (405,1521,'Časopisi za decu',8), (406,1385,'Mašine za sečenje',8), (407,2993,'Zastave',8), (408,2549,'Ostalo',8), (409,1027,'Sport',8), (410,1033,'Militarija',8), (411,2317,'Galanterija',8), (412,1553,'Disney stripovi',8), (413,2577,'Usisivači i pegle',8), (414,792,'Judaizam',8), (415,2333,'Oprema za ptice',8), (416,31,'Kablovi',8), (417,546,'Ručne testere',8), (418,1061,'Ostale bivše republike',8), (419,1069,'Skandinavija',8), (420,1849,'Oprema i delovi',8), (421,61,'Biznis i organizacija',8), (422,65,'Istorija',8), (423,2369,'Plafonjere',8), (424,1609,'Folije i nalepnice',8), (425,78,'Mikrotalasne rerne',8), (426,1108,'Turska',8), (427,107,'Papuče',8), (428,1134,'Praznične',8), (429,112,'Papuče',8), (430,114,'Kreme i mleka za telo',8), (431,1397,'Za mašine i alate',8), (432,2165,'Kursevi',8), (433,2937,'Med i proizvodi od meda',8), (434,2437,'Escajg i pribor',8), (435,1421,'Kravate i manžetne',8), (436,1677,'Portabl uređaji',8), (437,146,'Bluetooth, slušalice i handsfree',8), (438,914,'Ostalo',8), (439,147,'Kertridži i toneri',8), (440,148,'Projektori',8), (441,2461,'Kartolerija',8), (442,2979,'Edukativne knjige',8), (443,430,'Udaraljke',8), (444,1477,'Tableti',8), (445,1733,'Gaćice',8), (446,3013,'Istorijski roman',8), (447,1225,'Mašine za pranje pod pritiskom',8), (448,2761,'Udice',8), (449,1497,'Politika i svet',8), (450,993,'Pantalone',8), (451,994,'Suknje',8), (452,483,'Odela i sakoi',8), (453,1765,'Ostalo',8), (454,235,'Vetrobrani i prozori',8), (455,492,'Jakne',8), (456,1773,'Tastature za laptop',8), (457,2285,'Kreveti i dušeci',8), (458,502,'Bebi oprema',8), (459,2861,'Kišobrani',8), (460,2517,'Peškiri',8), (461,2857,'Pozamanterija i izrada nakita',8), (462,3077,'Klasici',8), (463,8,'Promo materijali',8), (464,526,'Kriminalistika',8), (465,24,'Štampači',8), (466,1817,'Kuleri i hladnjaci za laptop',8), (467,1071,'Ostalo',8), (468,1585,'Za teretna i kombi vozila',8), (469,2865,'Plakati i posteri',8), (470,1082,'Srbija',8), (471,1109,'Švedska',8), (472,1373,'Ašovi i lopate',8), (473,1136,'Flora i fauna',8), (474,121,'Za mršavljenje',8), (475,899,'Stolarski materijal',8), (476,141,'Punjači',8), (477,2975,'Enciklopedije za decu',8), (478,936,'HTZ oprema',8), (479,1965,'Veš majice',8), (480,1513,'Kompjuterski časopisi',8), (481,493,'Kaputi i mantili',8), (482,1977,'Bade mantili',8), (483,3001,'Bodi',8), (484,1028,'Auto',8), (485,1797,'Procesori',8), (486,268,'Držači',8), (488,2581,'Za negu tela i zdravlje',8), (489,2837,'Evropski i ostali stripovi',8), (490,23,'Skeneri',8), (491,32,'Kuleri',8), (492,2337,'Ostala oprema',8), (493,2849,'Stari uređaji',8), (494,1317,'Ostalo',8), (495,1080,'SCG 1992-2006',8), (496,2633,'Oprema i delovi',8), (497,75,'Mašine za veš i sudove',8), (498,1100,'Ostatak Evrope',8), (499,2381,'LED trake',8), (500,2405,'Police',8), (501,616,'Ostalo',8), (502,109,'Sandale',8), (503,2413,'Čaše',8), (504,1135,'Komične',8), (505,113,'Patike',8), (506,115,'Kozmetika za brijanje',8), (507,2941,'Sokovi',8), (508,2689,'Nameštaj',8), (509,3201,'Lingvistika',8), (510,907,'Ostalo',8), (511,916,'Ostalo',8), (512,664,'Ostalo',8), (513,155,'Mini linije',8), (514,2971,'Ostalo',8), (515,2465,'Baštenski dekor',8), (516,4261,'Farmerke / Pantalone',8), (517,2219,'Rezervni delovi i oprema za tablet',8), (518,429,'Oprema i delovi',8), (519,2489,'Stolnjaci',8), (520,1229,'Konfekcijske i šivaće mašine',8), (521,2765,'Varalice',8), (522,478,'Duksevi',8), (523,2529,'Delovi i oprema',8), (524,487,'Bunde',8), (525,1000,'Salvete',8), (526,3057,'Lirska poezija',8), (527,1525,'Automobilizam',8), (528,1517,'National Geographic',8), (529,1537,'Ostalo',8), (530,270,'Memorijske kartice',8), (531,527,'Pedagogija',8), (532,27,'Zvučnici',8), (533,1054,'Austrija',8), (534,59,'Rečnici i leksikoni',8), (535,1101,'Ostatak Sveta',8), (536,80,'Kućišta',8), (537,1369,'Kosačice, trimeri i lančane testere',8), (538,865,'Prirodni preparati',8), (539,1137,'Ostalo',8), (540,2957,'Za venčanje',8), (541,2963,'Lazy bag',8), (542,3005,'Ljubavni roman',8), (543,497,'Odela i sakoi',8), (544,242,'Društvene igre',8), (545,1781,'Memorija za laptop',8), (546,1020,'Ulaznice i pozivnice',8), (547,905,'Kontroleri',8), (548,3085,'Memoari',8), (549,2585,'Fiksni telefoni i fax',8), (550,2841,'Manga stripovi',8), (551,26,'Web kamere',8), (552,33,'Matične ploče',8), (553,2609,'Oprema i delovi',8), (554,1084,'Dopisnice i koverte',8), (555,64,'Informacione tehnologije',8), (556,67,'Političke nauke',8), (557,73,'Bojleri',8), (558,2377,'Spotovi',8), (559,1110,'Ostalo',8), (560,360,'Enciklopedije i atlasi',8), (561,617,'Ostalo',8), (562,2153,'Militarija',8), (563,876,'Baletanke',8), (564,2417,'Šolje',8), (565,2945,'Vino',8), (566,2697,'Kolica',8), (567,144,'Ukrasi i olovke',8), (568,2469,'Ostalo',8), (569,4269,'Helanke',8), (570,2991,'Ortopedska pomagala',8), (571,2493,'Krpe i kecelje',8), (572,2769,'Primame i mamci',8), (573,479,'Džemperi',8), (574,2277,'Čiviluci, cipelarnici i vešalice',8), (575,1015,'Duvanijada',8), (576,1785,'Optički uređaji',8), (577,2297,'Grejna tela',8), (578,2809,'Zaštitna oprema i steznici',8), (579,1681,'PC gaming oprema',8), (580,1717,'USB flash memorije',8), (581,3589,'Popularna psihologija',8), (582,1055,'Flora i fauna',8), (583,34,'Memorije',8), (584,68,'Poljoprivreda',8), (585,357,'Antikvarne knjige',8), (586,398,'Pravo',8), (587,405,'Naučna fantastika',8), (588,489,'Duksevi',8), (589,1021,'Istorijski dokumenti',8), (590,1789,'Matične ploče',8), (591,69,'Psihologija',8), (592,1793,'Hard diskovi',8), (593,1557,'Revije',8), (594,35,'Napajanja',8), (595,2601,'Oprema i delovi',8), (596,811,'Rezervni delovi',8), (597,1081,'Sport',8), (598,326,'Domeni i hosting',8), (599,2389,'Spoljna rasveta',8), (600,3189,'Sociologija',8), (601,2425,'Modle i kalupi',8), (602,2693,'Auto sedišta',8), (603,2949,'Rakija i žestoka pića',8), (604,2497,'Otirači i prostirke',8), (605,4293,'Kaput',8), (606,3021,'Politički roman',8), (607,1237,'Ostalo',8), (608,2773,'Plovci i olova',8), (609,2265,'Kuhinjski elementi',8), (610,484,'Pantalone',8), (611,494,'Kupaći',8), (612,2813,'Sportski rekviziti',8), (613,36,'Procesori',8), (614,1083,'Umetnost',8), (615,60,'Alternativna učenja',8), (616,3205,'Teologija',8), (617,1437,'Bermude',8), (618,3049,'Pripovetke',8), (619,1957,'Šortsevi',8), (620,37,'TV, FM  i SAT Kartice',8), (621,1085,'UPU, UIT, ptt i komunikacije',8), (622,2393,'Profesionalna rasveta',8), (623,2433,'Sudopere',8), (624,2705,'Rekreacija',8), (625,1441,'Prsluci',8), (626,1445,'Farmerke',8), (627,425,'Žurnalistika i novinarstvo',8), (628,2501,'Tepisi i staze',8), (629,4301,'Kupaći',8), (630,3025,'Psihološki roman',8), (631,2777,'Prateći pribor i oprema',8), (632,2289,'Sezonski predmeti',8), (633,1022,'Tito',8), (634,3073,'Putopisi',8), (635,38,'Zvučne kartice',8), (636,1086,'Ostalo',8), (637,361,'Umetnost',8), (638,3209,'Ostalo',8), (639,1433,'Čarape',8), (640,1961,'Kardigani',8), (641,2565,'Roštilji',8), (642,2821,'Zimski sportovi',8), (643,2845,'Rezervni delovi',8), (644,2397,'Reflektori',8), (645,872,'Igračke',8), (646,1139,'Baterije i punjači',8), (647,1409,'Prsluci',8), (648,400,'Sport',8), (649,1181,'Helanke',8), (650,3009,'Romantična komedija (Čik lit)',8), (651,4309,'Pidžama',8), (652,2281,'Oprema',8), (653,1014,'Breweriana',8), (654,2869,'Pribor za kancelariju',8), (655,1969,'Kombinezoni',8), (656,3081,'Satirični roman',8), (657,1053,'Ostalo',8), (658,367,'Kuvari i ishrana',8), (659,4313,'Prsluk',8), (660,500,'Trenerke',8), (661,257,'Planinarenje i kampovanje',8), (662,1029,'Ostalo',8), (663,1573,'Ostalo',8), (664,2401,'Dodatna oprema',8), (665,875,'Ostalo',8), (666,624,'Ostalo',8), (667,625,'Ostalo',8), (668,629,'Ostalo',8), (669,630,'Ostalo',8), (670,894,'Ostalo',8), (671,896,'Ostalo',8), (672,2953,'Ostalo',8), (673,404,'Vojna literatura',8), (674,1949,'Bermude',8), (675,2509,'Ostalo',8), (676,982,'Ostalo',8), (677,986,'Ostalo',8), (678,4317,'Rolka',8), (679,999,'Ostalo',8), (680,3053,'Tinejdž roman',8), (681,1945,'Šortsevi',8), (682,4321,'Sako',8), (683,501,'Venčanice',8), (684,375,'Navigacija',8), (685,1425,'Odeća za trudnice',8), (686,414,'Školska oprema',8), (687,4325,'Skafander',8), (688,2805,'Tenis, stoni tenis i badminton',8), (689,3041,'Triler / Misterija',8), (690,3089,'Ostalo',8), (691,622,'Ostalo',8), (692,663,'Ostalo',8), (693,668,'Ostalo',8), (694,2785,'Lopte',8), (695,2829,'Šah',8), (696,2195,'Psi i mačke',8), (697,1429,'Čarape',8), (698,4341,'Tunika',8), (699,4345,'Zeke',8), (700,2825,'Ostali sportovi',8), (701,4349,'Ostala odeća',8), (702,476,'Šorts',8), (703,1144,'Košulja',8), (704,475,'Haljina',8), (705,2557,'Ostalo',8), (706,470,'Džemper',8), (707,468,'Donji veš',8), (708,469,'Duks',8), (709,474,'Majica',8), (710,473,'Trenerka',8), (711,471,'Jakna',8), (712,503,'Suknja',8), (713,2537,'Ostalo',8), (714,2293,'Ostalo',8), (487,14,'Pokloni',28)) 
	    	redovi(export_grupe_id, grupa_id, naziv, export_id) 
	        WHERE export_grupe_id NOT IN (SELECT export_grupe_id FROM export_grupe)");
		DB::statement("SELECT setval('export_grupe_export_grupe_id_seq', (SELECT MAX(export_grupe_id) + 1 FROM export_grupe), FALSE)");

	    DB::statement("INSERT INTO validator (validator_id,kod,aktivan) SELECT * FROM (VALUES 
	    	(8,'captcha',1) ) redovi(validator_id,kod,aktivan) WHERE validator_id NOT IN (SELECT validator_id FROM validator)");
	    DB::statement("INSERT INTO validator_jezik (validator_id,jezik_id,poruka) SELECT * FROM (VALUES 
	    	(8,1,'Dokažite da niste robot!')) redovi(validator_id,jezik_id,poruka) WHERE poruka NOT IN (SELECT poruka FROM validator_jezik)");

		DB::statement("DROP TABLE IF EXISTS footer_data");
		DB::statement("DROP TABLE IF EXISTS footer_data_type");
		DB::statement("DROP TABLE IF EXISTS footer_data_stranice");

	    // footer section type
		DB::statement("SELECT addtable('public','futer_sekcija_tip','futer_sekcija_tip_id bigserial NOT NULL,naziv character varying(255),aktivan smallint DEFAULT 1, CONSTRAINT futer_sekcija_tip_pkey PRIMARY KEY (futer_sekcija_tip_id),CONSTRAINT futer_sekcija_tip_unique UNIQUE (naziv)')");
		// footer section
	    DB::statement("SELECT addtable('public','futer_sekcija','futer_sekcija_id bigserial NOT NULL,slika character varying(255),link character varying(255),rbr smallint DEFAULT 0,aktivan smallint DEFAULT 1,futer_sekcija_tip_id integer NOT NULL, CONSTRAINT futer_sekcija_pkey PRIMARY KEY (futer_sekcija_id), CONSTRAINT futer_sekcija_tip_id_fkey FOREIGN KEY (futer_sekcija_tip_id) REFERENCES public.futer_sekcija_tip (futer_sekcija_tip_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
	    DB::statement("SELECT addtable('public','futer_sekcija_jezik','futer_sekcija_id integer NOT NULL,jezik_id integer NOT NULL,naslov character varying(255),sadrzaj text, CONSTRAINT futer_sekcija_jezik_pkey PRIMARY KEY (futer_sekcija_id, jezik_id), CONSTRAINT futer_sekcija_id_fkey FOREIGN KEY (futer_sekcija_id) REFERENCES public.futer_sekcija (futer_sekcija_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
		DB::statement("SELECT addtable('public','futer_sekcija_strana','futer_sekcija_id integer DEFAULT NULL,web_b2c_seo_id integer DEFAULT NULL, CONSTRAINT futer_sekcija_id_fkey FOREIGN KEY (futer_sekcija_id) REFERENCES public.futer_sekcija (futer_sekcija_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT web_b2c_seo_id_fkey FOREIGN KEY (web_b2c_seo_id) REFERENCES public.web_b2c_seo (web_b2c_seo_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

		DB::statement("DELETE FROM ac_group_module agm where (select sifra from ac_module where ac_module_id = agm.ac_module_id) not in ('NARUDZBINE', 'NARUDZBINE_PREGLED', 'NARUDZBINE_AZURIRANJE', 'ARTIKLI', 'ARTIKLI_PREGLED', 'ARTIKLI_AZURIRANJE', 'KUPCI_I_PARTNERI', 'KUPCI_I_PARTNERI_PREGLED', 'KUPCI_I_PARTNERI_AZURIRANJE', 'ANALITIKA', 'STRANICE', 'STRANICE_PREGLED', 'STRANICE_AZURIRANJE', 'FUTER', 'FUTER_PREGLED', 'FUTER_AZURIRANJE', 'BANERI_I_SLAJDER', 'BANERI_I_SLAJDER_PREGLED', 'BANERI_I_SLAJDER_AZURIRANJE', 'VESTI', 'VESTI_PREGLED', 'VESTI_AZURIRANJE', 'KOMENTARI', 'KOMENTARI_PREGLED', 'KOMENTARI_AZURIRANJE', 'KONTAKT_PODACI', 'KONTAKT_PODACI_PREGLED', 'KONTAKT_PODACI_AZURIRANJE', 'WEB_IMPORT', 'WEB_IMPORT_PREGLED', 'WEB_IMPORT_AZURIRANJE', 'KORISNICI', 'SIFARNICI', 'SIFARNICI_PREGLED', 'SIFARNICI_AZURIRANJE', 'GARANCIJE', 'GARANCIJE_PREGLED', 'GARANCIJE_AZURIRANJE', 'RMA', 'RMA_PREGLED', 'RMA_AZURIRANJE', 'PODESAVANJA', 'PODESAVANJA_PREGLED', 'PODESAVANJA_AZURIRANJE', 'B2B_NARUDZBINE', 'B2B_NARUDZBINE_PREGLED', 'B2B_NARUDZBINE_AZURIRANJE', 'B2B_PARTNERI', 'B2B_PARTNERI_PREGLED', 'B2B_PARTNERI_AZURIRANJE', 'B2B_ANALITIKA', 'B2B_STRANICE', 'B2B_STRANICE_PREGLED', 'B2B_STRANICE_AZURIRANJE', 'B2B_BANERI_I_SLAJDER', 'B2B_BANERI_I_SLAJDER_PREGLED', 'B2B_BANERI_I_SLAJDER_AZURIRANJE', 'B2B_VESTI', 'B2B_VESTI_PREGLED', 'B2B_VESTI_AZURIRANJE', 'B2B_RABAT', 'B2B_RABAT_PREGLED', 'B2B_RABAT_AZURIRANJE', 'B2B_PODESAVANJA', 'B2B_PODESAVANJA_PREGLED', 'B2B_PODESAVANJA_AZURIRANJE')");

		DB::statement("DELETE FROM ac_module WHERE sifra NOT in ('NARUDZBINE', 'NARUDZBINE_PREGLED', 'NARUDZBINE_AZURIRANJE', 'ARTIKLI', 'ARTIKLI_PREGLED', 'ARTIKLI_AZURIRANJE', 'KUPCI_I_PARTNERI', 'KUPCI_I_PARTNERI_PREGLED', 'KUPCI_I_PARTNERI_AZURIRANJE', 'ANALITIKA', 'STRANICE', 'STRANICE_PREGLED', 'STRANICE_AZURIRANJE', 'FUTER', 'FUTER_PREGLED', 'FUTER_AZURIRANJE', 'BANERI_I_SLAJDER', 'BANERI_I_SLAJDER_PREGLED', 'BANERI_I_SLAJDER_AZURIRANJE', 'VESTI', 'VESTI_PREGLED', 'VESTI_AZURIRANJE', 'KOMENTARI', 'KOMENTARI_PREGLED', 'KOMENTARI_AZURIRANJE', 'KONTAKT_PODACI', 'KONTAKT_PODACI_PREGLED', 'KONTAKT_PODACI_AZURIRANJE', 'WEB_IMPORT', 'WEB_IMPORT_PREGLED', 'WEB_IMPORT_AZURIRANJE', 'KORISNICI', 'SIFARNICI', 'SIFARNICI_PREGLED', 'SIFARNICI_AZURIRANJE', 'GARANCIJE', 'GARANCIJE_PREGLED', 'GARANCIJE_AZURIRANJE', 'RMA', 'RMA_PREGLED', 'RMA_AZURIRANJE', 'PODESAVANJA', 'PODESAVANJA_PREGLED', 'PODESAVANJA_AZURIRANJE', 'B2B_NARUDZBINE', 'B2B_NARUDZBINE_PREGLED', 'B2B_NARUDZBINE_AZURIRANJE', 'B2B_PARTNERI', 'B2B_PARTNERI_PREGLED', 'B2B_PARTNERI_AZURIRANJE', 'B2B_ANALITIKA', 'B2B_STRANICE', 'B2B_STRANICE_PREGLED', 'B2B_STRANICE_AZURIRANJE', 'B2B_BANERI_I_SLAJDER', 'B2B_BANERI_I_SLAJDER_PREGLED', 'B2B_BANERI_I_SLAJDER_AZURIRANJE', 'B2B_VESTI', 'B2B_VESTI_PREGLED', 'B2B_VESTI_AZURIRANJE', 'B2B_RABAT', 'B2B_RABAT_PREGLED', 'B2B_RABAT_AZURIRANJE', 'B2B_PODESAVANJA', 'B2B_PODESAVANJA_PREGLED', 'B2B_PODESAVANJA_AZURIRANJE')");


		//dodavanje nivoa modula
		DB::statement("INSERT INTO ac_module (ac_module_id, aplikacija_id, modul, sifra, opis, tip, rbr, parrent_ac_module_id, aktivan) SELECT * FROM (VALUES
			(1, 100, 'fromNarudzbine', 'NARUDZBINE', 'NARUDŽBINE', 0, 0, 1, 1),
			(2, 100, 'fromNarudzbine', 'NARUDZBINE_PREGLED', 'Narudžbine pregled', 1, 1, 1, 1),
			(3, 100, 'fromNarudzbine', 'NARUDZBINE_AZURIRANJE', 'Narudžbine ažuriranje', 1, 2, 1, 1),
			(4, 100, 'fromArtikli', 'ARTIKLI', 'ARTIKLI', 0, 0, 4, 1),
			(5, 100, 'fromArtikli', 'ARTIKLI_PREGLED', 'Artikli pregled', 1, 1, 4, 1),
			(6, 100, 'fromArtikli', 'ARTIKLI_AZURIRANJE', 'Artikli ažuriranje', 1, 2, 4, 1),
			(7, 100, 'fromKupciPartneri', 'KUPCI_I_PARTNERI', 'KUPCI I PARTNERI', 0, 0, 7, 1),
			(8, 100, 'fromKupciPartneri', 'KUPCI_I_PARTNERI_PREGLED', 'Kupci i partneri pregled', 1, 1, 7, 1),
			(9, 100, 'fromKupciPartneri', 'KUPCI_I_PARTNERI_AZURIRANJE', 'Kupci i partneri ažuriranje', 1, 2, 7, 1),
			(10, 100, 'fromAnalitika', 'ANALITIKA', 'ANALITIKA', 0, 0, 10, 1),
			(11, 100, 'fromStranice', 'STRANICE', 'STRANICE', 0, 0, 11, 1),
			(12, 100, 'fromStranice', 'STRANICE_PREGLED', 'Stranice pregled', 1, 1, 11, 1),
			(13, 100, 'fromStranice', 'STRANICE_AZURIRANJE', 'Stranice ažuriranje', 1, 2, 11, 1),
			(14, 100, 'fromFuter', 'FUTER', 'FUTER', 0, 0, 14, 1),
			(15, 100, 'fromFuter', 'FUTER_PREGLED', 'Futer_pregled', 1, 1, 14, 1),
			(16, 100, 'fromFuter', 'FUTER_AZURIRANJE', 'Futer_azuriranje', 1, 2, 14, 1),
			(17, 100, 'fromBaneriSlajder', 'BANERI_I_SLAJDER', 'BANERI I SLAJDER', 0, 0, 17, 1),
			(18, 100, 'fromBaneriSlajder', 'BANERI_I_SLAJDER_PREGLED', 'Baneri i slajder pregled', 1, 1, 17, 1),
			(19, 100, 'fromBaneriSlajder', 'BANERI_I_SLAJDER_AZURIRANJE', 'Baneri i slajder ažuriranje', 1, 2, 17, 1),
			(20, 100, 'fromVesti', 'VESTI', 'VESTI', 0, 0, 20, 1),
			(21, 100, 'fromVesti', 'VESTI_PREGLED', 'Vesti_pregled', 1, 1, 20, 1),
			(22, 100, 'fromVesti', 'VESTI_AZURIRANJE', 'Vesti_azuriranje', 1, 2, 20, 1),
			(23, 100, 'fromKomentari', 'KOMENTARI', 'KOMENTARI', 0, 0, 23, 1),
			(24, 100, 'fromKomentari', 'KOMENTARI_PREGLED', 'Komentari_pregled', 1, 1, 23, 1),
			(25, 100, 'fromKomentari', 'KOMENTARI_AZURIRANJE', 'Komentari_azuriranje', 1, 2, 23, 1),
			(26, 100, 'fromKontaktPodaci', 'KONTAKT_PODACI', 'KONTAKT PODACI', 0, 0, 26, 1),
			(27, 100, 'fromKontaktPodaci', 'KONTAKT_PODACI_PREGLED', 'Kontakt podaci pregled', 1, 1, 26, 1),
			(28, 100, 'fromKontaktPodaci', 'KONTAKT_PODACI_AZURIRANJE', 'Kontakt podaci ažuriranje', 1, 2, 26, 1),
			(29, 100, 'fromWebImport', 'WEB_IMPORT', 'WEB IMPORT', 0, 0, 29, 1),
			(30, 100, 'fromWebImport', 'WEB_IMPORT_PREGLED', 'Web import pregled', 1, 1, 29, 1),
			(31, 100, 'fromWebImport', 'WEB_IMPORT_AZURIRANJE', 'Web import ažuriranje', 1, 2, 29, 1),
			(32, 100, 'fromKorisnici', 'KORISNICI', 'KORISNICI', 0, 0, 32, 1),
			(33, 100, 'fromSifarnici', 'SIFARNICI', 'ŠIFARNICI', 0, 0, 33, 1),
			(34, 100, 'fromSifarnici', 'SIFARNICI_PREGLED', 'Šifarnici pregled', 1, 1, 33, 1),
			(35, 100, 'fromSifarnici', 'SIFARNICI_AZURIRANJE', 'Šifarnici ažuriranje', 1, 2, 33, 1),
			(36, 100, 'fromGarancije', 'GARANCIJE', 'GARANCIJE', 0, 0, 36, 1),
			(37, 100, 'fromGarancije', 'GARANCIJE_PREGLED', 'Garancije pregled', 1, 1, 36, 1),
			(38, 100, 'fromGarancije', 'GARANCIJE_AZURIRANJE', 'Garancije ažuriranje', 1, 2, 36, 1),
			(39, 100, 'fromRMA', 'RMA', 'RMA', 0, 0, 39, 1),
			(40, 100, 'fromRMA', 'RMA_PREGLED', 'RMA_pregled', 1, 1, 39, 1),
			(41, 100, 'fromRMA', 'RMA_AZURIRANJE', 'RMA_azuriranje', 1, 2, 39, 1),
			(42, 100, 'fromPodesavanja', 'PODESAVANJA', 'PODEŠAVANJA', 0, 0, 42, 1),
			(43, 100, 'fromPodesavanja', 'PODESAVANJA_PREGLED', 'Podešavanja pregled', 1, 1, 42, 0),
			(44, 100, 'fromPodesavanja', 'PODESAVANJA_AZURIRANJE', 'Podešavanja ažuriranje', 1, 2, 42, 0),
			(45, 100, 'fromB2BNarudzbine', 'B2B_NARUDZBINE', 'B2B NARUDŽBINE', 0, 0, 45, 1),
			(46, 100, 'fromB2BNarudzbine', 'B2B_NARUDZBINE_PREGLED', 'B2B Narudžbine pregled', 1, 1, 45, 1),
			(47, 100, 'fromB2BNarudzbine', 'B2B_NARUDZBINE_AZURIRANJE', 'B2B Narudžbine ažuriranje', 1, 2, 45, 1),
			(48, 100, 'fromB2BPartneri', 'B2B_PARTNERI', 'B2B PARTNERI', 0, 0, 48, 1),
			(49, 100, 'fromB2BPartneri', 'B2B_PARTNERI_PREGLED', 'B2B Partneri pregled', 1, 1, 48, 1),
			(50, 100, 'fromB2BPartneri', 'B2B_PARTNERI_AZURIRANJE', 'B2B Partneri ažuriranje', 1, 2, 48, 1),
			(51, 100, 'fromB2BAnalitika', 'B2B_ANALITIKA', 'B2B ANALITIKA', 0, 0, 51, 1),
			(52, 100, 'fromB2BStranice', 'B2B_STRANICE', 'B2B STRANICE', 0, 0, 52, 1),
			(53, 100, 'fromB2BStranice', 'B2B_STRANICE_PREGLED', 'B2B Stranice pregled', 1, 1, 52, 1),
			(54, 100, 'fromB2BStranice', 'B2B_STRANICE_AZURIRANJE', 'B2B Stranice ažuriranje', 1, 2, 52, 1),
			(55, 100, 'fromB2BBaneriSlajder', 'B2B_BANERI_I_SLAJDER', 'B2B BANERI I SLAJDER', 0, 0, 55, 1),
			(56, 100, 'fromB2BBaneriSlajder', 'B2B_BANERI_I_SLAJDER_PREGLED', 'B2B Baneri i slajder pregled', 1, 1, 55, 1),
			(57, 100, 'fromB2BBaneriSlajder', 'B2B_BANERI_I_SLAJDER_AZURIRANJE', 'B2B Baneri i slajder ažuriranje', 1, 2, 55, 1),
			(58, 100, 'fromB2BVesti', 'B2B_VESTI', 'B2B VESTI', 0, 0, 58, 1),
			(59, 100, 'fromB2BVesti', 'B2B_VESTI_PREGLED', 'B2B Vesti pregled', 1, 1, 58, 1),
			(60, 100, 'fromB2BVesti', 'B2B_VESTI_AZURIRANJE', 'B2B Vesti ažuriranje', 1, 2, 58, 1),
			(61, 100, 'fromB2BRabat', 'B2B_RABAT', 'B2B RABAT', 0, 0, 61, 1),
			(62, 100, 'fromB2BRabat', 'B2B_RABAT_PREGLED', 'B2B Rabat pregled', 1, 1, 61, 1),
			(63, 100, 'fromB2BRabat', 'B2B_RABAT_AZURIRANJE', 'B2B Rabat ažuriranje', 1, 2, 61, 1),
			(64, 100, 'fromB2BPodesavanja', 'B2B_PODESAVANJA', 'B2B PODEŠAVANJA', 0, 0, 64, 1),
			(65, 100, 'fromB2BPodesavanja', 'B2B_PODESAVANJA_PREGLED', 'B2B Podešavanja pregled', 1, 1, 64, 0),
			(66, 100, 'fromB2BPodesavanja', 'B2B_PODESAVANJA_AZURIRANJE', 'B2B Podešavanja ažuriranje', 1, 2, 64, 0),
			(67, 100, 'fromDOKUMENTI', 'DOKUMENTI', 'DOKUMENTI', 0, 0, 67, 1),
			(68, 100, 'fromDOKUMENTI', 'DOKUMENTI_PREGLED', 'Dokumenti pregled', 1, 1, 67, 1),
			(69, 100, 'fromDOKUMENTI', 'DOKUMENTI_AZURIRANJE', 'Dokumenti azuriranje', 1, 2, 67, 1),
			(70, 100, 'fromANKETE', 'ANKETE', 'ANKETE', 0, 0, 70, 1),
			(71, 100, 'fromANKETE', 'ANKETE_PREGLED', 'Ankete pregled', 1, 1, 70, 1),
			(72, 100, 'fromANKETE', 'ANKETE_AZURIRANJE', 'Ankete azuriranje', 1, 2, 70, 1),
			(73, 100, 'fromCRM', 'CRM', 'CRM pregled', 0, 0, 73, 1)
			)
			redovi(ac_module_id, aplikacija_id, modul, sifra, opis, tip, rbr, parrent_ac_module_id, aktivan)	
			WHERE ac_module_id NOT IN (SELECT ac_module_id FROM ac_module)");

		DB::statement("INSERT INTO ac_group_module (ac_group_id, ac_module_id, alow, alow_tico) SELECT * FROM (VALUES
			(32,1,1,1),(32,2,1,1),(32,3,1,1),(32,4,1,1),(32,5,1,1),(32,6,1,1),(32,7,1,1),(32,8,1,1),(32,9,1,1),(32,10,1,1),(32,11,0,1),(32,12,0,1),(32,13,0,1),(32,29,1,1),(32,30,1,1),(32,31,1,1),(32,32,0,1),(32,33,1,1),(32,34,1,1),(32,35,1,1),(32,36,1,1),(32,37,1,1),(32,38,1,1),(32,39,1,1),(32,40,1,1),(32,41,1,1),(32,45,1,1),(32,46,1,1),(32,47,1,1),(32,48,1,1),(32,49,1,1),(32,50,1,1),(32,51,0,1),(32,52,0,1),(32,53,0,1),(32,54,0,1),(32,14,0,1),(32,15,0,1),(32,16,0,1),(32,17,0,1),(32,18,0,1),(32,19,0,1),(32,20,0,1),(32,21,0,1),(32,22,0,1),(32,23,0,1),(32,24,0,1),(32,25,0,1),(32,26,0,1),(32,27,0,1),(32,28,0,1),(32,42,0,1),(32,55,0,1),(32,56,0,1),(32,57,0,1),(32,58,0,1),(32,73,0,1),(0,26,1,1),(0,27,1,1),(0,28,1,1),(0,29,1,1),(0,30,1,1),(0,31,1,1),(32,59,0,1),(32,60,0,1),(32,61,0,1),(32,62,0,1),(32,63,0,1),(32,64,0,1),(0,32,1,1),(0,33,1,1),(0,34,1,1),(0,35,1,1),(0,36,1,1),(0,37,1,1),(0,38,1,1),(0,39,1,1),(0,40,1,1),(0,41,1,1),(0,42,1,1),(0,45,1,1),(0,46,1,1),(0,47,1,1),(0,48,1,1),(0,49,1,1),(0,50,1,1),(0,51,1,1),(0,52,1,1),(0,53,1,1),(0,1,1,1),(0,2,1,1),(0,3,1,1),(0,4,1,1),(0,5,1,1),(0,6,1,1),(0,7,1,1),(0,8,1,1),(0,9,1,1),(0,10,1,1),(0,11,1,1),(0,12,1,1),(0,13,1,1),(0,14,1,1),(0,15,1,1),(0,16,1,1),(0,17,1,1),(0,18,1,1),(0,19,1,1),(0,20,1,1),(0,21,1,1),(0,22,1,1),(0,23,1,1),(0,24,1,1),(0,25,1,1),(0,54,1,1),(0,55,1,1),(0,56,1,1),(0,57,1,1),(0,58,1,1),(0,59,1,1),(0,60,1,1),(0,61,1,1),(0,62,1,1),(0,63,1,1),(0,64,1,1),(0,65,1,1),(0,66,1,1),(0,67,1,1),(0,68,1,1),(0,69,1,1),(0,70,1,1),(0,71,1,1),(0,72,1,1),(0,73,1,1),(32,67,1,1),(32,68,1,1),(32,69,1,1)
			)
			redovi(ac_group_id, ac_module_id, alow, alow_tico)	
			WHERE (ac_group_id, ac_module_id) NOT IN (SELECT ac_group_id, ac_module_id FROM ac_group_module)");

		DB::statement("DELETE FROM futer_sekcija_tip WHERE naziv = 'drustvene_mreze'");
		DB::statement("INSERT INTO futer_sekcija_tip (naziv) SELECT * FROM (VALUES 
	    	('slika'),('text'),('linkovi'),('kontakt'),('mapa'),('drustvene_mreze') ) redovi(naziv) WHERE naziv NOT IN (SELECT naziv FROM futer_sekcija_tip)");
	    DB::statement("INSERT INTO futer_sekcija (futer_sekcija_id,futer_sekcija_tip_id,rbr) SELECT * FROM (VALUES 
	    	(1,1,1),
	    	(2,3,2),
	    	(3,3,3),
	    	(4,4,4)
	    ) redovi(futer_sekcija_id,futer_sekcija_tip_id,rbr) WHERE futer_sekcija_id NOT IN (SELECT futer_sekcija_id FROM futer_sekcija)");
	    DB::statement("INSERT INTO futer_sekcija_jezik (futer_sekcija_id,jezik_id,naslov,sadrzaj) SELECT * FROM (VALUES 
	    	(1,1,'Logo','U našoj prodavnici imamo široku ponudu artikala.'),
	    	(2,1,'Brzi linkovi',NULL),
	    	(3,1,'Informacije',NULL),
	    	(4,1,'Kontaktirajte nas',NULL)
	    ) redovi(futer_sekcija_id,jezik_id,naslov,sadrzaj) WHERE NOT EXISTS (SELECT * FROM futer_sekcija_jezik)");
	    DB::statement("SELECT setval('futer_sekcija_futer_sekcija_id_seq', (SELECT MAX(futer_sekcija_id) + 1 FROM futer_sekcija), FALSE)");
	    // DB::statement("INSERT INTO futer_sekcija_strana (futer_sekcija_id,web_b2c_seo_id) SELECT * FROM (VALUES 
	    // 	(2,25),(2,27),(3,36) ) redovi(futer_sekcija_id,web_b2c_seo_id) WHERE NOT EXISTS (SELECT * FROM futer_sekcija_strana)");

		DB::statement("SELECT addtable('public','web_b2c_newslatter_description','web_b2c_newslatter_description_id integer NOT NULL, jezik_id integer, naslov character varying(255), sadrzaj text, CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
		DB::statement("INSERT INTO web_b2c_newslatter_description (web_b2c_newslatter_description_id,jezik_id,naslov,sadrzaj) SELECT * FROM (VALUES 
	    	(1,1,'Želite li da Vas obavestimo o novim kolekcijama?','Za najnovije informacije o našim proizvodima i kolekcijama prijavite se na našu e-mail listu.')
	    ) redovi(web_b2c_newslatter_description_id,jezik_id,naslov,sadrzaj) WHERE NOT EXISTS (SELECT * FROM web_b2c_newslatter_description)");

		//stari linkovi
		DB::statement("SELECT addtable('public','stari_linkovi','sifra character varying(255), link character varying(500)')");
		DB::statement("SELECT addcol('public','stari_linkovi','id','integer DEFAULT NULL')");
		DB::statement("SELECT addcol('public','stari_linkovi','kategorija','smallint DEFAULT 0')");
		DB::statement("SELECT addcol('public','stari_linkovi','naziv','character varying(255)')");
		//front admin labels
		DB::statement("SELECT addtable('public','front_admin_labele','front_admin_labele_id bigserial NOT NULL, labela character varying(500), CONSTRAINT front_admin_labele_pkey PRIMARY KEY (front_admin_labele_id)')");
	    DB::statement("SELECT addtable('public','front_admin_labele_jezik','front_admin_labele_id integer NOT NULL,jezik_id integer NOT NULL,sadrzaj text, CONSTRAINT front_admin_labele_jezik_pkey PRIMARY KEY (front_admin_labele_id, jezik_id), CONSTRAINT front_admin_labele_id_fkey FOREIGN KEY (front_admin_labele_id) REFERENCES public.front_admin_labele (front_admin_labele_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

		DB::statement("INSERT INTO front_admin_labele (front_admin_labele_id,labela) SELECT * FROM (VALUES 
	    	(1,'Najpopularniji proizvodi'),
	    	(2,'Najprodavaniji proizvodi'),
	    	(3,'Najnoviji proizvodi'),
	    	(4,'Text footer-a'),
	    	(5,'Prezentacija opis')
	    ) redovi(front_admin_labele_id,labela) WHERE front_admin_labele_id NOT IN (SELECT front_admin_labele_id FROM front_admin_labele)");
		DB::statement("SELECT setval('front_admin_labele_front_admin_labele_id_seq', (SELECT MAX(front_admin_labele_id) + 1 FROM front_admin_labele), FALSE)");
		DB::statement("INSERT INTO front_admin_labele_jezik (front_admin_labele_id,jezik_id,sadrzaj) SELECT * FROM (VALUES 
	    	(4,1,'<p>Cene su informativnog karaktera. Prodavac ne odgovara za tačnost cena iskazanih na sajtu, zadržava pravo izmena cena. Ponudu za ostale artikle, informacije o stanju lagera i aktuelnim cenama možete dobiti na upit. Plaćanje se isključivo vrši virmanski - bezgotovinski.</p>'),
	    	(5,1,'<p>Prezentacija opis.</p>')
	    ) redovi(front_admin_labele_id,jezik_id,sadrzaj) WHERE (front_admin_labele_id, jezik_id) NOT IN (SELECT front_admin_labele_id, jezik_id FROM front_admin_labele_jezik)");

		//kurs
		DB::statement("SELECT addcol('public','kursna_lista','web','numeric(20,2) DEFAULT 0')");

		//roba_partner_kategorija
		DB::statement("SELECT addtable('public','roba_partner_kategorija','roba_id integer, id_kategorije integer, cena numeric(20,2) DEFAULT 0')");

		// web import kolone
		DB::statement("SELECT addtable('public','web_import_kolone','kolona_sifra character varying(255),naziv character varying(255),width integer DEFAULT 35,css_class text, CONSTRAINT web_import_kolone_pkey PRIMARY KEY (kolona_sifra)')");
		DB::statement("INSERT INTO web_import_kolone (kolona_sifra,naziv,width,css_class) SELECT * FROM (VALUES 
	    	('naziv','naziv',330,NULL)
	    ) redovi(kolona_sifra,naziv,width,css_class) WHERE kolona_sifra NOT IN (SELECT kolona_sifra FROM web_import_kolone)");

		// admin artikli kolone
		DB::statement("SELECT addtable('public','admin_artikli_kolone','kolona_sifra character varying(255),naziv character varying(255),width integer DEFAULT 35,css_class text, CONSTRAINT admin_artikli_kolone_pkey PRIMARY KEY (kolona_sifra)')");
		DB::statement("INSERT INTO admin_artikli_kolone (kolona_sifra,naziv,width,css_class) SELECT * FROM (VALUES 
	    	('naziv','naziv',330,NULL)
	    ) redovi(kolona_sifra,naziv,width,css_class) WHERE kolona_sifra NOT IN (SELECT kolona_sifra FROM admin_artikli_kolone)");

		//
		DB::statement("SELECT addcol('public','web_b2c_seo_jezik','naziv','character varying(255)')");	
		DB::statement("SELECT addcol('public','web_b2c_seo_jezik','slug','character varying(255)')");

		DB::statement("SELECT addcol('public','web_b2b_seo_jezik','naziv','character varying(255)')");	
		DB::statement("SELECT addcol('public','web_b2b_seo_jezik','slug','character varying(255)')");

		DB::statement("SELECT addcol('public','prevodilac','is_js','smallint DEFAULT 0')");
		DB::statement("SELECT addcol('public','jezik','putanja_slika','character varying(255)')");

		DB::statement("UPDATE web_b2c_seo SET naziv_stranice = 'pravno-lice' WHERE naziv_stranice = 'pravno_lice'");
		DB::statement("UPDATE web_b2c_seo SET naziv_stranice = 'fizicko-lice' WHERE naziv_stranice = 'fizicko_lice'");

		DB::statement("ALTER TABLE web_vest_b2c_jezik ALTER COLUMN naslov TYPE character varying(255)");
		DB::statement("ALTER TABLE web_vest_b2c_jezik ALTER COLUMN title TYPE character varying(255)");

/****************************************************************************************************************************/
		

	    DB::statement("INSERT INTO log_akcija (log_akcija_id,sifra,naziv) SELECT * FROM (VALUES 
	    	(1,'LOGOVANJE','Logovanje'),
	    	(2,'LOGOVANJE_OUT','Log out'),
	    	(3,'STRANICA_OBRISI','BRISANJE stranice'),
	    	(4,'STRANICA_DODAJ','DODAVANJE stranice'),
	    	(5,'STRANICA_IZMENI','IZMENA stranice'),
	    	(6,'SLIKA_POSTAVI','Stranice - Postavljanje slike'),
	    	(7,'SLIKA_OPIS_POSTAVI','Artikli - Postavljanje slike u opis'),
	    	(8,'EPONUDA_POSTAVI','Postavljanje elektronske ponude'),
	    	(9,'SHOPMANIA_POSTAVI','Postavljanje shopmanie'),
	    	(10,'SWIFT_MAILER_KONFIGURISI','Konfigurisanje SWIFT mailer-a'),
	    	(11,'PRODAVNICA_PODESAVANJA_PRIKAZ_ARTIKALA_DA','Podešavanja prikaza artikala - da'),
	    	(12,'PRODAVNICA_PODESAVANJA_PRIKAZ_ARTIKALA_NE','Podešavanja prikaza artikala - ne'),
	    	(13,'PRODAVNICA_PODESAVANJA_SWIFT_MAILER_AKT','Podešavanja swift mejlera - aktivan'),
	    	(14,'PRODAVNICA_PODESAVANJA_SWIFT_MAILER_NEAKT','Podešavanja swift mejlera - neaktivan'),
	    	(15,'PODESAVANJA_TEME','Podešavanja teme'),
	    	(16,'TEMA_ZAKLJUCAJ','Zaključavanje teme'),
	    	(17,'PODESAVANJA_PRODAVNICE_PORTAL_AKTIVAN_B2C','IZMENA portala - aktivan b2c'),
	    	(18,'PODESAVANJA_PRODAVNICE_PORTAL_AKTIVAN_B2B','IZMENA portala - aktivan b2b'),
	    	(19,'PODESAVANJA_PRODAVNICE_PORTAL_AKTIVAN_OBA','IZMENA portala - aktivni i b2b i b2c'),
	    	(20,'PODESAVANJA_PRODAVNICE_NACIN_PLACANJA','Podešavanja načina plaćanja'),
	    	(21,'PODESAVANJA_PRODAVNICE_NACIN_ISPORUKE','Podešavanja načina isporuke'),
	    	(22,'PODESAVANJA_PRODAVNICE_MAGACIN','Omogućavanje magacina'),
	    	(23,'PODESAVANJA_PRODAVNICE_NE_MAGACIN','Onemogućavanje magacina'),
	    	(24,'PODESAVANJA_PRODAVNICE_MAGACIN_PRIMARNI','Podešavanja magacina - primarni'),
	    	(25,'PODESAVANJA_PRODAVNICE_VRSTA_CENE','Podešavanja prodavnice - vrsta cene - valuta'),
	    	(26,'PODESAVANJA_PRODAVNICE_VRSTA_CENE_OMOGUCI','Podešavanja prodavnice - vrsta cene omogući'),
	    	(27,'PODESAVANJA_PRODAVNICE_VRSTA_CENE_ONEMOGUCI','Podešavanja prodavnice - vrsta cene onemogući'),
	    	(28,'PODESAVANJA_PRODAVNICE_EKSPORT_OMOGUCI','Omogućavanje eksporta'),
	    	(29,'PODESAVANJA_PRODAVNICE_EKSPORT_ONEMOGUCI','Onemogućavanje eksporta'),
	    	(30,'PODESAVANJA_PRODAVNICE_A_IMPORT_NEAKTIVAN','Automatski import neaktivan'),
	    	(31,'PODESAVANJA_PRODAVNICE_A_IMPORT_AKTIVAN','Automatski import aktivan'),
	    	(32,'PODESAVANJA_PRODAVNICE_JEZIK_DA','Podešavanja jezika omogući'),
	    	(33,'PODESAVANJA_PRODAVNICE_JEZIK_NE','Podešavanja jezika onemogući'),
	    	(34,'PODESAVANJA_PRODAVNICE_JEZIK_PRIMARNI','Podešavanja jezika - primarni'),
	    	(35,'PODESAVANJA_PRODAVNICE_GOOGLE_ANALITIKA','Podešavanja google analitike'),
	    	(36,'PODESAVANJA_PRODAVNICE_FACEBOOK_PIXEL','Podešavanja facebook piksela'),
	    	(37,'PODESAVANJA_PRODAVNICE_CET','Podešavanja boje prodavnice'),
	    	(38,'PODESAVANJA_PRODAVNICE_BOJE','Logovanje'),
	    	(39,'PODESAVANJA_INTESA_KLJUCEVI','Podešavanja ključeva za intesu'),
	    	(40,'SITEMAP_GENERISI','Generisanje Sitemapa'),
	    	(41,'ARTIKLI_BACKUP','Backup artikala'),
	    	(42,'ARTIKLI_SLIKE_BACKUP','Backup slika artikala'),
	    	(43,'KARAKTERISTIKE_DODAVANJE_GENERISANE','Artikli - DODAVANJE generisanih karakteristika'),
	    	(44,'KARAKTERISTIKA_DODAVANJE','Artikli - Dodavanje generisanih karakteristika'),
	    	(45,'ARTIKLI_G_KARAK_VREDNOST_DODAJ','Artikli - DODAVANJE vrednosti generisanoj karakteristici'),
	    	(46,'G_KARAKTERISTIKE_IZMENA','IZMENA generisanih karakteristika'),
	    	(47,'G_KARAKTERISTIKE_OBRISI','Atikli - BRISANJE generisanih karakteristika'),
	    	(48,'DOBAVLJAC_KARAKTERISTIKE_DODAJ','Artikli - DODAVANJE karakteristika dobavljača'),
	    	(49,'DOBAVLJAC_KARAKTERISTIKE_IZMENI','Artikli - IZMENA karakteristika dobavljača'),
	    	(50,'DOBAVLJAC_KARAKTERISTIKE_OBRISI','Artikli - BRISANJE karakteristika dobavljača'),
	    	(51,'ARTIKLI_KARAKTERISTIKA_IZMENI','Artikli - IZMENA karakteristike'),
	    	(52,'ARTIKLI_KARAKTERISTIKA_DODAJ','Artikli - DODAVANJE karakteristike'),
	    	(53,'GRUPA_KARAKTERISTIKA_OBRISI','Šifarnici - BRISANJE karakteristike grupe'),
	    	(54,'VEZANI_ARTIKLI_OBRISI','BRISANJE vezanih artikala'),
	    	(55,'VEZANI_ARTIKLI_FLAG_CENA_IZMENI','IZMENA flaga za cenu kod vezanih artikala'),
	    	(56,'ARTIKLI_DOBAVLJAC_GRUPA_DODAJ','Web import - DODAVANJE grupe'),
	    	(57,'ARTIKLI_DOBAVLJAC_PROIZVODJAC_DODAJ','Web import - DODAVANJE proizvođača'),
	    	(58,'ARTIKLI_DOBAVLJAC_TARIFNA_GRUPA_DODAJ','Web import - DODAVANJE tarifne grupe'),
	    	(59,'ARTIKLI_DOBAVLJAC_KOLICINA','Web import - DODAVANJE količine'),
	    	(60,'ARTIKLI_DOBAVLJAC_MP_MARZA','Web import - DODAVANJE mp marže'),
	    	(61,'ARTIKLI_DOBAVLJAC_WEB_MARZA','Web import - DODAVANJE web marže'),
	    	(62,'DOBAVLJAC_ARTIKLI_UNOS_NOVIH','Web import - Unos novih'),
	    	(63,'WEB_IMPORT_PRIHVATI_LAGER','Web import - Prihvatanje lagera'),
	    	(64,'WEB_IMPORT_PRIHVATI_CENE','Web import - Prihvatanje cena'),
	    	(65,'WEB_IMPORT_PREPISI_CENU','Prepisivanje web cene'),
	    	(66,'WEB_IMPORT_PRERACUNAJ_CENU','Web import - Preračunavanje cene'),
	    	(67,'WEB_IMPORT_SKINI_SA_WEBA','Web import - Skini sa weba'),
	    	(68,'WEB_IMPORT_STAVI_NA_WEB','Web import - Stavi na web'),
	    	(69,'WEB_IMPORT_OTKLJUCAJ','Web import - Otključaj'),
	    	(70,'WEB_IMPORT_ZAKLJUCAJ','Web import - Zaključaj'),
	    	(71,'DOBAVLJAC_ARTIKLI_PRERACUNAJ_CENU_KURS','Preračunavanje cene kursa'),
	    	(72,'WEB_IMPORT_STORNIRAJ_KOLICINA_0','Web import - Storniranje količine na 0'),
	    	(73,'WEB_IMPORT_OBRISI','Web import - BRISANJE artikala'),
	    	(74,'WEB_IMPORT_PREUZMI_KARAKTERISTIKE_SLIKE','Web import - Preuzimanje karakteristika i slika'),
	    	(75,'WEB_IMPORT_POVEZI_ARTIKLE','Web import - Povezivanje artikala'),
	    	(76,'WEB_IMPORT_DODAJ_TEKST','Web import - DODAVANJE teksta'),
	    	(77,'WEB_IMPORT_IZMENI_TEKST','Web import - IZMENA teksta'),
	    	(78,'WEB_IMPORT_RAZVEZI_ARTIKLE','Web import - Razvezivanje artikala'),
	    	(79,'WEB_IMPORT_PREBACI_U_AKTIVNE','Web import - Prebaci u aktivne'),
	    	(80,'WEB_IMPORT_PREBACI_U_NEAKTIVNE','Web import - Prebaci u neaktivne'),
	    	(81,'WEB_IMPORT_ARTIKAL_IZMENI','Web import - IZMENA artikla'),
	    	(82,'WEB_IMPORT_KOLICINA_BRZA_IZMENA','Web import - Brza IZMENA količine'),
	    	(83,'WEB_IMPORT_NAZIV_BRZA_IZMENA','Web import - Brza IZMENA naziva'),
	    	(84,'WEB_IMPORT_SIFRA_BRZA_IZMENA','Web import - Brza IZMENA šifre'),
	    	(85,'PARTNER_VRSTA_IZMENA','IZMENA vrste partnera'),
	    	(86,'prazno','prazno'),
	    	(87,'KONFIGURATOR_GRUPE_DODAJ','DODAVANJE grupe konfiguratora'),
	    	(88,'KONFIGURATOR_GRUPE_IZMENI','IZMENA grupe konfiguratora'),
	    	(89,'KONFIGURATOR_GRUPE_OBRISI','BRISANJE grupe konfiguratora'),
	    	(90,'RTIKLI_NAZIV_BRZA_IZMENA','Artikli - brza IZMENA naziva'),
	    	(91,'ARTIKLI_KOLICINA_BRZA_IZMENA','Artikli - brza IZMENA količine'),
	    	(92,'ARTIKLI_WEB_CENA_BRZA_IZMENA','Artikli - brza IZMENA web cene'),
	    	(93,'ARTIKLI_AKTIVNO','Artikli - Aktivno'),
	    	(94,'ARTIKLI_NEAKTIVNO','Artikli - Neaktivno'),
	    	(95,'ARTIKLI_SKLONI','Artikli - Skloni'),
	    	(96,'ARTIKLI_OTKLJUCAJ','Artikli - Otključaj'),
	    	(97,'ARTIKLI_ZAKLJUCAJ','Artikli - Zaključaj'),
	    	(98,'ARTIKLI_SKLONI_SA_WEBA','Artikli - Skloni sa web-a'),
	    	(99,'ARTIKLI_PRIKAZI_NA_WEBU','Artikli - Prikaži na web-u'),
	    	(100,'ARTIKLI_PRIKAZI_NA_B2B','Artikli - Prikaži na B2B-u'),
	    	(101,'ARTIKLI_SKLONI_SA_B2B','Artikli - Skloni sa B2B-a'),
	    	(102,'ARTIKLI_IMPORT','Import artikala'),
	    	(103,'FRONT_ARTIKLI_STAVI_NA_WEB','Front - Stavljanje artikla na web'),
	    	(104,'FRONT_ARTIKLI_SKINI_SA_WEBA','Front - Sklanjanje artikla sa weba'),
	    	(105,'FRONT_ARTIKLI_STAVI_NA_PRODAJU','Front - Stavljanje artikla na prodaju'),
	    	(106,'FRONT_ARTIKLI_SKINI_SA_PRODAJE','Front - Skidanje artikla sa prodaje'),
	    	(107,'ARTIKLI_IZMENI','IZMENA artikla'),
	    	(108,'ARTIKLI_DODAJ','DODAVANJE artikla'),
	    	(109,'ARTIKLI_OBRISI','BRISANJE artikala'),
	    	(110,'ARTIKLI_DODAJ_SLIKE','Artikli - DODAVANJE slika'),
	    	(111,'ARTIKLI_OBRISI_SLIKE','Artikli - BRISANJE slika'),
	    	(112,'ARTIKLI_OBRISI_SLIKE_GET','Artikli - BRISANJE slike'),
	    	(113,'ARTIKLI_OPIS','Opis artikala'),
	    	(114,'ARTIKLI_KARAKTERISTIKE_IZMENI','IZMENA HTML karakteristika artikala'),
	    	(115,'ARTIKLI_VEZI','Vezivanje artikala'),
	    	(116,'ARTIKLI_EXPORT_SVI','Eksport svih artikala'),
	    	(117,'ARTIKLI_AKCIJA_IZMENI','IZMENA akcije artikla'),
	    	(118,'ARTIKLI_FAJLOVI_DODAJ','DODAVANJE fajla artiklu'),
	    	(119,'ARTIKLI_AKCIJA_POPUST','Popust (akcija) na artiklima'),
	    	(120,'ARTIKLI_OBRISI_FAJLOVI','Artikli - BRISANJE fajla'),
	    	(121,'ARTIKLI_EKSPORT_DODELA','Dodela artikala prema eksportu'),
	    	(122,'ARTIKLI_EKSPORT_SKLONI','Sklanjanje artikla iz eksporta'),
	    	(123,'ARTIKLI_AKCIJA','Akcija na artiklima'),
	    	(124,'ARTIKLI_SLIKE_DODAJ','DODAVANJE slika'),
	    	(125,'ARTIKLI_SLIKA_GLAVNA','Artikli - Slika postaje glavna'),
	    	(126,'ARTIKLI_SLIKA_OPIS','Artikli - Slika opis'),
	    	(127,'ARTIKLI_SLIKE_PRIKAZI_PRODAVNICA','Artikli - Prikaži slike u prodavnici'),
	    	(128,'ARTIKLI_SLIKE_SKLONI_PRODAVNICA','Artikli - Skloni slike iz prodavnice'),
	    	(129,'ARTIKLI_IZMENI_CENE_MARZE','Artikli - izmeni cenu marže'),
	    	(130,'ARTIKLI_IZMENI_CENE_RABATA','Artikli - izmeni rabat'),
	    	(131,'ARTIKLI_IZMENI_WEB_CENA','Artikli - izmeni web cenu'),
	    	(132,'ARTIKLI_IZMENI_MP_CENA','Artikli - izmeni MP cenu'),
	    	(133,'ARTIKLI_DODAJ_TEKST_NAZIV','Artikli - DODAVANJE teksta u naziv'),
	    	(134,'ARTIKLI_DODAJ_TEKST_OPIS','Artikli - DODAVANJE teksta u opis'),
	    	(135,'ARTIKLI_IZMENI_TEKST_NAZIV','Artikli - IZMENA naziva'),
	    	(136,'ARTIKLI_IZMENI_TEKST_OPIS','Artikli - IZMENA opisa'),
	    	(137,'ARTIKLI_PRERACUNAJ_WEB_CENU','Artikli - Preračunaj web cenu'),
	    	(138,'ARTIKLI_PRERACUNAJ_MP_CENU','Artikli - Preračunaj MP cenu'),
	    	(139,'ARTIKLI_DODAJ_G_KARAKTERISTIKE','Artikli - DODAVANJE generisanih karakteristika'),
	    	(140,'ARTIKLI_OBRISI_G_KARAKTERISTIKE','Artikli - BRISANJE generisanih karakteristika'),
	    	(141,'ARTIKLI_DODAJ_KARAKTERISTIKE','Artikli - DODAVANJE karakteristika'),
	    	(142,'ARTIKLI_IZMENI_GRUPU','Artikli - IZMENA grupe'),
	    	(143,'ARTIKLI_IZMENI_PROIZVODJAC','Artikli - IZMENA proizvođača'),
	    	(144,'ARTIKLI_IZMENI_TIP','Artikli - IZMENA tipa'),
	    	(145,'ARTIKLI_IZMENI_VRSTU_CENE','Artikli - IZMENA vrste cene'),
	    	(146,'ARTIKLI_IZMENI_KOLICINA',' Artikli - IZMENA količine'),
	    	(147,'ARTIKLI_IZMENI_PORESKA_STOPA','Artikli - IZMENA poreske stope'),
	    	(148,'ARTIKLI_IZMENI_TEZINU',' Artikli - IZMENA težine'),
	    	(149,'ARTIKLI_DODAJ_TAG','Artikli - DODAVANJE taga'),
	    	(150,'ARTIKLI_OBRISI_KARAKTERISTIKE','Artikli - BRISANJE karakteristika'),
	    	(151,'ARTIKLI_PREBACI_HTML_KARAKTERISTIKE','Prebacivanje html karakteristika u opis'),
	    	(152,'ARTIKLI_IZMENA_SEO','IZMENA seo - a'),
	    	(153,'PRODUCT_OSOBINA_DODAJ_SVE','DODAVANJE osobina'),
	    	(154,'UCITANE_SLIKE_DODAJ','DODAVANJE učitane slike'),
	    	(155,'UCITANE_SLIKE_OBRISI','BRISANJE učitane slike'),
	    	(156,'ARTIKAL_KARAKTERISTIKE_POZICIJA','Pozicija karakteristika u artiklu'),
	    	(157,'ARTIKAL_KARAKTERISTIKE_NAZIV_POZICIJA','Pozicija naziva i vrednosti karakteristike u artiklu'),
	    	(158,'SIFARNICI_GRUPA_OBRISI','Šifarnici - BRISANJE grupe'),
	    	(159,'SIFARNICI_GRUPA_DODAJ','Šifarnici - DODAVANJE grupe'),
	    	(160,'SIFARNICI_GRUPA_IZMENI','Šifarnici - IZMENA grupe'),
	    	(161,'SIFARNICI_GRUPA_KARAKTERISTIKE_DODAJ','Šifarnici - grupa - DODAVANJE karakteristike'),
	    	(162,'SIFARNICI_GRUPA_NAZIV_IZMENI','Šifarnici - grupa - IZMENA karakteristike'),
	    	(163,'SIFARNICI_GRUPA_KARAKTERISTIKA_AKTIVNA_IZMENI','Šifarnici - grupa - karakteristika - IZMENA flaga aktivan'),
	    	(164,'ARTIKLI_KARAKTERISTIKA_OBRISI','Artiki - BRISANJE karakteristike'),
	    	(165,'SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_DODAJ','Šifarnici - grupa - karakteristika - DODAVANJE vrednosti'),
	    	(166,'SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_IZMENI','Šifarnici - grupa - karakteristika - IZMENA vrednosti'),
	    	(167,'SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_AKTIVNA_IZMENI','Šifarnici - grupa - karakteristika - IZMENA flaga aktivan'),
	    	(168,'SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_OBRISI','Šifarnici - grupa - karakteristika - BRISANJE vrednosti'),
	    	(169,'GRUPA_REDNI_BROJ_IZMENI','IZMENA rednog broja grupe'),
	    	(170,'KUPAC_IZMENI','IZMENA kupaca'),
	    	(171,'KUPAC_DODAJ','DODAVANJE kupaca'),
	    	(172,'KUPCI_OBRISI','BRISANJE kupaca'),
	    	(173,'KUPAC_KAO_PARTNER_DODAJ','DODAVANJE kupca kao partnera'),
	    	(174,'PARTNER_DODAJ','DODAVANJE partnera'),
	    	(175,'PARTNER_IZMENI','IZMENA partnera'),
	    	(176,'PARTNER_OBRISI','BRISANJE partnera'),
	    	(177,'EXPORT_MAIL','Eksportovanje mejlova'),
	    	(178,'SIFARNIK_NACIN_ISPORUKE_DODAJ','Šifarnik - Način isporuke DODAVANJE'),
	    	(179,'SIFARNIK_NACIN_ISPORUKE_IZMENI','Šifarnik - Način isporuke IZMENA'),
	    	(180,'SIFARNIK_NACIN_ISPORUKE_OBRISI','Šifarnik - Način isporuke BRISANJE'),
	    	(181,'SIFARNIK_NACIN_PLACANJA_DODAJ','Šifarnik - Način plaćanja DODAVANJE'),
	    	(182,'SIFARNIK_NACIN_PLACANJA_IZMENI','Šifarnik - Način plaćanja IZMENA'),
	    	(183,'SIFARNIK_NACIN_PLACANJA_OBRISI','Šifarnik - Način plaćanja BRISANJE'),
	    	(184,'NARUDZBINA_STATUS_STORNIRAJ','Narudžbina status - storniraja'),
	    	(185,'NARUDZBINA_IZMENI','IZMENA narudžbine'),
	    	(186,'NARUDZBINA_DODAJ','DODAVANJE narudžbine'),
	    	(187,'NARUDZBINA_OBRISI','BRISANJE narudžbine'),
	    	(188,'NARUDZBINA_STATUS_PRIHVATI','Narudžbina status - prihvati'),
	    	(189,'NARUDZBINA_STATUS_REALIZUJ','Narudžbina status - realizuj'),
	    	(190,'NARUDZBINA_STAVKA_OBRISI','Narudžbina stavka obriši'),
	    	(191,'NARUDZBINA_STAVKA_DODAJ','Narudžbina stavka dodaj'),
	    	(192,'NARUDZBINA_IZMENI_KOLICINA','Narudžbina izmeni količinu'),
	    	(193,'NARUDZBINA_ODSTORNIRAJ','Narudzbina odstornirana'),
	    	(194,'NARUDZBINA_STAVKA_IZMENI','Izmeni narudžbina stavka'),
	    	(195,'NARUDZBINA_STAVKA_CENA_IZMENI','IZMENA cene kod narudžbine stavka'),
	    	(196,'SIFARNIK_OSOBINA_DODAJ','Šifarnik - Osobina DODAVANJE'),
	    	(197,'SIFARNIK_OSOBINA_IZMENI','Šifarnik - Osobina IZMENA'),
	    	(198,'SIFARNIK_OSOBINA_VREDNOST_DODAJ','Šifarnik - Osobina vrednost - DODAVANJE'),
	    	(199,'SIFARNIK_OSOBINA_VREDNOST_IZMENI','Šifarnik - Osobina vrednost - IZMENA'),
	    	(200,'SIFARNIK_OSOBINA_OBRISI','Šifarnik - Osobina naziv BRISANJE'),
	    	(201,'SIFARNIK_OSOBINA_VREDNOST_OBRISI','Šifarnik - Osobina vrednost BRISANJE'),
	    	(202,'SIFARNIK_OSOBINA_VREDNOST_POZICIJA','Šifarnik - Osobina vrednost pozicija'),
	    	(203,'PRODUCT_OSOBINA_ROBA_DODAJ','Artikal - Osobina Roba - DODAVANJE'),
	    	(204,'PRODUCT_OSOBINA_ADD_ALL','Artikal - Osobina - DODAVANJE svih'),
	    	(205,'PRODUCT_OSOBINA_IZMENI','Artikal osobina IZMENA'),
	    	(206,'PRODUCT_OSOBINA_OBRISI','Artikal BRISANJE osobine'),
	    	(207,'PRODUCT_OSOBINA_OBRISI_GRUPA','Artikal BRISANJE grupe osobina'),
	    	(208,'PRODUCT_OSOBINA_OBRISI_SVE','Artikal BRISANJE svih osobina'),
	    	(209,'ADMINISTRATOR_DODAJ','DODAVANJE administratora'),
	    	(210,'ADMINISTRATOR_IZMENI','IZMENA administratora'),
	    	(211,'ADMINISTRATOR_OBRISI','BRISANJE administratora'),
	    	(212,'NIVOI_PRISTUPA_DODAJ','DODAVANJE nivoa pristupa'),
	    	(213,'NIVOI_PRISTUPA_IZMENI','IZMENA nivoa pristupa'),
	    	(214,'NIVOI_PRISTUPA_OBRISI','BRISANJE nivoa pristupa'),
	    	(215,'PROIZVODJAC_DODAJ','DODAVANJE proizvođača'),
	    	(216,'PROIZVODJAC_IZMENI','IZMENA proizvođača'),
	    	(217,'PROIZVODJAC_OBRISI','BRISANJE proizvođača'),
	    	(218,'TIP_ARTIKLA_IZMENI','IZMENA tipa artikla'),
	    	(219,'TIP_ARTIKLA_DODAJ','DODAVANJE tipa artikla'),
	    	(220,'TIP_ARTIKLA_OBRISI','BRISANJE tipa artikla'),
	    	(221,'JEDINICA_MERE_IZMENI','IZMENA jedinice mere'),
	    	(222,'JEDINICA_MERE_DODAJ','DODAVANJE jedinice mere'),
	    	(223,'JEDINICA_MERE_OBRISI','BRISANJE jedinice mere'),
	    	(224,'PORESKA_STOPA_IZMENI','IZMENA poreske stope'),
	    	(225,'PORESKA_STOPA_DODAJ','DODAVANJE poreske stope'),
	    	(226,'PORESKA_STOPA_OBRISI','BRISANJE poreske stope'),
	    	(227,'STANJE_ARTIKLA_IZMENI','IZMENA stanja artikla'),
	    	(228,'STANJE_ARTIKLA_DODAJ','DODAVANJE stanja artikla'),
	    	(229,'STANJE_ARTIKLA_OBRISI','BRISANJE stanja artikla'),
	    	(230,'STATUS_NARUDZBINE_IZMENI','IZMENA statusa narudžbine'),
	    	(231,'STATUS_NARUDZBINE_DODAJ','DODAVANJE statusa narudžbine'),
	    	(232,'STATUS_NARUDZBINE_OBRISI','BRISANJE statusa narudžbine'),
	    	(233,'KURIRSKA_SLUZBA_DODAJ','DODAVANJE kurirske službe'),
	    	(234,'KURIRSKA_SLUZBA_IZMENI','IZMENA kurirske službe'),
	    	(235,'KURIRSKA_SLUZBA_OBRISI','BRISANJE kurirske službe'),
	    	(236,'KONFIGURATOR_DODAJ','DODAVANJE konfiguratora'),
	    	(237,'KONFIGURATOR_IZMENI','IZMENA konfiguratora'),
	    	(238,'KONFIGURATOR_OBRISI','BRISANJE konfiguratora'),
	    	(239,'MESTO_DODAJ','DODAVANJE mesta'),
	    	(240,'MESTO_IZMENI','IZMENA mesta'),
	    	(241,'MESTO_OBRISI','BRISANJE mesta'),
	    	(242,'SIFARNIK_DEFINISANE_MARZE_DODAJ','DODAVANJE definisanih marži'),
	    	(243,'SIFARNIK_DEFINISANE_MARZE_IZMENI','IZMENA definisanih marži'),
	    	(244,'SIFARNIK_DEFINISANE_MARZE_OBRISI','BRISANJE definisanih marži'),
	    	(245,'SIFARNIK_TROSKOVI_ISPORUKE_DODAJ','DODAVANJE troškova isporuke'),
	    	(246,'SIFARNIK_TROSKOVI_ISPORUKE_IZMENI','IZMENA troškova isporuke'),
	    	(247,'SIFARNIK_TROSKOVI_ISPORUKE_OBRISI','BRISANJE troškova isporuke'),
	    	(248,'CENA_IZMENI','IZMENA cene isporuke'),
	    	(249,'B2B_CENA_IZMENI','B2B IZMENA cene narudžbine'),
	    	(250,'CENA_DODAJ','DODAVANJE cene isporuke'),
	    	(251,'CENA_OBRISI','BRISANJE cene isporuke'),
	    	(252,'VESTI_DODAJ','DODAVANJE vesti'),
	    	(253,'VESTI_IZMENI','IZMENA vesti'),
	    	(254,'VESTI_OBRISI','Vesti BRISANJE'),
	    	(255,'B2B_NARUDZBINA_STAVKA_DODAJ','B2B Narudžbina stavka - DODAVANJE'),
	    	(256,'B2B_NARUDZBINA_STATUS_PRIHVATI','B2B Narudžbina stavka status - prihvaćen'),
	    	(257,'B2B_NARUDZBINA_STATUS_REALIZUJ','B2B Narudžbina stavka status - realizovan'),
	    	(258,'B2B_NARUDZBINA_STATUS_STORNIRAJ','B2B Narudžbina stavka status - storniran'),
	    	(259,'B2B_NARUDZBINA_ODSTORNIRAJ','B2B Odstorniranje narudžbine'),
	    	(260,'B2B_NARUDZBINA_STAVKA_OBRISI','B2B Narudžbina stavka - BRISANJE'),
	    	(261,'B2B_NARUDZBINA_(STAVKA)_OBRISI','B2B Narudžbina BRISANJE'),
	    	(262,'B2B_NARUDZBINA_STAVKA_IZMENI_KOLICINA','B2B Narudžbina stavka IZMENA količine'),
	    	(263,'B2B_PRODAVNICA_DODAJ','B2B Stranica - DODAVANJE'),
	    	(264,'B2B_PRODAVNICA_IZMENI','B2B Stranica IZMENA'),
	    	(265,'B2B_PRODAVNICA_OBRISI','B2B Stranica BRISANJE'),
	    	(266,'B2B_STRANICE_SLIKA_POSTAVI','B2B Postavljanje slike kod stranica'),
	    	(267,'B2B_KATEGORIJA_IZMENI_RAB','B2B IZMENA kategorije'),
	    	(268,'B2B_KATEGORIJA_IZMENI','B2B IZMENA kategorije'),
	    	(269,'B2B_KATEGORIJA_DODAJ','B2B DODAVANJE kategorije'),
	    	(270,'B2B_KATEGORIJA_OBRISI','B2B BRISANJE kategorije'),
	    	(271,'B2B_MAGACIN_OMOGUCI','B2B Omogućavanje magacina'),
	    	(272,'B2B_MAGACIN_ONEMOGUCI','B2B Onemogućavanje magacina'),
	    	(273,'B2B_MAGACIN_PRIMARNI','B2B Magacin postaje primaran'),
	    	(274,'B2B_MAGACIN_DODAJ','B2B DODAVANJE magacina'),
	    	(275,'B2B_MAGACIN_OBRISI','B2B BRISANJE magacina'),
	    	(276,'B2B_OPSTA_PODESAVANJA_DA','B2B Opšta podešavanja - uključi'),
	    	(277,'B2B_OPSTA_PODESAVANJA_NE','B2B Opšta podešavanja - isključi'),
	    	(278,'B2B_VESTI_IZMENI','B2B IZMENA vesti'),
	    	(279,'B2B_VESTI_DODAJ','B2B DODAVANJE vesti'),
	    	(280,'B2B_VESTI_OBRISI','B2B BRISANJE vesti'),
	    	(281,'FUTER_DODAJ','Futer - DODAVANJE sekcije'),
	    	(282,'FUTER_IZMENI','Futer - IZMENA sekcije'),
	    	(283,'POZICIJA_STRANICA','Pozicija stranica'),
	    	(284,'BANER_POZICIJA','Pozicija - banera'),
	    	(285,'SLAJDER_POZICIJA','Pozicija - slajdera'),
	    	(286,'POP_UP_POZICIJA','Pozicija - pop - up banera'),
	    	(287,'KONTAKT_PODACI_IZMENI','IZMENA kontakt podataka'),
	    	(288,'LOGO_IZMENI','IZMENA logoa firme'),
	    	(289,'CSS_IZMENI','IZMENA css - a'),
	    	(290,'BANERI_LINKOVI','Baneri i slajderi - DODAVANJE linkova'),
	    	(291,'BANERI_SLIKA','Baneri i slajderi - DODAVANJE pozadinske slike'),
	    	(292,'BANERI_SLIKA_OBRISI','Baneri i slajderi - BRISANJE pozadinske slike i linkova'),
	    	(293,'PODESAVANJA_IZMENI','IZMENA podešavanja'),
	    	(294,'PODESAVANJA_OPTIONS_IZMENI','IZMENA podešavanja'),
	    	(295,'BANERI_OBRISI','BRISANJE banera'),
	    	(296,'SLAJDERI_OBRISI','BRISANJE slajdera'),
	    	(297,'POPUP_BANERI_OBRISI','BRISANJE popup banera'),
	    	(298,'FUTER_OBRISI','Futer - BRISANJE sekcije'),
	    	(299,'LOGOVANJE','Logovanje'),
	    	(300,'FUTER_STRANICA_OBRISI','BRISANJE stranice futera'),
	    	(301,'POZICIJA_FUTER_SEKCIJA','Futer - promena pozicije'),
	    	(302,'POZICIJA_OSOBINE','Pozicija osobine'),
	    	(303,'SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_POZICIJA','Šifarnici - grupa - karakteristika - vrednost - pozicija'),
	    	(304,'SIFARNICI_GRUPA_KARAKTERISTIKA_POZICIJA','Šifarnici - grupa - karakteristika - pozicija'),
	    	(305,'KOMENTAR_IZMENI','IZMENA komentara (bez odobravanja)'),
	    	(306,'KOMENTAR_IZMENI_ODOBRAVANJE','IZMENA komentara (sa odobravanjem)'),
	    	(307,'KOMENTAR_OBRISI','BRISANJE komentara'),
	    	(308,'KURS_IZMENI','IZMENA kursa'),
	    	(309,'PARTNER_GRUPA_OBRISI','BRISANJE povezanih grupa u web importu'),
	    	(310,'WEB_IMPORT_PROIZVODJAC_DODAJ','Web import - DODAVANJE proizvođača'),
	    	(311,'WEB_IMPORT_PROIZVODJAC_IZMENI','Web import - IZMENA proizvođača'),
	    	(312,'WEB_IMPORT_PROIZVODJAC_OBRISI','Web import - obriši proizvođača'),
	    	(313,'PORUDZBINA_OBRISI','BRISANJE porudžbine'),
	    	(314,'B2B_NARUDZBINA_IZMENI','B2B IZMENA narudžbine'),
	    	(315,'B2B_NARUDZBINA_DODAJ','B2B DODAVANJE narudžbine'),
	    	(316,'B2B_NARUDZBINA_STATUS_REALIZUJ','B2B IZMENA statusa narudžbine - realizuj'),
	    	(317,'B2B_NARUDZBINA_STATUS_PRIHVATI','B2B IZMENA statusa narudžbine - prihvati'),
	    	(318,'B2B_NARUDZBINA_STATUS_STORNIRAJ','B2B IZMENA statusa narudžbine - storniraj'),
	    	(319,'B2B_RABAT_IZMENI','B2B IZMENA rabata'),
	    	(320,'B2B_RABAT_AKCIJSKA_GRUPA_IZMENI','B2B Dodela akcijskog rabata'),
	    	(321,'B2B_RABAT_GRUPA_IZMENI','B2B Dodela maksimalnog rabata'),
	    	(322,'B2B_RABAT_KOMBINACIJA_IZMENI','B2B Rabat kombinacija izmeni'),
	    	(323,'RABAT_PARTNER_IZMENI','B2B IZMENA rabata partnera'),
	    	(324,'KATEGORIJA_PARTNER_IZMENI','B2B IZMENA kategorije partnerima'),
	    	(325,'RABAT_PROIZVODJAC_IZMENI','B2B IZMENA rabata proizvođača'),
	    	(326,'RABAT_KOMBINACIJA_DODAJ','B2B DODAVANJE partnera u kombinacije rabata'),
	    	(327,'RABAT_KOMBINACIJE_OBRISI','B2B BRISANJE partnera iz rabat kombinacija'),
	    	(328,'B2B_BANERI_DODAJ','B2B DODAVANJE banera'),
	    	(329,'B2B_BANERI_IZMENI','B2B IZMENA banera'),
	    	(330,'B2B_BANERI_OBRISI','B2B BRISANJE banera'),
	    	(331,'B2B_SLAJDERI_DODAJ','B2B DODAVANJE slajdera'),
	    	(332,'B2B_SLAJDERI_IZMENI','B2B IZMENA slajdera'),
	    	(333,'B2B_SLAJDERI_OBRISI','B2B BRISANJE slajdera'),
	    	(334,'B2B_SLAJDER_POZICIJA','B2B pozicija slajdera'),
	    	(335,'B2B_POP_UP_DODAJ','B2B DODAVANJE pop - up banera'),
	    	(336,'B2B_POP_UP_IZMENI','B2B IZMENA pop - up banera'),
	    	(337,'B2B_POP_UP_OBRISI','B2B BRISANJE pop - up banera'),
	    	(338,'B2B_POP_UP_POZICIJA','B2B pozicija pop - up banera'),
	    	(339,'B2B_BANERI_SLIKA_DODAJ','B2B Baneri DODAVANJE pozadinske slike'),
	    	(340,'B2B_BANERI_LINKOVI_DODAJ','B2B Baneri DODAVANJE linkova'),
	    	(341,'B2B_BANERI_SLIKE_LINKOVI_OBRISI','B2B Baneri BRISANJE pozadinske slike i linkova'),
	    	(342,'B2B_STRANICE_POZICIJA','B2B Stranice Pozicija'),
	    	(343,'B2B_BANER_POZICIJA','B2B pozicija banera'),
	    	(344,'GRUPE_POVEZI','Povezivanje grupa'),
	    	(345,'GRUPE_RAZVEZI','Razvezivanje grupa'),
	    	(346,'GRUPA_FAJL_OBRISI','BRISANJE fajla'),
	    	(347,'GRUPA_FAJL_DODAJ','DODAVANJE fajla'),
	    	(348,'GLAVNI_MODUL_IZMENA','IZMENA glavnog modula'),
	    	(349,'SPOREDNI_MODUL_IZMENA','IZMENA sporednog modula'),
	    	(350,'FRONT_IZMENA_PRODAJA','Front - IZMENA prodaje'),
	    	(351,'FRONT_IZMENA_VESTI','Front - IZMENA vesti'),
	    	(352,'FRONT_IZMENA_SVI_ARTIKLI','Front - IZMENA vesti'),
	    	(353,'FRONT_IZMENA_TIP','Front - IZMENA tipa'),
	    	(354,'FRONT_IZMENA_NASLOV_SLAJDER','Front - IZMENA naslova slajdera'),
	    	(355,'FRONT_IZMENA_PODNASLOV_SLAJDER','Front - IZMENA podnaslova slajdera'),
	    	(356,'FRONT_IZMENA_SADRZAJ_SLAJDER','Front - IZMENA sadržaja slajdera'),
	    	(357,'FRONT_IZMENA_DUGME_SLAJDER','Front - IZMENA dugmeta slajdera'),
	    	(358,'FRONT_IZMENA_FUTER_NASLOV','Front - IZMENA naslova futera'),
	    	(359,'FRONT_IZMENA_FUTER_SADRZAJ','Front - IZMENA sadržaja futera'),
	    	(360,'FRONT_IZMENA_BROSURA','Front - IZMENA brošure'),
	    	(361,'FRONT_IZMENA_SADRZAJ_BROSURA','Front - IZMENA sadržaja brošure'),
	    	(362,'FRONT_IZMENA_NASLOV_SADRZAJ','Front - IZMENA naslova sadržaja'),
	    	(363,'FRONT_IZMENA_SADRZAJ','Front - IZMENA sadržaja'),
	    	(364,'B2B_PARTNER_IZMENI','B2B IZMENA partnera'),
	    	(365,'SADRZAJ_KORPE_OBRISI','B2B BRISANJE sadržaja korpe'),
	    	(366,'SADRZAJ_KORPE_OBRISI_SVE','B2B kompletno BRISANJE sadržaja korpe'),
	    	(367,'UPLOADOVANE_SLIKE_OBRISI','Stranice - BRISANJE slika'),
	    	(368,'BANERI_IZMENI','IZMENA banera'),
	    	(369,'SLAJDERI_IZMENI','IZMENA slajdera'),
	    	(370,'POPUP_BANERI_IZMENI','IZMENA popup banera'),
	    	(371,'BANERI_DODAJ','DODAVANJE banera'),
	    	(372,'SLAJDERI_DODAJ','DODAVANJE slajdera'),
	    	(373,'POPUP_BANERI_DODAJ','DODAVANJE popup banera'),
	    	(374,'B2B_UPLOADOVANE_SLIKE_OBRISI','B2B BRISANJE uploadovanih slika kod stranica'),
	    	(375,'WEB_IMPORT_FILE_UPLOAD','Web import - Upload fajl-a na server'),
	    	(376,'WEB_IMPORT_DODAJ_GRUPU','Web import - DODAVANJE grupe'),
	    	(377,'WEB_IMPORT_IZMENI_GRUPU','Web import - IZMENA grupe'),
	    	(378,'WEB_IMPORT_OBRISI_GRUPU','Web import - BRISANJE grupe'),
	    	(379,'WEB_IMPORT_IMPORTUJ_CENOVNIK','Web import - Importovanje cenovnika'),
	    	(380,'ARTIKLI_DEFINISANE_MARZE_DODELI','Artikli - Dodela definisanih marži'),
	    	(381,'ARTIKLI_PRODUZENE_GARANCIJE_DODAJ','Artikli - Dodela definisanih marži'),
	    	(382,'ARTIKLI_PRODUZENE_GARANCIJE_DODAJ','Artikli - DODELA produžene garancije'),
	    	(383,'ARTIKLI_PRODUZENE_GARANCIJE_OBRISI','Artikli - BRISANJE produžene garancije'),
	    	(384,'ARTIKLI_DODATNE_GRUPE_DODELI','Artikli - DODAVANJE dodatne grupe'),
	    	(385,'ARTIKLI_DODATNE_GRUPE_OBRISI','Artikli - BRISANJE dodatne grupe'),
	    	(386,'ARTIKLI_DODATNE_GRUPE_OBRISI_SVE','Artikli - BRISANJE svih dodatnih grupa'),
	    	(387,'VEZANI_ARTIKLI_IZMENI_CENA','IZMENA cene vezanog artikla'),
	    	(388,'SRODNI_ARTIKLI_IZMENI_KARAK','Srodni artikli - IZMENA vrednosti karakteristike'),
	    	(389,'SRODNI_ARTIKLI_FLAG_CENA_IZMENI','Srodni artikli - flag cena IZMENA'),
	    	(390,'SRODNI_ARTIKLI_OBRISI','BRISANJE srodnih artikala'),
	    	(391,'SRODNI_ARTIKLI_DODAJ','DODAVANJE srodnih artikala'),
	    	(392,'RADNI_NALOG_DODAJ','DODAVANJE radnog naloga'),
	    	(393,'RADNI_NALOG_IZMENI','IZMENA radnog naloga'),
	    	(394,'RMA_OPERACIJA_GRUPA_DODAJ','RMA DODAVANJE grupe operacija'),
	    	(395,'RMA_OPERACIJA_GRUPA_IZMENI','RMA IZMENA grupe operacija'),
	    	(396,'RMA_OPERACIJA_GRUPA_OBRISI','RMA BRISANJE grupe operacija'),
	    	(397,'RMA_OPERACIJA_IZMENI','RMA IZMENA operacije'),
	    	(398,'RMA_OPERACIJA_DODAJ','RMA DODAVANJE operacije'),
	    	(399,'RMA_OPERACIJA_OBRISI','RMA BRISANJE operacije'),
	    	(400,'RMA_SERVISER_IZMENI','RMA IZMENA servisera'),
	    	(401,'RMA_SERVISER_DODAJ','RMA DODAVANJE servisera'),
	    	(402,'RMA_SERVISER_OBRISI','RMA BRISANJE servisera'),
	    	(403,'CSS_IZMENI_CUSTOM','CSS izmena'),
	    	(404,'CSS_OBRISI_CUSTOM','CSS brisanje')
	    	) redovi(log_akcija_id,sifra,naziv) 

	        WHERE NOT EXISTS (SELECT * FROM log_akcija)");


		DB::statement("ALTER TABLE roba ALTER COLUMN ambalaza TYPE numeric(20,4)");

		DB::statement("INSERT INTO vrsta_dokumenta (vrsta_dokumenta_id,sifra_vd,naziv_vd,naziv) SELECT * FROM (VALUES 
			(40,'PON','Ponuda','Ponuda'),
			(50,'PRE','Predračun','Predračun'),
	    	(100,'RAC','Račun','Račun')
	    ) redovi(vrsta_dokumenta_id,sifra_vd,naziv_vd,naziv) WHERE vrsta_dokumenta_id NOT IN (SELECT vrsta_dokumenta_id FROM vrsta_dokumenta)");

		DB::statement("INSERT INTO partner_vrsta (partner_vrsta_id,naziv) SELECT * FROM (VALUES 
	    	(118,'Servis'),
	    	(119,'Uslužni servis'),
	    	(120,'Proizvođač'),
	    	(258,'CRM'),
	    	(122,'Instalater')
	    ) redovi(partner_vrsta_id,naziv) WHERE partner_vrsta_id NOT IN (SELECT partner_vrsta_id FROM partner_vrsta)");

	    DB::statement("INSERT INTO partner_je (partner_id,partner_vrsta_id) SELECT * FROM (VALUES 
	    	(1,118)
	    ) redovi(partner_id,partner_vrsta_id) WHERE (partner_id,partner_vrsta_id) NOT IN (SELECT partner_id,partner_vrsta_id FROM partner_je)");

		DB::statement("SELECT addtable('public','racun','racun_id bigserial NOT NULL,broj_dokumenta character varying(20) NOT NULL,datum_racuna date,partner_id integer,vrsta_dokumenta_id smallint,iznos numeric(20,2) DEFAULT 0,CONSTRAINT racun_pkey PRIMARY KEY (racun_id),CONSTRAINT racun_fk1 FOREIGN KEY (vrsta_dokumenta_id) REFERENCES vrsta_dokumenta (vrsta_dokumenta_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION, CONSTRAINT racun_partner_id_fkey FOREIGN KEY (partner_id) REFERENCES partner (partner_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION')");
		DB::statement("SELECT addtable('public','racun_stavka','racun_stavka_id bigserial NOT NULL,racun_id bigint NOT NULL,broj_stavke integer,roba_id integer,nab_cena numeric(20,4),pdv real,pcena numeric(20,4),kolicina real,opis_robe character varying(300),CONSTRAINT racun_stavka_pkey PRIMARY KEY (racun_stavka_id),CONSTRAINT racun_stavka_racun_id_fkey FOREIGN KEY (racun_id) REFERENCES racun (racun_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

		//RMA
		DB::statement("SELECT addtable('public','operacija_grupa','operacija_grupa_id bigserial NOT NULL,naziv character varying(255) NOT NULL,CONSTRAINT operacija_grupa_pkey PRIMARY KEY (operacija_grupa_id)')");
		DB::statement("SELECT addtable('public','operacija','operacija_id bigserial NOT NULL, naziv character varying(255) NOT NULL, cena numeric(20,2) NOT NULL, operacija_grupa_id integer NOT NULL, CONSTRAINT operacija_pkey PRIMARY KEY (operacija_id), CONSTRAINT operacija_fk FOREIGN KEY (operacija_grupa_id) REFERENCES public.operacija_grupa (operacija_grupa_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION')");
		DB::statement("SELECT addtable('public','operacija_servis','operacija_servis_id bigserial NOT NULL, partner_id bigint NOT NULL, operacija_id integer NOT NULL, tarifna_grupa_id integer NOT NULL, cena numeric(20,2) NOT NULL, CONSTRAINT operacija_servis_pkey PRIMARY KEY (operacija_servis_id), CONSTRAINT operacija_servis_fk FOREIGN KEY (partner_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION, CONSTRAINT operacija_servis_fk1 FOREIGN KEY (operacija_id) REFERENCES public.operacija (operacija_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION, CONSTRAINT operacija_servis_fk2 FOREIGN KEY (tarifna_grupa_id) REFERENCES public.tarifna_grupa (tarifna_grupa_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION')");

		DB::statement("SELECT addtable('public','radni_nalog_status','radni_nalog_status_id bigserial NOT NULL,naziv character varying(255) NOT NULL,CONSTRAINT radni_nalog_status_pkey PRIMARY KEY (radni_nalog_status_id)')");

		DB::statement("SELECT addtable('public','serviser','
		  serviser_id bigserial NOT NULL,
		  ime character varying(255) NOT NULL,
		  prezime character varying(255) NOT NULL,
		  email character varying(255) NOT NULL,
		  lozinka character varying(255) NOT NULL,
		  adresa character varying(255),
		  mesto character varying(255),
		  telefon character varying(255),
		  aktivan smallint DEFAULT 1,
		  partner_id bigint NOT NULL,
		  CONSTRAINT serviser_pkey PRIMARY KEY (serviser_id),
		  CONSTRAINT serviser_fk1 FOREIGN KEY (partner_id)
		      REFERENCES public.partner (partner_id) MATCH SIMPLE
		      ON UPDATE CASCADE ON DELETE NO ACTION
			')");

		DB::statement("SELECT addtable('public','radni_nalog','
		  radni_nalog_id bigserial NOT NULL,
		  partner_id bigint NOT NULL,
		  kupac_id bigint NOT NULL,
		  primio_serviser_id bigint,
		  predao_serviser_id bigint,
		  broj_naloga character varying(20) NOT NULL,
		  datum_prijema timestamp without time zone NOT NULL,
		  datum_zavrsetka timestamp without time zone,
		  napomena character varying(1000),
		  uredjaj character varying(500),
		  opis_kvara character varying(1000),
		  roba_id bigint,
		  serijski_broj character varying(255),
		  preuzeo_ime character varying(50),
		  preuzeo_brojlk character varying(20),
		  status_id integer NOT NULL DEFAULT 1,
		  predat_uredjaj character varying(500),
		  napomena_operacije character varying(500),
		  datum_servisiranja date,
		  partner_proizvodjac_id integer,
		  datum_kupovine date,
		  broj_fiskalnog_racuna character varying(500),
		  maloprodaja character varying(500),
		  CONSTRAINT radni_nalog_pkey PRIMARY KEY (radni_nalog_id),
		  CONSTRAINT radni_nalog_fk1 FOREIGN KEY (partner_id)
		      REFERENCES public.partner (partner_id) MATCH SIMPLE
		      ON UPDATE CASCADE ON DELETE NO ACTION,
		  CONSTRAINT radni_nalog_fk2 FOREIGN KEY (kupac_id)
		      REFERENCES public.web_kupac (web_kupac_id) MATCH SIMPLE
		      ON UPDATE CASCADE ON DELETE NO ACTION,
		  CONSTRAINT radni_nalog_fk3 FOREIGN KEY (primio_serviser_id)
		      REFERENCES public.serviser (serviser_id) MATCH SIMPLE
		      ON UPDATE CASCADE ON DELETE NO ACTION,
		  CONSTRAINT radni_nalog_fk4 FOREIGN KEY (predao_serviser_id)
		      REFERENCES public.serviser (serviser_id) MATCH SIMPLE
		      ON UPDATE NO ACTION ON DELETE NO ACTION,
		  CONSTRAINT radni_nalog_fk5 FOREIGN KEY (status_id)
		      REFERENCES public.radni_nalog_status (radni_nalog_status_id) MATCH SIMPLE
		      ON UPDATE NO ACTION ON DELETE NO ACTION,
		  CONSTRAINT radni_nalog_fk6 FOREIGN KEY (roba_id)
		      REFERENCES public.roba (roba_id) MATCH FULL
		      ON UPDATE NO ACTION ON DELETE NO ACTION,
		  CONSTRAINT radni_nalog_fk7 FOREIGN KEY (partner_proizvodjac_id)
		      REFERENCES public.partner (partner_id) MATCH FULL
		      ON UPDATE NO ACTION ON DELETE NO ACTION')");

		DB::statement("SELECT addtable('public','radni_nalog_operacije','
			radni_nalog_operacije_id bigserial NOT NULL,
			radni_nalog_id bigint NOT NULL,
			serviser_id bigint NOT NULL,
			operacija_servis_id bigint NOT NULL,
			roba_id bigint,
			opis_operacije character varying(100) NOT NULL,
			norma_sat numeric(20,2) NOT NULL DEFAULT 0,
			cena_sata numeric(20,2) NOT NULL DEFAULT 0,
			popust numeric(20,2) NOT NULL DEFAULT 0,
			uradjeno numeric(20,2) NOT NULL DEFAULT 0,
			status smallint NOT NULL DEFAULT 0,
			iznos numeric(20,2) NOT NULL DEFAULT 0,
			CONSTRAINT radni_nalog_operacije_pkey PRIMARY KEY (radni_nalog_operacije_id),
			CONSTRAINT radni_nalog_operacije_fk FOREIGN KEY (radni_nalog_id)
			  REFERENCES public.radni_nalog (radni_nalog_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE CASCADE,
			CONSTRAINT radni_nalog_operacije_fk1 FOREIGN KEY (operacija_servis_id)
			  REFERENCES public.operacija_servis (operacija_servis_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE NO ACTION,
			CONSTRAINT radni_nalog_operacije_fk2 FOREIGN KEY (serviser_id)
			  REFERENCES public.serviser (serviser_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE NO ACTION,
			CONSTRAINT radni_nalog_operacije_fk3 FOREIGN KEY (roba_id)
			  REFERENCES public.roba (roba_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE NO ACTION')");

		DB::statement("SELECT addtable('public','radni_nalog_trosak','
			radni_nalog_trosak_id bigserial NOT NULL,
			radni_nalog_id bigint NOT NULL,
			partner_id bigint NOT NULL,
			datum_racuna date NOT NULL,
			vrednost_racuna numeric(20,2) NOT NULL DEFAULT 0,
			broj_racuna character varying(255),
			zaduzuje_kupca smallint,
			opis character varying(500),
			CONSTRAINT radni_nalog_trosak_pkey PRIMARY KEY (radni_nalog_trosak_id),
			CONSTRAINT radni_nalog_trosak_fk FOREIGN KEY (radni_nalog_id)
			  REFERENCES public.radni_nalog (radni_nalog_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE CASCADE,
			CONSTRAINT radni_nalog_trosak_fk1 FOREIGN KEY (partner_id)
			  REFERENCES public.partner (partner_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE NO ACTION
			  ')");   

		DB::statement("SELECT addtable('public','radni_nalog_rezervni_deo','
			radni_nalog_rezervni_deo_id bigserial NOT NULL,
			radni_nalog_id bigint NOT NULL,
			roba_id bigint NOT NULL,
			kolicina integer NOT NULL DEFAULT 0,
			cena numeric(20,2) NOT NULL DEFAULT 0,
			iznos numeric(20,2) NOT NULL DEFAULT 0,
			CONSTRAINT radni_nalog_rezervni_deo_pkey PRIMARY KEY (radni_nalog_rezervni_deo_id),
			CONSTRAINT radni_nalog_rezervni_deo_fk FOREIGN KEY (radni_nalog_id)
			  REFERENCES public.radni_nalog (radni_nalog_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE CASCADE,
			CONSTRAINT radni_nalog_rezervni_deo_fk1 FOREIGN KEY (roba_id)
			  REFERENCES public.roba (roba_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE NO ACTION
			  ')");

		DB::statement("SELECT addtable('public','radni_nalog_trosak_dokument','
		  radni_nalog_trosak_dokument_id bigserial NOT NULL,
		  radni_nalog_trosak_id bigint NOT NULL,
		  radni_nalog_trosak_broj_dokumenta character varying(20) NOT NULL,
		  uput smallint NOT NULL DEFAULT 1,
		  datum_dokumenta date NOT NULL,
		  serijski_broj character varying(255),
		  uredjaj character varying(500),
		  opis_kvara character varying(1000),
		  napomena character varying(1000),
		  CONSTRAINT radni_nalog_trosak_dokument_pkey PRIMARY KEY (radni_nalog_trosak_dokument_id),
		  CONSTRAINT radni_nalog_trosak_dokument_fk1 FOREIGN KEY (radni_nalog_trosak_id)
		      REFERENCES public.radni_nalog_trosak (radni_nalog_trosak_id) MATCH SIMPLE
		      ON UPDATE CASCADE ON DELETE NO ACTION')");

		DB::statement("SELECT addtable('public','vaucer','vaucer_id bigserial NOT NULL, vaucer_broj text, iznos integer, datum_od date, datum_do date, realizovan smallint, web_b2c_narudzbina_id integer DEFAULT NULL, web_kupac_id integer DEFAULT NULL, CONSTRAINT web_b2c_narudzbina_id_fkey FOREIGN KEY (web_b2c_narudzbina_id) REFERENCES public.web_b2c_narudzbina (web_b2c_narudzbina_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT web_kupac_id_fkey FOREIGN KEY (web_kupac_id) REFERENCES public.web_kupac (web_kupac_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

		DB::statement("SELECT addcol('public','vaucer','vaucer_tip_id','integer DEFAULT -1')");
		DB::statement("SELECT addcol('public','vaucer','naziv','character varying(255)')");
		DB::statement("SELECT addcol('public','vaucer','grupa_pr_id','integer DEFAULT 0')");

		DB::statement("SELECT addtable('public','vaucer_tip','vaucer_tip_id integer, vaucer_tip character varying(255)')");

		DB::statement("INSERT INTO radni_nalog_status (radni_nalog_status_id,naziv) SELECT * FROM (VALUES 
	    	(1,'Na servisu'),
	    	(2,'Uslužni servis'),
	    	(3,'Vraćeni'),
	    	(4,'Realizovani')
	    ) redovi(radni_nalog_status_id,naziv) WHERE radni_nalog_status_id NOT IN (SELECT radni_nalog_status_id FROM radni_nalog_status)");

		DB::statement("SELECT addcol('public','web_nacin_placanja','b2c_default','smallint DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_nacin_placanja','b2b_default','smallint DEFAULT 0')");
/****************************************************************************************************************************/

		// // DB::statement("UPDATE dobavljac_cenovnik SET flag_opis_postoji = 0 WHERE opis = ''");
		// // DB::statement("UPDATE dobavljac_cenovnik SET flag_opis_postoji = 1 WHERE opis != ''");

		// // DB::statement("ALTER TABLE baneri ALTER COLUMN web_b2c_seo_id TYPE integer USING CAST(web_b2c_seo_id AS integer)");
		// // DB::statement("ALTER TABLE baneri_b2b ALTER COLUMN web_b2b_seo_id TYPE integer USING CAST(web_b2b_seo_id AS integer)");


		// DB::statement("UPDATE web_b2c_seo SET naziv_stranice = 'blog', title = 'Blog', seo_title = 'Blog' WHERE naziv_stranice = 'vesti'");
		// DB::statement("UPDATE web_b2c_seo SET naziv_stranice = 'prijava', title = 'Prijava' WHERE naziv_stranice = 'login'");
		// DB::statement("UPDATE web_b2c_seo SET seo_title = 'Akcija' WHERE naziv_stranice = 'akcija'");
		// DB::statement("UPDATE web_b2c_seo SET seo_title = 'Lista svih artikala' WHERE naziv_stranice = 'pocetna'");
		// DB::statement("UPDATE web_b2c_seo SET disable = 0 WHERE naziv_stranice = 'sve-o-kupovini'");

		// DB::statement("UPDATE web_nacin_isporuke SET naziv = 'Preuzimam lično' WHERE naziv = 'Preuzimam licno'");
		// DB::statement("UPDATE web_nacin_placanja SET naziv = 'Uplatnicom preko žiro računa' WHERE naziv = 'Uplatnicom preko ziro računa'");
		// DB::statement("UPDATE web_nacin_placanja SET naziv = 'Pouzećem u gotovini pri prijemu robe' WHERE naziv = 'Pouzecem u gotovini pri prijemu robe'");

		DB::statement("SELECT addtable('public','cena_isporuke','cena_id integer, cena_do numeric(20,2) DEFAULT 0, cena numeric(20,2) DEFAULT 0')");
		DB::statement("INSERT INTO cena_isporuke (cena_id,cena_do,cena) SELECT * FROM (VALUES 
	    	(1,0,0)
	    ) redovi(cena_id,cena_do,cena) WHERE cena_id NOT IN (SELECT cena_id FROM cena_isporuke)");

		// $prodavnica_stil = DB::select("SELECT prodavnica_stil_id FROM prodavnica_stil WHERE izabrana = 1");
		// if(isset($prodavnica_stil[0])){
		// 	if(in_array($prodavnica_stil[0]->prodavnica_stil_id, array(1,2,4,5,9))){
		// 		DB::statement("UPDATE prodavnica_stil SET izabrana = 0");
		// 		DB::statement("UPDATE prodavnica_stil SET izabrana = 1 WHERE prodavnica_stil_id = 11");
		// 	}else if(in_array($prodavnica_stil[0]->prodavnica_stil_id, array(3,6))){
		// 		DB::statement("UPDATE prodavnica_stil SET izabrana = 0");
		// 		DB::statement("UPDATE prodavnica_stil SET izabrana = 1 WHERE prodavnica_stil_id = 12");
		// 	}else if(in_array($prodavnica_stil[0]->prodavnica_stil_id, array(7,8))){
		// 		DB::statement("UPDATE prodavnica_stil SET izabrana = 0");
		// 		DB::statement("UPDATE prodavnica_stil SET izabrana = 1 WHERE prodavnica_stil_id = 10");
		// 	}
		// }

		// DB::statement("SELECT dropcol('public','partner','id_kategorije')");
		// DB::statement("SELECT addcol('public','partner','id_kategorije','integer DEFAULT NULL')");

		// DB::statement("UPDATE web_options SET naziv = 'Korisnik mora da uradi potvrdu registracije' WHERE web_options_id = 100");
		// DB::statement("UPDATE web_options SET naziv = 'Broj proizvoda na akciji' WHERE web_options_id = 101");
		
		// DB::statement("UPDATE options SET naziv = 'Chat' WHERE options_id = 3020");

		// DB::statement("UPDATE posta_slanje SET difolt = 1 WHERE posta_slanje_id = 3");

		//DB::statement("DELETE FROM options WHERE options_id IN (1340,3014,3015,3016,3017,3027,3031)");

		DB::statement("SELECT addtable('public','definisane_marze','definisane_marze_id bigserial NOT NULL, web_marza bigint, mp_marza bigint, nc numeric(20,2) DEFAULT 0,CONSTRAINT definisane_marze_pkey PRIMARY KEY (definisane_marze_id)')");
		
		DB::statement("SELECT addcol('public','partner_grupa','definisane_marze','smallint DEFAULT 0')");

		DB::statement("create or replace function definisana_web_marza(nabavna_cena numeric)
returns numeric 
language 'plpgsql'
as 
$$
declare 
    web_marza numeric ;
    def_marza definisane_marze%rowtype;
begin 
	web_marza := 0;

    FOR def_marza IN SELECT * FROM definisane_marze ORDER BY definisane_marze_id ASC
    LOOP
		IF nabavna_cena < def_marza.nc THEN
        	web_marza := def_marza.web_marza;
		END IF;

		EXIT WHEN nabavna_cena < def_marza.nc;
    END LOOP;


return web_marza;
end;
$$");

		DB::statement("create or replace function definisana_mp_marza(nabavna_cena numeric)
returns numeric 
language 'plpgsql'
as 
$$
declare 
    mp_marza numeric ;
    def_marza definisane_marze%rowtype;
begin 
	mp_marza := 0;

    FOR def_marza IN SELECT * FROM definisane_marze ORDER BY definisane_marze_id ASC
    LOOP
		IF nabavna_cena < def_marza.nc THEN
        	mp_marza := def_marza.mp_marza;
		END IF;

		EXIT WHEN nabavna_cena < def_marza.nc;
    END LOOP;


return mp_marza;
end;
$$");

		DB::statement("SELECT addcol('public','roba','produzena_garancija','smallint DEFAULT 0')");
		
		DB::statement("SELECT addcol('public','web_kupac','bodovi','integer DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_kupac','bodovi_aktivno','smallint DEFAULT 0')");


		DB::statement("SELECT addcol('public','web_b2b_narudzbina','avans','numeric(20,4) DEFAULT 0')");
		DB::statement("SELECT addcol('public','web_b2b_narudzbina','komentar','character varying')");
		DB::statement("SELECT addcol('public','baneri_b2b','mesto_prikaza','integer DEFAULT 0')");

	    DB::statement("SELECT addtable('public','produzene_garancije','produzene_garancije_id bigserial NOT NULL, ime character varying(255), prezime character varying(255), adresa character varying(255), grad character varying(100), email character varying(255), telefon character varying(255), proizvodjac_id integer,roba_id integer, serijski_broj character varying(255),maloprodaja character varying(255),prodavnica_mesto character varying(255),fiskalni character varying(255),datum_izdavanja date,parent_id integer, datum_kreiranja timestamp without time zone NOT NULL, CONSTRAINT produzene_garancije_pkey PRIMARY KEY (produzene_garancije_id), CONSTRAINT produzene_garancije_roba_id_fkey FOREIGN KEY (roba_id) REFERENCES roba (roba_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT produzene_garancije_proizvodjac_id_fkey FOREIGN KEY (proizvodjac_id) REFERENCES proizvodjac (proizvodjac_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT produzene_garancije_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES produzene_garancije (produzene_garancije_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

		DB::statement("create or replace function get_final_parent_date_created(id integer)
returns timestamp 
language 'plpgsql'
as 
$$
declare 
    created timestamp ;
    parent_id integer ;
    datum_kreiranja timestamp ;
begin 
	
	execute 'select parent_id, datum_kreiranja from produzene_garancije  where  produzene_garancije_id = ' || id
      into   parent_id, datum_kreiranja ;

      if(parent_id is null ) then 
           created := datum_kreiranja;
      else
          created := get_final_parent_date_created(parent_id);
      end if;


return created;
end;
$$");


	DB::statement("SELECT addcol('public','web_b2c_seo','redirect_web_b2c_seo_id','integer DEFAULT null')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'redirect_web_b2c_seo_id_fkey'")) == 0){
		DB::statement("ALTER TABLE web_b2c_seo ADD CONSTRAINT redirect_web_b2c_seo_id_fkey FOREIGN KEY (redirect_web_b2c_seo_id) REFERENCES public.web_b2c_seo (web_b2c_seo_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE");
	}
	DB::statement("SELECT addcol('public','web_b2b_seo','redirect_web_b2b_seo_id','integer DEFAULT null')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'redirect_web_b2b_seo_id_fkey'")) == 0){
		DB::statement("ALTER TABLE web_b2b_seo ADD CONSTRAINT redirect_web_b2b_seo_id_fkey FOREIGN KEY (redirect_web_b2b_seo_id) REFERENCES public.web_b2b_seo (web_b2b_seo_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE");
	}
	DB::statement("SELECT addcol('public','proizvodjac','produzena_garancija','smallint DEFAULT 0')");

	DB::statement("SELECT addcol('public','web_slika','parent_id','integer DEFAULT null')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'web_slika_pkey'")) == 0){
		DB::statement("ALTER TABLE web_slika ADD CONSTRAINT web_slika_pkey PRIMARY KEY (web_slika_id)");
	}
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'web_slika_parent_id_fkey'")) == 0){
		DB::statement("ALTER TABLE web_slika ADD CONSTRAINT web_slika_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.web_slika (web_slika_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE");
	}
	
	DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','posta_paket','integer')");

	DB::statement("SELECT addcol('public','posta_slanje','auth_code','character varying(255)')");
	DB::statement("UPDATE posta_slanje SET auth_code = '419AFE8D-15D9-4750-B4FC-9BE3851560CA' WHERE posta_slanje_id = 3");
	DB::statement("SELECT addcol('public','roba','labela', 'integer DEFAULT NULL')");
	DB::statement("SELECT addtable('public','labela_artikla','labela_artikla_id integer,naziv character varying(255),grupa_pr_id integer,active smallint,rbr smallint')");
	DB::statement("SELECT addcol('public','labela_artikla','slika', 'character varying(255)')");
	DB::statement("SELECT dropcol('public','labela_artikla','slika')");
	DB::statement("SELECT addcol('public','labela_artikla','slika_desno', 'character varying(255)')");
	DB::statement("SELECT addcol('public','labela_artikla','slika_levo', 'character varying(255)')");
	DB::statement("SELECT addcol('public','roba_flag_cene','prikaz_cene', 'smallint DEFAULT 1')");

	DB::statement("SELECT addtable('public','srodni_artikli','
	  srodni_artikli_id bigserial NOT NULL,
	  roba_id bigint NOT NULL,
	  srodni_roba_id bigint NOT NULL,
	  flag_cena smallint,
	  grupa_pr_vrednost_id bigint,
	  CONSTRAINT srodni_artikli_pkey PRIMARY KEY (srodni_artikli_id),
	  CONSTRAINT srodni_artikli_fk1 FOREIGN KEY (roba_id)
	      REFERENCES public.roba (roba_id) MATCH SIMPLE
	      ON UPDATE CASCADE ON DELETE NO ACTION,
	  CONSTRAINT srodni_artikli_fk2 FOREIGN KEY (srodni_roba_id)
	      REFERENCES public.roba (roba_id) MATCH SIMPLE
	      ON UPDATE CASCADE ON DELETE NO ACTION,
	  CONSTRAINT srodni_artikli_fk3 FOREIGN KEY (grupa_pr_vrednost_id)
	      REFERENCES public.grupa_pr_vrednost (grupa_pr_vrednost_id) MATCH SIMPLE
	      ON UPDATE CASCADE ON DELETE NO ACTION
		')");

	DB::statement("SELECT addcol('public','web_b2c_korpa_stavka','parent_vezani_roba_id','integer DEFAULT null')");

	DB::statement("SELECT addcol('public','jezik','admin_izabrani','smallint DEFAULT 0')");
    DB::statement("SELECT addtable('public','admin_prevodilac','admin_prevodilac_id bigserial NOT NULL, izabrani_jezik_id integer, izabrani_jezik_reci character varying(1000), jezik_id integer, reci character varying(1000)')");

    DB::statement("SELECT addcol('public','radni_nalog','uzrok_kvara','character varying(500)')");
    DB::statement("SELECT addcol('public','radni_nalog','otpis','smallint DEFAULT 0')");
    DB::statement("SELECT addcol('public','radni_nalog','ostecen','smallint DEFAULT 0')");
    DB::statement("SELECT addcol('public','radni_nalog','trajanje_popravke','character varying(255)')");
    DB::statement("SELECT addcol('public','radni_nalog','predjeni_kilometri','character varying(255)')");
    DB::statement("SELECT addcol('public','radni_nalog','u_garanciji','smallint DEFAULT 0')");

    DB::statement("SELECT addcol('public','tip_artikla','prikaz','smallint DEFAULT 0')");

    DB::statement("SELECT addcol('public','web_b2c_narudzbina','trosak_isporuke','numeric')");


// 		DB::statement("create or replace function get_main_grupa_pr_id(id integer)
// returns integer 
// language 'plpgsql'
// as 
// $$
// declare 
// 	main_id integer ;
//     parrent_grupa_pr_id integer ;
// begin 
	
// 	execute 'select parrent_grupa_pr_id from grupa_pr where grupa_pr_id = ' || id
//       into  parrent_grupa_pr_id ;

//       if(parrent_grupa_pr_id  > 0) then 
//           main_id := get_main_grupa_pr_id(parrent_grupa_pr_id);
//       else
//           main_id := id;
//       end if;


// return main_id;
// end;
// $$");

// 		DB::statement("create or replace function new_cena_nc(racunska_cena_a numeric, b2b_akcijski_rabat numeric)
// returns numeric 
// language 'plpgsql'
// as 
// $$
// declare 
// 	cena_nc numeric ;
// begin 
//       if(b2b_akcijski_rabat = 0) then 
//           cena_nc := racunska_cena_a * 0.88;
//       else
//           cena_nc := racunska_cena_a * (1 - b2b_akcijski_rabat/100);
//       end if;


// return cena_nc;
// end;
// $$");

    DB::statement("ALTER TABLE roba ALTER COLUMN sku TYPE character(255)");

    DB::statement("SELECT addcol('public','informacioni_sistem','pokrenut','smallint DEFAULT 0')");

    DB::statement("SELECT addcol('public','proizvodjac','produzena_garancija_slika','character varying(500)')");
    DB::statement("SELECT addcol('public','proizvodjac','produzena_garancija_link','character varying(500)')");	

    DB::statement("SELECT addcol('public','dobavljac_cenovnik','sifra_is','character varying(255)')");

    DB::statement("SELECT addcol('public','partner','api_username','character varying(255)')");
	DB::statement("SELECT addcol('public','partner','api_password','character varying(255)')");
	DB::statement("SELECT addcol('public','partner','aktivan_api','smallint DEFAULT 0')");

	DB::statement("UPDATE web_nacin_placanja SET naziv='Visa, Master, Maestro, Dina ili AMEX karticom' WHERE web_nacin_placanja_id = 3");
	DB::statement("SELECT addcol('public','baneri_jezik','link','character varying(500)')");

	if(count(DB::select("SELECT column_name FROM information_schema.columns WHERE table_name='roba' and column_name='grupa_pr_id_kupindo'")) > 0){
		DB::statement("ALTER TABLE roba RENAME COLUMN grupa_pr_id_kupindo TO rbr");
		DB::statement("UPDATE roba SET rbr = NULL");
	}

	if(count(DB::select("SELECT column_name FROM information_schema.columns WHERE table_name='roba' and column_name='akcija_stara_cena'")) > 0){
        DB::statement("ALTER TABLE roba RENAME COLUMN akcija_stara_cena TO stara_cena");
    } 

	DB::statement("SELECT addcol('public','web_troskovi_isporuke','cena_do','numeric(22,2) DEFAULT 0.00')");

	DB::statement("DELETE FROM partner_vrsta WHERE partner_vrsta_id IN (0,2,4,5,6,7,8,9,10,20,101,103,104,105,114,116)");

	if(count(DB::select("SELECT column_name FROM information_schema.columns WHERE table_name='roba' and column_name='flag_promena_connect'")) > 0){
        DB::statement("ALTER TABLE roba RENAME COLUMN flag_promena_connect TO b2b_akcija_flag_primeni");
    }

    if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'osobina_vrednost_pkey'")) == 0){
	    DB::statement("ALTER TABLE osobina_vrednost ADD PRIMARY KEY (osobina_vrednost_id)");
	}

	DB::statement("SELECT addtable('public','osobina_kombinacija',
		'osobina_kombinacija_id bigserial NOT NULL,
		roba_id bigint NOT NULL,
		kolicina integer DEFAULT 0,
		cena numeric(20,2) DEFAULT 0,
		aktivna integer DEFAULT 1,
	  CONSTRAINT osobina_kombinacija_pkey PRIMARY KEY (osobina_kombinacija_id),
	  CONSTRAINT osobina_kombinacija_fk1 FOREIGN KEY (roba_id) REFERENCES roba (roba_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
	
	DB::statement("SELECT addcol('public','osobina_kombinacija','sifra','character varying(255)')");

	DB::statement("SELECT addtable('public','osobina_kombinacija_vrednost',
		'osobina_kombinacija_id bigint NOT NULL,
		osobina_vrednost_id bigint NOT NULL,
		CONSTRAINT osobina_kombinacija_vrednost_fk1 FOREIGN KEY (osobina_kombinacija_id) REFERENCES osobina_kombinacija (osobina_kombinacija_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
		CONSTRAINT osobina_kombinacija_vrednost_fk2 FOREIGN KEY (osobina_vrednost_id) REFERENCES osobina_vrednost (osobina_vrednost_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	DB::statement("SELECT addtable('public','kupovina_na_rate','rata_id integer DEFAULT 0, br_meseca integer DEFAULT 0, kamata numeric')");
	DB::statement("INSERT INTO kupovina_na_rate (rata_id,br_meseca,kamata) SELECT * FROM (VALUES 
	    	(0,0,0)) 
	    	redovi(rata_id,br_meseca,kamata) 
	        WHERE rata_id NOT IN (SELECT rata_id FROM kupovina_na_rate)");
	
	DB::statement("SELECT addcol('public','web_b2c_korpa_stavka','br_rata','numeric DEFAULT 0')");

	DB::statement("SELECT addtable('public','partner_korisnik','
	  partner_korisnik_id bigserial NOT NULL,
	  partner_id bigint NOT NULL,
	  korisnicko_ime character varying(255) NOT NULL,
	  lozinka character varying(255) NOT NULL,
	  naziv character varying(500) NOT NULL,
	  adresa character varying(255) NOT NULL,
	  mesto character varying(255) NOT NULL,
	  telefon character varying(255) NOT NULL,
	  email character varying(255),
	  aktivan smallint DEFAULT 1,
	  CONSTRAINT partner_korisnik_pkey PRIMARY KEY (partner_korisnik_id),
	  CONSTRAINT partner_korisnik_fk1 FOREIGN KEY (partner_id)
	      REFERENCES public.partner (partner_id) MATCH SIMPLE
	      ON UPDATE CASCADE ON DELETE NO ACTION
		')");

	if(count(DB::select("SELECT column_name FROM information_schema.columns WHERE table_name='web_b2b_narudzbina' and column_name='flag_promena_connect'")) > 0){
        DB::statement("ALTER TABLE web_b2b_narudzbina RENAME COLUMN flag_promena_connect TO partner_korisnik_id");
        DB::statement("ALTER TABLE web_b2b_narudzbina ALTER COLUMN partner_korisnik_id SET DEFAULT (NULL)::integer");
        DB::statement("UPDATE web_b2b_narudzbina SET partner_korisnik_id = (NULL)::integer");
		DB::statement("ALTER TABLE web_b2b_narudzbina ADD CONSTRAINT web_b2b_narudzbina_fk5 FOREIGN KEY (partner_korisnik_id) REFERENCES public.partner_korisnik (partner_korisnik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL");
    }

    DB::statement("SELECT addcol('public','valuta','izabran','smallint DEFAULT 0')");
	if(count(DB::select("SELECT * FROM valuta WHERE izabran = 1")) == 0){
		DB::statement("UPDATE valuta SET izabran = 1 WHERE valuta_id = 1");
    }

    DB::statement("ALTER TABLE partner ALTER COLUMN sifra TYPE character varying(255)");

    DB::statement("SELECT addcol('public','roba','b2b_akcija_redni_broj','smallint DEFAULT 0')");
    DB::statement("SELECT addcol('public','roba','b2b_datum_akcije_od','date')");
    DB::statement("SELECT addcol('public','roba','b2b_datum_akcije_do','date')");
    DB::statement("SELECT addcol('public','roba','b2b_akcijska_cena','numeric(22,2) DEFAULT 0.00')");
    DB::statement("SELECT addcol('public','roba','b2b_akcija_popust','numeric(22,2) DEFAULT 0.00')");

    DB::statement("SELECT addcol('public','web_kupac','maticni_br','numeric')");
    

    DB::statement("SELECT addtable('public','katalog_vrsta','
		  katalog_vrsta_id bigserial NOT NULL,
		  naziv character varying(255) NOT NULL,
		  aktivan smallint DEFAULT 1,		 
		  CONSTRAINT katalog_vrsta_pkey PRIMARY KEY (katalog_vrsta_id)	  	  
			')");
	DB::statement("INSERT INTO katalog_vrsta (katalog_vrsta_id,naziv,aktivan) SELECT * FROM (VALUES 
	    	(1,'Grupe',1),
	    	(2,'Tipovi',1),
	    	(3,'Karakteristike',1))
	    	redovi(katalog_vrsta_id,naziv,aktivan) 
	        WHERE katalog_vrsta_id NOT IN (SELECT katalog_vrsta_id FROM katalog_vrsta)");

    DB::statement("SELECT addtable('public','katalog','
		  katalog_id bigserial NOT NULL,
		  naziv character varying(255) NOT NULL,
		  aktivan smallint DEFAULT 1,
		  vazi_od date,
		  sort character varying(255) NOT NULL,
		  katalog_vrsta_id bigint NOT NULL,		  
		  CONSTRAINT katalog_pkey PRIMARY KEY (katalog_id),
	  	  CONSTRAINT katalog_vrsta_fk1 FOREIGN KEY (katalog_vrsta_id)
	      REFERENCES public.katalog_vrsta (katalog_vrsta_id) MATCH SIMPLE
	      ON UPDATE CASCADE ON DELETE NO ACTION
			')");

    DB::statement("SELECT addtable('public','katalog_roba',
		'katalog_id bigint NOT NULL,
		roba_id bigint NOT NULL,
		CONSTRAINT katalog_roba_fk1 FOREIGN KEY (katalog_id) REFERENCES katalog (katalog_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
		CONSTRAINT katalog_roba_fk2 FOREIGN KEY (roba_id) REFERENCES roba (roba_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

    DB::statement("SELECT addtable('public','katalog_polja','
		  katalog_polja_id bigserial NOT NULL,
		  katalog_id bigint NOT NULL,
		  naziv character varying(255) NOT NULL,
		  title character varying(255) NOT NULL,
		  rbr integer DEFAULT 0, 
		  CONSTRAINT katalog_polja_pkey PRIMARY KEY (katalog_polja_id),
	  	  CONSTRAINT katalog_polja_fk1 FOREIGN KEY (katalog_id)
	      REFERENCES public.katalog (katalog_id) MATCH SIMPLE
	      ON UPDATE CASCADE ON DELETE CASCADE
			')");
    DB::statement("SELECT addtable('public','katalog_grupe_sort',
		'katalog_id bigint NOT NULL,
		grupa_pr_id bigint NOT NULL,
		grupa_pr_naziv_id bigint NOT NULL,
		CONSTRAINT katalog_grupe_sort_fk1 FOREIGN KEY (katalog_id) REFERENCES katalog (katalog_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
		CONSTRAINT katalog_grupe_sort_fk2 FOREIGN KEY (grupa_pr_id) REFERENCES grupa_pr (grupa_pr_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
		CONSTRAINT katalog_grupe_sort_fk3 FOREIGN KEY (grupa_pr_naziv_id) REFERENCES grupa_pr_naziv (grupa_pr_naziv_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

    //ponuda
	DB::statement("SELECT addtable('public','dokumenti_status','dokumenti_status_id bigserial NOT NULL,naziv character varying(255) NOT NULL,aktivan smallint DEFAULT 1,CONSTRAINT dokumenti_status_pkey PRIMARY KEY (dokumenti_status_id)')");
	DB::statement("INSERT INTO dokumenti_status (dokumenti_status_id,naziv,aktivan) SELECT * FROM (VALUES 
    	(1,'Poslato',1),
    	(2,'Realizovano',1)
    ) redovi(dokumenti_status_id,naziv,aktivan) WHERE dokumenti_status_id NOT IN (SELECT dokumenti_status_id FROM dokumenti_status)");

	DB::statement("SELECT addtable('public','ponuda','
		ponuda_id bigserial NOT NULL,
		broj_dokumenta character varying(20) NOT NULL,
		datum_ponude date,
		vazi_do date,
		rok_isporuke date,
		partner_id integer,
		vrsta_dokumenta_id smallint,
		iznos numeric(20,2) DEFAULT 0,
		nacin_placanja character varying(255) DEFAULT NULL,
		predracun smallint DEFAULT 0,
		tekst text,
		tekst_footer text,
		dokumenti_status_id bigint DEFAULT null,
		u_korpu smallint DEFAULT 0,
		CONSTRAINT ponuda_pkey PRIMARY KEY (ponuda_id),
		CONSTRAINT ponuda_fk1 FOREIGN KEY (vrsta_dokumenta_id) 
			REFERENCES vrsta_dokumenta (vrsta_dokumenta_id) MATCH SIMPLE 
			ON UPDATE NO ACTION ON DELETE NO ACTION,
		CONSTRAINT ponuda_partner_id_fkey FOREIGN KEY (partner_id) 
			REFERENCES partner (partner_id) MATCH SIMPLE 
			ON UPDATE NO ACTION ON DELETE NO ACTION,
		CONSTRAINT dokumenti_status_id_fkey FOREIGN KEY (dokumenti_status_id) 
			REFERENCES dokumenti_status (dokumenti_status_id) MATCH SIMPLE 
			ON UPDATE NO ACTION ON DELETE NO ACTION')");
	DB::statement("SELECT addtable('public','ponuda_stavka','
		ponuda_stavka_id bigserial NOT NULL,
		ponuda_id bigint NOT NULL,
		broj_stavke integer,
		naziv_stavke character varying(300) NOT NULL,
		roba_id integer,
		nab_cena numeric(20,4),
		rabat real DEFAULT 0,
		pdv real,
		pcena numeric(20,4),
		kolicina real,
		kolicina_narudzbine real,
		opis_robe character varying(300),
		CONSTRAINT ponuda_stavka_pkey PRIMARY KEY (ponuda_stavka_id),
		CONSTRAINT ponuda_stavka_ponuda_id_fkey FOREIGN KEY (ponuda_id) 
			REFERENCES ponuda (ponuda_id) MATCH SIMPLE 
			ON UPDATE CASCADE ON DELETE CASCADE')");

	DB::statement("SELECT addcol('public','racun','rok_placanja','date')");
	DB::statement("SELECT addcol('public','racun','ponuda_id','bigint DEFAULT null')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'ponuda_id_fkey'")) == 0){
		DB::statement("ALTER TABLE racun ADD CONSTRAINT ponuda_id_fkey FOREIGN KEY (ponuda_id) REFERENCES public.ponuda (ponuda_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL");
	}

	DB::statement("SELECT addtable('public','dokument_podesavanja','
		dokument_podesavanja_id bigserial NOT NULL,
		partner_id bigint DEFAULT (NULL)::integer,
		admin smallint DEFAULT 0,
		logo character varying(255),
		sablon text,
		footer text,
		CONSTRAINT dokument_podesavanja_pkey PRIMARY KEY (dokument_podesavanja_id),
		CONSTRAINT dokument_podesavanja_partner_id_fkey FOREIGN KEY (partner_id) 
			REFERENCES partner (partner_id) MATCH SIMPLE 
			ON UPDATE CASCADE ON DELETE NO ACTION')");

	DB::statement("SELECT addcol('public','partner','parent_id','integer DEFAULT null')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'parent_id_fkey'")) == 0){
		DB::statement("ALTER TABLE partner ADD CONSTRAINT parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL");
	}

	DB::statement("SELECT addcol('public','racun','dokumenti_status_id','bigint DEFAULT null')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'dokumenti_status_id_fkey'")) == 0){
		DB::statement("ALTER TABLE racun ADD CONSTRAINT dokumenti_status_id_fkey FOREIGN KEY (dokumenti_status_id) REFERENCES public.dokumenti_status (dokumenti_status_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL");
	}
	DB::statement("SELECT addcol('public','racun_stavka','naziv_stavke','character varying(300)')");

	DB::statement("SELECT addtable('public','predracun','
		predracun_id bigserial NOT NULL,
		broj_dokumenta character varying(20) NOT NULL,
		datum_predracuna date,
		vazi_do date,
		rok_isporuke date,
		partner_id integer,
		vrsta_dokumenta_id smallint,
		iznos numeric(20,2) DEFAULT 0,
		nacin_placanja character varying(255) DEFAULT NULL,
		tekst text,
		tekst_footer text,
		dokumenti_status_id bigint DEFAULT null,
		ponuda_id bigint DEFAULT null,
		CONSTRAINT predracun_pkey PRIMARY KEY (predracun_id),
		CONSTRAINT predracun_fk1 FOREIGN KEY (vrsta_dokumenta_id) 
			REFERENCES vrsta_dokumenta (vrsta_dokumenta_id) MATCH SIMPLE 
			ON UPDATE NO ACTION ON DELETE NO ACTION,
		CONSTRAINT predracun_partner_id_fkey FOREIGN KEY (partner_id) 
			REFERENCES partner (partner_id) MATCH SIMPLE 
			ON UPDATE NO ACTION ON DELETE NO ACTION,
		CONSTRAINT dokumenti_status_id_fkey FOREIGN KEY (dokumenti_status_id) 
			REFERENCES dokumenti_status (dokumenti_status_id) MATCH SIMPLE 
			ON UPDATE NO ACTION ON DELETE NO ACTION,
		CONSTRAINT ponuda_id_fkey FOREIGN KEY (ponuda_id) 
			REFERENCES public.ponuda (ponuda_id) MATCH SIMPLE 
			ON UPDATE CASCADE ON DELETE SET NULL')");
	DB::statement("SELECT addtable('public','predracun_stavka','
		predracun_stavka_id bigserial NOT NULL,
		predracun_id bigint NOT NULL,
		broj_stavke integer,
		naziv_stavke character varying(300) NOT NULL,
		roba_id integer,
		nab_cena numeric(20,4),
		pdv real,
		pcena numeric(20,4),
		kolicina real,
		opis_robe character varying(300),
		CONSTRAINT predracun_stavka_pkey PRIMARY KEY (predracun_stavka_id),
		CONSTRAINT predracun_stavka_predracun_id_fkey FOREIGN KEY (predracun_id) 
			REFERENCES predracun (predracun_id) MATCH SIMPLE 
			ON UPDATE CASCADE ON DELETE CASCADE')");

	DB::statement("SELECT addcol('public','racun','predracun_id','bigint DEFAULT null')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'predracun_id_fkey'")) == 0){
		DB::statement("ALTER TABLE racun ADD CONSTRAINT predracun_id_fkey FOREIGN KEY (predracun_id) REFERENCES public.predracun (predracun_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL");
	}
	DB::statement("SELECT addcol('public','racun','rok_isporuke','date')");
	DB::statement("SELECT addcol('public','racun','nacin_placanja','character varying(255) DEFAULT NULL')");

	DB::statement("SELECT addcol('public','roba_flag_cene','b2b_selected','smallint DEFAULT 1')");


	DB::statement("SELECT addcol('public','narudzbina_mesto','posta_slanje_id','bigint')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'narudzbina_mesto_posta_slanje_id_fkey'")) == 0){
		DB::statement("ALTER TABLE narudzbina_mesto ADD CONSTRAINT narudzbina_mesto_posta_slanje_id_fkey FOREIGN KEY (posta_slanje_id) REFERENCES public.posta_slanje (posta_slanje_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL");
	}
	DB::statement("SELECT addcol('public','narudzbina_opstina','posta_slanje_id','bigint')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'narudzbina_opstina_posta_slanje_id_fkey'")) == 0){
		DB::statement("ALTER TABLE narudzbina_opstina ADD CONSTRAINT narudzbina_opstina_posta_slanje_id_fkey FOREIGN KEY (posta_slanje_id) REFERENCES public.posta_slanje (posta_slanje_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL");
	}

	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'narudzbina_mesto_code_fkey'")) > 0){
	DB::statement("ALTER TABLE narudzbina_ulica DROP CONSTRAINT narudzbina_mesto_code_fkey");
	}

	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'narudzbina_opstina_code_fkey'")) > 0){
	DB::statement("ALTER TABLE narudzbina_mesto DROP CONSTRAINT narudzbina_opstina_code_fkey");
	}

	DB::statement("ALTER TABLE narudzbina_mesto ALTER COLUMN code TYPE character varying(255)");
	DB::statement("ALTER TABLE narudzbina_mesto ALTER COLUMN narudzbina_opstina_code TYPE character varying(255)");
	DB::statement("ALTER TABLE narudzbina_opstina ALTER COLUMN code TYPE character varying(255)");
	

	DB::statement("SELECT addtable('public','posta_zahtev_api','
			posta_zahtev_api_id bigserial NOT NULL,
			web_b2c_narudzbina_id bigint NOT NULL,
			posta_slanje_id bigint NOT NULL,
			posiljalac_id bigint NOT NULL,
			posiljalac_ptt character varying(255) DEFAULT NULL,
			primalac_id bigint NOT NULL,
			primalac_ptt character varying(255) DEFAULT NULL,
			vreme_dostave integer NOT NULL DEFAULT 100,
			tip_obavestenja_id integer NOT NULL DEFAULT 0,
			dostava_subotom boolean DEFAULT false,
			opis character varying(255),
			napomena character varying(150),
			masa numeric(20,4) DEFAULT 0,
			posta_odgovor_id character varying(255),
			labela text,	
			dokument text,
			otkupnina integer,
			cena_otkupa numeric(20,2) DEFAULT 0,	
			CONSTRAINT posta_zahtev_api_pkey PRIMARY KEY (posta_zahtev_api_id),
			CONSTRAINT posta_zahtev_api_fk FOREIGN KEY (web_b2c_narudzbina_id)
			  REFERENCES public.web_b2c_narudzbina (web_b2c_narudzbina_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE CASCADE,
			CONSTRAINT posta_zahtev_api_fk1 FOREIGN KEY (posta_slanje_id)
			  REFERENCES public.posta_slanje (posta_slanje_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE CASCADE,
			CONSTRAINT posta_zahtev_api_fk2 FOREIGN KEY (posiljalac_id)
			  REFERENCES public.partner (partner_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE CASCADE,  
			CONSTRAINT posta_zahtev_api_fk3 FOREIGN KEY (primalac_id)
			  REFERENCES public.web_kupac (web_kupac_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE NO ACTION 
			  ')");
	DB::statement("SELECT addtable('public','web_b2c_narudzbina_stavka_posta_zahtev','
			web_b2c_narudzbina_stavka_posta_zahtev_id bigserial NOT NULL,
			stavka_id bigint NOT NULL,
			posta_zahtev_api_id bigint NOT NULL,
			kolicina numeric(20,4) DEFAULT 0.00,						
			CONSTRAINT web_b2c_narudzbina_stavka_posta_zahtev_pkey PRIMARY KEY (web_b2c_narudzbina_stavka_posta_zahtev_id),			
			CONSTRAINT web_b2c_narudzbina_stavka_posta_zahtev_fk1 FOREIGN KEY (stavka_id)
			  REFERENCES public.web_b2c_narudzbina_stavka (web_b2c_narudzbina_stavka_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE CASCADE,
			CONSTRAINT web_b2c_narudzbina_stavka_posta_zahtev_fk2 FOREIGN KEY (posta_zahtev_api_id)
			  REFERENCES public.posta_zahtev_api (posta_zahtev_api_id) MATCH SIMPLE
			  ON UPDATE CASCADE ON DELETE CASCADE
			  ')");
	//DB::statement("SELECT addcol('public','posta_zahtev_api','cena_otkupa','numeric(20,2) DEFAULT 0')");
	DB::statement("SELECT addcol('public','posta_zahtev_api','tracking_num','character varying(255) DEFAULT NULL')");
	DB::statement("SELECT addcol('public','posta_zahtev_api','pickup_id','character varying(255) DEFAULT NULL')");

	DB::statement("SELECT addcol('public','partner','ptt_cityexpress','character varying(255) DEFAULT NULL')");
	DB::statement("SELECT addcol('public','web_kupac','ptt_cityexpress','character varying(255) DEFAULT NULL')");

	DB::statement("SELECT addcol('public','log_b2b_partner','ip','character varying(255)')");

	DB::statement("SELECT addcol('public','ponuda_stavka','added_rabat','real DEFAULT 0')");

	DB::statement("SELECT addcol('public','roba','bodovi_popust','smallint DEFAULT 1')");

	DB::statement("SELECT addtable('public','bodovi_popust','bodovi_popust_id bigserial NOT NULL, web_cena bigint, popust numeric(20,2),CONSTRAINT bodovi_popust_pkey PRIMARY KEY (bodovi_popust_id)')");

	DB::statement("SELECT addtable('public','bodovi_ostvareni','bodovi_ostvareni_id bigserial NOT NULL, web_cena bigint, broj_bodova integer NOT NULL DEFAULT 0,CONSTRAINT bodovi_ostvareni_pkey PRIMARY KEY (bodovi_ostvareni_id)')");

	DB::statement("SELECT addtable('public','vauceri_popust','vauceri_popust_id bigserial NOT NULL, web_cena bigint, popust numeric(20,2),CONSTRAINT vauceri_popust_pkey PRIMARY KEY (vauceri_popust_id)')");

	DB::statement("SELECT addcol('public','baneri_jezik','alt','character varying(255)')");

	DB::statement("ALTER TABLE web_b2b_korpa ALTER COLUMN datum TYPE timestamp");
    DB::statement("SELECT addcol('public','web_b2b_korpa','naruceno','smallint DEFAULT 0')");
    DB::statement("ALTER TABLE web_b2b_narudzbina ALTER COLUMN datum_dokumenta TYPE timestamp");
    DB::statement("ALTER TABLE web_b2c_narudzbina ALTER COLUMN datum_dokumenta TYPE timestamp");
    DB::statement("SELECT addcol('public','web_kupac','datum_kreiranja','timestamp')");
    DB::statement("SELECT addcol('public','partner','datum_kreiranja','timestamp')");

    // DB::statement("SELECT addcol('public','web_b2b_seo','parent_id','integer DEFAULT 0')");
	DB::statement("UPDATE web_b2b_seo SET parent_id = 0 WHERE parent_id IS null");
	DB::statement("SELECT addcol('public','web_b2b_seo','parent_id','integer DEFAULT null')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'web_b2b_seo_pkey'")) == 0){
		DB::statement("ALTER TABLE web_b2b_seo ADD CONSTRAINT web_b2b_seo_pkey PRIMARY KEY (web_b2b_seo_id)");
	}
	// if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'web_b2b_seo_parent_id_fkey'")) == 0){
	// 	DB::statement("ALTER TABLE web_b2b_seo ADD CONSTRAINT web_b2b_seo_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.web_b2b_seo (web_b2b_seo_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE");
	// }
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'web_b2b_seo_parent_id_fkey'")) == 1){
		DB::statement("ALTER TABLE public.web_b2b_seo DROP CONSTRAINT web_b2b_seo_parent_id_fkey");
 	}

    //tabele za ankete
    DB::statement("SELECT addtable('public','anketa','anketa_id bigserial NOT NULL, naziv text, flag_aktivan integer NOT NULL DEFAULT 1, izabran integer NOT NULL DEFAULT 0, narudzbina_izabran integer NOT NULL DEFAULT 0, CONSTRAINT anketa_pkey PRIMARY KEY (anketa_id)')");

    DB::statement("SELECT addtable('public','anketa_pitanje','anketa_pitanje_id bigserial NOT NULL, anketa_id integer NOT NULL, tip character varying(20), pitanje text, flag_aktivan integer NOT NULL DEFAULT 1, rbr integer, CONSTRAINT anketa_pitanje_pkey PRIMARY KEY (anketa_pitanje_id), CONSTRAINT anketa_id_fkey FOREIGN KEY (anketa_id) REFERENCES public.anketa (anketa_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

    DB::statement("SELECT addtable('public','anketa_odgovor','anketa_odgovor_id bigserial NOT NULL, anketa_pitanje_id integer NOT NULL, odgovor text, flag_aktivan integer NOT NULL DEFAULT 1, rbr integer, CONSTRAINT anketa_odgovor_pkey PRIMARY KEY (anketa_odgovor_id), CONSTRAINT anketa_pitanje_id_fkey FOREIGN KEY (anketa_pitanje_id) REFERENCES public.anketa_pitanje (anketa_pitanje_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

    DB::statement("SELECT addtable('public','anketa_odgovor_korisnik', 'anketa_pitanje_id integer NOT NULL, anketa_odgovor_id integer NOT NULL, anketa_odgovor_text character varying(500), ip character varying(255),datum timestamp, web_kupac_id integer, web_b2c_narudzbina_id integer, CONSTRAINT anketa_pitanje_id FOREIGN KEY (anketa_pitanje_id) REFERENCES public.anketa_pitanje (anketa_pitanje_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT anketa_odgovor_id_fkey FOREIGN KEY (anketa_odgovor_id) REFERENCES public.anketa_odgovor (anketa_odgovor_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT web_kupac_id_fkey FOREIGN KEY (web_kupac_id) REFERENCES public.web_kupac (web_kupac_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT web_b2c_narudzbina_id_fkey FOREIGN KEY (web_b2c_narudzbina_id) REFERENCES public.web_b2c_narudzbina (web_b2c_narudzbina_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	DB::statement("SELECT addcol('public','web_b2c_seo','anketa_id','integer DEFAULT null')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'anketa_id_fkey'")) == 0){
		DB::statement("ALTER TABLE web_b2c_seo ADD CONSTRAINT anketa_id_fkey FOREIGN KEY (anketa_id) REFERENCES public.anketa (anketa_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE");
	}

	DB::statement("SELECT addcol('public','web_b2b_narudzbina','web_b2b_korpa_id','integer DEFAULT null')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'web_b2b_korpa_id_fkey'")) == 0){
		DB::statement("ALTER TABLE web_b2b_narudzbina ADD CONSTRAINT web_b2b_korpa_id_fkey FOREIGN KEY (web_b2b_korpa_id) REFERENCES public.web_b2b_korpa (web_b2b_korpa_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE");
	}

	DB::statement("SELECT addcol('public','katalog','b2c_aktivno','smallint DEFAULT 0')");
	DB::statement("SELECT addcol('public','katalog','b2b_aktivno','smallint DEFAULT 0')");
	DB::statement("SELECT addcol('public','katalog','putanja_slika','character varying (300)')");
	DB::statement("SELECT addcol('public', 'katalog', 'putanja_pdf', 'character varying (300)')");

	DB::statement("ALTER TABLE kursna_lista ALTER COLUMN kupovni TYPE numeric(20,6)");
	DB::statement("ALTER TABLE kursna_lista ALTER COLUMN srednji TYPE numeric(20,6)");
	DB::statement("ALTER TABLE kursna_lista ALTER COLUMN prodajni TYPE numeric(20,6)");
	DB::statement("ALTER TABLE kursna_lista ALTER COLUMN ziralni TYPE numeric(20,6)");
	DB::statement("ALTER TABLE kursna_lista ALTER COLUMN web TYPE numeric(20,6)");


	//CRM
    DB::statement("SELECT addtable('public','crm_tip','crm_tip_id bigserial NOT NULL, naziv character varying(255), flag_aktivan integer NOT NULL DEFAULT 1, rbr integer, CONSTRAINT crm_tip_pkey PRIMARY KEY (crm_tip_id)')");
    DB::statement("SELECT addtable('public','crm_status','crm_status_id bigserial NOT NULL, naziv character varying(255),flag_aktivan integer NOT NULL DEFAULT 1, CONSTRAINT crm_status_pkey PRIMARY KEY (crm_status_id)')");
    DB::statement("SELECT addtable('public','crm','crm_id bigserial NOT NULL, partner_id bigint NOT NULL, crm_tip_id bigint NOT NULL, crm_status_id bigint NOT NULL, roba_id bigint DEFAULT NULL, ponuda_id bigint DEFAULT NULL, racun_id bigint DEFAULT NULL, opis text, flag_zavrseno smallint NOT NULL DEFAULT 0, datum_kreiranja timestamp, datum_zavrsetka timestamp, CONSTRAINT crm_pkey PRIMARY KEY (crm_id), CONSTRAINT crm_partner_id_fkey FOREIGN KEY (partner_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT crm_tip_id_fkey FOREIGN KEY (crm_tip_id) REFERENCES public.crm_tip (crm_tip_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT crm_status_id_fkey FOREIGN KEY (crm_status_id) REFERENCES public.crm_status (crm_status_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT crm_roba_id_fkey FOREIGN KEY (roba_id) REFERENCES public.roba (roba_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL, CONSTRAINT crm_ponuda_id_fkey FOREIGN KEY (ponuda_id) REFERENCES public.ponuda (ponuda_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL, CONSTRAINT crm_racun_id_fkey FOREIGN KEY (racun_id) REFERENCES public.racun (racun_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL')");
    DB::statement("SELECT addtable('public','crm_akcija_tip','crm_akcija_tip_id bigserial NOT NULL, naziv character varying(255), flag_aktivan integer NOT NULL DEFAULT 1, rbr integer, CONSTRAINT crm_akcija_tip_pkey PRIMARY KEY (crm_akcija_tip_id)')");
    DB::statement("SELECT addtable('public','crm_akcija','crm_akcija_id bigserial NOT NULL, crm_akcija_tip_id bigint NOT NULL, crm_id bigint NOT NULL, opis text, flag_zavrseno smallint NOT NULL DEFAULT 0, datum timestamp, datum_zavrsetka timestamp, CONSTRAINT crm_akcija_pkey PRIMARY KEY (crm_akcija_id), CONSTRAINT crm_akcija_tip_id_fkey FOREIGN KEY (crm_akcija_tip_id) REFERENCES public.crm_akcija_tip (crm_akcija_tip_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT crm_id_fkey FOREIGN KEY (crm_id) REFERENCES public.crm (crm_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
    DB::statement("SELECT addtable('public','imenik_crm','imenik_id bigint NOT NULL, crm_id bigint NOT NULL, CONSTRAINT imenik_crm_pkey PRIMARY KEY (imenik_id,crm_id), CONSTRAINT imenik_crm_imenik_id_fkey FOREIGN KEY (imenik_id) REFERENCES public.imenik (imenik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT imenik_crm_crm_id_fkey FOREIGN KEY (crm_id) REFERENCES public.crm (crm_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
    if(DB::select("SELECT COUNT(*) FROM information_schema.sequences WHERE sequence_name='vrsta_kontakta_vrsta_kontakta_id_seq'")[0]->count == 0){
		DB::statement("CREATE SEQUENCE vrsta_kontakta_vrsta_kontakta_id_seq");
		DB::statement("ALTER TABLE vrsta_kontakta ALTER vrsta_kontakta_id SET DEFAULT NEXTVAL('vrsta_kontakta_vrsta_kontakta_id_seq')");
		DB::statement("SELECT setval('vrsta_kontakta_vrsta_kontakta_id_seq', (SELECT MAX(vrsta_kontakta_id) FROM vrsta_kontakta) + 1, FALSE)");
	}
	DB::statement("SELECT addcol('public','crm','vrednost','integer')");
	DB::statement("SELECT addcol('public','crm','kursna_lista_id','integer')");
	DB::statement("SELECT addconstraint('crm','crm_kursna_lista_id_fkey','FOREIGN KEY (kursna_lista_id) REFERENCES public.kursna_lista (kursna_lista_id) MATCH SIMPLE ON UPDATE SET NULL ON DELETE SET NULL')");

	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'vrsta_fajla_id_pkey'")) == 0){
		DB::statement("ALTER TABLE vrsta_fajla ADD CONSTRAINT vrsta_fajla_id_pkey PRIMARY KEY (vrsta_fajla_id)");
	}

	 DB::statement("SELECT addtable('public',
	 	'crm_dokumenta',
	 	'crm_dokumenta_id bigserial NOT NULL, 
		vrsta_fajla_id bigint NOT NULL,
	 	 crm_id bigint NOT NULL, 
	 	 putanja character varying(1000),
	 	 putanja_http character varying(1000),
	 	 naziv character varying(255),
		CONSTRAINT crm_dokumenta_pkey PRIMARY KEY (crm_dokumenta_id),
	 	 CONSTRAINT crm_id_fkey FOREIGN KEY (crm_id) REFERENCES public.crm (crm_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, 
	 	 CONSTRAINT vrsta_fajla_id_fkey FOREIGN KEY (vrsta_fajla_id) REFERENCES public.vrsta_fajla (vrsta_fajla_id) MATCH SIMPLE ON UPDATE SET NULL ON DELETE SET NULL')");
	 DB::statement("SELECT addcol('public','crm_dokumenta','datum_kreiranja','timestamp')");

	 DB::statement("SELECT addtable('public','crm_fakture','crm_fakture_id bigserial NOT NULL, partner_id bigint NOT NULL,crm_tip_id bigint NOT NULL, datum_fakturisanja timestamp ,opis text,vrednost integer, kursna_lista_id integer, CONSTRAINT crm_fakture_pkey PRIMARY KEY (crm_fakture_id), CONSTRAINT crm_fakture_partner_id_fkey FOREIGN KEY (partner_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT crm_fakture_crm_tip_id_fkey FOREIGN KEY (crm_tip_id) REFERENCES public.crm_tip (crm_tip_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,CONSTRAINT crm_fakture_kursna_lista_id_fkey FOREIGN KEY (kursna_lista_id) REFERENCES public.kursna_lista (kursna_lista_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
	 DB::statement("ALTER TABLE crm_fakture ALTER COLUMN vrednost TYPE text");
	 DB::statement("SELECT addtable('public','crm_log','crm_log_id bigserial NOT NULL, datum_izmene timestamp, akcija character varying(255), stara_vrednost text, nova_vrednost text, imenik_id bigserial NOT NULL,CONSTRAINT crm_log_pkey PRIMARY KEY (crm_log_id),
		CONSTRAINT imenik_id_fkey FOREIGN KEY (imenik_id) REFERENCES public.imenik (imenik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
	 DB::statement("SELECT addcol('public','crm_fakture','flag_poslato','smallint')");
	 DB::statement("INSERT INTO crm_tip (naziv,flag_aktivan) SELECT * FROM (VALUES 
	    	('Administracija portala',1))
	    	redovi(naziv,flag_aktivan) 
	        WHERE naziv NOT IN (SELECT naziv FROM crm_tip)");
	 DB::statement("INSERT INTO crm_status (naziv,flag_aktivan) SELECT * FROM (VALUES 
	    	('Ponuda poslata',1),
	    	('Sklopljen ugovor',2))
	    	redovi(naziv,flag_aktivan) 
	        WHERE naziv NOT IN (SELECT naziv FROM crm_status)");
	 DB::statement("INSERT INTO crm_akcija_tip (naziv,flag_aktivan) SELECT * FROM (VALUES 
	    	('Telefon',1))
	    	redovi(naziv,flag_aktivan) 
	        WHERE naziv NOT IN (SELECT naziv FROM crm_akcija_tip)");
	 DB::statement("SELECT addcol('public','crm_fakture','popust','character varying(255)')");
	 DB::statement("INSERT INTO vrsta_kontakta (naziv) SELECT * FROM (VALUES 
	    	('Mail za fakturisanje'))
	    	redovi(naziv) 
	        WHERE naziv NOT IN (SELECT naziv FROM vrsta_kontakta)");
	 DB::statement("SELECT addtable('public','crm_sertifikati','crm_sertifikati_id bigserial NOT NULL, partner_id bigint NOT NULL, datum_sertifikata timestamp ,opis text, CONSTRAINT crm_sertifikati_pkey PRIMARY KEY (crm_sertifikati_id), CONSTRAINT crm_fakture_partner_id_fkey FOREIGN KEY (partner_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
	//gotov crm

	//custom css
	DB::statement("SELECT addtable('public','custom_css','custom_css_id bigserial NOT NULL, sadrzaj text, flag_aktivan integer DEFAULT 1')");
	DB::statement("INSERT INTO custom_css (custom_css_id,sadrzaj) SELECT * FROM (VALUES 
	    	(1,'/* enter your css here */'))	    	
	    	redovi(custom_css_id,sadrzaj) 
	        WHERE custom_css_id NOT IN (SELECT custom_css_id FROM custom_css)");

	//PitchPrint
	DB::statement("SELECT addcol('public','roba','design_id','character varying (100)')");
	DB::statement("SELECT addcol('public','web_b2c_narudzbina_stavka','project_id','character varying(255)')");	
	DB::statement("SELECT addcol('public','web_b2c_korpa_stavka','project_id','character varying(255)')");	

    //sliders
    DB::statement("SELECT addtable('public','slajder_tip','slajder_tip_id bigserial NOT NULL, naziv character varying(255), flag_aktivan integer NOT NULL DEFAULT 1, rbr integer, CONSTRAINT slajder_tip_pkey PRIMARY KEY (slajder_tip_id)')");
    DB::statement("SELECT addtable('public','slajder','slajder_id bigserial NOT NULL, slajder_tip_id bigint NOT NULL, naziv character varying(255), flag_aktivan integer NOT NULL DEFAULT 1, CONSTRAINT slajder_pkey PRIMARY KEY (slajder_id), CONSTRAINT slajder_tip_id_fkey FOREIGN KEY (slajder_tip_id) REFERENCES public.slajder_tip (slajder_tip_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
    DB::statement("SELECT addtable('public','slajder_stavka','slajder_stavka_id bigserial NOT NULL, slajder_id bigint NOT NULL, image_path character varying(500), flag_aktivan integer NOT NULL DEFAULT 1, datum_od date, datum_do date, rbr smallint, CONSTRAINT slajder_stavka_pkey PRIMARY KEY (slajder_stavka_id), CONSTRAINT slajder_id_fkey FOREIGN KEY (slajder_id) REFERENCES public.slajder (slajder_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
    DB::statement("SELECT addtable('public','slajder_stavka_jezik', 'slajder_stavka_id bigint NOT NULL, jezik_id bigint NOT NULL, naslov character varying(100), sadrzaj text, naslov_dugme character varying(100), link character varying(500), alt character varying(255), CONSTRAINT slajder_stavka_jezik_pkey PRIMARY KEY (slajder_stavka_id,jezik_id), CONSTRAINT slajder_stavka_fkey FOREIGN KEY (slajder_stavka_id) REFERENCES public.slajder_stavka (slajder_stavka_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT sekcija_stranice_jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
	DB::statement("INSERT INTO slajder_tip (naziv,rbr) SELECT * FROM (VALUES 
	    	('Slajder',1),
	    	('Baner',2),
	    	('Popup Baner',3),
	    	('Tekst Baner (tekst levo)',4),
	    	('Galerija Baner',6),
	    	('Galerija Slajder',7),
	    	('Tekst Baner (tekst desno)',5),
	    	('Baner - cetiri banera',8),
	    	('Baner - jedan baner',9))
	    	redovi(naziv,rbr) 
	        WHERE naziv NOT IN (SELECT naziv FROM slajder_tip)");

    //page sections
    DB::statement("SELECT addtable('public','sekcija_stranice_tip','sekcija_stranice_tip_id bigserial NOT NULL, naziv character varying(255), flag_aktivan integer NOT NULL DEFAULT 1, rbr integer, CONSTRAINT sekcija_stranice_tip_pkey PRIMARY KEY (sekcija_stranice_tip_id)')");
	DB::statement("SELECT addtable('public','sekcija_stranice','sekcija_stranice_id bigserial NOT NULL, sekcija_stranice_tip_id bigint NOT NULL, naziv character varying(255), flag_aktivan integer NOT NULL DEFAULT 1, slajder_id bigint, tip_artikla_id bigint, file_path character varying(500), CONSTRAINT sekcija_stranice_pkey PRIMARY KEY (sekcija_stranice_id), CONSTRAINT sekcija_stranice_tip_id_fkey FOREIGN KEY (sekcija_stranice_tip_id) REFERENCES public.sekcija_stranice_tip (sekcija_stranice_tip_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT slajder_id_fkey FOREIGN KEY (slajder_id) REFERENCES public.slajder (slajder_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL, CONSTRAINT tip_artikla_id_fkey FOREIGN KEY (tip_artikla_id) REFERENCES public.tip_artikla (tip_artikla_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL')");
	DB::statement("SELECT addtable('public','sekcija_stranice_jezik','sekcija_stranice_jezik_id bigserial NOT NULL, sekcija_stranice_id bigint NOT NULL, jezik_id bigint NOT NULL, sadrzaj text, CONSTRAINT sekcija_stranice_jezik_id_pkey PRIMARY KEY (sekcija_stranice_jezik_id), CONSTRAINT sekcija_stranice_id_fkey FOREIGN KEY (sekcija_stranice_id) REFERENCES public.sekcija_stranice (sekcija_stranice_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT sekcija_stranice_jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
	DB::statement("SELECT addtable('public','stranica_sekcija','web_b2c_seo_id bigint NOT NULL, sekcija_stranice_id bigint NOT NULL, rbr smallint, CONSTRAINT stranica_sekcija_pkey PRIMARY KEY (web_b2c_seo_id,sekcija_stranice_id), CONSTRAINT web_b2c_seo_id_fkey FOREIGN KEY (web_b2c_seo_id) REFERENCES public.web_b2c_seo (web_b2c_seo_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT sekcija_stranice_id_fkey FOREIGN KEY (sekcija_stranice_id) REFERENCES public.sekcija_stranice (sekcija_stranice_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
	DB::statement("INSERT INTO sekcija_stranice_tip (naziv,rbr) SELECT * FROM (VALUES 
	    	('Text',1),
	    	('Slajder',2),
	    	('Lista artikala',3),
	    	('Lista vesti',4),
	    	('Newsletter',5))
	    	redovi(naziv,rbr) 
	        WHERE naziv NOT IN (SELECT naziv FROM sekcija_stranice_tip)");
	DB::statement("INSERT INTO tip_artikla (tip_artikla_id,naziv) SELECT * FROM (VALUES 
	    	(-3,'Sa najvecim pregledom'),
	    	(-2,'Najpopularniji'),
	    	(-1,'Najnoviji'))
	    	redovi(tip_artikla_id,naziv) 
	        WHERE tip_artikla_id NOT IN (SELECT tip_artikla_id FROM tip_artikla)");
	DB::statement("UPDATE tip_artikla SET naziv = 'Najnoviji' WHERE tip_artikla_id=-1");
	// B2B SLIDERS
    DB::statement("SELECT addtable('public','b2b_slajder_tip','b2b_slajder_tip_id bigserial NOT NULL, naziv character varying(255), flag_aktivan integer NOT NULL DEFAULT 1, rbr integer, CONSTRAINT b2b_slajder_tip_pkey PRIMARY KEY (b2b_slajder_tip_id)')");
    DB::statement("SELECT addtable('public','b2b_slajder','b2b_slajder_id bigserial NOT NULL, b2b_slajder_tip_id bigint NOT NULL, naziv character varying(255), flag_aktivan integer NOT NULL DEFAULT 1, CONSTRAINT b2b_slajder_pkey PRIMARY KEY (b2b_slajder_id), CONSTRAINT b2b_slajder_tip_id_fkey FOREIGN KEY (b2b_slajder_tip_id) REFERENCES public.b2b_slajder_tip (b2b_slajder_tip_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
    DB::statement("SELECT addtable('public','b2b_slajder_stavka','b2b_slajder_stavka_id bigserial NOT NULL, b2b_slajder_id bigint NOT NULL, image_path character varying(500), flag_aktivan integer NOT NULL DEFAULT 1, datum_od date, datum_do date, rbr smallint, CONSTRAINT b2b_slajder_stavka_pkey PRIMARY KEY (b2b_slajder_stavka_id), CONSTRAINT b2b_slajder_id_fkey FOREIGN KEY (b2b_slajder_id) REFERENCES public.b2b_slajder (b2b_slajder_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
    DB::statement("SELECT addtable('public','b2b_slajder_stavka_jezik', 'b2b_slajder_stavka_id bigint NOT NULL, jezik_id bigint NOT NULL, naslov character varying(100), sadrzaj text, naslov_dugme character varying(100), link character varying(500), alt character varying(255), CONSTRAINT b2b_slajder_stavka_jezik_pkey PRIMARY KEY (b2b_slajder_stavka_id,jezik_id), CONSTRAINT b2b_slajder_stavka_fkey FOREIGN KEY (b2b_slajder_stavka_id) REFERENCES public.b2b_slajder_stavka (b2b_slajder_stavka_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT sekcija_stranice_jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");
	DB::statement("INSERT INTO b2b_slajder_tip (naziv,rbr) SELECT * FROM (VALUES 
	    	('Slajder',1),
	    	('Baner',2),
	    	('Popup Baner',3),
	    	('Tekst Baner (tekst levo)',4),
	    	('Galerija Baner',6),
	    	('Galerija Slajder',7),
	    	('Tekst Baner (tekst desno)',5))
	    	redovi(naziv,rbr) 
	        WHERE naziv NOT IN (SELECT naziv FROM b2b_slajder_tip)");

	// B2B SECTIONS
    DB::statement("SELECT addtable('public','b2b_sekcija_stranice_tip','b2b_sekcija_stranice_tip_id bigserial NOT NULL, naziv character varying(255), flag_aktivan integer NOT NULL DEFAULT 1, rbr integer, CONSTRAINT b2b_sekcija_stranice_tip_pkey PRIMARY KEY (b2b_sekcija_stranice_tip_id)')");

	DB::statement("SELECT addtable('public','b2b_sekcija_stranice','b2b_sekcija_stranice_id bigserial NOT NULL, b2b_sekcija_stranice_tip_id bigint NOT NULL, naziv character varying(255), flag_aktivan integer NOT NULL DEFAULT 1, b2b_slajder_id bigint, tip_artikla_id bigint, file_path character varying(500), CONSTRAINT b2b_sekcija_stranice_pkey PRIMARY KEY (b2b_sekcija_stranice_id), CONSTRAINT b2b_sekcija_stranice_tip_id_fkey FOREIGN KEY (b2b_sekcija_stranice_tip_id) REFERENCES public.b2b_sekcija_stranice_tip (b2b_sekcija_stranice_tip_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT b2b_slajder_id_fkey FOREIGN KEY (b2b_slajder_id) REFERENCES public.b2b_slajder (b2b_slajder_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL, CONSTRAINT tip_artikla_id_fkey FOREIGN KEY (tip_artikla_id) REFERENCES public.tip_artikla (tip_artikla_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL')");

	DB::statement("SELECT addtable('public','b2b_sekcija_stranice_jezik','b2b_sekcija_stranice_jezik_id bigserial NOT NULL, b2b_sekcija_stranice_id bigint NOT NULL, jezik_id bigint NOT NULL, sadrzaj text, CONSTRAINT b2b_sekcija_stranice_jezik_id_pkey PRIMARY KEY (b2b_sekcija_stranice_jezik_id), CONSTRAINT b2b_sekcija_stranice_id_fkey FOREIGN KEY (b2b_sekcija_stranice_id) REFERENCES public.b2b_sekcija_stranice (b2b_sekcija_stranice_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT b2b_sekcija_stranice_jezik_id_fkey FOREIGN KEY (jezik_id) REFERENCES public.jezik (jezik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	DB::statement("SELECT addtable('public','b2b_stranica_sekcija','web_b2b_seo_id bigint NOT NULL, b2b_sekcija_stranice_id bigint NOT NULL, rbr smallint, CONSTRAINT b2b_stranica_sekcija_pkey PRIMARY KEY (web_b2b_seo_id,b2b_sekcija_stranice_id), CONSTRAINT web_b2b_seo_id_fkey FOREIGN KEY (web_b2b_seo_id) REFERENCES public.web_b2b_seo (web_b2b_seo_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT b2b_sekcija_stranice_id_fkey FOREIGN KEY (b2b_sekcija_stranice_id) REFERENCES public.b2b_sekcija_stranice (b2b_sekcija_stranice_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	DB::statement("INSERT INTO b2b_sekcija_stranice_tip (naziv,rbr) SELECT * FROM (VALUES 
	    	('Text',1),
	    	('Slajder',2),
	    	('Lista artikala',3),
	    	('Lista vesti',4),
	    	('Newsletter',5))
	    	redovi(naziv,rbr) 
	        WHERE naziv NOT IN (SELECT naziv FROM b2b_sekcija_stranice_tip)");
 
	DB::statement("SELECT addcol('public','b2b_sekcija_stranice','puna_sirina','smallint NOT NULL DEFAULT 0')");
	DB::statement("SELECT addcol('public','b2b_sekcija_stranice','boja_pozadine','character varying (255)')");
    DB::statement("SELECT addtable('public','web_b2b_prodata_roba','web_b2b_prodata_roba_id bigserial NOT NULL, partner_id bigint NOT NULL, roba_id bigint NOT NULL, broj integer DEFAULT 0, rbr integer DEFAULT 0, CONSTRAINT web_b2b_prodata_roba_pkey PRIMARY KEY (web_b2b_prodata_roba_id), CONSTRAINT partner_id_fkey FOREIGN KEY (partner_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT roba_id_fkey FOREIGN KEY (roba_id) REFERENCES public.roba (roba_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

    // Banner Category
	DB::statement("SELECT addcol('public','grupa_pr','baner_id','integer DEFAULT null')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'grupa_pr_baner_id_fkey'")) == 0){
		DB::statement("ALTER TABLE grupa_pr ADD CONSTRAINT grupa_pr_baner_id_fkey FOREIGN KEY (baner_id) REFERENCES public.slajder (slajder_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE");
	}

    DB::statement("SELECT addcol('public','web_b2b_narudzbina','imenik_id','integer DEFAULT null')");
	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'web_b2b_narudzbina_imenik_id_fkey'")) == 0){
		DB::statement("ALTER TABLE web_b2b_narudzbina ADD CONSTRAINT web_b2b_narudzbina_imenik_id_fkey FOREIGN KEY (imenik_id) REFERENCES public.imenik (imenik_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE");
	}

	DB::statement("UPDATE web_b2c_seo SET disable = 0 WHERE naziv_stranice IN ('pocetna')");

	DB::statement("SELECT addcol('public','sekcija_stranice','puna_sirina','smallint NOT NULL DEFAULT 0')");
	DB::statement("SELECT addcol('public','sekcija_stranice','boja_pozadine','character varying (255)')");

	DB::statement("SELECT addcol('public','web_b2c_narudzbina','poslednja_izmena','timestamp')");

	DB::statement("UPDATE vrsta_dokumenta SET sifra_vd = 'WNC' WHERE sifra_vd = 'VNC'");

	DB::statement("SELECT addcol('public','posta_zahtev_api','pickup_id','character varying (255)')");

	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'narudzbina_status_pkey'")) == 0){
		DB::statement("ALTER TABLE narudzbina_status ADD CONSTRAINT narudzbina_status_pkey PRIMARY KEY (narudzbina_status_id)");
	}
	DB::statement("SELECT addtable('public','narudzbina_status_narudzbina','narudzbina_status_id bigint NOT NULL, web_b2c_narudzbina_id bigint NOT NULL, CONSTRAINT narudzbina_status_narudzbina_pkey PRIMARY KEY (narudzbina_status_id,web_b2c_narudzbina_id), CONSTRAINT narudzbina_status_id_fkey FOREIGN KEY (narudzbina_status_id) REFERENCES public.narudzbina_status (narudzbina_status_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE, CONSTRAINT web_b2c_narudzbina_id_fkey FOREIGN KEY (web_b2c_narudzbina_id) REFERENCES public.web_b2c_narudzbina (web_b2c_narudzbina_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE')");

	DB::statement("SELECT addcol('public','web_b2b_korpa_stavka','racunska_cena_end','numeric(20,4) DEFAULT 0.00')");
	DB::statement("SELECT addcol('public','web_b2b_narudzbina_stavka','racunska_cena_end','numeric(20,4) DEFAULT 0.00')");

	DB::statement("SELECT addtable('public','web_b2b_komentari','web_b2b_komentar_id bigserial,roba_id integer, ime_osobe character varying(255), ip_adresa character varying(200), pitanje character varying, odgovor character varying, komentar_odobren integer, datum date, odgovoreno integer, ocena smallint')");

	DB::statement("SELECT addcol('public','radni_nalog','pregledan','smallint DEFAULT 1')");
	DB::statement("SELECT addcol('public','radni_nalog','roba','smallint DEFAULT 1')");
	DB::statement("SELECT addcol('public','radni_nalog','hitno','smallint DEFAULT 0')");
	DB::statement("SELECT addcol('public','web_kupac','servis','smallint DEFAULT 1')");
	DB::statement("SELECT addcol('public','radni_nalog','proizvodjac','character varying(255)')");
	DB::statement("ALTER TABLE radni_nalog ALTER COLUMN kupac_id DROP NOT NULL");
	DB::statement("SELECT addcol('public','radni_nalog','b2b_partner_id','integer DEFAULT null')");
    if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'radni_nalog_b2b_partner_id_fkey'")) == 0){
		DB::statement("ALTER TABLE radni_nalog ADD CONSTRAINT radni_nalog_b2b_partner_id_fkey FOREIGN KEY (b2b_partner_id) REFERENCES public.partner (partner_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE NO ACTION");
	}

	//ENERGETSKE KLASE
    DB::statement("SELECT addcol('public','roba','energetska_klasa','character varying(50)')");
    DB::statement("SELECT addcol('public','roba','deklaracija','text')");
    DB::statement("SELECT addcol('public','roba','energetska_klasa_pdf','character varying(1000)')");
    DB::statement("SELECT addcol('public','roba','energetska_klasa_link','character varying(1000)')");
    DB::statement("SELECT addcol('public','roba','check_link','smallint DEFAULT 0')");
    DB::statement("SELECT addcol('public','roba','check_old_class','smallint DEFAULT 0')");
    DB::statement("SELECT addtable('public','energetska_klasa','energetska_klasa_id bigserial NOT NULL, klasa character varying(50)')");

    DB::statement("INSERT INTO energetska_klasa (energetska_klasa_id,klasa) SELECT * FROM (VALUES 
	    	(1, 'A+++'),
	    	(2, 'A++'),
	    	(3, 'A+'),
	    	(4, 'A'),
	    	(5, 'B'),
	    	(6, 'C'),
	    	(7, 'D'),
	    	(8, 'E'),
	    	(9, 'F'),
	    	(10, 'G'),
	    	(11, ''))
	    	redovi(energetska_klasa_id,klasa) 
	        WHERE energetska_klasa_id NOT IN (SELECT energetska_klasa_id FROM energetska_klasa)");

	DB::statement("SELECT dropcol('public','radni_nalog','roba_id')");

	// USLUGE PRIJAVE KVARA - RMA B2C
	DB::statement("SELECT addtable('public','web_b2c_rma','web_b2c_rma_id bigserial, flag_vrsta_kupca smallint, ime character varying(255), prezime character varying(255), naziv character varying(255), pib character varying(255), maticni_br character varying(255), email character varying(255), telefon character varying(255), adresa character varying(255), mesto character varying(255), proizvodjac character varying(255), model character varying(255), serijski character varying(255), opis text, preuzeti character varying(1)')");

	// DEVICE SLIDERS
	DB::statement("SELECT addcol('public','slajder','slajder_device_id','smallint NOT NULL DEFAULT 0')");
	DB::statement("SELECT addtable('public','slajder_device','slajder_device_id bigserial NOT NULL, naziv character varying(255), napomena character varying(255), 
		CONSTRAINT slajer_device_id_pkey PRIMARY KEY (slajder_device_id)')");
	DB::statement("INSERT INTO slajder_device (naziv) SELECT * FROM (VALUES 
	    	('Default'),
	    	('Desktop'),
	    	('Mobile'))
	    	redovi(naziv) 
	        WHERE naziv NOT IN (SELECT naziv FROM slajder_device)");

	if(count(DB::select("SELECT 1 FROM pg_constraint WHERE conname = 'slajder_device_id_fkey'")) == 1){
		DB::statement("ALTER TABLE slajder ADD CONSTRAINT slajder_device_id_fkey FOREIGN KEY (slajder_device_id) REFERENCES public.slajder_device (slajder_device_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE");
	}

	DB::statement("SELECT addcol('public','web_b2c_korpa','besplatna_dostava', 'smallint DEFAULT 0')");

	DB::statement("SELECT addcol('public','web_b2c_narudzbina','besplatna_dostava', 'smallint DEFAULT 0')");

	DB::statement("SELECT addcol('public','web_b2c_narudzbina','vaucer_popust', 'numeric DEFAULT 0')");

	DB::statement("SELECT addtable('public','vaucer_cart','web_b2c_korpa_id bigint,vaucer_id bigint')");

	DB::statement("INSERT INTO vaucer_tip (vaucer_tip_id,vaucer_tip) SELECT * FROM (VALUES 
	    	(1,'Akcija'),
	    	(2,'Popust'),
	    	(3,'Besplatna dostava'))
	    	redovi(vaucer_tip_id,vaucer_tip) 
	        WHERE vaucer_tip_id NOT IN (SELECT vaucer_tip_id FROM vaucer_tip)");

	DB::statement("SELECT addcol('public','vaucer','web_nacin_placanja_id','smallint DEFAULT 0')");

 	DB::statement("ALTER TABLE roba_jezik ALTER COLUMN title TYPE character varying(500)");

 	DB::statement("SELECT addtable('public','garancija_datum','garancija_datum_id bigserial NOT NULL,serijski_broj character varying(255),datum_garancije character varying(255), CONSTRAINT garancija_datum_pkey PRIMARY KEY (garancija_datum_id),CONSTRAINT serijski_broj_unique UNIQUE (serijski_broj)')");

 	DB::statement("SELECT addcol('public','garancija_datum','naziv_artikla','character varying(500)')");

 	    $this->info('Database has been updated successfully.');
 	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
