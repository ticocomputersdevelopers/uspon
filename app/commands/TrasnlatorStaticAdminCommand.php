<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;



class TranslatorStaticAdminCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'staticadmin:translate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Translate.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$langs = $this->argument('langs');
		$langs = explode('-',$langs);
		$default_lang = DB::table('jezik')->where(array('jezik_id'=>1,'izabrani'=>1))->pluck('kod');

		$products_file = 'files/langs/static.xlsx';
		PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
        $excelObj = $excelReader->load($products_file);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

		// $products_file_js = 'files/langs/jsstatic.xlsx';
		// PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
  //       $excelReaderJs = PHPExcel_IOFactory::createReaderForFile($products_file_js);
  //       $excelObjJs = $excelReaderJs->load($products_file_js);
  //       $worksheetJs = $excelObjJs->getSheet(0);
  //       $lastRowJs = $worksheetJs->getHighestRow();


		foreach($langs as $lang){
			$jezik = DB::table('jezik')->where(array('kod'=>$lang,'aktivan'=>1,'izabrani'=>0))->first();
			if(is_null($jezik)){
				break;
			}

			//static
			for ($row = 1; $row <= $lastRow; $row++) {
				$srWord = trim($worksheet->getCell('A'.$row)->getValue());
				if($lang=='en'){
					$langWord = trim($worksheet->getCell('B'.$row)->getValue());
				}else if($lang=='de'){
					$langWord = trim($worksheet->getCell('C'.$row)->getValue());
				}

				if(trim($srWord) && trim($srWord) != ''){
					if(DB::table('admin_prevodilac')->where(array('izabrani_jezik_id'=>1,'jezik_id'=>$jezik->jezik_id,'izabrani_jezik_reci'=>trim($srWord)))->count() == 0){
						DB::table('admin_prevodilac')->insert(array('izabrani_jezik_id'=>1,'jezik_id'=>$jezik->jezik_id,'izabrani_jezik_reci'=>trim($srWord),'reci'=>trim($langWord)));
					}
				}
			}

			// //jsstatic
			// for ($row = 1; $row <= $lastRowJs; $row++) {
			// 	$srWord = trim($worksheet->getCell('A'.$row)->getValue());
			// 	if($lang=='en'){
			// 		$langWord = trim($worksheet->getCell('B'.$row)->getValue());
			// 	}else if($lang=='de'){
			// 		$langWord = trim($worksheet->getCell('C'.$row)->getValue());
			// 	}

			// 	if(trim($srWord) && trim($srWord) != ''){
			// 		if(DB::table('prevodilac')->where(array('izabrani_jezik_id'=>1,'jezik_id'=>$jezik->jezik_id,'izabrani_jezik_reci'=>trim($srWord)))->count() == 0){
			// 			DB::table('prevodilac')->insert(array('izabrani_jezik_id'=>1,'jezik_id'=>$jezik->jezik_id,'izabrani_jezik_reci'=>trim($srWord),'reci'=>trim($langWord),'is_js'=>1));
			// 		}
			// 	}
			// }

		}

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('langs', InputArgument::OPTIONAL, 'Languages.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}