	<div class="m-tabs clearfix">
		<div class="m-tabs__tab{{ $strana=='radni_nalog_prijem' ? ' m-tabs__tab--active' : '' }}"><a href="{{RmaOptions::base_url()}}rma/radni-nalog-prijem/{{ $radni_nalog->radni_nalog_id }}">PRIJEM </a></div>
		@if($radni_nalog->radni_nalog_id != 0)
		<div class="m-tabs__tab{{ $strana=='radni_nalog_opis_rada' ? ' m-tabs__tab--active' : '' }}"><a href="{{RmaOptions::base_url()}}rma/radni-nalog-opis-rada/{{ $radni_nalog->radni_nalog_id }}">OPIS RADA </a></div>
		@if(count($radni_nalog_troskovi) > 0)
		<div class="m-tabs__tab{{ $strana=='radni_nalog_dokumenti' ? ' m-tabs__tab--active' : '' }}"><a href="{{RmaOptions::base_url()}}rma/radni-nalog-trosak-dokumenti/{{$radni_nalog->radni_nalog_id}}/{{ $radni_nalog_troskovi[0]->radni_nalog_trosak_id }}">USLUŽNI SERVIS </a></div>
		@endif
		<div class="m-tabs__tab{{ $strana=='radni_nalog_predaja' ? ' m-tabs__tab--active' : '' }}"><a href="{{RmaOptions::base_url()}}rma/radni-nalog-predaja/{{ $radni_nalog->radni_nalog_id }}">PREDAJA </a></div>
		@endif

	</div>