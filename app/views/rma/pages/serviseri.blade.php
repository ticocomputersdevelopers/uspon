@extends('rma.templates.main')
@section('content')
<section id="main-content">


	<div class="row">
		@if(count($servisi) > 1)
		<section class="medium-4 small-12 columns">
			<div class="flat-box"> 
				<h3 class="title-med">Servisi</h3> 
				<div class="row"> 
					<div class="columns medium-12"> 
						<select id="JSServis" class="margin-bottom">
						@foreach($servisi as $servis)
							<option value="{{ $servis->partner_id }}" {{ ($servis_id == $servis->partner_id) ? 'selected' : '' }}>{{ $servis->naziv }}</option>
						@endforeach
						</select>
					</div>
				</div>
 			</div> <!-- end of .flat-box -->
 			
		</section>
		@endif
	
		@if($servis_id > 0)
		<section class="small-12 medium-{{ (count($servisi) > 1) ? '4' : '6' }} columns">
			<div class="flat-box"> 
				<h3 class="title-med">Serviseri</h3> 
				<div class="row"> 
					<div class="columns medium-12"> 
						<label>Ime i Prezime</label>		
					</div>
				</div> 
				<ul class="banner-list name-ul">
					<li class="name-ul__li @if($serviser_id == 0) active @endif">
						<label class="no-margin">Dodaj novog servisera</label> 
						<a href="{{ RmaOptions::base_url() }}rma/serviseri/{{ $servis_id }}/0" class="name-ul__li__right-arrow button-option tooltipz" aria-label="Dodaj">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</a>
					</li>
					@foreach($serviseri as $row)
					<li class="name-ul__li @if($row->serviser_id == $serviser_id) active @endif" >
						<label class="no-margin">{{ $row->ime.' '.$row->prezime }}</label> 
						<a href="{{ RmaOptions::base_url() }}rma/serviseri/{{ $servis_id }}/{{ $row->serviser_id }}" class="name-ul__li__right-arrow button-option tooltipz" aria-label="Dodaj">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</a>
					</li>
					@endforeach
				</ul>
 			</div> <!-- end of .flat-box --> 
		</section>

		<section class="small-12 medium-{{ (count($servisi) > 1) ? '4' : '6' }} columns">
			<div class="flat-box">
						
				<h3 class="title-med">Serviser</h3>
				<form method="POST" action="{{ RmaOptions::base_url() }}rma/serviseri-post">
				    <input type="hidden" name="serviser_id" value="{{ $serviser->serviser_id }}" />
					<input type="hidden" name="partner_id" value="{{ $serviser->partner_id }}" />

					<div class="row">
						<div class="columns medium-12 {{ $errors->first('ime') ? ' error' : '' }}">
							<label for="">Ime</label>
							<input type="text" name="ime" data-id="" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : $serviser->ime) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>
					<div class="row">
						<div class="columns medium-12 {{ $errors->first('prezime') ? ' error' : '' }}">
							<label for="">Prezime</label>
							<input type="text" name="prezime" data-id="" value="{{ htmlentities(Input::old('prezime') ? Input::old('prezime') : $serviser->prezime) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>
					<div class="row">
						<div class="columns medium-12 {{ $errors->first('email') ? ' error' : '' }}">
							<label for="">Email</label>
							<input type="text" name="email" data-id="" value="{{ htmlentities(Input::old('email') ? Input::old('email') : $serviser->email) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>
					<div class="row">
						<div class="columns medium-12 {{ $errors->first('lozinka') ? ' error' : '' }}">
							<label for="">Lozinka</label>
							<input type="text" name="lozinka" data-id="" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : $serviser->lozinka) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>
					<div class="row">
						<div class="columns medium-12 {{ $errors->first('adresa') ? ' error' : '' }}">
							<label for="">Adresa</label>
							<input type="text" name="adresa" data-id="" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : $serviser->adresa) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>
					<div class="row">
						<div class="columns medium-12 {{ $errors->first('mesto') ? ' error' : '' }}">
							<label for="">Mesto</label>
							<input type="text" name="mesto" data-id="" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : $serviser->mesto) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>
		 
					<div class="row">
						<div class="columns medium-6 {{ $errors->first('telefon') ? ' error' : '' }}">
							<label for="">Telefon</label>
							<input type="text" name="telefon" data-id="" value="{{ htmlentities(Input::old('telefon') ? Input::old('telefon') : $serviser->telefon) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						<div class="columns medium-6">
							<label for="">Aktivan</label>
							<select name="aktivan" {{ Admin_model::check_admin(array('RMA_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@if(Input::old('aktivan'))
									@if(Input::old('aktivan'))
									<option value="1" selected>DA</option>
									<option value="0" >NE</option>
									@else
									<option value="1" >DA</option>
									<option value="0" selected>NE</option>
									@endif
								@else
									@if($serviser->aktivan)
									<option value="1" selected>DA</option>
									<option value="0" >NE</option>
									@else
									<option value="1" >DA</option>
									<option value="0" selected>NE</option>
									@endif
								@endif
							</select>
						</div>
					</div>
					
					@if(Admin_model::check_admin(array('RMA_AZURIRANJE')))
					    <div class="row">
							<div class="btn-container center">
								<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
								@if($serviser_id > 0)
									<button class="btn btn-danger JSbtn-delete" data-link="{{ RmaOptions::base_url() }}rma/serviseri/{{ $servis_id }}/{{ $serviser_id }}/delete">Obriši</button>
								@endif
							</div>
						</div>	
					@endif				
				</form>				
 			</div>  
		</section>
		@endif
	</div> 
</section>
@endsection