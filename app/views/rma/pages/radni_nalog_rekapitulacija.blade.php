<!DOCTYPE html>
<html><head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>PDF</title>
<style>
* { 
	box-sizing: border-box;
 	padding: 0;
	margin: 0; 
	line-height: 1;	
} 
body {
	font-size: 14px;  
	font-family: DejaVu Sans;
}
ul { list-style-type: none; }

table {border-collapse: collapse; margin-bottom: 15px; width: 100%;}

table tr td, table tr th {
	border: 1px solid #cacaca; 
	text-align: left;
	padding: 3px 5px;
}
 
.row::after {content: ""; clear: both; display: table;}
[class*="col-"] { float: left; padding: 5px; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}
.inline-block { display: inline-block; }
.text-right{ text-align: right; }
.text-center{ text-align: center; } 
.relative { position: relative; }

img{
	max-width: 100%;
}
.header{
	border-bottom: 4px solid #000; 
	padding: 10px 5px 5px;
}
.header .logo {
	max-height: 100px;  
	margin: 0 20px;
} 
p, table tr td, table tr th{
	font-size: 13px;
}  
.main-content {
	padding: 15px;   
}
.main-content .product-img{
	max-height: 220px;
} 
.main-content .foo_1{
	font-size: 20px;
	margin: 15px 0;
	padding: 5px 0;
	border-bottom: 1px solid #ddd;
} 
.main-content .foo_2{
	font-size: 25px;
	margin: 15px 0;
	color: #cc0000;
} 
.main-content .foo_2 span{
	font-size: 90%;
} 
.main-content .foo_3 .ul_1,.main-content .foo_3 .genkar{ 
	border: 1px solid #ddd; 
}  
.main-content .foo_3 .ul_1 .li_1{ 
	width: 30%; 
}
.main-content .foo_3 .ul_1 .li_2{ 
	width: 60%; 
	border-bottom: 1px solid #ddd;
} 
.main-content .foo_3 ul li{
    font-size: 12px; 
    padding: 2px 10px;      
    width: 45%;
    display: inline-block;   
}      
.main-content .foo_3 .genkar li{
  	border-bottom: 1px solid #ddd;
}    
.footer{
	background: #464853; 
	color: #fff;
	padding: 20px 15px;
	position: absolute;
	bottom: 0;
	font-size: 12px;
}

</style>
</head><body>
	<div class="container relative"> 
		<div class="row header">
			<div class="col-6">
				<img class="logo" src="{{ Options::base_url()}}{{Options::company_logo()}}" alt="logo">
			</div>
			<div class="col-5">
				<div>
					<p>Firma: {{RmaOptions::company_name()}}</p>
					<p>Adresa: {{RmaOptions::company_adress()}}</p>
		<!-- 			<p>Servis: {{RmaOptions::company_phone()}}</p>
					<p>Centrala: {{RmaOptions::company_fax()}}</p> -->
					<!-- <p>PIB: {{RmaOptions::company_pib()}}</p> -->
					<!-- <p>E-mail: {{RmaOptions::company_email()}}</p> -->
					<p>E-mail: {{RmaOptions::company_email()}}</p>
					<p>Telefon: {{RmaOptions::company_phone()}}</p>
				</div>
			</div>
		</div>

 
	<div class="main-content"> 
		<div class="row">
	  		<div class="col-12">  
				<p><span>Rekapitulacija servisnog naloga br:</span> {{ $radni_nalog->broj_naloga }}</p>
		  		<p>Datum servisa: {{ RMA::format_datuma($radni_nalog->datum_prijema,true) }}</p>  
			</div>
		</div>
		<br>
		<div class="row">
	  		<div class="col-12">
				<table>
					<tr>
						<th style="width: 5%;">Rbr.</th>
						<th style="width: 40%;">Naziv operacije / Usluga / Rezervni deo</th>
						<th style="width: 15%;">Radni sat /Kol</th>
						<th style="width: 15%;">Cena</th>
						<td style="width: 15%;">Svega</td>
					</tr>
					@foreach($radni_nalog_operacije as $op_key => $operacija)
					<tr>
						<th class="text-center">{{ ($op_key+1) }}</th>
						<td>{{ $operacija->opis_operacije }}</td>
						<td>{{ $operacija->norma_sat }}</td>
						<td>{{ RMA::format_cene($operacija->iznos/$operacija->norma_sat) }}</td>
						<!-- <td>{{ RMA::format_cene($operacija->iznos / (1 + $operacija->porez/100)) }}</td> -->
						<td>{{ RMA::format_cene($operacija->iznos) }}</td>
					</tr>
					@endforeach

					@foreach($radni_nalog_rezervni_delovi as $rd_key => $radni_nalog_rezervni_deo)
					<tr>
						<th class="text-center">{{ (count($radni_nalog_operacije)+$rd_key+1) }}</th>
						<td>{{ RMA::find($radni_nalog_rezervni_deo->roba_id,'naziv_web') }}</td>
						<td>{{ $radni_nalog_rezervni_deo->kolicina }}</td>
						@if(RmaOptions::gnrl_options(3043) == 1)
						<td>{{ RMA::format_cene($radni_nalog_rezervni_deo->cena) }}</td>
						<td>{{ RMA::format_cene($radni_nalog_rezervni_deo->iznos) }}</td>
						@else
						<td></td>
						<td></td>
						@endif
					</tr>
					@endforeach

					@foreach($radni_nalog_troskovi as $tr_key => $trosak)
					<tr>
						<th class="text-center">{{ (count($radni_nalog_operacije)+count($radni_nalog_rezervni_delovi)+$tr_key+1) }}</th>
						<td>{{ $trosak->opis }}</td>
						<td>1</td>
						<td>{{ RMA::format_cene($trosak->vrednost_racuna) }}</td>
						<td>{{ RMA::format_cene($trosak->vrednost_racuna) }}</td>
					</tr>
					@endforeach

					<tr>
						<th></th>
						<td></td>
						<td></td>
						<td>Ukupno:</td>
						@if(RmaOptions::gnrl_options(3043) == 1)
						<td>{{ RMA::format_cene($cene->ukupna_cena_rada) }}</td>
						@else
						<td>{{ RMA::format_cene($cene->cena_rada) }}</td>
						@endif
					</tr>
		<!-- 			<tr>
						<th></th>
						<td></td>
						<td></td>
						<td>PDV:</td>
						@if(RmaOptions::gnrl_options(3043) == 1)
						<td>{{ RMA::format_cene($cene->pdv_cena) }}</td>
						@else
						<td>{{ RMA::format_cene($cene->cena_rada*0.2) }}</td>
						@endif
					</tr> -->
	<!-- 				<tr>
						<th></th>
						<td></td>
						<td></td>
						<td>Ukupno:</td>
						@if(RmaOptions::gnrl_options(3043) == 1)
						<td>{{ RMA::format_cene($cene->ukupno) }}</td>
						@else
						<td>{{ RMA::format_cene($cene->cena_rada*1.2) }}</td>
						@endif
					</tr> -->
				</table>
			</div>
		 
		<div class="row"><br>
			<div class="col-12">
				<p>Napomena: {{ $radni_nalog->napomena }}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<p>Datum: {{ RMA::format_datuma($radni_nalog->datum_zavrsetka,true) }}</p>
			</div>
	 	</div>
	 	<div class="row"><br>
	  		<div class="col-5">
	  			<p style="text-align: center;">Serviser</p>
	  		</div>
	  		<div class="col-5">
	  			<p style="text-align: center;">Korisnik</p>
	  		</div>
	  	</div>
	 	<div class="row">
	  		<div class="col-5" style="text-align: center;">
	  			----------------------------
	  		</div>
	  		<div class="col-5" style="text-align: center;">
	  			----------------------------
	  		</div>
	  	</div> 
	  	</div>
	  	</div>
	</div>  
</body></html>
