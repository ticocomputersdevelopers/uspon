<section class="comments-page" id="main-content">

	@include('admin/partials/tabs')

	<div class="row">
		
		<section class="columns medium-6 medium-centered">
			<div class="flat-box">
				@if($solution == 'b2c')
				<form action="{{AdminOptions::base_url()}}admin/komentari/b2c/{{$comment->web_b2c_komentar_id}}" method="POST">
					<input name="web_b2c_komentar_id" type="hidden" value="{{ $comment->web_b2c_komentar_id }}" />
					
					<div class="row"> 
						<div class="columns">
							<label for="ime_osobe">{{ AdminLanguage::transAdmin('Ime') }}</label>
							<input name="ime_osobe" type="text" value="{{ $comment->ime_osobe }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }} />

							<label for="pitanje">{{ AdminLanguage::transAdmin('Komentar') }}</label>
							<textarea name="pitanje" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $comment->pitanje }}</textarea>

							<label for="odgovor">{{ AdminLanguage::transAdmin('Odgovor') }}</label>
							<textarea name="odgovor" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $comment->odgovor }}</textarea>

							<label for="">{{ AdminLanguage::transAdmin('Datum (G-M-D)') }}</label>
							<input style="width: 80px;" type="text" name="datum" value="{{ $comment->datum }}" disabled/>

							<label>{{ AdminLanguage::transAdmin('Artikal') }}</label>
							<a href="{{AdminArticles::article_link($comment->roba_id)}}" target="_blank">
								{{AdminArticles::find($comment->roba_id, 'naziv')}}
							</a>

							<label for="">{{ AdminLanguage::transAdmin('Ocena') }}</label>
							<input class="ordered-number" type="text" name="ocena" value="{{ $comment->ocena }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }} />
 
							&nbsp;
 	
							<label class="inline-block"> 
								@if($comment->komentar_odobren == 1)
								<input name="komentar_odobren" type="checkbox" checked {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>{{ AdminLanguage::transAdmin('Dozvoli komentar') }}
								@else
								<input name="komentar_odobren" type="checkbox" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>{{ AdminLanguage::transAdmin('Dozvoli komentar') }}
								@endif
							</label> 
							
							&nbsp;

							<label class="inline-block"> 
								@if($comment->odgovoreno == 1)
								<input name="odgovoreno" type="checkbox" checked {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>{{ AdminLanguage::transAdmin('Odgovoreno') }}
								@else
								<input name="odgovoreno" type="checkbox" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>{{ AdminLanguage::transAdmin('Odgovoreno') }}
								@endif
							</label>

							@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
							<div class="text-center btn-container">
								<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>

								<button class="btn btn-danger JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/komentari/{{$comment->web_b2c_komentar_id}}/b2c/delete">{{ AdminLanguage::transAdmin('Obriši') }}</button> 
							</div>
							@endif
						</div>
					</div>
				</form>
				@else
				<form action="{{AdminOptions::base_url()}}admin/komentari/b2b/{{$comment->web_b2b_komentar_id}}" method="POST">
					<input name="web_b2b_komentar_id" type="hidden" value="{{ $comment->web_b2b_komentar_id }}" />
					
					<div class="row"> 
						<div class="columns">
							<label for="ime_osobe">{{ AdminLanguage::transAdmin('Ime') }}</label>
							<input name="ime_osobe" type="text" value="{{ $comment->ime_osobe }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }} />

							<label for="pitanje">{{ AdminLanguage::transAdmin('Komentar') }}</label>
							<textarea name="pitanje" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $comment->pitanje }}</textarea>

							<label for="odgovor">{{ AdminLanguage::transAdmin('Odgovor') }}</label>
							<textarea name="odgovor" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $comment->odgovor }}</textarea>

							<label for="">{{ AdminLanguage::transAdmin('Datum (G-M-D)') }}</label>
							<input style="width: 80px;" type="text" name="datum" value="{{ $comment->datum }}" disabled/>

							<label>{{ AdminLanguage::transAdmin('Artikal') }}</label>
							@if(!AdminOptions::checkB2C())
							<a href="{{AdminArticles::article_link($comment->roba_id)}}" target="_blank">
								{{AdminArticles::find($comment->roba_id, 'naziv')}}
							</a>
							@else
							<a href="{{AdminOptions::base_url().'b2b/'.str_replace(AdminOptions::base_url(),"",AdminArticles::article_link($comment->roba_id))}}" target="_blank">
								{{AdminArticles::find($comment->roba_id, 'naziv')}}
							</a>
							@endif

							<label for="">{{ AdminLanguage::transAdmin('Ocena') }}</label>
							<input class="ordered-number" type="text" name="ocena" value="{{ $comment->ocena }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }} />
 
							&nbsp;
 	
							<label class="inline-block"> 
								@if($comment->komentar_odobren == 1)
								<input name="komentar_odobren" type="checkbox" checked {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>{{ AdminLanguage::transAdmin('Dozvoli komentar') }}
								@else
								<input name="komentar_odobren" type="checkbox" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>{{ AdminLanguage::transAdmin('Dozvoli komentar') }}
								@endif
							</label> 
							
							&nbsp;

							<label class="inline-block"> 
								@if($comment->odgovoreno == 1)
								<input name="odgovoreno" type="checkbox" checked {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>{{ AdminLanguage::transAdmin('Odgovoreno') }}
								@else
								<input name="odgovoreno" type="checkbox" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>{{ AdminLanguage::transAdmin('Odgovoreno') }}
								@endif
							</label>

							@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
							<div class="text-center btn-container">
								<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>

								<button class="btn btn-danger JSbtn-delete" data-link="{{AdminOptions::base_url()}}admin/komentari/{{$comment->web_b2b_komentar_id}}/b2b/delete">{{ AdminLanguage::transAdmin('Obriši') }}</button> 
							</div>
							@endif
						</div>
					</div>
				</form>
				@endif
			</div> 
		</section> 
	</div>
</section>