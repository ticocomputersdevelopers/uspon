<section id="main-content">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif
	
	@if(Session::has('alert'))
	<script>
		alertify.error('{{ Session::get('alert') }}');
	</script>
	@endif
	
	<div class="m-subnav"> 
		<a class="m-subnav__link JS-nazad" href="#">
			<div class="m-subnav__link__icon">
				<i class="fa fa-arrow-left" aria-hidden="true"></i>
			</div>

			<div class="m-subnav__link__text">
				{{ AdminLanguage::transAdmin('Nazad') }}
			</div>
		</a> 
	</div>
	
	<form class="flat-box" method="POST" action="{{AdminOptions::base_url()}}admin/narudzbina">
		<input type="hidden" id="narudzbina_id" name="web_b2c_narudzbina_id" value="{{ $web_b2c_narudzbina_id }}">
		
		<div class="row">
			<div class="columns medium-6"> 
				<div class="row"> 
					<div class="medium-6 columns">
				<?php $kupac = AdminKupci::kupacPodaci(!is_null(Input::old('web_kupac_id')) ? Input::old('web_kupac_id') : $web_kupac_id); ?>
						@if($kupac->flag_vrsta_kupca != 0)
						<label for="">{{ AdminLanguage::transAdmin('Pravno lice') }}</label>
						@else
						<label for="">{{ AdminLanguage::transAdmin('Ime kupca') }}</label>
						@endif
						<select name="web_kupac_id" class="search_select" id="JSWebKupac" class="{{ $errors->first('web_kupac_id') ? 'error' : '' }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }} >
							{{ AdminPartneri::kupciSelect(!is_null(Input::old('web_kupac_id')) ? Input::old('web_kupac_id') : $web_kupac_id) }}
						</select>
						@if($realizovano != 1 and $stornirano != 1)
						<a href="#" id="JSKupacDetail" class="btn btn-primary btn-small tooltipz" aria-label="{{ AdminLanguage::transAdmin('Izmeni') }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
						@if(Admin_model::check_admin(array('KUPCI_I_PARTNERI_AZURIRANJE')))
						<a href="{{ AdminOptions::base_url() }}admin/kupci_partneri/kupci/0" class="btn btn-primary btn-small" target="_blank">{{ AdminLanguage::transAdmin('Kreiraj kupca') }}</a>
						@endif
						@endif
					</div>

					@if($web_kupac_id >0)
					<div class="medium-6 columns">
						<label for="">{{ AdminLanguage::transAdmin('Istorija narudžbina') }}</label>
						<select data-order="{{$web_b2c_narudzbina_id}}" id="OrderSelect" {{(AdminPartneri::getKupacStatus($web_kupac_id) != 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>		
							<option value="{{ AdminPartneri::narudzbineSelect($web_kupac_id) }}" ></option>
						</select>
					</div>
					@endif
				</div>

				<?php $kupac = AdminKupci::kupacPodaci(!is_null(Input::old('web_kupac_id')) ? Input::old('web_kupac_id') : $web_kupac_id); ?>
				<div id="JSKupacAjaxContent" class="row">
					@include('admin/partials/ajax/narudzbina_kupac')
				</div>

				<br>
				
				<div class="row"> 
					<div class="columns" style="visibility: {{ ($errors->first() || $web_b2c_narudzbina_id == 0) ? 'hidden' : 'visible' }}">
						@if($prihvaceno == 0 and $realizovano == 0 and $stornirano == 0)			
						@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
						<a href="#" class="JS-prihvati btn btn-create">{{ AdminLanguage::transAdmin('PRIHVATI') }}</a>
						<a href="#" class="JS-storniraj btn btn-primary">{{ AdminLanguage::transAdmin('STORNIRAJ') }}</a>
						<a href="#" class="JS-delete btn btn-primary">{{ AdminLanguage::transAdmin('OBRIŠI') }}</a>
						@endif
						<p class="order-status-p"> {{ AdminLanguage::transAdmin('Status narudžbine: Nova') }}</p>

						@elseif($prihvaceno != 0 and $realizovano == 0 and $stornirano == 0)
						@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
						<a href="#" class="JS-realizuj btn btn-create">{{ AdminLanguage::transAdmin('REALIZUJ') }}</a>
						<a href="#" class="JS-storniraj btn btn-primary">{{ AdminLanguage::transAdmin('STORNIRAJ') }}</a>
						@endif
						
						<label class="inline-block"> 
							<input id="poslato" name="posta_slanje_poslato" type="checkbox" {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
							{{ AdminLanguage::transAdmin('Poslato') }} 
						</label>

						<p class="order-status-p">{{ AdminLanguage::transAdmin('Status narudžbine: Prihvaćena') }}</p>
						@elseif($realizovano != 0 and $stornirano == 0)
						<a href="#" class="JS-storniraj btn btn-primary">{{ AdminLanguage::transAdmin('STORNIRAJ') }}</a>								
						<p class="order-status-p">{{ AdminLanguage::transAdmin('Status narudžbine: Realizovana') }}</p>
						@elseif($stornirano != 0)
						<p class="order-status-p">{{ AdminLanguage::transAdmin('Status narudžbine: Stornirana') }}</p>
						<a href="#" class="JS-nestorniraj btn btn-primary">{{ AdminLanguage::transAdmin('VRATI IZ STORNIRANIH') }}</a>
						@endif
					</div>
				</div>
			</div>

			<div class="columns medium-6">
				<div class="row"> 
					<div class="columns medium-6">
						<label for="">{{ AdminLanguage::transAdmin('Broj dokumenta') }}</label>
						<input name="broj_dokumenta" type="text" class="{{ $errors->first('broj_dokumenta') ? 'error' : '' }}" value="{{ !is_null(Input::old('broj_dokumenta')) ? Input::old('broj_dokumenta') : $broj_dokumenta }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="columns medium-6">
						<label for="">{{ AdminLanguage::transAdmin('Datum') }}</label>
						<input id="order-date" class="datum-val has-tooltip" name="datum_dokumenta" type="text" value="{{ Input::old('datum_dokumenta') ? Input::old('datum_dokumenta') : $datum_dokumenta }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
					</div>
				</div>

				<div class="row"> 
					<div class="columns medium-6">
						<label for="">{{ AdminLanguage::transAdmin('Način plaćanja') }}</label>
						<select name="web_nacin_placanja_id" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
							{{ AdminPartneri::nacinPlacanja(!is_null(Input::old('web_nacin_placanja_id')) ? Input::old('web_nacin_placanja_id') : $web_nacin_placanja_id) }}
						</select>
					</div>

					<div class="columns medium-6">
						<label for="">{{ AdminLanguage::transAdmin('Način isporuke') }}</label>
						<select name="web_nacin_isporuke_id" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
							{{ AdminPartneri::nacinIsporuke(!is_null(Input::old('web_nacin_isporuke_id')) ? Input::old('web_nacin_isporuke_id') : $web_nacin_isporuke_id) }}
						</select>
					</div>
				</div>

				<div class="row"> 
					<div class="columns medium-6">
						<label for="">{{ AdminLanguage::transAdmin('Kurirska služba') }}</label>
						<select name="posta_slanje_id" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
							{{ AdminPartneri::kurirskaSluzba(!is_null(Input::old('posta_slanje_id')) ? Input::old('posta_slanje_id') : $posta_slanje_id) }}
						</select>
					</div>

					<div class="columns medium-6">
						<label for="">{{ AdminLanguage::transAdmin('Dodeli status') }}</label>
						<select  name="narudzbina_status_id[]" id="narudzbina_status_id" {{($stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }} multiple="Dodeli status">
						@foreach(AdminNarudzbine::dodatni_statusi() as $statusi)
							@if(in_array(strval($statusi->narudzbina_status_id),$narudzbina_status_id))	
							<option value="{{ $statusi->narudzbina_status_id }}" selected>{{ $statusi->naziv }}</option>
							@else
							<option value="{{ $statusi->narudzbina_status_id }}">{{ $statusi->naziv }}</option>
							@endif				
						@endforeach
						</select>
					</div>
				</div>

				<div class="row"> 	
					<div class="columns medium-6">
						<label for="">{{ AdminLanguage::transAdmin('IP adresa') }}</label>
						<input name="ip_adresa" type="text" value="{{ Input::old('ip_adresa') ? Input::old('ip_adresa') : $ip_adresa }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>	

					<div class="columns medium-6">
						<label for="">{{ AdminLanguage::transAdmin('Broj pošiljke') }}</label>
						<input name="posta_slanje_broj_posiljke" type="text" class="{{ $errors->first('posta_slanje_broj_posiljke') ? 'error' : '' }}" value="{{ !is_null(Input::old('posta_slanje_broj_posiljke')) ? Input::old('posta_slanje_broj_posiljke') : $posta_slanje_broj_posiljke }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>
				</div>

				<div class="row"> 	
					<div class="columns">
						<label for="">{{ AdminLanguage::transAdmin('Napomena') }}</label>
						<textarea name="napomena" rows="2" >{{ !is_null(Input::old('napomena')) ? Input::old('napomena') : $napomena }}</textarea>
					</div>
					<div class="columns">
						<label for="">{{ AdminLanguage::transAdmin('Komentar') }}</label>
						<textarea name="komentar" rows="2" >{{ !is_null(Input::old('komentar')) ? Input::old('komentar') : $komentar }}</textarea>
					</div>
				</div>
			</div>

			<div class="columns text-center"> 
				<div class="btn-container">
					
					<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					

					@if(!$errors->first() AND $web_b2c_narudzbina_id != 0)
					<a target="_blank" href="/admin/pdf/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">PDF</a>

					@if($realizovano == 1 and $stornirano == 0)
					<a target="_blank" href="/admin/pdf_racun/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">{{ AdminLanguage::transAdmin('Račun') }}</a>
					@elseif($realizovano == 0 and $stornirano == 0 and $prihvaceno == 1)
					<a target="_blank" href="/admin/pdf_ponuda/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">{{ AdminLanguage::transAdmin('Ponuda') }}</a>
					<a target="_blank" href="/admin/pdf_predracun/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">{{ AdminLanguage::transAdmin('Predračun') }}</a>
					@elseif($stornirano == 1)
					<a target="_blank" href="/admin/pdf_racun/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">{{ AdminLanguage::transAdmin('Račun') }}</a>
					@else
					<a target="_blank" href="/admin/pdf_ponuda/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">{{ AdminLanguage::transAdmin('Ponuda') }}</a>
					<a target="_blank" href="/admin/pdf_predracun/{{ $web_b2c_narudzbina_id }}" class="btn btn-primary">{{ AdminLanguage::transAdmin('Predračun') }}</a>
					@endif
					@endif
				</div>
			</div>
		</div> <!-- end of .row -->
	</form> 

	@if(!$errors->first() AND $web_b2c_narudzbina_id != 0)
	
	<div class="flat-box">
		<div class="row">

			<div class="columns medium-12 large-12">
				<div class="add-to-order">
					<label> {{ AdminLanguage::transAdmin('Dodaj artikal narudžbini') }}</label>
				</div>
				@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
				<div class="relative">
					<input class="" autocomplete="off" data-timer="" type="text" id="JSsearch_porudzbine" data-id="" placeholder="Unesite id,sku ili ime artikla ili prozvođača ili krajnju grupu" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} />
					<div id="search_content"></div>
				</div>
				@endif

				<div class="items-order">
					<table class="table table_narudzbina">
						<thead>
							<tr>
								@if(AdminOptions::sifra_view()==1)
								<th>{{ AdminLanguage::transAdmin('ROBA ID') }}</th>
								@elseif(AdminOptions::sifra_view()==4)
								<th>{{ AdminLanguage::transAdmin('ŠIFRA') }}</th>
								@elseif(AdminOptions::sifra_view()==3)
								<th>{{ AdminLanguage::transAdmin('SKU') }}</th>
								@elseif(AdminOptions::sifra_view()==2)
								<th>{{ AdminLanguage::transAdmin('ŠIFRA_IS') }}</th>
								@endif
								<th>{{ AdminLanguage::transAdmin('KOL') }}</th>
								<th>{{ AdminLanguage::transAdmin('NAZIV') }}</th>
								<th class="hide-for-small-only">{{ AdminLanguage::transAdmin('LAGER') }}</th>
								<th data-coll="cena">{{ AdminLanguage::transAdmin('CENA PO JEDINICI') }}</th>
								<th data-coll="dobavljac">{{ AdminLanguage::transAdmin('DOBAVLJAC') }}</th>

								@if(AdminNarudzbine::api_posta(2) OR AdminNarudzbine::api_posta(3))

								<th>{{ AdminLanguage::transAdmin('POŠILJALAC') }}</th>
								<th class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('KURIR. SLUŽBA') }}">
								{{ AdminLanguage::transAdmin('KURIR. SLUŽBA') }}</th>
								<th class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('POŠTA STATUS') }}">
								{{ AdminLanguage::transAdmin('POŠTA STATUS') }}</th>
								<th class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('POŠTA ZAHTEV') }}">
								{{ AdminLanguage::transAdmin('POŠTA ZAHTEV') }}</th>
								@endif
								@if($realizovano != 1 and $stornirano != 1)
								<th></th>
								@endif 
							</tr>
						</thead>

						@foreach($narudzbina_stavke as $row)
						<tr>
							<td>
								@if(AdminOptions::sifra_view()==1)
								&nbsp;{{ AdminArticles::find($row->roba_id, 'roba_id')}}
								@elseif(AdminOptions::sifra_view()==4)
								&nbsp;{{ AdminArticles::find($row->roba_id, 'sifra_d') }}
								@elseif(AdminOptions::sifra_view()==3)
								&nbsp;{{ AdminArticles::find($row->roba_id, 'sku') }}
								@elseif(AdminOptions::sifra_view()==2)
								&nbsp;{{ AdminArticles::find($row->roba_id, 'sifra_is') }}
								@endif
							</td>
							<td>
								<?php
									// AdminOptions::vodjenje_lagera()==1 ? $max=AdminSupport::lager($row->roba_id)+AdminNarudzbine::kolicina($web_b2c_narudzbina_id,$row->roba_id) : $max=1000;
									$max=1000000000;
								?>
								@if(AdminOptions::web_options(320)==1)
								@if( (AdminArticles::jedinica_mere($row->roba_id)->jedinica_mere_id) == 3 AND Product::pakovanje($row->roba_id) ) 
								<input class="kolicina show-arrow-mozz ammount-input kol-inline" data-stavka-id="{{ $row->web_b2c_narudzbina_stavka_id }}" type="number" step="0.1" min="0.1" max="{{ $max }}" value="{{ $row->kolicina }}" data-old-kolicina="{{ $row->kolicina }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}><span>&nbsp;{{Language::trans('kg')}}</span>
								@else
								<input class="kolicina show-arrow-mozz ammount-input kol-inline" data-stavka-id="{{ $row->web_b2c_narudzbina_stavka_id }}" type="number" min="1" max="{{ $max }}" value="{{ round($row->kolicina) }}" data-old-kolicina="{{ round($row->kolicina) }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@endif
								@else	
								<input class="kolicina show-arrow-mozz ammount-input kol-inline" data-stavka-id="{{ $row->web_b2c_narudzbina_stavka_id }}" type="number" min="1" max="{{ $max }}" value="{{ round($row->kolicina) }}" data-old-kolicina="{{ round($row->kolicina) }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@endif	
							</td>
							<td class="narudzbina-td-naziv">
								<span class="inline-block max-width-450 no-white-space">
									{{ AdminArticles::find($row->roba_id, 'naziv') }}
									{{ AdminOsobine::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids) }}
									@if(!empty($row->project_id))
										<div> 
											<a href="https://pdf.pitchprint.io?id={{$row->project_id}}" target="_blank" class="btn btn-small btn-secondary">{{ AdminLanguage::transAdmin('Dizajn')}}</a>
										</div>
									@endif
								</span>
							</td>
							<td class="JSLager hide-for-small-only">
								{{ AdminSupport::lager($row->roba_id) }}
							</td>	

							@if($realizovano != 1 and $stornirano != 1)
							<td class="cena">
								<div class="relative"> 
									<span class="JSCenaVrednost">
										{{ number_format($row->jm_cena, 2, '.', '') }}
									</span>
									<span class="JSEditCenaBtnSpan">
										<input type="text" data-stavka-id="{{ $row->web_b2c_narudzbina_stavka_id }}" class="JSCenaInput" value="{{ number_format($row->jm_cena, 2, '.', '') }} " >
										@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
										<i class="fa fa-pencil JSEditCenaBtn" aria-hidden="true"></i>
										@endif
									</span>
								</div>
							</td>
							@else
							<td class="cena">
								<div class="relative"> 
									<span class="JSCenaVrednost">
										{{ number_format($row->jm_cena, 2, '.', '') }}
									</span>
								</div>	
							</td>
							@endif
							@if(!is_null(AdminNarudzbine::dobavljac($row->roba_id)))
							<td class="dobavljac">
								{{ AdminNarudzbine::dobavljac($row->roba_id)->naziv }}
							</td>
							@else
							<td>

							</td>
							@endif


							@if(AdminNarudzbine::api_posta(2))
							
							<td>{{ AdminPartneri::partner($row->posiljalac_id)->naziv }}</td>
							<?php $posta_slanje_naziv = AdminSupport::find_kurirska_sluzba($row->posta_slanje_id, 'naziv'); ?>
							<td>{{ !is_null($posta_slanje_naziv) ? $posta_slanje_naziv : AdminSupport::find_kurirska_sluzba($posta_slanje_id, 'naziv') }}</td>
							
							@if(!is_null($row->posta_status_id))
							<td class="tab-widtd-2">{{AdminSupport::posta_status($row->posta_status_id)->name}}</td>
							@else
							<td></td>
							@endif
							<td> 
								@if($row->posta_zahtev == 1)
								<span style="color: red;">{{ AdminLanguage::transAdmin('POSLATO') }}</span>
								@endif
							</td>		

							<td>							
								@if($row->posta_slanje_id == 3 )
								@if($row->posta_zahtev == 0)
								<a class="mail-btn" href="{{AdminOptions::base_url()}}admin/narudzbina-stavka/{{$row->web_b2c_narudzbina_stavka_id}}">{{ AdminLanguage::transAdmin('POŠTA') }}</a>
								@endif
								@endif 
							</td>
							@elseif(AdminNarudzbine::api_posta(3))

							<td>{{ AdminPartneri::partner($row->posiljalac_id)->naziv }}</td>
							<?php $posta_slanje_naziv = AdminSupport::find_kurirska_sluzba($row->posta_slanje_id, 'naziv'); ?>
							<td>{{ !is_null($posta_slanje_naziv) ? $posta_slanje_naziv : AdminSupport::find_kurirska_sluzba($posta_slanje_id, 'naziv') }}</td>
							@if(!is_null($row->posta_status_id))
							<td class="tab-widtd-2">{{AdminSupport::posta_status($row->posta_status_id)->name}}</td>
							@else
							<td></td>
							@endif

							<td> 
								@if($row->posta_zahtev == 1)
								<span style="color: red;">{{ AdminLanguage::transAdmin('POSLATO') }}</span>
								@endif
							</td>
							<td>
								@if($row->posta_zahtev == 0)
								<a class="mail-btn" href="{{AdminOptions::base_url()}}admin/narudzbina-stavka/{{$row->web_b2c_narudzbina_stavka_id}}">{{ AdminLanguage::transAdmin('POŠTA') }}</a>
								@endif
							</td>

							@endif

							@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
							@if($realizovano != 1 and $stornirano != 1)
							<td class="text-center">
								<button data-stavka-id="{{ $row->web_b2c_narudzbina_stavka_id }}" class="JSDeleteStavka name-ul__li__remove button-option tooltipz" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}"><i class="fa fa-times" aria-hidden="true"></i></button>
							</td>
							@endif
							@endif
						</tr>
						@endforeach 

						<!-- ************************************* --> 

						@if($troskovi_isporuke>0 AND $web_nacin_isporuke_id !=2) 
						<tr> 
							<td colspan="100%" class="text-right">
								{{ AdminLanguage::transAdmin('Cena artikala') }}: 
								<span class="inline-block min-width-90" id="cena-artikla">{{ number_format(floatval($ukupna_cena),2,'.','') }}</span> 
							</td>
						</tr>

						<tr> 
							<td colspan="100%" class="trosak text-right">{{ AdminLanguage::transAdmin('Troškovi isporuke') }}: 
								<span class="relative custom-edit inline-block min-width-90">
									<span class="JStrosakVrednost" id="troskovi-isporuke">		
										{{ AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id) != 0 ? number_format(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id),2,'.','') : number_format($troskovi_isporuke,2,'.','') }}
									</span>										
									<span class="JSEdittrosakBtnSpan">
										<input style="height: auto;" type="text" class="JStrosakInput text-right" value="{{ AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id)>0 ? number_format(AdminCommon::cena_dostave_custom($web_b2c_narudzbina_id),2,'.','') : number_format($troskovi_isporuke,2,'.','') }} " >

										<i class="fa fa-pencil JSEdittrosakBtn" aria-hidden="true"></i>
									</span>										
								</span>
							</td> 
						</tr>

						@if($popust > 0)
						<tr>
							<td colspan="100%" class="text-right">
								{{ AdminLanguage::transAdmin('Popust') }}: 
								<span class="inline-block min-width-90" id="troskovi-isporuke">{{ $popust }}</span>
							</td> 
						</tr>
						@endif

						<?php $vaucer_popust = Order::vaucer_popust($web_b2c_narudzbina_id); ?>
                      	@if($vaucer_popust > 0)
						<tr>
							<td colspan="100%" class="text-right">
								{{ AdminLanguage::transAdmin('Vaucer popust') }}: 
								<span class="inline-block min-width-90" id="troskovi-isporuke">{{ $vaucer_popust }}</span>
							</td> 
						</tr>
						@endif

						<tr>
							<td colspan="100%" class="text-right">
								<b>{{ AdminLanguage::transAdmin('Ukupno za uplatu') }}:</b>  
								<span class="inline-block min-width-90" id="ukupna-cena"><b> {{ number_format(floatval($ukupna_cena+(!is_null($cena_dostave_custom) ? $cena_dostave_custom : $troskovi_isporuke)-$popust-$vaucer_popust),2,'.','') }}</b></span>
							</td>
						</tr>
						@else

						@if($popust > 0)
						<tr>
							<td colspan="100%" class="text-right">
								{{ AdminLanguage::transAdmin('Popust') }}:  
								<span class="inline-block min-width-90" id="troskovi-isporuke">{{ $popust }}</span>
							</td> 
						</tr>
						@endif

						<?php $vaucer_popust = Order::vaucer_popust($web_b2c_narudzbina_id); ?>
                      	@if($vaucer_popust > 0)
						<tr>
							<td colspan="100%" class="text-right">
								{{ AdminLanguage::transAdmin('Vaucer popust') }}: 
								<span class="inline-block min-width-90" id="troskovi-isporuke">{{ $vaucer_popust }}</span>
							</td> 
						</tr>
						@endif

						<tr>
							<td colspan="100%" class="text-right">
								<b>{{ AdminLanguage::transAdmin('Ukupno za uplatu') }}:</b> 
								<span class="inline-block min-width-90" id="ukupna-cena"><b>{{ number_format(floatval($ukupna_cena-$vaucer_popust),2,'.','') }}</b></span>

							</td>
						</tr>
						@endif

					</table>  
				</div>
			</div>

		</div> <!-- end of .row -->
	</div> 

	@if($posta_slanje_id == 2 ) 
	<div class="flat-box">
		<label>
			{{ AdminLanguage::transAdmin('Pošte slanja') }} 		
			@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
			<a href="{{AdminOptions::base_url()}}admin/dodaj-novu-city/{{$web_b2c_narudzbina_id}}/{{$id_posta_slanje}}" class="mail-btn">{{ AdminLanguage::transAdmin('Dodaj Novu') }}</a>
			@endif
		</label>

		<table> 
			@foreach($narudzbina_poste_slanja as $posta_slanje)
			@if($posta_slanje->posta_slanje_id == 3)
			<tr>
				<td>&nbsp; {{ $posta_slanje->naziv }}</td>
				
				<td>
					@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
					<a href="{{AdminOptions::base_url()}}admin/posta-posalji/{{ $web_b2c_narudzbina_id }}/{{ $posta_slanje->posta_slanje_id }}" class="mail-btn">{{ AdminLanguage::transAdmin('POŠALJI') }}</a>
					@endif
				</td>
				
				<td>
					@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
					<a href="{{AdminOptions::base_url()}}admin/posta-resetuj/{{ $web_b2c_narudzbina_id }}/{{ $posta_slanje->posta_slanje_id }}" class="mail-btn">{{ AdminLanguage::transAdmin('RESETUJ') }}</a>
					@endif
				</td>
			</tr>
			@elseif($posta_slanje->posta_slanje_id == 2)
			
			<tr>
				<th>{{ AdminLanguage::transAdmin('POŠTA') }}</th>
				<th>{{ AdminLanguage::transAdmin('PARTNER SLANJE') }}</th>
				<th>{{ AdminLanguage::transAdmin('ARTIKAL') }}</th>
				<th>{{ AdminLanguage::transAdmin('KOLIČINA') }}</th>
				<th></th>
				<th></th>
				<th></th>
			</tr>  

			@foreach(AdminNarudzbine::posta_slanje_narudzbine($web_b2c_narudzbina_id) as $narudzbina)
			<tr>
				<td>{{ $posta_slanje->naziv }} 
					<a href="{{AdminOptions::base_url()}}admin/obrisi-city/{{$web_b2c_narudzbina_id}}/{{$narudzbina->posta_zahtev_api_id}}"><i class="fa fa-times" aria-hidden="true"></i></a>
				</td>

				<td> 
					{{ AdminPartneri::partner_naziv($narudzbina->posiljalac_id) }} 
				</td>
				
				<td>
					@foreach(DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->where('posta_zahtev_api_id',$narudzbina->posta_zahtev_api_id)->get() as $stavka)
					<div>{{AdminNarudzbine::nazivweb($stavka->stavka_id) }}</div>
					@endforeach						
				</td>

				<td>
					@foreach(DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->where('posta_zahtev_api_id',$narudzbina->posta_zahtev_api_id)->get() as $stavka)
					<div>{{ number_format($stavka->kolicina,0, '.', '')}}</div>
					@endforeach						
				</td>

				<td>
					@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
					<a class="mail-btn" href="{{AdminOptions::base_url()}}admin/narudzbina-stavka-city/{{$narudzbina->web_b2c_narudzbina_id}}/{{$narudzbina->posta_zahtev_api_id}}">{{ AdminLanguage::transAdmin('UREDI') }}</a>
					@endif
				</td>

				<td>
					@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
						@if(AdminNarudzbine::city_odgovor_id($narudzbina->web_b2c_narudzbina_id,$narudzbina->posta_zahtev_api_id) =='')
						<a id="CityMailSend" href="{{AdminOptions::base_url()}}admin/posta-posalji-city/{{ $narudzbina->web_b2c_narudzbina_id }}/{{$narudzbina->posta_zahtev_api_id}}/{{ $posta_slanje->posta_slanje_id }}" data-narudzbina="{{$narudzbina->web_b2c_narudzbina_id}}" class="mail-btn">{{ AdminLanguage::transAdmin('POŠALJI ZAHTEV') }}</a>
						@endif
						@if(AdminNarudzbine::city_odgovor_id($narudzbina->web_b2c_narudzbina_id,$narudzbina->posta_zahtev_api_id) !='')
						<a href="{{AdminOptions::base_url()}}admin/priprema-za-slanje/{{ $narudzbina->web_b2c_narudzbina_id }}/{{$narudzbina->posta_zahtev_api_id}}" class="mail-btn">{{ AdminLanguage::transAdmin('PRIPREMI ZA SLANJE') }}</a>
						@endif
					@endif
					@if(AdminNarudzbine::city_odgovor_id($narudzbina->web_b2c_narudzbina_id,$narudzbina->posta_zahtev_api_id) != '' AND $narudzbina->posiljalac_id ==1)
					@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
					<a href="{{AdminOptions::base_url()}}admin/posta-nalepnice/{{ $narudzbina->web_b2c_narudzbina_id }}/{{$narudzbina->posta_zahtev_api_id}}" class="mail-btn">{{ AdminLanguage::transAdmin('ŠTAMPAJ NALEPNICU') }}</a>
						@if(AdminNarudzbine::posta_dokument_postoji($narudzbina->web_b2c_narudzbina_id,$narudzbina->posta_zahtev_api_id) != '' AND $narudzbina->posiljalac_id ==1)
						<a href="{{AdminOptions::base_url()}}admin/posta-dokument/{{ $narudzbina->web_b2c_narudzbina_id }}/{{$narudzbina->posta_zahtev_api_id}}" class="mail-btn">{{ AdminLanguage::transAdmin('LISTA ZA PREUZIMANJE') }}</a>
						@endif
					@endif
					@endif
				</td>

				<td>
					@if($narudzbina->posiljalac_id==1 AND AdminNarudzbine::city_odgovor_id($narudzbina->web_b2c_narudzbina_id,$narudzbina->posta_zahtev_api_id) !='')
					@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
					<a href="{{AdminOptions::base_url()}}admin/posta-otkazi-city/{{ $narudzbina->web_b2c_narudzbina_id }}/{{$narudzbina->posta_zahtev_api_id}}" class="mail-btn">{{ AdminLanguage::transAdmin('OTKAŽI') }}</a>
					@endif
					@endif
				</td>
			</tr> 			
			@endforeach 
			
			@endif
			@endforeach
		</table>  
	</div> 
	@endif
	@endif
	
</section>