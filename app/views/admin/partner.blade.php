<div id="main-content">
	@if(Session::has('message') AND !$dokumenti)
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	<div class="m-subnav"> 
		<a class="m-subnav__link JS-nazad" href="{{AdminOptions::base_url()}}{{!$dokumenti ? 'admin/kupci_partneri' : 'dokumenti'}}/partneri">
			<div class="m-subnav__link__icon">
				<i class="fa fa-arrow-left" aria-hidden="true"></i>
			</div>
			<div class="m-subnav__link__text">
				{{ AdminLanguage::transAdmin('Nazad') }}
			</div>
		</a>
	</div> 
 
		<form action="{{AdminOptions::base_url()}}{{!$dokumenti ? 'admin/kupci_partneri' : 'dokumenti'}}/partneri/{{ $partner_id }}/save" method="post" class="new-customer-form" autocomplete="false">

			<input type="hidden" id="partner_id" name="partner_id" value="{{ $partner_id }}">

			<div class="row">
				<div class="column medium-4">

					<div class="flat-box padding-h-8">
						<div>
							<label>{{ AdminLanguage::transAdmin('Naziv') }}</label>
							<input class="{{ $errors->first('naziv') ? 'error' : '' }}" id="naziv" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('naziv') }}</div>
						</div>

						<div>
							<label>{{ AdminLanguage::transAdmin('Puni naziv') }}</label>
							<input class="{{ $errors->first('naziv_puni') ? 'error' : '' }}" id="naziv_puni" name="naziv_puni" value="{{ Input::old('naziv_puni') ? Input::old('naziv_puni') : $naziv_puni }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						@if(AdminNarudzbine::api_posta(3))
						<div class="row"> 
							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Opština') }}</label>				 
								<select class="form-control" name="opstina" id="JSOpstina" tabindex="6" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									@foreach(AdminNarudzbine::opstine() as $key => $opstina)
									@if((Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($ulica_id)) == $opstina->narudzbina_opstina_id)
									<option value="{{ $opstina->narudzbina_opstina_id }}" selected>{{ $opstina->naziv }}</option>
									@else
									<option value="{{ $opstina->narudzbina_opstina_id }}">{{ $opstina->naziv }}</option>
									@endif
									@endforeach
								</select>
							</div> 

							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Naselje') }}</label>
								<select class="form-control" name="mesto_id" id="JSMesto" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									@foreach(AdminNarudzbine::mesta(Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($ulica_id)) as $key => $mesto_obj)
									@if((Input::old('mesto_id') ? Input::old('mesto_id') : AdminNarudzbine::mesto_id($ulica_id)) == $mesto_obj->narudzbina_mesto_id)
									<option value="{{ $mesto_obj->narudzbina_mesto_id }}" selected>{{ $mesto_obj->naziv }}</option>
									@else
									<option value="{{ $mesto_obj->narudzbina_mesto_id }}">{{ $mesto_obj->naziv }}</option>
									@endif
									@endforeach
								</select>
							</div>
						</div>

						<div class="row"> 
							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Ulica') }}</label>				 
								<select class="form-control" name="ulica_id" id="JSUlica" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									@foreach(AdminNarudzbine::ulice(Input::old('mesto_id') ? Input::old('mesto_id') : AdminNarudzbine::mesto_id($ulica_id)) as $key => $ulica)
									@if((Input::old('ulica_id') ? Input::old('ulica_id') : $ulica_id) == $ulica->narudzbina_ulica_id)
									<option value="{{ $ulica->narudzbina_ulica_id }}" selected>{{ $ulica->naziv }}</option>
									@else
									<option value="{{ $ulica->narudzbina_ulica_id }}">{{ $ulica->naziv }}</option>
									@endif
									@endforeach
								</select>
							</div>
					 

							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Broj') }}</label>
								<input class="{{ $errors->first('broj') ? 'error' : '' }}" id="without-reg-number" class="form-control" name="broj" type="text" value="{{ htmlentities(Input::old('broj') ? Input::old('broj') : $broj) }}" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('broj') }}</div>
							</div>
						</div>
						@endif	

						<div class="row"> 
							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Adresa') }}</label>
								<input class="{{ $errors->first('adresa') ? 'error' : '' }}" id="adresa" name="adresa" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : $adresa) }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('adresa') }}</div>
							</div>

							<div class="columns medium-6">
								<label> {{ AdminLanguage::transAdmin('Mesto/Grad') }}</label>
								<input class="{{ $errors->first('mesto') ? 'error' : '' }}" type="text" name="mesto" value="{{ Input::old('mesto') ? Input::old('mesto') : $mesto }}">
								<div class="error" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ $errors->first('mesto') }}</div>
							</div>
						</div>

						<div class="row"> 
							<div class="columns medium-6">
								<label for="">{{ AdminLanguage::transAdmin('Email') }}</label> 
								<input class="{{ $errors->first('mail') ? 'error' : '' }}" id="mail" autocomplete="off" name="mail" value="{{ Input::old('mail') ? Input::old('mail') : $mail }}" type="email" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
								<div class="error">{{ $errors->first('mail') }}</div>	
							</div>

							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Država') }}</label>
								<select type="text" name="drzava_id" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									{{ AdminKupci::listDrzava(Input::old('drzava_id') ? Input::old('drzava_id') : $drzava_id) }}
								</select>
							</div>
						</div>

						<div>
							<label>{{ AdminLanguage::transAdmin('Naziv delatnosti') }}</label>
							<input class="{{ $errors->first('delatnost_naziv') ? 'error' : '' }}" id="delatnost_naziv" name="delatnost_naziv" value="{{ Input::old('delatnost_naziv') ? Input::old('delatnost_naziv') : $delatnost_naziv }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div> 
						
						<div class="row"> 
							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Limit') }}</label>
								<input class="{{ $errors->first('limit_p') ? 'error' : '' }}" id="limit_p" name="limit_p" value="{{ Input::old('limit_p') ? Input::old('limit_p') : $limit_p }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('limit_p') }}</div>
							</div>

							<div class="columns medium-6">
								<label for="">{{ AdminLanguage::transAdmin('Valuta') }}</label>
								<select type="text" name="valuta_id" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									{{ AdminKupci::listValuta(Input::old('valuta_id') ? Input::old('valuta_id') : $valuta_id) }}
								</select>
							</div>
						</div>

						<div>
							<label>{{ AdminLanguage::transAdmin('Tip cene') }}</label>
							<select type="text" name="tip_cene_id" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								{{ AdminKupci::listTipCene(Input::old('tip_cene_id') ? Input::old('tip_cene_id') : $tip_cene_id) }}
							</select>
						</div>

						@if(!$dokumenti)
						@if(!is_null(AdminSupport::getLoggedUser('ADMIN')))
						<div>
							<label>{{ AdminLanguage::transAdmin('Komercijalista') }}</label>
							<select type="text" name="imenik_id[]" id="JSKomercijalista" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }} multiple="multiple">
								@foreach(AdminSupport::komercijaliste() as $komercijalista)
								@if(in_array($komercijalista->imenik_id,(Input::old('imenik_id') ? Input::old('imenik_id') : $imenik_id)))
								<option value="{{ $komercijalista->imenik_id }}" selected>{{ $komercijalista->ime }} {{ $komercijalista->prezime }}</option>
								@else
								<option value="{{ $komercijalista->imenik_id }}">{{ $komercijalista->ime }} {{ $komercijalista->prezime }}</option>
								@endif
								@endforeach
							</select>
						</div>
						@endif

						<div class="field-group">
							<label>{{ AdminLanguage::transAdmin('Napomena') }}</label>
							<textarea class="{{ $errors->first('napomena') ? 'error' : '' }}" id="napomena" name="napomena" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('napomena') ? Input::old('napomena') : $napomena }}</textarea>
							<div class="error">{{ $errors->first('napomena') }}</div>
						</div> 
						@endif
					</div> 
				</div>
 
				<div class="column medium-4">
					<div class="flat-box padding-h-8">

						<div>
							<label>{{ AdminLanguage::transAdmin('Kontakt Osoba') }}</label>
							<input type="text" class="{{ $errors->first('kontakt_osoba') ? 'error' : '' }}" id="kontakt_osoba" name="kontakt_osoba" value="{{ Input::old('kontakt_osoba') ? Input::old('kontakt_osoba') : $kontakt_osoba }}" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('kontakt_osoba') }}</div>
						</div>

						<div class="row"> 
							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Telefon') }}</label>
								<input type="text" class="{{ $errors->first('telefon') ? 'error' : '' }}" id="telefon" name="telefon" value="{{ Input::old('telefon') ? Input::old('telefon') : $telefon }}" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('telefon') }}</div>
							</div>

							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Fax') }}</label>
								<input class="{{ $errors->first('fax') ? 'error' : '' }}" id="fax" name="fax" value="{{ Input::old('fax') ? Input::old('fax') : $fax }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('fax') }}</div>
							</div>
						</div>

						<div class="row"> 
							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Šifra') }}</label>
								<input id="sifra" class="{{ $errors->first('sifra') ? 'error' : '' }}" name="sifra" value="{{ Input::old('sifra') ? Input::old('sifra') : $sifra }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('sifra') }}</div>
							</div>

							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Pib') }}</label>
								<input class="{{ $errors->first('pib') ? 'error' : '' }}" id="pib" name="pib" value="{{ Input::old('pib') ? Input::old('pib') : $pib }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('pib') }}</div>
							</div>
						</div>

						<div>
							<label>{{ AdminLanguage::transAdmin('Račun') }}</label>
							<input class="{{ $errors->first('racun') ? 'error' : '' }}" id="racun" autocomplete="off" name="racun" value="{{ Input::old('racun') ? Input::old('racun') : $racun }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('racun') }}</div>
						</div>

						<div class="row"> 
							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Šifra delatnosti') }}</label>
								<input class="{{ $errors->first('delatnost_sifra') ? 'error' : '' }}" id="delatnost_sifra" name="delatnost_sifra" value="{{ Input::old('delatnost_sifra') ? Input::old('delatnost_sifra') : $delatnost_sifra }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('delatnost_sifra') }}</div>
							</div>

							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Matični broj') }}</label>
								<input class="{{ $errors->first('broj_maticni') ? 'error' : '' }}" id="broj_maticni" name="broj_maticni" value="{{ Input::old('broj_maticni') ? Input::old('broj_maticni') : $broj_maticni }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('broj_maticni') }}</div>
							</div>
						</div>

						<div class="row"> 
							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Broj registra') }}</label>
								<input class="{{ $errors->first('broj_registra') ? 'error' : '' }}" id="broj_registra" name="broj_registra" value="{{ Input::old('broj_registra') ? Input::old('broj_registra') : $broj_registra }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('broj_registra') }}</div>
							</div>

							<div class="columns medium-6">
								<label>{{ AdminLanguage::transAdmin('Broj rešenja') }}</label>
								<input class="{{ $errors->first('broj_resenja') ? 'error' : '' }}" id="broj_resenja" name="broj_resenja" value="{{ Input::old('broj_resenja') ? Input::old('broj_resenja') : $broj_resenja }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<div class="error">{{ $errors->first('broj_resenja') }}</div>
							</div>
						</div>
					
						@if(!$dokumenti)
						<div>
							<label>{{ AdminLanguage::transAdmin('Rabat') }}</label>
							<div class="row collapse">
								<div class="small-10 columns">
									<input class="{{ $errors->first('rabat') ? 'error' : '' }}" id="rabat" name="rabat" value="{{ Input::old('rabat') ? Input::old('rabat') : $rabat }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
								</div>
								<div class="small-2 columns">
									<span class="postfix">%</span>
								</div>
							</div>
						</div>

						<div>
							<label>{{ AdminLanguage::transAdmin('Login') }}</label>
							<input class="{{ $errors->first('login') ? 'error' : '' }}" id="login" name="login" value="{{ Input::old('login') ? Input::old('login') : $login }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('login') }}</div>
						</div>

						<div>
							<label for="">{{ AdminLanguage::transAdmin('Password') }}</label>

							<input type="password" class="" id="login-pass1" name="password" value="{{ htmlentities(Input::old('password') ? Input::old('password') : $password) }}" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<input style="display: none" id="login-pass2" type="text" value="{{ htmlentities(Input::old('password') ? Input::old('password') : $password) }}" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<label class="no-margin">
								<a href="#" id="show-pass">{{ AdminLanguage::transAdmin('Prikaži lozinku') }}</a>
							</label>
							<div class="error">{{ $errors->first('password') }}</div>
						</div>

						<div>
							<label>API-Username</label>
							<input class="{{ $errors->first('api_username') ? 'error' : '' }}" id="api_username" name="api_username" value="{{ Input::old('api_username') ? Input::old('api_username') : $api_username }}" type="text" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('api_username') }}</div>
						</div>

						<div>
							<label for="">API-Password</label>								
							<input type="password" class="" id="login-api_pass1" name="api_password" value="{{ htmlentities(Input::old('api_password') ? Input::old('api_password') : $api_password) }}" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<input style="display: none" id="login-api_pass2" type="text" value="{{ htmlentities(Input::old('api_password') ? Input::old('api_password') : $api_password) }}" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>			
							<label class="no-margin"><a href="#!" id="show-api-pass">Prikaži API lozinku</a></label>
							<div class="error">{{ $errors->first('api_password') }}</div>
						</div>

						<div class="row">
							<div class="columns medium-12 field-group">
								<label>
									<input name="aktivan_api" type="checkbox" @if($aktivan_api == 1) checked @endif> Aktivan Api
								</label>
							</div>
						</div>
						@endif
					</div> 
				</div>

				<div class="column medium-4">
					<div class="flat-box padding-h-8">
						@if($partner_id != 0 AND !$dokumenti)
						<label><a href="{{ AdminOptions::base_url() }}admin/partner/{{ $partner_id }}/korisnici" class="btn btn-small btn-secondary" target="_blank">{{ AdminLanguage::transAdmin('Korisnici') }}</a></label>

						<div>
							<label class="no-margin text-center" for="">{{ AdminLanguage::transAdmin('Vrsta partnera') }}</label>
							<div class="JSVrstePartnera m-checkbox-list">
								@foreach(DB::table('partner_vrsta')->get() as $row)
								<label class="no-margin">
									<input class="JS-vrsta" data-id="{{ $row->partner_vrsta_id }}" type="checkbox" {{ in_array($row->partner_vrsta_id, AdminKupci::checkVrstaPartner($partner_id)) ? 'checked' : '' }} {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }}> {{ $row->naziv }}
								</label>
								@endforeach
							</div>
						</div>
						@endif

						@if(Admin_model::check_admin() AND !$dokumenti)
						<div class="field-group column JSServiceSetings" {{ in_array(117, AdminKupci::checkVrstaPartner($partner_id)) ? '' : 'hidden' }}>
							<label for="">{{ AdminLanguage::transAdmin('Podaci za import') }}</label>

							<div>
								<label for="">{{ AdminLanguage::transAdmin('Izaberite podrzan import') }}</label>
								<select name="import_naziv_web" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									{{ AdminKupci::podrzaniImporti(Input::old('import_naziv_web') ? Input::old('import_naziv_web') : $import_naziv_web) }}
								</select>
							</div>

							<div>
								<label for="">{{ AdminLanguage::transAdmin('Link za automatsko skidanje fajla') }}</label>
								<input type="text" class="{{ $errors->first('auto_link') ? 'error' : '' }}" name="auto_link" value="{{ Input::old('auto_link') ? Input::old('auto_link') : $auto_link }}" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>

							<div>
								<label for="">{{ AdminLanguage::transAdmin('Puni automatski import') }}</label>
								<select name="auto_import" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									{{ AdminSupport::selectCheck(Input::old('auto_import') ? Input::old('auto_import') : $auto_import) }}
								</select>
							</div>

							<div>
								<label for="">{{ AdminLanguage::transAdmin('Automatsko azuriranje cena i kolicina') }}</label>
								<select name="auto_import_short" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									{{ AdminSupport::selectCheck(Input::old('auto_import_short') ? Input::old('auto_import_short') : $auto_import_short) }}
								</select>
							</div>

							<div>
								<label for="">{{ AdminLanguage::transAdmin('Preračunavanje cena (Kratki auto import)') }}</label>
								@if($preracunavanje_cena == 1)
								<input type="checkbox" name="preracunavanje_cena" checked {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@else
								<input type="checkbox" name="preracunavanje_cena" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@endif
							</div>

							<div>
								<label for="">{{ AdminLanguage::transAdmin('Korisnicko ime Servisa') }}</label>
								<input type="text" class="{{ $errors->first('service_username') ? 'error' : '' }}" name="service_username" value="{{ Input::old('service_username') ? Input::old('service_username') : $service_username }}" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>

							<div>
								<label for="">{{ AdminLanguage::transAdmin('Lozinka za Servis') }}</label>
								<input type="text" class="{{ $errors->first('service_password') ? 'error' : '' }}" name="service_password" value="{{ Input::old('service_password') ? Input::old('service_password') : $service_password }}" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>

							<div>
								<label for="">{{ AdminLanguage::transAdmin('Kurs') }}</label>
								<input type="number" class="{{ $errors->first('zadnji_kurs') ? 'error' : '' }}" name="zadnji_kurs" value="{{ Input::old('zadnji_kurs') ? Input::old('zadnji_kurs') : $zadnji_kurs }}" {{ Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>
						</div>
						@endif
					</div> 
				</div>
			</div> 

			@if(Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')))
			<div class="row"> 
				<div class="column medium-12"> 
					<div class="btn-container center">
						<button type="submit" id="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						<a href="{{AdminOptions::base_url()}}{{!$dokumenti ? 'admin/kupci_partneri' : 'dokumenti'}}/partneri" class="btn btn-secondary">{{ AdminLanguage::transAdmin('Otkaži') }}</a>
						@if($partner_id!=0)
						<a class="btn btn-danger JSbtn-delete" data-link="{{AdminOptions::base_url()}}{{!$dokumenti ? 'admin/kupci_partneri/partneri' : 'dokumenti/partner'}}/{{ $partner_id }}/0/delete">{{ AdminLanguage::transAdmin('Obriši') }}</a>
						@endif
					</div>
				</div>
			</div>
			@endif

		</form> 

	@if(in_array(AdminOptions::web_options(130),array(1,2)) AND $partner_id>0 AND !$dokumenti)
	<div class="row">
		<div class="columns medium-9">
			<h2 class="title-med">{{ AdminLanguage::transAdmin('Kartica Partnera') }}</h2>
			@if(AdminOptions::info_sys('calculus') || AdminOptions::info_sys('infograf') || AdminOptions::info_sys('logik'))
			<form method="POST" action="{{AdminOptions::base_url()}}admin/partner/card-refresh/{{$partner_id}}">
				<button onClick="this.form.submit(); this.disabled=true;" class="btn btn-primary">{{ AdminLanguage::transAdmin('Osveži karticu') }}</button>
			</form>
			@endif
			<div class="table-scroll">  
				<table> 
					<thead> 
						<tr> 
							<th>{{ AdminLanguage::transAdmin('Vrsta dokumenta') }}</th>						
							<th>{{ AdminLanguage::transAdmin('Datum dokumenta') }}</th> 
							<th>{{ AdminLanguage::transAdmin('Datum dospeća') }}</th>
							<th>{{ AdminLanguage::transAdmin('Duguje') }}</th>
							<th>{{ AdminLanguage::transAdmin('Potražuje') }}</th>
						</tr>
					</thead>
					<tbody> 
						@foreach($kartica as $item)
						<tr> 
							<td>{{$item->vrsta_dokumenta .' - '.$item->opis}}</td>							
							<td>{{date("d-m-Y",strtotime($item->datum_dokumenta))}}</td>
							<td>{{date("d-m-Y",strtotime($item->datum_dospeca))}}</td>
							<td>{{number_format($item->duguje,2)}}</td>
							<td>{{number_format($item->potrazuje,2)}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				{{ $kartica->links() }} 
			</div>
		</div>

		<br>

		<div class="columns medium-3 admin-partner"> 
			<div class="row">
				<span class="columns medium-7 no-padd">{{ AdminLanguage::transAdmin('Ukupno dugovanje') }}: </span>
				<span class="columns medium-5 no-padd">{{number_format($sumDuguje,2)}} </span>
			</div>
			<div class="row">
				<span class="columns medium-7 no-padd">{{ AdminLanguage::transAdmin('Ukupno potraživanje') }}: </span>
				<span class="columns medium-5 no-padd">{{number_format($sumPotrazuje,2)}} </span>
			</div>
			<div class="row">
				<span class="columns medium-7 no-padd">{{ AdminLanguage::transAdmin('Saldo') }}: </span>
				<span class="columns medium-5 no-padd">{{number_format($saldo,2)}} </span>
			</div>
		</div>
	</div>
	@endif

	<input type="hidden" id="JSCheckPartnerConfirm" data-id="{{ Session::has('confirm_partner_id')?Session::get('confirm_partner_id'):0 }}">
</div>

