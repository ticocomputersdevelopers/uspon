<!--=========== LOADER =============== -->
<div class="JSads-loader"> 
	<div class="ads-loader-inner"> 
		<div>  
			<span class="loadSpiner"></span>
			<span>{{ AdminLanguage::transAdmin('Sadržaj  se učitava. Molimo Vas sačekajte') }}.</span>
		</div>
	</div>
</div>   


<div class="flat-box relative">  
	<div class="fixed-cat-menu JScat_toggle_btn">
		<i class="fa fa-arrow-circle-left" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Kategorije') }}
	</div>

	<div class="row small-gutter"> 
		<div class="column medium-7">

			<label class="text-center">{{ AdminLanguage::transAdmin('Filtriranje') }} </label>

			<div class="row"> 
				<div class="columns medium-3">	
					<div class="table-scroll"> 
						<table class="filter-table">
							<tr>
								<th width="25">{{ AdminLanguage::transAdmin('Da') }}</th>
								<th width="25">{{ AdminLanguage::transAdmin('Ne') }}</th> 
							</tr>

							@if(Admin_model::check_admin(array('WEB_IMPORT')))
							<tr>
								<td><input class="filter-check" type="checkbox" data-check="11"></td>
								<td><input class="filter-check" type="checkbox" data-check="12"></td>
								<td>{{ AdminLanguage::transAdmin('Lager') }} (naš)</td>
							</tr>
							<tr>
								<td><input class="filter-check" type="checkbox" data-check="21"></td>
								<td><input class="filter-check" type="checkbox" data-check="22"></td>
								<td>{{ AdminLanguage::transAdmin('Lager dobavljača') }}</td>
							</tr>
							@endif

							@if(AdminOptions::checkB2C())
							<tr>
								<td><input class="filter-check" type="checkbox" data-check="13"></td>
								<td><input class="filter-check" type="checkbox" data-check="14"></td>
								<td>{{ AdminLanguage::transAdmin('Akcija B2C') }}</td>
							</tr>
							@endif

							@if(AdminOptions::checkB2B())
							<tr>
								<td><input class="filter-check" type="checkbox" data-check="31"></td>
								<td><input class="filter-check" type="checkbox" data-check="32"></td>
								<td>{{ AdminLanguage::transAdmin('Akcija B2B') }}</td>
							</tr>
							@endif
							<tr>
								<td><input class="filter-check" type="checkbox" data-check="15"></td>
								<td><input class="filter-check" type="checkbox" data-check="16"></td>
								<td>{{ AdminLanguage::transAdmin('Zaključan') }}</td>
							</tr>
						</table>
					</div>
				</div>	 

				<div class="columns medium-3">
					<div class="table-scroll"> 
						<table class="filter-table">
							<tr>
								<th width="25">{{ AdminLanguage::transAdmin('Da') }}</th>
								<th width="25">{{ AdminLanguage::transAdmin('Ne') }}</th> 
							</tr>

							@if(AdminOptions::checkB2C())
							<tr>
								<td><input class="filter-check" type="checkbox" data-check="19"></td>
								<td><input class="filter-check" type="checkbox" data-check="20"></td>
								<td>{{ AdminLanguage::transAdmin('Na Web-u') }}</td>
							</tr>
							@endif

							@if(AdminOptions::checkB2B())
							<tr>
								<td><input class="filter-check" type="checkbox" data-check="23"></td>
								<td><input class="filter-check" type="checkbox" data-check="24"></td>
								<td>{{ AdminLanguage::transAdmin('Na B2B-u') }}</td> 
							</tr>
							@endif

							<tr>
								<td><input class="filter-check" type="checkbox" data-check="17"></td>
								<td><input class="filter-check" type="checkbox" data-check="18"></td>
								<td>{{ AdminLanguage::transAdmin('Aktivan') }}</td>
							</tr>
						</table>
					</div>

					<div class="nc-options row">
						<div class="columns medium-7 small-8">
							<div class="columns medium-6 small-6">
								<label for="" class="tooltipz" aria-label="Nabavna cena">{{ AdminLanguage::transAdmin('NC od') }}</label>
								<input type="text" id="JSCenaOd" class="">
							</div>
							<div class="columns medium-6 small-6">
								<label for="" class="tooltipz" aria-label="Nabavna cena">{{ AdminLanguage::transAdmin('NC do') }}</label>
								<input type="text" id="JSCenaDo" class="">
							</div>
						</div>
						<div class="columns medium-5 small-4 btns-od-do">
							<label>&nbsp;</label>
							<a href="#" id="JSCenaSearch" class="m-input-and-button__btn btn btn-primary">
								<i class="fa fa-search" aria-hidden="true"></i>
							</a>
							<a href="#" class="m-input-and-button__btn btn btn-secondary tooltipz" aria-label="{{ AdminLanguage::transAdmin('Poništi') }}">
								<i class="fa fa-times" id="JSCenaClear" aria-hidden="true"></i>
							</a>
						</div>
					</div> 
				</div>

				<div class="columns medium-6">	
					<div class="table-scroll"> 
						<table class="filter-table">
							<tr>
								<th width="25">{{ AdminLanguage::transAdmin('Da') }}</th>
								<th width="25">{{ AdminLanguage::transAdmin('Ne') }}</th>
								<th></th>
								<th width="25">{{ AdminLanguage::transAdmin('Da') }}</th>
								<th width="25">{{ AdminLanguage::transAdmin('Ne') }}</th>
								<th></th>
								<th width="25">{{ AdminLanguage::transAdmin('Da') }}</th>
								<th width="25">{{ AdminLanguage::transAdmin('Ne') }}</th>
								<th></th>
							</tr>

							<tr>
								<td><input class="filter-check" type="checkbox" data-check="9"></td>
								<td><input class="filter-check" type="checkbox" data-check="10"></td>
								<td>{{ AdminLanguage::transAdmin('Slike') }}</td>
								<td><input class="filter-check" type="checkbox" data-check="25"></td>
								<td><input class="filter-check" type="checkbox" data-check="26"></td>
								<td>{{ AdminLanguage::transAdmin('Težina') }}</td>



								<td><input class="filter-check" type="checkbox" data-check="33"></td>
								<td><input class="filter-check" type="checkbox" data-check="34"></td>
								<td>{{ AdminLanguage::transAdmin('Visina') }}</td>
							</tr>


							<tr>
								<td><input class="filter-check" type="checkbox" data-check="1"></td>
								<td><input class="filter-check" type="checkbox" data-check="2"></td>
								<td>{{ AdminLanguage::transAdmin('Opis') }}</td>
								<td><input class="filter-check" type="checkbox" data-check="27"></td>
								<td><input class="filter-check" type="checkbox" data-check="28"></td>
								<td>{{ AdminLanguage::transAdmin('Deklaracija') }}</td>


								<td><input class="filter-check" type="checkbox" data-check="35"></td>
								<td><input class="filter-check" type="checkbox" data-check="36"></td>
								<td>{{ AdminLanguage::transAdmin('Širina') }}</td>

							</tr>

							<tr>
								<td><input class="filter-check" type="checkbox" data-check="3"></td>
								<td><input class="filter-check" type="checkbox" data-check="4"></td>
								<td>{{ AdminLanguage::transAdmin('Karakteristike (HTML)') }}</td>
								<td><input id="JSFlag" class="" type="checkbox" {{ $criteria['flag']==1?'checked':'' }}></td>
								<td></td>
								<td>{{ AdminLanguage::transAdmin('Flag') }}</td>
								
								<td><input class="filter-check" type="checkbox" data-check="37"></td>
								<td><input class="filter-check" type="checkbox" data-check="38"></td>
								<td>{{ AdminLanguage::transAdmin('Dužina') }}</td>
							</tr>

							<tr>
								<td><input class="filter-check" type="checkbox" data-check="5"></td>
								<td><input class="filter-check" type="checkbox" data-check="6"></td>
								<td>{{ AdminLanguage::transAdmin('Karakteristike (generisane)') }}</td>
								<td><input class="filter-check" type="checkbox" data-check="29"></td>
								<td><input class="filter-check" type="checkbox" data-check="30"></td>
								<td>{{ AdminLanguage::transAdmin('Garancije') }}</td>
								
								<td><input class="filter-check" type="checkbox" data-check="39"></td>
								<td><input class="filter-check" type="checkbox" data-check="40"></td>
								<td>{{ AdminLanguage::transAdmin('Bruto masa') }}</td>
							</tr>

							<tr>
								@if(Admin_model::check_admin(array('WEB_IMPORT')))
									<td><input class="filter-check" type="checkbox" data-check="7"></td>
									<td><input class="filter-check" type="checkbox" data-check="8"></td>
									<td>{{ AdminLanguage::transAdmin('Karakteristike (dobavljač)') }}</td>
									<td></td>
									<td></td>
									<td></td>
								@endif

								<td><input class="filter-check" type="checkbox" data-check="41"></td>
								<td><input class="filter-check" type="checkbox" data-check="42"></td>
								<td>{{ AdminLanguage::transAdmin('Jedinica mere mase') }}</td>
							</tr>
							
						</table>
					</div>
				</div>  
			</div>
		</div>

		@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
		<div class="column medium-5 action-buttons">	
			<label class="text-center">{{ AdminLanguage::transAdmin('Opcije') }} </label>

			<div class="row">	 
				<div class="column medium-12 small-4">
					<button class="btn btn-secondary" id="all"><i class="fa fa-list" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Izaberi sve (F3)') }}</button>
				</div>

				<div class="column medium-3 small-4">
					<button class="btn btn-secondary JSexecute" data-execute='[{"table":"roba", "column":"flag_zakljucan", "val":"false"},{"table":"dobavljac_cenovnik", "column":"flag_zakljucan", "val":"0"}]'><i class="fa fa-unlock-alt" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Otključaj') }}</button>
				</div>

				<div class="column medium-3 small-4">
					<button class="btn btn-secondary JSexecute" data-execute='[{"table":"roba", "column":"flag_zakljucan", "val":"true"},{"table":"dobavljac_cenovnik", "column":"flag_zakljucan", "val":"1"}]'><i class="fa fa-lock" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Zaključaj') }}</button>
				</div>

				@if(AdminOptions::checkB2C())
				<div class="column medium-3 small-4">
					<button class="btn btn-secondary JSexecute" data-execute='[{"table":"roba", "column":"flag_prikazi_u_cenovniku", "val":"1"}, {"table":"dobavljac_cenovnik", "column":"flag_prikazi_u_cenovniku", "val":"1"}]'><i class="fa fa-upload" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Prikaži na Web-u') }}</button>
				</div>

				<div class="column medium-3 small-4">
					<button class="btn btn-secondary JSexecute" data-execute='[{"table":"roba", "column":"flag_prikazi_u_cenovniku", "val":"0"}, {"table":"dobavljac_cenovnik", "column":"flag_prikazi_u_cenovniku", "val":"0"}]'><i class="fa fa-download" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Skloni sa Web-a') }}</button> 
				</div> 

				<div class="column medium-3 small-4">
					<button id="JSexecuteAkcija" class="btn btn-secondary " ><i class="fa fa-calendar-check-o" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Akcija') }}</button>
				</div>

				<div class="column medium-3 small-4">
					<button class="btn btn-secondary JSexecute" data-execute='[{"table":"roba", "column":"akcija_flag_primeni", "val":"0"},{"table":"roba", "column":"akcija_popust", "val":"0"},{"table":"roba", "column":"akcijska_cena", "val":"0"}]'><i class="fa fa-calendar-times-o" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Skloni') }}</button>	
				</div>
				@endif

				@if(AdminOptions::checkB2B())
				<div class="column medium-3 small-4">
					<button class="btn btn-secondary JSexecute" data-execute='[{"table":"roba", "column":"b2b_akcija_flag_primeni", "val":"1"}]'><i class="fa fa-calendar-check-o" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Akcija B2B')}}</button>
				</div> 

				<div class="column medium-3 small-4">
					<button class="btn btn-secondary JSexecute" data-execute='[{"table":"roba", "column":"b2b_akcija_flag_primeni", "val":"0"}]'><i class="fa fa-calendar-times-o" aria-hidden="true"></i>{{ AdminLanguage::transAdmin('B2B Akcija-Skloni') }}</button>
				</div>

				<div class="column medium-3 small-4">
					<button class="btn btn-secondary JSexecute" data-execute='[{"table":"roba", "column":"flag_cenovnik", "val":"1"}]'><i class="fa fa-upload" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Prikaži na B2B-u') }}</button>
				</div>

				<div class="column medium-3 small-4">
					<button class="btn btn-secondary JSexecute" data-execute='[{"table":"roba", "column":"flag_cenovnik", "val":"0"}]'><i class="fa fa-download" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Skloni sa B2B-a') }}</button>
				</div> 
				@endif

				<div class="column medium-3 small-4">
					<button class="btn btn-secondary JSexecute" data-execute='[{"table":"roba", "column":"flag_aktivan", "val":"1"}, {"table":"dobavljac_cenovnik", "column":"flag_aktivan", "val":"1"}]'><i class="fa fa-toggle-on" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Aktivno') }}</button>
				</div>

				<div class="column medium-3 small-4">
					<button class="btn btn-secondary JSexecute" data-execute='[{"table":"roba", "column":"flag_aktivan", "val":"0"}, {"table":"dobavljac_cenovnik", "column":"flag_aktivan", "val":"0"}]'><i class="fa fa-toggle-off" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Neaktivno') }}</button>
				</div>
			</div>  
		</div>
		@endif 
	</div> 
</div>

<div class="flat-box">
	<div class="article-custom-columns">
		<section class="order-filters first-col loco search-section-group clearfix">
			<div class="row collapse"> 
				<div class="column medium-8 small-9">		
					<input type="text" class="" id="search" autocomplete="off" placeholder="{{ AdminLanguage::transAdmin('Pretraži') }}...">
				</div>
				<div class="column medium-4 small-3">
					<button type="submit" id="search-btn" value="Pronađi" class="m-input-and-button__btn btn btn-primary btn-xm"><i class="fa fa-search" aria-hidden="true"></i></button>
					<button type="submit" id="clear-btn" value="Poništi" aria-label="{{ AdminLanguage::transAdmin('Poništi') }}" class="m-input-and-button__btn btn btn-secondary btn-xm tooltipz"><i class="fa fa-times" id="JSCenaClear" aria-hidden="true"></i></button>
				</div>
			</div>
		</section>
		<!-- filter za karakteristike -->
		@if( $criteria['grupa_pr_id'] > 0 )	
		<div class="third-col loco col-h">
			<select class="admin-select m-input-and-button__input" id="karakt_select">
				<option value="0">{{ AdminLanguage::transAdmin('Izaberi karakteristiku') }}</option>
				@foreach(AdminSupport::getKarakNaziv($grupa_pr_id) as $karak)
				@if(str_replace('-','',$criteria['karakteristika']) == $karak->grupa_pr_naziv_id )						
				<option value="{{ $karak->grupa_pr_naziv_id }}" selected>{{ $karak->naziv }}</option>
				@else
				<option value="{{ $karak->grupa_pr_naziv_id }}" >{{ $karak->naziv }}</option>
				@endif
				@endforeach
			</select>
			<button class="btn btn-primary btn-abs dobavljac_proizvodjac m-input-and-button__btn" title="{{ AdminLanguage::transAdmin('Artikli koji imaju dodeljenu izabranu karakteristiku') }}"><i class="fa fa-check" aria-hidden="true"></i>
			</button>
			<button class="btn btn-primary btn-abs2 dobavljac_proizvodjac m-input-and-button__btn" data-kind="NE" title="{{ AdminLanguage::transAdmin('Artikli koji nemaju dodeljenu izabranu karakteristiku') }}"><i class="fa fa-minus-square-o" aria-hidden="true" ></i>
			</button> 
		</div>
		@endif 

		<div class="second-col loco col-h">
			<div class="form-bottom-btn">
				@if(Admin_model::check_admin(array('WEB_IMPORT')))
				<select class="" id="dobavljac_select" multiple="multiple">
					@foreach(AdminSupport::dobavljac_ime() as $dobavljac)
					@if( in_array($dobavljac->partner_id ,explode("-",$criteria['dobavljac'])) )
					<option value="{{ $dobavljac->partner_id }}" selected>{{ $dobavljac->naziv }}</option>
					@else
					<option value="{{ $dobavljac->partner_id }}">{{ $dobavljac->naziv }}</option>
					@endif
					@endforeach
				</select>
				<button class="btn btn-primary btn-abs dobavljac_proizvodjac m-input-and-button__btn"><i class="fa fa-check" aria-hidden="true"></i></button>
				@endif
			</div>
		</div>

		<div class="fourth-col loco col-h">
			<div class="form-bottom-btn">
				<select class="admin-select m-input-and-button__input" id="proizvodjac_select" multiple="multiple">
					@foreach(AdminSupport::getProizvodjaci() as $proizvodjac)
					@if( in_array($proizvodjac->proizvodjac_id ,explode("+",$criteria['proizvodjac'])) )
					<option value="{{ $proizvodjac->proizvodjac_id }}" selected>{{ $proizvodjac->naziv }}</option>
					@else
					<option value="{{ $proizvodjac->proizvodjac_id }}">{{ $proizvodjac->naziv }}</option>
					@endif
					@endforeach
				</select>
				<button class="btn btn-primary btn-abs dobavljac_proizvodjac m-input-and-button__btn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
		</div>

		<div class="fifth-col loco col-h">
			<div class="form-bottom-btn">
				<select class="admin-select m-input-and-button__input" id="magacin_select">
					<option value="0">{{ AdminLanguage::transAdmin('Svi magacini') }}</option>
					@foreach(DB::table('orgj')->where('b2c',1)->orderBy('orgj_id','asc')->get() as $row)
					@if( in_array($row->orgj_id ,explode("-",$criteria['magacin'])) )
					<option value="{{ $row->orgj_id }}" selected>{{ $row->naziv }}</option>
					@else
					<option value="{{ $row->orgj_id }}">{{ $row->naziv }}</option>
					@endif
					@endforeach
				</select>
				<button class="btn btn-primary btn-abs dobavljac_proizvodjac m-input-and-button__btn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
		</div>

		<div class="sixth-col loco col-h">
			<div class="form-bottom-btn">
				<select class=" admin-select m-input-and-button__input" id="tip_select" multiple="multiple">
					@foreach(AdminSupport::getTipovi() as $tip)
					@if( in_array($tip->tip_artikla_id ,explode("-",$criteria['tip'])) )
					<option value="{{ $tip->tip_artikla_id }}" selected>{{ $tip->naziv }}</option>
					@else
					<option value="{{ $tip->tip_artikla_id }}">{{ $tip->naziv }}</option>
					@endif
					@endforeach
				</select>
				<button class="btn btn-primary btn-abs dobavljac_proizvodjac m-input-and-button__btn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
		</div>

		<div class="sixth-col loco col-h">
			<div class="form-bottom-btn">
				<select class=" admin-select m-input-and-button__input" id="labela_select" multiple="multiple">
					@foreach(AdminSupport::getLabela() as $labela)
					@if( in_array($labela->labela_artikla_id ,explode("-",$criteria['labela'])) )
					<option value="{{ $labela->labela_artikla_id }}" selected>{{ $labela->naziv }}</option>
					@else
					<option value="{{ $labela->labela_artikla_id }}">{{ $labela->naziv }}</option>
					@endif
					@endforeach
				</select>
				<button class="btn btn-primary btn-abs dobavljac_proizvodjac m-input-and-button__btn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
		</div> 
	</div>
</div>

<!-- nesto ovako je bilo order-filters -->
<section @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) class="JSarticles-listing flat-box" @endif>
	<div class="row collapse flex"> 
		<section class="total-article-count columns medium-6">
			@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
			<a class="dodavanje_artikla btn-create add-new-article" id="new-art" href="{{AdminOptions::base_url()}}admin/product{{AdminOptions::gnrl_options(3029)==1 ? '-short' : ''}}/0" target="_blank">
				<i class="fa fa-plus-square plusikona" aria-hidden="true"></i>{{ AdminLanguage::transAdmin('Dodaj novi artikal (F2)') }}
			</a>
			@endif

			@if(isset($count_products))
			<small>{{ AdminLanguage::transAdmin('Ukupno artikala') }}: <b>{{$count_products}}</b></small> &nbsp;&nbsp; 
			@endif
			<small>{{ AdminLanguage::transAdmin('Po strani') }}: <b>{{$limit}}</b></small>
			| <small>{{ AdminLanguage::transAdmin('Selektovano') }}: </small><b><small id="JSselektovano">0</small></b>
			<small><b>| {{$title}}</b></small>
		</section> <!-- end of .total-article-count -->
 		
 		@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
		<div class="columns medium-3 text-right">
			<form method="POST" action="{{AdminOptions::base_url()}}admin/products-warranty-import" enctype="multipart/form-data">
				<div class="bg-image-file"> 
					<input type="file" name="import_warranty_file">
					<button class="btn btn-primary btn-small" type="submit">{{ AdminLanguage::transAdmin('Importuj garancije') }}</button>
					<div class="error"> 
						{{ $errors->first('import_warranty_file') ? $errors->first('import_warranty_file') : '' }}
					</div>
				</div>	 
			</form>
		</div>
		@endif

		@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
		<div class="columns medium-3 text-right">
			<form method="POST" action="{{AdminOptions::base_url()}}admin/products-import" enctype="multipart/form-data">
				<div class="bg-image-file"> 
					<input type="file" name="import_file">

					<button class="btn btn-primary btn-small" type="submit">{{ AdminLanguage::transAdmin('Importuj') }}</button>
					<div class="error"> 
						{{ $errors->first('import_file') ? $errors->first('import_file') : '' }}
					</div>
				</div>	 
			</form>
		</div>
		@endif  
	</div>  

	<div class="table-scroll">
		<table class="article-artiacle-table articles-page-tab fixed-table-header">
			<thead>
				<tr class="st order-list-titles row">
					<th class=""></th>
<!-- 					@if(AdminOptions::sifra_view()==1)
					<th class="table-head" data-coll="r.roba_id" data-sort="ASC"><a href="#">{{ AdminLanguage::transAdmin('ID') }}</a> </th>
					@endif					
					@if(AdminOptions::sifra_view()==2)
					<th class="table-head" data-coll="r.sifra_is" data-sort="ASC"><a href="#">{{ AdminLanguage::transAdmin('Šifra IS') }}</a> </th>
					@endif					
					@if(AdminOptions::sifra_view()==3)
					<th class="table-head" data-coll="r.sku" data-sort="ASC"><a href="#">{{ AdminLanguage::transAdmin('SKU') }}</a> </th>
					@endif
					@if(AdminOptions::sifra_view()==4)
					<th class="table-head" data-coll="r.sifra_d" data-sort="ASC"><a href="#">{{ AdminLanguage::transAdmin('Šifra Dob') }}</a> </th>
					@endif -->
					<th class="table-head" data-coll="r.barkod" data-sort="ASC"><a href="#">{{ AdminLanguage::transAdmin('Barkod') }}</a> </th>
					<!-- <th class="table-head" data-coll="sku" data-sort="ASC"><a href="#">SKU</a> </th> -->
					<th class="table-head" data-coll="flag_prikazi_u_cenovniku" data-sort="ASC"><a href="#">{{ AdminLanguage::transAdmin('WEB-B2C') }}</a></th>

					<th class="table-head" data-coll="flag_cenovnik" data-sort="ASC"><a href="#">{{ AdminLanguage::transAdmin('WEB-B2B') }}</a></th>

					<th class="table-head" data-coll="akcija_flag_primeni" data-sort="ASC">
						<a href="#">{{ AdminLanguage::transAdmin('AKCIJA') }}</a>
					</th>
					<th class="table-head" data-coll="naziv" data-sort="ASC" style="width: {{$naziv_width}}px">
						<a href="#">{{ AdminLanguage::transAdmin('NAZIV') }} </a>
					</th>
					<th class="table-head tooltipz tooltipz" data-coll="kolicina" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('Količina') }}">
						<a href="#">{{ AdminLanguage::transAdmin('Kol') }} </a>
					</th>
					<th class="table-head tooltipz" data-coll="web_cena" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('Web cena') }}">
						<a href="#">{{ AdminLanguage::transAdmin('WebCena') }} </a>
					</th>
					<th class="table-head tooltipz" data-coll="web_marza" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('Web Marža') }}">
						<a href="#">{{ AdminLanguage::transAdmin('W-M') }} </a>
					</th>
					<th class="table-head tooltipz" data-coll="mpcena" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('Maloprodajna cena') }}">
						<a href="#">{{ AdminLanguage::transAdmin('MpCena') }} </a>
					</th>
					<th class="table-head tooltipz" data-coll="mp_marza" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('Maloprodajna Marža') }}">
						<a href="#">{{ AdminLanguage::transAdmin('Mp-M') }} </a>
					</th>
					<th class="table-head" data-coll="kolicina" data-sort="ASC">
						<a href="#">PDV </a>
					</th>
					<th class="table-head tooltipz" data-coll="racunska_cena_nc" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('Nabavna cena') }}">
						<a href="#">{{ AdminLanguage::transAdmin('NC') }}</a> 
					</th>

					<th class="table-head tooltipz" data-coll="" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('End cena') }}">
						<a href="#">{{ AdminLanguage::transAdmin('E-CENA') }}</a> 
					</th>
					<th class="table-head tooltipz" data-coll="end_marza" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('End Marža') }}">
						<a href="#">{{ AdminLanguage::transAdmin('End-M') }} </a>
					</th>
					<th class="table-head tooltipz" data-coll="dobavljac_id" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('Dobavljač') }}">
						<a href="#">{{ AdminLanguage::transAdmin('DOB') }}. </a>
					</th>
<!-- <th class="table-head tooltipz" data-coll="dobavljac_kolicina" data-sort="ASC">
<a href="#">Kol D. </a>
</th>
<th class="table-head tooltipz" data-coll="sifra_d" data-sort="ASC">
<a href="#">ŠIFRA D</a> 
</th> -->
<th class="table-head" data-coll="web_opis" data-sort="ASC">
	<a href="#">{{ AdminLanguage::transAdmin('OPIS') }}</a> 
</th>
<th class="table-head tooltipz" data-coll="web_flag_karakteristike" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('Flag karakteristika') }}">
	<a href="#">{{ AdminLanguage::transAdmin('FLAG KARAK') }}.</a> 
</th>

<th class="table-head tooltipz" data-coll="br_slika" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('Broj slika') }}">
	<a href="#">{{ AdminLanguage::transAdmin('Br.sl.') }}</a> 
</th>
<th class="table-head" data-coll="tip_cene" data-sort="ASC">
	<a href="#">{{ AdminLanguage::transAdmin('TIP') }}</a>
</th>
<th class="table-head tooltipz" data-coll="flag_zakljucan" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('Zaključan') }}">
	<a href="#" >{{ AdminLanguage::transAdmin('ZKLJ') }}</a>
</th>
<th class="table-head" data-coll="grupa_pr_id" data-sort="ASC">
	<a href="#">{{ AdminLanguage::transAdmin('GRUPA') }}</a>
</th>
<th class="table-head tooltipz" data-coll="proizvodjac_id" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('Proizvođač') }}">
	<a href="#">{{ AdminLanguage::transAdmin('PRO') }}. </a>
</th>
<th class="table-head tooltipz" data-coll="flag_aktivan" data-sort="ASC" aria-label="{{ AdminLanguage::transAdmin('Aktivno') }}">
	<a href="#">{{ AdminLanguage::transAdmin('AKT') }}</a>
</th>	
</tr>  
</thead>

<tbody  @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE'))) id="selectable" @endif>
	@foreach($articles as $row)
	<tr class="admin-list order-list-item row ui-widget-content {{$row->flag_aktivan == 1 ? '' : 'text-red'}}" data-id="{{ $row->roba_id }}">
		<td>
			@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
			<button class="btn btn-secondary btn-xm edit-article-btn btn-circle small JSArticleEdit tooltipz" aria-label="{{ AdminLanguage::transAdmin('Izmeni artikal') }}" data-zakljucan="{{ $row->flag_zakljucan == 'true'?'1':'0' }}" data-short="{{ AdminOptions::gnrl_options(3029) }}">
				<i class="fa fa-pencil" aria-hidden="true"></i>
			</button>
			@endif
		</td>
		<td class="live-edit">
<!-- 			@if(AdminOptions::sifra_view()==1)
			<span class="JSCodeValue">	
				{{ $row->roba_id }}
			</span>
			@elseif(AdminOptions::sifra_view()==2)
			<span class="JSCodeValue">	
				{{ $row->sifra_is }}
			</span>
			@elseif(AdminOptions::sifra_view()==3)
			<span class="JSCodeValue">	
				{{ $row->sku }}
			</span>
			@elseif(AdminOptions::sifra_view()==4)
			<span class="JSCodeValue">	
				{{ $row->sifra_d }}
			</span>
			@endif -->
			<span class="JSCodeValue">	
				{{ $row->barkod }}
			</span>
			@if(AdminOptions::sifra_view()!=1)
			<span class="JSEditCodeBtnSpan">
				<input type="text" class="JSCodeInput" value="{{ AdminOptions::sifra_view()==2 ? $row->sifra_is : (AdminOptions::sifra_view()==3 ? $row->sku : $row->sifra_d) }}" />
				@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
				<i class="fa fa-pencil JSEditCodeBtn" aria-hidden="true"></i>
				@endif
			</span>
			@endif					
		</td>
		<td class="flag_prikazi_u_cenovniku">{{ $row->flag_prikazi_u_cenovniku == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}</td>
		<td class="flag_cenovnik">{{ $row->flag_cenovnik == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}</td>

		<td class="akcija_flag_primeni">
			<?php $akcija_flag_primeni = AdminArticles::provera_akcija($row->roba_id); $b2b_akcija_flag_primeni = AdminArticles::provera_akcija($row->roba_id,'b2b'); ?>
			@if($akcija_flag_primeni==1 OR $b2b_akcija_flag_primeni==1)
			@if($akcija_flag_primeni == 1 AND $b2b_akcija_flag_primeni == 1)
			<span class="akcija-a" style="background-color: orange;">A</span>
			@elseif($akcija_flag_primeni == 1)
			<span class="akcija-a">A</span>
			@else
			<span class="akcija-a" style="background-color: blue;">A</span>
			@endif
			@else
			<span class="akcija-bez">-</span>
			@endif
		</td>

		<td class="table-fixed-width1 naziv1 text-left">
			<!-- <div class="max-width-450 inline-block vert-align">  -->
			<div class="inline-block vert-align"> 
				<span class="JSnaziv1Vrednost">{{ $row->naziv }}</span>
				<span class="JSEditNaziv1BtnSpan">
					<input type="text" class="JSnaziv1Input" value="{{ htmlentities($row->naziv) }}" />
					@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
					<i class="fa fa-pencil JSEditNaziv1Btn" aria-hidden="true"></i>
					@endif
				</span>
			</div>
		</td>

		<td class="kolicina">
			<span class="JSkolicinaVrednost">
				{{ AdminArticles::kolicina($row->kolicina,$row->roba_id,$row->kolicina,$criteria['magacin']) }}
			</span>
			<span class="JSEditkolicinaBtnSpan">
				<input type="text" class="JSkolicinaInput" value="{{ AdminArticles::kolicina($row->kolicina,$row->roba_id,$row->kolicina,$criteria['magacin'])}}" />
				@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
				<i class="fa fa-pencil JSEditkolicinaBtn" aria-hidden="true"></i>
				@endif
			</span>
		</td>

		<td class="wc-col WebCena">
			<span class="JSWebCenaVrednost">
				{{ $row->web_cena }}
			</span>
			<span class="JSEditWebCenaBtnSpan">
				<input type="text" class="JSWebCenaInput" value="{{ htmlentities($row->web_cena) }}" />
				@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
				<i class="fa fa-pencil JSEditWebCenaBtn" aria-hidden="true"></i>
				@endif
			</span>
		</td>
		<td class="web_marza">{{ $row->web_marza }}</td>
		<td class="mpcena">{{ $row->mpcena }}</td>
		<td class="mp_marza">{{ $row->mp_marza }}</td>
		<td class="tarifna_grupa">{{ AdminSupport::find_tarifna_grupa($row->tarifna_grupa_id,'porez') }}</td>
		<td class="racunska_cena_nc">{{ $row->racunska_cena_nc }}</td>

		<td class= "endcena EndCena">
			<span class="JSEndCenaVrednost">
				{{$row->racunska_cena_end}} 
			</span>
			<span class="JSEditEndCenaBtnSpan">
				<input type="text" class="JSEndCenaInput" value="{{$row->racunska_cena_end}}"/>
				@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
				<i class="fa fa-pencil JSEditEndCenaBtn" aria-hidden="true"></i>
				@endif
			</span>
		</td>
		<td class="end_marza">{{ $row->end_marza }}</td>

		<td class="">{{ $row->dobavljac }}</td>
		<td class="">{{ $row->web_opis != null && $row->web_opis != '' ?  AdminLanguage::transAdmin('DA') : AdminLanguage::transAdmin('NE') }}</td>
		<td class="web_flag_karakteristike">{{ AdminSupport::nameOfCharact($row->web_flag_karakteristike) }}</td>

		<td class="web_slika">{{ $row->br_slika }}</td>
		<td class="tip_cene">{{ AdminSupport::tip_naziv($row->tip_cene) }}</td>
		<td class="flag_zakljucan">{{ $row->flag_zakljucan == 'true' ?  AdminLanguage::transAdmin('DA') : AdminLanguage::transAdmin('NE') }}</td>
		<td class="grupa_pr_id">{{ AdminGroups::find($row->grupa_pr_id,'grupa') }}</td>
		<td class="proizvodjac_id">{{ AdminSupport::find_proizvodjac($row->proizvodjac_id,'naziv') }}</td>
		<td class="flag_aktivan">{{ $row->flag_aktivan == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}</td>	

	</tr>
	@endforeach

</tbody>
</table>
</div>

{{ Paginator::make($articles, $count_products, $limit)->links() }}

</section>

<!-- ====================== -->
<div class="text-center">
	<a href="#" class="video-manual" data-reveal-id="articles-manual">{{ AdminLanguage::transAdmin('Uputstvo') }} <i class="fa fa-film"></i></a>
	<div id="articles-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<div class="video-manual-container"> 
			<p><span class="video-manual-title">{{ AdminLanguage::transAdmin('Artikli') }}</span></p> 
			<iframe src="https://player.vimeo.com/video/271254462" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
			<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		</div>
	</div>
</div>
<!-- ====================== -->

<!-- MODAL END -->
<div id="wait">
	<p><img class="loader" src="{{ AdminOptions::base_url()}}images/admin/wheel.gif">{{ AdminLanguage::transAdmin('Molimo vas sačekajte') }}...</p>
</div>


<!-- MODALS -->


<div id="JSchooseGroupModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Promeni grupu') }}</h4>
	<div class="m-input-and-button small-margin-bottom column medium-12">
			<select class="JSexecuteInput m-input-and-button__input" id="change-group">
				{{AdminSupport::selectGroups()}}
			</select>
			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn" data-execute='[{"table":"roba", "column":"grupa_pr_id"},{"table":"dobavljac_cenovnik", "column":"grupa_pr_id"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
		<br /><br />

		<div class="m-input-and-button small-margin-bottom column medium-12">
			<select id="JSDodatneGrupe" multiple="multiple">
				{{ AdminSupport::select2Groups() }}
			</select>
			<button id="JSDodatneGrupeBtn" class="m-input-and-button__btn btn btn-secondary btn-xm"><i class="fa fa-check" aria-hidden="true"></i></button>
			<button id="JSDodatneGrupeObrisiBtn" class="m-input-and-button__btn btn btn-secondary btn-xm"><i class="fa fa-close" aria-hidden="true"></i></button>
		</div>

		<button id="JSDodatneGrupeObrisiSveBtn" class="m-input-and-button__btn btn btn-secondary btn-xm">{{ AdminLanguage::transAdmin('Obrši sve dodatne grupe') }}</button>
	</div>

<div id="JSchooseBrandModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Promeni proizvođača') }}</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<select class="JSexecuteInput m-input-and-button__input" id="change-manufac">
				<option value="">{{ AdminLanguage::transAdmin('Promeni proizvođača') }}</option>
				@foreach(AdminSupport::getProizvodjaci() as $row)
				<option value="{{ $row->proizvodjac_id }}">{{ $row->naziv }}</option>
				@endforeach
			</select>
			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn" data-execute='[{"table":"roba", "column":"proizvodjac_id"},{"table":"dobavljac_cenovnik", "column":"proizvodjac_id"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>
</div>

<div id="JSchooseTipModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Promeni tip') }}</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<select class="JSexecuteInput m-input-and-button__input">
				<option value="null">{{ AdminLanguage::transAdmin('Bez tipa') }}</option>
				@foreach(AdminSupport::getTipovi() as $row)
				<option value="{{ $row->tip_artikla_id }}">{{ $row->naziv }} ({{ $row->tip_artikla_id }})</option>
				@endforeach
			</select>
			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn" data-execute='[{"table":"roba", "column":"tip_cene"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>
</div>

<div id="JSchooseEnergyClass" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Dodeli energetsku klasu') }}</h4>
	<div class="row"> 
		<div class="column medium-4">
			<label>{{ AdminLanguage::transAdmin('Klasa') }}</label>
		</div>
		<div class="column medium-4">
			<label>{{ AdminLanguage::transAdmin('Stara klasifikacija') }}</label>
		</div>
		<div class="column medium-4">
			<label>{{ AdminLanguage::transAdmin('Link') }}</label>
		</div>

		<div class="m-input-and-button small-margin-bottom column medium-4">
			<select class="JSexecuteInput m-input-and-button__input">
				@foreach(AdminSupport::getEnergetskeKlase() as $row)
					<option value="{{ $row->klasa }}">{{ $row->klasa }}</option>
				@endforeach
			</select>
			
			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn" data-execute='[{"table":"roba", "column":"energetska_klasa"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>

		<div class="m-input-and-button small-margin-bottom column medium-4">
			<select class="JSexecuteInput m-input-and-button__input" name="check_old_class">
				<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
				<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
			</select>

			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn" data-execute='[{"table":"roba", "column":"check_old_class"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>

		<div class="m-input-and-button small-margin-bottom column medium-4">
			<select class="JSexecuteInput m-input-and-button__input adminCheckEnergyLink" name="check_old_class">
				<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
				<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
			</select>

			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn adminCheckEnergyLinkBtn" data-execute='[{"table":"roba", "column":"check_link"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>

		<div class="m-input-and-button small-margin-bottom column medium-12 adminEnergyLink">
			<div><label>{{ AdminLanguage::transAdmin('Link') }}</label></div>
			<input class="JSexecuteInput m-input-and-button__input" type="text">

			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn" data-execute='[{"table":"roba", "column":"energetska_klasa_link"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>

		<div class="small-margin-bottom column medium-12 adminEnergyPdf" style="display: none;">
			<div><label>{{ AdminLanguage::transAdmin('Slika/Pdf') }}</label></div>
			<input type="file" id="JSEnergyImageUpload" value="Izaberi pdf">
		</div>

		<div class="m-input-and-button small-margin-bottom column medium-10">
			<button type="button" id="removeEnergyClass" class="btn">
				{{ AdminLanguage::transAdmin('Ukloni klase') }}
			</button>
		</div>

    </div>
</div>

<div id="JSchoosePriceModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Promeni vrstu cene') }}</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<select class="JSexecuteInput m-input-and-button__input">
				@foreach(AdminSupport::getFlagCene() as $row)
				<option value="{{ $row->roba_flag_cene_id }}">{{ $row->naziv }} </option>
				@endforeach
			</select>
			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn" data-execute='[{"table":"roba", "column":"roba_flag_cene_id"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>	
</div>

<div id="JSchooseLabelaModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Dodeli labelu') }}</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<select class="JSexecuteInput m-input-and-button__input">
				<option value="null">{{ AdminLanguage::transAdmin('Bez labele') }}</option>
				@foreach(AdminSupport::getlabela() as $row)
				<option value="{{ $row->labela_artikla_id }}">{{ $row->naziv }} ({{ $row->labela_artikla_id }})</option>
				@endforeach
			</select>
			<button class="m-input-and-button__btn btn btn-secondary btn-xm JSexecuteBtn" data-execute='[{"table":"roba", "column":"labela"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>
</div>

<div id="JSchooseQuantityModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Promeni količinu') }}</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<select class="JSMagacinKolicina m-input-and-button__input">
				@foreach(DB::table('orgj')->where('b2c',1)->orderBy('orgj_id','asc')->get() as $row)
				<option value="{{ $row->orgj_id }}">{{ $row->naziv }} </option>
				@endforeach
			</select>
			<input type="text" class="m-input-and-button__input" id="JSKolicinaAdd" placeholder="{{ AdminLanguage::transAdmin('Promeni količinu') }}">
			<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSKolicinaBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>	
</div>

<div id="JSTezinaModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Promeni težinu (gr.)') }}</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<input type="text" class="m-input-and-button__input" id="JSTezinaAdd" placeholder="{{ AdminLanguage::transAdmin('Promeni težinu') }}">
			<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSTezinaBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>	
</div> 

<div id="JSTagModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Dodeli tagove') }}</h4>
	<div class="row"> 
		<div class="m-input-and-button small-margin-bottom column medium-12">
			<input type="text" class="m-input-and-button__input" id="JSTagAdd" placeholder="{{ AdminLanguage::transAdmin('Dodeli tagove') }}">
			<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSTagBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
		</div>
	</div>	
</div>

<div id="JSchooseTaxModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Poreska stopa') }}</h4>

	<div class="row">
		<div class="column medium-6">
			<label>{{ AdminLanguage::transAdmin('Poreska stopa') }}</label>
			<select class="m-input-and-button__input" id="JSValPoreskaStopa">
				<option value=""></option>
				@foreach(AdminSupport::getPoreskeGrupe() as $row)
				<option value="{{ $row->tarifna_grupa_id }}">{{ $row->naziv }}</option>
				@endforeach
			</select>
		</div>

		<div class="column medium-6">
			<label>{{ AdminLanguage::transAdmin('Promeni i web cenu') }}</label>
			<select class="m-input-and-button__input" id="JSCenaPoreskaStopa">
				<option value="1">{{ AdminLanguage::transAdmin('DA') }}</option>
				<option value="0">{{ AdminLanguage::transAdmin('NE') }}</option>
			</select>
		</div>
	</div>

	<br>

	<div class="text-center"> 
		<button class="btn-admin btn btn-primary btn-small" id="JSPoreskaStopa">{{ AdminLanguage::transAdmin('Promeni poresku stopu') }}</button>
	</div>
</div> 

<div id="JSchooseExportModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Export artikala') }}</h4>

	@if(count(AdminSupport::getExporti()) > 0)
	<label class="text-center">{{ AdminLanguage::transAdmin('Export') }}</label>

	<div class="row collapse">
		<div class="columns medium-8"> 
			<select class="columns medium-6" id="export_select">
				<option value="0">{{ AdminLanguage::transAdmin('Svi artikli') }}</option>
				@foreach(AdminSupport::getExporti() as $export)
				@if($export->export_id == str_replace('-','',$criteria['exporti']))
				<option value="{{ $export->export_id }}" selected data-export="{{ $export->vrednost_kolone }}">{{ $export->naziv }}</option>
				@else
				<option value="{{ $export->export_id }}" data-export="{{ $export->vrednost_kolone }}">{{ $export->naziv }}</option>
				@endif
				@endforeach
			</select>&nbsp;

			<button class="btn btn-primary dobavljac_proizvodjac btn-small">{{ AdminLanguage::transAdmin('Da') }}</button>

			<button class="btn btn-primary btn-small" id="JSExportNo">{{ AdminLanguage::transAdmin('Ne') }}</button>

			<button class="btn btn-primary btn-small JSExport tooltipz" aria-label="{{ AdminLanguage::transAdmin('Dodeli') }}" data-kind="export_dodeli">
				<i class="fa fa-plus" aria-hidden="true"></i>
			</button>

			<button class="btn btn-primary btn-small JSExport tooltipz" aria-label="{{ AdminLanguage::transAdmin('Ukloni') }}" data-kind="export_ukloni">
				<i class="fa fa-times" aria-hidden="true"></i>
			</button>
		</div> 

		<div class="columns medium-4">
			<select id="JSExportKind" class="columns medium-5">
				<option value="xml">XML</option>
				<option value="xls">XLS</option>
				<option value="csv">CSV</option>
				<option value="json">JSON</option>
			</select>&nbsp;
			<button class="btn btn-primary btn-small" id="JSExportExecute">{{ AdminLanguage::transAdmin('Exportuj') }}</button>
		</div>
	</div>
	@endif

	<br>

	<div> 
		<a class="articles-btn exp-btn btn btn-primary btn-xm" href="{{ AdminOptions::base_url()}}admin/export_all_xls">{{ AdminLanguage::transAdmin('EXPORT SVIH ARTIKALA (.xls)') }}</a>	
	</div>

	<br>

	<div>
		<button class="articles-btn exp-btn btn btn-primary btn-xm" id="JSSimpleExportSelected">{{ AdminLanguage::transAdmin('EXPORT SELEKTOVANIH ARTIKALA (.xls)') }}</button>	
	</div> 
</div>

<div id="JSchooseKatalogModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Katalog') }}</h4>

	@if(count(AdminSupport::getCreatedKatalog()) > 0)  
	<div class="row"> 
		<div class="columns medium-6"> 
			<label>&nbsp;</label>
			<select id="katalog_select">
				<option value="0">{{ AdminLanguage::transAdmin('Svi katalozi') }}</option>
				@foreach(AdminSupport::getCreatedKatalog() as $katalog)
				@if($katalog->katalog_id == str_replace('-','',$criteria['katalog']))
				<option value="{{ $katalog->katalog_id }}" selected data-katalog="{{ $katalog->katalog_id }}">{{ $katalog->naziv }}</option>
				@else
				<option value="{{ $katalog->katalog_id }}" data-katalog="{{ $katalog->katalog_id }}">{{ $katalog->naziv }}</option>
				@endif
				@endforeach
			</select>
		</div>

		<div class="columns medium-3"> 
			<label>{{ AdminLanguage::transAdmin('Filteri') }}</label>
			<button class="btn btn-primary m-input-and-button__btn JSKatalogNo" data-check="1">
			{{ AdminLanguage::transAdmin('Da') }}</button>

			&nbsp; 

			<button class="btn btn-primary m-input-and-button__btn JSKatalogNo" data-check="0">
			{{ AdminLanguage::transAdmin('Ne') }}</button> 
		</div>

		<div class="columns medium-3"> 
			<label>{{ AdminLanguage::transAdmin('Akcija') }}</label>

			<button class="btn btn-primary m-input-and-button__btn JSKatalog" title="{{ AdminLanguage::transAdmin('Dodeli') }}" data-kind="katalog_dodeli">
			<i class="fa fa-plus" aria-hidden="true"></i></button>

			&nbsp; 

			<button class="btn btn-primary m-input-and-button__btn JSKatalog" title="{{ AdminLanguage::transAdmin('Ukloni') }}" data-kind="katalog_ukloni">
			<i class="fa fa-times" aria-hidden="true"></i></button>  
		</div>

	</div>  
	@endif 
</div>


	<div id="JSmassEditModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Edit cena') }}</h4>

		<div class="row">
			<div class="medium-4 columns">
				<label for="">{{ AdminLanguage::transAdmin('Od cene') }}</label>
				<select id="JSMassEditCenaOd">
					<option value="nc">{{ AdminLanguage::transAdmin('Nabavna cena') }}</option>
					<option value="wc">{{ AdminLanguage::transAdmin('Web cena') }}</option>
					<option value="mc">{{ AdminLanguage::transAdmin('Maloprodajna cena') }}</option>
					<option value="ec">{{ AdminLanguage::transAdmin('End cena') }}</option>
				</select>
			</div>

			<div class="medium-4 columns">
				<label for="">{{ AdminLanguage::transAdmin('Za cenu') }}</label>
				<select id="JSMassEditCenaZa">
					<option value="nc">{{ AdminLanguage::transAdmin('Nabavna cena') }}</option>
					<option value="wc">{{ AdminLanguage::transAdmin('Web cena') }}</option>
					<option value="mc">{{ AdminLanguage::transAdmin('Maloprodajna cena') }}</option>
					<option value="ec">{{ AdminLanguage::transAdmin('End cena') }}</option>
				</select>
			</div>

			<div class="medium-4 columns">
				<label for="">{{ AdminLanguage::transAdmin('Preracunavanje Web ,MP i END cene') }}</label>
				
				<div class="btn-container"> 
					<button id="JSPreracunajWebCenuButton" class="btn btn-primary btn-small" title="WebCena">{{ AdminLanguage::transAdmin('Preracunaj Web Cenu') }}</button>
				</div>

				<div class="btn-container">
					<button id="JSPreracunajMPCenuButton" class="btn btn-primary btn-small" title="MpCena">{{ AdminLanguage::transAdmin('Preracunaj MP Cenu') }}</button>
				</div>

				<div class="btn-container">
					<button id="JSPreracunajEndCenuButton" class="btn btn-primary btn-small" title="EndCena">{{ AdminLanguage::transAdmin('Preracunaj End Cenu') }}</button>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="medium-6 columns">
				<label for="">{{ AdminLanguage::transAdmin('Marža') }} (%)</label>
				<input type="text" id="JSMassEditMarzaInput" class="m-input-and-button__input fl inp-sm" id="" placeholder="">
				<button id="JSMassEditMarzaButton" class="btn btn-primary m-input-and-button__btn"><i class="fa fa-plus" aria-hidden="true"></i></button>
				
				<label class="no-margin"> 
					<input type="checkbox" id="JSMassEditMarzaCheck">{{ AdminLanguage::transAdmin('Prihvati maržu') }}
				</label>
			</div>
			<div class="medium-6 columns">
				<label for="">{{ AdminLanguage::transAdmin('Web Cena') }}</label>
				<input type="text" class="m-input-and-button__input fl inp-sm" id="JSWebCenaEdit" placeholder="">
				<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSWebCenaEditBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
		</div>


		<div class="row"> 

			<div class="medium-6 columns">
				<label for="">{{ AdminLanguage::transAdmin('Rabat') }} (%)</label>
				<input type="text" id="JSMassEditRabatInput" class="m-input-and-button__input fl inp-sm" id="" placeholder="">
				<button id="JSMassEditRabatButton" class="btn btn-primary m-input-and-button__btn"><i class="fa fa-plus" aria-hidden="true"></i></button>
			</div>

			<div class="medium-6 columns">
				<label for="">{{ AdminLanguage::transAdmin('End Cena') }}</label>
				<input type="text" class="m-input-and-button__input fl inp-sm" id="JSEndCenaEdit" placeholder="">
				<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSEndCenaEditBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>

			<div class="medium-6 columns">
				<label for="">{{ AdminLanguage::transAdmin('MP Cena') }}</label>
				<input type="text" class="m-input-and-button__input fl inp-sm" id="JSMPEdit" placeholder="">
				<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSMPEditBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>


			<div class="medium-6 columns">
				<label for="">{{ AdminLanguage::transAdmin('Zaokruži web cenu') }}</label>
				<input type="text" class="m-input-and-button__input fl inp-sm" id="JSzaokruziweb" placeholder="{{ AdminLanguage::transAdmin('Poslednje dve cifre') }}">
				<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSZaokruziWebCenuButton"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
			<div class="medium-6 columns">
				<label for="">{{ AdminLanguage::transAdmin('Zaokruži mp cenu') }}</label>
				<input type="text" class="m-input-and-button__input fl inp-sm" id="JSzaokruzimp" placeholder="{{ AdminLanguage::transAdmin('Poslednje dve cifre') }}">
				<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSZaokruziMpCenuButton"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
			<div class="medium-6 columns">
				<label for="">{{ AdminLanguage::transAdmin('Zaokruži end cenu') }}</label>
				<input type="text" class="m-input-and-button__input fl inp-sm" id="JSzaokruziend" placeholder="{{ AdminLanguage::transAdmin('Poslednje dve cifre') }}">
				<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSZaokruziEndCenuButton"><i class="fa fa-check" aria-hidden="true"></i></button>
			</div>
		</div> 
	</div>


		<div id="JScharacteristicsModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Dodela karakteristika') }}</h4>
			<div id="JSTableContentGenerisane" class="m-input-and-button small-margin-bottom column medium-12">	
			</div>
		</div>


		<div id="JSobradaKarakteristikaModal" class="reveal-modal xsm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Obrada karakteristika') }}</h4>
			<br>
			<div class="row"> 
				<div class="column medium-6 large-6">
					<select id="JSKarakteristikaVrsta">
						<option value="">{{ AdminLanguage::transAdmin('Izaberite vrstu karakteristika') }}</option>
						<option value="0">{{ AdminLanguage::transAdmin('HTML karak teristike') }}</option>
						<option value="1">{{ AdminLanguage::transAdmin('Generisane karakteristike') }}</option>
						@if(Admin_model::check_admin(array('WEB_IMPORT')))
						<option value="2">{{ AdminLanguage::transAdmin('Od dobavljača') }}</option>
						@endif
					</select>
				</div>

				<div class="column medium-6 large-6">
					<button id="JSKarakteristikeDodeli" class="btn btn-primary m-input-and-button__btn">{{ AdminLanguage::transAdmin('Dodeli') }}</button>
					<button id="JSKarakteristikeObrisi" class="btn btn-danger m-input-and-button__btn">{{ AdminLanguage::transAdmin('Obriši') }}</button>
				</div>
			</div>
			<hr>

			<div class="column medium-12 large-12">
				{{ AdminLanguage::transAdmin('Prebaci HTML karakteristike u opis') }}
				<button id="JSHtmlPrebaci" class="btn btn-primary m-input-and-button__btn">{{ AdminLanguage::transAdmin('Prebaci') }}</button>
			</div>
		</div>

		<div id="JSArticleEditModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Izmena artikla') }}</h4>	
			<div class="column medium-12 large-12 text-center">
				{{ AdminLanguage::transAdmin('Artikal je zaključan! Da li želite da otključate artikal') }}?
			</div>
			<div class="column medium-12 large-12 text-center">
				<button id="JSArticleEditYes" class="btn btn-primary m-input-and-button__btn">{{ AdminLanguage::transAdmin('DA') }}</button>
				<button id="JSArticleEditNo" class="btn btn-danger m-input-and-button__btn">{{ AdminLanguage::transAdmin('NE') }}</button>
			</div>
		</div>

		<div id="JStextEditModal" class="reveal-modal sm-modal txt-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Edit teksta') }}</h4>

			<div class="row">
				<div class="columns medium-5 text-right">
					<input type="radio" id="JStextEditNaziv" name="naziv-opis" value="naziv" checked />{{ AdminLanguage::transAdmin('Naziv') }}
				</div>
				<div class="columns medium-6 center">
					<input type="radio" id="JStextEditOpis" name="naziv-opis" value="opis">{{ AdminLanguage::transAdmin('Opis') }}
				</div>
			</div> 

			<h3 class="title-txt">{{ AdminLanguage::transAdmin('Dodavanje teksta') }}</h3>

			<div class="row"> 
				<div class="columns medium-6 center">
					<input type="radio" id="JStextEditStart" name="start-end" value="start" checked /><label>{{ AdminLanguage::transAdmin('Na početku') }}</label> 
					<input type="radio" id="JStextEditEnd" name="start-end" value="end"><label>{{ AdminLanguage::transAdmin('Na kraju') }}</label>	
				</div>
				<div class="columns medium-6">	  
					<input type="text" id="JStextEditNazivInput" class="m-input-and-button__input"  placeholder="{{ AdminLanguage::transAdmin('Tekst') }}">
					<button id="JStextInsertButton" class="btn btn-primary btn-abs m-input-and-button__btn">{{ AdminLanguage::transAdmin('Dodaj') }}</button>
				</div>
			</div> 
			<br>

			<h3 class="title-txt">{{ AdminLanguage::transAdmin('Zameni tekst') }}</h3>

			<div class="row"> 
				<div class="columns medium-12">
					<div class="row"> 
						<div class="columns medium-6 center">
							<input type="radio" id="JStextEditDeo" name="ceo-deo" value="deoTeksta" checked /><label>{{ AdminLanguage::transAdmin('Deo teksta') }}</label> 
							<input type="radio" id="JStextEditCelo" name="ceo-deo" value="celaRec"><label>{{ AdminLanguage::transAdmin('Cela reč') }}</label>  
						</div>
					</div>
					<div class="row"> <br>
						<div class="columns medium-6">
							<input type="text" name="JStextEditInput1" id="JStextEditInput1" class="m-input-and-button__input" placeholder="{{ AdminLanguage::transAdmin('Tekst koji se menja') }}">
						</div>
						<div class="columns medium-6"> 
							<input type="text" name="JStextEditInput2" id="JStextEditInput2" class="m-input-and-button__input" placeholder="{{ AdminLanguage::transAdmin('Novi tekst') }}">
							<button id="JStextReplaceButton" class="btn btn-primary btn-abs m-input-and-button__btn">{{ AdminLanguage::transAdmin('Zameni') }}</button>
						</div> 
					</div>
				</div> 
			</div>
			<br>
			<label id="Results" class='error'></label>
		</div> 

		<div id="JSakcijaModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('AKCIJA') }}</h4>  

			<div class="JSakc_tabs">
				@if(AdminOptions::checkB2C())
				<a href="b2c" class="btn btn-primary btn-small active">B2C</a>
				@endif
				@if(AdminOptions::checkB2B())
				<a href="b2b" class="btn btn-primary btn-small">B2B</a>
				@endif
			</div>

			<!-- B2C -->
			<div data-akc="b2c"> 
				<div class="row">
					<div class="columns medium-4">
						<label>{{ AdminLanguage::transAdmin('Unesite popust na B2C') }}</label>
						<input type="text" style="width: 90%;" id="JSAkcijaAdd" placeholder="{{ AdminLanguage::transAdmin('Akcijski popust') }} (%)"> 
					</div>
					<div class="column medium-4 akcija-column">
						<label>{{ AdminLanguage::transAdmin('Datum akcije od') }}</label>

						<div class="relative"> 
							<input class="akcija-input" id="datum_akcije_od" name="datum_akcije_od" autocomplete="off" type="text" required>
							<span id="datum_od_delete" class="absolute-right text-red"><i class="fa fa-times" aria-hidden="true"></i></span>
						</div>
					</div> 

					<div class="column medium-4 akcija-column">
						<label>{{ AdminLanguage::transAdmin('Datum akcije do') }}</label>

						<div class="relative"> 
							<input class="akcija-input" id="datum_akcije_do" name="datum_akcije_do" autocomplete="off" type="text" required>
							<span id="datum_do_delete" class="absolute-right text-red"><i class="fa fa-times" aria-hidden="true"></i></span>
						</div>
					</div>	 
				</div> 

				<br>

				<div class="btn-container text-center"> 
					<button class="btn-small btn btn-secondary" id="JSAkcijaBtn">&nbsp; {{ AdminLanguage::transAdmin('Unesi') }} &nbsp;</button> 
				</div>
			</div>

			<!-- B2B -->
			<div data-akc="b2b" hidden="hidden"> 
				<div class="row">
					<div class="columns medium-4">
						<label>{{ AdminLanguage::transAdmin('Unesite popust na B2B') }}</label>
						<input type="text" style="width: 90%;" id="JSAkcijaB2BAdd" placeholder="{{ AdminLanguage::transAdmin('Akcijski popust') }} (%)"> 
					</div>

					<div class="column medium-4 akcija-column">
						<label>{{ AdminLanguage::transAdmin('Datum akcije od') }}</label>

						<div class="relative"> 
							<input class="akcija-input" id="b2b_datum_akcije_od" name="b2b_datum_akcije_od" autocomplete="off" type="text" required>
							<span id="b2b_datum_od_delete" class="absolute-right text-red"><i class="fa fa-times" aria-hidden="true"></i></span>
						</div>
					</div> 

					<div class="column medium-4 akcija-column">
						<label>{{ AdminLanguage::transAdmin('Datum akcije do') }}</label>

						<div class="relative"> 
							<input class="akcija-input" id="b2b_datum_akcije_do" name="b2b_datum_akcije_do" autocomplete="off" type="text" required>
							<span id="b2b_datum_do_delete" class="absolute-right text-red"><i class="fa fa-times" aria-hidden="true"></i></span>
						</div>
					</div>	 
				</div> 

				<br>

				<div class="btn-container text-center"> 
					<button class="btn-small btn btn-secondary" id="JSAkcijaB2BBtn">&nbsp; {{ AdminLanguage::transAdmin('Unesi') }} &nbsp;</button> 
				</div>
			</div>   
		</div>

		<div id="JSGarancijaModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Garancija') }}</h4>

			<div class="row">
				<div class="columns medium-6">
					<label>{{ AdminLanguage::transAdmin('Garancija broj meseci') }}</label>
					<input type="text" id="JSGarancijaMeseci" value="0"> 
				</div>

				<div class="column medium-6">
					<br>
					
					<label> 
						<input type="checkbox" id="JSProduzenaGarancija"> 
						{{ AdminLanguage::transAdmin('Produžena garancija') }} 
					</label>
				</div>  
			</div> 

			<br>

			<div class="text-center"> 
				<button class="btn btn-secondary btn-small" id="JSGarancijaSave"> {{ AdminLanguage::transAdmin('Sačuvaj') }} </button>
				<button class="btn btn-secondary btn-small" id="JSGarancijaDelete"> {{ AdminLanguage::transAdmin('Obriši') }} </button> 
			</div>
		</div>

		<div id="JSDimenzijeModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Dimenzije') }}</h4>
			<div class="row">
				<div class="m-input-and-button small-margin-bottom column medium-12">
					<input type="text" class="m-input-and-button__input" id="JSVisinaAdd" placeholder="{{ AdminLanguage::transAdmin('Promeni visinu') }}">
					<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSVisinaBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
				</div>
				<div class="m-input-and-button small-margin-bottom column medium-12">
					<input type="text" class="m-input-and-button__input" id="JSSirinaAdd" placeholder="{{ AdminLanguage::transAdmin('Promeni širinu') }}">
					<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSSirinaBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
				</div>
				<div class="m-input-and-button small-margin-bottom column medium-12">
					<input type="text" class="m-input-and-button__input" id="JSDuzinaAdd" placeholder="{{ AdminLanguage::transAdmin('Promeni dužinu') }}">
					<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSDuzinaBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
				</div>
				<div class="m-input-and-button small-margin-bottom column medium-12">
					<input type="text" class="m-input-and-button__input" id="JSBrutoMasaAdd" placeholder="{{ AdminLanguage::transAdmin('Promeni bruto masu') }}">
					<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSBrutoMasaBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
				</div>
				<div class="m-input-and-button small-margin-bottom column medium-12">
					<select class="m-input-and-button__input" id="JSJedinicaMereMaseAdd">
						<option value="">{{ 'Promeni jedinicu mere mase' }}</option>
						@foreach(AdminSupport::getJediniceMere() as $row)
						<option value="{{ $row->jedinica_mere_id }}">{{ $row->naziv }}</option>
						@endforeach
					</select>
					<button class="m-input-and-button__btn btn btn-secondary btn-xm" id="JSJedinicaMereMaseBtn"><i class="fa fa-check" aria-hidden="true"></i></button>
				</div>
			</div>
		</div>
