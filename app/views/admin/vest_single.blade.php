<section id="main-content">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	<div class="m-subnav"> 
		<a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/vesti" >
			<div class="m-subnav__link__icon">
				<i class="fa fa-arrow-left" aria-hidden="true"></i>
			</div>
			<div class="m-subnav__link__text">
				{{ AdminLanguage::transAdmin('Nazad') }}
			</div>
		</a> 
	</div>

	<div class="row"> 
		<div class="column medium-10 large-10 large-centered"> 
			<form action="{{AdminOptions::base_url()}}admin/vesti/{{ $web_vest_b2c_id }}/save" class="flat-box" method="post" enctype="multipart/form-data">
				<input type="hidden" name="id" value="{{ $web_vest_b2c_id }}">
				<input type="hidden" name="jezik_id" value="{{ $jezik_id }}">

				<div class="row">
					<div class="columns medium-9">   
						<label for="">{{ AdminLanguage::transAdmin('Naslov') }}</label>
						<input class="news-single-title" type="text" name="naslov" value="{{ htmlentities(!is_null(Input::old('naslov')) ? Input::old('naslov') : $naslov) }}" {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'readonly' : '' }}>

						<div class="{{ $errors->first('naslov') ? ' error' : '' }}">{{$errors->first('naslov')}}</div>
					</div>

					<div class="columns medium-3">  
						<label>&nbsp;</label>
						<div class="inline-block"> 
							<div class="{{ $errors->first('rbr') ? ' error' : '' }}">
								<label class="inline-block">{{ AdminLanguage::transAdmin('Redni broj') }}</label>
								<input class="ordered-number" type="text" name="rbr" value="{{ !is_null(Input::old('rbr')) ? Input::old('rbr') : $rbr }}" {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>
						</div>

						&nbsp;

						<a class="btn btn-primary btn-secondary" href="{{ AdminVesti::news_link($web_vest_b2c_id) }}" target="_blank">{{ AdminLanguage::transAdmin('Vidi vest') }}</a>
					</div>
				</div> 

				<br>

				<div class="row">  
					<div class="columns">
						@if(in_array(AdminSupport::fileExtension($slika),array('jpg','png','jpeg','gif')))
						<img class="news-pic" src="{{ $slika }}">
						@else
						<iframe width="560" height="315" src="{{ $slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>					
						@endif

						@if(Admin_model::check_admin(array('VESTI_AZURIRANJE')))
						<div class="input-group">
							<div class="bg-image-file">
								<label>{{ AdminLanguage::transAdmin('Dodaj sliku ili video') }}</label>
								<input type="file" name="news_img"> 
							</div>
							<div class="">
								<label>{{ AdminLanguage::transAdmin('Dodaj link za sliku ili video') }}</label>
								<input type="text" name="news_img_link" value="{{$slika}}"> 
							</div>
						</div>
						@endif
 
						@if(Admin_model::check_admin(array('VESTI_AZURIRANJE')))
							@if($web_vest_b2c_id != 0 && count($jezici) > 1) 
							<div class="languages">
								<ul>	
									@foreach($jezici as $jezik)
										<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/vest/{{$web_vest_b2c_id}}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
									@endforeach
								</ul>
							</div> 
							@endif
						@endif

						<div class="field-group{{ $errors->first('tekst') ? ' error' : '' }}">
							<textarea {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'readonly' : '' }} @if(Admin_model::check_admin(array('VESTI_AZURIRANJE')))) class="special-textareas" @endif name="tekst" id="content">{{ !is_null(Input::old('tekst')) ? Input::old('tekst') : $tekst }}</textarea>
						</div>

						<h3 class="center seo-info tooltipz" aria-label="{{ AdminLanguage::transAdmin('SEO (Search Engine Optimization) polja je potrebno popuniti jednostavnim jezikom, kako bi Vas kupci lakše pronašli na internetu - ovi opisi nisu vidljivi direktno u prodavnici, već samo kako bi Google prepoznao Vaš proizvod i povezao Vas sa ljudima koji ga traže') }}.">{{ AdminLanguage::transAdmin('SEO') }}</h3>

						<div class="field-group{{ $errors->first('seo_title') ? ' error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Naslov (title) do 60 karaktera') }}</label>
							<input type="text" name="seo_title" value="{{ htmlentities(Input::old('seo_title') ? Input::old('seo_title') : $seo_title) }}" {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div>

						<div class="field-group{{ $errors->first('description') ? ' error' : '' }}">
							<label for="description">{{ AdminLanguage::transAdmin('Opis (description) do 158 karaktera') }} </label>
							<input type="text" name="description"
							value="{{ Input::old('description') ? Input::old('description') : $description }}" {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'readonly' : '' }} />
						</div>
 
						<div class="field-group{{ $errors->first('keywords') ? ' error' : '' }}">
							<label for="keywords">{{ AdminLanguage::transAdmin('Ključne reči (keywords)') }}</label>
							<input type="text" name="keywords"
							value="{{ Input::old('keywords') ? Input::old('keywords') : $keywords }}" {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'readonly' : '' }} />
						</div>

						<label>
							<input type="checkbox" name="aktuelno" {{ $aktuelno == 1 ? 'checked' : '' }} {{ Admin_model::check_admin(array('VESTI_AZURIRANJE')) == false ? 'disabled' : '' }} >{{ AdminLanguage::transAdmin('Aktivna vest') }} 
						</label>

					</div>
				</div>

				@if(Admin_model::check_admin(array('VESTI_AZURIRANJE'))) 
				<div class="btn-container text-center"> 
					<button class="btn btn-large news-save save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					@if($web_vest_b2c_id != 0)
					<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/vesti/{{ $web_vest_b2c_id }}/delete">{{ AdminLanguage::transAdmin('Obriši') }}</button>
					@endif
				</div> 
				@endif		
			</form>
		</div>	
	</div> 
</section>