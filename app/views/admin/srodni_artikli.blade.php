<div id="main-content">
	
	@include('admin/partials/product-tabs')
	
	@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
	<div class="row">
		<div class="columns medium-6 medium-centered">
			<br>

			<div class="relative">
				<input class="" autocomplete="off" data-timer="" type="text" id="articles1" data-id="{{ $roba_id }}" placeholder="{{ AdminLanguage::transAdmin('Unesite id ili ime artikla ili prozvođača ili krajnju grupu') }}" />
				
				<div class="flat-box"> 
					<div id="search_content1"></div>
				</div>
			</div>
		</div>
	</div>
	@endif

	<div class="row"> 
		<div class="columns medium-6 medium-centered">
			<div class="flat-box">
				<div class="table-scroll"> 
					<table>
						@if(count($srodni_artikli)>0)
						<thead>
							<tr>
								<th>{{ AdminLanguage::transAdmin('ROBA ID') }}</th>
								<th>{{ AdminLanguage::transAdmin('GRUPA') }}</th>
								<th>{{ AdminLanguage::transAdmin('NAZIV') }}</th>
								<th colspan="4">{{ AdminLanguage::transAdmin('FLAG CENE') }}</th>  
							</tr>
						</thead>

						<tbody> 
						@foreach($srodni_artikli as $row)
							<tr>
								<td>{{ $row->srodni_roba_id }}</td>

								<td>
									{{ AdminArticles::findGrupe(AdminArticles::find($row->srodni_roba_id,'grupa_pr_id'),'grupa') }}
								</td>

								<td>
									<div class="max-width-450 no-white-space"> 
										{{ AdminArticles::find($row->srodni_roba_id,'naziv_web') }}
									</div>
								</td>

								<td>						
									<select class="SrodniVrednost" data-store='{"roba_id":"{{ $roba_id }}", "srodni_roba_id":"{{ $row->srodni_roba_id }}"}' {{ Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')) == false ? 'disabled' : '' }}>
										<option value=-1>
											{{ AdminLanguage::transAdmin('Izaberite vrednost') }}
										</option>
										@foreach(AdminGroups::karakteristikeNaziv(AdminArticles::find($row->srodni_roba_id,'grupa_pr_id')) as $karak)
										<option disabled>
											{{$karak->naziv}}
										</option>
										@foreach(AdminGroups::karakteristikeVrednost($karak->grupa_pr_naziv_id) as $vred)
										@if($row->grupa_pr_vrednost_id == $vred->grupa_pr_vrednost_id)
										<option value="{{$vred->grupa_pr_vrednost_id}}" selected>
											{{$vred->naziv}}
										</option>
										@else
										<option value="{{$vred->grupa_pr_vrednost_id}}">
											{{$vred->naziv}}
										</option>
										@endif
										@endforeach
										@endforeach
									</select> 
								</td>

								<td>
									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<input type="checkbox" class="JSFlagCenaVezanSrodni" data-store='{"roba_id":"{{ $roba_id }}", "srodni_roba_id":"{{ $row->srodni_roba_id }}"}' {{ $row->flag_cena == 1 ? 'checked' : '' }}>
									@endif
								</td>

								<td>
									@if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
									<button class="JSDeleteSrodni btn btn-xm btn-secondary" data-store='{"roba_id":"{{ $roba_id }}", "srodni_roba_id":"{{ $row->srodni_roba_id }}"}'><i class="fa fa-times"></i></button>
									@endif
								</td>
							</tr>
						@endforeach
						</tbody>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>
</div>  
