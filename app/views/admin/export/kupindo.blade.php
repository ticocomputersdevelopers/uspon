<html>
	<head>
		<title>{{ AdminLanguage::transAdmin('Export Kupindo') }}</title>
		<link href="{{ AdminOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
		<link href="{{ AdminOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
       
        <style>

body {
	background-color: #f2f2f2;
}
* {
	box-sizing: border-box;
}
.row::after {
    content: "";
    clear: both;
    display: table;
}
[class*="col-"] {
    float: left;
}
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

.logo{
	display: block;
    padding: 10px;
    text-align: center;
    margin: auto;
}	
header{
	background-color: #4e0e7c;
}
.text-center {text-align: center;}
.button {
    display: inline-block;
    padding: 5px 10px;
    background-color: #f80;
    margin: 0;
    border-radius: 5px;
}
.button:hover{
	background-color: #f60;
	transition: .1s ease-in-out;
}
.input-div input{
	border-radius: 5px;
}
.select-div span ul li input{
    margin: 5px;
    height: auto;
    width: auto !important;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice{
    margin: 4px 2px;
    font-size: 13px;
}
.select2-results__option{
	padding: 0 5px !important;
    font-size: 13px;
}
</style>
	</head>
	<body>
		 
		<header> 
			<div class="row"> 
				<h1><img class="logo" src="https://www.kupindo.com/images/Kupindo_deo_limundograda-alpha.png" alt="Kupindo Logo"></h1>
			</div>
		</header>

	   <div class="row"> 
			<form method="POST" action="{{AdminOptions::base_url()}}export/kupindo-config/{{ $key }}">
				<input type="hidden" name="export_id" value="{{ $export_id }}">
				<input type="hidden" name="kind" value="{{ $kind }}">

				<div class="row">
					<br>
				 	<h2 class="text-center">{{ AdminLanguage::transAdmin('Export - Kupindo') }}</h2>
					<div class="col-3 text-center">
						<a href="{{ AdminOptions::base_url()}}export/export-grupe/{{$export_id}}/{{$kind}}/{{$key}}" class="button">{{ AdminLanguage::transAdmin('Upari grupe') }}</a>
					</div>

					<div class="col-6 text-center select-div">		
						<select id="grupa_pr_ids" name="grupa_pr_ids[]" multiple="multiple">
						{{ AdminSupport::select2Groups() }}
						</select> 
					</div>

					<div class="col-3 text-center input-div">
						<input class="button" type="submit" value="{{ AdminLanguage::transAdmin('Eksportuj') }}">
					</div>
				</div>

			</form>
		</div>

		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
		<script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
		<script src="{{ AdminOptions::base_url()}}js/admin/admin_export.js" type="text/javascript"></script>


	</body>
	<script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
	<script type="text/javascript">
	  	$("#grupa_pr_ids").select2({
	  		placeholder: {{ AdminLanguage::transAdmin('Izaberi grupe') }}
	  	});
	</script>
</html>