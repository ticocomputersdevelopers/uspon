<html>
	<head>
		<title>{{ AdminLanguage::transAdmin('Export Shopify') }}</title>
		<link href="{{ AdminOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
		<link href="{{ AdminOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
       
        <style>

* { box-sizing: border-box; }

.row::after {
    content: "";
    clear: both;
    display: table;
}
[class*="col-"] {
    float: left;
}
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

.text-center { text-align: center; }

header { background-color: #f2f2f2; padding: 30px 0; }

.custom-col { padding: 20px 0; }

.custom-btn { background: #a6a6a6; padding: 10px 15px; }

        </style>
	</head>
	<body>

		<form method="POST" action="{{AdminOptions::base_url()}}export/shopify-config/{{ $key }}">
			<input type="hidden" name="export_id" value="{{ $export_id }}">
			<input type="hidden" name="kind" value="{{ $kind }}">

			<header class="text-center">
				<h1>{{ AdminLanguage::transAdmin('Export - Shopify') }}</h1>
			</header>

			<div class="row">
					<div class="col-4 custom-col">
						<div><input name="name" type="checkbox" checked> {{ AdminLanguage::transAdmin('Naziv') }}</div>
						<div><input name="url" type="checkbox" checked> {{ AdminLanguage::transAdmin('URL proizvoda') }}</div>
						<div><input name="title_meta" type="checkbox" checked> {{ AdminLanguage::transAdmin('Meta titl') }}</div>
						<div><input name="description_meta" type="checkbox" checked> {{ AdminLanguage::transAdmin('Meta opis') }}</div>
						<div><input name="tags" type="checkbox" checked> {{ AdminLanguage::transAdmin('Tagovi') }}</div>
						<div><input name="description" type="checkbox" checked> {{ AdminLanguage::transAdmin('Opis') }}</div>
						<div><input name="characteristics" type="checkbox" checked> {{ AdminLanguage::transAdmin('Osobine') }}</div>
						<div><input name="manufacturer" type="checkbox" checked> {{ AdminLanguage::transAdmin('Proizvođac') }}</div>			 
					</div>

					<div class="col-4 custom-col">
						<div><input name="image" type="checkbox" checked> {{ AdminLanguage::transAdmin('Slike') }}</div>
						<div><input name="additional_images" type="checkbox" checked> {{ AdminLanguage::transAdmin('Dodatne slike') }}</div>
						<div><input name="published" type="checkbox" checked> {{ AdminLanguage::transAdmin('Vidljivost') }}</div>
						<div><input name="price" type="checkbox" checked> {{ AdminLanguage::transAdmin('Cena') }}</div>
						<div><input name="action" type="checkbox" checked> {{ AdminLanguage::transAdmin('Akcija') }}</div>
					</div>

					<div class="col-4 custom-col">
						<div><input name="tax" type="checkbox" checked> {{ AdminLanguage::transAdmin('Porez') }}</div>
						<div><input name="quantity" type="checkbox" checked> {{ AdminLanguage::transAdmin('Kolicina') }}</div>
						<div><input name="stock" type="checkbox" checked> {{ AdminLanguage::transAdmin('Vođenje lagera') }}</div>
						<div><input name="shiping" type="checkbox" checked> {{ AdminLanguage::transAdmin('Slanje') }}</div>
						<div><input name="weight" type="checkbox" checked> {{ AdminLanguage::transAdmin('Tezina') }}</div>
						<div><input name="barcode" type="checkbox" checked> {{ AdminLanguage::transAdmin('Barkod') }}</div>
						<div><input name="type" type="checkbox" checked> {{ AdminLanguage::transAdmin('Tip') }}</div>
						<div><input name="is_full" type="checkbox" checked> {{ AdminLanguage::transAdmin('Prikazi ostale kolone') }}</div>
					</div>

					<div class="col-12 text-center"> 
			 			<button class="custom-btn" type="submit">{{ AdminLanguage::transAdmin('Eksportuj') }}</button>
			 		</div>
			</div>
			
		</form>
		<script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
		<script src="{{ AdminOptions::base_url()}}js/admin/admin_export.js" type="text/javascript"></script>
</body>
</html>