<!DOCTYPE html>
<html><head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php /* header('Content-Type: text/html; charset=utf-8;'); */?> 
<title>{{ AdminLanguage::transAdmin('PDF') }}</title>
<style>

.row::after {content: ""; clear: both; display: table;}

[class*="col-"] { float: left; padding: 5px; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

* {margin: 0; font-family: DejaVu Sans; box-sizing: border-box;}

body {font-size: 16px;}

table {border-collapse: collapse; font-size: 12px; margin-bottom: 15px; width: 100%;}

td, th {border: 1px solid #cacaca; text-align: left; padding: 3px;}

tr:nth-child(even) {background-color: #f5f6ff;}

h2 {margin-bottom: 10px;}

.container {width: 90%;	margin: 42px auto;}

.logo img {	max-width: 150px; width: 100%;}

.text-right {text-align: right;}

.signature {background: none; color: #808080;}

.signature td, th {border: none; background: none;}
 
.company-info {font-size: 11px;}

.comp-name {font-size: 18px; font-weight: bold;}
 
.kupac-info td {border: none;}

.kupac-info { border: 1px solid #ddd; }

thead {background: #eee;}

.ziro {font-weight: bold; font-size: 14px; margin: 10px 0;}
 
.napomena {
	padding-top: 60px;
}

.napomena p {margin: 5px 0; font-size: 12px; text-align: center; line-height: 1}

.info-1 {margin-bottom: 30px;}

.rbr {width: 50px;}

.artikli td {text-align: center;}

.cell-product-name {text-align: left !important;}

.sum-span {margin-right: 20px;}

.summary { padding: 5px 0;  }

.page-break {
    page-break-after: always;
}

/* Custom */
#main_row {
	display: flex;
	align-items: center;
	justify-content: space-between;
}
.skewed {
	background: #05a34f;
    transform: skewX(-10deg);
	-webkit-transform: skewX(-10deg);
    padding: 7px 5px;
}
.narudzbina-heading {
	font-weight: bold;
	font-size: 16px;
}
.no-padding {  padding: 0;  }
.my-1 { margin: 10px 0;  }
.my-2 { margin: 20px 0;  }
.my-3 { margin: 30px 0;  }

img { width: 50%; height: 50%; object-fit: cover }

.skewedRed {
	background: #e91b25;
    transform: skewX(-10deg);
    -webkit-transform: skewX(-10deg);
    padding: 5px 10px;
    display: inline-block;
    color: #fff;
    text-align: center;
}
.between {
	display: flex;
    justify-content: space-between;
    align-items: center;
}
.text-bold { font-weight: bold;  }

/*.info > div { margin: 10px 0;  }*/
.gray-text { color: gray; margin-right: 10px }
/*.info div:first-child {  margin-bottom: 20px;  }*/
/* End of Custom */
table.pdf_table {
  border: 1px solid #DDDDDD;
  width: 100%;
  border-collapse: collapse;
}
table.pdf_table td, table.pdf_table th {
  border: 1px solid #AAAAAA;
  padding: 3px 2px;
}
table.pdf_table tbody td {
  font-size: 13px;
}
table.pdf_table tr:nth-child(even) {
  background: #D0E4F5;
}
table.pdf_table thead {
  background: #DDDDDD;
  background: -moz-linear-gradient(top, #e5e5e5 0%, #e0e0e0 66%, #DDDDDD 100%);
  background: -webkit-linear-gradient(top, #e5e5e5 0%, #e0e0e0 66%, #DDDDDD 100%);
  background: linear-gradient(to bottom, #e5e5e5 0%, #e0e0e0 66%, #DDDDDD 100%);
  border-bottom: 1px solid #444444;
}
table.pdf_table thead th {
  font-size: 15px;
  font-weight: bold;
  text-align: center;
  color: #000000;
  border-left: 1px solid #DDDDDD;
}
table.pdf_table thead th:first-child {
  border-left: none;
}

</style>
</head><body>


	<?php
		$show_header = true;
	    $limit = 6;
	    $stavke_count = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->count();
		$count_pages = floor($stavke_count/$limit);
		$mod = fmod($stavke_count,$limit);
		if($mod != 0) {
			$count_pages++;
		}

	    $rbr = 1;
	?>
		@foreach(range(0,$count_pages-1) as $page)
		<?php $stavke = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->limit($limit)->offset($page*$limit)->get(); ?>
		@if(!($page == ($count_pages-1) and !$show_header))
		<div class="container">
			@include('admin.partials.pdf.profaktura_header')
		@endif

		<div class="artikli">
			@if(!($page == ($count_pages-1) and !$show_header))
	        @endif
	        	
        	<div class="row">
        		<div class="col-12">

        			<table class="pdf_table">
						<thead>
							<tr>
								<th>Naziv proizvoda</th>
								<th>JM</th>
								<th>Cena</th>
								<th>PDV%</th>
								<th>Kol.</th>
								<th>Iznos sa PDV</th>
							</tr>
						</thead>
						
						<tbody>
        					@foreach($stavke as $row)
							<tr>
								<td class="text-bold">{{ Product::short_title($row->roba_id) }}</td>
								<td>Kom</td>
								<td>{{ AdminCommon::cena($row->jm_cena/(1 + Product::b2bTax($row->tarifna_grupa_id)/100)) }}</td>
								<td>PDV</td>
								<td>{{ (int)$row->kolicina }}</td>
								<td>{{ AdminCommon::cena($row->jm_cena) }}</td>
							</tr>
					        @endforeach

			        <?php $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first(); 
			        			$troskovi_isporuke = Cart::troskovi($web_b2c_narudzbina_id);
			        ?>
			      	@if($web_b2c_narudzbina->web_nacin_isporuke_id != 2  && $troskovi_isporuke > 0)
			      		<tr>
			      			<td class="text-bold">Troškovi isporuke</td>
			      			<td></td>
			      			<td>{{ AdminCommon::cena($troskovi_isporuke/1.2) }}</td>
									<td>PDV</td>
									<td>1</td>
									<td>{{ AdminCommon::cena($troskovi_isporuke) }}</td>
			      		</tr>
			      	@endif
						</tbody>
					</table>
        			
        		</div>
        	</div>
	       
        </div>

		@if($page == ($count_pages-1))
		    @include('admin.partials.pdf.profaktura_rekapitulacija')
			@include('admin.partials.pdf.profaktura_footer')
		@else
			<div class="page-break"></div>
			
		@endif
	
		@endforeach

	</div>




</body></html>
