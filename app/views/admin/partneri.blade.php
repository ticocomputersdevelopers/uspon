<div id="main-content">
	@if(Session::has('message') && !$dokumenti)
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@if(in_array(AdminOptions::web_options(130),array(0)))
	@include('admin/partials/sifarnik-tabs')  
	@endif

	<div>
		@if(AdminOptions::web_options(130) != 0) 
		<h3 class="title-med">{{(!$dokumenti) ? 'Partneri' : 'Kupci'}}</h3>
		@endif
		@if($dokumenti)
		<a class="btn btn-create btn-small" href="{{AdminOptions::base_url()}}dokumenti/partner/0">{{ AdminLanguage::transAdmin('Novi kupac') }}</a>
		@else
		<a class="btn btn-create btn-small" href="{{AdminOptions::base_url()}}admin/kupci_partneri/partner/0">{{ AdminLanguage::transAdmin('Novi partner') }}</a>
		@endif
	</div> 

	<div class="flat-box"> 
		<div class="row collapse"> 
			<form method="POST" action="{{AdminOptions::base_url()}}{{(!$dokumenti) ? 'admin/kupci_partneri' : 'dokumenti/partneri'}}/partneri-filter" id="JSPartneriFilter" class="columns medium-10">

				<div class="row collapse flex"> 

					<div class="columns medium-4">  
						<div class="flex">
							<input type="text" name="search" value="{{ urldecode($search) }}" placeholder="{{ AdminLanguage::transAdmin('Pretraga') }}..." class="m-input-and-button__input">

							<input class="btn btn-primary btn-small" value="Pretraga" type="submit">

							<a class="btn btn-danger btn-small" href="{{AdminOptions::base_url()}}{{(!$dokumenti) ? 'admin/kupci_partneri' : 'dokumenti'}}/partneri">{{ AdminLanguage::transAdmin('Poništi') }}</a>
						</div> 
					</div>

					@if(!$dokumenti)
					<div class="columns medium-3 text-right">
						<div class="inline-block"> 
							<label for="partner_vrsta_id" class="no-margin">{{ AdminLanguage::transAdmin('Vrsta partnera') }}</label>
						</div>

						<div class="inline-block"> 
							<select name="partner_vrsta_id" id="JSPartnerVrstaFilter">
								<option value="0">{{ AdminLanguage::transAdmin('Svi') }}</option>
								@foreach($partner_vrste as $partner_vrsta)
								<option value="{{ $partner_vrsta->partner_vrsta_id }}" {{ ($partner_vrsta_id == strval($partner_vrsta->partner_vrsta_id)) ? 'selected' : '' }}>{{ $partner_vrsta->naziv }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="columns medium-2 center"> 
						<div class="inline-block"> 
							<label for="partner_vrsta_id" class="no-margin">{{ AdminLanguage::transAdmin('API cenovnik') }}</label>
						</div>
						<div class="inline-block" style="min-width: 50px;"> 
							<select name="api_aktivan" id="JSPartnerApiFilter">
								<option value="null">{{ AdminLanguage::transAdmin('Svi') }}</option>
								<option value="1" {{ ($api_aktivan == '1') ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Da') }}</option>
								<option value="0" {{ ($api_aktivan == '0') ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Ne') }}</option>
							</select>
						</div>  
					</div>

					<div class="columns medium-2">
						<div class="inline-block"> 
							<label for="partner_vrsta_id" class="no-margin">{{ AdminLanguage::transAdmin('B2B pristup') }}</label>
						</div>
						<div class="inline-block" style="min-width: 50px;"> 
							<select name="b2b_access" id="JSPartnerB2BAccessFilter">
								<option value="null">{{ AdminLanguage::transAdmin('Svi') }}</option>
								<option value="1" {{ (!is_null($b2b_access) AND boolval($b2b_access)) ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Da') }}</option>
								<option value="0" {{ (!is_null($b2b_access) AND !boolval($b2b_access)) ? 'selected' : '' }}>{{ AdminLanguage::transAdmin('Ne') }}</option>
							</select>
						</div>
					</div>
					@endif
				</div>
			</form>

			@if(Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')))
			<div class="columns medium-2">
				<div class="text-right">
					<a class="btn btn-primary btn-small JSImport" href="#">{{ AdminLanguage::transAdmin('Import') }}</a>
					<a class="btn btn-primary btn-small JSExport" href="#">{{ AdminLanguage::transAdmin('Export') }}</a>
				</div>
			</div>
			@endif

		</div> 
	</div>

	<div id="JSchooseImport" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Import Partnera') }}</h4>
		<label>{{ AdminLanguage::transAdmin('Primer kako treba da izgleda Excel file') }}</label>
		<img src="{{AdminOptions::base_url()}}images/importPartnera.png">
		<div class="row text-center">
			<form method="POST" action="{{AdminOptions::base_url()}}admin/partner-import" enctype="multipart/form-data">
				<div class="bg-image-file"> 
					<input type="file" name="import_file">

					<button class="btn btn-primary btn-small" type="submit">{{ AdminLanguage::transAdmin('Importuj') }}</button>
					<div class="error"> 
						{{ $errors->first('import_file') ? $errors->first('import_file') : '' }}
					</div>
				</div>	 
			</form>
		</div>
	</div>

	<!-- export partnera -->
	<div id="JSchoose" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Export') }}</h4>
		<div class="row text-center">
			<h3 class="text-center">{{ AdminLanguage::transAdmin('Izaberite željene kolone') }}:</h3> 
			<div class="columns medium-2"> 
				<input type="checkbox" data-name="naziv" class="JSKolonaExport" data-status="naziv">{{ AdminLanguage::transAdmin('Naziv') }} 	
			</div>
			<div class="columns medium-2"> 
				<input type="checkbox" data-name="adresa" class="JSKolonaExport" data-status="adresa">{{ AdminLanguage::transAdmin('Adresa') }} 
			</div>
			<div class="columns medium-2"> 
				<input type="checkbox" data-name="mesto" class="JSKolonaExport" data-status="mesto">{{ AdminLanguage::transAdmin('Mesto') }} 
			</div>
			<div class="columns medium-2"> 
				<input type="checkbox" data-name="email" class="JSKolonaExport" data-status="email">{{ AdminLanguage::transAdmin('Email') }}
			</div> 
			<div class="columns medium-2"> 
				<input type="checkbox" data-name="telefon" class="JSKolonaExport" data-status="telefon">{{ AdminLanguage::transAdmin('Telefon') }}
			</div> 
			<div class="columns medium-2"> 
				<input type="checkbox" data-name="sifra" class="JSKolonaExport" data-status="sifra">{{ AdminLanguage::transAdmin('Šifra') }}
			</div> 
		</div> 
		<div class="text-center"><br>
			<button data-kind="xml" class="btn btn-primary m-input-and-button__btn JSExportPartneri" >{{ AdminLanguage::transAdmin('Export XML') }}</button>
			<button data-kind="xls" class="btn btn-primary m-input-and-button__btn JSExportPartneri" >{{ AdminLanguage::transAdmin('Export XLS') }}</button>
		</div>
	</div>

	<div class="flat-box">  
		<label class="no-margin">{{ AdminLanguage::transAdmin('Ukupno') }}: {{ $count }}</label>
		<div class="table-scroll"> 
			<table>
				<thead> 
					<tr>
						@if(AdminOptions::info_sys_active() == 1)
						<th>{{ AdminLanguage::transAdmin('Šifra') }}</th>
						@endif
						<th>{{ AdminLanguage::transAdmin('Naziv') }}</th>
						<th>{{ AdminLanguage::transAdmin('Adresa') }}</th>
						<th>{{ AdminLanguage::transAdmin('Mesto') }}</th>
						<th>{{ AdminLanguage::transAdmin('Email') }}</th>
						<th>{{ AdminLanguage::transAdmin('Telefon') }}</th>
						<th></th>
					</tr>
				</thead>
				
				<tbody>
					@foreach($partneri as $row)
					<tr>
						@if(AdminOptions::info_sys_active() == 1)
						<td>{{ $row->sifra }}</td>
						@endif
						<td>{{ $row->naziv }}</td>
						<td>{{ $row->adresa }}</td>
						<td>{{ $row->mesto }}</td>
						<td>{{ $row->mail }}</td>
						<td>{{ $row->telefon }}</td>
						<td class="table-col-icons">
							<a class="edit icon" href="{{AdminOptions::base_url()}}{{!$dokumenti ? 'admin/kupci_partneri' : 'dokumenti'}}/partner/{{ $row->partner_id }}"><i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{ AdminLanguage::transAdmin('Izmeni') }}</a>
							@if(Admin_model::check_admin(array('B2B_PARTNERI_AZURIRANJE')))
							<a class="remove icon JSbtn-delete" data-link="{{AdminOptions::base_url()}}{{!$dokumenti ? 'admin/kupci_partneri/partneri' : 'dokumenti/partner'}}/{{ $row->partner_id }}/0/delete"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;{{ AdminLanguage::transAdmin('Obriši') }}</a>

							@if(!is_null(AdminSupport::getLoggedUser('KOMERCIJALISTA')))
							<a class="remove icon" href="{{AdminOptions::base_url()}}admin/commercialist-b2b-login/{{ $row->partner_id }}" target="_blank" >&nbsp;&nbsp;&nbsp;{{ AdminLanguage::transAdmin('Naruči') }}</a>
							@endif
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>

			</table>
		</div>
		{{ Paginator::make($partneri,$count,20)->links() }}

	</div>
	<input type="hidden" id="JSCheckPartnerConfirm" data-id="{{ Session::has('confirm_partner_id')?Session::get('confirm_partner_id'):0 }}" data-dokumenti="{{ $dokumenti ? '1' : '0' }}">
</div>