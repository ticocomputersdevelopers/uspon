<script src="/Scripts/jquery-1.7.1.js"></script>
    <script type="text/javascript">
        function myFunction(chk) {
             document.getElementsByName("web_marza")[0].disabled = chk.checked
             document.getElementsByName("mp_marza")[0].disabled = chk.checked
        }
    </script>
<div id="main-content" class="kupci-page">
    @if(Session::has('message'))
        <script>
            alertify.success('{{ Session::get('message') }}');
        </script>
    @endif
    @if(Session::has('alert'))
        <script>
            alertify.error('{{ Session::get('alert') }}');
        </script>
    @endif
    <div class="row m-subnav">
        <div class="large-2 medium-3 small-12 columns ">
             <a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/web_import/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn/0/nn-nn" >
                <div class="m-subnav__link__icon">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </div>
                <div class="m-subnav__link__text">
                    {{ AdminLanguage::transAdmin('Nazad') }}
                </div>
            </a>
        </div>
    </div><br>

    <div class="row">
        <div class="medium-7 columns">
            <div class="table-groups-import">
                <div class="column medium-4 no-padd"> 
                    <input type="text" style='margin: 2px 0 0;' class="search-field" id="search_grupe" value="{{ $search ? $search : ''}}" autocomplete="on" placeholder="{{ AdminLanguage::transAdmin('Pretraži grupe') }}..."> 
                </div>
                <div class="column medium-3 no-padd"> 
                     <button type="submit" id="search_grupe-btn" value="{{$search}}" data-partner_id={{$partner_id}} class="m-input-and-button__btn btn btn-primary">
                        <i class="fa fa-search" aria-hidden="true"></i>                        
                    </button>
                    <a href="#" class="m-input-and-button__btn btn btn-danger">
                        <span class="fa fa-times tooltipz-left" data-partner_id={{$partner_id}} id="JSSearchClear" aria-hidden="true"></span>
                    </a>
                </div>
                <table>
                    <thead>
                        <tr class="rower">
                            <th>{{ AdminLanguage::transAdmin('Partner') }}</th>
                            <th>{{ AdminLanguage::transAdmin('Grupa dobavljača') }}</th>
                            <th>{{ AdminLanguage::transAdmin('Naša grupa') }}</th>
                            <th class="tooltipz" aria-label="Web marža">{{ AdminLanguage::transAdmin('WM') }}</th>
                            <th class="tooltipz" aria-label="MP marža">{{ AdminLanguage::transAdmin('MPM') }}</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="text-right">
                            <td>
                                <a class="btn btn-small btn-primary" href="{{AdminOptions::base_url()}}admin/import_podaci_grupa/{{ $partner_id }}/0/0">{{ AdminLanguage::transAdmin('Dodaj novi') }}
                                </a>
                            </td>
                        </tr>
                        @foreach($query as $grupa)
                        <tr class="rower">
                            <td>{{ AdminSupport::dobavljac($grupa->partner_id) }}</td>
                            <td>{{ $grupa->grupa}}</td>
                            <td>{{ AdminGroups::find($grupa->grupa_pr_id,'grupa') }}</td>
                            <td>{{ round($grupa->web_marza, 2)}}</td>
                            <td>{{ round($grupa->mp_marza, 2)}}</td>
                            <td>
                                <a class="btn btn-small btn-danger" href="{{AdminOptions::base_url()}}admin/import_podaci_delete/{{ $grupa->partner_id }}/{{ $grupa->partner_grupa_id }}/0">{{ AdminLanguage::transAdmin('Obriši') }}</a>
                            </td>
                            <td>
                                <a class="btn btn-small btn-primary" href="{{AdminOptions::base_url()}}admin/import_podaci_grupa/{{ $grupa->partner_id }}/{{ $grupa->partner_grupa_id }}/0">{{ AdminLanguage::transAdmin('Izmeni') }}</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="medium-5 columns">
            <div class="flat-box">
                <h1 class="title-med">{{ AdminLanguage::transAdmin('Import podaci') }}</h1>
                <form method="POST" action="{{AdminOptions::base_url()}}admin/import_podaci_save" >
                    <input type="hidden" name="partner_grupa_id" value="{{$partner_grupa_id}}">
                    <div class="columns medium-12 field-group">
                        <div class="columns medium-12 field-group {{ $errors->first('partner_id') ? ' error' : '' }}">
                            <label>{{ AdminLanguage::transAdmin('Partneri') }}</label>
                            <select name="partner_id" class="m-input-and-button__input import-select search_select" id="JSPartneriSelect">
                                <option value="0">{{ AdminLanguage::transAdmin('Svi partneri') }}</option>
								@foreach(AdminImport::dobavljaci_web_import_tabela() as $dobavljac)
                                @if($dobavljac->partner_id == $partner_id)
                                <option value="{{ $dobavljac->partner_id }}" selected>{{ $dobavljac->naziv }}</option>
                                @else
                                <option value="{{ $dobavljac->partner_id }}">{{ $dobavljac->naziv }}</option>
                                @endif
                                @endforeach
                            </select>
                            {{ $errors->first('partner_id') }}
                        </div>
                        <div class="columns medium-12 field-group {{ $errors->first('grupa') ? ' error' : '' }}">
                            <label>{{ AdminLanguage::transAdmin('Grupa dobavljača') }}</label>
                            <div class="short-group-container">
                                <select name="grupa" class="search_select">
                                    @foreach($grupe_dobavljaca as $row)
                                    <option value="{{ AdminImport::mapped_groups($row->grupa,$row->podgrupa) }}" {{ (Input::old('grupa') ? Input::old('grupa') : $partner_grupa_item->grupa) == AdminImport::mapped_groups($row->grupa,$row->podgrupa) ? 'selected' : '' }}>{{ AdminImport::mapped_groups($row->grupa,$row->podgrupa) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{ $errors->first('grupa') }}
                        </div>
                        <div class="columns medium-12 field-group {{ $errors->first('grupa_pr_id') ? ' error' : '' }}">
                            <label>{{ AdminLanguage::transAdmin('Naša grupa') }}</label>
                            <select name="grupa_pr_id" class="search_select">
                            {{ AdminSupport::selectGroups(Input::old('grupa_pr_id') ? Input::old('grupa_pr_id') : $partner_grupa_item->grupa_pr_id) }}
                            </select>
                            {{ $errors->first('grupa_pr_id') }}
                        </div>
                        <div class="columns medium-3 field-group {{ $errors->first('web_marza') ? ' error' : '' }}">
                            <label>{{ AdminLanguage::transAdmin('Web marža') }} </label>
                            <input type="text" name="web_marza" value="{{(Input::old('web_marza') ? Input::old('web_marza') : round($partner_grupa_item->web_marza,2))}}" {{ !is_null(Input::old('definisane_marze')) ? "disabled" : ($partner_grupa_item->definisane_marze == 1 ? "disabled" : "") }}>
                            {{ $errors->first('web_marza') }}
                        </div>
                        <div class="columns medium-3 field-group {{ $errors->first('mp_marza') ? ' error' : '' }}">
                            <label>{{ AdminLanguage::transAdmin('Mp marža') }}</label>
                            <input type="text" name="mp_marza" value="{{(Input::old('mp_marza') ? Input::old('mp_marza') : round($partner_grupa_item->mp_marza,2))}}" {{ !is_null(Input::old('definisane_marze')) ? "disabled" : ($partner_grupa_item->definisane_marze == 1 ? "disabled" : "") }}>
                            {{ $errors->first('mp_marza') }}
                        </div>
                        <div class="columns medium-6 field-group">
                            <label>{{ AdminLanguage::transAdmin('Koristi definisane marže') }}</label>
                            <input type="checkbox" class="Blocked" name="definisane_marze" {{ !is_null(Input::old('definisane_marze')) ? "checked" : ($partner_grupa_item->definisane_marze == 1 ? "checked" : "") }} onclick="myFunction(this)" />
                        </div>
                        <div class="columns medium-6 field-group">
                            <label>{{ AdminLanguage::transAdmin('Mapiraj sve artikle') }}</label>
                            <input type="checkbox" name="mapped_all" {{ !is_null(Input::old('mapped_all')) ? "checked" : "" }}>
                        </div>
                        <div class="columns medium-6 field-group">
                            <label>{{ AdminLanguage::transAdmin('Mapiraj artikle bez naše grupe') }}</label>
                            <input type="checkbox" name="mapped_news" {{ !is_null(Input::old('mapped_news')) ? "checked" : "" }}>
                        </div>
                    </div>
                    <div class="btn-container center">
                        <button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>

                    </div>
                </form>
            </div>
        </div>
      </div>
 </div>

