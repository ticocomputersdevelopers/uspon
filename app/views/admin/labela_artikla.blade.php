<div id="main-content" >
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('admin/partials/tabs')
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi labelu') }}</h3>
				<div class="manufacturer"> 
					<select class="JSeditSupport search_select">
						<option value="{{ AdminOptions::base_url() }}admin/labela/0">{{ AdminLanguage::transAdmin('Dodaj novu') }}</option>
						@foreach(AdminSupport::getLabela(true) as $row)
							<option value="{{ AdminOptions::base_url() }}admin/labela/{{ $row->labela_artikla_id }}" @if($row->labela_artikla_id == $labela_artikla_id) {{ 'selected' }} @endif>({{ $row->labela_artikla_id }}) {{ $row->naziv }} {{$row->rbr}}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>

		<section class="small-12 medium-12 large-5 large-centered columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1>
				<!-- <h1 id="info"></h1> -->

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/labela-edit" enctype="multipart/form-data">
					  	<input type="hidden" name="labela_artikla_id" value="{{ $labela_artikla_id }}">

					  	<div class="row">
							<div class="columns medium-12 field-group{{ $errors->first('naziv') ? ' error' : '' }}">
								<label for="naziv">{{ AdminLanguage::transAdmin('Naziv labele') }}</label>
								<input type="text" name="naziv" value="{{ Input::old('naziv') ? Input::old('naziv') : $naziv }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div>
						</div>

					    <div class="row">
							<div class="columns medium-6 field-group">
								<label>{{ AdminLanguage::transAdmin('Aktivna') }}</label>
								<select name="active" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
									@if(Input::old('active'))
										@if(Input::old('active'))
										<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
										@else
										<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
										@endif
									@else
										@if($active)
										<option value="1" selected>{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" >{{ AdminLanguage::transAdmin('NE') }}</option>
										@else
										<option value="1" >{{ AdminLanguage::transAdmin('DA') }}</option>
										<option value="0" selected>{{ AdminLanguage::transAdmin('NE') }}</option>
										@endif
									@endif
								</select>
							</div>
							<div class="columns medium-6 field-group{{ $errors->first('rbr') ? ' error' : '' }}">
								<label for="rbr">{{ AdminLanguage::transAdmin('Redni broj') }}</label>
								<input type="text" name="rbr" value="{{ Input::old('rbr') ? Input::old('rbr') : $rbr }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>
							</div> 
						</div>
						<div class="row">
							<div class="columns medium-4 manufacturers-img"> 
								<img <?php if(isset($slika_levo)){ ?> src="{{ AdminOptions::base_url().$slika_levo }}" <?php } ?>  />
								@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
									<div class="field-group">
										<label>{{ AdminLanguage::transAdmin('Dodeli stiker levo') }}</label>
										<div class="bg-image-file"> 
											<input type="file" name="slika_levo">
										</div> 
									</div>
								@endif
								@if(isset($slika_levo))
									<input type="checkbox" name="slika_levo_delete">{{ AdminLanguage::transAdmin('Obriši levi stiker') }}
								@endif
							</div> 
						
							
							<div class="columns medium-4 manufacturers-img medium-offset-4"> 
								<img <?php if(isset($slika_desno)){ ?> src="{{ AdminOptions::base_url().$slika_desno }}" <?php } ?>  />
								@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
									<div class="field-group">
										<label>{{ AdminLanguage::transAdmin('Dodeli stiker desno') }}</label>
										<div class="bg-image-file"> 
											<input type="file" name="slika_desno">
										</div> 
									</div>
								@endif
								@if(isset($slika_desno))
									<input type="checkbox" name="slika_desno_delete">{{ AdminLanguage::transAdmin('Obriši desni stiker') }}
								@endif
							</div> 
						</div>

						@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
							<div class="row"> 
								<div class="btn-container center">
									<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
									@if($labela_artikla_id != 0)
									<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/labela-delete/{{ $labela_artikla_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
				 					@endif
								</div>
							</div>
						@endif
					 
				</form>
			</div>
	</section>
</div> <!-- end of .flat-box -->
				
 