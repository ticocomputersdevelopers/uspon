<div id="main-content" class="kupci-page">
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
@if(Session::has('error_message'))
	<script>
		alertify.error('{{ Session::get('error_message') }}');
	</script>
@endif

	<div class="row m-subnav">
		<div class="large-2 medium-3 small-12 columns ">
			 <a class="m-subnav__link" href="{{AdminOptions::base_url()}}admin/garancije/null/null/null/produzene_garancije_id/desc" >
				<div class="m-subnav__link__icon">
					<i class="fa fa-arrow-left" aria-hidden="true"></i>
				</div>
				<div class="m-subnav__link__text">
					{{ AdminLanguage::transAdmin('Lista garancija') }}
				</div>
			</a>
		</div>
	</div>

	<div class="row">
		<div class="medium-6 medium-centered columns">
			<div class="flat-box">
			
				<form action="{{AdminOptions::base_url()}}admin/garancija/save" method="post" class="new-customer-form" autocomplete="false">

					<div class="row">

					<div class="column medium-6 {{ $errors->first('ime') ? ' error' : '' }}">
						<label for="">{{ AdminLanguage::transAdmin('Ime') }}</label>
						<input id="ime" name="ime" value="{{ Input::old('ime') ? Input::old('ime') : $garancija->ime }}" type="text" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-6 {{ $errors->first('prezime') ? ' error' : '' }}">
						<label for="">{{ AdminLanguage::transAdmin('Prezime') }}</label>
						<input id="prezime" name="prezime" value="{{ Input::old('prezime') ? Input::old('prezime') : $garancija->prezime }}" type="text" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>


					<div class="column medium-6 {{ $errors->first('adresa') ? ' error' : '' }}">
						<label for="">{{ AdminLanguage::transAdmin('Adresa') }}</label>
						<input id="adresa" name="adresa" value="{{ Input::old('adresa') ? Input::old('adresa') : $garancija->adresa }}" type="text" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-6 {{ $errors->first('grad') ? ' error' : '' }}">
				    	<label for=""> {{ AdminLanguage::transAdmin('Grad') }}</label>
						<input type="text" name="grad" value="{{ Input::old('grad') ? Input::old('grad') : $garancija->grad }}" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>
			 
					<div class="column medium-6 {{ $errors->first('email') ? ' error' : '' }}">
						<label>{{ AdminLanguage::transAdmin('Email') }}</label>
						<input id="email" autocomplete="off" name="email" value="{{ Input::old('email') ? Input::old('email') : $garancija->email }}" type="email" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-6 {{ $errors->first('telefon') ? ' error' : '' }}">
						<label for="">{{ AdminLanguage::transAdmin('TelefonProizvođač') }}</label>
						<input id="telefon" name="telefon" value="{{ Input::old('telefon') ? Input::old('telefon') : $garancija->telefon }}" type="text" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div> 

					<div class="column medium-6 {{ $errors->first('proizvodjac_id') ? ' error' : '' }}">
						<label>{{ AdminLanguage::transAdmin('Proizvođač') }}</label>
						<select name="proizvodjac_id" id="proizvodjac_id" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'disabled' : '' }}>
							@foreach($proizvodjaci as $proizvodjac)
								@if((!is_null(Input::old('proizvodjac_id')) ? Input::old('proizvodjac_id') : $garancija->proizvodjac_id) == $proizvodjac->proizvodjac_id)
									<option  value="{{ $proizvodjac->proizvodjac_id }}" selected>{{$proizvodjac->naziv}}</option>
								@else
									<option value="{{ $proizvodjac->proizvodjac_id }}">{{$proizvodjac->naziv}}</option>
								@endif
							@endforeach
						</select>
					</div>

					<div class="column medium-6 {{ $errors->first('roba_id') ? ' error' : '' }}">
						<label>{{ AdminLanguage::transAdmin('Model proizvoda') }}</label>
	<!-- 					<input name="model" value="{{ Input::old('model') ? Input::old('model') : (!is_null($garancija->roba_id) ? AdminArticles::find($garancija->roba_id,'naziv_web') : '') }}" autocomplete="off" data-timer="" type="text" id="articles" placeholder="Unesite ime artikla ili prozvođača ili krajnju grupu" />
						<div id="search_content" class="custom-select relative"></div> -->
						<select name="roba_id" id="roba_id" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'disabled' : '' }}>
							@foreach(AdminGarancije::getProizvodjacArticles(!is_null(Input::old('proizvodjac_id')) ? Input::old('proizvodjac_id') : $garancija->proizvodjac_id) as $article)
								@if((!is_null(Input::old('roba_id')) ? Input::old('roba_id') : $garancija->roba_id) == $article->roba_id)
									<option  value="{{ $article->roba_id }}" selected>{{$article->naziv_web}}</option>
								@else
									<option value="{{ $article->roba_id }}">{{$article->naziv_web}}</option>
								@endif
							@endforeach
						</select>
					</div>

					<div class="column medium-6 {{ $errors->first('maloprodaja') ? ' error' : '' }}">
						<label >{{ AdminLanguage::transAdmin('Maloprodaja') }}</label>
						<input id="maloprodaja" name="maloprodaja" value="{{ Input::old('maloprodaja') ? Input::old('maloprodaja') : $garancija->maloprodaja }}" type="text" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-6 {{ $errors->first('prodavnica_mesto') ? ' error' : '' }}">
						<label >{{ AdminLanguage::transAdmin('Mesto maloprodaje') }}</label>
						<input id="prodavnica_mesto" name="prodavnica_mesto" value="{{ Input::old('prodavnica_mesto') ? Input::old('prodavnica_mesto') : $garancija->prodavnica_mesto }}" type="text" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-6 {{ $errors->first('fiskalni') ? ' error' : '' }}">
						<label >{{ AdminLanguage::transAdmin('Fiskalni Račun') }}</label>
						<input id="fiskalni" name="fiskalni" value="{{ Input::old('fiskalni') ? Input::old('fiskalni') : $garancija->fiskalni }}" type="text" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-6 {{ $errors->first('datum_izdavanja') ? ' error' : '' }}">
						<label>{{ AdminLanguage::transAdmin('Datum izdavanja fiskalnog računa') }}</label>
						<input name="datum_izdavanja" type="text" value="{{ Input::old('datum_izdavanja') ? Input::old('datum_izdavanja') : $garancija->datum_izdavanja }}" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<div class="column medium-6 {{ $errors->first('serijski_broj') ? ' error' : '' }}">
						<label>{{ AdminLanguage::transAdmin('Serijski broj') }}</label>
						<input id="mobilni" name="serijski_broj" value="{{ Input::old('serijski_broj') ? Input::old('serijski_broj') : $garancija->serijski_broj }}" type="text" {{ Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')) == false ? 'readonly' : '' }}>
					</div>

					<input type="hidden" name="parent_id" value="{{ $garancija->produzene_garancije_id }}">
				</div> <!-- end of .row -->
			  
			  		@if(Admin_model::check_admin(array('GARANCIJE_AZURIRANJE')))
						<div class="row"> 
							 <div class="btn-container center">
								<button type="submit" id="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
								<a href="{{AdminOptions::base_url()}}admin/garancija/{{$garancija->produzene_garancije_id}}" class="btn btn-secondary">{{ AdminLanguage::transAdmin('Otkaži') }}</a>
								<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/garancija-delete/{{ $garancija->produzene_garancije_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
							</div>
						</div>
					@endif
				</form> 
			</div>
		</div>
	</div>

	<div class="row"> 
		<div class="warr-cont">
		<table>
			<thead>
				<tr>
					<th title="Redni broj">RB</th>
					<th>Ime i prezime</th>
					<th>Adresa</th>
					<th>E-mail</th>
					<th>Telefon</th>
					<th>Proizvođač</th>
					<th>Grupa</th>
					<th>Proizvod</th>
					<th>Serijski broj</th>
					<th title="Naziv maloprodaje">Naziv mp.</th>
					<th title="Mesto maloprodaje">Mesto mp.</th>
					<th>Fisk. račun</th>
					<th>Izmena</th>
					<th>PDF</th>
				</tr>
			</thead>
			<tbody> 
			@foreach($branch as $row)  
				<tr>
					<td>{{$row->produzene_garancije_id}}</td> 
					<td>{{$row->ime}} {{$row->prezime}}</td> 
					<td>{{$row->adresa}}, {{$row->grad}}</td> 
					<td>{{$row->email}}</td> 
					<td>{{$row->telefon}}</td> 
					<td>{{$row->proizvodjac}}</td> 
					<td>{{$row->grupa}}</td> 
					<td>{{$row->naziv_web}}</td> 
					<td>{{$row->serijski_broj}}</td> 
					<td>{{$row->maloprodaja}}</td> 
					<td>{{$row->prodavnica_mesto}}</td> 
					<td>{{$row->fiskalni}}</td> 
					<td>{{ $row->datum_kreiranja }}</td> 
					<td>{{ $row->datum_kreiranja }}</td> 
					<td>
						<a href="{{AdminOptions::base_url()}}admin/produzena-garancija-pdf/{{$row->produzene_garancije_id}}" target="_blank">PDF</a>
					</td>
				</tr> 
			@endforeach
			</tbody>
		</table>
	</div>
		{{ $branch->links() }}
	</div> 
</div>