<div id="main-content" >
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	@include('admin/partials/tabs')

	<div class="row">
		<div class="medium-3 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi status') }}</h3>
				<div class="manufacturer"> 
					<select class="JSeditSupport search_select">
						<option value="{{ AdminOptions::base_url() }}admin/status_narudzbine">{{ AdminLanguage::transAdmin('Dodaj novi') }}</option>
						@foreach(AdminSupport::getFlagStatus(true) as $row)
						<option value="{{ AdminOptions::base_url() }}admin/status_narudzbine/{{ $row->narudzbina_status_id }}" @if($row->narudzbina_status_id == $narudzbina_status_id) {{ 'selected' }} @endif>{{ $row->naziv }} </option>
						@endforeach -->
					</select>
				</div>
			</div>
		</div>

		<section class="medium-5 columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }} {{ $narudzbina_status_id == 1 ? AdminLanguage::transAdmin('Podrazumevani') : '' }}</h1> 

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/status_narudzbine_edit" enctype="multipart/form-data">
					<input type="hidden" name="narudzbina_status_id" value="{{ $narudzbina_status_id }}"> 
					
					<div class="row">
						<div class="columns medium-6 {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">{{ AdminLanguage::transAdmin('Naziv statusa') }}</label>
							<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div>
						<div class="columns medium-6">
							<label>{{ AdminLanguage::transAdmin('Aktivno') }}</label>
							<select name="selected" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								{{ AdminSupport::selectCheck(Input::old('selected') ? Input::old('selected') : $selected) }}
							</select>
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))  
					<div class="btn-container center">
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						@if($narudzbina_status_id != 0)	
						<button class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/status_narudzbine_delete/{{ $narudzbina_status_id }}">{{ AdminLanguage::transAdmin('Obriši') }}</button>
						@endif
					</div> 
					@endif

				</form> 
			</div>
		</section>
	</div>  
</div>
