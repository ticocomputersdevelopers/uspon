<header id="admin-header" class="text-center">

	<!-- WIDTH TOGGLE BUTTON -->
	<div class="header-width-toggle"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>

	
	<div class="logo-wrapper">
		<a class="logo" href="{{ AdminOptions::base_url()}}admin" title="Selltico">
			<img src="{{ AdminOptions::base_url()}}/images/admin/logo-selltico-white.png" alt="Selltico">
		</a>
	</div>

	@if(Admin_model::check_admin()) 
	<p class="logged-user text-white">{{ AdminOptions::versions() }}</p> 
	@endif	 
	
	<nav class="main-menu">  
		<a href="{{ AdminOptions::base_url()}}" target="_blank" class="menu-item__link">
			<div class="menu-item__icon">
				<img class="shop-logo" src="{{ AdminOptions::base_url()}}images/admin/wbp-logo.png" alt="{{Options::company_name()}}" />
				<span class="menu-tooltip">{{ AdminOptions::is_shop() ? AdminLanguage::transAdmin('VIDI SHOP') : AdminLanguage::transAdmin('VIDI SAJT') }}</span>
			</div>

			<div class="menu-item__text">{{ AdminOptions::is_shop() ? AdminLanguage::transAdmin('VIDI SHOP') : AdminLanguage::transAdmin('VIDI SAJT') }}</div>
		</a> 

		<ul>
			<li class="logged-user text-white"> 
				{{ AdminLanguage::transAdmin('Ulogovan') }}: {{ Admin_model::get_admin() }} 
			</li>

			@if(AdminOptions::gnrl_options(3013))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/podesavanje-naloga" class="menu-item__link">
					<div class="menu-item__icon">
						<i class="fa fa-cogs" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Podešavanje naloga') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Podešavanje naloga') }}</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('NARUDZBINE')) && Admin_model::check_admin(array('NARUDZBINE_PREGLED')) && AdminOptions::is_shop())
			<li>
				<a href="{{ AdminOptions::base_url()}}admin" class="menu-item__link  @if(in_array($strana,array('pocetna', 'narudzbina', 'narudzbina_stavka'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-ticket" aria-hidden="true"></i>
						@if( AdminAnalitika::getNew() > 0) <span class="notify-num-small">{{ AdminAnalitika::getNew() }}</span> @endif
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Narudžbine') }}</span> 
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Narudžbine') }}
						@if( AdminAnalitika::getNew() > 0) <span class="notify-num">{{ AdminAnalitika::getNew() }}</span> @endif
					</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')) && AdminOptions::is_shop())
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/artikli/0/0/0/0/0/0/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-nn-1-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn/0/0/nn-nn" class="menu-item__link  @if(in_array($strana,array('artikli', 'product_short', 'product', 'slike', 'opis', 'karakteristike', 'generisane', 'dobavljac_karakteristike', 'product_osobine', 'vezani_artikli', 'srodni_artikli', 'product_akcija', 'product_akcija_b2b', 'dodatni_fajlovi', 'seo' ))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cubes" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Artikli') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Artikli') }}</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('KUPCI_I_PARTNERI')) && Admin_model::check_admin(array('KUPCI_I_PARTNERI_PREGLED')) && AdminOptions::is_shop())
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/kupci_partneri/kupci" class="menu-item__link @if(in_array($strana,array('kupci','partneri', 'kupac'))) active @endif">
					@if(AdminOptions::web_options(130) == 0)
					<div class="menu-item__icon">
						<i class="fa fa-users" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Kupci i partneri') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Kupci i partneri') }}</div>
					@else
					<div class="menu-item__icon">
						<i class="fa fa-users" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Kupci') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Kupci') }}</div>
					@endif
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('ANALITIKA')) && AdminOptions::is_shop())
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/analitika" class="menu-item__link  @if($strana == 'analitika') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-line-chart" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Analitika') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Analitika') }}</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('STRANICE')) && Admin_model::check_admin(array('STRANICE_PREGLED')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/stranice/0" class="menu-item__link  @if(in_array($strana,array('stranice', 'sekcija_stranice'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-file-text" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Stranice') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Stranice') }}</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('FUTER')) && Admin_model::check_admin(array('FUTER_PREGLED')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/futer/0" class="menu-item__link  @if($strana == 'footer') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-window-maximize" style="transform: rotate(180deg);"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Futer') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Futer') }}</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('BANERI_I_SLAJDER')) && Admin_model::check_admin(array('BANERI_I_SLAJDER_PREGLED')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/slider/0" class="menu-item__link  @if($strana == 'slajder') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-picture-o" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Baneri i slajderi') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Baneri i slajderi') }}</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('VESTI')) && Admin_model::check_admin(array('VESTI_PREGLED')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/vesti" class="menu-item__link  @if(in_array($strana,array('vesti', 'vest'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-newspaper-o" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Vesti') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Vesti') }}</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('KOMENTARI')) && Admin_model::check_admin(array('KOMENTARI_PREGLED')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/komentari" class="menu-item__link  @if(in_array($strana,array('komentari', 'komentar'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-comment" aria-hidden="true"></i>
						@if( AdminSupport::countNewComments() > 0) <span class="notify-num-small">{{ AdminSupport::countNewComments() }}</span> @endif
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Komentari') }}</span> 
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Komentari') }}
						@if( AdminSupport::countNewComments() > 0) <span class="notify-num">{{ AdminSupport::countNewComments() }}</span> @endif
					</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('KONTAKT_PODACI')) && Admin_model::check_admin(array('KONTAKT_PODACI_PREGLED')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/kontakt-podaci" class="menu-item__link  @if($strana == 'kontakt') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Kontakt podaci') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Kontakt podaci') }}</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('WEB_IMPORT')) && Admin_model::check_admin(array('WEB_IMPORT_PREGLED')) && AdminOptions::is_shop())
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/web_import/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn/0/nn-nn" class="menu-item__link  @if($strana == 'web_import') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cloud-upload" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Web import') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Web import') }}</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('KORISNICI')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/administratori" class="menu-item__link @if($strana == 'administratori' || $strana == 'grupa_modula' || $strana == 'logovi') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-user-plus" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Korisnici') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Korisnici') }}</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')) && AdminOptions::is_shop())
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/grupe/0" class="menu-item__link  @if(in_array($strana,array('grupe','proizvodjac','mesto','tip_artikla','jedinica_mere','poreske_stope','stanje_artikla','status_narudzbine','kurirska_sluzba','konfigurator','osobine','nacin_placanja','nacin_isporuke','troskovi_isporuke','kurs', 'bodovi', 'definisane_marze', 'vauceri', 'kupovina_na_rate', 'katalog', 'labela_artikla', 'ankete' ))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-industry" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Šifarnici') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Šifarnici') }}</div>
				</a>
			</li>
			@endif

			@if(AdminOptions::is_shop() && AdminOptions::gnrl_options(3039) && Admin_model::check_admin(array('GARANCIJE')) && Admin_model::check_admin(array('GARANCIJE_PREGLED')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/garancije/null/null/null/produzene_garancije_id/desc" class="menu-item__link  @if(in_array($strana,array('garancije','garancija'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-check-square-o" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Garancije') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Garancije') }}</div>
				</a>
			</li>
			@endif

			@if(AdminOptions::gnrl_options(3040) && Admin_model::check_admin(array('RMA')) && Admin_model::check_admin(array('RMA_PREGLED')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/rma" class="menu-item__link">
					<div class="menu-item__icon">
						<i class="fa fa-wrench" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('RMA') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('RMA') }}</div>
				</a>
			</li>
			@endif
			
			@if(AdminOptions::checkB2B())
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/b2b" class="menu-item__link">
					<div class="menu-item__icon">
						B2B
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('B2B Admin') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('B2B Admin') }}</div>
				</a>
			</li>
			@endif


			@if(Admin_model::check_admin(array('CRM')) && AdminOptions::gnrl_options(3063))
			<li class="menu-item">
				<a href="{{ AdminOptions::base_url()}}crm/crm_home?aktivni=0" class="menu-item__link @if($strana == 'crm_home'|| $strana == 'crm_sifrarnik'|| $strana == 'crm_akcija'|| $strana == 'crm_status' || $strana == 'crm_tip' || $strana == 'crm_kontakt_vrsta' || $strana == 'crm_partneri') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-vcard-o" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('CRM') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('CRM') }}  <span class="notify-num">{{AdminCrm::getAkcijeDanas(date('Y-m-d'))}}</span></div>
				</a>
			</li>
			@endif
			<li>
			@if(Admin_model::check_admin(array('Custom CSS')) && AdminOptions::gnrl_options(3064))
				<a href="{{ AdminOptions::base_url()}}admin/css" class="menu-item__link  @if($strana == 'css') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-paint-brush" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('CSS') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('CSS') }}</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('PODESAVANJA')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/podesavanja" class="menu-item__link  @if($strana == 'podesavanja') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cogs" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Podešavanja') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Podešavanja') }}</div>
				</a>
			</li>
			@endif
		</ul>
	</nav>

	<div class="logout-wrapper">
		<a href="{{ AdminOptions::base_url()}}admin-logout" class="menu-item__link">
			<div class="menu-item__icon">
				<i class="fa fa-sign-out" aria-hidden="true"></i>
				<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Odjavi se') }}</span>
			</div>
			<div class="menu-item__text">{{ AdminLanguage::transAdmin('Odjavi se') }}</div>
		</a>
		<p class="footnote">TiCo &copy; {{ date('Y') }} - {{ AdminLanguage::transAdmin('All rights reserved') }}</p>
	</div>
</header>

