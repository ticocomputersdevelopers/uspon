<ul class="custom-menu articles-mn">
	<li>
		<a class="custom-menu-item" id="new-art" href="{{AdminOptions::base_url()}}admin/product/0" target="_blank">{{ AdminLanguage::transAdmin('Dodaj novi artikal (F2)') }}</a>

	</li>
	<li>
		<button class="custom-menu-item" id="JSIzmeni">{{ AdminLanguage::transAdmin('Izmeni artikal (F4)') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSUrediOpis">{{ AdminLanguage::transAdmin('Uredi opis (F7)') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSKlonirajArtikal">{{ AdminLanguage::transAdmin('Kloniraj artikal') }}</button>
	</li>
	
	<!-- <li>
		<button class="custom-menu-item custom-menu-sub" id="">Postavi karakteristike <i class="fa fa-caret-right" aria-hidden="true"></i></button>
		<ul class="custom-menu-sub-item">
			<li>
				<button class="custom-menu-item JSexecute" data-execute='[{"table":"roba", "column":"web_flag_karakteristike", "val":"0"}]'>HTML кarakteristike</button>	
			</li>
			<li>
				<button class="custom-menu-item JSexecute" data-execute='[{"table":"roba", "column":"web_flag_karakteristike", "val":"1"}]'>Generisane</button>
			</li>
			<li>
				<button class="custom-menu-item JSexecute" data-execute='[{"table":"roba", "column":"web_flag_karakteristike", "val":"2"}]'>Od dobavljača</button>
			</li>	
		</ul>
	</li>	 -->
	<li>
		<button class="custom-menu-item" id="JSobradaKarakteristika">{{ AdminLanguage::transAdmin('Obrada karakteristika') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSObrisiArtikal">{{ AdminLanguage::transAdmin('Obriši artikal/le (F8)') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSRefresh">{{ AdminLanguage::transAdmin('Osveži (F5)') }}</button>
	</li>
<!-- 	<li>
		<button class="custom-menu-item" id="JSEditTeksta" value="">Edit teksta</button>
	</li> -->
	<li>
		<button class="custom-menu-item" id="JSchooseGroup">{{ AdminLanguage::transAdmin('Promeni grupu') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchooseBrand">{{ AdminLanguage::transAdmin('Promeni proizvođača') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchooseTip">{{ AdminLanguage::transAdmin('Promeni tip') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchooseLabela">{{ AdminLanguage::transAdmin('Dodeli labelu') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchoosePrice">{{ AdminLanguage::transAdmin('Promeni vrstu cene') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchooseQuantity">{{ AdminLanguage::transAdmin('Promeni količinu') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchooseTax">{{ AdminLanguage::transAdmin('Promeni poresku stopu') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSTezina">{{ AdminLanguage::transAdmin('Promeni težinu') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSTag">{{ AdminLanguage::transAdmin('Dodeli tagove') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSGallery">{{ AdminLanguage::transAdmin('Dodavanje slike') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchooseExport">{{ AdminLanguage::transAdmin('Export artikala') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSchooseKatalog">{{ AdminLanguage::transAdmin('Katalog') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSmassEdit">{{ AdminLanguage::transAdmin('Edit cena') }}</button>
	</li>
	<li>
		@if(AdminOptions::web_options(324)==1)
			<button class="custom-menu-item" id="JSEnergyClass">{{ AdminLanguage::transAdmin('Energetska klasa') }}</button>
		@endif
	</li>
	<li>
		<button class="custom-menu-item" id="JScharacteristics">{{ AdminLanguage::transAdmin('Dodela karakteristika') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JStextEdit">{{ AdminLanguage::transAdmin('Edit teksta') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSakcijaEdit">{{ AdminLanguage::transAdmin('Masovna dodela akcije') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSdefMarza">{{ AdminLanguage::transAdmin('Dodela definisanih marži') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSGarancije">{{ AdminLanguage::transAdmin('Dodela garancije') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSConnectSrodni">{{ AdminLanguage::transAdmin('Poveži kao srodne artikle') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSDisconnectSrodni">{{ AdminLanguage::transAdmin('Razveži medjusobno srodne') }}</button>
	</li>
	<li>
		<button class="custom-menu-item" id="JSDimenzije">{{ AdminLanguage::transAdmin('Dodela dimenzija') }}</button>
	</li>

	
</ul>