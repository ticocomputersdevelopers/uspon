<option selected disabled>{{ AdminLanguage::transAdmin('Izaberite tarifnu grupu') }}</option>
@foreach (AdminSupport::getPoreskeGrupe() as $porez)
<option value="{{ $porez->tarifna_grupa_id }}">{{ $porez->naziv }}</option>
@endforeach