
<div class="row text-right" style="padding-top: 30px;"> 

	<div class="col-12"> 
   
	    <div class="row">
	    	<div class="col-7 text-right">
		    	<p class="summary text-right"><strong class="sum-span">{{ AdminLanguage::transAdmin('Osnovica') }}:</strong></p>
	    	</div>

	    	<div class="col-5 text-right">
			 	<strong style="padding-right: 30px;">
			 	@if($web_b2c_narudzbina->web_nacin_isporuke_id != 2  && $troskovi_isporuke > 0)
			 		{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id) + $troskovi_isporuke/1.2)}}
			 	@else
			 		{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}
			 	@endif
			 	</strong>
	    	</div>
	    </div>

	    <div class="row">		                
	    	<div class="col-7 text-right">
		    	<p class="summary text-right"><strong class="sum-span">{{ AdminLanguage::transAdmin('PDV') }}:</strong></p>
	    	</div>

	    	<div class="col-5 text-right">
			 	<strong style="padding-right: 30px;">20%</strong>
	    	</div>
	    </div>
		
	    <div class="row">
	    	<div class="col-7 text-right">
		   		<p class="summary text-right"><strong class="sum-span">{{ AdminLanguage::transAdmin('Ukupno sa PDV-om') }}:</strong></p>
	    	</div>

	    	<div class="col-5 text-right">
			   <strong style="padding-right: 30px;">
			   		@if($web_b2c_narudzbina->web_nacin_isporuke_id != 2  && $troskovi_isporuke > 0)
			   			{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) + $troskovi_isporuke)}}
			   		@else
			   			{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id))}}
			   		@endif
			   </strong>
	    	</div>
	    </div>

	</div>

</div>