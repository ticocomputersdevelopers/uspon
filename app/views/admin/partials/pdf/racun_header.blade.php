	<div class="row"> 
		<div class="logo col-3">
			<img src="{{ AdminOptions::base_url()}}{{Options::company_logo()}}" alt="logo">
		</div>
		<div class="col-4 company-info">
			<p class="comp-name">{{AdminOptions::company_name()}}</p>
			<p>{{AdminOptions::company_adress()}}, {{AdminOptions::company_mesto()}}</p>
			<p>{{ AdminLanguage::transAdmin('Telefon') }} {{AdminOptions::company_phone()}}, {{ AdminLanguage::transAdmin('Fax') }}: {{AdminOptions::company_fax()}}</p>
			<p>{{ AdminLanguage::transAdmin('PIB') }}: {{AdminOptions::company_pib()}}</p>
		    <p>{{ AdminLanguage::transAdmin('E-mail') }}: {{AdminOptions::company_email()}}</p>
		</div>
		
		<div class="col-4 kupac-info">
			{{AdminSupport::narudzbina_kupac_pdf($web_b2c_narudzbina_id)}}
		</div>
 	</div>		

 	<div class="row"> 
		<p class="ziro">{{ AdminLanguage::transAdmin('Žiro racun') }}: {{AdminOptions::company_ziro()}}</p>
	 </div>

 	<div class="row">
		<h4 class="racun-br">{{ AdminLanguage::transAdmin('Račun broj') }}: {{AdminNarudzbine::find($web_b2c_narudzbina_id,'broj_dokumenta')}}</h4>
	 </div>

	<div class="row"> 
		<table class="info-1">
			<thead>
				<tr>
					<td>{{ AdminLanguage::transAdmin('Mesto i datum izdavanja računa') }}</td>
					<td>{{ AdminLanguage::transAdmin('Mesto i datum prometa dobara i usluga') }}</td>
					<td>{{ AdminLanguage::transAdmin('Valuta plaćanja') }}</td>
					<td>{{ AdminLanguage::transAdmin('Način isporuke') }}</td>
					<td>{{ AdminLanguage::transAdmin('Način plaćanja') }}</td>
				</tr>
			</thead>

			<tbody>
				<tr>						
					<td>{{AdminOptions::company_mesto()}},{{AdminNarudzbine::formatDate(AdminNarudzbine::find($web_b2c_narudzbina_id,'datum_dokumenta'))}}</td>
					<td>{{AdminOptions::company_mesto()}},{{AdminNarudzbine::formatDate(AdminNarudzbine::find($web_b2c_narudzbina_id,'datum_dokumenta'))}}</td>
					<td>{{AdminNarudzbine::formatDate(AdminNarudzbine::find($web_b2c_narudzbina_id,'datum_dokumenta'))}}</td>
					<td>{{AdminCommon::n_i($web_b2c_narudzbina_id)}}</td>
					<td>{{AdminCommon::n_p($web_b2c_narudzbina_id)}}</td>
				</tr>
			</tbody>
		</table>
	</div>