<div class="row"> 
        	<br>
			<h4>Rekapitulacija</h4>
		</div>

		<div class="row"> 
			<div class="col-7"> 
				<table class="rekapitulacija">
					<thead>
						<tr>
							<td></td>
							<td>{{ AdminLanguage::transAdmin('Osnovica') }}</td>
							<td>{{ AdminLanguage::transAdmin('PDV') }}</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td>{{ AdminLanguage::transAdmin('Oslobođeno poreza') }}</td>
							<td>0,00</td>
							<td>0,00</td>
						</tr>
						<tr>
							<td>{{ AdminLanguage::transAdmin('Po posebnoj stopi') }}</td>
							<td>0,00</td>
							<td>0,00</td>
						</tr>
						<tr>
							<td>{{ AdminLanguage::transAdmin('Po opštoj stopi') }}</td>
							@if(AdminOptions::web_options(311)==1)
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							@else
							<td> {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
							<td>0,00</td>
							@endif
						</tr>
						<tr>
							<td><strong>{{ AdminLanguage::transAdmin('Ukupno') }}</strong></td>
							@if(AdminOptions::web_options(311)==1)
							<td>{{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							<td>{{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
							@else
							<td> {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
							<td>0,00</td>
							@endif
						</tr>
					</tbody>
				</table>
		 	</div>

		 	<div class="col-4"> 
		        <table class="summary">
		        	<tr class="text-right">
		        		 @if(AdminOptions::web_options(311)==1)
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Iznos osnovice') }}:</span>
		                 {{AdminCommon::cena(AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
		                 @else
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Iznos osnovice') }}:</span>
		                 {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id))}}</td>
		                 @endif
		            </tr>
		            <tr class="text-right">
		                @if(AdminOptions::web_options(311)==1)
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Iznos PDV-a') }}:</span>
		                 {{AdminCommon::cena(AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id) - AdminCommon::racun_ukupno_bez_pdv($web_b2c_narudzbina_id))}}</td>
		                @else
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Iznos PDV-a') }}:</span>0,00</td>
		                @endif
		            </tr>
					<?php 
						$besplatna_dostava = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->pluck('besplatna_dostava'); 
						$nacin_isporuke = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->pluck('web_nacin_isporuke_id'); 
						$troskovi = 0;
					?>
					<?php $vaucer_popust = Order::vaucer_popust($web_b2c_narudzbina_id); ?>
                  	@if($vaucer_popust > 0)
					<tr  class="text-right">
						<td class="summary text-right">
							<span class="sum-span">{{ AdminLanguage::transAdmin('Vaucer popust') }}:</span>
							{{ number_format($vaucer_popust, 2, ',', ' ') }}
						</td> 
					</tr>
					@endif
					@if(($besplatna_dostava == 0) || ($nacin_isporuke != 2))
			            @if(($cena_dostave = AdminCommon::cena_dostave($web_b2c_narudzbina_id)) > 0)
			            <tr class="text-right">	              
	        	
			                <td class="summary text-right"><span class="sum-span">Troškovi isporuke:</span>
			                	{{$troskovi = AdminCommon::cena($cena_dostave)}}    
			             	</td>
			            </tr>
			            @endif
		            @endif
		            <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Ukupno') }}:</span> 
		                {{AdminCommon::cena((AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id)+$troskovi-$vaucer_popust))}}</td>
		            </tr>
		        </table>
	        </div>
		</div>