<div class="row" > 
	<div class="col-6 no-padding">
		<p>{{AdminOptions::company_name()}}</p>
		<p>{{AdminOptions::company_adress()}}, {{AdminOptions::company_mesto()}}</p>
		<p>{{AdminOptions::company_phone()}}</p>
	</div>

	<div class="col-6 no-padding">
		<div><span>{{ Language::trans_chars('Matični broj') }}:</span> <span>{{AdminOptions::company_maticni()}}</span></div>
		<div><span>{{ Language::trans_chars('Žiro račun u Raiffeisen banci') }}:</span> <span>265-3030310001570-45</span></div>
		<div><span>{{ Language::trans_chars('PIB') }}:</span> <span>{{AdminOptions::company_pib()}}</span></div>
	</div>
</div>
<?php $id=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id'); 
	  $web_kupac =DB::table('web_kupac')->where('web_kupac_id',$id)->first();
?>
<div class="row" style="padding-bottom: 45px;">

	<div class="col-12 no-padding">
		<div><span>{{ Language::trans_chars('Za')}}:</span> <span>{{ $web_kupac->ime .' '. $web_kupac->prezime }}</span></div>

		<div><span>{{ Language::trans_chars('Adresa') }}:</span> <span>{{ $web_kupac->adresa }}</span></div>

		<div><span>{{ Language::trans_chars('Telefon') }}:</span> <span>{{ $web_kupac->telefon }}</span></div>
	</div>
</div>

<div class="row" style="padding-left: 45px;">
	<div class="col-4 no-padding">
		<p>{{ Language::trans_chars('Datum: ') }} {{ date('m.d.Y.',strtotime(AdminNarudzbine::find($web_b2c_narudzbina_id,'datum_dokumenta'))) }}</p>
	</div>

	<div class="col-4 no-padding">
		<p>{{ Language::trans_chars('Broj profakture: ')}} {{AdminNarudzbine::find($web_b2c_narudzbina_id,'broj_dokumenta')}}</p>
	</div>

	<div class="col-4 no-padding">
		<p>{{ Language::trans_chars('Rok važenja: 3 dana') }}</p>
	</div>
</div>