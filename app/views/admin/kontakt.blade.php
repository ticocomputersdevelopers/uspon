<section id="main-content">

	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif	

	<div class="row"> 
		<section class="medium-3 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Logo') }}</h3>
				
				<div class="logo-upload-area">
					
					<form method="post" name="logo_upload" enctype="multipart/form-data" action="upload_logo" class="text-right JSLogoForm">

						@if(Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')))
						<div class="row"> 
							<div class="column medium-12 clearfix"> 
								<label>{{ AdminLanguage::transAdmin('Dodajte logo') }}</label>
								<a href="#" class="file-upload btn btn-secondary btn-small JSLogoUploadButton">   
									<input type="file" name="logo" class="file-input JSLogoUpload">
									{{ AdminLanguage::transAdmin('Dodaj sliku') }}
								</a>
							</div>
						</div>
						@endif
					</form>
					<!-- mladja dodao -->
					<section class="logo-preview-area">
						<img class="logo-preview" src="{{Admin_model::logo()}}" alt="" />
					</section>				
				</div>
				
				<div class="logo-upload-area">		 
					<form method="post" name="logo_upload" enctype="multipart/form-data" action="upload_potpis" class="text-right JSPotpisForm">
						@if(Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')))
						<div class="row"> 
							<div class="column medium-12 clearfix"> 
								<label>{{ AdminLanguage::transAdmin('Dodajte potpis') }}</label>
								<a href="#" class="file-upload btn btn-secondary btn-small JSPotpisUploadButton">   
									<input type="file" name="potpis" class="file-input JSPotpisUpload">
									{{ AdminLanguage::transAdmin('Dodaj potpis') }}
								</a>
							</div>
						</div>
						@endif
					</form>
					<section class="text-center">
						<img class="sig-preview" src="{{AdminSupport::potpis()}}" alt="" />
					</section>
				</div>
			</div>  
		</section>

		<!-- CONTACT DETAILS EDIT -->

		<section class="medium-9 columns">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Kontakt podaci') }}</h3>
				<form method="POST" action="{{ AdminOptions::base_url() }}admin/kontakt_update">
					<div class="row">
						<input name="preduzece_id" value="{{ $preduzece->preduzece_id }}" type="hidden">

						<div class="medium-4 columns {{ $errors->first('naziv') ? 'error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Naziv firme') }}</label>
							<input class="company-name"  name="naziv" value="{{ htmlentities(!is_null(Input::old('naziv')) ? Input::old('naziv') : $preduzece->naziv) }}" type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="medium-4 columns {{ $errors->first('email') ? 'error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('E-mail') }}</label>
							<input class="e-mail"  name="email" value="{{ !is_null(Input::old('email')) ? Input::old('email') : $preduzece->email }}" type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						@if(AdminNarudzbine::api_posta(3))
						<div class="medium-4 columns"> 
							<label>{{ AdminLanguage::transAdmin('Opština') }}</label>				 
							<select class="form-control" name="opstina" id="JSOpstina" tabindex="6" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@foreach(AdminNarudzbine::opstine() as $key => $opstina)
								@if((Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($preduzece->ulica_id)) == $opstina->narudzbina_opstina_id)
								<option value="{{ $opstina->narudzbina_opstina_id }}" selected>{{ $opstina->naziv }}</option>
								@else
								<option value="{{ $opstina->narudzbina_opstina_id }}">{{ $opstina->naziv }}</option>
								@endif
								@endforeach
							</select>
						</div>

						<div class="medium-4 columns"> 
							<label>{{ AdminLanguage::transAdmin('Naselje') }}</label>
							<select class="form-control" name="mesto" id="JSMesto" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@foreach(AdminNarudzbine::mesta(Input::old('opstina') ? Input::old('opstina') : AdminNarudzbine::opstina_id($preduzece->ulica_id)) as $key => $mesto)
								@if((Input::old('mesto') ? Input::old('mesto') : AdminNarudzbine::mesto_id($preduzece->ulica_id)) == $mesto->narudzbina_mesto_id)
								<option value="{{ $mesto->narudzbina_mesto_id }}" selected>{{ $mesto->naziv }}</option>
								@else
								<option value="{{ $mesto->narudzbina_mesto_id }}">{{ $mesto->naziv }}</option>
								@endif
								@endforeach
							</select>
						</div>

						<div class="medium-4 columns"> 
							<label>{{ AdminLanguage::transAdmin('Ulica') }}</label>				 
							<select class="form-control" name="ulica_id" id="JSUlica" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'disabled' : '' }}>
								@foreach(AdminNarudzbine::ulice(Input::old('mesto') ? Input::old('mesto') : AdminNarudzbine::mesto_id($preduzece->ulica_id)) as $key => $ulica)
								@if((Input::old('ulica_id') ? Input::old('ulica_id') : $preduzece->ulica_id) == $ulica->narudzbina_ulica_id)
								<option value="{{ $ulica->narudzbina_ulica_id }}" selected>{{ $ulica->naziv }}</option>
								@else
								<option value="{{ $ulica->narudzbina_ulica_id }}">{{ $ulica->naziv }}</option>
								@endif
								@endforeach
							</select>
						</div>

						<div class="medium-4 columns {{ $errors->first('broj') ? 'error' : '' }}"> 
							<label>{{ AdminLanguage::transAdmin('Broj') }}</label>
							<input id="without-reg-number" class="form-control" name="broj" type="text" value="{{ htmlentities(Input::old('broj') ? Input::old('broj') : $preduzece->broj) }}" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						@else

						<div class="medium-4 columns {{ $errors->first('mesto') ? 'error' : '' }}">				
							<label>{{ AdminLanguage::transAdmin('Grad') }}</label>
							<input class="phone-number"  name="mesto" value="{{ !is_null(Input::old('mesto')) ? Input::old('mesto') : $preduzece->mesto }}" type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>			
						</div>
						
						<div class="medium-4 columns {{ $errors->first('adresa') ? 'error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Adresa') }}</label>
							<input class="address"  name="adresa" value="{{ htmlentities(!is_null(Input::old('adresa')) ? Input::old('adresa') : $preduzece->adresa) }}" type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						@endif

						<div class="medium-4 columns {{ $errors->first('kontakt_osoba') ? 'error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Kontakt Osoba') }}</label>
							<input class="phone-number"  name="kontakt_osoba" value="{{ !is_null(Input::old('kontakt_osoba')) ? Input::old('kontakt_osoba') : $preduzece->kontakt_osoba }}" type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="medium-4 columns {{ $errors->first('telefon') ? 'error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Broj telefona') }}</label>
							<input class="phone-number"  name="telefon" value="{{ !is_null(Input::old('telefon')) ? Input::old('telefon') : $preduzece->telefon }}" type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						
						<div class="medium-4 columns">		
							<label>{{ AdminLanguage::transAdmin('Fax') }}</label>
							<input class="fax" name="fax" value="{{ !is_null(Input::old('fax')) ? Input::old('fax') : $preduzece->fax }}"  type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						
						<div class="medium-4 columns {{ $errors->first('matbr_registra') ? 'error' : '' }}">					
							<label>{{ AdminLanguage::transAdmin('Matični broj') }}</label>
							<input class="matbr_registra" name="matbr_registra" value="{{ !is_null(Input::old('matbr_registra')) ? Input::old('matbr_registra') : $preduzece->matbr_registra }}"  type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>				
						</div>

						<div class="medium-4 columns {{ $errors->first('pib') ? 'error' : '' }}">		
							<label>{{ AdminLanguage::transAdmin('Pib') }}</label>
							<input class="pib" name="pib" value="{{ !is_null(Input::old('pib')) ? Input::old('pib') : $preduzece->pib }}"  type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						
						<div class="medium-4 columns {{ $errors->first('ziro') ? 'error' : '' }}">		
							<label>{{ AdminLanguage::transAdmin('Žiro račun') }}</label>
							<input class="bank-account" name="ziro" value="{{ !is_null(Input::old('ziro')) ? Input::old('ziro') : $preduzece->ziro }}"  type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>	
						</div>

						<div class="medium-4 columns {{ $errors->first('delatnost_sifra') ? 'error' : '' }}">	
							<label>{{ AdminLanguage::transAdmin('Šifra delatnosti') }}</label>
							<input class="del-number" name="delatnost_sifra" value="{{ !is_null(Input::old('delatnost_sifra')) ? Input::old('delatnost_sifra') : $preduzece->delatnost_sifra }}"  type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
					</div>

					<hr>

					<div class="row">
						<h3 class="title-med">{{ AdminLanguage::transAdmin('Lokacija') }}</h3> 
						<div class="medium-6 columns {{ $errors->first('latitude') ? 'error' : '' }}">		
							<label>{{ AdminLanguage::transAdmin('Geografska širina (latitude)') }}</label>
							<input class="google-map-latitude" name="latitude" value="{{ !is_null(Input::old('latitude')) ? Input::old('latitude') : $latitude }}"  type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="medium-6 columns {{ $errors->first('longitude') ? 'error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Geografska dužina (longitude)') }}</label>
							<input class="google-map-latitude" name="longitude" value="{{ !is_null(Input::old('longitude')) ? Input::old('longitude') : $longitude }}"  type="text" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div> 

						<div class="columns">
							<label>{{ AdminLanguage::transAdmin('Da bi ste pravilno popunili polja za google mapu mozete posetiti ovaj sajt') }} <a href="http://www.latlong.net/" target="_blank">www.latlong.net</a>, {{ AdminLanguage::transAdmin('gde upisujete svoju željenu adresu npr') }}: <span style="color: green">{{ AdminLanguage::transAdmin('Dušanova 65 Niš') }}</span>
							{{ AdminLanguage::transAdmin('i uzimate potrebne koordinate') }}</label>
						</div>		
					</div>  

					<hr>

					<div class="social-edit row">
						<h3 class="title-med">{{ AdminLanguage::transAdmin('Društvene mreže') }}:</h3>
						
						<div class="columns medium-4 field-group" title="{{ AdminLanguage::transAdmin('Dodajte Facebook link') }}">
							<i class="fa fa-facebook-official" aria-hidden="true"></i>
							<input name="facebook" class="social-edit__group__input {{ $errors->first('facebook') ? 'error' : '' }}" placeholder="{{ AdminLanguage::transAdmin('Dodajte Facebook link') }}" type="text" value="{{ !is_null(Input::old('facebook')) ? Input::old('facebook') : $preduzece->facebook }}" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="columns medium-4 field-group" title="{{ AdminLanguage::transAdmin('Dodajte Twitter link') }}">
							<i class="fa fa-twitter" aria-hidden="true"></i>
							<input name="twitter" class="social-edit__group__input {{ $errors->first('facebook') ? 'twitter' : '' }}" placeholder="{{ AdminLanguage::transAdmin('Dodajte Twitter link') }}" type="text" value="{{ !is_null(Input::old('twitter')) ? Input::old('twitter') : $preduzece->twitter }}" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						
						<div class="columns medium-4 field-group" title="{{ AdminLanguage::transAdmin('Dodajte Google+ link') }}">
							<i class="fa fa-google-plus" aria-hidden="true"></i>
							<input name="google_p" class="social-edit__group__input {{ $errors->first('facebook') ? 'google_p' : '' }}" placeholder="{{ AdminLanguage::transAdmin('Dodajte Google Plus link') }}" type="text" value="{{ !is_null(Input::old('google_p')) ? Input::old('google_p') : $preduzece->google_p }}" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						
						<div class="columns medium-4 field-group" title="{{ AdminLanguage::transAdmin('Dodajte Skype link') }}">
							<i class="fa fa-skype" aria-hidden="true"></i>
							<input name="skype" class="social-edit__group__input {{ $errors->first('facebook') ? 'skype' : '' }}" placeholder="{{ AdminLanguage::transAdmin('Dodajte Skype link') }}" type="text" value="{{ !is_null(Input::old('skype')) ? Input::old('skype') : $preduzece->skype }}" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="columns medium-4 field-group" title="{{ AdminLanguage::transAdmin('Dodajte Instagram link') }}">
							<i class="fa fa-instagram" aria-hidden="true"></i>
							<input name="instagram" class="social-edit__group__input {{ $errors->first('facebook') ? 'instagram' : '' }}" placeholder="{{ AdminLanguage::transAdmin('Dodajte instagram link') }}" type="text" value="{{ !is_null(Input::old('instagram')) ? Input::old('instagram') : $preduzece->instagram }}" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>
						
						<div class="columns medium-4 field-group" title="{{ AdminLanguage::transAdmin('Dodajte linkedIn link') }}">
							<i class="fa fa-linkedin-square" aria-hidden="true"></i>
							<input name="linkedin" class="social-edit__group__input {{ $errors->first('facebook') ? 'linkedin' : '' }}" placeholder="{{ AdminLanguage::transAdmin('Dodajte linkedIn link') }}" type="text" value="{{ !is_null(Input::old('linkedin')) ? Input::old('linkedin') : $preduzece->linkedin }}" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="columns medium-4 field-group" title="{{ AdminLanguage::transAdmin('Dodajte youtube link') }}">
							<i class="	fa fa-youtube-square" aria-hidden="true"></i>
							<input name="youtube" class="social-edit__group__input {{ $errors->first('facebook') ? 'youtube' : '' }}" placeholder="{{ AdminLanguage::transAdmin('Dodajte youtube link') }}" type="text" value="{{ !is_null(Input::old('youtube')) ? Input::old('youtube') : $preduzece->youtube }}" {{ Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>  
					</div>

					@if(Admin_model::check_admin(array('KONTAKT_PODACI_AZURIRANJE'))) 
					<div class="btn-container center">
						<button type="submit" class="contact-edit-save btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					</div> 
					@endif
				</form>
			</div>  
		</section>
	</div>
</section>  
