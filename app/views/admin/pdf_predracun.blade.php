<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php /* header('Content-Type: text/html; charset=utf-8;'); */?> 
<title>{{ AdminLanguage::transAdmin('PDF') }}</title>
<style>

.row::after {content: ""; clear: both; display: table;}

[class*="col-"] { float: left; padding: 5px; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

* {margin: 0; font-family: DejaVu Sans; box-sizing: border-box;}

body {font-size: 16px;}

table {border-collapse: collapse; font-size: 12px; margin-bottom: 15px; width: 100%;}

td, th {border: 1px solid #cacaca; text-align: left; padding: 3px;}

tr:nth-child(even) {background-color: #f5f6ff;}

h2 {margin-bottom: 15px;}

.container {width: 90%;	margin: 42px auto;}

.logo img {	max-width: 150px; width: 100%;}

.text-right {text-align: right;}

.signature {background: none; color: #808080;}

.signature td, th {border: none; background: none;}
 
.company-info {font-size: 11px;}

.comp-name {font-size: 18px; font-weight: bold;}
 
.kupac-info td {border: none;}

.kupac-info { border: 1px solid #ddd; }

thead {background: #eee;}

.ziro {font-weight: bold; font-size: 14px; margin: 10px 0;}
 
.napomena p {margin: 10px 0; font-size: 13px;}

.info-1 {margin-bottom: 30px;}

.rbr {width: 50px;}

.artikli td {text-align: center;}

.cell-product-name {text-align: left !important;}

.sum-span {margin-right: 20px;}

.page-break {
    page-break-after: always;
}

</style>
</head>
<body>
	@foreach($web_b2c_narudzbina_ids as $idKey => $web_b2c_narudzbina_id)
	<?php
		$show_header = true;
	    $limit = 10;
	    $stavke_count = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->count();
		$count_pages = floor($stavke_count/$limit);
		$mod = fmod($stavke_count,$limit);
		if($mod > 0){ $count_pages++; }
		if($mod > 5){
			$count_pages++;
			$show_header = false;
		}else if($mod == 0){
			$count_pages++;
			$show_header = false;
		}
	    $rbr = 1;
	?>
	<div class="container">
	@foreach(range(0,$count_pages-1) as $page)
	<?php $stavke = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->limit($limit)->offset($page*$limit)->get(); ?>
		@if(!($page == ($count_pages-1) and !$show_header))
			@include('admin.partials.pdf.predracun_header')
		@endif
		<div class="row"> 
		<table class="artikli">
			@if(!($page == ($count_pages-1) and !$show_header))
	        <tr>
	        	<td class="cell">{{ AdminLanguage::transAdmin('Rbr') }}</td>
	        	@if(AdminOptions::sifra_view()==1)
	        		<td class="cell">{{ AdminLanguage::transAdmin('Roba_id') }}</td>
				@elseif(AdminOptions::sifra_view()==2)
					<td class="cell">{{ AdminLanguage::transAdmin('Šifra IS') }}</td>
				@elseif(AdminOptions::sifra_view()==3)
					<td class="cell">{{ AdminLanguage::transAdmin('SKU') }}</td>
				@elseif(AdminOptions::sifra_view()==4)
					<td class="cell">{{ AdminLanguage::transAdmin('Šifra Dob') }}</td>
				@endif
	            <td class="cell-product-name">{{ AdminLanguage::transAdmin('Naziv proizvoda') }}</td>
	            <td class="cell">{{ AdminLanguage::transAdmin('Kol') }}.</td>
	            <td class="cell">{{ AdminLanguage::transAdmin('Cena') }}</td>
	            <td class="cell">{{ AdminLanguage::transAdmin('Poreska osnovica') }}</td>
	            <td class="cell">{{ AdminLanguage::transAdmin('PDV') }}</td>
	            <td class="cell">{{ AdminLanguage::transAdmin('Iznos PDV') }}</td>
	            <td class="cell">{{ AdminLanguage::transAdmin('Sa PDV-om') }}</td>
	        </tr>
	        @endif

	        @foreach($stavke as $row)
	        	<tr>
	        		<td class="cell"><?php echo $rbr; $rbr++; ?></td>
	        		@if(AdminOptions::sifra_view()==1)
	        			<td class="cell">{{ AdminArticles::find($row->roba_id, 'roba_id')}}</td>
	        		@elseif(AdminOptions::sifra_view()==2)
	        			<td class="cell">{{ AdminArticles::find($row->roba_id, 'sifra_is') }}</td>
	        		@elseif(AdminOptions::sifra_view()==3)
	        			<td class="cell">{{ AdminArticles::find($row->roba_id, 'sku') }}</td>
	        		@elseif(AdminOptions::sifra_view()==4)
	        			<td class="cell">{{ AdminArticles::find($row->roba_id, 'sifra_d') }}</td>
	        		@endif
	                <td class="cell-product-name">{{ AdminCommon::short_title($row->roba_id)}} {{ AdminOsobine::getOsobineStr($row->roba_id, $row->osobina_vrednost_ids) }}</td>
	                @if(Options::web_options(320) == 1)
                        @if ( AdminArticles::jedinica_mere($row->roba_id)->jedinica_mere_id == 3 AND Product::pakovanje($row->roba_id) ) 
                        <td>{{ $row->kolicina }}kg</td>
                        @else
                        <td>{{ (int)$row->kolicina }}</td>
                        @endif
                    @else
                    <td>{{ (int)$row->kolicina }}</td>
                    @endif
	                <td class="cell">{{ AdminCommon::cena(($row->jm_cena / AdminNarudzbine::tarifnaGrupaPdv($row->tarifna_grupa_id))) }}</td>
	                <td class="cell">{{ AdminCommon::cena(($row->jm_cena / AdminNarudzbine::tarifnaGrupaPdv($row->tarifna_grupa_id)) * $row->kolicina) }}</td>
	                <td class="cell">{{ AdminNarudzbine::pdv($row->tarifna_grupa_id) }}</td>
	                <td class="cell">{{ AdminCommon::cena(($row->jm_cena - ($row->jm_cena / AdminNarudzbine::tarifnaGrupaPdv($row->tarifna_grupa_id))) * $row->kolicina) }}</td>
	                <td class="cell">{{ AdminCommon::cena($row->kolicina * $row->jm_cena) }}</td>
	            </tr>
	        @endforeach
        </table>
        </div>
		@if($page == ($count_pages-1))
		    @include('admin.partials.pdf.predracun_rekapitulacija')
			@include('admin.partials.pdf.predracun_footer')
		@else
			<div class="page-break"></div>
			<br>
		@endif
	@endforeach

	</div>
	@if((count($web_b2c_narudzbina_ids)-1) != $idKey)
	<div class="page-break"></div>
	@endif
@endforeach
</body>
</html>
