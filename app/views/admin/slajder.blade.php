<section id="main-content" class="banners">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif

	<div class="row"> 
		<section class="medium-3 columns">
			<div class="flat-box">

				<label class="text-center">{{ AdminLanguage::transAdmin('Filter') }}</label>
				<select class="JSdeviceFilter">
					@foreach(AdminSupport::sliderDevices() as $sliderDevice)
						<option id="{{ $sliderDevice->slajder_device_id }}">{{ AdminLanguage::transAdmin($sliderDevice->naziv) }}</option>
					@endforeach
				</select>

				<!-- BANNER LIST -->
				<label class="text-center">{{ AdminLanguage::transAdmin('Slajderi') }} / {{ AdminLanguage::transAdmin('Baneri') }}</label>
				<ul @if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE'))) class="banner-list" @endif data-table="2">
					@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
					<li id="0" class="new-banner new-elem relative">
						<a href="{{AdminOptions::base_url()}}admin/slider/0">{{ AdminLanguage::transAdmin('Novi slajder') }} / {{ AdminLanguage::transAdmin('baner') }}</a>
						<span class="absolute-right padding-h-8 text-gray"><i class="fa fa-plus" aria-hidden="true"></i></span>
					</li>
					@endif
					@foreach($sliders as $slider)
					<li class="ui-state-default relative @if($selectedSlider->slajder_id == $slider->slajder_id) active @endif" data-slajder_device_id="{{$slider->slajder_device_id}}"  id="{{$slider->slajder_id}}">
						<a href="{{AdminOptions::base_url()}}admin/slider/{{$slider->slajder_id}}">{{ $slider->naziv }} - <label class="inline-block no-margin"><b>{{ AdminLanguage::transAdmin('tip') }}</b> {{ AdminSlajder::getSliderTypeBySliderId($slider->slajder_id)->naziv }}</label></a>
						@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
						<a class="absolute-right padding-h-8 tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminOptions::base_url()}}admin/slider-delete/{{$slider->slajder_id}}"><i class="fa fa-times text-gray" aria-hidden="true"></i></a>
						@endif
					</li>
					@endforeach
				</ul>
			</div>

			<!-- BACKGROUND IMAGE -->
			@if(in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(5,6,7,10)))
			<div class="flat-box">
				<label class="text-center">{{ AdminLanguage::transAdmin('Pozadinska slika') }}</label>
				<ul class="">
					<form action="{{AdminOptions::base_url()}}admin/bg-img-edit" method="POST" enctype="multipart/form-data">
						@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
						<input type="file" name="bgImg" value="Izaberi sliku" class="no-margin">
						@endif

						@if(AdminSupport::getBGimg() == null)
						<img src="{{AdminOptions::base_url()}}/images/no-image.jpg" alt="" class="bg-img">
						@else
						<img src="{{AdminOptions::base_url()}}{{ AdminSupport::getBGimg() }}" alt="" class="bg-img">
						@endif
						<label class="text-center">{{ AdminLanguage::transAdmin('Preporučene dimenzije slike') }}: 1920x1080</label>


						<div> 
							<label class="text-center">{{ AdminLanguage::transAdmin('Link leve strane') }}</label>
							<input id="link" value="{{AdminSupport::getlink()}}" type="text" name="link" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<label class="text-center">{{ AdminLanguage::transAdmin('Link desne strane') }}</label>
							<input id="link2" value="{{AdminSupport::getlink2()}}" type="text" name="link2" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<br>

						<input type="hidden" value="3" name="flag">
						@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
						<div class="text-center btn-container">
							<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
							<a class="btn btn-danger" href="{{AdminOptions::base_url()}}admin/bg-img-delete">{{ AdminLanguage::transAdmin('Obriši') }}</a>
						</div>
						@endif
					</form>
				</ul>
			</div>
			@endif

		</section>

		<section class="medium-3 columns">
			<div class="flat-box"> 
				<form action="{{AdminOptions::base_url()}}admin/slider-edit" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="logo-right">
					<label class="text-center">{{ AdminLanguage::transAdmin('Slajder / Baner') }}</label>

					<section class="banner-edit-top row">
						<div class="medium-12 columns">
							<label>{{ AdminLanguage::transAdmin('Naziv') }}</label>
							<input id="naziv" value="{{ Input::old('naziv') ? Input::old('naziv') : trim($selectedSlider->naziv) }}" type="text" name="naziv" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('naziv') ? $errors->first('naziv') : '' }}</div>
						</div>

						<div class="medium-4 columns">
							<label>{{ AdminLanguage::transAdmin('Tip') }}</label>
							<select name="slajder_tip_id" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'disabled' : '' }}>
							@foreach(AdminSlajder::sliderTypes() as $slider_type)
							<option value="{{$slider_type->slajder_tip_id}}" 
							{{ ($slider_type->slajder_tip_id == intval($selectedSlider->slajder_tip_id) ? 
							'selected' :  '') }}
							>{{ $slider_type->naziv }}</option>
							@endforeach
							</select>
						</div>
 
						<div class="medium-4 columns">
							<label>{{ AdminLanguage::transAdmin('Aktivan') }}</label>
							<select name="flag_aktivan" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value="1">{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" {{ (Input::old('flag_aktivan') == '0') ? 'selected' : (($selectedSlider->flag_aktivan == 0) ? 'selected' : '') }}>{{ AdminLanguage::transAdmin('NE') }}</option>
							</select>
						</div>
						
						<div class="medium-4 columns">
							<label>{{ AdminLanguage::transAdmin('Uređaj') }}</label>
							<select class="JSdeviceType" name="slajder_device_id" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value="1" {{ (Input::old('slajder_device_id') == '0') ? 'selected' : (($selectedSlider->slajder_device_id == 1) ? 'selected' : '') }}>{{ AdminLanguage::transAdmin('Default') }}</option>
								<option value="2" {{ (Input::old('slajder_device_id') == '0') ? 'selected' : (($selectedSlider->slajder_device_id == 2) ? 'selected' : '') }}>{{ AdminLanguage::transAdmin('Desktop') }}</option>
								<!-- <option value="3" {{ (Input::old('slajder_device_id') == '0') ? 'selected' : (($selectedSlider->slajder_device_id == 3) ? 'selected' : '') }}>{{ AdminLanguage::transAdmin('Tablet') }}</option> -->
								<option value="4" {{ (Input::old('slajder_device_id') == '0') ? 'selected' : (($selectedSlider->slajder_device_id == 4) ? 'selected' : '') }}>{{ AdminLanguage::transAdmin('Mobile') }}</option>
							</select>
						</div>
					</section>

					<br>

					<div>
						<input type="hidden" name="slajder_id" value="{{ $selectedSlider->slajder_id }}" />
						<input type="hidden" name="jezik_id" value="{{$lang_id}}" />
						@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
						<div class="btn-container center">
							<button class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						</div>
						@endif
					</div>
				</form> 
			</div>
		</section>

		@if($selectedSlider->slajder_id > 0)
		<section class="medium-3 columns">
			<div class="flat-box">

				<label class="text-center">{{ AdminLanguage::transAdmin('Stavke') }}</label>
				@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
				<ul class="banner-list">	
					<li class="new-banner new-elem relative">
						<a href="{{AdminOptions::base_url()}}admin/slider/{{ $selectedSlider->slajder_id }}/0">{{ AdminLanguage::transAdmin('Nova stavka') }}</a>
						<span class="absolute-right padding-h-8 text-gray"><i class="fa fa-plus" aria-hidden="true"></i></span>
					</li>
				</ul>
				@endif
				<ul class="banner-list @if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE'))) JSSliderItemsSortable @endif" data-slajder_id="{{$selectedSlider->slajder_id}}">
					@foreach($sliderItems as $key => $sliderItem)
					<li class="ui-state-default relative @if($selectedSliderItem->slajder_stavka_id == $sliderItem->slajder_stavka_id) active @endif" data-slajder_stavka_id="{{$sliderItem->slajder_stavka_id}}">
						<a href="{{AdminOptions::base_url()}}admin/slider/{{ $selectedSlider->slajder_id }}/{{$sliderItem->slajder_stavka_id}}">
							{{ AdminLanguage::transAdmin('Slika') }} {{ ($key+1) }}
							<img class="slide-xs-img" src="{{ AdminOptions::base_url().$sliderItem->image_path }}">
						</a>
						@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
						<a class="absolute-right padding-h-8 tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminOptions::base_url()}}admin/slider-item-delete/{{$sliderItem->slajder_stavka_id}}"><i class="fa fa-times text-gray" aria-hidden="true"></i></a>
						@endif
					</li>
					@endforeach
				</ul>
			</div>  
		</section>

		<section class="medium-3 columns">
			<div class="flat-box">
				@if($selectedSliderItem->slajder_stavka_id > 0)
				<section class="banner-preview-area"> 
					<img class="img-responsive margin-h-10 margin-v-auto img-max-h-200" src="{{ AdminOptions::base_url() }}{{ $selectedSliderItem->image_path }}">
				</section>
				@endif

				<form action="{{AdminOptions::base_url()}}admin/slider-item-edit" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="logo-right"> 

					<div class="row">
						<div class="medium-6 columns">
							<label>{{ AdminLanguage::transAdmin('Slika') }}</label>
							<input class="no-margin" id="image_path" type="file" name="image_path" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('image_path') ? $errors->first('image_path') : '' }}</div>
						</div>

						<div class="medium-6 columns">
							<label>{{ AdminLanguage::transAdmin('Aktivan') }}</label>
							<select name="flag_aktivan" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value="1">{{ AdminLanguage::transAdmin('DA') }}</option>
								<option value="0" {{ (Input::old('flag_aktivan') == '0') ? 'selected' : (($selectedSliderItem->flag_aktivan == 0) ? 'selected' : '') }}>{{ AdminLanguage::transAdmin('NE') }}</option>
							</select>
						</div>
					</div>

					<div class="row"> 
						<div class="medium-6 columns">
							<label>{{ AdminLanguage::transAdmin('Datum od') }}</label>

							<div class="relative"> 
								<input id="datum_od" value="{{ Input::old('datum_od') ? Input::old('datum_od') : trim($selectedSliderItem->datum_od) }}" type="text" name="datum_od" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<span class="absolute-right pointer color-red" id="datum_od_delete"><i class="fa fa-close"></i></span>
							</div>

							<div class="error">{{ $errors->first('datum_od') ? $errors->first('datum_od') : '' }}</div>
						</div>

						<div class="medium-6 columns">
							<label>{{ AdminLanguage::transAdmin('Datum od') }}</label>

							<div class="relative"> 
								<input id="datum_do" value="{{ Input::old('datum_do') ? Input::old('datum_do') : trim($selectedSliderItem->datum_do) }}" type="text" name="datum_do" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
								<span class="absolute-right pointer color-red" id="datum_do_delete"><i class="fa fa-close"></i></span>
							</div>

							<div class="error">{{ $errors->first('datum_do') ? $errors->first('datum_do') : '' }}</div>
						</div>
					</div>

					<div class="medium-12 columns">
						@if($selectedSliderItem->slajder_stavka_id != 0 && count($jezici) > 1)
						<div class="languages">
							<ul>	
								@foreach($jezici as $jezik)
								<li><a class="{{ $lang_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/slider/{{ $selectedSlider->slajder_id }}/{{ $selectedSliderItem->slajder_stavka_id }}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
								@endforeach
							</ul>
						</div> 
						@endif

						<div>
							<label>{{ AdminLanguage::transAdmin('Alt') }}</label>
							<input id="alt" value="{{ Input::old('alt') ? Input::old('alt') : trim($sliderItemLang->alt) }}" type="text" name="alt" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('alt') ? $errors->first('alt') : '' }}</div>
						</div>

						<div>
							<label>{{ AdminLanguage::transAdmin('Link') }}</label>
							<input id="link" value="{{ Input::old('link') ? Input::old('link') : trim($sliderItemLang->link) }}" type="text" name="link" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('link') ? $errors->first('link') : '' }}</div>
						</div>

						<div>
							<label>{{ AdminLanguage::transAdmin('Naslov') }}</label>
							<input id="naslov" value="{{ Input::old('naslov') ? Input::old('naslov') : trim($sliderItemLang->naslov) }}" type="text" name="naslov" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('naslov') ? $errors->first('naslov') : '' }}</div>
						</div>

						<div>
							<label>{{ AdminLanguage::transAdmin('Naslov dugmeta') }}</label>
							<input id="naslov_dugme" value="{{ Input::old('naslov_dugme') ? Input::old('naslov_dugme') : trim($sliderItemLang->naslov_dugme) }}" type="text" name="naslov_dugme" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>
							<div class="error">{{ $errors->first('naslov_dugme') ? $errors->first('naslov_dugme') : '' }}</div>
						</div>

						<div>
							<label>{{ AdminLanguage::transAdmin('Sadržaj') }}</label>
							<textarea @if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE'))) class="special-textareas" @endif name="sadrzaj" {{ Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('sadrzaj') ? Input::old('sadrzaj') : trim($sliderItemLang->sadrzaj) }}</textarea>
						</div> 

						<br>

						<div>
							<input type="hidden" name="slajder_stavka_id" value="{{ $selectedSliderItem->slajder_stavka_id }}" />
							<input type="hidden" name="slajder_id" value="{{ $selectedSliderItem->slajder_id }}" />
							<input type="hidden" name="jezik_id" value="{{$lang_id}}" />
							@if(Admin_model::check_admin(array('BANERI_I_SLAJDER_AZURIRANJE')))
							<div class="btn-container center">
								<button class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
							</div>
							@endif
						</div>
					</div>
				</form>
			</div> 
		</section>
		
		@endif

	</div> 
</section>
