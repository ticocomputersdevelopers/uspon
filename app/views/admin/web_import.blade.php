<section class="import-content" id="main-content">
	<!--=========== LOADER =============== -->
	<div class="JSads-loader"> 
		<div class="ads-loader-inner"> 
			<div>  
				<span class="loadSpiner"></span>
				<span>{{ AdminLanguage::transAdmin('Sadržaj se učitava. Molimo Vas sačekajte') }}.</span>
			</div>
		</div>
	</div>   

	<div class="row small-gutter">
		
		@if(Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE'))) 
		<div class="columns medium-2">
			<div class="flat-box"> 
				<label class="text-center">{{ AdminLanguage::transAdmin('Import cenovnika') }}</label>

				<select class="m-input-and-button__input import-select" id="dobavljac_select">
					<option value="0">{{ AdminLanguage::transAdmin('Svi dobavljači') }}</option>
					@foreach(AdminImport::dobavljaci_web_import_tabela() as $dobavljac)
					@if( in_array($dobavljac->partner_id ,explode("-",$criteriaImport['dobavljac'])) )
					<option value="{{ $dobavljac->partner_id }}" selected>{{ $dobavljac->naziv }}</option>
					@else
					<option value="{{ $dobavljac->partner_id }}">{{ $dobavljac->naziv }}</option>
					@endif
					@endforeach
				</select>

				<form method="POST" action="{{AdminOptions::base_url()}}admin/upload_web_import" enctype="multipart/form-data">
					<input type="hidden" name="partner_id" value="{{ Session::has('upload_file_js') ? Session::get('upload_file_js')[3] : $partner_id }}">  

					<div class="row collapse" style="padding: 5px 0 0;"> 
						<div class="columns small-6"> 
							<label class="no-margin">{{ AdminLanguage::transAdmin('Kurs evra') }}: </label>
						</div>
						<div class="columns small-6">  
							<input class="JSkursVal wiKurs" type="text" name="kurs"  value="{{ Input::old('kurs') ? Input::old('kurs') : $kurs }}">
						</div> 
					</div>

					@if(AdminOptions::gnrl_options(3030)==1)
					<input type="checkbox" name="roba_insert" {{ !is_null(Input::old('roba_insert')) ? 'checked' : '' }} > <label> {{ AdminLanguage::transAdmin('Umatiči nove') }}</label>
					@endif 


					<div class="text-center">
						<div class="bg-image-file">
							<!-- <label>Izaberite fajl:</label> -->
							<input type="file" name="upload_file" value="Izaberi fajl" placeholder="{{ AdminLanguage::transAdmin('Izaberi fajl') }}">
							<input type="hidden" id="has_import" value="{{ Session::has('upload_file_js') ? Session::get('upload_file_js')[0] : null }}">
							<input type="hidden" id="upload_file" value="{{ Session::has('upload_file_js') ? Session::get('upload_file_js')[1] : null }}">
							<input type="hidden" id="upload_file_extension" value="{{ Session::has('upload_file_js') ? Session::get('upload_file_js')[2] : null }}">
							<input type="hidden" id="roba_insert" value="{{ Session::has('upload_file_js') ? Session::get('upload_file_js')[4] : null }}">
						</div>

						<input class="btn-admin btn btn-primary btn-import btn-small center" type="submit" value="Importuj" name="submit">
						@if(AdminOptions::gnrl_options(3002)==1)
						<a class="btn btn-primary btn-import add-new-btn tooltipz" aria-label="{{ AdminLanguage::transAdmin('Upload fajla') }}" href="{{ AdminOptions::base_url()}}admin/file-upload">A</a>
						@endif
						@if(AdminOptions::gnrl_options(3030)==1)
						<a class="btn btn-primary btn-import add-new-btn tooltipz" aria-label="{{ AdminLanguage::transAdmin('Import podaci grupa') }}" href="{{ AdminOptions::base_url()}}admin/import_podaci_grupa/{{ explode('-',$criteriaImport['dobavljac'])[0] }}/0/0">G</a>
						<a class="btn btn-primary btn-import add-new-btn tooltipz" aria-label="{{ AdminLanguage::transAdmin('Import podaci Proizvodjač') }}" href="{{ AdminOptions::base_url()}}admin/import-podaci-proizvodjac/{{ isset($criteriaImport['dobavljac']) ? explode('-',$criteriaImport['dobavljac'])[0] : '0' }}/0">P</a>
						@endif
					</div> 
				</form>

				<div id="message_import" class="center"></div>
			</div> 
		</div>		

		<div class="columns medium-4">
			<div class="flat-box">
				<label class="text-center">{{ AdminLanguage::transAdmin('Rad sa artiklima') }}</label>
				<div class="row">
					<div class="columns medium-7 small-10"> 
						<div class="m-input-and-button">
							<select id="webImportGrupa" class="admin-select m-input-and-button__input JSexecuteInput">
								{{ AdminSupport::selectGroups(0) }}
							</select>
							<button id="grupaButton" class="btn btn-icon btn-secondary m-input-and-button__btn JSexecuteBtn btn-radius tooltipz" aria-label="{{ AdminLanguage::transAdmin('Dodeli') }}" data-execute='[{"table":"dobavljac_cenovnik", "column":"grupa_pr_id"},{"table":"roba", "column":"grupa_pr_id"}]'><i class="fa fa-check" aria-hidden="true"></i></button>

							<a class="btn btn-primary m-input-and-button__btn tooltipz" aria-label="{{ AdminLanguage::transAdmin('Napravi novu grupu') }}" href="{{AdminOptions::base_url()}}admin/grupe/0" target="_blank"><i class="fa fa-plus" aria-hidden="true"></i></a>
						</div> <!-- end of .field-group no-label-margin__flex -->

						<div class="m-input-and-button">
							<select id="webImportProizvodjac" class="m-input-and-button__input JSexecuteInput">
								<option value="-1">{{ AdminLanguage::transAdmin('Izabrerite proizvođača') }}</option>
								@foreach(AdminSupport::getProizvodjaci() as $proizvodjac)
								<option value="{{ $proizvodjac->proizvodjac_id }}">{{ $proizvodjac->naziv }}</option>
								@endforeach
							</select>

							<button id="proizvodjacButton" class="m-input-and-button__btn btn btn-icon btn-secondary JSexecuteBtn btn-radius tooltipz" aria-label="{{ AdminLanguage::transAdmin('Dodeli') }}" data-execute='[{"table":"dobavljac_cenovnik", "column":"proizvodjac_id"},{"table":"roba", "column":"proizvodjac_id"}]'><i class="fa fa-check" aria-hidden="true"></i></button>

							<a class="btn btn-primary m-input-and-button__btn tooltipz" aria-label="{{ AdminLanguage::transAdmin('Napravi novog proizvođača') }}" href="{{AdminOptions::base_url()}}admin/proizvodjac/0" target="_blank"><i class="fa fa-plus" aria-hidden="true"></i></a>
						</div> 

						<div class="m-input-and-button">
							<select id="webImportTarifnagrupa" class="m-input-and-button__input JSexecuteInput">
								<option selected>{{ AdminLanguage::transAdmin('Izaberite tarifnu grupu') }}</option>
								@foreach (AdminSupport::getPoreskeGrupe() as $porez)
								@if($porez->default_tarifna_grupa == 1)
								<option selected value="{{ $porez->tarifna_grupa_id }}">{{ $porez->naziv }}</option>
								@else
								<option value="{{ $porez->tarifna_grupa_id }}">{{ $porez->naziv }}</option>
								@endif

								@endforeach
							</select>	
							<button id="tarifna_grupaButton" class="m-input-and-button__btn btn btn-secondary JSexecuteBtn btn-radius tooltipz" aria-label="{{ AdminLanguage::transAdmin('Dodeli') }}" data-execute='[{"table":"dobavljac_cenovnik", "column":"tarifna_grupa_id"}]'><i class="fa fa-check" aria-hidden="true"></i></button>				
						</div> <!-- end of .m-input-and-button --> 
					</div> <!-- end of .medium-6 .large-6 -->

					<div class="columns medium-offset-1 medium-4">

						<div class="field-group no-label-margin">
							<!-- 	<label for="">MP marža</label> -->
							<div class="m-input-and-button">
								<input class="JSexecuteInput web-marza-input" type="text" id="mp_marza" placeholder="MP marža"> 
								<button id="mp_marza_but" class="btn btn-icon btn-primary JSexecuteBtn btn-radius tooltipz" aria-label="Dodeli" data-execute='[{"table":"dobavljac_cenovnik", "column":"mp_marza"},{"table":"roba", "column":"mp_marza"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
							</div>
						</div>

						<div class="field-group no-label-margin">
							<!-- <label for="">Web marža</label> -->
							<div class="m-input-and-button">
								<input class="JSexecuteInput web-marza-input" type="text" id="web_marza" placeholder="{{ AdminLanguage::transAdmin('Web marža') }}"> 
								<button id="web_marza_but" class="btn btn-icon btn-primary JSexecuteBtn btn-radius tooltipz" aria-label="{{ AdminLanguage::transAdmin('Dodeli') }}" data-execute='[{"table":"dobavljac_cenovnik", "column":"web_marza"},{"table":"roba", "column":"web_marza"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
							</div>
						</div>
						<div class="field-group no-label-margin">
							<!-- 	<label for="">Količina</label> -->
							<div class="m-input-and-button">
								<input class="JSexecuteInput web-marza-input" type="text" id="kolicina_mass" placeholder="{{ AdminLanguage::transAdmin('Količina') }}"> 
								<button id="mp_marza_but" class="btn btn-icon btn-primary JSexecuteBtn btn-radius tooltipz" aria-label="{{ AdminLanguage::transAdmin('Dodeli') }}" data-execute='[{"table":"dobavljac_cenovnik", "column":"kolicina"}]'><i class="fa fa-check" aria-hidden="true"></i></button>
							</div>
						</div>
					</div>  
				</div>

				<div class="text-center">  
					<button class="JSunesi_nove btn-admin btn btn-primary btn-import add-new-btn center" id="unesi_nove" value="">{{ AdminLanguage::transAdmin('Unesi nove') }}</button>
				</div> 
			</div>
		</div>
		@endif

		<div class="columns medium-6">
			<div class="flat-box">
				<label class="text-center">{{ AdminLanguage::transAdmin('Filtriranje') }}</label>
				<div class="row collapse filter-table"> 
					<div class="columns medium-4"> 
						<table>
							<tr>
								<th width="25">{{ AdminLanguage::transAdmin('Da') }}</th>
								<th width="25">{{ AdminLanguage::transAdmin('Ne') }}</th>
							</tr>
							<tr>
								<td>
									<input class="filter-check" type="checkbox" data-check="13">
								</td>
								<td>
									<input class="filter-check" type="checkbox" data-check="14">
								</td>
								<td>{{ AdminLanguage::transAdmin('Umatičeni') }}</td>
							</tr>
							<tr>
								<td>
									<input class="filter-check" type="checkbox" data-check="9">
								</td>
								<td>
									<input class="filter-check" type="checkbox" data-check="10">
								</td>
								<td>{{ AdminLanguage::transAdmin('Lager dobavljača') }}</td>
							</tr>
							<tr>
								<td>
									<input class="filter-check" type="checkbox" data-check="19">
								</td>
								<td>
									<input class="filter-check" type="checkbox" data-check="20">
								</td>
								<td>{{ AdminLanguage::transAdmin('Lager (naš)') }}</td>
							</tr>
							<tr>
								<td>
									<input class="filter-check" type="checkbox" data-check="21">
								</td>
								<td>
									<input class="filter-check" type="checkbox" data-check="22">
								</td>
								<td>{{ AdminLanguage::transAdmin('Najniža NC') }}</td>
							</tr>
							<tr>
								<td>
									<input class="filter-check" type="checkbox" data-check="17">
								</td>
								<td>
									<input class="filter-check" type="checkbox" data-check="18">
								</td>
								<td>{{ AdminLanguage::transAdmin('Nova NC') }}</td>
							</tr>
						</table>
					</div>	

					<div class="columns medium-4">
						<table class="filter-table">
							<tr>
								<th width="25">{{ AdminLanguage::transAdmin('Da') }}</th>
								<th width="25">{{ AdminLanguage::transAdmin('Ne') }}</th>
							</tr>

							<tr>
								<td>
									<input class="filter-check" type="checkbox" data-check="15">
								</td>
								<td>
									<input class="filter-check" type="checkbox" data-check="16">
								</td>
								<td>{{ AdminLanguage::transAdmin('Na webu') }}</td>
							</tr>

							<tr>
								<td>
									<input class="filter-check" type="checkbox" data-check="11">
								</td>
								<td>
									<input class="filter-check" type="checkbox" data-check="12">
								</td>
								<td>{{ AdminLanguage::transAdmin('Aktivni') }}</td>
							</tr>

							<tr>
								<td>
									<input class="filter-check" type="checkbox" data-check="23">
								</td>
								<td>
									<input class="filter-check" type="checkbox" data-check="24">
								</td>
								<td>{{ AdminLanguage::transAdmin('Povezani') }}</td>
							</tr>
						</table>

						<div class="nc-options row">
							<div class="columns medium-8 small-9">
								<div class="columns medium-6 small-6">
									<label for="" class="tooltipz" aria-label="Nabavna cena">{{ AdminLanguage::transAdmin('NC od') }}</label>
									<input type="text" id="JSCenaOd" class="">
								</div>
								<div class="columns medium-6 small-6">
									<label for="" class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('Nabavna cena') }}">{{ AdminLanguage::transAdmin('NC do') }}</label>
									<input type="text" id="JSCenaDo" class="">
								</div>
							</div>
							<div class="columns medium-4 small-3 btns-od-do">
								<label>&nbsp;</label>
								<a href="#" id="JSCenaSearch" class="m-input-and-button__btn btn btn-primary btn-xm btn-radius">
									<i class="fa fa-search" aria-hidden="true"></i>
								</a>
								<a href="#" class="m-input-and-button__btn btn btn-secondary tooltipz" aria-label="{{ AdminLanguage::transAdmin('Poništi') }}">
									<i class="fa fa-times" id="JSCenaClear" aria-hidden="true"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="columns medium-4"> 
						<table>
							<tr>
								<th width="25">{{ AdminLanguage::transAdmin('Da') }}</th>
								<th width="25">{{ AdminLanguage::transAdmin('Ne') }}</th>
							</tr>
							<tr>
								<td>
									<input class="filter-check" type="checkbox" data-check="1">
								</td>
								<td>
									<input class="filter-check" type="checkbox" data-check="2">
								</td>
								<td>{{ AdminLanguage::transAdmin('Opis') }}</td>
							</tr>

							<tr>
								<td>
									<input class="filter-check" type="checkbox" data-check="5">
								</td>
								<td>
									<input class="filter-check" type="checkbox" data-check="6">
								</td>
								<td>{{ AdminLanguage::transAdmin('Karak. (dob.)') }}</td>
							</tr>
							<tr>
								<td>
									<input class="filter-check" type="checkbox" data-check="7">
								</td>
								<td>
									<input class="filter-check" type="checkbox" data-check="8">
								</td>
								<td>{{ AdminLanguage::transAdmin('Slike') }}</td>
							</tr>
						</table> 
					</div>
				</div>
			</div> <!-- end of .flat-box -->
		</div>

	</div><!-- opcije end -->

	<section class="search-section">
		<div class="flat-box search-block">

			<div class="row">

				<div class="columns medium-4 no-padd">
					<section class="m-input-and-button">
						<input type="text" class="m-input-and-button__input search-field" id="search" autocomplete="off" placeholder="{{ AdminLanguage::transAdmin('Pretraži') }}...">
						<button type="submit" id="search-btn" value="Pronađi" class="m-input-and-button__btn btn btn-primary btn-radius"><i class="fa fa-search" aria-hidden="true"></i></button> 

						<button type="submit" id="clear-btn" value="Poništi" class="m-input-and-button__btn btn btn-secondary tooltipz" aria-label="{{ AdminLanguage::transAdmin('Poništi') }}"><i class="fa fa-times" id="JSCenaClear" aria-hidden="true"></i></button> 
					</section>
				</div>

				<section class="columns medium-3 m-input-and-button select-box">
					<select  id="grupa_select" class="m-input-and-button">						
						{{ AdminSupport::selectGroups($criteriaImport['grupa_pr_id']) }}
					</select>
					<button class="btn btn-primary btn-block m-input-and-button__btn btn-icon JSGrProDob btn-radius tooltipz" aria-label="{{ AdminLanguage::transAdmin('Dodeli') }}"><i class="fa fa-check" aria-hidden="true"></i></button>
				</section>

				<section class="columns medium-3 select-box">
					<div class="m-input-and-button">
						<select class="admin-select js-example-basic-multiple m-input-and-button" id="proizvodjac_select" multiple="multiple">
							{{ AdminSupport::selectProizvodjaci($criteriaImport['proizvodjac']) }}
						</select>
						<button class="btn btn-primary btn-block JSGrProDob m-input-and-button__btn btn-icon btn-radius tooltipz" aria-label="{{ AdminLanguage::transAdmin('Dodeli') }}"><i class="fa fa-check" aria-hidden="true"></i></button>
					</div>
				</section>
			</div>
		</div>
	</section>

	<ul class="custom-menu">
		<li>
			<button class="custom-menu-item" id="all" value="">{{ AdminLanguage::transAdmin('Izaberi sve (F3)') }}</button>
		</li>
		<li>
			<button class="JSunesi_nove custom-menu-item" id="">{{ AdminLanguage::transAdmin('Unesi nove') }}</button>
		</li>
		<li>
			@if(AdminOptions::gnrl_options(3006)==1)
			<button class="custom-menu-item" id="JSCeneRunModal" value="">{{ AdminLanguage::transAdmin('Prihvati cene i nazive') }}</button>
			@else
			<button class="custom-menu-item JSPrihvatiCeneNaziv" data-naziv="0" value="">{{ AdminLanguage::transAdmin('Prihvati cene') }}</button>
			@endif	
		</li>
		<li>
			@if(AdminOptions::gnrl_options(3006)==1)
			<button class="custom-menu-item" id="JSLagerRunModal" value="">{{ AdminLanguage::transAdmin('Prihvati lager') }}</button>
			@else
			<button class="custom-menu-item JSPrihvatiLager" value="">{{ AdminLanguage::transAdmin('Prihvati lager') }}</button>
			@endif
		</li>
		<li>
			<button class="custom-menu-item" id="preracunaj_cene" value="">{{ AdminLanguage::transAdmin('Preračunaj cene') }}</button>
		</li>
		<li>
			<button class="custom-menu-item JSexecute" data-execute='[{"table":"dobavljac_cenovnik", "column":"flag_prikazi_u_cenovniku", "val":"0"},{"table":"roba", "column":"flag_prikazi_u_cenovniku", "val":"0"}]'>{{ AdminLanguage::transAdmin('Skini sa web-a') }}</button>
		</li>		
		<li>
			<button class="custom-menu-item JSexecute" data-execute='[{"table":"dobavljac_cenovnik", "column":"flag_prikazi_u_cenovniku", "val":"1"},{"table":"roba", "column":"flag_prikazi_u_cenovniku", "val":"1"}]'>{{ AdminLanguage::transAdmin('Stavi na web') }}</button>
		</li>
		<li>
			<button class="custom-menu-item JSexecute" data-execute='[{"table":"dobavljac_cenovnik", "column":"flag_zakljucan", "val":"1"},{"table":"roba", "column":"flag_zakljucan", "val":"true"}]'>{{ AdminLanguage::transAdmin('Zaključaj') }}</button>
		</li>   
		<li>
			<button class="custom-menu-item JSexecute" data-execute='[{"table":"dobavljac_cenovnik", "column":"flag_zakljucan", "val":"0"},{"table":"roba", "column":"flag_zakljucan", "val":"false"}]'>{{ AdminLanguage::transAdmin('Otključaj') }}</button>
		</li>
		<li>
			<button class="custom-menu-item" id="kolicina_null">{{ AdminLanguage::transAdmin('Storniraj kolicine na 0') }}.</button>
		</li>
		<li>
			@if(AdminOptions::gnrl_options(3006)==1)
			<button class="custom-menu-item" id="JSKarakSlikeRunModal" value="">{{ AdminLanguage::transAdmin('Preuzmi dodatne karakteristike i slike') }}</button>
			@else
			<button class="custom-menu-item JSKarakSlike" value="">{{ AdminLanguage::transAdmin('Preuzmi dodatne karakteristike i slike') }}</button>
			@endif
		</li>
		<li>
			<button class="custom-menu-item" id="obrisi" value="">{{ AdminLanguage::transAdmin('Obriši artikal') }}</button>
		</li>
		<li>
			<button class="custom-menu-item" id="prepisi_cenu" value="">{{ AdminLanguage::transAdmin('Prepiši web cenu') }}</button>
		</li>
		<li>
			<button class="custom-menu-item" id="povezi_artikle" value="">{{ AdminLanguage::transAdmin('Poveži artikle (F9)') }}</button>
		</li>
		<li>
			<button class="custom-menu-item" id="razvezi_artikle" value="">{{ AdminLanguage::transAdmin('Razveži artikle (F10)') }}</button>
		</li>
		<li>
			<button class="custom-menu-item JSexecute" data-execute='[{"table":"dobavljac_cenovnik", "column":"flag_aktivan", "val":"1"},{"table":"roba", "column":"flag_aktivan", "val":"1"}]'>{{ AdminLanguage::transAdmin('Prebaci u aktivne') }}</button>
		</li>
		<li>
			<button class="custom-menu-item JSexecute" data-execute='[{"table":"dobavljac_cenovnik", "column":"flag_aktivan", "val":"0"},{"table":"roba", "column":"flag_aktivan", "val":"0"}]'>{{ AdminLanguage::transAdmin('Prebaci u neaktivne') }}</button>
		</li>
		<li>
			<button class="custom-menu-item" id="JSRefresh" value="">{{ AdminLanguage::transAdmin('Osveži (F5)') }}</button>
		</li>
		<li>
			<button class="custom-menu-item" id="JSEditTeksta" value="">{{ AdminLanguage::transAdmin('Edit teksta') }}</button>
		</li>
		<li>
			<button class="custom-menu-item" id="JSimpDefMarza" value="">{{ AdminLanguage::transAdmin('Dodela definisanih marži') }}</button>
		</li>

	</ul>

	<section @if(Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE'))) class="webimport-table" @endif>
		<div class="flat-box">

			<div>
				<span>{{ AdminLanguage::transAdmin('Broj artikala') }}: <b>{{$count}}</b></span>
				|<span>{{ AdminLanguage::transAdmin('Po strani') }}: <b>{{$limit}}</b></span>
				|<b>{{ AdminLanguage::transAdmin('Selektovano') }}: <span id="JSselektovano">0</span></b>
			</div>

			<div class="table-scroll artiacle-table">
				<table class="dc_table fixed-table-header">
					<thead>
						<tr>
							<th id="but-col" class="table-head"></th>
							<th id="dob-col" class="table-head JSSort tooltipz" aria-label="{{ AdminLanguage::transAdmin('Dobavljač') }}" data-coll="dc.partner_id" data-sort="ASC">{{ AdminLanguage::transAdmin('Dob') }}</th>
							<th id="rid-col" class="table-head JSSort" data-coll="dc.roba_id" data-sort="ASC">{{ AdminLanguage::transAdmin('Roba id') }}</th>
							<!-- <th id="sku-col" class="table-head JSSort" data-coll="dc.sku" data-sort="ASC">Sku</th> -->
							<th id="sfr-col" class="table-head JSSort" data-coll="dc.sifra_kod_dobavljaca" data-sort="ASC">{{ AdminLanguage::transAdmin('Šifra') }}</th>

							<th id="sfr-col" class="table-head JSSort" data-coll="dc.barkod" data-sort="ASC">{{ AdminLanguage::transAdmin('Barkod') }}</th>

							<th id="akt-col" class="table-head JSSort tooltipz" aria-label="{{ AdminLanguage::transAdmin('Aktivan') }}" data-coll="dc.flag_aktivan" data-sort="ASC">{{ AdminLanguage::transAdmin('Akt') }}.</th>

							<th id="fpc-col" class="table-head JSSort" data-coll="dc.flag_prikazi_u_cenovniku" data-sort="ASC">{{ AdminLanguage::transAdmin('Web') }}</th>
							<th id="sl-col" class="table-head JSSort" data-coll="dc.flag_slika_postoji" data-sort="ASC">{{ AdminLanguage::transAdmin('Slika') }}</th>
							<th id="op-col" class="table-head JSSort" data-coll="dc.flag_opis_postoji" data-sort="ASC">{{ AdminLanguage::transAdmin('Opis') }}</th>
							<th id="naz-col" class="table-head JSSort" data-coll="dc.naziv" data-sort="ASC">{{ AdminLanguage::transAdmin('Naziv') }}</th>
							<th id="kol-col" class="table-head JSSort tooltipz" aria-label="{{ AdminLanguage::transAdmin('Količina') }}" data-coll="dc.kolicina" data-sort="ASC">{{ AdminLanguage::transAdmin('Kol') }}</th>
							<th id="wc-col" class="table-head JSSort tooltipz" aria-label="{{ AdminLanguage::transAdmin('Web Cena') }}" data-coll="dc.web_cena" data-sort="ASC">{{ AdminLanguage::transAdmin('WCen') }}</th>
							<th id="wm-col" class="table-head JSSort tooltipz" aria-label="{{ AdminLanguage::transAdmin('Web marža') }}" data-coll="dc.web_marza" data-sort="ASC">{{ AdminLanguage::transAdmin('WM') }}</th>
							<th id="nc-col" class="table-head JSSort tooltipz" aria-label="{{ AdminLanguage::transAdmin('Nabavna cena') }}" data-coll="dc.cena_nc" data-sort="ASC">{{ AdminLanguage::transAdmin('NC') }}</th>
							<th id="ncp-col" class="table-head tooltipz" aria-label="{{ AdminLanguage::transAdmin('Nabavna cena + PDV') }}">{{ AdminLanguage::transAdmin('NC+PDV') }}</th>
							<th id="mc-col" class="table-head JSSort tooltipz" aria-label="{{ AdminLanguage::transAdmin('Maloprodajna cena') }}" data-coll="dc.mpcena" data-sort="ASC">{{ AdminLanguage::transAdmin('MCen') }}</th>
							<th id="mpm-col" class="table-head JSSort tooltipz" aria-label="{{ AdminLanguage::transAdmin('Maloprodajna marža') }}" data-coll="dc.mp_marza" data-sort="ASC">{{ AdminLanguage::transAdmin('MPM') }}</th>
							<th id="pmc-col" class="table-head JSSort tooltipz" aria-label="{{ AdminLanguage::transAdmin('Preporučena Maloprodajna Cena') }}" data-coll="dc.pmp_cena" data-sort="ASC">{{ AdminLanguage::transAdmin('PMC') }}</th>
							<th id="pdv-col" class="table-head JSSort tooltipz" aria-label="{{ AdminLanguage::transAdmin('PDV') }}" data-coll="dc.tarifna_grupa_id" data-sort="ASC">{{ AdminLanguage::transAdmin('PDV') }}</th>
							<th id="gr-col" class="table-head JSSort" data-coll="dc.grupa" data-sort="ASC">{{ AdminLanguage::transAdmin('Grupa') }}</th>
							<th id="pgr-col" class="table-head JSSort" data-coll="dc.podgrupa" data-sort="ASC">{{ AdminLanguage::transAdmin('Podgrupa') }}</th>
							<th id="ngr-col" class="table-head JSSort" data-coll="dc.grupa_pr_id" data-sort="ASC">{{ AdminLanguage::transAdmin('Naša grupa') }}</th>
							<th id="pro-col" class="table-head JSSort" data-coll="dc.proizvodjac" data-sort="ASC">{{ AdminLanguage::transAdmin('Proizvođač') }}</th>
							<th id="npr-col" class="table-head JSSort" data-coll="dc.proizvodjac_id" data-sort="ASC">{{ AdminLanguage::transAdmin('Naš proizvođač') }}</th>
							<!-- <th id="mod-col" class="table-head JSSort" data-coll="dc.model" data-sort="ASC">Model</th> -->
							<th id="zak-col" class="table-head JSSort tooltipz" aria-label="{{ AdminLanguage::transAdmin('Zaključan') }}" data-coll="dc.flag_zakljucan" data-sort="ASC">{{ AdminLanguage::transAdmin('Zklj') }}</th>
						</tr>
					</thead>
					<tbody @if(Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE'))) id="selectable" @endif class="web-import-selectable">
						@if ($tabela != null)
						@foreach($tabela as $row)
						<tr class="ui-widget-content {{ AdminImport::rowColors($row->flag_aktivan,$row->povezan) }}" data-id="{{ $row->dobavljac_cenovnik_id  }}">
							<td class="but-col"><button class="btn btn-secondary edit-article-btn btn-circle small JSEditRow tooltipz-right" aria-label="{{ AdminLanguage::transAdmin('Izmeni') }}" data-sifra="{{ $row->sifra_kod_dobavljaca }}" data-zakljucan="{{ $row->flag_zakljucan }}"><i class="fa fa-pencil" aria-hidden="true"></i></button></td>

							<td class="dob-col">
								<span>{{ $row->dobavljac_naziv }}</span>
							</td>

							<td class="check-roba_id">{{ $row->roba_id }}</td>
						     <!--    <td class="sku-col">{{ $row->sku }}</td>
						     -->
						     <td class="sfr-col sifra">
						     	<span class="JSsifraVrednost">
						     		{{ $row->sifra_kod_dobavljaca }}
						     	</span>
						     	<span class="JSEditSifraBtnSpan">
						     		<input type="text" class="JSsifraInput" value="{{ htmlentities($row->sifra_kod_dobavljaca) }}" />
						     		@if(Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')))
						     		<i class="fa fa-pencil JSEditSifraBtn" aria-hidden="true"></i>
						     		@endif
						     	</span>
						     </td>

						     <td>
						     	{{$row->barkod}}
						     </td>

						     <td class="flag_aktivan"><span>{{ $row->flag_aktivan == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}</span></td>

						     <td class="flag_prikazi_u_cenovniku"><span>{{ $row->flag_prikazi_u_cenovniku == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}</span></td>

						     <td class="flag_slika_postoji sl-col">
						     	<span>  {{ $row->flag_slika_postoji == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}</span>
						     </td>

						     <td class="flag_opis_postoji sl-col">
						     	<span>	{{ $row->flag_opis_postoji == 1 ? '<i class="fa fa-check green" aria-hidden="true"></i>' : '<i class="fa fa-times red" aria-hidden="true"></i>' }}
						     	</span>
						     </td>

						     <td class="table-fixed-width naziv text-left" style="max-width: {{$naziv_width}}px;width: {{$naziv_width}}px;">
						     	<span class="JSnazivVrednost">
						     		{{ $row->naziv }}
						     	</span>
						     	<span class="JSEditNazivBtnSpan">
						     		<input type="text" class="JSnazivInput" value="{{ htmlentities($row->naziv) }}" />
						     		@if(Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')))
						     		<i class="fa fa-pencil JSEditNazivBtn" aria-hidden="true"></i>
						     		@endif
						     	</span>
						     </td>

						     <td class="kolicina">
						     	<span class="JSkolicinaVrednost">{{ isset($row->kolicina) ? intval($row->kolicina) : '' }}</span>
						     	<span class="JSEditKolicinaBtnSpan">
						     		<input type="number" class="JSkolicinaInput" value="{{ isset($row->kolicina) ? intval($row->kolicina) : '' }}" />
						     		@if(Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')))
						     		<i class="fa fa-pencil JSEditKolicinaBtn" aria-hidden="true"></i>
						     		@endif
						     	</span>
						     </td>				

						     <td class="cena_web_marza"><span>{{ round($row->web_cena).'.00' }}</span></td>
						     <td class="web_marza"><span>{{ $row->web_marza }}</span></td>
						     <td class="cena_nc"><span>{{ round($row->cena_nc).'.00' }}</span></td>
						     <td class="cena_nc_pdv"><span>{{ round(($row->cena_nc*(1+$row->tarifna_grupa_porez/100))).'.00' }}</span></td>
						     <td class="cena_mp_marza"><span>{{ round($row->mpcena).'.00' }}</span></td>
						     <td class="mp_marza"><span>{{ round($row->mp_marza).'.00' }}</span></td>
						     <td class="pmc-col"><span>{{ round($row->pmp_cena).'.00' }}</span></td>
						     <td class="tarifna_grupa_id"><span>{{ $row->tarifna_grupa_porez }}</span></td>
						     <td class="gr-col"><span>{{ $row->grupa }}</span></td>
						     <td class="table-fixed-width-small pgr-col"><span>{{ $row->podgrupa }}</span></td>
						     <td class="grupa_pr_id"><span>{{ $row->nasa_grupa }}</span></td>
						     <td class="pro-col"><span>{{ $row->proizvodjac }}</span></td>
						     <td class="proizvodjac_id"><span>{{ $row->nas_proizvodjac }}</span></td>
						     <!-- <td class="model">{{ $row->model }}</td> -->
						     <td class="flag_zakljucan"><span>{{ $row->flag_zakljucan == 1 ? AdminLanguage::transAdmin('DA') : AdminLanguage::transAdmin('NE') }}</span></td>
						 </tr>
						 @endforeach
						 @endif		   
						</tbody>
					</table>
				</div> 

				<div class="row"> 
					{{ is_array($tabela) ? Paginator::make($tabela, $count, $limit)->links() : '' }} 
				</div>

				<div class="table-scroll second-table-import artiacle-table">
					<table>
						<thead>
							<tr class="table-fixed-header__tr">
								<th class=""></th>
								<th class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('Dobavljač') }}">{{ AdminLanguage::transAdmin('Dob') }}</th>
								<th class="">{{ AdminLanguage::transAdmin('Roba_id') }}</th>
								<th class="">{{ AdminLanguage::transAdmin('Šifra') }}</th>
								<th class="">{{ AdminLanguage::transAdmin('Barkod') }}</th>
								<th class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('Aktivan') }}">{{ AdminLanguage::transAdmin('Akt') }}.</th>
								<th class="">{{ AdminLanguage::transAdmin('Web') }}</th>
								<th height="20" class=" naziv-fix">{{ AdminLanguage::transAdmin('Naziv') }}</th>
								<th class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('Količina') }}">{{ AdminLanguage::transAdmin('Kol') }}</th>
								<th class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('Nabavna cena') }}">{{ AdminLanguage::transAdmin('NC') }}</th>
								<th class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('Web marža') }}">{{ AdminLanguage::transAdmin('WM') }}</th>
								<th class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('Web cena') }}">{{ AdminLanguage::transAdmin('WCen') }}</th>
								<th class="">{{ AdminLanguage::transAdmin('Naša grupa') }}</th>
								<th class="">{{ AdminLanguage::transAdmin('Naš proizvođač') }}</th>		      		 
							</tr>
						</thead>
						<tbody id="JSRobaList">
							@foreach($roba as $row)
							<tr class="JSRobaRow" data-id="{{ $row->roba_id  }}">
								<td class="">
									<a target="_blank" class="" href="{{AdminOptions::base_url()}}admin/product/{{ $row->roba_id }}">
										<button class="btn btn-secondary btn-xm edit-article-btn btn-circle small tooltipz-right" aria-label="{{ AdminLanguage::transAdmin('Izmeni') }}">
											<i class="fa fa-pencil" aria-hidden="true"></i>
										</button>
									</a>
								</td>
								<td class="dobavljac">{{ AdminSupport::dobavljac($row->dobavljac_id) }}</td>
								<td>{{ $row->roba_id }}</td>
								<td>{{ $row->sifra_d }}</td>
								<td>{{ $row->barkod }}</td>
								<td class="flag_aktivan">{{ $row->flag_aktivan == 1 ? AdminLanguage::transAdmin('DA') : AdminLanguage::transAdmin('NE') }}</td>
								<td class="flag_prikazi_u_cenovniku">{{ $row->flag_prikazi_u_cenovniku == 1 ? AdminLanguage::transAdmin('DA') : AdminLanguage::transAdmin('NE') }}</td>
								<td class="roba_naziv">{{ $row->naziv }}</td>
								<td class="kolicina">{{ isset($row->kolicina) ? intval($row->kolicina) : '' }}</td>	
								<td class="racunska_cena_nc">{{ $row->racunska_cena_nc }}</td>
								<td class="web_marza">{{ $row->web_marza }}</td>
								<td class="web_cena">{{ $row->web_cena }}</td>
								<td>{{ AdminArticles::findGrupe($row->grupa_pr_id,'grupa') }}</td>
								<td>{{ AdminSupport::find_proizvodjac($row->proizvodjac_id,'naziv') }}</td>
							</tr>
							@endforeach
							@if($criteriaImport['grupa_pr_id'] || $criteriaImport['proizvodjac'] || $criteriaImport['dobavljac'] || $criteriaImport['search'])
							<tr>
								<td colspan="19">
									<div class="btn-container center no-margin">
										<button id="JSMore" data-page="2" class="btn btn-small btn-secondary btn-block">{{ AdminLanguage::transAdmin('VIŠE') }}</button>
									</div>
								</td>
							</tr>
							@endif
						</tbody>
					</table>
				</div> 
			</div>
		</section> 
	</section>

	<div id="wait">
		<p><img class="loader" src="{{ AdminOptions::base_url()}}images/admin/wheel.gif">{{ AdminLanguage::transAdmin('Molimo vas sačekajte') }}...</p>
	</div>

	<div id="JSEditFieldsModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Izmeni polja') }}</h4><br>
		<input type="hidden" id="JSEditDCId" class="JSdcid">
		<input type="hidden" id="JSEditPorez" class="JSporez">
		<div class="columns medium-12 large-12">
			<div class="row">
				<div class="columns medium-12 large-12">					
					<label>{{ AdminLanguage::transAdmin('Naziv') }}</label>
					<input type="text" id="JSEditNaziv" {{ Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')) == false ? 'readonly' : '' }}>
				</div>
			</div>
			<div class="row">
				<div class="columns medium-4 large-4">
					<label>{{ AdminLanguage::transAdmin('Količina') }}</label>
					<input type="text" id="JSEditKolicina" {{ Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')) == false ? 'readonly' : '' }}>
				</div>
				<div class="columns medium-4 large-4">
					<label>{{ AdminLanguage::transAdmin('Web marža') }}</label>
					<input type="text" id="JSEditWM" {{ Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')) == false ? 'readonly' : '' }}>
					@if(Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')))
					<span class="JSEditWMbtn btn  btn-primary btn-small-check"><i class="fa fa-check" aria-hidden="true"></i></span>
					@endif
				</div>
				<div class="columns medium-4 large-4">
					<label>{{ AdminLanguage::transAdmin('MP marža') }}</label>
					<input type="text" id="JSEditMM" {{ Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')) == false ? 'readonly' : '' }}>
					@if(Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')))
					<span class="JSEditMMbtn btn  btn-primary btn-small-check"><i class="fa fa-check" aria-hidden="true"></i></span>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="columns medium-4 large-4">
					<label>{{ AdminLanguage::transAdmin('Nabavna cena') }}</label>
					<input type="text" id="JSEditNC" {{ Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')) == false ? 'readonly' : '' }}>
					@if(Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')))
					<span class="JSEditNCbtn btn  btn-primary btn-small-check"><i class="fa fa-check" aria-hidden="true"></i></span>
					@endif
				</div>
				<div class="columns medium-4 large-4">
					<label>{{ AdminLanguage::transAdmin('Web cena') }}</label>
					<input type="text" id="JSEditWC" {{ Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')) == false ? 'readonly' : '' }}>
					@if(Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')))
					<span class="JSEditWCbtn btn  btn-primary btn-small-check"><i class="fa fa-check" aria-hidden="true"></i></span>
					@endif
				</div>
				<div class="columns medium-4 large-4">
					<label>{{ AdminLanguage::transAdmin('MP cena') }}</label>
					<input type="text" id="JSEditMC" {{ Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')) == false ? 'readonly' : '' }}>
					@if(Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')))
					<span class="JSEditMCbtn btn  btn-primary btn-small-check"><i class="fa fa-check" aria-hidden="true"></i></span>
					@endif
				</div>
			</div>
			<div class="row">
				<div class="columns medium-12 large-12">	
					<div class="edit-modl-div">			
						<span>{{ AdminLanguage::transAdmin('Slike') }}</span>						
						<div id="JSDobSlike"></div>
					</div>	
				</div>
			</div>

			<div class="row">
				<div class="columns medium-4 large-4">					
					<label>{{ AdminLanguage::transAdmin('Opis') }}</label>
					<textarea type="text" id="JSOpis" {{ Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')) == false ? 'readonly' : '' }}></textarea>
				</div>
			</div>

			@if(Admin_model::check_admin(array('WEB_IMPORT_AZURIRANJE')))
			<div class="row">
				<div class="columns medium-12 large-12 text-center btn-container">
					<button id="submitBtn" class="btn btn-primary save-it-btn"> {{ AdminLanguage::transAdmin('Sačuvaj') }}</button>


				</div>
			</div>
			@endif
		</div>
	</div>

	<div id="JSPrihvatiLagerModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Prihvati lager') }}</h4><br>
		<div class="columns medium-12 large-12">
			<div class="row">
				<div class="columns medium-8 large-8">
					<select class=" admin-select m-input-and-button__input" id="magacin_select">
						@foreach(DB::table('orgj')->where('b2c',1)->orderBy('orgj_id','asc')->get() as $row)
						@if($row->orgj_id == DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'))
						<option value="{{ $row->orgj_id }}" selected>{{ $row->naziv }}</option>
						@else
						<option value="{{ $row->orgj_id }}">{{ $row->naziv }}</option>
						@endif
						@endforeach
					</select>
				</div>
				<div class="columns medium-4 large-4">					
					<button id="" class="btn  btn-primary JSPrihvatiLager"><i class="fa fa-check" aria-hidden="true"></i>{{ AdminLanguage::transAdmin(' Prihvati lager') }}</button>
				</div>
			</div>
		</div>
	</div>

	<div id="JSPrihvatiCeneModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Prihvati cene i naziv') }}</h4><br>
		<div class="columns medium-12 large-12">
			<div class="row">
				<div>{{ AdminLanguage::transAdmin('Primarni artikl kod povezanih artikala') }}</div>
				<div class="columns medium-3 large-3">
					<select class=" admin-select m-input-and-button__input" id="JSPrimaryPartner">
						<option value="0">{{ AdminLanguage::transAdmin('Izaberi dobavljaca') }}</option>
						@foreach(AdminImport::dobavljaci_web_import_tabela() as $dobavljac)
						<option value="{{ $dobavljac->partner_id }}">{{ $dobavljac->naziv }}</option>
						@endforeach
					</select>
				</div>
				<div class="columns medium-2 large-2">
					<input type="checkbox" name="kolicina" id="kolicina" checked>
					{{ AdminLanguage::transAdmin('Sa kolicinom') }}
				</div>
				<div class="columns medium-2 large-2">
					<input type="checkbox" name="cena" id="cena" checked>
					{{ AdminLanguage::transAdmin('Sa najnizom cenom') }}
				</div>
				<div class="columns medium-2 large-2">					
					<button class="btn  btn-primary JSPrihvatiCeneNaziv" data-naziv="0"><i class="fa fa-check" aria-hidden="true"></i>{{ AdminLanguage::transAdmin('Prihvati cene') }}</button>
				</div>
				<div class="columns medium-2 large-2">					
					<button class="btn  btn-primary JSPrihvatiCeneNaziv" data-naziv="1"><i class="fa fa-check" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Prihvati naziv') }}</button>
				</div>
			</div>
		</div>
	</div>

	<div id="JSKarakSlikeModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Preuzmi dodatne karakteristike i slike') }}</h4><br>
		<div class="columns medium-12 large-12">
			<div class="row">
				<div>{{ AdminLanguage::transAdmin('Primarni artikl kod povezanih artikala') }}</div>
				<div class="columns medium-8 large-8">
					<select class=" admin-select m-input-and-button__input" id="JSPrPartnerId">
						<option value="0">{{ AdminLanguage::transAdmin('Izaberi dobavljaca') }}</option>
						@foreach(AdminImport::dobavljaci_web_import_tabela() as $dobavljac)
						<option value="{{ $dobavljac->partner_id }}">{{ $dobavljac->naziv }}</option>
						@endforeach
					</select>
				</div>
				<div class="columns medium-4 large-4">					
					<button class="btn  btn-primary JSKarakSlike" data-naziv="0"><i class="fa fa-check" aria-hidden="true"></i> {{ AdminLanguage::transAdmin('Preuzmi') }}</button>
				</div>
			</div>
		</div>
	</div>

	<div id="JSEditModal" class="reveal-modal sm-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Izmena artikla') }}</h4><br>
		<div class="column medium-12 large-12 text-center">
			{{ AdminLanguage::transAdmin('Artikal je zaključan! Da li želite da otključate artikal') }}?
		</div>
		<div class="column medium-12 large-12 text-center">
			<button id="JSEditYes" class="btn btn-primary m-input-and-button__btn">{{ AdminLanguage::transAdmin('DA') }}</button>
			<button id="JSEditNo" class="btn btn-danger m-input-and-button__btn">{{ AdminLanguage::transAdmin('NE') }}</button>
		</div>
	</div>

	<div id="JStextEditModal" class="reveal-modal sm-modal txt-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		<h4 class="modal-title-h4">{{ AdminLanguage::transAdmin('Edit teksta') }}</h4>

		<div class="row">
			<div class="columns medium-5 text-right">
				<input type="radio" id="JStextEditNaziv" name="naziv-opis" value="naziv" checked />{{ AdminLanguage::transAdmin('Naziv') }}
			</div>
			<div class="columns medium-6 center">
				<input type="radio" id="JStextEditOpis" name="naziv-opis" value="opis">{{ AdminLanguage::transAdmin('Opis') }}
			</div>
		</div> 

		<h3 class="title-txt">{{ AdminLanguage::transAdmin('Dodavanje teksta') }}</h3>

		<div class="row"> 
			<div class="columns medium-6 center">
				<input type="radio" id="JStextEditStart" name="start-end" value="start" checked /><label>{{ AdminLanguage::transAdmin('Na početku') }}</label> 
				<input type="radio" id="JStextEditEnd" name="start-end" value="end"><label>{{ AdminLanguage::transAdmin('Na kraju') }}</label>	
			</div>
			<div class="columns medium-6">	  
				<input type="text" id="JStextEditNazivInput" class="m-input-and-button__input"  placeholder="{{ AdminLanguage::transAdmin('Tekst') }}">
				<button id="JStextInsertButton" class="btn btn-primary btn-abs m-input-and-button__btn">{{ AdminLanguage::transAdmin('Dodaj') }}</button>
			</div>
		</div> 
		<br>

		<h3 class="title-txt">{{ AdminLanguage::transAdmin('Zameni tekst') }}</h3>

		<div class="row"> 
			<div class="columns medium-12">
				<div class="row"> 
					<div class="columns medium-6 center">
						<input type="radio" id="JStextEditDeo" name="ceo-deo" value="deoTeksta" checked /><label>{{ AdminLanguage::transAdmin('Deo teksta') }}</label> 
						<input type="radio" id="JStextEditCelo" name="ceo-deo" value="celaRec"><label>{{ AdminLanguage::transAdmin('Cela reč') }}</label>
					</div>
				</div>
				<div class="row"> <br>
					<div class="columns medium-6">
						<input type="text" name="JStextEditInput1" id="JStextEditInput1" class="m-input-and-button__input" placeholder="{{ AdminLanguage::transAdmin('Tekst koji se menja') }}">
					</div>
					<div class="columns medium-6"> 
						<input type="text" name="JStextEditInput2" id="JStextEditInput2" class="m-input-and-button__input" placeholder="{{ AdminLanguage::transAdmin('Novi tekst') }}">
						<button id="JStextReplaceButton" class="btn btn-primary btn-abs m-input-and-button__btn">{{ AdminLanguage::transAdmin('Zameni') }}</button>
					</div> 
				</div>
			</div>
		</div>
		<br>
		<label id="Results"></label>
	</div>