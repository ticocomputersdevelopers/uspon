<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5TRTLQM');
	</script>
@if(Config::get('app.livemode'))
	   	
<script>
				$('.JSadd-to-cart').on('click', function() {
        		var roba_id_tag = $(this).attr('roba_id');
        		console.log($(this));
        	});
        @if(Session::has('success_add_to_cart'))
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
              "event": "add_to_cart",
              "ecommerce": {
                "currency": "{{ Articles::get_valuta() }}",
                "add": {
                  "items": [{
                    "name": "{{ addslashes(str_replace("'","",$title)) }}",
                    "content_type": "product_group",
                    "item_group_id": parseInt("{{ ($category = Product::get_main_category($roba_id)) ? $category->grupa_pr_id : '' }}"),
                    "id": parseInt("{{ $roba_id }}"),
                    "value": "{{ Product::get_price($roba_id) }}",
                    "brand": "{{ addslashes(str_replace("'","",Product::get_proizvodjac_name($roba_id))) }}",
                    "category": "{{ ($category = Product::get_main_category($roba_id)) ? $category->grupa : 'None' }}",
                    "variant": "Standard",
                    "quantity": "{{ Session::get('success_add_to_cart')['quantity'] }}"
                   }]
                }
              },
              'eventCallback': function() {
          
              }
            });
        @endif
    
    @if($strana == 'artikal')
       
            window.dataLayer = window.dataLayer || [];
            window.dataLayer.push({
              "event": "view_item",
              "ecommerce": {                
                "view_item": {
                  'actionField': {'list': 'Search Results'},
                  "currency": "{{ Articles::get_valuta() }}",
                  "items": [{
                    "name": "{{ addslashes(str_replace("'","",$title)) }}",   
                    "id": parseInt("{{ $roba_id }}"),
                    "value": "{{ Product::get_price($roba_id) }}",
                    "brand": "{{ addslashes(str_replace("'","",Product::get_proizvodjac_name($roba_id))) }}",
                    "category": "{{ ($category = Product::get_main_category($roba_id)) ? $category->grupa : 'None' }}",
                    "variant": "Standard",
                    "position": 1
                   }]
                }
              },
              'eventCallback': function() {
          
              }
            });
        
    @endif 
    @if(Session::has('add_to_cart') )
		// alert(123123)
	    var productObjects = $.parseJSON('<?php echo Session::get('add_to_cart'); ?>');	
	    console.log(productObjects)
	    var items = [];
		for (var i = 0; i < productObjects.length; i++) {
			items.push({
	  			"name": productObjects[i].name,
	  			"id": productObjects[i].id,
	  			"value": productObjects[i].price,
	  			"brand": productObjects[i].brand,
	  			"category": productObjects[i].category,
	  			"variant": "Standard",
	  	        "quantity": productObjects[i].quantity
			});
		}
	
		  window.dataLayer = window.dataLayer || [];
		  window.dataLayer.push({
		    "event": "add_to_cart",
		    "ecommerce": {
		      "add": {		        
		        "items": items
		     }
		   }
		  }); 
		   <?php Session::forget('add_to_cart'); 
		   		Session::regenerate();
		   ?>
	          
	@endif
    //pojedinacno brisanje artikla 
    @if(Session::has('cart_remove_item') )
		
	    var productObject = $.parseJSON('<?php echo Session::get('cart_remove_item'); ?>');			
		  window.dataLayer = window.dataLayer || [];
		  window.dataLayer.push({
		    "event": "remove_from_cart",
		    "ecommerce": {
		      "remove": {		        
		        "items": [productObject]
		     }
		   }
		  }); 
		   <?php Session::forget('cart_remove_item'); 
		   		Session::regenerate();
		   ?>
	          
	@endif

	@if(Session::has('user_login') )
		
	    var productObject = $.parseJSON('<?php echo Session::get('user_login'); ?>');			
		  window.dataLayer = window.dataLayer || [];
		  window.dataLayer.push({
		    "event": "login",
		    "ecommerce": "Shop login"
		  }); 
		   <?php Session::forget('user_login'); 
		   		Session::regenerate();
		   ?>
	          
	@endif

	@if(Session::has('user_sign_up') )
		
	    var productObject = $.parseJSON('<?php echo Session::get('user_sign_up'); ?>');			
		  window.dataLayer = window.dataLayer || [];
		  window.dataLayer.push({
		    "event": "sign_up",
		    "ecommerce": "Shop sign up"
		  }); 
		   <?php Session::forget('user_sign_up'); 
		   		Session::regenerate();
		   ?>
	          
	@endif

	// grupno brisanje artikala
	@if(Session::has('cart_remove_items') AND $strana == 'korpa')

		var productObjects = $.parseJSON('<?php echo Session::get('cart_remove_items'); ?>');
		  window.dataLayer = window.dataLayer || [];
		  window.dataLayer.push({
		    "event": "remove_from_cart",
		    "ecommerce": {
		      "remove": {		        
		        "items": [productObjects]
		     }
		   }
		  }); 
	   	<?php Session::forget('cart_remove_items'); 
	   		Session::regenerate();
	   	?>		

	@endif
	

	@if($strana == 'order' and count($articles_details) > 0)

    	var productObjects = $.parseJSON('<?php echo json_encode($articles_details); ?>');
		var products = [];
		for (var i = 0; i < productObjects.length; i++) {
			products.push({
	  			"name": productObjects[i].name,
	  			"id": productObjects[i].id,
	  			"value": productObjects[i].price,
	  			"brand": productObjects[i].brand,
	  			"category": productObjects[i].category,
	  			"variant": "Standard",
	  	        "quantity": productObjects[i].quantity
			});
		}

		  window.dataLayer = window.dataLayer || [];
		  window.dataLayer.push({
		    "event": "purchase",
		    "ecommerce": {
              "currency": "{{ Articles::get_valuta() }}",
		      "purchase": {
			      "actionField": {
			        "transaction_id": "{{ Order::broj_dokumenta($web_b2c_narudzbina_id) }}",
			        "affiliation": "{{Options::company_name()}}",
			        "value": "{{ Order::narudzbina_ukupno($web_b2c_narudzbina_id) + ((Options::checkTroskoskovi_isporuke() == 1 AND Options::checkTezina() == 0 AND Order::narudzbina_ukupno($web_b2c_narudzbina_id) < Cart::cena_do() AND Cart::cena_do() > 0) ? Cart::cena_dostave() : 0) }}",
			        "shipping": 0
			      },
		          "items": products
		     }
		   }
		  });
	@endif

	@if($strana == 'korpa' and count($articles_details) > 0)

    var productObjects = $.parseJSON('<?php echo json_encode($articles_details); ?>');
		var products = [];
		<?php $troskovi = Cart::troskovi(); ?>
		for (var i = 0; i < productObjects.length; i++) {
			products.push({
	  			"name": productObjects[i].name,
	  			"id": productObjects[i].id,
	  			"value": productObjects[i].price,
	  			"brand": productObjects[i].brand,
	  			"category": productObjects[i].category,
	  			"variant": "Standard",
	  	        "quantity": productObjects[i].quantity
			});
		}

		  window.dataLayer = window.dataLayer || [];
		  window.dataLayer.push({
		    "event": "begin_checkout",
		    "ecommerce": {
              "currency": "{{ Articles::get_valuta() }}",
		      "begin_checkout": {
			      "actionField": {
			        "value": "{{ Cart::cena(Cart::cart_ukupno()+$troskovi) }}"
			      },
		          "items": products
		     }
		   }
		  });
	@endif
</script>

@endif

