@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<br>
<h2><span class="section-title">{{ Language::trans('Izmeni podatke naloga') }}</span></h2>

<br>

<div class="user-info-form clearfix">
	<form action="{{ Options::base_url()}}korisnik-edit" method="post" class="login-form" autocomplete="off">
		<!-- PERSONAL INFO CHANGE -->
		<div class="row">
		@if($web_kupac->flag_vrsta_kupca == 0)
			<div class="col-md-3 col-sm-4 col-xs-12 form-group">
				<label for="name">{{ Language::trans('Ime') }}</label>
				<input name="ime" type="text" value="{{ Input::old('ime') ? Input::old('ime') : $web_kupac->ime }}">
				<div class="error red-dot-error">{{ $errors->first('ime') ? $errors->first('ime') : "" }}</div>
			</div>
			
			<div class="col-md-3 col-sm-4 col-xs-12 form-group">
				<label for="surname">{{ Language::trans('Prezime') }}</label>
				<input name="prezime" type="text" value="{{ Input::old('prezime') ? Input::old('prezime') : $web_kupac->prezime }}">
				<div class="error red-dot-error">{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}</div>
			</div>
	  
		<!-- COMPANY INFO CHANGE -->
			@elseif($web_kupac->flag_vrsta_kupca == 1) 
				<div class="col-md-3 col-sm-4 col-xs-12 form-group">
					<label for="company-name">{{ Language::trans('Naziv firme') }}</label>
					<input name="naziv" type="text" value="{{ Input::old('naziv') ? Input::old('naziv') : $web_kupac->naziv }}">
					<div class="error red-dot-error">{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}</div>
				</div>
				
				<div class="col-md-3 col-sm-4 col-xs-12 form-group">
					<label for="pib">{{ Language::trans('PIB') }}</label>
					<input name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : $web_kupac->pib }}">
					<div class="error red-dot-error">{{ $errors->first('pib') ? $errors->first('pib') : "" }}</div>
				</div> 
			@endif
	 
			<div class="col-md-3 col-sm-4 col-xs-12 form-group">
				<label for="e-mail">{{ Language::trans('E-mail') }}</label>
				<input name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : $web_kupac->email }}">
				<div class="error red-dot-error">{{ $errors->first('email') ? $errors->first('email') : "" }}</div>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-12 form-group">
				<label for="lozinka_user">{{ Language::trans('Lozinka') }}</label>
				<input name="lozinka" type="password" value="{{ Input::old('lozinka') ? Input::old('lozinka') : base64_decode($web_kupac->lozinka) }}">
				<div class="error red-dot-error">{{ $errors->first('lozinka') ? $errors->first('lozinka') : "" }}</div>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-12 form-group">
				<label for="telefon">{{ Language::trans('Telefon') }}</label>
				<input name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : $web_kupac->telefon }}">
				<div class="error red-dot-error">{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}</div>
			</div>	
 
			<div class="col-md-3 col-sm-4 col-xs-12 form-group">
				<label for="address">{{ Language::trans('Adresa') }}</label>
				<input name="adresa" type="text" value="{{ Input::old('adresa') ? Input::old('adresa') : $web_kupac->adresa }}">
				<div class="error red-dot-error">{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}</div>
			</div>
			<div class="col-md-3 col-sm-4 col-xs-12 form-group">
				<label for="mesto">{{ Language::trans('Mesto') }}</label>
				<input name="mesto" type="text" value="{{ Input::old('mesto') ? Input::old('mesto') : $web_kupac->mesto }}">
				<div class="error red-dot-error">{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}</div>
			</div>
		</div>
 
		<div class="text-center">  
			<input type="hidden" name="web_kupac_id" value="{{$web_kupac->web_kupac_id}}" />
			<input type="hidden" name="flag_vrsta_kupca" value="{{$web_kupac->flag_vrsta_kupca}}" />
			<button type="submit" class="button">{{ Language::trans('Izmeni') }}</button> 
		</div>
	</form>
	@if(Session::get('message'))

	<div class="row"> 
		<div class="success bg-success col-md-12 col-sm-12 col-xs-12">{{ Language::trans('Vaši podaci su uspešno sačuvani') }}!</div>
	</div>
	@endif
</div>

<br>

@if(count(All::getNarudzbine($web_kupac->web_kupac_id)) > 0)
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="user-orders-table table table-hover table-condensed">
				<tr>
					<th>{{ Language::trans('Broj narudž.') }}</th>
					<th>{{ Language::trans('Datum') }}</th>
					<th>{{ Language::trans('Iznos') }}</th>
					<th>{{ Language::trans('Status') }}</th>
					<th>{{ Language::trans('Vidi narudžbinu') }}</th>
				</tr>
		 
				@foreach(All::getNarudzbine($web_kupac->web_kupac_id) as $row)
				<tr>	
					<td>{{ $row->broj_dokumenta }}</td>
					<td>{{ $row->datum_dokumenta }}</td>
					<td>{{Cart::cena(Order::narudzbina_ukupno($row->web_b2c_narudzbina_id))}}</td>
					<td>{{Order::narudzbina_status_active($row->web_b2c_narudzbina_id)}}</td>
					<td>
						<button type="button" class="user-order-button" data-toggle="modal" data-target="#order-modal{{ $row->web_b2c_narudzbina_id }}"><i class="fa fa-plus" aria-hidden="true"></i></button>
					</td>
				</tr>
				@endforeach
	 
			</table>
		</div>
		<!-- Modal -->
		@foreach(All::getNarudzbine($web_kupac->web_kupac_id) as $row)
		<div class="modal fade" id="order-modal{{ $row->web_b2c_narudzbina_id }}" role="dialog">
			<div class="modal-dialog modal-lg">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
						<h4 class="modal-title text-center">{{ Language::trans('Vaše narudžbine') }}</h4>
					</div>
					<div class="modal-body">
						<div class="table-responsive">
							<table class="table table-hover table-condensed">
								<tr>
									<th>{{ Language::trans('Naziv proizvoda') }}</th>
									<th>{{ Language::trans('Cena') }}</th>
									<th>{{ Language::trans('Količina') }}</th>
									<th>{{ Language::trans('Ukupna cena') }}</th>
								</tr>
								<tr> 
									@foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$row->web_b2c_narudzbina_id)->get() as $row2)
									<td>{{ Product::short_title($row2->roba_id) }} {{Product::getOsobineStrNonActv($row2->roba_id,$row2->osobina_vrednost_ids)}}</td>
									<td>{{ Cart::cena($row2->jm_cena) }}</td>
									<td>{{ (int)$row2->kolicina }}</td>
									<td>{{ Cart::cena($row2->kolicina*$row2->jm_cena) }}</td>
								</tr>
								@endforeach
								<tr>
									<td colspan="3"></td>
									<td class="summary"><b> {{ Language::trans('Ukupno') }}:{{ Cart::cena(Order::narudzbina_ukupno($row->web_b2c_narudzbina_id)) }}</b></td> 
								</tr>					 
							</table>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="button" data-dismiss="modal">{{ Language::trans('Zatvori') }}</button>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
@endif

@if(count(All::getWishIds($web_kupac->web_kupac_id)) > 0)

<h2><span class="section-title">{{ Language::trans('Lista želja') }}</span></h2> 

@foreach(All::getWishIds($web_kupac->web_kupac_id) as $row)
<div class="JSproduct col-md-3 col-sm-4 col-xs-12 no-padding"> 
	<div class="shop-product-card relative">  

		<div class="product-image-wrapper relative">

			<a class="flex" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">
				<img class="product-image img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
			</a>

			<div class="add-to-cart-container">   
				@if(Product::getStatusArticle($row->roba_id) == 1)	
				@if(Cart::check_avaliable($row->roba_id) > 0)	 
				<button class="buy-btn JSadd-to-cart" data-roba_id="{{$row->roba_id}}">{{ Language::trans('U korpu') }}</button>  			    
				@else  	 
				<button class="not-available"> {{ Language::trans('Nije dostupno') }}</button>   
				@endif 

				@else  
				<button class="buy-btn">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>
				@endif 	

				@if(Product::getStatusArticle($row->roba_id) == 1)	
					@if(Cart::check_avaliable($row->roba_id) > 0)	 
						<button class="JSukloni button" data-roba_id="{{$row->roba_id}}">{{Language::trans('Ukloni')}} <span class="fas fa-trash-alt"></span></button>
					@else
						<button class="JSukloni button" data-roba_id="{{$row->roba_id}}">{{Language::trans('Ukloni')}} <span class="fas fa-trash-alt"></span></button>
					@endif
					@else
						<button class="JSukloni button" data-roba_id="{{$row->roba_id}}">{{Language::trans('Ukloni')}} <span class="fas fa-trash-alt"></span></button>
				@endif 	
			</div>  

		</div>

		<div class="product-meta"> 

			<h2 class="product-name"> 
				<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">{{ Product::short_title($row->roba_id) }}</a>
			</h2>

			<div class="price-holder">
				<div> {{ Cart::cena(Product::get_price($row->roba_id)) }} </div>
				@if(All::provera_akcija($row->roba_id))
				<span class="product-old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
				@endif 
			</div>	  

		</div> 
	</div>
</div>

@endforeach
@else

<h2><span class="section-title">{{ Language::trans('Lista želja je prazna') }}</span></h2>

@endif
@endsection

