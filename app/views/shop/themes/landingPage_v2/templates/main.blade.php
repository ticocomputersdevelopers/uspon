<!DOCTYPE html>
<html lang="sr"> 
	<head>
		@include('shop/themes/'.Support::theme_path().'partials/head') 
		<script type="application/ld+json">
		@if($strana == All::get_page_start())
			{ 
				"@context" : "http://schema.org",
				"@type" : "WebSite",
				"name" : "<?php echo Options::company_name(); ?>",
				"alternateName" : "<?php echo $title; ?>",
				"url" : "<?php echo Options::domain(); ?>",
				"publisher": {
			        "@type": "Organization",
			        "name": "<?php echo Options::company_name(); ?>"
			    }
			}
		@else
			{
			    "@context": "http://schema.org",
			    "@type": "WebPage",
			    "name": "<?php echo $title; ?>",
			    "description": "<?php echo $description; ?>",
			    "url" : "<?php echo Options::base_url().$org_strana; ?>",
			    "publisher": {
			        "@type": "Organization",
			        "name": "<?php echo Options::company_name(); ?>"
			    }
			}
		@endif	
		</script> 
	</head>
<body @if($strana == All::get_page_start()) id="start-page" @endif 
    @if(Support::getBGimg() != null) 
        style="background-image: url({{Options::domain()}}{{Support::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" 
    @endif> 
       
 
	@include('shop/themes/'.Support::theme_path().'partials/header') 

	<!-- MAIN.blade -->
    <div class="d-content relative"> 

    	<!-- FOR SEO -->
 	  	<h1 class="seo"> {{ $title }} </h1>

		@yield('page')  

 	</div>
	<!-- MAIN.blade END -->
 
 
    @include('shop/themes/'.Support::theme_path().'partials/footer')

	@include('shop/themes/'.Support::theme_path().'partials/popups')


	<!-- BASE REFACTORING -->
	<input type="hidden" id="base_url" value="{{Options::base_url()}}" /> 
   
 	<script src="{{Options::domain()}}js/slick.min.js"></script> 
 
 	@if(Session::has('b2c_admin'.Options::server()))
	<script src="{{Options::domain()}}js/tinymce_5.1.3/tinymce.min.js"></script> 
	<script src="{{Options::domain()}}js/shop/front_admin/main.js"></script> 
	@endif
	
	<script src="{{Options::domain()}}js/shop/translator.js"></script>
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script> 
	<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script> 

	@if($strana==Seo::get_kontakt())
	<input type="hidden" id="lat" value="{{ All::lat_long()[0] }}" />
	<input type="hidden" id="long" value="{{ All::lat_long()[1] }}" />
	@endif

	@if(Options::header_type()==1) 
 		<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
	@endif
</body>
</html>
