@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 

<div class="row">
	@foreach(All::getNews() as $row) 

	<div class="col-md-4 col-sm-4 col-xs-12">

		<div class="news relative">
		 
		 	<a class="overlink" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"></a>


            @if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))
              
            	<!-- <div class="bg-img" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></div> -->
				<img 
					src="{{ $row->slika }}" 
					class="img-responsive" 
					alt="news" 
					loading="lazy" 
					decoding="async" 
					style="aspect-ratio: 5 / 3; object-fit: cover;"
				/>
            
            @else
            
            	<iframe width="560" height="315" src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   
            
            @endif

		   
			<h2 class="news-title overflow-hdn">{{ $row->naslov }}</h2>


			<div class="news-desc overflow-hdn">{{ All::shortNewDesc($row->tekst) }}</div>


			<div class="text-center">
				<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" class="button inline-block relative z-index-1">{{ Language::trans('Pročitaj članak') }}</a>
			</div> 

		</div>
	</div>

	<!-- 	<div>
			{{ All::getNews()->links() }}
		</div>   -->

		@endforeach
</div> 

@endsection