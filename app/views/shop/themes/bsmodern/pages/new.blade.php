@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 

<!-- NEW.blade -->

<div class="single-news">
 
    @if(in_array(Support::fileExtension($slika),array('jpg','png','jpeg','gif')))
	
		<img class="max-width" src="{{ $slika }}" alt="{{ $naslov }}">
    
    @else
    
    	<iframe src="{{ $slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   
    
    @endif
     

	<h2 class="news-title">{{ $naslov }}</h2>

	{{ $sadrzaj }} 
 
</div>  

<!-- NEW.blade END -->

@endsection