@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<!-- CONTACT.blade -->

<div class="row"> 
	<div class="col-md-5 col-sm-12 col-xs-12 sm-no-padd">

		<br>

		<h2><span class="section-title">{{ Language::trans('Kontakt informacije') }}</span></h2>

		@if(Options::company_name() != '') 
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Firma') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_name() }}</div>
		</div> 
		@endif

		@if(Options::company_adress() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Adresa') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_adress() }}</div>
		</div>
		@endif
		
		@if(Options::company_city() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Grad') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_city() }}</div>
		</div>
		@endif
		
		@if(Options::company_phone() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Telefon') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_phone() }}</div>
		</div>
		@endif
		
		@if(Options::company_fax() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Fax') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_fax() }}</div>
		</div>
		@endif
		
		@if(Options::company_pib() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('PIB') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_pib() }}</div>
		</div>
		@endif
		
		@if(Options::company_maticni() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('Matični broj') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7"> {{ Options::company_maticni() }}</div>
		</div>
		@endif
		
		@if( Options::company_email() != '')
		<div class="row">
			<div class="col-md-5 col-sm-5 col-xs-5 no-padding">{{ Language::trans('E-mail') }}:</div>
			<div class="col-md-7 col-sm-7 col-xs-7">
				<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
			</div> 
		</div>
		@endif

		<br>
		<!-- Podaci -->
		<ul>
			<li>
				<span>Čačak:</span> <br>
				<span>Bela tehnika i kućni aparati: ulica Gradsko šetalište 57, Telefon: <a href="tel:032/345-323"> 032/345-323</a></span> <br>
				<span>e-mail:<a href="mailto:cacak@uspon.rs"> cacak@uspon.rs</a></span>
			</li>

			<br>
			<li>
				<span>IT SHOP:</span> <br>
				<span>ulica Gradsko šetalište bb, Telefon: <a href="tel:032/51-21-653"> 032/51-21-653</a></span> <br>
				<span>e-mail:<a href="mailto:cacak1@uspon.rs"> cacak1@uspon.rs</a></span>
			</li>
			<br>
			<li>
				<span>Gornji Milanovac:</span> <br>
				<span>ulica Vuka Karadžića 1, Telefon: <a href="tel:032/710-499"> 032/710-499</a></span> <br>
				<span>e-mail:<a href="mailto:gornji.milanovac@uspon.rs"> gornji.milanovac@uspon.rs</a></span>
			</li>
			<br>

			<li>
				<span>Kraljevo:</span> <br>
				<span>ulica Trg Kralja Petra I Oslobodioca br. 1, Telefon: <a href="tel:036/220-220"> 036/220-220</a></span> <br>
				<span>e-mail: <a href="mailto:kraljevo@uspon.rs">kraljevo@uspon.rs</a></span>
			</li>
			<br>

			<li>
				<span>Užice:</span> <br>
				<span>ulica Kneza Lazara 4, Telefon: <a href="tel:031/520-919"> 031/520-919</a></span> <br>
				<span>e-mail: <a href="mailto:uzice@uspon.rs">uzice@uspon.rs</a></span>
			</li>
			<br>

			<li>
				<span>Trstenik:</span> <br>
				<span>ulica Svetog Save 22b, Telefon: <a href="tel:037/719-560"> 037/719-560</a></span> <br>
				<span>e-mail: <a href="mailto:trstenik@uspon.rs">trstenik@uspon.rs</a></span>
			</li>
			<br>

		</ul>

	</div> 


	<div class="col-md-7 col-sm-12 col-xs-12 sm-no-padd"> 

		<br>
		
		<h2><span class="section-title">{{ Language::trans('Pošaljite poruku') }}</span></h2>

		<form method="POST" action="{{ Options::base_url() }}contact-message-send">
			<div>
				<label id="label_name">{{ Language::trans('Vaše ime') }} *</label>
				<input class="contact-name" name="contact-name" id="JScontact_name" type="text" value="{{ Input::old('contact-name') }}">
				<div class="error red-dot-error">{{ $errors->first('contact-name') ? $errors->first('contact-name') : "" }}</div>
			</div> 

			<div>
				<label id="label_email">{{ Language::trans('Vaša e-mail adresa') }} *</label>
				<input class="contact-email" name="contact-email" id="JScontact_email" type="text" value="{{ Input::old('contact-email') }}" >
				<div class="error red-dot-error">{{ $errors->first('contact-email') ? $errors->first('contact-email') : "" }}</div>
			</div>		

			<div>	
				<label id="label_message">{{ Language::trans('Vaša poruka') }} </label>
				<textarea class="contact-message" name="contact-message" rows="5" id="message">{{ Input::old('contact-message') }}</textarea>
				<div class="error red-dot-error">{{ $errors->first('contact-message') ? $errors->first('contact-message') : "" }}</div>
			</div> 

			<div class="capcha text-center"> 
				{{ Captcha::img(5, 160, 50) }}<br>
				<span>{{ Language::trans('Unesite kod sa slike') }}</span>
				<input type="text" name="captcha-string" autocomplete="off">
				<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
			</div>

			<div class="text-right"> 
				<button type="submit" class="button">{{ Language::trans('Pošalji') }}</button>
			</div>
		</form>
	</div>
</div>


@if(Options::company_map() != '' && Options::company_map() != ';')
<div class="map-frame relative">
	
	<div class="map-info">
		<h5>{{ Options::company_name() }}</h5>
		<h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
	</div>

	<iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

</div> 
@endif
 

@if(Session::get('message'))
<script>
	$(document).ready(function(){     
 
        bootboxDialog({ message: "<p>{{ Session::get('message') }}</p>" }); 

	});
</script>
@endif

<!-- CONTACT.blade END -->

@endsection     