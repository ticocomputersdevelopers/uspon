@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
	<div class="sm-padd-15 max-w-70">
		@if($status == 1)
			<br><br>
			<h1>{{ Language::trans('Uspešno ste realizovali kredit') }}</h1>
			<br><br>

			<p> {{ Language::trans('Poštovani') }}, </p>
			<p> {{ Language::trans('Uspešno ste realizovali kredit Raiffeisen banke. Dobićete obaveštenje o statusu Vaše porudžbine.') }} </p>
		@else
			<br><br>
			<h1>{{ Language::trans('Neuspešno apliciranje za kredit') }}</h1>
			<br><br>

			<div>
				<p> {{ Language::trans('Poštovani') }}, </p>
				<p> {{ Language::trans('Neuspešno ste aplicirali za Web kredit Raiffeisen banke, ili ste odustali od ovog načina plaćanja.') }}</p>
				<p> {{ Language::trans('Vaša porudžbina') }} {{ $broj_porudzbine }} {{ Language::trans('neće biti realizovana. Možete da ponovite naručivanje i odaberete drugi način plaćanja.') }}</p>
				<p> {{ Language::trans('U slučaju dodatnih pitanja, kontaktirajte nas putem email adrese.') }} </p>
			</div>
		@endif

		<div> 
			<p> <strong>{{ Language::trans('Broj porudžbine:') }} </strong> {{ $broj_porudzbine }}</p>
			<p><strong>{{ Language::trans('Iznos:') }}</strong> {{ $iznos }}</p>
			<p>{{ Language::trans('Ime:') }} {{ $web_kupac->ime }}</p>
			<p>{{ Language::trans('Prezime:') }} {{ $web_kupac->prezime }}</p>
			<p>{{ Language::trans('Email:') }} {{ $web_kupac->email }}</p>
			<p>{{ Language::trans('Telefon:') }} {{ $web_kupac->telefon }}</p>
			<p>{{ Language::trans('Adresa:') }} {{ $web_kupac->adresa }}</p>
			<p><strong>{{ Language::trans('Broj transakcije:') }}</strong> {{ $broj_porudzbine }}</p>
			<p><strong>{{ Language::trans('Datum transakcije:') }}</strong> {{ $datum_transakcije }}</p>
		</div>
		<br>
	</div>
@endsection