@extends('shop/themes/'.Support::theme_path().'templates/products')

@section('products_list')

<div class="product-options row flex">
			
    <div class="col-md-6 col-sm-6 col-xs-12 sm-text-center">
 
<!-- PER PAGE -->
		
		<div class="JS-mob-o-filters inline-block hidden-lg hidden-md text-uppercase"> {{ Language::trans('Filteri') }} </div>
		
		<span>{{ Language::trans('UKUPNO') }}: {{ $count_products }}</span>
        @if(Options::product_number()==1)
		<div class="dropdown inline-block">	 
			 <button class="currency-btn dropdown-toggle" type="button" data-toggle="dropdown">	
			 	@if(Session::has('limit'))
				{{Session::get('limit')}}
				@else
				20
				@endif
    			<span class="caret"></span>
    		</button>
			<ul class="dropdown-menu currency-list">			
				<li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('limit') }}/20" rel="nofollow">20</a></li>
				<li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('limit') }}/30" rel="nofollow">30</a></li>
				<li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('limit') }}/50" rel="nofollow">50</a></li>			
			</ul>			 
		</div>
		@endif
   
 		@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
			<!-- MODAL TRIGGER BUTTON -->
			<button type="button" id="JScompareArticles" class="currency-btn {{ (Session::has('compare_ids') AND count(Session::get('compare_ids')) > 0) ? 'show-compered-active' : 'show-compered' }}" data-toggle="modal" data-target="#compared-articles">
				<span class="hidden-sm hidden-xs"> {{ Language::trans('Upoređeni artikli') }} </span>
				<span class="hidden-lg hidden-md fas fa-exchange-alt"> </span>
			</button>
		@endif
	
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12 text-right sm-text-center">
        @if(Options::product_currency()==1)
            <div class="dropdown inline-block">
            	 <button class="currency-btn dropdown-toggle" type="button" data-toggle="dropdown">
            	 	 {{Articles::get_valuta()}} 
            	 	 <span class="caret"></span>
            	 </button>
                 
                <ul class="dropdown-menu currency-list">
                	@foreach(DB::table('valuta')->where('ukljuceno',1)->orderBy('izabran','desc')->get() as $valuta)
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('valuta') }}/{{ $valuta->valuta_id }}" rel="nofollow">{{ Language::trans($valuta->valuta_sl) }}</a></li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(Options::product_sort()==1)
            <div class="dropdown inline-block"> 
            	 <button class="currency-btn dropdown-toggle" type="button" data-toggle="dropdown">
	                 {{Articles::get_sort()}}
	                 <span class="caret"></span>
            	</button>
                <ul class="dropdown-menu currency-list">
                	@if(Options::web_options(207) == 0)
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_asc" rel="nofollow">{{ Language::trans('Cena min') }}</a></li>
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_desc" rel="nofollow">{{ Language::trans('Cena max') }}</a></li>
                    @else
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_desc" rel="nofollow">{{ Language::trans('Cena max') }}</a></li>
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/price_asc" rel="nofollow">{{ Language::trans('Cena min') }}</a></li>
                    @endif
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/news" rel="nofollow">{{ Language::trans('Najnovije') }}</a></li>
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/name" rel="nofollow">{{ Language::trans('Prema nazivu') }}</a></li>
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/rbr" rel="nofollow">{{ Language::trans('Popularni') }}</a></li>
                    <li><a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sortiranje') }}/quantity" rel="nofollow">{{ Language::trans('Stanje na lageru') }}</a></li>
                </ul>
            </div>
        @endif
	          
	    <!-- GRID LIST VIEW -->
    	@if(Options::product_view()==1)
		@if(Session::has('list'))
		<div class="view-buttons inline-block"> 
			<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('prikaz') }}/list" rel="nofollow"><span class="fas fa-list"></span></a>
			<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('prikaz') }}/grid" rel="nofollow"><span class="fas fa-th active"></span></a>  		 
		 </div>
		@else
		<div class="view-buttons inline-block">  
			<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('prikaz') }}/list" rel="nofollow"><span class="fas fa-list active"></span></a>
			<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('prikaz') }}/grid" rel="nofollow"><span class="fas fa-th"></span></a>
		 </div>
		@endif
        @endif  
	</div>
</div>

<div class="text-center">
	{{ Paginator::make($articles, $count_products, $limit)->links() }}
</div>
 
   <!-- MODAL FOR COMPARED ARTICLES -->
  <div class="modal fade" id="compared-articles" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-center">{{ Language::trans('Upoređeni artikli') }}</h4>
        </div>
        <div class="modal-body">
			<div class="compare-section">
				<div id="compare-article">
					<div id="JScompareTable" class="compare-table text-center table-responsive"></div>
				</div>
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="button" data-dismiss="modal">{{ Language::trans('Zatvori') }}</button>
        </div>
      </div>    
    </div>
  </div>

<!-- PRODUCTS -->
@if(Session::has('list') or Options::product_view()==3)

<!-- LIST PRODUCTS -->
	@foreach($articles as $row)
		@include('shop/themes/'.Support::theme_path().'partials/products/product_on_list') 
	@endforeach 

@else
<!-- Grid products -->
<div class="row padding-v-20 flex">
	@foreach($articles as $row)
		@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
	@endforeach
</div>		
@endif

@if($count_products == 0) 
	<div class="col-md-12 col-sm-12 col-xs-12 no-padding"> 
		<div class="no-articles"> {{ Language::trans('Trenutno nema artikla za date kategorije') }}</div>
	</div>
@endif

<div class="text-center"> 
	{{ Paginator::make($articles, $count_products, $limit)->links() }}
</div>

@endsection