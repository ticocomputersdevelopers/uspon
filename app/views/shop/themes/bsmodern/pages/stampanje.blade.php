<!DOCTYPE html>
<html><head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>PDF</title>
<style>
* { 
	box-sizing: border-box;
 	padding: 0;
	margin: 0; 
	line-height: 1;	
} 
body {
	font-size: 14px;  
	font-family: DejaVu Sans;
}
ul { list-style-type: none; }

table {border-collapse: collapse; margin-bottom: 10px; width: 100%; }

td { 
	page-break-inside: avoid;
	page-break-after: avoid;
}

td, th {border: 1px solid #cacaca; text-align: left; padding: 3px; }
 
.row::after {content: ""; clear: both; display: table;}
[class*="col-"] { float: left; padding: 5px; display: inline-block;}

.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}
.inline-block { display: inline-block; }
.text-right{ text-align: right; }
.text-center{ text-align: center; } 
.relative { position: relative; }

img{
	max-width: 100%;
}
.header{
	border-bottom: 2px solid #cc0000;
	background-color: #f2f2f2;
	padding: 3px 5px;
}
.header .logo {
	max-height: 70px;  
	margin: 10px;
} 
.main-content {
	padding: 15px;   
}
.main-content .product-img{
	max-height: 230px;
	margin: 0;
	padding: 0;
} 
.grey-text, .red-text {
	padding: 10px 0;
	text-align: center;
}
.grey-text {
	background: #ccc;
	color: #000;
}
.red-text {
	background: crimson;
}
.main-content .foo_1{
	font-size: 15px;
	/* margin: 10px 0; */
	padding: 5px 0;
} 
.main-content .foo_2{
	font-size: 18px;
	margin: 10px 0;
	color: #000;
	font-weight: bold;
} 
.product-det{
	font-size: 80%;
} 
.main-content .foo_2 span{
	font-size: 90%;
} 
.main-content .foo_3 .ul_1,.main-content .foo_3 .genkar{ 
	border: 1px solid #ddd; 
}  
.main-content .foo_3 .ul_1 .li_1{ 
	width: 30%; 
}
.foo_3 > div { padding-left: 20px; }
.main-content .foo_3 .ul_1 .li_2{ 
	width: 60%; 
	border-bottom: 1px solid #ddd;
} 
.main-content .foo_3 ul li{
    font-size: 12px; 
    padding: 2px 10px;      
    width: 45%;
    display: inline-block;   
}      
.main-content .foo_3 .genkar li{
  	border-bottom: 1px solid #ddd;
}    
.border-top {
	height: 1px;
	background: #ccc;
	width: 95%;
	margin: 10px 0
}
.footer{
	background: #464853; 
	color: #fff;
	padding: 20px 15px;
	position: absolute;
	bottom: 0;
	font-size: 12px;
}
</style>
</head><body>
	<div class="container relative"> 
		<div class="row header">
			<div class="col-3">
				<img class="logo" src="{{ Options::domain()}}{{Options::company_logo()}}" alt="logo">
			</div>
		</div>

	  	<div class="row main-content">
	  		<div class="col-6">  
			  	<p>{{Language::trans('Kategorija') }}: <strong> {{ Product::get_grupa_title($roba_id) }} </strong></p>
				<p>{{Language::trans('Proizvođač') }}: <strong> {{ product::get_proizvodjac($roba_id) }} </strong></p>

		  		<p class="foo_1"><strong>{{ Product::seo_title($roba_id) }}</strong></p> 
				 
				@if(Product::get_mpcena($roba_id) != 0) 
				<p class="foo_2"><span>{{Language::trans('Maloprodajna cena') }}:</span> {{ str_replace(" rsd.","RSD",Cart::cena(Product::get_mpcena($roba_id))) }}</p>
				@endif
				
				<p class="foo_2"><span>{{Language::trans('Web cena') }}:</span> {{ str_replace(" rsd.","RSD",Cart::cena(Product::get_price($roba_id))) }}</p>
				
				@if(Product::get_mpcena($roba_id) - Product::get_price($roba_id) > 0)
				<p class="foo_2" style="color: red;"><span>{{Language::trans('Ušteda') }}:</span> {{ str_replace(" rsd.","RSD",Cart::cena(Product::get_mpcena($roba_id) - Product::get_price($roba_id))) }}</p>
				@endif
  
				
				<!-- <div>
					<p>Firma: {{AdminOptions::company_name()}}</p>
					<p>Adresa: {{AdminOptions::company_adress()}}</p>
					<p>Telefon: {{AdminOptions::company_phone()}}</p>
					<p>Fax: {{AdminOptions::company_fax()}}</p>
					<p>PIB: {{AdminOptions::company_pib()}}</p>
					<p>E-mail: {{AdminOptions::company_email()}}</p>
				</div> -->
			</div>

			<div class="col-6 text-center"> 
				@if(AdminOptions::sifra_view_web()==1)
				<div class="product-det">
				<p>{{Language::trans('Šifra') }}: <strong>{{Product::sifra($roba_id)}}</strong></p>
				@elseif(AdminOptions::sifra_view_web()==4)                       
				<p>{{Language::trans('Šifra') }}: <strong>{{Product::sifra_d($roba_id)}}</strong></p>
				@elseif(AdminOptions::sifra_view_web()==3)                       
				<p>{{Language::trans('Šifra') }}:<strong>{{Product::sku($roba_id)}}</strong></p>
				@elseif(AdminOptions::sifra_view_web()==2)                       
				<p>{{Language::trans('Šifra') }}: <strong>{{Product::sifra_is($roba_id)}}</strong></p>
				@endif	

				<img class="product-img" src="{{ Options::domain() }}{{ Product::web_slika($roba_id) }}" alt="{{ Product::seo_title($roba_id) }}" />
			</div>

			<div class="border-top"></div>
	  	 
		  	<div class="row foo_3">
				  <span>Specifikacija</span>
	  			  <div>{{ str_replace(array("<tbody>","<br>","</tbody>"),"",DB::table('roba')->where('roba_id',$roba_id)->pluck('web_karakteristike')) }}</div>
		  	</div> 	
	 	</div>

	  	<div class="row footer">
			<p class="grey-text">Raspoloživost robe možete proveriti pozivanjem Call Centra na 032/340-410</p>
	  		<p class="red-text"><br /> Cena na sajtu su iskazane u dinarima sa uračunatim porezom, a plaćanje se vrši isključivo u dinarima. <br>
			  <br> Nastojimo da budemo što prezicniji u opisu proizvoda, prikazu slika i samih cena, ali ne možemo garantovati da su sve informacije kompletne i bez grešaka.
			  Svi artikli prikazani na sajtu su deo naše ponude i ne podrazumeva da su dostupni u svakom trenutku.
			</p>
	  	</div> 
	</div>  
</body></html>
