@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<!-- <div class="ktlg-page">
	@foreach(All::getKatalog() as $row)  
	<div class="box"> 
		<div class="row"> 		
			<div class="col-md-8 col-sm-8 col-xs-12"> 		
				<h2><span>{{ $row->naziv }}</span></h2>		 
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 text-right"> 	
				<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('katalog') }}/{{ $row->katalog_id }}" target="_blank" class="button">{{ Language::trans('Preuzmi katalog') }}</a>
			</div> 
		</div>
	</div>
	@endforeach
</div> -->

<div class="ktlg-page">
	<div class="row">
		@foreach(All::getKatalog() as $row)
		<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="box text-center" {{ $row->aktivan == 0 ? 'style="filter: grayscale(1);"' : '' }}>
				<img 
					src="{{ Options::domain() }}{{ $row->putanja_slika }}"
					alt="{{ $row->naziv }}"
					width="170"
					height="170"
					class="img-responsive margin-auto"
					loading="lazy"
					decoding="async"
				/>
				
				<div class="box-inner-ktlg">
					<h4 class="text-uppercase">{{ $row->naziv }}</h4>
					<a href="{{ Options::base_url() }}{{ $row->putanja_pdf }}" target="_blank" class="button text-uppercase">{{ Language::trans('Preuzmite') }}</a>
				</div>
				
			</div>
		</div>
		@endforeach
	</div>
</div>


@endsection