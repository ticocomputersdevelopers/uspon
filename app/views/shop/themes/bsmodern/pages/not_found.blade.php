@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 
	<div class="row">
		<div class="col-md-7 col-sm-7 col-xs-12 text-wrapper">
			<h3 class="col-xs-12">{{ Language::trans('Poštovani,') }}</h3>
			<h3 class="col-xs-12">{{ Language::trans('Link koji ste pratili nije ispravan ili stranica više ne postoji.') }}
			</h3>
			<a href="/" class="col-xs-12 wrap-home-button flex"><span class="home-button">{{ Language::trans('Početna strana') }}</span></a>
		</div> 
		<div class="col-md-5 col-sm-5 col-xs-12">
			<img src="/images/404.png">
		</div>
	</div>
@endsection