@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')


	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
		<br><br>
		<h3>{{ Language::trans('Nakon potvrde vrši se preusmeravanje na sajt Raiffaisen Banke, gde se na zaštićenoj stranici realizuje proces plaćanja') }}.</h3></br>

		<h4>{{ Language::trans('Podaci o korisniku') }}:</h4>
		<ul>
			@if($web_kupac->flag_vrsta_kupca == 1)
				<li>{{ Language::trans('Firma') }}: {{ $web_kupac->naziv }}</li>
			@else
				<li>{{ Language::trans('Ime') }}: {{ $web_kupac->ime.' '.$web_kupac->prezime }}</li>
			@endif
			<li>{{ Language::trans('Adresa') }}: {{ $web_kupac->adresa }}, {{ $web_kupac->mesto }}</li>
		</ul>
		</br>
		<h4>{{ Language::trans('Podaci o trgovcu') }}:</h4>
		<ul>
			<li>{{ $preduzece->naziv }}</li>
			<li>{{ Language::trans('Kontakt osoba') }}: {{ $preduzece->kontakt_osoba }}</li>
			@if($preduzece->matbr_registra != '')
			<li>{{ Language::trans('MBR') }}: {{ $preduzece->matbr_registra }}</li>
			@endif
			@if($preduzece->pib != '')
			<li>VAT Reg No: {{ $preduzece->pib }}</li>
			@endif
			<li>{{ $preduzece->adresa }}, {{ $preduzece->mesto }}</li>
			<li>{{ Language::trans('Telefon') }}: {{ $preduzece->telefon }}</li>
			<li><a href="mailto:{{ $preduzece->email }}">{{ $preduzece->email }}</a></li>
		</ul>
		</br>

		<input type="checkbox" id="JSConditions" onclick="clickCheckbox()"> <a href="{{ Options::base_url() }}uslovi-kupovine" target="_blank">
		{{ Language::trans('Uslovi korišćenja') }}</a>
		
        <!-- <form action="https://rs.raiffeisenbank.rs/RetailNTBV5/webkredit/kalkulator" method="post" id="rba-form" enctype="multipart/form-data"> -->
        <form action="https://rol.raiffeisenbank.rs/online/webkredit/kalkulator" method="post" id="rba-form" enctype="multipart/form-data">

		 		<button type="submit" class="button" id="JSRaifSubmit" disabled>{{ Language::trans('Plati kreditom Raiffeisen banke') }}</button>

 				<input type="hidden" name="customer_first_name" value="{{ $customer_first_name }}">
				<input type="hidden" name="customer_last_name" value="{{ $customer_last_name }}">
				<input type="hidden" name="customer_email" value="{{ $customer_email }}">
				<input type="hidden" name="customer_mobile_phone" value="{{ $customer_mobile_phone }}">
				<input type="hidden" name="customer_address" value="{{ $customer_address }}">
				<input type="hidden" name="merchant_name" value="{{ $merchant_name }}">
				<input type="hidden" name="merchant_address1" value="{{ $merchant_address1 }}">
				<input type="hidden" name="merchant_address2" value="{{ $merchant_address2 }}">
				<input type="hidden" name="point_of_sale_code" value="{{ $point_of_sale_code }}">
				<input type="hidden" name="merchant_registration_number" value="{{ $merchant_registration_number }}">
				<input type="hidden" name="order_number" value="{{ $order_number }}">
				<input type="hidden" name="order_expiration_date" value="{{ $order_expiration_date }}">
				<input type="hidden" name="order_amount" value="{{ round($order_amount) }}">
				<input type="hidden" name="order" value="{{ htmlspecialchars(json_encode($order),ENT_QUOTES, 'UTF-8') }}">
				<input type="hidden" name="return_url" value="{{ $return_url }}">
				<input type="hidden" name="nok_return_url" value="{{ $nok_return_url }}">
				<input type="hidden" name="order_base64" value="{{ $order_base64 }}">
				
				<?php DB::table('web_b2c_narudzbina')->where(array('broj_dokumenta'=>$order_number,'web_nacin_placanja_id'=>5,'stornirano' => 1))->update(array('post_date' => date('Y-m-d H:i:s'))); ?>
				
		</form>
		<script type="text/javascript">
			function clickCheckbox(){
				if(document.getElementById("JSConditions").checked){
					document.getElementById("JSRaifSubmit").removeAttribute('disabled');
				}else{
					document.getElementById("JSRaifSubmit").setAttribute('disabled','disabled');
				}
			}
		</script>
	</div>
@endsection
