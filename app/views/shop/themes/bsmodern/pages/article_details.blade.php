@extends('shop/themes/'.Support::theme_path().'templates/article')

@section('article_details')

<!-- ARTICLE_DETAILS.blade -->

<div id="fb-root"></div> 
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

@if(Session::has('success_add_to_cart'))
    $(document).ready(function(){     
     
        bootboxDialog({ message: "<p>" + trans('Artikal je dodat u korpu') + ".</p>" }, 2200); 

    });
@endif

@if(Session::has('success_comment_message'))
    $(document).ready(function(){     
     
        bootboxDialog({ message: "<p>" + trans('Vaš komentar je poslat') + ".</p>" }, 2200); 

    });
@endif

@if(Session::has('message'))
    $(document).ready(function(){    
        bootboxDialog({ message: "<p>" + trans('Uspešno ste prosledili upit') + ".</p>" }, 2200); 
    });
@endif
</script> 


<main class="d-content JSmain relative"> 

    <div class="container">
     
        <ul class="breadcrumb"> 
            {{ Product::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
        </ul>
     
        <div class="row"> 
            
            <div class="JSproduct-preview-image col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="row"> 
                    <div id="gallery_01" class="col-md-3 col-sm-3 col-xs-12 text-center sm-no-padd">

                        @if(!empty(Product::design_id($roba_id)) AND Options::pitchprint_aktiv() == 1)                             
                            @if(Product::pitchPrintImageExist($roba_id) == TRUE)                          
                                <a class="elevatezoom-gallery JSp_print_small_img" 
                                href="javascript:void(0)" 
                                data-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766" 
                                data-zoom-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766">
                               
                                <img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766" 
                                alt="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_2.jpg?k=0.4491390433183766"/> 

                                </a>
                            @endif   
                                <a class="elevatezoom-gallery JSp_print_small_img" 
                                href="javascript:void(0)" 
                                data-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766" 
                                data-zoom-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766">
                               
                                <img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766" 
                                alt="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766"/> 

                            </a>   

                        @else

                            @foreach($slike as $image)
                            <a class="elevatezoom-gallery" href="javascript:void(0)" data-image="/{{$image->putanja}}" data-zoom-image="{{ Options::domain().$image->putanja }}">

                                <img alt="{{ Options::domain().$image->putanja }}" id="{{ Options::domain().$image->web_slika_id }}" src="{{ Options::domain().$image->putanja }}"/>

                            </a>
                            @endforeach
                        @endif
                    </div>

                    <div class="col-md-9 col-sm-6 col-xs-12 disableZoomer relative"> 

                        @if(All::provera_akcija($roba_id))
                        <div class="for-sale-price text-right">- {{ Product::getSale($roba_id) }} %</div> 
                        @endif

                        <a class="fancybox" href="{{ Options::domain() }}{{ $slika_big }}">

                            @if(!empty(Product::design_id($roba_id)) AND Options::pitchprint_aktiv() == 1)

                                <span id="zoom_03" class="JSzoom_03" style="display: block;" data-zoom-image="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766">
                                    
                                    <img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($roba_id)}}_1.jpg?k=0.4491390433183766" id="art-img" class="JSzoom_03 img-responsive" alt="{{ Product::seo_title($roba_id)}}" />
                                
                                </span>

                            @else
                                <span id="zoom_03" class="JSzoom_03" style="display: block;" data-zoom-image="{{ Options::domain() }}{{ $slika_big }}">
                                   
                                    <img class="JSzoom_03 img-responsive" id="art-img" src="{{ Options::domain() }}{{ $slika_big }}" alt="{{ Product::seo_title($roba_id)}}" />

                                </span>
                            @endif
                        </a>

                        <div class="product-sticker flex">
                            @if( B2bArticle::stiker_levo($roba_id) != null )
                                <a class="article-sticker-img">
                                    <img class="img-responsive" src="{{ Options::domain() }}{{B2bArticle::stiker_levo($roba_id) }}"  />
                                </a>
                            @endif 
                            
                            @if( B2bArticle::stiker_desno($roba_id) != null )
                                <a class="article-sticker-img clearfix">
                                    <img class="img-responsive pull-right" src="{{ Options::domain() }}{{B2bArticle::stiker_desno($roba_id) }}"  />
                                </a>
                            @endif   
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 additional_img">    
                        @foreach($glavne_slike as $slika)
                        <a class="inline-block text-center" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}/{{$slika->web_slika_id}}"> 
                            <img src="{{ Options::domain().$slika->putanja }}" alt=" {{ Options::domain().$slika->putanja }}" class="img-responsive inline-block"> 
                        </a>
                        @endforeach
                    </div>
                </div>
               
            </div>

            <div class="product-preview-info lg-width-col col-lg-5 col-md-6 col-sm-12 col-xs-12">

                <!-- FOR SEO -->
                <h1 class="article-heading">{{ Product::seo_title($roba_id) }}</h1>

                <div class="review">{{ Product::getRating($roba_id) }}</div>

                {{ Product::get_kratak_opis($roba_id) }}
                
                @if($proizvodjac_id != -1)
                    @if( Product::slikabrenda($roba_id) != null )
                   
                    <a class="article-brand-img inline-block" href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">
                        <img src="{{ Options::domain() }}{{product::slikabrenda($roba_id) }}" alt="{{ product::get_proizvodjac($roba_id) }}" />
                    </a>

                    @else

                    <a href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}" class="artical-brand-text inline-block">{{ product::get_proizvodjac($roba_id) }}</a>

                    @endif                                   
                @endif     

                <!-- ARTICLE PASSWORD -->
<!--                 @if(AdminOptions::sifra_view_web()==1)
                <div>
                    <span class="gray-code">{{Language::trans('Šifra artikla') }}:</span> 
                    <span class="text-bold">{{Product::sifra($roba_id)}}</span>
                </div>
                @elseif(AdminOptions::sifra_view_web()==4)                       
                <div>
                    <span class="gray-code">{{Language::trans('Šifra artikla') }}:</span> 
                    <span class="text-bold">{{Product::sifra_d($roba_id)}}</span>
                </div>
                @elseif(AdminOptions::sifra_view_web()==3)                       
                <div>
                    <span class="gray-code">{{Language::trans('Šifra artikla') }}:</span> 
                    <span class="text-bold">{{Product::sku($roba_id)}}</span>
                </div>
                @elseif(AdminOptions::sifra_view_web()==2)                       
                <div>
                    <span class="gray-code">{{Language::trans('Šifra artikla') }}:</span> 
                    <span class="text-bold">{{Product::sifra_is($roba_id)}}</span>
                </div>  
                @endif -->
                @if($id_is = Product::get_id_is($roba_id))
                <div>
                    <span class="gray-code">{{Language::trans('Ident') }}:</span>
                    <!-- <span class="gray-code">{{Language::trans('Šifra artikla') }}:</span>  -->
                    <span class="text-bold">{{ $id_is }}</span>
                </div>
                @endif
                @if($barkod = Product::get_barkod($roba_id))
                <div>
                    <span class="gray-code">{{Language::trans('EAN') }}:</span> 
                    <span class="text-bold">{{ $barkod }}</span>
                </div>
                @endif

                <div class="row flex dellivery-info">  
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                        <span class="gray-code"> {{Language::trans('Dostupnost') }}:</span>
                        @if(Product::getStatusArticle($roba_id) == 1 AND Cart::check_avaliable($roba_id) > 0)
                            <span class="available-green inline-block valign">{{Language::trans('Na stanju')}}</span>
                        @else
                           <span class="not-available-red inline-block valign">{{Language::trans('Nema na stanju')}} </span>
                        @endif
                    </div>
                    <!-- <div class="col-md-6 col-sm-6 col-xs-12 no-padding text-uppercase"> 
                        <div> <p> {{Language::trans('Besplatna dostava') }} </p> <p> {{Language::trans('osim za belu tehniku') }}</p> </div> 
                    </div> -->
                </div>

                <!-- ENERGY CLASS -->
                <?php $energy_check = DB::table('roba')->where('roba_id', $roba_id)->pluck('energetska_klasa'); ?>
                @if(AdminOptions::energy_class()==1 && isset($energy_check))
                <div class="JSenergyClassToggle" energy-data="{{ Product::get_energetska_klasa_tip($roba_id) }}">
                    <div class="flex">
                        <span class="gray-code">
                            {{ Language::trans('Energetska klasa') }}: 
                        </span>
                        @if(Product::get_energetska_klasa_old_or_new($roba_id) == 0)
                        <div class="oldEnergyClassMark flex">A <i class="fas fa-arrow-up"></i> G</div>
                        @endif
                        <div class="energyClassWrap">
                            <a href="{{ Product::get_energetska_klasa_link($roba_id) }}" target="_blank">
                                <span class="energyClass flex" energy-class="{{ Product::get_energetska_klasa_tip($roba_id) }}"><span>{{ Product::get_energetska_klasa($roba_id) }}</span></span>
                            </a>
                        </div>
                    </div>
                    <div class="energyLinkWrap">
                        @if(Product::get_energetska_klasa_old_or_new($roba_id) == 1)
                        <a href="{{ Options::base_url() }}novi-standardi-za-klase-energetske-efikasnosti" target="_blank">
                            <div>{{ Language::trans('Napomena: Po prethodnoj klasifikaciji') }}</div>
                        </a>
                        @endif
                        <a href="{{Options::base_url()}}product-deklaracija/{{ $roba_id }}" target="_blank">
                            <span class="energyLink">{{ Language::trans('Lista sa podacima aparata') }}</span>
                        </a>
                    </div>
                </div>
                @endif

                <ul>
                    @if($grupa_pr_id != -1)
                    <li class="hidden">
                        <span class="gray-code">{{Language::trans('Proizvod iz grupe')}}</span>:{{ Product::get_grupa($roba_id) }}
                    </li>
                    @endif

                    @if($proizvodjac_id != -1) 
                    <li class="hidden">
                        <span class="gray-code">{{Language::trans('Proizvođač')}}:</span>

                        @if(Support::checkBrand($roba_id))
                        <a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac')}}/{{ Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">{{Product::get_proizvodjac($roba_id)}}</a>
                        @else
                        <span>{{Product::get_proizvodjac($roba_id)}}</span>
                        @endif
                    </li>
                    @endif 

                    @if(Options::vodjenje_lagera() == 1)
                      <li class="hidden"><span class="gray-code">{{Language::trans('Dostupna količina')}}:</span> {{Cart::check_avaliable($roba_id)}}</li>  
                    @endif  

                    @if(Options::checkTezina() == 1 AND Product::tezina_proizvoda($roba_id)>0)
                    <li><span class="gray-code">{{Language::trans('Težina artikla')}}:</span> {{Product::tezina_proizvoda($roba_id)/1000}} kg</li>
                    @endif 
                </ul>

                <a href="/opsti-uslovi-poslovanja" class="inline-block term-article" target="_blank"> *uslovi kupovine </a>

                <!-- PRICE -->
                <div class="product-preview-price @if((Product::getStatusArticle($roba_id) == 1) AND (!Cart::check_avaliable($roba_id) > 0)) hidden @endif">
               
                    @if(Product::getStatusArticlePrice($roba_id) == 1)

    <!--                         @if(Product::pakovanje($roba_id)) 
                        <div> 
                            <span class="price-label">{{ Language::trans('Pakovanje') }}:</span>
                            <span class="price-num">{{ Product::ambalaza($roba_id) }}</span> 
                        </div>
                        @endif  -->                                

                        @if(All::provera_akcija($roba_id))   
                            <div class="flex row">

                                @if(Product::get_mpcena($roba_id) != 0) 
                                <!-- <div>
                                    <span class="price-label">{{ Language::trans('Maloprodajna cena') }}:</span>
                                    <span class="price-num mp-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                                </div> -->
                                @endif  

                                <div class="col-md-6 col-sm-6 col-xs-12 left-product-price p-r-8 no-padding">

                                    <div class="delivery-content flex">
                                        <img src="{{ Options::domain() }}images/truck_icon.png">
                                        <div class="text-bold text-uppercase"> {{Language::trans('Rok isporuke')}} <br> 2-5 {{Language::trans('radnih dana')}} </div>
                                    </div>
                                       
                                    <!-- <h5> {{ Language::trans('Cena') }} </h5> -->
                                    <h5 class="text-uppercase text-white">
                                        @if(Product::sifra_is($roba_id) != '')  
                                            {{ Language::trans('Web cena') }} 
                                        @else 
                                             {{ Language::trans('Samo online') }} 
                                        @endif
                                    </h5>
                                    <!-- <p> *{{ Language::trans('Za gotovinsko plaćanje') }} </p> -->

                                    <!-- ACTION PRICE -->
                                    <span class="JSaction_price price-num main-price" data-action_price="{{Product::get_price($roba_id)}}">
                                        {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                                    </span>

                                    @if(Product::getPopust_akc($roba_id,Product::get_mpcena($roba_id))>0)
                                        <!-- <div> 
                                            <span class="price-label">{{ Language::trans('ušteda') }}: </span>
                                            <span class="discount">{{ Cart::cena(Product::getPopust_akc($roba_id,Product::get_mpcena($roba_id))) }}</span>
                                        </div>

                                        <div class="discount-percent">
                                            <span>{{ Language::trans('ostvareni popust u iznosu od') }}:</span>
                                            <span>{{ Product::getPopustProcenat(Product::get_mpcena($roba_id),Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }} %</span>
                                        </div> -->
                                    @endif
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 right-product-price p-l-8 no-padding"> 
                                        
                                    <span class="price-label text-uppercase text-bold">{{ Language::trans('Maloprodajna cena')}}</span> 
                                    <p class="text-uppercase text-bold"> *{{ Language::trans('Za odloženo plaćanje') }}: </p>
                                    <!-- WEB CENA - MPCENA -->
                                    <span class="price-num mp-price"> {{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                                    @if(Product::get_price($roba_id) >= 8000 && Product::get_price($roba_id) <= 400000)
    
                                        <div class="w_credit_part">                 
                                            <span class="price-label text-uppercase text-bold">{{ Language::trans('Rata putem WEB kredita') }}</span>
                                            
                                            @if(date('Y-m-d') >= '2025-01-01')
                                                <div class="">23 x {{ Cart::cena(round((Product::get_price($roba_id) * 13.95)/1200/(1-pow((1 + 13.95/1200),-23)))) }}</div>
                                            @else
                                                <div class="">23 x {{ Cart::cena(round((Product::get_price($roba_id) * 19.9)/1200/(1-pow((1 + 19.9/1200),-23)))) }}</div>
                                            @endif
                                            
                                            
                                            <a class="inline-block price-label text-uppercase text-bold" href="{{ AdminOptions::base_url().'raiffeisen-web-kredit' }}" target="_blank">{{ Language::trans('Reprezentativni primer') }}</a>
                                        </div>
                                    @elseif(Product::sifra_is($roba_id) != '')
                                        <div>
                                            <span class="price-label text-uppercase text-bold">{{ Language::trans('Rata na 12 meseci') }}:</span>
                                            <span class="price-num discount text-right">{{ Cart::cena(Product::get_mpcena($roba_id) * 1.05 / 12) }}</span>
                                        </div>
                                    @endif 
                                </div>
                               <!--  <div>
                                    <span class="price-label">{{ Language::trans('Web cena') }}:</span>
                                    <span class="price-num mp-price"> {{ Cart::cena(Product::get_price($roba_id,false,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}</span> 
                                </div> -->
                        
                            </div>
                        @else

                            <div class="row flex">
                                <div class="col-md-6 col-sm-6 col-xs-12 left-product-price p-r-8 no-padding">

                                    <div class="delivery-content flex">
                                        <img src="{{ Options::domain() }}images/truck_icon.png"> 
                                        <div class="text-bold text-uppercase"> {{Language::trans('Rok isporuke')}} <br> 2-5 {{Language::trans('radnih dana')}} </div>
                                    </div>
                                        
                                    <h5 class="text-uppercase text-white">
                                        @if(Product::sifra_is($roba_id) != '')  
                                            {{ Language::trans('Web cena') }} 
                                        @else 
                                             {{ Language::trans('Samo online') }} 
                                        @endif
                                    </h5>
                                    <!-- <h5> {{ Language::trans('Cena') }} </h5>
                                    <p> *{{ Language::trans('Za gotovinsko plaćanje') }} </p> -->

                                      <div> 
                                        <!-- WEB CENA -->
                                        <span class="JSweb_price price-num main-price" data-cena="{{Product::get_price($roba_id)}}">
                                           {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                                       </span>

                                       @if(Product::getPopust($roba_id)>0)
                                           @if(AdminOptions::web_options(132)==1)
                                                <!-- <div> 
                                                    <span class="price-label">{{ Language::trans('ušteda') }}:</span>
                                                    <span class="discount">{{ Cart::cena(Product::getPopust($roba_id)) }}</span>
                                                </div>

                                                <div class="discount-percent">
                                                    <span>{{ Language::trans('ostvareni popust u iznosu od') }}:</span>
                                                    <span>{{ Product::getPopustProcenat(Product::get_mpcena($roba_id),Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }} %</span>
                                                </div> -->
                                            @endif
                                        @endif
                                   </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 right-product-price p-l-8 no-padding">

                                        
                                        <div> 
                                            <span class="price-label text-uppercase text-bold">{{ Language::trans('Maloprodajna cena') }}</span>
                                            <p class="text-uppercase text-bold"> *{{ Language::trans('Za odloženo plaćanje') }}: </p>
                                            <span class="price-num mp-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                                        </div>
                                        @if(Product::get_price($roba_id) >= 8000 && Product::get_price($roba_id) <= 400000)
                                            <div class="w_credit_part">
                                                <span class="price-label text-uppercase text-bold">{{ Language::trans('Rata putem WEB kredita') }}</span>
                                                
                                                @if(date('Y-m-d') >= '2025-01-01')
                                                    <div class="price-num mp-price">23 x {{ Cart::cena(round((Product::get_price($roba_id) * 13.95)/1200/(1-pow((1 + 13.95/1200),-23)))) }}</div>
                                                @else
                                                    <div class="price-num mp-price">23 x {{ Cart::cena(round((Product::get_price($roba_id) * 19.9)/1200/(1-pow((1 + 19.9/1200),-23)))) }}</div>
                                                @endif

                                                
                                                <a class="inline-block price-label text-uppercase text-bold" href="{{ AdminOptions::base_url().'raiffeisen-web-kredit' }}" target="_blank">{{ Language::trans('Reprezentativni primer') }}</a>
                                            </div>
                                        @elseif(Product::sifra_is($roba_id) != '')
                                            <div>
                                                <span class="price-label text-uppercase text-bold">{{ Language::trans('Rata na 12 meseci') }}:</span>
                                                <span class="price-num discount text-right">{{ Cart::cena(Product::get_mpcena($roba_id) * 1.05 / 12) }}</span>
                                            </div>
                                        @endif

                                </div>
                            </div>
                        @endif
                    @endif 
                </div> 

                @if(All::provera_akcija($roba_id))
                @if(All::action_start($roba_id) || All::action_deadline_article($roba_id))
                    <span class="action_text_info">
                        {{ Language::trans('Akcija traje od') }} {{ All::action_start($roba_id) }}. {{ Language::trans('do') }} {{ All::action_deadline_article($roba_id) }}.
                        {{ Language::trans('ili do isteka zaliha') }}
                    </span>
                @endif
                @endif

            <div class="add-to-cart-area clearfix">    

             @if(Product::getStatusArticle($roba_id) == 1)
                 @if(Cart::check_avaliable($roba_id) > 0)
                 <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm"> 
                  
                    <!-- PITCHPRINT -->
                    @if(!empty(Product::design_id($roba_id) AND !empty(Options::pitchprint() AND Options::pitchprint_aktiv() == 1)))
                        @include('shop/themes/'.Support::theme_path().'partials/pitchprint')
                    @endif
                    <!-- PITCHPRINT END -->

                @if(Product::check_osobine($roba_id))  
                    @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)

                    <div class="attributes text-bold">

                        <div>{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}</div>

                        @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id) as $osobina_vrednost_id)
                        <label class="relative no-margin" style="background-color: {{ Product::find_osobina_vrednost($osobina_vrednost_id, 'boja_css') }}">

                            <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>

                            <span class="inline-block">{{ Product::find_osobina_vrednost($osobina_vrednost_id, 'vrednost') }}</span>
                            
                        </label>
                        @endforeach

                    </div>

                    @endforeach 
                @endif

                @if(AdminOptions::web_options(313)==1) 
                <div class="num-rates"> 
                    <div> 
                        <div class="inline-block lorem-1">{{ Language::trans('Broj rata') }}</div>
                    </div>
                    <select class="JSinterest" name="kamata">
                        {{ Product::broj_rata(Input::old('kamata')) }}
                    </select>
                </div>
                @endif

                <input type="hidden" name="roba_id" value="{{ $roba_id }}">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6 no-padding p-r-8">
                        <div class="flex cart-input">

                            <span class="hidden-xs">{{Language::trans('Količina')}}&nbsp;</span>

                            <div> 
                                @if(Options::web_options(320) == 1 AND (Product::jedinica_mere($roba_id)->jedinica_mere_id) == 3 AND Product::pakovanje($roba_id) ) 
                                    <input type="number" name="kolicina" class="cart-amount weight" min="0" step="0.1" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}"><span>&nbsp;{{Language::trans(' kg')}}&nbsp;</span>
                                @else
                                    <span class="cart-add-amount add-sub-cont text-center valign"> 
                                        <div class="JSsub-num">-</div>

                                        <input type="text" name="kolicina" class="cart-amount JScart-num" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">
                                      
                                        <div class="JSadd-num">+</div>
                                    </span>
                                    
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6 no-padding p-l-8"> 
                        <!-- FAKE INFO -->
                        <!-- <span class="gray-code info-cart-product hidden">&nbsp;</span> -->

                        <button type="submit" id="JSAddCartSubmit" class="add-to-cart button">
                            <!-- <span class="cart-product inline-block v-align"></span> -->
                            <span>{{Language::trans('Dodaj u korpu')}}</span>
                        </button>
                        <input type="hidden" name="projectId" value=""> 

                        <div class="red-dot-error">{{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}</div>  
                    </div>
                </div>

                <div class="print-wish row">
                    <div class="col-md-6 col-sm-6 col-xs-6 no-padding text-right p-r-8">
                        <div class="printer inline-block text-center">  
                            <a href="{{Options::base_url()}}stampanje/{{ $roba_id }}" class="inline-block" target="_blank" rel="nofollow">
                                <i class="fas fa-print"></i>
                                <p> {{ Language::trans('Štampaj') }} </p>
                            </a>
                            
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6 no-padding p-l-8">
                        <div class="text-center inline-block">
                            @if(Cart::kupac_id() > 0)
                            <button class="like-it JSadd-to-wish" type="button" data-roba_id="{{$roba_id}}">
                                <i class="far fa-heart"></i>
                                <p> {{ Language::trans('Dodaj') }} <span class="center-block"> {{ Language::trans('u listu želja') }} </span> </p>
                            </button>
                            @else
                            <button class="like-it JSnot_logged" type="button" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla u listu želja moguće je samo registrovanim korisnicima') }}">
                                <i class="far fa-heart"></i>
                                <p> {{ Language::trans('Dodaj') }} <span class="center-block"> {{ Language::trans('u listu želja') }} </span> </p>
                            </button> 
                            @endif
                        </div>
                    </div>
                </div>

            </form>

            @else

            <!-- <button class="not-available button">{{Language::trans('Nije dostupno')}}</button> -->
            <button class="not-available button JSenquiry" data-roba_id="{{$roba_id}}">{{Language::trans('Proverite stanje')}}</button> 

            @endif

            @else
            <button class="button" data-roba-id="{{$roba_id}}">
                {{ Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') }}
            </button>
            @endif
        </div>


        @if(!empty(Product::get_labela($roba_id)))
        <div class="custom-label inline-block relative">
            <i class="fa fa-info-circle"></i>
            {{Product::get_labela($roba_id)}} 
        </div>
        @endif

        <div class="facebook-btn-share flex hidden">

            <div class="soc-network inline-block"> 
                <div class="fb-like" data-href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
            </div>

            <div class="soc-network"> 
                <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            </div>  
        </div>


        <!-- ADMIN BUTTON-->
        @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
        <div class="admin-article"> 
            @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
            <a class="JSFAProductModalCall article-level-edit-btn" data-roba_id="{{$roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a> 
            @endif
            <span class="supplier"> {{ Product::get_dobavljac($roba_id) }}</span> 
            <span class="supplier">{{ Language::trans('NCP') }}: {{ Product::get_ncena($roba_id) }}</span>
        </div>
        @endif
    </div>
    </div>

    @if(count($srodni_artikli) > 0)
        <div class="related-custom JSproducts_slick row"> 
        @foreach($srodni_artikli as $srodni_artikl)
        <div class="JSproduct col-md-4 col-sm-4 col-xs-12">  
            <div class="card flex row">  
                <div class="col-xs-3 no-padding">
                    <div class="img-wrap"> 
                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($srodni_artikl->srodni_roba_id))}}">
                            <img class="img-responsive" src="{{ Options::domain() }}{{ Product::web_slika_big($srodni_artikl->srodni_roba_id) }}" alt="{{ Product::seo_title($srodni_artikl->srodni_roba_id)}}" />
                        </a>
                    </div>
                </div>

                <div class="col-xs-9">
                    <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($srodni_artikl->srodni_roba_id))}}">
                        <h2 class="title">{{ Product::seo_title($srodni_artikl->srodni_roba_id) }}</h2>
                    </a>

                    <div>{{ Product::get_karakteristika_srodni($srodni_artikl->grupa_pr_vrednost_id) }}</div>

                    <div class="price"> 
                        <span>{{ Cart::cena(Product::get_price($srodni_artikl->srodni_roba_id)) }}</span>
                    </div>  
                </div>   
            </div>
        </div>
        @endforeach 
        </div>
    @endif

    @if(Options::checkTags() == 1)
        @if(Product::tags($roba_id) != '')
        <div class="product-tags">  
            <div>{{ Language::trans('Tagovi') }}:</div>

            <h6 class="text-white inline-block">
                {{ Product::tags($roba_id) }} 
            </h6>
        </div>       
        @endif
    @endif    

    <!-- PRODUCT PREVIEW TABS-->
    <div id="product_preview_tabs" class="product-preview-tabs row">
    <div class="col-xs-12 padding-v-20"> 
        <ul class="nav nav-tabs tab-titles">
            <li class="{{ !Session::has('contactError') ? 'active' : '' }}"><a data-toggle="tab" href="#description-tab" rel="nofollow">{{Language::trans('Specifikacija')}}</a></li>
            
            <li><a data-toggle="tab" href="#declaration-tab" rel="nofollow">{{Language::trans('Deklaracija')}}</a></li>
            <!-- <li><a data-toggle="tab" href="#service_desc-tab" rel="nofollow">{{Language::trans('Specifikacija')}}</a></li> -->
            <!-- <li><a data-toggle="tab" href="#technical-docs" rel="nofollow">{{Language::trans('Sadržaji')}}</a></li> -->
            <li class="{{ Session::has('contactError') ? 'active' : '' }}">
                <a class="relative" data-toggle="tab" href="#the-comments" rel="nofollow">
                    {{Language::trans('Komentari')}}
                    
                    <div class="JScomment-number line-h"> 
                        <span class="far fa-comment"> 
                            <div class="flex justify-center"> <!-- JS CONTENT --> </div>
                        </span> 
                    </div>
                </a>
            </li>
        </ul>

        <div class="tab-content"> 

            <div id="description-tab" class="tab-pane fade{{ !Session::has('contactError') ? ' in active' : '' }}">

                {{ Product::get_karakteristike($roba_id) }}

                @if(Product::get_proizvodjac_name($roba_id)=='Bosch')
                <script type="text/javascript"></script>

                <div class="loadbeeTabContent" data-loadbee-apikey="{{Options::loadbee()}}" data-loadbee-gtin="{{Product::get_barkod($roba_id)}}" data-loadbee-locale="sr_RS"></div>

                <script src="https://cdn.loadbee.com/js/loadbee_integration.js" async=""></script>
                @endif
                <div id="flix-minisite"></div>
                <div id="flix-inpage"></div>
                <!-- fixmedia -->
                <script type="text/javascript" src="//media.flixfacts.com/js/loader.js" data-flix-distributor="{{Options::flixmedia()}}" data-flix-language="rs" data-flix-brand="{{Product::get_proizvodjac_name($roba_id)}}" data-flix-mpn="" data-flix-ean="{{Product::get_barkod($roba_id)}}" data-flix-sku="" data-flix-button="flix-minisite" data-flix-inpage="flix-inpage" data-flix-button-image="" data-flix-price="" data-flix-fallback-language="en" async>                                    
                </script>
            </div>

            <div id="declaration-tab" class="tab-pane fade">
                {{ Product::get_deklaracija($roba_id) }}
            </div>

            <!-- <div id="service_desc-tab" class="tab-pane fade">
                {{ Product::get_opis($roba_id) }}
            </div> -->

            <div id="technical-docs" class="tab-pane fade">
                @if(Options::web_options(120))
                    @if(count($fajlovi) > 0) 

                        @foreach($fajlovi as $row)
                        <div class="files-list-item">
                            <a class="files-link" href="{{ $row->putanja != null ? Options::domain().$row->putanja : $row->putanja_etaz }}" target="_blank">
                                <img src="{{ Options::domain() }}images/file-icon.png" alt="{{ $row->naziv }}">
                                <div class="files-list-item">
                                    <div class="files-name">{{ Language::trans($row->naziv) }}</div> <!-- {{ Product::getExtension($row->vrsta_fajla_id) }} --> 
                                </div>
                            </a>
                        </div>
                        @endforeach 
                    @endif
                @endif
            </div>


            <div id="the-comments" class="tab-pane fade{{ Session::has('contactError') ? ' in active' : '' }}">
                <div class="row"> 
                    <?php $query_komentary=DB::table('web_b2c_komentari')->where(array('roba_id'=>$roba_id,'komentar_odobren'=>1));
                    if($query_komentary->count() > 0){?>
                        <div class="col-md-6 col-sm-12 col-xs-12"> 
                            <ul class="comments">
                                <?php foreach($query_komentary->orderBy('web_b2c_komentar_id', 'DESC')->get() as $row)
                                { ?>
                                    <li class="comment">
                                        <ul class="comment-content relative">
                                            <li class="comment-name">{{$row->ime_osobe}}</li>
                                            <li class="comment-date">{{$row->datum}}</li>
                                            <li class="comment-rating">{{Product::getRatingStars($row->ocena)}}</li>
                                            <li class="comment-text">{{ $row->pitanje }}</li>
                                        </ul>
                                        <!-- REPLIES -->
                                        @if($row->odgovoreno == 1)
                                        <ul class="replies">
                                            <li class="comment">
                                                <ul class="comment-content relative">
                                                    <li class="comment-name">{{ Options::company_name() }}</li>
                                                    <li class="comment-text">{{ $row->odgovor }}</li>
                                                </ul>
                                            </li>
                                        </ul>
                                        @endif
                                    </li>
                                <?php }?>
                            </ul>
                        </div>
                    <?php } ?>
                    <div class="col-md-6 col-sm-12 col-xs-12"> 
                        <form method="POST" action="{{ Options::base_url() }}comment-add">
                            <label>{{Language::trans('Vaše ime')}}</label>
                            <input name="comment-name" type="text" value="{{ Input::old('comment-name') }}" {{ $errors->first('comment-name') ? 'style="border: 1px solid red;"' : '' }} />
                    
                            <label for="JScomment_message">{{Language::trans('Komentar')}}</label>
                            <textarea class="comment-message" name="comment-message" rows="5"  {{ $errors->first('comment-message') ? 'style="border: 1px solid red;"' : '' }}>{{ Input::old('comment-message') }}</textarea>
                            <input type="hidden" value="{{ $roba_id }}" name="comment-roba_id" />
                            <span class="review JSrev-star">
                                <span>{{Language::trans('Ocena')}}:</span>
                                <i id="JSstar1" class="far fa-star review-star" aria-hidden="true"></i>
                                <i id="JSstar2" class="far fa-star review-star" aria-hidden="true"></i>
                                <i id="JSstar3" class="far fa-star review-star" aria-hidden="true"></i>
                                <i id="JSstar4" class="far fa-star review-star" aria-hidden="true"></i>
                                <i id="JSstar5" class="far fa-star review-star" aria-hidden="true"></i>
                                <input name="comment-review" id="JSreview-number" value="0" type="hidden"/>
                            </span>
                            <div class="capcha text-center"> 
                                {{ Captcha::img(5, 160, 50) }}<br>
                                <span>{{ Language::trans('Unesite kod sa slike') }}</span>
                                <input type="text" name="captcha-string" tabindex="10" autocomplete="off" {{ $errors->first('captcha') ? 'style="border: 1px solid red;"' : '' }}>
                            </div>
                            <button class="pull-right button">{{Language::trans('Pošalji')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- BRENDOVI SLAJDER -->
                <!--  <div class="row">
                     <div class="col-md-12">
                        <div class="dragg JSBrandSlider"> 
                               <?php //foreach(DB::table('proizvodjac')->where('brend_prikazi',1)->get() as $row){ ?>
                            <div class="col-md-12 col-sm-6 end sub_cats_item_brend">
                                <a class="brand-link" href="{{Options::base_url()}}{{ Url_mod::slug_trans('proizvodjac') }}/<?php //echo $row->naziv; ?> ">
                                     <img src="{{ Options::domain() }}<?php //echo $row->slika; ?>" />
                                 </a>
                            </div>
                            <?php //} ?>
                        </div>
                     </div>
                 </div> -->

                 @if(Options::web_options(118))
                 <br>
                     @if(count($vezani_artikli))
                     <h2 class="JSlist-article"><span class="section-title">{{Language::trans('Vezani artikli')}}</span></h2>
                     
                     <div class="JSproducts_slick row">

                        @foreach($vezani_artikli as $vezani_artikl)
                        @if(Product::checkView($vezani_artikl->roba_id))
                        <div class="JSproduct col-md-3 col-sm-4 col-xs-12 no-padding">
                            <div class="shop-product-card relative"> 
                                <!-- PRODUCT IMAGE -->
                                <div class="relative-card relative">
                                    <div class="product-image-wrapper flex relative">

                                        <a class="margin-auto" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}">
                                            <img class="product-image img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}{{ Product::web_slika($vezani_artikl->vezani_roba_id) }}" alt="{{ Product::seo_title($vezani_artikl->vezani_roba_id) }}" />
                                        </a>

                                        <!-- <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}" class="article-details hidden-xs">
                                            <i class="fas fa-search-plus"></i> {{Language::trans('Detaljnije')}}</a> -->
                                        </div>

                                        <h2 class="product-name">
                                            <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}">  {{ Product::short_title($vezani_artikl->vezani_roba_id) }}</a>
                                        </h2>
                                    </div>
                                    <div class="product-meta">
                                        
                                        <div class="text-right hidden"> 
                                            <span class="review">
                                                {{ Product::getRating($vezani_artikl->roba_id) }}
                                            </span>
                                        </div>

                                        <div class="flex flex-wrap-container"> 
                                            <!-- PRODUCT PRICE -->
                                            <div>
                                                <div class="price-holder text-center"> {{ Cart::cena(Product::get_price_vezani($vezani_artikl->roba_id,$vezani_artikl->vezani_roba_id)) }}</div>
                                            </div>   

                                            @if($vezani_artikl->flag_cena == 1)
                                            <div class="add-to-cart-container text-center">
                                                <!-- WISH LIST  --> 
                                                @if(Cart::kupac_id() > 0)
                                                <button class="hidden like-it JSadd-to-wish" data-roba_id="{{$vezani_artikl->roba_id}}" title="{{Language::trans('Dodaj u listu želja')}}"><i class="far fa-heart"></i></button>
                                                @else
                                                <button class="hidden like-it JSnot_logged" data-roba_id="{{$vezani_artikl->roba_id}}" title="{{Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
                                                @endif   

                                                @if(Product::getStatusArticle($vezani_artikl->roba_id) == 1)
                                                    @if(Cart::check_avaliable($vezani_artikl->vezani_roba_id) > 0)
                                                        <button class="buy-btn button JSadd-to-cart-similar text-uppercase" data-vezani_roba_id="{{ $vezani_artikl->vezani_roba_id }}" data-roba_id="{{ $vezani_artikl->roba_id }}">
                                                            <span class="cart-product inline-block v-align"></span>
                                                            <span> {{ Language::trans('U korpu') }} </span>
                                                        </button>
                                                    <!-- <input type="text" class="JSkolicina linked-articles-input like-it" value="1" onkeypress="validate(event)"> -->
                                                   
                                                    @else  
                                                   
                                                    <button class="not-available button text-uppercase">{{Language::trans('Nije dostupno')}}</button>
                                                   
                                                    @endif
                                                   
                                                    @else
                                                    <button class="buy-btn button text-uppercase custom-status">
                                                        {{ Product::find_flag_cene(Product::getStatusArticle($vezani_artikl->roba_id),'naziv') }}
                                                    </button>
                                                @endif
                                            </div>
                                            @endif
                                        </div>
                                </div>

                                <div class="currency-info text-bold"><span>**{{ Language::trans('cene su izražene u RSD') }}</span></div>
                                <!-- ADMIN BUTTON -->
                                @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                                <a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$vezani_artikl->vezani_roba_id}}" href="#">{{Language::trans('IZMENI ARTIKAL')}}</a>
                                @endif
                            </div>
                        </div>
                        @endif
                        @endforeach 
                    </div> 
                    @endif
                @endif

                @if(count(Product::get_related($roba_id)))
                    <!-- RELATED PRODUCTS --> 
                    <br>
                    <h2 class="JSlist-article"><span class="section-title">{{Language::trans('Srodni proizvodi')}}</span></h2>
                    <div class="JSproducts_slick row">
                        @foreach(Product::get_related($roba_id) as $row)
                            @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                        @endforeach
                    </div>
                @endif

            </div>
        </div> 
    </div> 
</main>

<script type="text/javascript">
    if($('.discount').width() < $('.mp-price').width()) {
        $('.discount').css('width', $('.mp-price').width());
    }
</script>
<!-- ARTICLE_DETAILS.blade -->

@endsection