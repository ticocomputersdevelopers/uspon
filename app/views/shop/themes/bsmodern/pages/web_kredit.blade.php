@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
 
<div class="sm-padd-15"> 
	<br>
	<h1 class="text-center"> {{ Language::trans('Web kredit') }} </h1>
	<br>
	<div class="flex justify-center">
		<img class="img-responsive" src="{{ Options::domain() }}images/web_credit_page_dec.jpg" alt="web_kredit">
		<!-- <img class="img-responsive" src="{{ Options::domain() }}images/web_credit_page.jpg" alt="web_kredit"> -->
		<!-- <img class="img-responsive hidden-xs" src="{{ Options::domain() }}images/web_credit_page_november.jpg" alt="web_kredit">
		<img class="img-responsive visible-xs" src="{{ Options::domain() }}images/web_credit_page_november_mob.jpg" alt="web_kredit_mob"> -->
	</div>
	
	<div class="row flex-me xs-d-blog">
		<div class="col-md-4 col-sm-4 col-xs-12 no-padding wrapper">
			<div class="card-credit text-center">
				<div class="image-card-credit vert-align inline-block"> </div>
				<h3> {{ Language::trans('Kupovina na rate za 20 minuta') }} </h3>
				<p> {{ Language::trans('Kupovina na rate za 20 minuta je odličan način da olakšate kupovinu i rasteretite svoje mesečne obaveze.') }} </p>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 no-padding wrapper">
			<div class="card-credit text-center">
				<div class="image-card-credit no-paper-icon vert-align inline-block"> </div>
				<h3> {{ Language::trans('Bez papira') }} </h3>
				<p> {{ Language::trans('Svi podaci se unose i proveravaju onlajn. Do 60.000 RSD kupovine, dovoljni su Vam samo lična karta ili pasoš, dok su za veće iznose iznose potrebna i poslednja 3 mesečna izvoda po tekućem računu banke u kojoj primate zaradu. Potrebno je da izvodi budu u PDF formatu. ') }} 
				</p>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 no-padding wrapper">
			<div class="card-credit text-center">
				<div class="image-card-credit online-icon no-paper-icon vert-align inline-block"> </div>
				<h3> {{ Language::trans('Bez ijednog dolaska u banku - 100% online') }} </h3>
				<p> {{ Language::trans('Kratak video razgovor s agentom banke, bez odlaska u filijalu, čak ni za potpisivanje ugovorne dokumentacije.') }} 
				</p>
			</div>
		</div>
	</div>

	<div class="bg-gray">
		<h3 class="text-center"> {{ Language::trans('Koje su pogodnosti Web kredita?') }} </h3>
		<div class="row flex-me xs-d-blog">
			<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
				<div class="card-credit text-center">
					<div class="image-card-benefits benefit_first_icon vert-align inline-block"> </div>
					<p> {{ Language::trans('Web kredit je dostupan svim građanima u iznosu od 8.000 do 400.000 RSD.') }} </p>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
				<div class="card-credit text-center">
					<div class="image-card-benefits benefit_second_icon vert-align inline-block"> </div>
					<p> {{ Language::trans('Kamatna stopa je fiksna i rata ostaje ista tokom celog perioda otplate. Vi sami birate broj rata (od 3 do 23). Prevremena otplata kredita je bez naknade.') }} 
					</p>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
				<div class="card-credit text-center">
					<div class="image-card-benefits benefit_third_icon vert-align inline-block"> </div>
					<p> {{ Language::trans('Možete aplicirati i ukoliko niste klijent Raiffeisen banke. Tekući račun će Vam biti automatski otvoren u toku realizacije kredita, bez naknade održavanja u prvih 12 meseci. Prenos zarade nije obavezan.') }} 
					</p>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
				<div class="card-credit text-center">
					<div class="image-card-benefits benefit_fourth_icon vert-align inline-block"> </div>
					<p> {{ Language::trans('Biće Vam omogućeno besplatno mobilno i internet bankarstvo, kao i obaveštenja, radi lakšeg praćenja otplate kredita.') }} 
					</p>
				</div>
			</div>
		</div>
	</div>

	<div>
		<br class="hidden-xs"> <br>  
		<h3 class="text-center"> {{ Language::trans('Ukoliko ste već klijent Raiffeisen banke') }} </h3> 
		<br class="hidden-xs"> <br>
		<p class="text-center"> {{ Language::trans('neće Vam se otvarati novi tekući račun. Ukoliko primate zaradu u Raiffeisen banci, nisu Vam potrebni mesečni izvodi čak ni za kupovinu preko 60.000 RSD.') }} </p>  
		<br class="hidden-xs"> <br> <br>

		<h3 class="text-center"> {{ Language::trans('Šta je sve potrebno da biste aplicirali za Web kredit?') }} </h3> <br class="hidden-xs"> <br>

		<div class="row terms-credit flex">
			<div class="col-md-2 col-sm-2 col-xs-12 text-center">
				<div class="image-card-apply apply_first_icon vert-align inline-block"> </div>
			</div>
			<div class="col-md-10 col-sm-10 col-xs-12">
				<h3> {{ Language::trans('Minimalni uslovi') }} </h3>
				<p>  {{ Language::trans('Da biste aplicirali, potrebno je da budete zaposleni ili penzioner, sa mesečnom zaradom/penzijom većom od 14.000 RSD. Ukoliko ste zaposleni, neophodno je da radite kod trenutnog poslodavca minimum 6 meseci. Potrebno je da imate bar 22 godine u trenutku podnošenja zahteva za kredit, i da niste stariji od 68 godina u trenutku otplate poslednje rate kredita.') }} </p>
			</div>
		</div>

		<div class="row terms-credit flex">
			<div class="col-md-2 col-sm-2 col-xs-12 text-center">
				<div class="image-card-apply apply_second_icon vert-align inline-block"> </div>
			</div>
			<div class="col-md-10 col-sm-10 col-xs-12">
				<h3> {{ Language::trans('Važeća lična karta ili pasoš') }} </h3>
				<p>  {{ Language::trans('Ukoliko lični dokument ne sadrži adresu prebivališta, potrebno je da pripremite i:') }} </p>
				<ul> 
					<li> {{ Language::trans('dokument koji sadrži adresu prebivališta (očitanu ličnu kartu, saobraćajnu dozvolu ili rešenje o utvrđivanju poreza na imovinu) ili ') }} </li>
					<li> {{ Language::trans('dokument koji sadrži adresu prebivališta (račun za telefon, električnu energiju ili komunalne usluge koji glasi na Vaše ime')}} </li>
				</ul>
			</div>
		</div>

		<div class="row terms-credit flex">
			<div class="col-md-2 col-sm-2 col-xs-12 text-center">
				<div class="image-card-apply apply_third_icon vert-align inline-block"> </div>
			</div>
			<div class="col-md-10 col-sm-10 col-xs-12">
				<h3> {{ Language::trans('Za iznose kredita preko 60.000 RSD potrebni su mesečni izvodi od banke') }} </h3>
				<p>  {{ Language::trans('Potrebno je priložiti poslednja 3 mesečna izvoda po tekućem računu na koji primate zaradu u elektronskoj formi. Izvode u PDF formatu obično dobijate i-mejlom od banke u kojoj primate zaradu ili ih možete preuzeti kroz internet ili mobilno bankarstvo. Apliciranje za WEB kredit nećete moći da završite ukoliko dostavite skenirane izvode, platne listiće i druga dokumenta. ') }} </p>
			</div>
		</div>

		<div class="row terms-credit flex">
			<div class="col-md-2 col-sm-2 col-xs-12 text-center">
				<div class="image-card-apply apply_fourth_icon vert-align inline-block"> </div>
			</div>
			<div class="col-md-10 col-sm-10 col-xs-12">
				<h3> {{ Language::trans('Broj telefona u domaćoj mreži') }} </h3>
				<p>  {{ Language::trans('Ovaj broj telefona će se koristiti prilikom validacije i dalje komunikacije.') }} </p>
			</div>
		</div>

		<div class="row terms-credit flex">
			<div class="col-md-2 col-sm-2 col-xs-12 text-center">
				<div class="image-card-apply apply_fifth_icon vert-align inline-block"> </div>
			</div>
			<div class="col-md-10 col-sm-10 col-xs-12">
				<h3> {{ Language::trans('Uređaj sa kamerom') }} </h3>
				<p>  {{ Language::trans('Računar ili telefon koji ima kameru sa zvukom je potreban za kratak video razgovor sa agentom banke prilikom identifikacije. U svakom trenutku aplikaciju možete nastaviti na drugom uređaju, svi uneti podaci ostaju sačuvani.') }} </p>
			</div>
		</div>
	</div>


	<div>
		<iframe width="700" height="400" src="https://www.youtube.com/embed/VuPYtILjeGY?si=4zp4vRZ2ZfHJm7KY" title="WEB kredit - Uputstvo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen="" style="margin: auto; display: flex;"></iframe>
		<!-- <a href="https://www.youtube.com/watch?v=CBKBrVf2c0Y" class="text-center flex justify-center" style="text-decoration: underline" target="_blank">https://www.youtube.com/watch?v=CBKBrVf2c0Y</a> -->
		<br class="hidden-xs"> <br>  
		<h3 class="text-center"> {{ Language::trans('Kako izgleda konkretan primer kupovine Web kreditom') }}? </h3> 
		<br class="hidden-xs"> <br> 


		@if(date('Y-m-d') >= '2025-01-01')
		<p> 
			{{Language::trans('Odlučili ste se da kupite artikal čija je vrednost 50.000 RSD i aplicirate za navedeni iznos online kredita. To znači da će iznos vaše mesečne rate biti 4.488 RSD na period otplate od 12 meseci. Ukupan trošak kamate posle godinu dana je 3.858 RSD, što se može videti i u tabeli.')}}
			<br><br>  
		</p>
		<table class="table table-condensed table-credit">
			<tbody>
				<tr> 
					<td> {{ Language::trans('Iznos kredita') }} </td>
					<td> 50.000 RSD </td>
				</tr>
				<tr> 
					<td> {{ Language::trans('Broj rata') }} </td>
					<td> 12 </td>
				</tr>
				<tr> 
					<td> {{ Language::trans('Nominalna kamatna stopa') }} </td>
					<td> 13.95% (godišnja, fiksna) </td>
				</tr>
				<tr> 
					<td> {{ Language::trans('Mesečna rata') }} </td>
					<td>  4.488 RSD </td>
				</tr>
				<tr> 
					<td> <strong>{{ Language::trans('EKS (na dan 01.01.2025.)') }} </strong> </td>
					<td> <strong> 12.85% </strong> </td>
				</tr>
				<tr> 
					<td colspan="2"> <div class="text-center">{{ Language::trans('Troškovi koji padaju na teret korisnika kredita') }} </div></td>
				</tr>
				<tr> 
					<td> {{ Language::trans('Trošak kamate') }} </td>
					<td> 3.858 RSD </td>
				</tr>
			</tbody>
		</table>
		@else
		<p> 
			{{Language::trans('Odlučili ste se da kupite televizor čija je vrednost 40.000 RSD i aplicirate za navedeni iznos online kredita. To znači da će iznos vaše mesečne rate biti 3.703 RSD na period otplate od 12 meseci. Ukupan trošak kamate posle godinu dana je 4.442 RSD, što se može videti i u tabeli.')}}
			<br><br>  
		</p>
		<table class="table table-condensed table-credit">
			<tbody>
				<tr> 
					<td> {{ Language::trans('Iznos kredita') }} </td>
					<td> 40.000 RSD </td>
				</tr>
				<tr> 
					<td> {{ Language::trans('Broj rata') }} </td>
					<td> 12 </td>
				</tr>
				<tr> 
					<td> {{ Language::trans('Nominalna kamatna stopa') }} </td>
					<td> 19.90% (godišnja, fiksna) </td>
				</tr>
				<tr> 
					<td> {{ Language::trans('Mesečna rata') }} </td>
					<td>  3.703 RSD </td>
				</tr>
				<tr> 
					<td> <strong>{{ Language::trans('EKS (na dan 05.12.2024.)') }} </strong> </td>
					<td> <strong> 19.07% </strong> </td>
				</tr>
				<tr> 
					<td colspan="2"> <div class="text-center">{{ Language::trans('Troškovi koji padaju na teret korisnika kredita') }} </div></td>
				</tr>
				<tr> 
					<td> {{ Language::trans('Trošak kamate') }} </td>
					<td> 4.442 RSD </td>
				</tr>
			</tbody>
		</table>
		@endif

		<br><br><br class="hidden-xs">
	</div>

	@if(date('Y-m-d') >= '2023-11-21' && date('Y-m-d') <= '2023-12-04')
	<div class="flex justify-center">
		<picture>
			<source media="(min-width:650px)" srcset="{{ Options::base_url() }}images/black-friday-mobile.jpg">
			<img src="{{ Options::base_url() }}images/black-friday-desk.jpg" alt="black friday web credit" class="img-responsive" />
		</picture>
		<!-- <img class="img-responsive" src="{{ Options::domain() }}images/web_credit_page_second_new.jpg" alt="web_kredit"> -->
		<!-- <img class="img-responsive hidden-xs" src="{{ Options::domain() }}images/web_credit_page_second_november.jpg" alt="web_kredit">
		<img class="img-responsive visible-xs" src="{{ Options::domain() }}images/web_credit_page_second_november_mob.jpg" alt="web_kredit"> -->
	</div>
	@endif
	<br><br><br>
	
</div>

@endsection     