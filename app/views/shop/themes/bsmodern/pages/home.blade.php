@extends('shop/themes/'.Support::theme_path().'templates/main')
 
@section('page')
    @include('shop/themes/'.Support::theme_path().'partials/products/action_type')
    
    @if(Options::web_options(136)==0)
        @if(Options::web_options(208)==1)
            <!-- ARTICLES AT FRONT PAGE -->
            <div class="JSaction_offset">
                <h2><span class="section-title JSInlineShort" data-target='{"action":"front_admin_label","id":"1"}'>{{ Language::trans(Support::front_admin_label(1)) }}</span></h2>
                 
                <div class="JSproducts_slick">
                    @foreach(Articles::mostPopularArticles() as $row)
                        @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                    @endforeach
                </div>
            </div>
        
            <div class="JSaction_offset">
                @if(count(Articles::bestSeller()))
                <h2><span class="section-title JSInlineShort" data-target='{"action":"front_admin_label","id":"2"}'>{{ Language::trans(Support::front_admin_label(2)) }}</span></h2>
               
                <div class="JSproducts_slick">
                    @foreach(Articles::bestSeller() as $row)
                        @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                    @endforeach
                </div>
                @endif
            </div>
          
            <div class="JSaction_offset">
                <h2><span class="section-title JSInlineShort" data-target='{"action":"front_admin_label","id":"3"}'>{{ Language::trans(Support::front_admin_label(3)) }}</span></h2>
                
                <div class="JSproducts_slick">
                    @foreach(Articles::latestAdded() as $row)
                        @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                    @endforeach
                </div>
            </div>
        @endif
    @else
       
        <h2><span class="section-title JSInlineShort" data-target='{"action":"home_all_articles"}'>{{ Language::trans(Support::title_all_articles()) }}</span></h2>
       
        <div class="JSproduct-slider">
            @foreach($articles as $row)
                @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
            @endforeach
        </div>
         
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            {{ Paginator::make($articles, $count_products, $limit)->links() }}
        </div>
    @endif  

<!--     <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12"> 
            <div class="JSBrandSlider brandSlider">  
                <?php foreach(All::getManufacturersShortList() as $row){ ?>
                <div class="text-center">
                    <a class="inline-block" href="{{Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac')}}/{{ Url_mod::slug_trans($row->naziv) }}"> 
                        <img alt="{{ ($row->naziv) }}" src="{{ Options::domain() }}<?php echo $row->slika; ?>" /> 
                    </a>
                </div>
                <?php } ?>
            </div>  
        </div>
    </div> -->
 
    <script>
        $(document).ready(function(){
            @if(Session::has('login_success')) 
 
                bootboxDialog({ message: "<p>{{ Session::get('login_success') }}</p>" }); 

            @endif
            
            @if(Session::has('registration_success')) 
   
                bootboxDialog({ message: "<p>{{ Session::get('registration_success') }}</p>" }); 

            @endif

            @if(Session::has('loggout_succes')) 
   
                bootboxDialog({ message: "<p>{{ Session::get('loggout_succes') }}</p>" }, 2200); 

            @endif

            @if(Session::has('confirm_registration_message')) 
 
                bootboxDialog({ message: "<p>{{ Session::get('confirm_registration_message') }}</p>" }); 

            @endif
        });
    </script>


@endsection