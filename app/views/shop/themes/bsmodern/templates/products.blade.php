<!DOCTYPE html>
<html lang="sr">
<head>
	@include('shop/themes/'.Support::theme_path().'partials/head')

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "WebPage",
			"name": "<?php echo $title; ?>",
			"description": "<?php echo $description; ?>",
			"url" : "<?php echo Options::base_url().$url; ?>",
			"publisher": {
			"@type": "Organization",
			"name": "<?php echo Options::company_name(); ?>"
		}
	}
</script>
</head>
<body id="product-page" 
@if($strana == All::get_page_start()) id="start-page"  @endif 
@if(Support::getBGimg() != null) 
style="background-image: url({{Options::domain()}}{{Support::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" 
@endif
>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5TRTLQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

@include('shop/themes/'.Support::theme_path().'partials/menu_top')
 
@include('shop/themes/'.Support::theme_path().'partials/header')

<!-- PRODUCTS.blade -->
<main class="d-content JSmain relative"> 
	<div class="container"> 
		<div class="row">  
			
			@if($strana!='akcija' and $strana!='tip')
			<ul class="breadcrumb">
				@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
				{{ Url_mod::breadcrumbs2()}}
				@elseif($strana == 'proizvodjac')
				<li>
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('brendovi') }}">{{ Language::trans('Brendovi') }}</a>  
					@if($grupa)
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac') }}/{{ Url_mod::slug_trans($proizvodjac) }}">{{ All::get_manofacture_name($proizvodjac_id) }}</a>
					{{ Groups::get_grupa_title($grupa) }}
					@else
					{{ All::get_manofacture_name($proizvodjac_id) }} 
					@endif
				</li>
				@else					
				<li><a href="{{ Options::base_url() }}">{{ All::get_title_page_start() }}</a></li>
				<li>{{$title}}</li>
				@endif
			</ul>
			@endif   

			<div class="col-md-3 col-sm-12 col-xs-12">  

				@if($filter_prikazi == 0)
					@if($strana == 'proizvodjac')
						@include('shop/themes/'.Support::theme_path().'partials/categories/manufacturer_category')
					@elseif($strana == 'tip')
						@include('shop/themes/'.Support::theme_path().'partials/categories/tip_category')
					@elseif($strana == 'akcija')
						@include('shop/themes/'.Support::theme_path().'partials/categories/akcija_category')
					@endif
				@endif

				@if(Options::enable_filters()==1 AND $filter_prikazi)
					@include('shop/themes/'.Support::theme_path().'partials/products/filters')
				@endif

				<div class="hiddne-sm hidden-xs">&nbsp; <!-- EMPTY SPACE --></div>
			</div>

 			<div class="col-md-9 col-sm-12 col-xs-12 product-page">

 				@if((!empty($baner_id) AND $banner = Slider::slajder($baner_id) AND count($bannerStavke = Slider::slajderStavke($banner->slajder_id)) > 0) AND !empty($bannerStavka = $bannerStavke[0]))
					<div class="banners-group padding-v-20">

						<a class="bg-img center-block" href="{{ $bannerStavka->link }}" style="background-image: url('{{ Options::domain() }}{{ $bannerStavka->image_path }}');"></a>

					</div>

				@endif

				@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac' and $strana != 'tagovi' and $strana!='tip' and $strana!='akcija')

				<!-- FOR SEO -->
				<h1 class="seo"> <span class="group-title">  {{ Groups::getGrupa($grupa_pr_id) }} </span> </h1>

					@if(isset($sub_cats) and !empty($sub_cats))
					<div class="row sub-group">
						@foreach($sub_cats as $row)
						<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
							<a class="flex" href="{{ Options::base_url()}}{{ $url }}/{{ Url_mod::slug_trans($row->grupa) }}">
								
								@if(isset($row->putanja_slika))
									<img src="{{ Options::domain() }}{{ $row->putanja_slika }}" alt="{{ $row->grupa }}" class=" img-responsive" />
						 		@endif
								
								<span>{{ $row->grupa }}</span>
							</a>
						</div>
						@endforeach

					</div>
					@endif
				@else
					<!-- FOR SEO -->
					<h1 class="seo"> 
						<span class="group-title">  
							@if($strana == 'pretraga')
								{{ Language::trans('Rezultati pretraživanja za pojam') }}: 
							@elseif($strana == 'proizvodjac')
								{{ Language::trans('Proizvodjač') }}: 
							@endif
							<strong> {{ Groups::getSearchResults() }} </strong> 
						</span>
					</h1>
				@endif

				@yield('products_list')

			</div>

		</div>
	</div>
</main>
<!-- PRODUCTS.blade END -->


@include('shop/themes/'.Support::theme_path().'partials/footer')
 
@include('shop/themes/'.Support::theme_path().'partials/popups')


<!-- BASE REFACTORING -->
<input type="hidden" id="base_url" value="{{Options::base_url()}}" />
<input type="hidden" id="in_stock" value="{{Options::vodjenje_lagera()}}" />
<input type="hidden" id="elasticsearch" value="{{ Options::gnrl_options(3055) }}" />

<!-- js includes -->
@if(Session::has('b2c_admin'.Options::server()) OR (Options::enable_filters()==1 AND $filter_prikazi))
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@endif
@if(Session::has('b2c_admin'.Options::server()))
<script src="{{Options::domain()}}js/tinymce_5.1.3/tinymce.min.js"></script> 
<script src="{{ Options::domain()}}js/jquery-ui.min.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
@endif

<script src="{{Options::domain()}}js/shop/translator.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>

@if(Options::header_type()==1)
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
@endif

<!-- MESSAGE FOR QUERY -->
<script>
	@if(Session::has('message'))
		$(document).ready(function(){    
		    bootboxDialog({ message: "<p>" + trans('Uspešno ste prosledili upit') + ".</p>" }, 2200); 
		});
	@endif
</script>

</body>
</html>