<!-- HEADER.blade -->
<header>   
    <div id="JSfixed_header" {{ Options::web_options(321, 'str_data') != '' ? 'style=background-color:' . Options::web_options(321, 'str_data') : '' }}>  
        <div class="container{{(Options::web_options(321)==1) ? '-fluid' : '' }}"> 
            <div class="row flex xs-header-ordered"> 

                <div class="col-md-3 col-sm-3 col-xs-5">
                    
                    <!-- <h1 class="seo">{{ Options::company_name() }}</h1> -->
                    
                    <a class="logo v-align inline-block" href="/" title="{{Options::company_name()}}" rel="nofollow">
                        <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
                    </a>
                </div>

                <div class="col-md-6 col-sm-5 col-xs-12">  
                    <div class="row header-search"> 
                    
                        @if(Options::gnrl_options(3055) == 0)
        
                            <div class="col-md-3 col-sm-3 col-xs-3 no-padding JSselectTxt hidden">  
                               
                                {{ Groups::firstGropusSelect('2') }} 
                    
                            </div>
                    
                        @else  
                    
                            <input type="hidden" class="JSSearchGroup2" value="">
                    
                        @endif 

                         
                        <div class="{{ Options::gnrl_options(3055) == 0 ? 'col-xs-12' : 'col-xs-12' }} no-padding JSsearchContent2">  
                            <div class="relative"> 
                                <form autocomplete="off">
                                    <input type="text" aria-label="Search" id="JSsearch2" />
                                </form>      
                                <button onclick="search2()" class="JSsearch-button2" aria-label="Pretraga"> <span class="header-icons inline-block v-align"></span>  </button>
                            </div>
                        </div> 
                    </div> 
                </div>

                
                <div class="col-md-3 col-sm-3 col-xs-5">
                    <div class="flex distance-header-icons">
                        @if(Cart::kupac_id() > 0)
                            <button class="like-it hidden JSadd-to-wish relative"  title="{{ Language::trans('Dodaj na listu želja') }}"><i class="far fa-heart"></i></button> 
                        @else
                            <button class="like-it hidden JSnot_logged"  title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
                        @endif       

                        <div class="dropdown inline-block">

                            <button class="dropdown-toggle login-btn" type="button" data-toggle="dropdown">
                                <span class="header-icons user-icon inline-block v-align"></span> 
                                <div class="text-uppercase line-h icon-text"> {{Language::trans('nalog')}} </div>
                            </button>

                            @if(Session::has('b2c_kupac'))

                            <ul class="dropdown-menu logged-dropdown-user">
                                <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}" rel="nofollow">{{ WebKupac::get_user_name() }}</a>

                                <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}" rel="nofollow">{{ WebKupac::get_company_name() }}</a>

                                <div> 
                                    <span class="JSbroj_wish fas fa-heart"> {{ Cart::broj_wish() }} </span>  

                                    <a id="logout-button" class="inline-block" href="{{Options::base_url()}}logout" rel="nofollow">{{ Language::trans('Odjavi se') }}</a>
                                </div>
                            </ul>
                            @else 
                                <ul class="dropdown-menu login-dropdown">
                                    <!-- ====== LOGIN MODAL TRIGGER ========== -->
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#loginModal" rel="nofollow">
                                        <i class="fas fa-user"></i> {{ Language::trans('Prijavi se') }}</a>
                                    </li>
                                    <li>
                                        <a href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}" rel="nofollow"> 
                                        <i class="fas fa-user-plus"></i> {{ Language::trans('Registracija') }}</a>
                                    </li>
                                </ul>
                            @endif
                        </div>                    

                        @include('shop/themes/'.Support::theme_path().'partials/cart_top')
                    </div>
                </div>

                <!-- RESPONSIVE BUTTON -->
                <div class="hidden-md hidden-lg col-sm-1 col-xs-2 text-center padding-10">
                    <div class="resp-nav-btn"><span class="fas fa-bars"></span></div>
                </div>

            </div> 
        </div>  
    </div> 
</header>

<div class="menu-background">   
    <div class="container"> 
        <div id="responsive-nav" class="row">

            @if(Options::category_view()==1)
                @include('shop/themes/'.Support::theme_path().'partials/categories/category_ajax')
            @endif               

            <div class="col-md-12 col-sm-12 col-xs-12">

                <ul class="main-menu text-white">

                    <li class="categories-title text-center text-white">
                        <i class="fas fa-bars"></i>
                        {{ Language::trans('Proizvodi') }} 
                    </li> 
                    @foreach(All::header_menu_pages() as $row)
                    <li>
                        @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a>
                        <ul class="drop-2">
                            @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                                <ul class="drop-3">
                                    @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                    <li> 
                                        <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                            <li>
                                <a href="javascript:void()" class="warranty_modal_open">Provera garancije</a>
                            </li>
                        </ul>
                        @else   
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                        @endif                    
                    </li>                     
                    @endforeach 

                    @if(Options::web_options(121)==1)
                    <?php $konfiguratori = All::getKonfiguratos(); ?>
                    @if(count($konfiguratori) > 0)
                    @if(count($konfiguratori) > 1)
                    <li>
                        
                        <a href="#!" rel="nofollow">{{Language::trans('Konfiguratori')}}</a>

                        <ul class="drop-2">
                            @foreach($konfiguratori as $row)
                            <li>
                                <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $row->konfigurator_id }}">{{ Language::trans($row->naziv) }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    @else
                    <li>
                        <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $konfiguratori[0]->konfigurator_id }}">
                            {{Language::trans('Konfigurator')}}
                        </a>
                    </li>
                    @endif
                    @endif
                    @endif
                    <li class="web-credit" title="Web kredit">
                        <a class="text-uppercase text-bold" href="{{ AdminOptions::base_url().'raiffeisen-web-kredit' }}" target="_blank">{{ Language::trans('Web kredit') }}</a>
                    </li>  
                </ul> 
            </div> 
        </div>
    </div>    
</div> 


<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content" >
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <p class="modal-title">{{ Language::trans('Dobrodošli') }}</p>
                <p class="no-margin">{{ Language::trans('Za pristup Vašem nalogu unesite Vaš e-mail i lozinku') }}.</p>
            </div>

            <div class="modal-body"> 
                <label for="JSemail_login">{{ Language::trans('E-mail') }}</label>
                <input id="JSemail_login" type="text" value="" autocomplete="off">
        
                <label for="JSpassword_login">{{ Language::trans('Lozinka') }}</label>
                <input autocomplete="off" id="JSpassword_login" type="password" value=""> 
            </div>

            <div class="modal-footer text-right">
                <a class="inline-block button" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{Language::trans('Registruj se')}}</a>
                
                <button type="button" onclick="user_login()" class="button">{{ Language::trans('Prijavi se') }}</button>
                
                <button type="button" onclick="user_forgot_password()" class="forgot-psw pull-left">{{ Language::trans('Zaboravljena lozinka') }}</button>

                <div class="field-group error-login JShidden-msg" id="JSForgotSuccess"><br>
                    {{ Language::trans('Novu lozinku za logovanje dobili ste na navedenoj e-mail adresi') }}.
                </div> 
            </div>
        </div>   
    </div>
</div>

<!-- Warranty Modal -->
<div class="warranty_modal_overlay"></div>

<div class="warranty_modal">
    <div class="warranty_modal_header text-right">
        <div class="flex justify-between mb-2">
            <h4>Proveri garanciju</h4>
            <button class="warranty_modal_close" aria-label="Close modal">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="warranty_modal_content">
        <div>
            <label for="serijski_broj">Serijski broj:</label>
            <input type="text" name="serijski_broj" value="" />
        </div>

       <button aria-label="Proveri garanciju" class="btn btn-block text-white JSCheckWarranty">Proveri</button>
    </div>

    <!-- Warranty Message -->
    <div class="warranty_msg text-center text-green"></div>
</div>

@if($strana != "kontakt")
<!-- OPEN MODAL ON CLICK PROVERITE STANJE BUTTON --> 
<div class="modal fade" id="checkState" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body"> 
                <h4 class="modal-title">
                    {{ Language::trans('Pozovite naš call centar na broj') }}

                    @if(Options::company_phone())
                        <a href="tel:{{ Options::company_phone() }}">
                            {{ Options::company_phone() }}
                        </a>
                    @endif

                    {{ Language::trans('ili nam prosledite upit') }}: 

                </h4>

                <form method="POST" action="{{ Options::base_url() }}enquiry-message-send">
                    <input type="hidden" id="JSStateArtID" name="roba_id" value="">
                   
                    <div>
                        <label id="label_name">{{ Language::trans('Vaše ime') }} *</label>
                        <input class="contact-name" name="contact-name" id="JScontact_name" type="text" value="{{ Input::old('contact-name') }}">
                        <div class="error red-dot-error">{{ $errors->first('contact-name') ? $errors->first('contact-name') : "" }}</div>
                    </div> 

                    <div>
                        <label id="label_email">{{ Language::trans('Vaša e-mail adresa') }} *</label>
                        <input class="contact-email" name="contact-email" id="JScontact_email" type="text" value="{{ Input::old('contact-email') }}" >
                        <div class="error red-dot-error">{{ $errors->first('contact-email') ? $errors->first('contact-email') : "" }}</div>
                    </div>      
                    <div>
                        <label id="label_phone">{{ Language::trans('Telefon') }} *</label>
                        <input class="contact-phone" name="contact-phone" id="JScontact_phone" type="text" value="{{ Input::old('contact-phone') }}" >
                        <div class="error red-dot-error">{{ $errors->first('contact-phone') ? $errors->first('contact-phone') : "" }}</div>
                    </div>  
                    <div>   
                        <label id="label_message">{{ Language::trans('Vaša poruka') }} </label>
                        <textarea class="contact-message" name="contact-message" rows="5" id="message">{{ Input::old('contact-message') }}</textarea>
                        <div class="error red-dot-error">{{ $errors->first('contact-message') ? $errors->first('contact-message') : "" }}</div>
                    </div> 

                    @if($strana == 'artikal' && $strana != 'korpa')
                    <div class="capcha text-center"> 
                        {{ Captcha::img(5, 160, 50) }}<br>
                        <span>{{ Language::trans('Unesite kod sa slike') }}</span>
                        <input type="text" name="captcha-string" tabindex="10" autocomplete="off">
                        <div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
                    </div>
                    @endif

                    <div class="text-right"> 
                        <button type="submit" class="button">{{ Language::trans('Pošalji') }}</button>
                    </div>
                </form>
            </div>

        </div>   
    </div>
</div>
@endif
<!-- HEADER.blade END