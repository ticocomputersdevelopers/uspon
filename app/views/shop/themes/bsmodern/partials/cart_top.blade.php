
<div class="header-cart-container relative">  
	

	<a class="header-cart inline-block text-center relative" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}" rel="nofollow">
		
	
		<span class="header-icons cart-icon inline-block v-align"></span> 
		<div class="text-uppercase line-h icon-text"> {{Language::trans('korpa')}} </div>
		<span class="JScart_num badge"> {{ Cart::broj_cart() }} </span> 		
		
		<!-- <input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	 -->
	</a>

	<div class="JSheader-cart-content hidden-sm hidden-xs">
		@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
	</div>
</div>
