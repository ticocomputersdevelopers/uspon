<!-- PRODUCT ON LIST -->
<div class="shop-product-card-list relative"> 

	@if(All::provera_akcija($row->roba_id)) 
	<span class="ribbon-sale"><span> {{ Language::trans('Akcija') }} </span></span>  
	@endif

	<div class="row"> 

		<div class="col-md-3 col-sm-3 col-xs-12 sm-no-padd product-image-wrapper">
			@if(!empty(Product::design_id($row->roba_id)))
			<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="flex full-height">
				<img class="product-image margin-auto img-responsive JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($row->roba_id)}}_1.jpg?k=0.4491390433183766" />
			</a>
			@else
			<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="flex full-height">
				<img class="product-image margin-auto img-responsive JSlazy_load" src="{{Options::base_url()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
			</a>
			@endif
		</div> 

		<div class="col-md-9 col-sm-9 col-xs-12"> 
			<h2 class="product-name">
				<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}"> {{ Product::short_title($row->roba_id) }}</a> 
			</h2>

			@if(AdminOptions::web_options(153)==1)
			<div class="generic_car_list col-md-6 col-sm-7 col-xs-12 no-padding">
				{{ Product::get_web_roba_karakteristike_short($row->roba_id) }}
			</div>
			@endif

			<div class="text-right price-holder price-product-on-list @if((Product::getStatusArticle($row->roba_id) == 1) AND (!Cart::check_avaliable($row->roba_id) > 0)) hidden @endif"> 
				<span>{{ Cart::cena(Product::get_price($row->roba_id)) }}</span>
				@if(All::provera_akcija($row->roba_id))
				<span class="product-old-price">{{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
				@endif 
			</div>

			<div class="text-right hidden"><span class="review">{{ Product::getRating($row->roba_id) }}</span></div>

			<div class="add-to-cart-container text-right"> 

				@if(Cart::kupac_id() > 0)
				<button class="like-it hidden JSadd-to-wish" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><i class="far fa-heart"></i></button> 
				@else
			  	<button class="like-it hidden JSnot_logged" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
				@endif		
				<!--  <div class="">Šifra artikla: {{ Product::get_sifra($row->roba_id) }}</div> -->

				@if(Product::getStatusArticle($row->roba_id) == 1)
					@if(Cart::check_avaliable($row->roba_id) > 0)

						@if(!Product::check_osobine($row->roba_id)) 
						<button class="buy-btn button JSadd-to-cart" data-roba_id="{{$row->roba_id}}">{{ Language::trans('U korpu') }}</button>

						@else

						<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="buy-btn button">{{ Language::trans('Vidi artikal') }}</a>			    
						@endif

						@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
						<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
							<i class="fas fa-exchange-alt" aria-hidden="true"></i>
						</button>
						@endif	

						@else 	

							<!-- <button class="not-available button"> {{ Language::trans('Nije dostupno') }}</button> -->
							<button class="not-available button text-uppercase JSenquiry" data-roba_id="{{$row->roba_id}}"> {{ Language::trans('Proverite stanje') }}</button>

							@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
							<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
								<i class="fas fa-exchange-alt" aria-hidden="true"></i>
							</button>
							@endif		 
						@endif
						
					@else	 

						<button class="buy-btn button">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>

						@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
						<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
							<i class="fas fa-exchange-alt" aria-hidden="true"></i>
						</button>
					@endif		
				@endif
			</div>
		</div> 
		
		<div class="text-right">
			<span class="currency-info">**{{ Language::trans('cene su izražene u RSD') }}</span>
	

			@if(All::provera_akcija($row->roba_id)) 
				@if(All::action_start($row->roba_id) || All::action_deadline_article($row->roba_id))
					<span class="action_text_info">
						{{ Language::trans('Akcija traje od') }} {{ All::action_start($row->roba_id) }}. {{ Language::trans('do') }} {{ All::action_deadline_article($row->roba_id) }}.
						{{ Language::trans('ili do isteka zaliha') }}
					</span>
				@endif
			@endif
		</div>

		<!-- OPIS KARAKTERISTIKE -->
	<!-- <div class="col-md-5  ">
		{{ Product::get_karakteristike_short_grupe($row->roba_id) }}
	</div>  -->

		@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
		<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a>
		@endif

	</div>
</div>
