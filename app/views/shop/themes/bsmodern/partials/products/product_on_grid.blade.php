<!-- PRODUCT ON GRID -->
<div class="product-card col-md-3 col-sm-4 col-xs-6 no-padding"> 
	<div class="shop-product-card relative"> 
 
		@if(All::provera_akcija($row->roba_id))
		<div class="sale-label text-white"> 
			<div>{{ Language::trans('Akcija') }}</div>
			<div class="for-sale-price">- {{ Product::getSale($row->roba_id) }} %</div> 
		</div>
		@endif

		<div class="relative-card relative">
			<div class="product-image-wrapper flex relative">
				@if(!empty(Product::design_id($row->roba_id)) AND Options::pitchprint_aktiv() == 1)
				<a class="margin-auto" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">
					<img class="product-image img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif" data-src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($row->roba_id)}}_1.jpg?k=0.4491390433183766" alt="{{ Product::seo_title($row->roba_id) }}" />
				</a>
				@else
				<a class="margin-auto" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">
					<img class="product-image img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif" data-src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}" />
				</a>
				@endif

				<!-- <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="article-details hidden-xs hidden-sm"><i class="fas fa-search-plus"></i> {{ Language::trans('Detaljnije') }}</a> -->

				@if(All::provera_akcija($row->roba_id)) 
					@if(All::provera_datuma_i_tajmera($row->roba_id))
					<input type="hidden" class="JSrok_akcije" value="{{ All::action_deadline($row->roba_id) }}">
					<table class="table product-timer">
						<tbody>
							<tr>
								<td class="JSdays"></td>
								<td class="JShours"></td>
								<td class="JSminutes"></td>
								<td class="JSseconds"></td>
							</tr>
						</tbody> 
					</table>
					@endif 
				@endif
				
				<div class="product-sticker flex">
				    @if( Product::stiker_levo($row->roba_id) != null )
			            <a class="article-sticker-img">
			                <img class="img-responsive" alt="left-sticker" src="{{ Options::domain() }}{{product::stiker_levo($row->roba_id) }}"  />
			            </a>
		            @endif 
		            
		            @if( Product::stiker_desno($row->roba_id) != null )
			            <a class="article-sticker-img clearfix">
			                <img class="img-responsive pull-right" alt="left-sticker" src="{{ Options::domain() }}{{product::stiker_desno($row->roba_id) }}"  />
			            </a>
		            @endif 
	        	</div>
			</div>

		</div>

		<div class="wrap-info">
			<h2 class="product-name">
				<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">
					{{ Product::short_title($row->roba_id) }}  
				</a>
			</h2>
			<div class="product-meta"> 
				<div class="text-right hidden"> 
					<span class="review">
						{{ Product::getRating($row->roba_id) }}
					</span>
				</div>

				@if(All::provera_akcija($row->roba_id)) 
					@if(All::action_start($row->roba_id) || All::action_deadline_article($row->roba_id))
						<div class="action_text_info text-center">
							{{ Language::trans('Akcija traje od') }} {{ All::action_start($row->roba_id) }}. {{ Language::trans('do') }} {{ All::action_deadline_article($row->roba_id) }}.
                    		{{ Language::trans('ili do isteka zaliha') }}
						</div>
					@endif
				@endif

				@if(AdminOptions::web_options(153)==1)
				<div class="generic_car">
					{{ Product::get_web_roba_karakteristike_short($row->roba_id) }}
				</div>
				@endif

				@if(Product::getStatusArticlePrice($row->roba_id))
					<div class="text-center @if((Product::getStatusArticle($row->roba_id) == 1) AND (!Cart::check_avaliable($row->roba_id) > 0)) opacity-0 @endif">
						@if(All::provera_akcija($row->roba_id))
						<span class="product-old-price"> {{ Cart::cena(Product::old_price($row->roba_id)) }}</span>
						@endif     
						
						@if(!All::provera_akcija($row->roba_id))
							<span class="mp_price_grid"> 
								@if(Product::sifra_is($row->roba_id) != '') 
									*{{ Language::trans('MP cena') }} {{ Cart::cena(Product::get_mpcena($row->roba_id)) }} 
								@endif  
							</span>
						@endif  
					</div>	
				@endif  

				<div class="flex flex-wrap-container @if((Product::getStatusArticle($row->roba_id) == 1) AND (Cart::check_avaliable($row->roba_id) > 0) AND (!Product::check_osobine($row->roba_id)) ) flex-initial-cart justify-center @endif">
					@if(Product::getStatusArticlePrice($row->roba_id))
					<div class="price-holder-wrapp 	@if((Product::getStatusArticle($row->roba_id) == 1) AND (!Cart::check_avaliable($row->roba_id) > 0)) hidden @endif">
						<div class="price-holder text-center"> 
							<div> {{ Cart::cena(Product::get_price($row->roba_id)) }} </div>
						</div>
					</div>	
					@endif    

					<div class="add-to-cart-container text-center">  
						@if(Cart::kupac_id() > 0)
						<button class="like-it hidden JSadd-to-wish relative" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}"><i class="far fa-heart"></i></button> 
						@else
					  	<button class="like-it hidden JSnot_logged " data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
						@endif							


						@if(Product::getStatusArticle($row->roba_id) == 1)	
							@if(Cart::check_avaliable($row->roba_id) > 0)

								@if(!Product::check_osobine($row->roba_id)) 

									<button class="buy-btn button JSadd-to-cart text-uppercase" data-roba_id="{{$row->roba_id}}" aria-label="Dodaj u korpu">
										<span class="cart-product inline-block v-align"></span>
										<!-- <span> {{ Language::trans('U korpu') }} </span> -->
									</button>

								@else

									<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="buy-btn button text-uppercase" aria-label="Dodaj u korpu">
										{{ Language::trans('Vidi artikal') }}
									</a>	 
								
								@endif

								@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
									<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
										<i class="fas fa-exchange-alt" aria-hidden="true"></i>
									</button>
								@endif		

							@else  	

								<button class="not-available button text-uppercase JSenquiry" data-roba_id="{{$row->roba_id}}"> {{ Language::trans('Proverite stanje') }}</button>
								<div class="not-ave-price">**Stara cena {{ Cart::cena(Product::get_price($row->roba_id)) }} </div>

								<!-- <button class="not-available button text-uppercase">{{ Language::trans('Nije dostupno') }}</button>	 -->

								@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
									<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
										<i class="fas fa-exchange-alt" aria-hidden="true"></i>
									</button>
								@endif	

							@endif

						@else  
							<button class="buy-btn button text-uppercase custom-status" aria-label="Dodaj u korpu" title="{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}"> 
								<span> {{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }} </span>
							</button>	

							@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
								<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
									<i class="fas fa-exchange-alt" aria-hidden="true"></i>
								</button>
							@endif	 

						@endif 	
					</div>   
				</div>       
			</div>
			
			<div class="currency-info"><span>**{{ Language::trans('cene su izražene u RSD') }}</span></div>
		</div>
		<!-- ADMIN BUTTON -->
		@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
		<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a>
		@endif
		
	</div>
</div>



