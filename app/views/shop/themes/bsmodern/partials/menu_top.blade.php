<!-- MENU_TOP.blade -->
@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
<div id="admin-menu"> 
    <div class="container-fluid text-right">
        <!-- <a href="#!" data-toggle="modal" data-target="#FAProductsModal" rel="nofollow"> <i class="fas fa-clipboard-list"></i> {{ Language::trans('Artikli') }}</a>
        |
        @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
        <a href="#!" id="JSShortAdminSave" rel="nofollow"> <i class="fas fa-save"></i> {{ Language::trans('Sačuvaj izmene') }}</a>
        @endif
        |  -->
        <span class="ms admin-links"><a target="_blank" href="{{ Options::domain() }}admin" rel="nofollow"><i class="fas fa-cogs"></i> {{ Language::trans('Admin Panel') }}</a></span> |
        <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout" rel="nofollow">{{ Language::trans('Odjavi se') }}</a></span>
    </div>
</div> 

@include('shop/front_admin/modals/products')
@include('shop/front_admin/modals/product')
@endif

<div class="preheader">

    <div class="social-icons hidden-sm hidden-xs">  
        {{Options::social_icon()}} 
    </div>

    <div class="container"> 
        <div class="row top-menu relative flex">

            
            <div class="col-md-3 col-sm-4 col-xs-5"> 

                <div class="call-centar">
                    <span class="bg-user-top inline-block v-align"> </span>

                    @if(Options::company_phone())
                    <span class="v-align">
                    {{ Language::trans('Call Centar') }}:
                   
                        <a href="tel:{{ Options::company_phone() }}">
                            {{ Options::company_phone() }}
                        </a>
                
                    </span>
                    @endif
                </div>
               
            </div>   

            <div class="col-md-9 col-sm-8 col-xs-7 row text-right sm-static">
                
                 <ul class="hidden-small JStoggle-content pages_class">
                    @foreach(All::menu_top_pages() as $row)
                    <li><a class="center-block" href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{ Url_mod::page_slug($row->naziv_stranice)->naziv }}</a></li>
                    @endforeach
                </ul> 

                 @if(Options::checkB2B())
                   
                    <a id="b2b-login-icon" href="{{Options::domain()}}b2b/login" class="confirm inline-block">  <i class="fas fa-handshake"></i>B2B</a> 
                 @endif 
            

                @if(Options::stranice_count() > 0)
                <span class="JStoggle-btn inline-block text-white hidden-md hidden-lg">
                   <i class="fas fa-bars"></i>                 
                </span>
                @endif 
                 
            </div>   
 
        </div> 
    </div>
</div>
<!-- MENU_TOP.blade END -->



