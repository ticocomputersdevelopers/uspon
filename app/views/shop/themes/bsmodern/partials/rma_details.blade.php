<svg style="display: none;">
    <style>
  
        table tr td { padding: 6px; border-bottom: 1px solid #ccc; }
        
        table tr th { 
            padding: 6px; 
            font-size: 17px; 
            text-align: left;
        }
        
        .table-title {
            background: #e30000;
            color: #fff;
        }
        .table-regular {
            width: 100%;
            table-layout: fixed;
            border: 1px solid #ccc;
        } 
        @media screen and (max-width: 768px){
            .table-respons tr td, .table-respons tr th {
                white-space: nowrap;
            }
            table tr th , table tr td { 
                word-break: break-word; 
            }
        }
    </style>
</svg>

<!-- ************************ -->
<?php 
    $web_b2c_rma_id = DB::table('web_b2c_rma')->pluck('web_b2c_rma_id')->first();
?>
<div class="order-page" style="max-width: 1170px; margin: auto; padding: 0 10px; font-size:14px;">
    <br>
    
    <div style="clear: both;">
        
        <h2 style="
            color: #222; 
            font-size: 20px;
            margin: 10px 0;
            text-transform: uppercase;
            font-weight: 600;
            display: inline-block;">
                {{ Language::trans('Uspešno ste prijavili kvar') }}.
        </h2>
        
        <a style="
            float: right;
            text-decoration: none;
            border: 1px solid #ddd;
            margin: 5px 0;
            padding: 5px;" href="{{Options::base_url()}}stampanje_narudzbenice/{{ $web_b2c_rma_id }}" target="_blank" rel="nofollow">{{ Language::trans('Štampaj') }}</a>
    </div>

    <br>

 <!-- ************************ -->
 
     <table class="table-regular">
        <thead>
            <tr>
                <th colspan="2" class="table-title">
                    {{ Language::trans('Podaci o prodavcu') }}:
                </th>
            </tr>
        </thead>
        <tbody> 
            <tr>  
                <td>{{ Language::trans('Naziv prodavca') }}:</td>
                <td>{{Options::company_name()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Adresa') }}:</td>
                <td>{{Options::company_adress()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Telefon') }}:</td>
                <td>{{Options::company_phone()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Fax') }}:</td>
                <td>{{Options::company_fax()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('PIB') }}:</td>
                <td>{{Options::company_pib()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Šifra delatnosti') }}:</td>
                <td>{{Options::company_delatnost_sifra()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Žiro račun') }}:</td>
                <td>{{Options::company_ziro()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('E-mail') }}:</td>
                <td>{{Options::company_email()}}</td>
            </tr>
        </tbody>
    </table>

    <br>

<!-- ************************ -->

    <table class="table-regular">
        <thead>
            <tr>
                <th colspan="2" class="table-title">
                    {{ Language::trans('Podaci o kupcu') }}:
                </th>
            </tr>
        </thead>
        <tbody>
            @if(Rma::vrsta_kupca() == 0)
            <tr>
                <td>{{ Language::trans('Ime i prezime') }}:</td>
                <td>{{Rma::broj_dokumenta($web_b2c_rma_id)}}</td>
            </tr>
            @else
            <tr>
                <td>{{ Language::trans('Naziv firme') }}:</td>
                <td>{{Rma::naziv_firme($web_b2c_rma_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('PIB') }}:</td>
                <td>{{Rma::pib_firme($web_b2c_rma_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Matični broj') }}:</td>
                <td>{{Rma::maticni_firme($web_b2c_rma_id)}}</td>
            </tr>
            @endif
            <tr>
                <td>{{ Language::trans('Email') }}:</td>
                <td>{{Rma::email($web_b2c_rma_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Telefon') }}:</td>
                <td>{{Rma::telefon($web_b2c_rma_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Adresa') }}:</td>
                <td>{{Rma::adresa($web_b2c_rma_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Mesto/grad') }}:</td>
                <td>{{Rma::mesto($web_b2c_rma_id)}}</td>
            </tr>
        </tbody> 
    </table> 

    <br>
 <!-- ************************ -->

    <table class="table-regular"> 
        <thead>
            <tr>
                <th colspan="2" class="table-title">
                    {{ Language::trans('Podaci o kvaru') }}:
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>  
                <td>{{ Language::trans('Proizvođač') }}:</td>
                <td>{{Rma::proizvodjac($web_b2c_rma_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Model') }}:</td>
                <td>{{Rma::model($web_b2c_rma_id)}}</td>
            </tr>
            <tr> 
                <td>{{ Language::trans('Serijski broj uređaja (IMEI broj za GSM uređaje)') }}:</td>
                <td>{{Rma::serijski($web_b2c_rma_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Opis kvara') }}:</td>
                <td>{{Rma::opis($web_b2c_rma_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Preuzeti uređaj na adresi korisnika:') }}:</td>
                <td>{{Rma::preuzeti($web_b2c_rma_id)}}</td>
            </tr>
        </tbody>
    </table>

    <br>
 <!-- ************************ -->
      
</div>