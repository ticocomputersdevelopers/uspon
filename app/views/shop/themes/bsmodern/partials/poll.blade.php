
@if(!is_null($anketa = Support::anketa($anketa_id)) AND !Support::checkAnketaKorisnikExists($anketa_id,Cart::kupac_id(),$web_b2c_narudzbina_id))

<br>

<div class="polls"> 
	
	<form method="POST" action="{{ Options::base_url() }}poll-answer">
		<input type="hidden" name="order_id" value="{{ $web_b2c_narudzbina_id }}">
		@foreach(Support::anketaPitanja($anketa->anketa_id) as $pitanje)
		<div class="JSPollAnswers poll">
			@if($pitanje->tip == 'checkbox')
				<h4 class="question">{{ Language::trans($pitanje->pitanje) }}</h4>
				
				@foreach(Support::anketaOdgovori($pitanje->anketa_pitanje_id) as $key => $odgovor)
				<div class="answer row">
					<div class="col-xs-10">{{($key+1)}}. {{ Language::trans($odgovor->odgovor) }}</div>

					<div class="col-xs-2 text-right">
						<input type="radio" name="poll_check_answer_{{ $pitanje->anketa_pitanje_id }}" value="{{$odgovor->anketa_odgovor_id}}" {{ ((Input::old('poll_check_answer_'.$pitanje->anketa_pitanje_id) == strval($odgovor->anketa_odgovor_id)) OR (count(Input::old()) == 0 AND $key == 0)) ? 'checked' : '' }} class="v-align" />
					</div>
				</div>
				@endforeach

			@elseif($pitanje->tip == 'text')
				
				<h4 class="question">{{ Language::trans($pitanje->pitanje) }}</h4>

				<textarea name="poll_text_answer_{{ $pitanje->anketa_pitanje_id }}" class="JSPollAnswerText">{{ Input::old('poll_text_answer_'.$pitanje->anketa_pitanje_id) }}</textarea>

			@endif
		</div> 
		@endforeach
		<div class="text-center">
			<button type="submit" class="btn">{{ Language::trans('Pošalji') }}</button>
		</div>
	</form>	

</div>

@else
	<br>
	<h3 class="text-center">{{ Language::trans('Vaša anketa je poslata') }}.</h3> 
@endif