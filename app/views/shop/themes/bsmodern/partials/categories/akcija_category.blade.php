<div class="manufacturer-categories"> 
    <h3 class="text-center">{{$title}}</h3>
    <ul>
        @foreach(Support::akcija_categories() as $key => $value)
        <li>
            <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans('akcija') }}/{{ Url_mod::slug_trans($key) }}">
                <span>{{ Language::trans($key) }}</span>
                <span>({{ $value }})</span>
            </a>
        </li>
        @endforeach  
    </ul>
</div> 
