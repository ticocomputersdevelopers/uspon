<div class="JScategories relative"> 
	<?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
	<div>
		<span class="JSclose-nav hidden-md hidden-lg">&times;</span>
	</div>

	<!-- CATEGORIES LEVEL 1 -->

	<ul class="JSlevel-1">
		<a class="logo v-align inline-block" href="/" title="{{Options::company_name()}}" rel="nofollow">
			<img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
		</a>
		<span class="JSclose-nav" id="JScategoryClose">&times;</span>

		@if(Options::category_type()==1) 
		@foreach ($query_category_first as $row1)
		@if(Groups::broj_cerki($row1->grupa_pr_id) >0)

		<li>
			<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}" class="">
				@if(Groups::check_image_grupa($row1->putanja_slika))
				<span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs"> 
					<img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
				</span>
				@endif
				{{ Language::trans($row1->grupa)  }} 
			</a>

			<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

			<ul class="JSlevel-2 row ">
				@foreach ($query_category_second->get() as $row2)
				<li class="col-md-3 col-sm-12 col-xs-12">  

					<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
						@if(Groups::check_image_grupa($row2->putanja_slika))
						<span class="lvl-2-img-cont hidden-sm hidden-xs">
							<img src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
						</span>
						@endif

						<span class="level-2-name relative">
							<span class="name-category">
								{{ Language::trans($row2->grupa) }}
							</span>
						</span>
					</a>

					@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
					<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

					<ul class="JSlevel-3">
						@foreach($query_category_third as $row3) 

						<li>						 
							<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}
							</a>
								<!-- @if(Groups::broj_cerki($row3->grupa_pr_id) >0)
								<span class="JSCategoryLinkExpend hidden-sm hidden-xs">
									<i class="fas fa-chevron-down" aria-hidden="true"></i>
								</span>
								@endif   -->

								@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
								<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

								<ul class="JSlevel-4">
									@foreach($query_category_forth as $row4) 
									<li>
										<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}

										</a>
									</li>
									@endforeach
								</ul>
								@endif		
							</li>					 	
							@endforeach
						</ul>
						@endif
					</li>
					@endforeach
				</ul>
			</li>

			@else

			<li>		 
				<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">		 
					@if(Groups::check_image_grupa($row1->putanja_slika))
					<span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs"> 
						<img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
					</span>
					@endif
					{{ Language::trans($row1->grupa)  }}  				
				</a>			 
			</li>
			@endif
			@endforeach
			@else
			@foreach ($query_category_first as $row1)


			@if(Groups::broj_cerki($row1->grupa_pr_id) >0)
			<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa)  }}</a>
			<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>


			@foreach ($query_category_second->get() as $row2)
			<li>
				<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
					{{ Language::trans($row2->grupa) }}
				</a>
				@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
				<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
				<ul>
					@foreach($query_category_third as $row3)
					<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}
					</a>
					@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
					<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

					<ul>
						@foreach($query_category_forth as $row4)
						<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
						@endforeach
					</ul>
					@endif	
					@endforeach
				</ul>
				@endif
			</li>
			@endforeach

			@else
			<li>
				<a href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa) }}</a>
			</li>
			@endif 
			@endforeach				
			@endif

			@if(Options::all_category()==1)
			<li>
				<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sve-kategorije') }}">{{ Language::trans('Sve kategorije') }}</a>
			</li>
			@endif

		</ul>
	</div>

