<!DOCTYPE html>
<html><head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
<title>PDF</title>
    <style>
        body {font-family: DejaVu Sans;}
        

        table tr td { padding: 6px; border-bottom: 1px solid #ccc; }
        
        table tr th { 
            padding: 6px; 
            font-size: 17px; 
            text-align: left;
        }
        
        .table-title {
            background: #e30000;
            color: #fff;
        }
        .table-regular {
            width: 100%;
            table-layout: fixed;
            border: 1px solid #ccc;
        } 
        .table-regular th { background: #e30000; color: #fff; }
        @media screen and (max-width: 768px){
            .table-respons tr td, .table-respons tr th {
                white-space: nowrap;
            }
            table tr th , table tr td { 
                word-break: break-word; 
            }
        }


    </style>
</head><body>

      <div style="clear: both;">
        
        <h2 style="
            color: #222; 
            font-size: 14px;
            margin: 6px 0;
            text-transform: uppercase;
            font-weight: 600;
            display: inline-block;">
            Poštovani - Vaša porudžbina je uspešno primljena. 
       
             @if(!is_null($bank_result))
                @if($bank_result->realized == 1)
                {{ Language::trans('Račun platne kartice je zadužen') }}.
                @else
                {{ Language::trans('Plaćanje nije uspešno, račun platne kartice nije zadužen. Najčešći uzrok je pogrešno unet broj kartice, datum isteka ili sigurnosni kod, pokušaje ponovo, u slučaju uzastopnih greški pozovite vašu banku') }}.
                @endif
            @else
                {{ Language::trans('Očekujte da Vas kontaktira naša služba prodaje. Zahvaljujemo Vam se na korišćenju naše Internet prodavnice.') }}
            @endif

              
        </h2>
        
    </div>

    
    <table class="table-regular">
            <tr>
                <th colspan="2" class="table-title">
                    {{ Language::trans('Informacije o narudžbini') }}:
                </th>
            </tr>
            <tr>
                <td>{{ Language::trans('Broj porudžbine') }}:</td>
                <td>{{Order::broj_dokumenta($web_b2c_narudzbina_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Datum porudžbine') }}:</td>
                <td>{{Order::datum_porudzbine($web_b2c_narudzbina_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Način isporuke') }}:</td>
                <td>{{Order::n_i($web_b2c_narudzbina_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Način plaćanja') }}:</td>
                <td>{{Order::n_p($web_b2c_narudzbina_id)}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Napomena') }}:</td>
                <td>{{Order::napomena_nar($web_b2c_narudzbina_id)}}</td>
            </tr> 
    </table>

    <br>

 <!--    <table class="table-regular">
            <tr>
                <th colspan="2" class="table-title">
                    {{ Language::trans('Informacije o kupcu') }}:
                </th>
            </tr>
            @if($kupac->flag_vrsta_kupca == 0)
            <tr>  
                <td>{{ Language::trans('Ime i Prezime') }}:</td>
                <td>{{ Language::trans_chars($kupac->ime.' '.$kupac->prezime )}}</td>
            </tr>
            @else
            <tr>
                <td>{{ Language::trans('Firma i PIB') }}:</td>
                <td>{{ Language::trans_chars($kupac->naziv).' '.$kupac->pib }}</td>
            </tr>
            <tr> 
                <td>{{ Language::trans('Matični broj') }}:</td>
                <td>{{ Language::trans_chars($kupac->maticni_br) }}</td> 
            </tr>
            @endif
 
            <tr>
                <td>{{ Language::trans('Adresa') }}:</td>
                <td>{{ Language::trans_chars($kupac->adresa) }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Mesto') }}:</td>
                <td>{{ Language::trans_chars($kupac->mesto) }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Telefon') }}:</td>
                <td>{{ $kupac->telefon }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('E-mail') }}:</td>
                <td>{{ $kupac->email }}</td>
            </tr>
    </table>

    <br>

    <table class="table-regular">
        
            <tr>
                <th colspan="2" class="table-title">
                    {{ Language::trans('Informacije o prodavcu') }}:
                </th>
            </tr>
        
        
            <tr>  
                <td>{{ Language::trans('Naziv prodavca') }}:</td>
                <td>{{Options::company_name()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Adresa') }}:</td>
                <td>{{Options::company_adress()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Telefon') }}:</td>
                <td>{{Options::company_phone()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Fax') }}:</td>
                <td>{{Options::company_fax()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('PIB') }}:</td>
                <td>{{Options::company_pib()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Šifra delatnosti') }}:</td>
                <td>{{Options::company_delatnost_sifra()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Žiro račun') }}:</td>
                <td>{{Options::company_ziro()}}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('E-mail') }}:</td>
                <td>{{Options::company_email()}}</td>
            </tr>
    </table> -->

  <div style="page-break-before:always">&nbsp;</div>

    @if(!is_null($bank_result))
    <table class="table-regular">
        
            <tr>
                <th colspan="2" class="table-title">
                    {{ Language::trans('Informacije o transakciji') }}:
                </th>
            </tr>
       
            <tr>  
                <td>{{ Language::trans('Broj narudžbine') }}:</td>
                <td>{{ isset($bank_result->oid) ? $bank_result->oid : '' }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Autorizacioni kod') }}</td>
                <td>{{ isset($bank_result->auth_code) ? $bank_result->auth_code : '' }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Status transakcije') }}</td>
                <td>{{ isset($bank_result->response) ? $bank_result->response : '' }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Kod statusa transakcije') }}</td>
                <td>{{ isset($bank_result->result_code) ? $bank_result->result_code : '' }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Broj transakcije') }}</td>
                <td>{{ isset($bank_result->trans_id) ? $bank_result->trans_id : '' }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Datum transakcije') }}</td>
                <td>{{ isset($bank_result->post_date) ? $bank_result->post_date : '' }}</td>
            </tr>
            <tr>
                <td>{{ Language::trans('Statusni kod transakcije') }}</td>
                <td>{{ isset($bank_result->md_status) ? $bank_result->md_status : '' }}</td>
            </tr>
    </table>

    <br>
    @endif 

         <div style="overflow-x: auto;"> 
         <table class="table-respons" style="width: 100%; border: 1px solid #ccc; border-collapse: initial;">
            
                <tr>
                    <th colspan="5" class="table-title">
                        {{ Language::trans('Informacije o naručenim proizvodima') }}:
                    </th>
                </tr>
                <tr>
                    @if(AdminOptions::sifra_view()==1)
                        <th>{{ AdminLanguage::transAdmin('Roba_id') }}</th>
                    @elseif(AdminOptions::sifra_view()==2)
                        <th>{{ AdminLanguage::transAdmin('Šifra') }}</th>
                    @elseif(AdminOptions::sifra_view()==3)
                        <th>{{ AdminLanguage::transAdmin('SKU') }}</th>
                    @elseif(AdminOptions::sifra_view()==4)
                        <th>{{ AdminLanguage::transAdmin('Šifra Dob') }}</th> 
                    @endif
                        <th>{{ Language::trans('Naziv proizvoda') }}:</th>
                        <th>{{ Language::trans('Cena') }}:</th>
                        <th>{{ Language::trans('Količina') }}:</th>
                        <th>{{ Language::trans('Ukupna cena') }}:</th>
                </tr>
                
                
                @foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->join('roba', 'roba.roba_id' , '=', 'web_b2c_narudzbina_stavka.roba_id')->get() as $row)
                <tr>
                    @if(AdminOptions::sifra_view()==1)
                        <td>{{ $row->roba_id}}</td>
                    @elseif(AdminOptions::sifra_view()==2)
                        @if(!empty($row->sifra_is))
                        <td>{{$row->sifra_is }}</td>
                        @else
                        <td>{{$row->sifra_d }}</td>
                        @endif
                    @elseif(AdminOptions::sifra_view()==3)
                        <td>{{ $row->sku }}</td>
                    @elseif(AdminOptions::sifra_view()==4)
                        <td>{{ $row->sifra_d }}</td>
                    @endif
                    <td>
                        <div style="max-width: 450px; overflow: hidden;">{{ Product::short_title($row->roba_id) }}</div>
                        {{ Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids) }}
                    </td>
                    <td>{{ Cart::cena($row->jm_cena) }}</td>
                    @if(Options::web_options(320) == 1)
                        @if ( Product::jedinica_mere($row->roba_id)->jedinica_mere_id == 3 AND Product::pakovanje($row->roba_id)) 
                        <td>{{ $row->kolicina }}kg</td>
                        @else
                        <td>{{ (int)$row->kolicina }}</td>
                        @endif
                    @else
                    <td>{{ (int)$row->kolicina }}</td>
                    @endif
                    <td>{{ Cart::cena(($row->kolicina*$row->jm_cena)) }}</td>
                </tr>
                @endforeach

                <?php $troskovi = Cart::troskovi($web_b2c_narudzbina_id);
                      $popust = Order::popust($web_b2c_narudzbina_id); ?>
                <tr>
                    <td colspan="3"></td>
                    <td><b>{{ Language::trans('Cena artikala') }}: </b></td>
                    <td><b>{{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id))}} </b></td>
                </tr>
                @if($troskovi>0 AND DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_nacin_isporuke_id') != 2)
                <tr>
                    <td colspan="3"></td> 
                    <td><b>{{ Language::trans('Troškovi isporuke') }}: </b></td>
                    <td><b>{{Cart::cena($troskovi)}} </b></td>
                </tr>
                @else
                <?php $troskovi = 0; ?>
                @endif
                @if($popust>0)
                <tr>
                    <td colspan="3"></td> 
                    <td><b>{{ Language::trans('Popust') }}: </b></td>
                    <td><b>{{Cart::cena($popust)}} </b></td>
                </tr>
                @endif
                <tr>
                    <td colspan="3"></td> 
                    <td><b>{{ Language::trans('Ukupno') }}: </b></td>
                    <td><b>{{Cart::cena(Order::narudzbina_ukupno($web_b2c_narudzbina_id)+$troskovi-$popust)}} </b></td>
                </tr>
        </table>
    </div>

</body></html>
