@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('baners_sliders')
    @include('shop/themes/'.Support::theme_path().'partials/baners_sliders')
@endsection
 
@section('page')
    @include('shop/themes/'.Support::theme_path().'partials/products/action_type')
    
    @if(Options::web_options(136)==0)
        @if(Options::web_options(208)==1)
            <!-- ARTICLES AT FRONT PAGE -->
          
            <h2><span class="section-title JSInlineShort" data-target='{"action":"front_admin_label","id":"1"}'>{{ Language::trans(Support::front_admin_label(1)) }}</span></h2>
             
            <div class="JSproducts_slick">
                @foreach(Articles::mostPopularArticles(5) as $row)
                    @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                @endforeach
            </div>
        
            @if(count(Articles::bestSeller()))
            <h2><span class="section-title JSInlineShort" data-target='{"action":"front_admin_label","id":"2"}'>{{ Language::trans(Support::front_admin_label(2)) }}</span></h2>
           
            <div class="JSproducts_slick">
                @foreach(Articles::bestSeller(5) as $row)
                    @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                @endforeach
            </div>
            @endif
          
            <h2><span class="section-title JSInlineShort" data-target='{"action":"front_admin_label","id":"3"}'>{{ Language::trans(Support::front_admin_label(3)) }}</span></h2>
            
            <div class="JSproducts_slick">
                @foreach(Articles::latestAdded(5) as $row)
                    @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                @endforeach
            </div>
        @endif
    @else
       
        <h2><span class="section-title JSInlineShort" data-target='{"action":"home_all_articles"}'>{{ Language::trans(Support::title_all_articles()) }}</span></h2>
       
        <div class="JSproduct-slider">
            @foreach($articles as $row)
                @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
            @endforeach
        </div>
         
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            {{ Paginator::make($articles, $count_products, $limit)->links() }}
        </div>
    @endif  

<!--     <br>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12"> 
            <div class="JSBrandSlider brandSlider">  
                <?php foreach(All::getManufacturersShortList() as $row){ ?>
                <div class="text-center">
                    <a class="inline-block" href="{{Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac')}}/{{ Url_mod::slug_trans($row->naziv) }}"> 
                        <img alt="{{ ($row->naziv) }}" src="{{ Options::domain() }}<?php echo $row->slika; ?>" /> 
                    </a>
                </div>
                <?php } ?>
            </div>  
        </div>
    </div> -->
 
    <script>
        $(document).ready(function(){
            @if(Session::has('login_success')) 
                alertSuccess('{{ Session::get("login_success") }}'); 
            @endif
            
           
            @if(Session::has('registration_success')) 
                alertSuccess('{{ Session::get("registration_success") }}'); 
            @endif

            
            @if(Session::has('loggout_succes')) 
                alertSuccess('{{ Session::get("loggout_succes") }}'); 
            @endif


            @if(Session::has('confirm_registration_message')) 
                alertSuccess('{{ Session::get("confirm_registration_message") }}'); 
            @endif
        });
    </script>


@endsection