 <!-- baneri proizvodjaca -->
    <div class="manufacturers-listing">
        <div class="container">
            <div class="row bw">
               <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="brend-slider-wrapper">

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
                                <h2 class="section-heading">
                                    <a href="/brendovi">{{ Language::trans('Proizvodjači') }}</a>
                                </h2>
                            </div>
                        </div>

                        <div class="row">
                            <div class="JSBrandSlider"> 
                                
                                <?php foreach(All::getManufacturersShortList() as $row){ ?>
                                <div>
                                    <a class="brand-link" href="{{Options::base_url() }}{{ Url_mod::convert_url('proizvodjac')}}/{{ Url_mod::url_convert($row->naziv) }}">

                                        <img alt="{{ ($row->naziv) }}" src="{{ Options::domain() }}<?php echo $row->slika; ?>" />

                                    </a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
