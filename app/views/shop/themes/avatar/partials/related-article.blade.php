


<!-- Related article -->
<div class="related-section">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<h2 class="section-heading">
				{{Language::trans('Srodni proizvodi')}}
			</h2>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
			<div class="JSrelated-products JSproduct-slider">
		    @foreach(Product::get_related($roba_id) as $row)
				@include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
			@endforeach
			</div>
		</div>
	</div>
</div>