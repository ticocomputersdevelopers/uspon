@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
<div id="admin-menu"> 
    <div class="container text-right">
        <a href="#!" data-toggle="modal" data-target="#FAProductsModal"> <i class="fas fa-clipboard-list"></i> {{ Language::trans('Artikli') }}</a>
        |
        @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
        <a href="#!" id="JSShortAdminSave"> <i class="fas fa-save"></i> {{ Language::trans('Sačuvaj izmene') }}</a>
        @endif
        | 
        <span class="ms admin-links"><a target="_blank" href="{{ Options::domain() }}admin"><i class="fas fa-cogs"></i> {{ Language::trans('Admin Panel') }}</a></span> |
        <span class="ms admin-links"><a href="{{ Options::domain() }}admin-logout">{{ Language::trans('Odjavi se') }}</a></span>
    </div>
</div> 

@include('shop/front_admin/modals/products')
@include('shop/front_admin/modals/product')
@endif

<div id="preheader">

    <div class="social-icons hidden-sm hidden-xs">  
        {{Options::social_icon()}} 
    </div>

    <div class="container"> 
        <div class="top-menu row">

            @if(Options::stranice_count() > 0)
                <span class="JStoggle-btn hidden-md hidden-lg inline-block"><i class="fas fa-bars"></i></span>
            @endif 

            <div class="col-md-7 col-sm-7 col-xs-12 hidden-small JStoggle-content">     
                
                <ul>
                    @foreach(All::menu_top_pages() as $row)
                    <li><a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{ Url_mod::page_slug($row->naziv_stranice)->naziv }}</a></li>
                    @endforeach
                </ul>  

            </div>


            <div class="col-md-5 col-sm-5 col-xs-11 xs-offset-1 text-right"> 

   <!-- LOGIN AND REGISTRATION --> 
                @if(Session::has('b2c_kupac'))

                    <span class="JSbroj_wish far fa-heart">{{ Cart::broj_wish() }}</span>  

                    <a class="logged-user" href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}">{{ WebKupac::get_user_name() }}</a>

                    <a class="logged-user" href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}">{{ WebKupac::get_company_name() }}</a>

                    <a id="logout-button" class="inline-block" href="{{Options::base_url()}}logout">{{ Language::trans('Odjavi se') }}</a>

                @else 
 
                    <a href="#" id="login-icon" data-toggle="modal" data-target="#loginModal">{{ Language::trans('Uloguj se') }}</a>
       
                    <a id="registration-icon" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{ Language::trans('Registracija') }}</a>
                 
                @endif 

                @if(Options::checkB2B())
                <a href="{{Options::domain()}}b2b/login" class="inline-block">B2B</a> 
                @endif 
            </div>   
 
        </div> 
    </div>
</div>


