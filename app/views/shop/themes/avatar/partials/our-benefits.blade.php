
    

    <div class="our-benefits-div">
		<div class="container">
			<div class="row">
				<div class="col-md-3 benefits-div">
					<span class="benefits-icon"><i class="fas fa-truck"></i></span>
					<p class="title">Worldwide Delivery</p>
					<p class="description">With sites in 5 languages, we ship to over 200 countries & regions.</p>
				</div>

				<div class="col-md-3 benefits-div">
					<span class="benefits-icon"><i class="far fa-credit-card"></i></span>
					<p class="title">Safe Payment</p>
					<p class="description">Pay with the world’s most popular and secure payment methods.</p>
				</div>

				<div class="col-md-3 benefits-div">
					<span class="benefits-icon"><i class="fas fa-shield-alt"></i></span>
					<p class="title">Shop with Confidence</p>
					<p class="description">Our Buyer Protection covers your purchase from click to delivery.</p>
				</div>

				<div class="col-md-3 benefits-div">
					<span class="benefits-icon"><i class="fas fa-headphones"></i></span>
					<p class="title">24/7 Help Center</p>
					<p class="description">Round-the-clock assistance for a smooth shopping experience.</p>
				</div>

			</div>
		</div>
	</div>