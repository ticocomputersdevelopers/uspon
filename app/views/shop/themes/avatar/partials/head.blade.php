
<title>{{ $title }}</title>

<meta charset="utf-8">  
<meta name="description" content="{{ $description }}" />
<meta name="keywords" content="{{ $keywords }}" />
<meta name="author" content="{{Options::company_name()}}" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<meta property="og:title" content="{{ $title }}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"}}" />
<meta property="og:image" content="{{isset($og_image) ? Options::domain().$og_image : ''}}" />
<meta property="og:description" content="{{ $description }}" />
 
<!-- BOOTSTRAP LOCAL -->  
<script src="{{ Options::domain() }}js/3.3.1_jquery.min.js"></script> 
<link href="{{ Options::domain() }}css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="{{Options::domain() }}js/bootstrap.min.js"></script>

<!-- BOOTSTRAP CDN-->
<!--  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
 
<script src="{{Options::domain()}}js/bootbox.min.js"></script>
<script src="{{ Options::domain() }}js/jquery.lazy.min.js"></script>
 
 <!-- CSS -->
<link href="{{ Options::domain().'css/themes/'.Support::theme_path()}}style.css" rel="stylesheet" type="text/css" />
<link href="{{ Options::domain().'css/themes/'.Support::theme_path()}}custom.css" rel="stylesheet" type="text/css" />
 
 <!-- FAVICON -->
<link rel="icon" type="image/png" href="{{Options::domain()}}favicon.ico">

 <!-- FANCYBOX -->
<link href="{{Options::domain()}}css/fancybox.css" rel="stylesheet" type="text/css" />
 
<!-- FONT CDN AWESOME 5 -->
<link href="{{Options::domain()}}css/fontawesome-free-5.11.2-web/css/all.min.css" rel="stylesheet" type="text/css">


<!-- FONT -->
<link href="http://fonts.googleapis.com/css?family=Raleway:400,500&display=swap" rel="stylesheet">

<!-- <link href="{{ Options::domain().'css/themes/'.Support::theme_path()}}open-sans.css" rel="stylesheet" type="text/css"> -->
 
<link href="{{Options::domain()}}css/slick.css" rel="stylesheet" type="text/css" />
 

@if(Options::enable_filters()==1 AND $strana=='artikli' AND $filter_prikazi )
<link rel="stylesheet" type="text/css" href="{{Options::domain()}}css/jquery-ui.css">   
@endif

<script src="{{ Options::domain() }}js/sweetalert.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.1/sweetalert.min.css"/>

@if(Session::has('b2c_admin'.Options::server()))
<link rel="stylesheet" href="{{ Options::domain() }}css/1.11.4_jquery-ui.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="{{ Options::domain() }}css/front_admin/style.css" rel="stylesheet" type="text/css" >
@endif

@if(Options::gnrl_options(3021) == 0)
<link href="{{ Options::domain().'css/themes/'.Support::theme_style_path()}}color.css" rel="stylesheet" type="text/css" />
@else <!-- CUSTOM COLORS -->
<style>
:root{
	--header_bg: {{ Options::prodavnica_boje(1) }}; 	        
	--top_menu_bg:{{ Options::prodavnica_boje(2) }};  		
	--top_menu_color: {{ Options::prodavnica_boje(3) }};	
	--menu_bg: {{ Options::prodavnica_boje(4) }};
	--footer_bg: {{ Options::prodavnica_boje(5) }};
	--body_color: {{ Options::prodavnica_boje(6) }};		
	--paragraph_color: {{ Options::prodavnica_boje(7) }};		
	--body_bg: {{ Options::prodavnica_boje(8) }};	
	--h2_color: {{ Options::prodavnica_boje(9) }};
	--h5_color: {{ Options::prodavnica_boje(10) }};	
	--btn_bg: {{ Options::prodavnica_boje(11) }};
	--btn_hover_bg: {{ Options::prodavnica_boje(12) }};
	--btn_color: {{ Options::prodavnica_boje(13) }};
	--a_href_color: {{ Options::prodavnica_boje(14) }};
	--level_1_color: {{ Options::prodavnica_boje(15) }};
 	--level_2_color: {{ Options::prodavnica_boje(16) }};
 	--categories_title_bg: {{ Options::prodavnica_boje(17) }};
 	--categories_level_1_bg: {{ Options::prodavnica_boje(18) }};
 	--categories_level_2_bg: {{ Options::prodavnica_boje(19) }};
 	--product_bg: {{ Options::prodavnica_boje(20) }};
 	--product_bottom_bg: {{ Options::prodavnica_boje(21) }};
 	--review_star_color: {{ Options::prodavnica_boje(22) }};
 	--product_title_color: {{ Options::prodavnica_boje(23) }};
 	--article_product_price_color: {{ Options::prodavnica_boje(24) }};
 	--sale_action_price_bg: {{ Options::prodavnica_boje(25) }};
 	--sale_action_bg: {{ Options::prodavnica_boje(26) }};
 	--product_old_price_color: {{ Options::prodavnica_boje(27) }};
 	--product_price_color: {{ Options::prodavnica_boje(28) }};
 	--login_btn_bg: {{ Options::prodavnica_boje(29) }};
 	--login_btn_color: {{ Options::prodavnica_boje(30) }};
 	--cart_number_bg: {{ Options::prodavnica_boje(31) }};
 	--inner_body_bg: {{ Options::prodavnica_boje(32) }};
 	--product_list_bg: {{ Options::prodavnica_boje(33) }};
 	--input_bg_color: {{ Options::prodavnica_boje(34) }};
 	--breadcrumb_bg:  {{ Options::prodavnica_boje(35) }};
 	--currency_bg: {{ Options::prodavnica_boje(36) }};
    --currency_color: {{ Options::prodavnica_boje(37) }};
    --filters_bg: {{ Options::prodavnica_boje(38) }};
    --pagination_bg: {{ Options::prodavnica_boje(39) }};
    --pagination_bg_active: {{ Options::prodavnica_boje(40) }};   
    --pagination_color_active: {{ Options::prodavnica_boje(41) }};
    --discount_price_color: {{ Options::prodavnica_boje(42) }};
    --add_to_cart_btn_bg: {{ Options::prodavnica_boje(43) }};
    --add_to_cart_btn_color: {{ Options::prodavnica_boje(44) }};
    --add_to_cart_btn_bg_hover: {{ Options::prodavnica_boje(45) }};
    --add_to_cart_btn_bg_not_available: {{ Options::prodavnica_boje(46) }};
 	--label_color: {{ Options::prodavnica_boje(47) }};
 	--scroll_top_bg: {{ Options::prodavnica_boje(48) }};
 	
	}
</style>
@endif  
 
@include('shop/google_analytics')
@include('shop/facebook_pixel')
@include('shop/chat')
@include('shop/drip')

<script>
	localStorage.setItem('translates','{{ Language::js_translates() }}');
</script>
