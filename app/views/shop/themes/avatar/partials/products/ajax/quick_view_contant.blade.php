<div class="row text-left">    

    <div class="col-md-6 col-sm-12 col-xs-12">
 
        <img class="img-responsive margin-auto quick-view-img" src="{{ Options::domain().$image }}" alt="product-image">
      
        @if(All::provera_akcija($roba_id))
        <div class="sale-label color-white"> 
             {{ Language::trans('Akcija') }} 
            <!-- <div class="for-sale-price">- {{ Product::getSale($roba_id) }} %</div> -->
        </div>
        @endif
    </div>

    <div class="col-md-6 col-sm-12 col-xs-12">
      
        <h2 class="product-name">
            <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}">
                {{ Product::short_title($roba_id) }}  
            </a>
        </h2> 
      
      
        @if(!is_null($proizvodjac) AND $proizvodjac->proizvodjac_id > 0)
        <p>{{Language::trans('Brend')}}: {{ Product::get_proizvodjac($roba_id) }}</p>
        @endif

        <div class="quick-desc"> 
            {{ Product::get_karakteristike_short_grupe($roba_id) }} 
        </div>
        
        <br>

        @if(Product::getStatusArticlePrice($roba_id))
        <div class="price-holder">
            <div> {{ Cart::cena(Product::get_price($roba_id)) }} </div>
            @if(All::provera_akcija($roba_id))
            <span class="product-old-price">{{ Cart::cena(Product::old_price($roba_id)) }}</span>
            @endif     
        </div>  
        @endif 
               

        <div class="add-to-cart-area"> 

            @if(AdminSupport::getStatusArticle($roba_id) == 1) 
                @if(Cart::check_avaliable($roba_id) > 0)

                <input type="text" name="kolicina" autocomplete="off" id="JSQuickViewAmount" class="modal-amount cart-amount" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">

                    @if(!Product::check_osobine($roba_id)) 

                    <button data-roba_id="{{$roba_id}}" class="JSAddToCartQuickView buy-btn">{{ Language::trans('U korpu') }}</button>

                    @else

                    <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" class="buy-btn text-center">{{ Language::trans('Vidi artikal') }}</a>

                    @endif
 
               
                @else    
               
                <button class="not-available">{{Language::trans('Nije dostupno')}}</button>   

                @endif      
            @endif
           
        </div>
    </div>
</div>