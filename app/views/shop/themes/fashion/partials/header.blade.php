<!-- header.blade -->
<header {{ Options::web_options(321, 'str_data') != '' ? 'style=background-color:' . Options::web_options(321, 'str_data') : '' }}>   
    <div class="relative" >  
        <div class="container{{(Options::web_options(321)==1) ? '-fluid' : '' }}">  
            <div class="row flex">  

                <div class="col-md-2 col-sm-3 col-xs-12 sm-text-center"> 
                    <!-- <h1 class="seo">{{ Options::company_name() }}</h1> -->
                    
                    <a class="logo inline-block" href="/" title="{{Options::company_name()}}" rel="canonical">
                        <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
                    </a>
                </div>

                <div class="col-md-7 col-sm-3 col-xs-12">
                    <div id="responsive-nav">
                        <div class="JSclose-nav text-right hidden-md hidden-lg">&times;</div> 

                        @include('shop/themes/'.Support::theme_path().'partials/categories/category')
                     
                        <ul id="main-menu" class="md-inline-block valign">
                            @foreach(All::header_menu_pages() as $row)
                            <li>
                               
                                @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                               
                                <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a>

                                <ul class="drop-2 text-left">
                                    @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                                    <li> 
                                        <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                                        <ul class="drop-3">
                                            @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                            <li> 
                                                <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                               
                                @else   
                               
                                <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                               
                                @endif                    
                            </li>                     
                            @endforeach 

                            @if(Options::web_options(121)==1)
                                <?php $konfiguratori = All::getKonfiguratos(); ?>
                               
                                @if(count($konfiguratori) > 0)
                                    @if(count($konfiguratori) > 1)
                                    <li>
                                        <a href="#!" class="konf-dropbtn">
                                            {{Language::trans('Konfiguratori')}}
                                        </a>
                                        <ul class="drop-2 text-left">
                                            @foreach($konfiguratori as $row)
                                            <li>
                                                <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $row->konfigurator_id }}">{{ Language::trans($row->naziv) }}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @else
                                    <li>
                                        <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $konfiguratori[0]->konfigurator_id }}">
                                            {{Language::trans('Konfigurator')}}
                                        </a>
                                    </li>
                                    @endif
                                @endif
                            @endif
                        </ul>    
                    </div>   
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 header-right">
                 
                    <!-- LOGIN AND REGISTRATION -->
                    @if(Session::has('b2c_kupac'))
                    <div class="inline-block log-user">

                        <span class="JSbroj_wish far fa-heart">{{ Cart::broj_wish() }}</span> 
                          
                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}">{{ WebKupac::get_user_name() }}</a> 

                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}">{{ WebKupac::get_company_name() }}</a> 
                        &nbsp;
                        <a id="logout-button" href="{{Options::base_url()}}logout" title="{{ Language::trans('Odjavi se') }}">
                            <i class="fas fa-sign-out-alt"></i>
                        </a>
                    </div>
                    
                    @else 
                    
                    <div class="dropdown inline-block">
                        
                        <a class="dropdown-toggle inline-block login-btn" href="#" role="button" data-toggle="dropdown">
                            <span class="fas fa-user"></span>
                        </a>
                        
                        <ul class="dropdown-menu login-dropdown"> 
                            <li>
                                <a href="#" id="login-icon" data-toggle="modal" data-target="#loginModal">{{ Language::trans('Prijava') }}</a>
                            </li>

                            <li>
                                <a id="registration-icon" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{ Language::trans('Registracija') }}</a>
                            </li>
                        </ul>
                    </div> 
                    @endif 

                    @include('shop/themes/'.Support::theme_path().'partials/cart_top')
                
                    <span class="inline-block pointer JSopen-search-modal"><i class="fas fa-search"></i></span>

                    <div class="resp-nav-btn inline-block hidden-md hidden-lg">
                        <span class="fas fa-bars"></span>
                    </div> 
                </div>  
                 
            </div>

            <!-- SEARCH BUTTON -->
            <div class="header-search JSsearch-modal">  
                <div class="search-content relative"> 
                     
                    <div class="row flex justify-center"> 
                        @if(Options::gnrl_options(3055) == 0)
                           
                            <div class="col-xs-4 no-padding JSselectTxt">  
                                {{ Groups::firstGropusSelect('2') }} 
                            </div>

                        @else

                            <input type="hidden" class="JSSearchGroup2" value="">

                        @endif
 
                        <div class="col-xs-8 no-padding JSsearchContent2">  
                            <form autocomplete="off" class="relative">
                                <input class="search-field" type="text" id="JSsearch2" placeholder="{{ Language::trans('Pretraži') }}" />

                                <button type="button" onclick="search2()" class="JSsearch-button2"> <i class="fas fa-search"></i> </button>
                            </form>      
                        </div>

                    </div>  
                </div> 
                
                <span class="inline-block close-search"><i class="fas fa-times"></i></span>
            </div>  
        </div>  
    </div> 
</header>

<!-- LOGIN MODAL --> 
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><i class="fas fa-times"></i></button>
                <h4 class="modal-title text-center">{{ Language::trans('Prijavite se') }}</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="JSemail_login">{{ Language::trans('E-mail') }}</label>
                    <input class="form-control" id="JSemail_login" type="text" value="" autocomplete="off">
                </div>
                <div class="form-group">
                    <label for="JSpassword_login">{{ Language::trans('Lozinka') }}</label>
                    <input class="form-control" autocomplete="off" id="JSpassword_login" type="password" value="">
                </div>
            </div>

            <div class="modal-footer text-right">
                <a class="button inline-block text-center" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{Language::trans('Registruj se')}}</a>
                
                <button type="submit" id="login" onclick="user_login()" class="button">{{ Language::trans('Prijavi se') }}</button>

                <button type="submit" onclick="user_forgot_password()" class="forgot-psw pull-left">{{ Language::trans('Zaboravljena lozinka') }}</button>
                
                <div class="field-group error-login JShidden-msg" id="JSForgotSuccess"><br>
                    {{ Language::trans('Novu lozinku za logovanje dobili ste na navedenoj e-mail adresi') }}.
                </div> 
            </div>
        </div>   
    </div>
</div>
<!-- header.blade end -->
