<div class="banners col-md-12 col-sm-12 col-xs-12 no-padding">

	<?php foreach(DB::table('baneri')->where('tip_prikaza', 1)->orderBy('redni_broj','asc')->get() as $row){ ?>
		<a class="col-md-12 col-sm-6 col-xs-12 center-block" href="<?php echo $row->link; ?>">
			<img class="img-responsive" src="{{ Options::domain() }}<?php echo $row->img; ?>" alt="{{$row->naziv}}" />
		</a>
	<?php } ?>  
 
</div>

   