 
<div class="header-cart-container inline-block relative">  
	
	<a class="header-cart inline-block text-center relative" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('korpa') }}">
		<span class="fas fa-shopping-bag"></span>		
		<span class="JScart_num badge"> {{ Cart::broj_cart() }} </span> 		
		<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	
	</a>

	<div class="JSheader-cart-content hidden-sm hidden-xs text-left">
		@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
	</div>
</div> 