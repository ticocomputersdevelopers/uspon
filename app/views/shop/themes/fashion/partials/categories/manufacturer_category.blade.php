<div class="manufacturer-categories"> 
    <h3 class="text-center">{{ Language::trans('Proizvođač') }} - {{$title}}</h3>
    <ul>
        @foreach(Support::manufacturer_categories($proizvodjac_id) as $key => $value)
        <li>
            <a href="{{ Options::base_url()}}{{ Url_mod::slug_trans('proizvodjac') }}/{{ Url_mod::slug_trans($proizvodjac) }}/{{ Url_mod::slug_trans($key) }}">
                <span>{{ Language::trans($key) }}</span>
                <span>{{ $value }}</span>
            </a>
        </li>
        @endforeach  
    </ul>
</div>
