@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 
<div class="half-width">  
	<h2><span class="page-title">{{ Language::trans('Kontaktirajte nas') }}</span></h2>

	<div class="row contact-page">  
		<div class="col-md-5 col-sm-5 col-xs-12 no-padding">  
			<br>
			<h3>{{ Language::trans('Postavite pitanje') }}</h3>
			<br> 

			<form method="POST" action="{{ Options::base_url() }}contact-message-send">
				<div class="form-group">
					<label id="label_name">{{ Language::trans('Vaše ime') }} *</label>
					<input class="contact-name" name="contact-name" id="JScontact_name" type="text" value="{{ Input::old('contact-name') }}">
					<div class="error red-dot-error">{{ $errors->first('contact-name') ? $errors->first('contact-name') : "" }}</div>
				</div> 

				<div class="form-group">
					<label id="label_email">{{ Language::trans('Vaša e-mail adresa') }} *</label>
					<input class="contact-email" name="contact-email" id="JScontact_email" type="text" value="{{ Input::old('contact-email') }}" >
					<div class="error red-dot-error">{{ $errors->first('contact-email') ? $errors->first('contact-email') : "" }}</div>
				</div>		

				<div class="form-group">	
					<label id="label_message">{{ Language::trans('Vaša poruka') }} </label>
					<textarea class="contact-message" name="contact-message" rows="5" id="message">{{ Input::old('contact-message') }}</textarea>
					<div class="error red-dot-error">{{ $errors->first('contact-message') ? $errors->first('contact-message') : "" }}</div>
				</div> 

				<div class="capcha text-center form-group"> 
					{{ Captcha::img(5, 160, 50) }}<br>
					<span>{{ Language::trans('Unesite kod sa slike') }}</span>
					<input type="text" name="captcha-string" tabindex="10" autocomplete="off">
					<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
				</div>

				<div class="text-right"> 
					<button type="submit" class="button">{{ Language::trans('Pošalji') }}</button>
				</div>
			</form> 
		</div>

		<div class="col-md-6 col-sm-6 col-xs-12 col-sm-offset-1 no-padding">
	 	   
		 	<ul>

		 		<li class="padding-h-15">
		 			<br>
		 			<h3>{{ Language::trans('Kontakt') }}</h3>
		 			<br>
		 		</li>

			 	@if(Options::company_name() != '')
				 	<li class="row">
						<span class="lab col-md-3 col-sm-5 col-xs-5">{{ Language::trans('Firma') }}:</span>
						<span class="col-md-9 col-sm-7 col-xs-7"> {{ Options::company_name() }} &nbsp;</span>
					</li>
				@endif
				@if(Options::company_adress() != '')
					<li class="row">
					    <span class="lab col-md-3 col-sm-5 col-xs-5">{{ Language::trans('Adresa') }}:</span>
					    <span class="col-md-9 col-sm-7 col-xs-7"> {{ Options::company_adress() }} &nbsp;</span>
					</li>
				@endif
				@if(Options::company_city() != '')
					<li class="row">
						<span class="lab col-md-3 col-sm-5 col-xs-5">{{ Language::trans('Grad') }}:</span>
						<span class="col-md-9 col-sm-7 col-xs-7"> {{ Options::company_city() }} &nbsp;</span>
					</li>
				@endif
				@if(Options::company_phone() != '')
					<li class="row">
						<span class="lab col-md-3 col-sm-5 col-xs-5">{{ Language::trans('Telefon') }}:</span>
						<span class="col-md-9 col-sm-7 col-xs-7"> {{ Options::company_phone() }} &nbsp;</span>
					</li>
				@endif
				@if(Options::company_fax() != '')
					<li class="row">
						<span class="lab col-md-3 col-sm-5 col-xs-5">{{ Language::trans('Fax') }}:</span>
						<span class="col-md-9 col-sm-7 col-xs-7"> {{ Options::company_fax() }} &nbsp;</span>
					</li>
				@endif
				@if(Options::company_pib() != '')
					<li class="row">
						<span class="lab col-md-3 col-sm-5 col-xs-5">{{ Language::trans('PIB') }}:</span>
						<span class="col-md-9 col-sm-7 col-xs-7"> {{ Options::company_pib() }} &nbsp;</span>
					</li>
				@endif
				@if(Options::company_maticni() != '')
					<li class="row">
						<span class="lab col-md-3 col-sm-5 col-xs-5">{{ Language::trans('Matični broj') }}:</span>
						<span class="col-md-9 col-sm-7 col-xs-7"> {{ Options::company_maticni() }} &nbsp;</span>
					</li>
				@endif
				@if( Options::company_email() != '')
					<li class="row">
						<span class="lab col-md-3 col-sm-5 col-xs-5">{{ Language::trans('E-mail') }}:</span>
						<span class="col-md-9 col-sm-7 col-xs-7">
							<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
						</span>
					</li>
				@endif
			</ul>

			@if(Options::company_map() != '' && Options::company_map() != ';')
			<div class="map-frame relative">
				
				<div class="map-info">
					<h5>{{ Options::company_name() }}</h5>
					<h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
				</div>

				<iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

			</div> 
			@endif
		</div> 
	</div>

	<br class="hidden-xs">
</div>

 

@if(Session::get('message'))
<script>
	$(document).ready(function(){ 
		alertSuccess("{{ Session::get('message') }}");
	});
</script>
@endif
@endsection     