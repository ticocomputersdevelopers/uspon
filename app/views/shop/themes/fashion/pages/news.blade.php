@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 
	<div class="row">

		<!-- COPIED FROM SECTION-NEWS -->
		
		@foreach(All::getNews() as $row) 
		<div class="col-md-4 col-sm-6">
			<div class="card-blog relative"> 
			 	<a class="overlink" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"></a>


            @if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))
              
            	<div class="bg-img" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></div>
            
            @else
            	
            	<div class="iframe-padd relative">
            		<iframe width="560" height="315" src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>         
            	</div>	          
            
            @endif
				<div class="new_description"> 
					<h3 class="blogs-title">
						<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">{{ $row->naslov }}</a>
					</h3>

					<div class="blogs-date text-uppercase clearfix">
						<span class="date_blog"> {{ Support::date_convert($row->datum) }} </span>

						<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}">{{ Language::trans('Pročitaj više') }}</a>
					</div>
				</div>
			</div>
		</div>
		@endforeach 

	</div> 
	
	<div>
		{{ All::getNews()->links() }}
	</div>  
@endsection