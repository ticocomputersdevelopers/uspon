
<title>{{ $title }}</title>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="{{ $description }}" />
<meta name="keywords" content="{{ $keywords }}" />
<meta name="author" content="{{Options::company_name()}}" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<meta property="og:title" content="{{ $title }}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"}}" />
<meta property="og:image" content="{{isset($og_image) ? Options::domain().$og_image : ''}}" />
<meta property="og:description" content="{{ $description }}" />
 
<!-- Bootstrap CDN --> 
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->

<!-- BOOTSTRAP LOCAL -->  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
<link href="{{ Options::domain() }}css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="{{ Options::domain() }}js/bootstrap.min.js"></script>

<!-- Sweet Alert -->
<script type="text/javascript" src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>  
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.1/sweetalert.min.css"/> 
<link href="{{ Options::domain().'css/themes/'.Support::theme_path()}}style.css" rel="stylesheet" type="text/css" />  

<!-- ROBOTO FONT -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500&display=swap" rel="stylesheet">
 
<!-- FAVICON -->
<link rel="icon" type="image/png" href="{{Options::domain()}}favicon.ico">

 <!-- FONT AWESOME 5 -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
   
<link href="{{Options::domain()}}css/slick.css" rel="stylesheet" type="text/css" /> 

@if(Session::has('b2c_admin'.Options::server()))
<link rel="stylesheet" href="{{ Options::domain() }}css/1.11.4_jquery-ui.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="{{ Options::domain() }}css/front_admin/style.css" rel="stylesheet" type="text/css" >
@endif

@include('shop/google_analytics')

@include('shop/chat')

<script>
	localStorage.setItem('translates','{{ Language::js_translates() }}');
</script>