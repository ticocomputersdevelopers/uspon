@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div class="container"> 
 	<br>
 	
	<h2><span class="section-title text-center center-block">{{ Language::trans('Kontakt') }}</span></h2> 

	<div class="row">

		<div class="col-md-6 col-sm-12 col-xs-12"> 

			<form method="POST" action="{{ Options::base_url() }}contact-message-send">
				<div class="form-group">
					<label id="label_name">{{ Language::trans('Vaše ime') }} *</label>
					<input class="contact-name" name="contact-name" id="JScontact_name" type="text" >
					<div class="error red-dot-error">{{ $errors->first('contact-name') ? $errors->first('contact-name') : "" }}</div>
				</div> 

				<div class="form-group">
					<label id="label_email">{{ Language::trans('Vaša e-mail adresa') }} *</label>
					<input class="contact-email" name="contact-email" id="JScontact_email" type="text" >
					<div class="error red-dot-error">{{ $errors->first('contact-email') ? $errors->first('contact-email') : "" }}</div>
				</div>		

				<div class="form-group">	
					<label id="label_message">{{ Language::trans('Vaša poruka') }} </label>
					<textarea class="contact-message" name="contact-message" rows="5" id="message"></textarea>
					<div class="error red-dot-error">{{ $errors->first('contact-message') ? $errors->first('contact-message') : "" }}</div>
				</div> 

				<div class="form-group capcha text-center"> 
					{{ Captcha::img(5, 160, 50) }}<br>
					<span>{{ Language::trans('Unesite kod sa slike') }}</span>
					<input type="text" name="captcha-string" tabindex="10" autocomplete="off">
					<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
				</div>

				<div class="text-right"> 
					<button type="submit" class="button">{{ Language::trans('Pošalji') }}</button>
				</div>
			</form>
		</div>

		
		<div class="col-md-6 col-sm-12 col-xs-12">  
  
			@if(Options::company_name() != '')
			<div class="row">
				<span class="col-md-3 col-sm-3 col-xs-3"><label>{{ Language::trans('Firma') }}:</label></span>
				<span class="col-md-9 col-sm-9 col-xs-9">{{ Options::company_name() }}</span>
			</div> 
			@endif

			@if(Options::company_adress() != '')
			<div class="row">
				<span class="col-md-3 col-sm-3 col-xs-3"><label>{{ Language::trans('Adresa') }}:</label></span>
				<span class="col-md-9 col-sm-9 col-xs-9">{{ Options::company_adress() }}</span>
			</div>
			@endif

			@if(Options::company_city() != '')
			<div class="row">
				<span class="col-md-3 col-sm-3 col-xs-3"><label>{{ Language::trans('Grad') }}:</label></span>
				<span class="col-md-9 col-sm-9 col-xs-9">{{ Options::company_city() }}</span>
			</div>
			@endif

			@if(Options::company_phone() != '')
			<div class="row">
				<span class="col-md-3 col-sm-3 col-xs-3"><label>{{ Language::trans('Telefon') }}:</label></span>
				<span class="col-md-9 col-sm-9 col-xs-9">{{ Options::company_phone() }}</span>
			</div>
			@endif

			@if(Options::company_fax() != '')
			<div class="row">
				<span class="col-md-3 col-sm-3 col-xs-3"><label>{{ Language::trans('Fax') }}:</label></span>
				<span class="col-md-9 col-sm-9 col-xs-9">{{ Options::company_fax() }}</span>
			</div>
			@endif

			@if(Options::company_email() != '')
			<div class="row">
				<span class="col-md-3 col-sm-3 col-xs-3"><label>{{ Language::trans('E-mail') }}:</label></span>
				<span class="col-md-9 col-sm-9 col-xs-9">
					<a class="mailto" href="mailto:{{ Options::company_email() }}">{{ Options::company_email() }}</a>
				</span>
			</div>
			@endif

			@if(Options::company_pib() != '')
			<div class="row">
				<span class="col-md-3 col-sm-3 col-xs-3"><label>{{ Language::trans('PIB') }}:</label></span>
				<span class="col-md-9 col-sm-9 col-xs-9">{{ Options::company_pib() }}</span>
			</div>
			@endif

			@if(Options::company_maticni() != '')
			<div class="row">
				<span class="col-md-3 col-sm-3 col-xs-3"><label>{{ Language::trans('Matični broj') }}:</label></span>
				<span class="col-md-9 col-sm-9 col-xs-9">{{ Options::company_maticni() }}</span>
			</div>
			@endif  
		</div>
	</div>
	
	<br>

	@if(Options::company_map() != '' && Options::company_map() != ';')
	<div class="map-frame relative">
		
		<div class="map-info">
			<h5>{{ Options::company_name() }}</h5>
			<h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
		</div>

		<iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

	</div> 
	@endif

</div>
 

@if(Session::get('message'))
<script>
	$(document).ready(function(){ 
		alertSuccess("{{ Session::get('message') }}");
	});
</script>
@endif
@endsection     

