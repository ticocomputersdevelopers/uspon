@if(Options::web_options(118))

<div class="bound-section">
    <div class="row">
        <div class="col-md-12">
            <h2 class="section-heading">
                {{Language::trans('Vezani artikli')}}
            </h2>
        </div>
         
            <div class="col-md-12 no-padding JSproduct-slider">
                 
            @foreach($vezani_artikli as $row)
                @if(Product::checkView($row->roba_id))

                <div class="JSproduct col-md-3">

                    <div class="shop-product-card"> 

                        <!-- SALE PRICE -->
                        @if(All::provera_akcija($row->roba_id))
                            <div class="sale-label">
                                {{ Language::trans('Akcija') }}
                                <!-- <span class="for-sale-price">- {{ Product::getSale($row->roba_id) }} din</span> -->
                            </div>
                        @endif



                    
                        <div class="product-image-div">
                        <!-- PRODUCT IMAGE -->
                            <a class="product-image-wrapper" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->vezani_roba_id))}}">

                                <img class="product-image img-responsive" 
                                src="{{ Options::domain() }}{{ Product::web_slika($row->vezani_roba_id) }}" alt="{{ Product::seo_title($row->vezani_roba_id) }}" />

                            </a>

                            <div class="button-div">
                                <span class="article-compare JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}">
                                    <i title="{{ Language::trans('Uporedi') }}" class="fas fa-exchange-alt"></i>
                                </span>

                                @if($row->flag_cena == 1)       
                                    @if(AdminSupport::getStatusArticle($row->roba_id) == 1)
                                        @if(Cart::check_avaliable($row->vezani_roba_id) > 0)
                                          <!--   <div class="JSadd-to-cart-vezani buy-btn dodavnje" data-roba_id="{{ $row->vezani_roba_id }}">
                                                {{Language::trans('Dodaj u korpu')}}
                                            </div> -->
                                             <a class="buy-btn" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->vezani_roba_id))}}">
                                                {{ Language::trans('Detaljnije') }} 
                                            </a>    
                                        @else  
                                            <div class="not-available">
                                                {{Language::trans('Nije dostupno')}}
                                            </div>
                                        @endif

                                        @else
                                            <div class="not-available">
                                                {{ AdminSupport::find_flag_cene(AdminSupport::getStatusArticle($row->roba_id),'naziv') }}
                                            </div>
                                      @endif
                                @endif

                                @if(Cart::kupac_id() > 0)
                                    <div class="wish-div">
                                        <span data-roba_id="{{$row->vezani_roba_id}}" data-toggle="tooltip" title="Dodaj na listu želja" class="JSadd-to-wish wish-list">
                                            <i class="far fa-heart"></i>
                                        </span>
                                    </div> 
                                @else
                                    <div class="wish-div">
                                        <a href="{{Options::base_url()}}prijava">
                                            <span data-roba_id="{{$row->vezani_roba_id}}" data-toggle="tooltip" title="Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima" class="wish-list" disabled>
                                                <i class="far fa-heart"></i>
                                            </span>
                                        </a>
                                    </div>  
                                @endif  
                            </div>
                        </div>


                     

                        <!-- PRODUCT TITLE -->
                        <div class="product-name-div">
                            <a class="product-name" href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->vezani_roba_id))}}">
                                <h2> 
                                    {{ Product::short_title($row->vezani_roba_id) }} 
                                </h2>
                            </a>
                        
                            <div class="price-holder">
                                <span class="product-price"> 
                                    {{ Cart::cena(Product::get_price($row->vezani_roba_id)) }} 
                                </span>

                                <!-- @if(All::provera_akcija($row->roba_id))
                                    <span class="product-old-price">{{ Cart::cena(Product::old_price($row->vezani_roba_id)) }}</span>
                                    @endif  -->    
                            </div>   
                        </div>
                        <!-- 
                            <a href="{{Options::base_url()}}{{Url_mod::convert_url('artikal')}}/{{Url_mod::url_convert(Product::seo_title($row->vezani_roba_id))}}">
                            <img class="product-image" src="{{ Options::domain() }}{{ Product::web_slika($row->vezani_roba_id) }}" alt="{{ Product::seo_title($row->vezani_roba_id) }}" />
                            </a> -->
                           

            
                        @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array(200)))
                            <a class="article-edit-btn" target="_blank" href="{{ Options::domain() }}admin/product/{{ $row->vezani_roba_id }}">
                                {{Language::trans('IZMENI ARTIKAL')}}
                            </a>
                        @endif


                   
                </div> <!-- End of JSproduct card -->

             </div> <!-- End of JSproduct -->


                @endif
            @endforeach
         
           

       </div> <!-- End of col -->
    </div> <!-- End of row -->
</div>
@endif