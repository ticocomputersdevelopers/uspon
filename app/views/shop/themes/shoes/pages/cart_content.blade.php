@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="half-width"> 

	@if(Session::has('b2c_korpa') and Cart::broj_cart() >= 1 )

	<h2><span class="page-title">{{ Language::trans('Korpa') }}</span></h2>

	@if(Session::has('failure-message'))
	<h4>{{ Session::get('failure-message') }}</h4>
	@endif
	<ul class="cart-labels row hidden-sm hidden-xs">
		<li class="col-md-3 col-sm-3 col-sm-offset-2 col-md-offset-2">{{ Language::trans('Proizvod') }}</li>			 
		<li class="col-md-2 col-sm-2">{{ Language::trans('Cena') }}</li>
		<li class="col-md-2 col-sm-2">{{ Language::trans('Količina') }}</li>
		<li class="col-md-2 col-sm-2">{{ Language::trans('Ukupno') }}</li> 	 
	</ul>
	@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
	<ul class="JScart-item row">	 

		<li class="col-md-2 col-sm-3 col-xs-3">
			<img src="{{ Options::domain() }}{{Product::web_slika($row->roba_id) }}" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image img-responsive" />
		</li>

		<li class="cart-name col-md-3 col-sm-9 col-xs-9">
			<span> 
				<a class="inline-block" href="{{ Options::base_url() }}{{Url_mod::slug_trans('artikal')}}/{{ Url_mod::slugify(Product::seo_title($row->roba_id)) }}">
					{{ Product::short_title($row->roba_id) }}
				</a>
				{{Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids)}}   
			</span>
		</li>

		<li class="cart-price col-md-2 col-sm-2 col-xs-4 hidden-xs"><span>{{ Cart::cena($row->jm_cena) }}</span></li>

		<li class="col-md-2 col-sm-3 col-xs-12">
			<div class="cart-add-amount clearfix">
				<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-less" href="javascript:void(0)" rel=”nofollow”><i class="fas fa-minus"></i></a>

				<input type="text" class="JScart-amount" value="{{ round($row->kolicina) }}" onkeypress="validate(event)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">
				<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-more" href="javascript:void(0)" rel=”nofollow”><i class="fas fa-plus"></i></a>
			</div>
		</li>

		<li class="cart-total-price col-md-2 col-sm-3 col-xs-12"><span class="JScart-item-price" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">{{ Cart::cena($row->jm_cena*$row->kolicina) }}</span></li>

		<li class="cart-remove col-md-1 col-sm-1 col-xs-12">
			<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="remove-item JSdelete_cart_item" rel=”nofollow”><span class="fas fa-times"></span></a>
		</li>			 
	</ul>
	@endforeach


	<br>

	<button class="button" id="JSDeleteCart">{{ Language::trans('Isprazni korpu') }}</button>


	<div class="row clearfix">  
		<div class="cart-sum col-md-4 col-sm-7 col-xs-12 pull-right text-right">
			
			@if(($troskovi = Cart::troskovi())>0) 
			<div>
				<span class="sum-label text-right">{{ Language::trans('Cena artikala') }}: </span>
				<span class="JSdelivery_total_amount sum-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
			</div>

			<div class="JSDelivery">
				<span class="sum-label text-right">{{ Language::trans('Troškovi isporuke') }}: </span>
				<span class="JSexpenses sum-amount">{{ $troskovi }}</span>
			</div>

			<div>
				<span class="sum-label text-right">{{ Language::trans('Ukupno') }}: </span>
				<span class="JStotal_amount JStotal_amount_weight sum-amount">{{Cart::cena(Cart::cart_ukupno()+$troskovi)}}</span>
			</div>

			@else
			<div>
				<span>{{ Language::trans('Ukupno') }}: </span>
				<span class="JSdelivery_total_amount sum-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
				
				<div class="JSfree_delivery text-right">
					<i class="fas fa-plus"></i> {{ Language::trans('Besplatna dostava') }} 
				</div> 
			</div>			
			@endif
		</div> 
	</div>

	<!-- CART ACTION BUTTONS -->
	<div class="cart-action-buttons text-right" id="cart_form_scroll">
		@if(!Session::has('b2c_kupac'))
			@if(Options::neregistrovani_korisnici()==1)
			
			<button class="button" id="JSRegToggle">{{ Language::trans('Kupi bez registracije') }}</button>
			
			@endif

			<a class="button inline-block" href="#" role="button" id="login-icon" data-toggle="modal" data-target="#loginModal">{{ Language::trans('Prijavi se') }}</a>
		@endif
	</div> 

	<br>

	<div id="JSRegToggleSec" {{ Session::has('b2c_kupac') ? "" : (count(Input::old()) == 0 ? "hidden='hidden'" : "") }}> 
		
		<form method="POST" action="{{ Options::base_url() }}order-create" id="JSOrderForm" class="without-reg-form row">
			@if(Options::neregistrovani_korisnici()==1 AND !Session::has('b2c_kupac'))
			<div class="row text-center"> 
				
				@if(Input::old('flag_vrsta_kupca') == 1)
				<div class="col-md-6 col-sm-6 col-xs-6"> 
					<div class="JScheck_user_type without-btn personal" data-vrsta="personal">
						{{ Language::trans('Fizičko Lice') }} <i class="fas fa-check"></i> 
					</div>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-6"> 
					<div class="JScheck_user_type without-btn active none-personal" data-vrsta="non-personal">
						{{ Language::trans('Pravno Lice') }} <i class="fas fa-check"></i>
					</div>
				</div>
				@else
				<div class="col-md-6 col-sm-6 col-xs-6"> 
					<div class="JScheck_user_type without-btn active personal" data-vrsta="personal">
						{{ Language::trans('Fizičko Lice') }} <i class="fas fa-check"></i>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6"> 
					<div class="JScheck_user_type without-btn none-personal" data-vrsta="non-personal">
						{{ Language::trans('Pravno Lice') }} <i class="fas fa-check"></i>
					</div>
				</div>
				@endif 

			</div>
			@endif
			<input type="hidden" name="flag_vrsta_kupca" value="{{ Input::old('flag_vrsta_kupca') == 1 ? '1' : '0' }}">

			@if(Options::neregistrovani_korisnici()==1 AND !Session::has('b2c_kupac'))
			<div class="row"> 
				<div class="col-md-6 col-sm-12 col-xs-12 form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
					<label for="without-reg-company"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Naziv Firme') }}:</label>
					<input id="without-reg-company" class="form-control" name="naziv" type="text" tabindex="1" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}</div>
				</div>

				<div class="col-md-3 col-sm-12 col-xs-12 form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
					<label for="without-reg-pib">{{ Language::trans('PIB') }}:</label>
					<input id="without-reg-pib" class="form-control" name="pib" type="text" tabindex="2" value="{{ htmlentities(Input::old('pib') ? Input::old('pib') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('pib') ? $errors->first('pib') : "" }}</div>
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12 form-group JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
					<label for="without-reg-maticni_br">{{ Language::trans('Matični broj') }}:</label>
					<input id="without-reg-maticni_br" class="form-control" name="maticni_br" type="text" tabindex="2" value="{{ htmlentities(Input::old('maticni_br') ? Input::old('maticni_br') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('maticni_br') ? $errors->first('maticni_br') : "" }}</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12 form-group JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
					<label for="without-reg-name"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Ime') }}</label>
					<input id="without-reg-name" class="form-control" name="ime" type="text" tabindex="1" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('ime') ? $errors->first('ime') : "" }}</div>
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12 form-group JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
					<label for="without-reg-surname"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Prezime') }}</label>
					<input id="without-reg-surname" class="form-control" name="prezime" type="text" tabindex="2" value="{{ htmlentities(Input::old('prezime') ? Input::old('prezime') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}</div>
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12 form-group"> 
					<label for="without-reg-phone"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Telefon') }}</label>
					<input id="without-reg-phone" class="form-control" name="telefon" type="text" tabindex="3" value="{{ htmlentities(Input::old('telefon') ? Input::old('telefon') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}</div>
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12 form-group"> 
					<label for="without-reg-e-mail"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('E-mail') }}</label>
					<input id="JSwithout-reg-email" class="form-control" name="email" type="text" tabindex="4" value="{{ htmlentities(Input::old('email') ? Input::old('email') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('email') ? $errors->first('email') : "" }}</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12 form-group"> 
					<label for="without-reg-address"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Adresa za dostavu') }}</label>
					<input id="without-reg-address" class="form-control" name="adresa" type="text" tabindex="5" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12 form-group"> 
					<label for="without-reg-city"><span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }}</label>				 
					<input type="text" class="form-control" name="mesto" tabindex="6" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}">
					<div class="error red-dot-error">{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}</div>
				</div>
			</div>
			@endif

			@if(Options::neregistrovani_korisnici()==1 OR Session::has('b2c_kupac'))
			@if(Session::has('b2c_kupac') AND Options::web_options(314)==1)
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12 form-group">
					<label>{{ Language::trans('Bodovi') }}</label>
					<div>{{ Language::trans('Ovom kupovinom broj bodova koji možete ostvariti je') }} <span id="JSAchievePoints">{{ Cart::bodoviOstvareniBodoviKorpa() }}</span>.</div>
					<div>{{ Language::trans('Broj bodova kiji imate je') }} {{ WebKupac::bodovi() }}.
						@if(!is_null($trajanjeBodova = WebKupac::trajanjeBodova()))
						{{ Language::trans('Rok važenja bodova je') }} {{ $trajanjeBodova }}.
						@endif
					</div>
					<div>{{ Language::trans('Maksimalni broj bodova kiji možete iskoristiti u ovoj kupovini je') }} <span id="JSMaxUsingPoints">{{ Cart::bodoviPopustBodoviKorpa() }}</span>.</div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12 form-group">
					<label>{{ Language::trans('Unesite bodove koje želite da iskoristite') }}:</label>
					<input type="text" class="form-control" name="bodovi" tabindex="6" value="{{ htmlentities(Input::old('bodovi') ? Input::old('bodovi') : '') }}">
					<div class="error red-dot-error">{{ Session::has('bodovi_error') ? Session::get('bodovi_error') : '' }}</div>
				</div>
			</div>		
			@endif

			@if(Session::has('b2c_kupac') AND Options::web_options(318)==1)
			<div class="row">
				<div class="col-md-6 col-sm-12 col-xs-12 form-group">
					<label>{{ Language::trans('Vaučeri') }}</label>
					<div>{{ Language::trans('Maksimalni popust preko vaučera kiji možete iskoristiti u ovoj kupovini je') }} <span id="JSMaxUsingVoucherPrice">{{ Cart::cena(Cart::vauceriPopustCenaKorpa()) }}</span></div>
				</div>
				<div class="col-md-6 col-sm-12 col-xs-12 form-group">
					<label>{{ Language::trans('Unesite kod sa vašeg vaučera') }}:</label>
					<input type="text" class="form-control" name="vaucer_code" tabindex="6" value="{{ htmlentities(Input::old('vaucer_code') ? Input::old('vaucer_code') : '') }}">
					<div class="error red-dot-error">{{ Session::has('vaucer_error') ? Session::get('vaucer_error') : '' }}</div>
				</div>
			</div>		
			@endif

			<div class="row"> 
				<div class="col-md-6 col-sm-12 col-xs-12 form-group">
					<label>{{ Language::trans('Način isporuke') }}:</label>
					<select name="web_nacin_isporuke_id" class="JSdeliveryInput form-control" tabindex="7" >
						{{Order::nacin_isporuke(Input::old('web_nacin_isporuke_id'))}}
					</select>
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12 form-group">
					<label>{{ Language::trans('Način plaćanja') }}:</label>
					<select name="web_nacin_placanja_id" class="form-control" tabindex="8">
						{{Order::nacin_placanja(Input::old('web_nacin_placanja_id'))}}
					</select>
				</div>
			</div>

			<div class="row"> 
				<div class="col-md-6 col-sm-6 col-xs-12 form-group"> 
					<label>{{ Language::trans('Napomena') }}:</label>
					@if(Cart::kamata(Cart::korpa_id()) >0 )					
					<textarea class="form-control" rows="5" tabindex="9" name="napomena">{{ Language::trans('Kupovina na:') }} {{ Cart::kamata(Cart::korpa_id()) }} {{ Language::trans('rata') }}</textarea>
					@else
					<textarea class="form-control" rows="5" tabindex="9" name="napomena">{{ htmlentities(Input::old('napomena') ? Input::old('napomena') : '') }}</textarea>
					@endif
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 form-group"> 
					<label class="hidden-xs">&nbsp;</label>
					<div class="capcha text-center"> 
						{{ Captcha::img(5, 160, 50) }}<br>
						<span>{{ Language::trans('Unesite kod sa slike') }}</span>
						<input type="text" name="captcha-string" class="form-control" tabindex="10" autocomplete="off">
						<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">  
					 <span class="red-dot-error"><i class="fas fa-star-of-life"></i></span> {{ Language::trans('Obavezna polja') }}
				</div>
			</div>
			@endif

			@if(Options::neregistrovani_korisnici()==1 OR Session::has('b2c_kupac'))
			<div class="text-center col-md-12 col-sm-12 col-xs-12"> 
				<button id="JSOrderSubmit" class="button">{{ Language::trans('Završi kupovinu') }}</button>
			</div>
			@endif
		</form>	
	</div>

	@else

	<h2><span class="page-title">{{ Language::trans('Korpa') }}</span></h2>

	<div class="empty-page-label">{{ Language::trans('Vaša korpa je trenutno prazna') }}.</div>

	@endif
</div>

<br>

@endsection