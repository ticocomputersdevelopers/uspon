@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
    <div class="category-listing col-md-9 col-sm-9 col-xs-12 no-padding">
        <?php 
            $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); 
        ?>

        @foreach ($query_category_first as $row1)
            @if(Groups::broj_cerki($row1->grupa_pr_id) >0)
                <!-- ROW 1 -->
               <div class="row">
                    <h2 id="{{ Language::trans($row1->grupa_pr_id)  }}" class="category-heading col-md-12 col-sm-12 col-xs-12">{{ Language::trans($row1->grupa) }} <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span></h2>

                    <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

                    @foreach ($query_category_second->get() as $row2)
                       <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <a class="category-name-link" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">{{ Language::trans($row2->grupa) }} <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}})</span></a>

                            <ul class="category__list">
                                <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                                @foreach($query_category_third as $row3)
                                <li class="category__list__item">
                                    <a class="category__list__item__link" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }} 
                                        <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row3->grupa_pr_id)}})</span>
                                    </a>
                                    <ul class="category__list">
                                        <?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                                        @foreach($query_category_forth as $row4)
                                        <li class="category__list__item">
                                            <a class="category__list__item__link" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }} 
                                                <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row4->grupa_pr_id)}})</span>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach

               </div> <!-- end .row -->
                <!-- END ROW 1 -->
            @else
                <div class="row">
                    <h2 id="{{ Language::trans($row1->grupa_pr_id)  }}" class="category-heading col-md-12 col-sm-12 col-xs-12">{{ Language::trans($row1->grupa)  }} 
                        <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span></h2>

                    <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

                    @foreach ($query_category_second->get() as $row2)
                       <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <a class="category-name-link" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">{{ Language::trans($row2->grupa) }} <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row2->grupa_pr_id)}})</span></a>

                            <ul class="category__list">
                                <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                                @foreach($query_category_third as $row3)
                                <li class="category__list__item">
                                    <a class="category__list__item__link" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }} 
                                        <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row3->grupa_pr_id)}})</span>
                                    </a>
                                    <ul class="category__list">
                                        <?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
                                        @foreach($query_category_forth as $row4)
                                        <li class="category__list__item">
                                            <a class="category__list__item__link" href="{{ Options::base_url()}}{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }} 
                                                <span class="category__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row4->grupa_pr_id)}})</span>
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach

               </div> <!-- end .row -->                        

            @endif
        @endforeach 

    </div><!-- end #main-content -->

     <!-- MAIN CONTENT -->
    <div class="sticky-element col-md-3 col-sm-3 col-xs-3">  
        <ul class="JScategory-sidebar__list">
            <li class="JScategory-sidebar__list__toggler"></li>
            <li> 
                <ul class="scrollable"> 
                    @foreach ($query_category_first as $row1)
                    <li class="category-sidebar__list__item">
                        <a href="#{{ Language::trans($row1->grupa_pr_id) }}" class="category-sidebar__list__item__link">{{ Language::trans($row1->grupa)  }} 
                            <span class="category-sidebar__list__item__link__span">({{Articles::brojiArtikleRekurzivno($row1->grupa_pr_id)}})</span>
                        </a>
                    </li>
                    @endforeach 
                </ul>
            </li>
        </ul>

    </div>

@endsection