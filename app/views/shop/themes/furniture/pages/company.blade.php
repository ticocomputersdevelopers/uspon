@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<!-- <div class="row login-page-padding flex"> 
	<div class="col-md-5 col-sm-12 col-xs-12 log-in">
		<h3>{{ Language::trans('Registracija za pravna lica') }}</h3><br>
		<div><b>  {{ Language::trans('Prednosti registracije') }} </b></div><br>
		<div>
			<i class="fas fa-angle-double-right"></i>&nbsp; {{ Language::trans('Brza kupovina') }}.
		</div><br>
		<div>
			<i class="fas fa-angle-double-right"></i>&nbsp; {{ Language::trans('Mogućnost skupljanja poena i ostvarivanja povremenih popusta') }}. 
		</div><br>
		<div>
			<i class="fas fa-angle-double-right"></i>&nbsp; {{ Language::trans('Ostvarivanje partnerskih popusta i mogućnost dobijanja dilerskih cena') }}.
		</div>
	</div>	

	<div class="col-md-5 col-sm-12 col-xs-12"> 
		<form action="{{ Options::base_url()}}registracija-post" method="post" autocomplete="off">
			<input type="hidden" name="flag_vrsta_kupca" value="1">

			<div>
				<label for="naziv">{{ Language::trans('Naziv firme') }}:</label>
				<input id="naziv" name="naziv" type="text" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" >
				<div class="error red-dot-error">{{ $errors->first('naziv') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('naziv') : "" }}
				</div>
			</div>

			<div>
				<label for="pib">{{ Language::trans('PIB') }}</label>
				<input id="pib" name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : '' }}" >
				<div class="error red-dot-error">{{ $errors->first('pib') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('pib') : "" }}</div>
			</div>	

			<div>
				<label for="maticni_br">{{ Language::trans('Matični broj') }}</label>
				<input id="maticni_br" name="maticni_br" type="text" value="{{ Input::old('maticni_br') ? Input::old('maticni_br') : '' }}" >
				<div class="error red-dot-error">{{ $errors->first('maticni_br') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('maticni_br') : "" }}</div>
			</div>	

			<div>
				<label for="email">E-mail</label>
				<input id="email" autocomplete="off" name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : '' }}" >
				<div class="error red-dot-error">{{ $errors->first('email') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('email') : "" }}
				</div>
			</div>	

			<div>
				<label for="lozinka">{{ Language::trans('Lozinka') }}</label>
				<input id="lozinka" name="lozinka" type="password" value="{{ htmlentities(Input::old('lozinka') ? Input::old('lozinka') : '') }}">
				<div class="error red-dot-error">{{ $errors->first('lozinka') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('lozinka') : "" }}</div>
			</div>

			<div>
				<label for="telefon">{{ Language::trans('Telefon') }}</label>
				<input id="telefon" name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : '' }}" >
				<div class="error red-dot-error">{{ $errors->first('telefon') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('telefon') : "" }}</div>
			</div>

			<div>
				<label for="adresa">{{ Language::trans('Adresa') }}</label>
				<input id="adresa" name="adresa" type="text" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" >
				<div class="error red-dot-error">{{ $errors->first('adresa') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('adresa') : "" }}</div>
			</div>

			<div>
				<label for="mesto">{{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }}</label>
				<input id="mesto" type="text" name="mesto" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}" >
				<div class="error red-dot-error">{{ $errors->first('mesto') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('mesto') : "" }}</div>
			</div>

			<div class="text-right">   
				<button type="submit" class="button">{{ Language::trans('Registruj se') }}</button>
			</div> 
		</form>

		@if(Session::get('message'))
		<script type="text/javascript">
			bootbox.alert({
				message: "<p>{{ Language::trans('Poslali smo Vam potvrdu o registraciji na e-mail koji ste uneli. Molimo Vas da potvrdite Vašu e-mail adresu klikom na link iz e-mail poruke') }}.</p>"
			});
		</script>
		@endif
	</div>
</div> -->

@endsection