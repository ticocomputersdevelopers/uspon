<link rel="stylesheet" href="">
<section class="medium-12 columns">
	<div class="flat-box">
		<div class="row small-gutter art-row">
			<div class="columns medium-6">
				<h2 class="title-med">{{ AdminLanguage::transAdmin('Magacini') }}</h2><br>
				<div class="btn-container box-sett"> 
					<input type="text" id="myInput" placeholder="Pretraga..">
				</div>
				<table id="myTable">
				   <tr class="header">
				    <th>{{ AdminLanguage::transAdmin('Magacini') }}</th>
				    <th>{{ AdminLanguage::transAdmin('Prikaz') }}</th>
				  </tr>
				  <p style="text-align: right;font-size: 13px;"><input type="checkbox" name="select-all-warehouse" id="select-all-warehouse"/>{{ AdminLanguage::transAdmin('Selektuj sve') }}</p>
				  @foreach($lageri as $row)
				  	<tr>
				  		<td>&nbsp; {{ $row->naziv }}</td>
				  		<?php $flag = DB::table('nivo_pristupa_lager')->where('orgj_id', $row->orgj_id)->where('partner_id', $partner_id)->where('unactive', 1)->get(); ?>
				  	<td>
				    	<span>
				    		<input type="checkbox" class="JSLager" data-id="{{ $row->orgj_id }}" @if($flag == null) : checked @endif>
				    	</span>
				    </td>
				  	</tr>
				  @endforeach
				</table>
			</div>

			<div class="columns medium-6"> 
				<h2 class="title-med">{{ AdminLanguage::transAdmin('Partneri') }}</h2><br>
				  	
				<div class="btn-container box-sett"> 
				<select id="access-warehouse-parner-change" name="Partner" class="admin-select m-input-and-button__input">
				  		<option value="-2" selected>{{ AdminLanguage::transAdmin('Nedefinisan') }}</option>
				  		@foreach($partneri as $row)
				  			@if($partner_id == $row->partner_id)
				  				<option value="{{$row->partner_id}}" selected>
				  				{{$row->naziv}}
				  				</option>
				  			@else
				  				<option value="{{$row->partner_id}}">{{$row->naziv}}</option>
				  			@endif
				  		@endforeach
				  	</select>
				</div>
			</div>

		</div>
	</div>
</section>
