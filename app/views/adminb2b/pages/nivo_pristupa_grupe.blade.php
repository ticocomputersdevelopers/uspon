<div class="row small-gutter art-row">
<div id="wait">
	<p><img class="loader" src="{{ AdminOptions::base_url()}}images/admin/wheel.gif">{{ AdminLanguage::transAdmin('Molimo vas sačekajte') }}...</p>
</div> 
<?php $query_gr=AdminSupport::query_gr_level(0); ?>
	<div class="columns medium-12">
		<h2 class="title-med">{{ AdminLanguage::transAdmin('Partneri') }}</h2><br>
		  	
		<div class="btn-container box-sett"> 
		<select id="access-groups-parner-change" data-id="{{$grupa_pr_id}}" name="Partner" class="admin-select m-input-and-button__input">
		  		<option value="-2" selected>{{ AdminLanguage::transAdmin('Nedefinisan') }}</option>
		  		@foreach($partneri as $row)
		  			@if($partner_id == $row->partner_id)
		  				<option value="{{$row->partner_id}}" selected>
		  				{{$row->naziv}}
		  				</option>
		  			@else
		  				<option value="{{$row->partner_id}}">{{$row->naziv}}</option>
		  			@endif
		  		@endforeach
		  	</select>
		</div>
	</div>

	<div class="flat-box">

		<div class="row">
			<div class="columns medium-12 small-12">
				<h2 class="title-med">{{ AdminLanguage::transAdmin('Grupe') }}</h2><br>
				  	
				<div class="btn-container box-sett"> 
					<input type="text" id="myInput" placeholder="Pretraga..">
				</div>
				
				 <p style="text-align: right;font-size: 13px;">
				  	<input type="checkbox" name="select-all-groups" id="select-all-groups"/>{{ AdminLanguage::transAdmin('Selektuj sve') }}
				 </p>

				<div class="group-like-table">
				<div class="clearfix heading-bold">
				   	<span class="inline-block"> {{ AdminLanguage::transAdmin('GRUPE') }} </span>
				    <span class="right inline-block"> {{ AdminLanguage::transAdmin('PRIKAZ') }} </span>
				</div>

				@foreach($query_gr as $row)
			  		<?php $flag = DB::table('nivo_pristupa_grupa')->where('grupa_pr_id', $row->grupa_pr_id)->where('partner_id', $partner_id)->where('unactive', 1)->get(); ?>
			  		<ul class="JSlevel-group-1">
						<li>
							<div>
								<a href="{{AdminOptions::base_url()}}admin/b2b/access-list-group-article/{{$partner_id}}/{{$row->grupa_pr_id}}">{{ $row->grupa }}
								</a>
							</div>
							 <div class="level-chec-abs">
						    	<span>
						    		<input type="checkbox" class="JSGrupa" data-id="{{ $row->grupa_pr_id }}" @if($flag == null) : checked @endif>
						    	</span>
						    </div>
					
							<?php $query_gr1=AdminSupport::query_gr_level($row->grupa_pr_id); ?>
							<!-- LEVEL 2 -->
							<ul>
								@foreach($query_gr1 as $row1)
									<?php $flag = DB::table('nivo_pristupa_grupa')->where('grupa_pr_id', $row1->grupa_pr_id)->where('partner_id', $partner_id)->where('unactive', 1)->get(); ?>
									<li>
										<div>
											<a href="{{AdminOptions::base_url()}}admin/b2b/access-list-group-article/{{$partner_id}}/{{$row1->grupa_pr_id}}">{{ $row1->grupa }}</a>
										</div>
										 <div class="level-chec-abs">
									    	<span>
									    		<input type="checkbox" class="JSGrupa" data-id="{{ $row1->grupa_pr_id }}" @if($flag == null) : checked @endif>
									    	</span>
									    </div>
									
										<?php $query_gr2=AdminSupport::query_gr_level($row1->grupa_pr_id); ?>
										<ul>
										<!-- LEVEL 3 -->
											@foreach($query_gr2 as $row2)
											<li>
											<?php $flag = DB::table('nivo_pristupa_grupa')->where('grupa_pr_id', $row2->grupa_pr_id)->where('partner_id', $partner_id)->where('unactive', 1)->get(); ?>
											
												<div>
													<a href="{{AdminOptions::base_url()}}admin/b2b/access-list-group-article/{{$partner_id}}/{{$row2->grupa_pr_id}}">{{ $row2->grupa }}</a>
												</div>
												 <div class="level-chec-abs">
											    	<span>
											    		<input type="checkbox" class="JSGrupa" data-id="{{ $row2->grupa_pr_id }}" @if($flag == null) : checked @endif>
											    	</span>
											    </div>
											
												<?php $query_gr3=AdminSupport::query_gr_level($row2->grupa_pr_id); ?>
												<!-- LEVEL 4 -->
												<ul>
													@foreach($query_gr3 as $row3)
													<li>
													<?php $flag = DB::table('nivo_pristupa_grupa')->where('grupa_pr_id', $row3->grupa_pr_id)->where('partner_id', $partner_id)->where('unactive', 1)->get(); ?>
													
														<div>
															<a href="{{AdminOptions::base_url()}}admin/b2b/access-list-group-article/{{$partner_id}}/{{$row3->grupa_pr_id}}">{{ $row3->grupa }}</a>
														</div>
														 <div class="level-chec-abs">
													    	<span>
													    		<input type="checkbox" class="JSGrupa" data-id="{{ $row3->grupa_pr_id }}" @if($flag == null) : checked @endif>
													    	</span>
													    </div>
													</li>
													@endforeach
												</ul>
											</li>
										@endforeach
										</ul>
									</li>
								@endforeach
							</ul>
						</li>
			 		</ul>
				@endforeach <!-- END LEVEL 1 -->
				</div>
			</div>


		</div>

	</div>	
</div>
<script src="{{ AdminB2BOptions::base_url()}}js/adminb2b/admin_b2b.js" type="text/javascript"></script>
	
	