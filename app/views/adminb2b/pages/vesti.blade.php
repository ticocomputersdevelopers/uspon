@extends('adminb2b.defaultlayout')
@section('content')
<div id="main-content" class="news-page">
	<div class="news-title row">
		<div class="column medium-12"> 
			<h1 class="big">Vesti</h1>
			@if(Admin_model::check_admin(array('VESTI_AZURIRANJE')))
				<a href="{{ AdminB2BOptions::base_url() }}admin/b2b/vest/0" class="btn btn-create btn-sm">Dodaj novu</a>
			@endif
			@if(Session::has('message'))
			<script>
			alertify.success('{{ Session::get('message') }}');
			</script>
			@endif
		</div>
	</div>
	<div class="row"> 
		<div class="column medium-12">
			<div class="flat-box">
				<div class="news-filter">
					<a href="{{ AdminB2BOptions::base_url() }}admin/b2b/vesti/">Sve </a>
					<a href="{{ AdminB2BOptions::base_url() }}admin/b2b/vesti/1">Aktivne </a>
					<a href="{{ AdminB2BOptions::base_url() }}admin/b2b/vesti/0">Neaktivne </a>
				</div>
				<table class="news-table">
					<tr>
						<th>&nbsp;Naslov</th>
						<th class="text-center">Status</th>
						<th>Datum</th>
					</tr>
					@foreach($vesti as $row)
					<tr>
						<td>
							<a href="{{ AdminB2BOptions::base_url() }}admin/b2b/vest/{{ $row->web_vest_b2b_id }}" class="title">
								{{ AdminB2BVesti::findB2BTitle($row->web_vest_b2b_id) }}</a>
							<div class="news-action">
								<a href="{{ AdminB2BVesti::news_link_b2b($row->web_vest_b2b_id) }}" target="_blank">
									<i class="fa fa-eye" aria-hidden="true"></i> Vidi
								</a>
								@if(Admin_model::check_admin(array('VESTI_AZURIRANJE')))
									<a href="{{ AdminB2BOptions::base_url() }}admin/b2b/vest/{{ $row->web_vest_b2b_id }}">
									<i class="fa fa-pencil" aria-hidden="true"></i>Izmeni
									</a>
									<a class="text-alert JSbtn-delete" data-link="{{ AdminB2BOptions::base_url() }}admin/b2b/vesti/{{ $row->web_vest_b2b_id }}/delete"><i class="fa fa-trash" aria-hidden="true"></i>Obriši</a>
								@endif
							</div>
						</td>
						@if($row->aktuelno == 1)
						<td class="text-center">
							<span class="active">Aktivna</span>
						</td>
						@else
						<td class="text-center">
							<span class="inactive">Neaktivna</span>
						</td>
						@endif
						<td>{{ $row->datum }}</td>
					</tr>
					@endforeach
					<div> {{ $vesti->links() }}</div>
				</table>
			</div> <!-- end of .flat-box -->
		</div>
	</div>
</div>
@endsection