@extends('adminb2b.defaultlayout')
@section('content')

<div id="main-content">
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif
	@if(Session::has('error_message'))
	<script>
		alertify.error('{{ Session::get('error_message') }}');
	</script>
	@endif

	<div class="row"> 
		@if(Admin_model::check_admin(array('B2B_PODESAVANJA')))	
		<div class="columns medium-4">
			<div class="flat-box"> 

				<h2 class="title-med">Opcije</h2>

				@foreach(DB::table('options')->whereIn('options_id',array(3018,3028,3032,3034))->orderBy('options_id','asc')->get() as $row)
				<div class="row"> 
					<div class="column medium-10">   
						<label class="no-margin">{{ $row->naziv }}</label> 
					</div>
					<div class="column medium-2 text-right">  
						<input type="checkbox" class="JSOptionId" data-id="{{ $row->options_id }}" {{ $row->int_data == 1 ? 'checked="checked"' : '' }}>	
					</div>
				</div>
				@endforeach 

				<br>

				<h2 class="title-med">Magacini</h2> 	

				@foreach(DB::table('orgj')->where('orgj_id','<>',-1)->orderBy('orgj_id','asc')->get() as $row)
				<div class="row"> 
					<div class="column medium-7"> 
						<label class="no-margin">{{ $row->naziv }}</label>
					</div>

					<div class="column medium-5 text-right">
						<input class="JSOrgjEnable" data-id="{{ $row->orgj_id }}" type="checkbox" {{ $row->b2b == 1 ? 'checked' : '' }} {{ DB::table('imenik_magacin')->where(array('imenik_magacin_id'=>20,'orgj_id'=>$row->orgj_id))->count() == 1 ? 'disabled' : '' }}>
						
						<input name="orgj_primary" class="JSOrgjPrimary" data-id="{{ $row->orgj_id }}" type="radio" {{ DB::table('imenik_magacin')->where(array('imenik_magacin_id'=>20,'orgj_id'=>$row->orgj_id))->count() == 1 ? 'checked' : '' }} {{ $row->b2b == 1 ? '' : 'disabled' }}>
						
						<button class="JSMagtacinDelete btn btn-xm btn-secondary" data-id="{{ $row->orgj_id }}"><i class="fa fa-close"></i></button>
					</div>
				</div> 
				@endforeach

				<br>

				<h2 class="title-med">Dodaj novi magacin</h2> 	

				<div class="row">
					<div class="column medium-12">
						<input placeholder="Novi magacin" type="text" id="JSNewMagtacinVal">
					</div>
				</div> 

				<div class="btn-container text-center">
					<button class="JSNewMagtacinSave btn btn-secondary save-it-btn">Sačuvaj magacin</button>
				</div> 
			</div>
		</div>
		@endif

		<div class="columns medium-4">
			<div class="flat-box"> 
				@if(AdminB2BOptions::gnrl_options(3044) == 1)
				
				<h2 class="title-med">Excel cenovnik</h2> 	 

				<form method="POST" action="{{ AdminB2BOptions::base_url() }}admin/b2b/upload-cenovnik" enctype="multipart/form-data" class="btn-container text-center">
					<input type="file" name="cenovnik">
					<input type="submit" name="submit" value="Postavi" class="btn btn-secondary save-it-btn">
				</form> 
				@endif
			</div>
		</div>
	</div>
</div>
@endsection