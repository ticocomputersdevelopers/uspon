<section class="medium-5 columns">
	<div class="flat-box">
		<div class="row small-gutter art-row">
			<div class="columns medium-6"> 
				<h2 class="title-med">{{ AdminLanguage::transAdmin('Strane') }}</h2><br>
				<div class="btn-container box-sett"> 
					<input type="text" id="myInput" placeholder="Pretraga..">
				</div>
				<table id="myTable">
				  <tr class="header">
				    <th>{{ AdminLanguage::transAdmin('Strane') }}</th>
				    <th>{{ AdminLanguage::transAdmin('Prikaz') }}</th>
				  </tr>

				  <p style="text-align: right;font-size: 13px;"><input type="checkbox" name="select-all-pages" id="select-all-pages"/>{{ AdminLanguage::transAdmin('Selektuj sve') }}</p>
				  @foreach($strane as $row)
				  	<tr>
				  		<td>&nbsp; {{ $row->title }}</td>
				  		<?php $flag = DB::table('nivo_pristupa_strana')->where('web_b2b_seo_id', $row->web_b2b_seo_id)->where('partner_id', $partner_id)->where('unactive', 1)->get(); ?>
				  		<td>
				    	<span>
				    		<input type="checkbox" class="JSStrana" data-id="{{ $row->web_b2b_seo_id }}" @if($flag == null) : checked @endif>
				    	</span>
				    </td>
				  	</tr>
				  @endforeach
				</table>
			</div>

			<div class="columns medium-6"> 
				<h2 class="title-med">{{ AdminLanguage::transAdmin('Partneri') }}</h2><br>
				  	
				<div class="btn-container box-sett"> 
				<select id="access-pages-parner-change" name="Partner" class="admin-select m-input-and-button__input">
				  		<option value="-2" selected>{{ AdminLanguage::transAdmin('Nedefinisan') }}</option>
				  		@foreach($partneri as $row)
				  			@if($partner_id == $row->partner_id)
				  				<option value="{{$row->partner_id}}" selected>
				  				{{$row->naziv}}
				  				</option>
				  			@else
				  				<option value="{{$row->partner_id}}">{{$row->naziv}}</option>
				  			@endif
				  		@endforeach
				  	</select>
				</div>
			</div>	


		</div>			
	</div>
</section>