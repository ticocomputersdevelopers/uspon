@extends('adminb2b.defaultlayout')
@section('content')
<section id="main-content">

	<div class="m-subnav"> 
		<a class="m-subnav__link JS-nazad" href="#">
			<div class="m-subnav__link__icon">
				<i class="fa fa-arrow-left" aria-hidden="true"></i>
			</div>
			<div class="m-subnav__link__text">
				Nazad
			</div>
		</a> 
	</div>

	<div class="row"> 
		<form class="flat-box" method="POST" action="{{AdminB2BOptions::base_url()}}admin/b2b/narudzbina">
			<input type="hidden" id="narudzbina_id" name="web_b2b_narudzbina_id" value="{{ $web_b2b_narudzbina_id }}">
			<div class="row">
				<div class="columns medium-6"> 
					<div class="row"> 
						<div class="customer-name medium-12 columns field-group">
							<label for="">Ime kupca</label>
							<select name="partner_id" id="JSWebKupac" class="{{ $errors->first('partner_id') ? 'error' : '' }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								{{ AdminB2BProizvodjac::partnerSelect(!is_null(Input::old('partner_id')) ? Input::old('partner_id') : $partner_id) }}
							</select>
							@if(Admin_model::check_admin(array('KUPCI_I_PARTNERI_AZURIRANJE')))
							<!-- @if($realizovano != 1 and $stornirano != 1)
			                <a href="#" id="JSKupacDetail" class="btn btn-primary btn-small tooltipz" aria-label="Izmeni"><i class="fa fa-pencil" aria-hidden="true"></i></a>
			                <a href="{{ AdminOptions::base_url() }}admin/partneri/0" class="btn btn-primary btn-small" target="_blank">Kreiraj kupca</a>
			                @endif -->
			                @endif
			            </div>
			        </div>

			        <?php $kupac = AdminB2BProizvodjac::PartnerPodaci(!is_null(Input::old('partner_id')) ? Input::old('partner_id') : $partner_id); ?>
			        <div id="JSKupacAjaxContent" class="row">
			        	@include('adminb2b/partials/ajax/narudzbina_partner')
			        </div>
			        <br>

			        <div class="row"> 
			        	<div class="columns medium-12 large-12 order-label" style="visibility: {{ ($errors->first() || $web_b2b_narudzbina_id == 0) ? 'hidden' : 'visible' }}">
			        		@if($prihvaceno == 0 and $realizovano == 0 and $stornirano == 0)			@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
			        		<a href="#" class="JS-prihvati btn btn-create">PRIHVATI</a>
			        		<a href="#" class="JS-storniraj btn btn-primary">STORNIRAJ</a>
			        		<a href="#" class="JS-delete btn btn-primary">OBRIŠI</a>
			        		@endif
			        		<p class="order-status-p"> Status narudzbine: Nova</p>

			        		@elseif($prihvaceno != 0 and $realizovano == 0 and $stornirano == 0)
			        		@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
			        		<a href="#" class="JS-realizuj btn btn-create">REALIZUJ</a>
			        		<a href="#" class="JS-storniraj btn btn-primary">STORNIRAJ</a>
			        		@endif
			        		<label for="">Poslato</label>
			        		<input id="poslato" name="posta_slanje_poslato" type="checkbox" {{ $posta_slanje_poslato == 1 ? 'checked' : '' }} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
			        		<p class="order-status-p">Status narudžbine: Prihvaćena</p>
			        		@elseif($realizovano != 0 and $stornirano == 0)
			        		@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
			        		<a href="#" class="JS-storniraj btn btn-primary">STORNIRAJ</a>	
			        		@endif							
			        		<p class="order-status-p">Status narudžbine: Realizovana</p>
			        		@elseif($stornirano != 0)
			        		<p class="order-status-p">Status narudžbine: Stornirana</p>
			        		@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
			        		<a href="#" class="JS-nestorniraj btn btn-primary">VRATI IZ STORNIRANIH</a>
			        		@endif
			        		@endif
			        	</div>
			        </div>
			    </div>

			    <div class="columns medium-6">
			    	<div class="row"> 
			    		<div class="field-group columns medium-6">
			    			<label for="">Broj dokumenta</label>
			    			<input name="broj_dokumenta" type="text" class="{{ $errors->first('broj_dokumenta') ? 'error' : '' }}" value="{{ !is_null(Input::old('broj_dokumenta')) ? Input::old('broj_dokumenta') : $broj_dokumenta }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'readonly' : '' }}>
			    		</div>
			    		<div class="field-group columns medium-6">
			    			<label for="">Datum</label>
			    			<input id="order-date" class="datum-val has-tooltip" name="datum_dokumenta" type="text" value="{{ Input::old('datum_dokumenta') ? Input::old('datum_dokumenta') : $datum_dokumenta }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
			    		</div>
			    	</div>

			    	<div class="row"> 
			    		<div class="field-group columns medium-6">
			    			<label for="">Način plaćanja</label>
			    			<select name="web_nacin_placanja_id" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
			    				{{ AdminPartneri::nacinPlacanja(!is_null(Input::old('web_nacin_placanja_id')) ? Input::old('web_nacin_placanja_id') : $web_nacin_placanja_id) }}
			    			</select>
			    		</div>
			    		<div class="field-group columns medium-6">
			    			<label for="">Način isporuke</label>
			    			<select name="web_nacin_isporuke_id" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
			    				{{ AdminPartneri::nacinIsporuke(!is_null(Input::old('web_nacin_isporuke_id')) ? Input::old('web_nacin_isporuke_id') : $web_nacin_isporuke_id) }}
			    			</select>
			    		</div>
			    	</div>

			    	<div class="row"> 
			    		<div class="field-group columns medium-6">
			    			<label for="">Kurirska služba</label>
			    			<select name="posta_slanje_id" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
			    				{{ AdminPartneri::kurirskaSluzba(!is_null(Input::old('posta_slanje_id')) ? Input::old('posta_slanje_id') : $posta_slanje_id) }}
			    			</select>
			    		</div>
			    		<div class="field-group columns medium-6">
			    			<label for="">Dodeli status</label>
			    			<select  name="narudzbina_status_id" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>
			    				{{ AdminPartneri::statusnarudzbine(!is_null(Input::old('narudzbina_status_id')) ? Input::old('narudzbina_status_id') : $narudzbina_status_id) }}
			    			</select>
			    		</div>
			    	</div>

			    	<div class="row"> 	
			    		<div class="field-group columns medium-6">
			    			<label for="">IP adresa</label>
			    			<input name="ip_adresa" type="text" value="{{ Input::old('ip_adresa') ? Input::old('ip_adresa') : $ip_adresa }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'readonly' : '' }}>
			    		</div>	
			    		<div class="field-group columns medium-6">
			    			<label for="">Broj pošiljke</label>
			    			<input name="posta_slanje_broj_posiljke" type="text" class="{{ $errors->first('posta_slanje_broj_posiljke') ? 'error' : '' }}" value="{{ !is_null(Input::old('posta_slanje_broj_posiljke')) ? Input::old('posta_slanje_broj_posiljke') : $posta_slanje_broj_posiljke }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'readonly' : '' }}>
			    		</div>
			    	</div>
			    	@if(!is_null($komercijalista))
			    	<div class="row"> 
			    		<div class="field-group columns medium-6">
			    			<label for="">Komercijalista</label>
			    			<input type="text" value="{{ $komercijalista->ime }} {{ $komercijalista->prezime }}" disabled>
			    		</div>
			    	</div>
			    	@endif

			    	<div class="row"> 	
			    		<div class="field-group columns medium-12">
			    			<label for="">Napomena</label>
			    			<textarea name="napomena" rows="2" >{{ !is_null(Input::old('napomena')) ? Input::old('napomena') : $napomena }}</textarea>
			    		</div>
			    		<div class="field-group columns medium-12">
			    			<label for="">Komentar</label>
			    			<textarea name="komentar" rows="2" >{{ !is_null(Input::old('komentar')) ? Input::old('komentar') : $komentar }}</textarea>
			    		</div>
			    	</div>
			    </div>

			    <div class="columns medium-12 large-12 text-center"> 
			    	<div class="btn-container"> 
			    		@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
			    		<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
			    		@endif

			    		@if(!$errors->first() AND $web_b2b_narudzbina_id != 0)
			    		<a target="_blank" href="/admin/b2b/pdf/{{ $web_b2b_narudzbina_id }}" class="btn btn-primary">PDF</a>

			    		@if($realizovano == 1 and $stornirano == 0)
			    		<a target="_blank" href="/admin/b2b/pdf_racun/{{ $web_b2b_narudzbina_id }}" class="btn btn-primary">Račun</a>
			    		@elseif($realizovano == 0 and $stornirano == 0 and $prihvaceno == 1)
			    		<a target="_blank" href="/admin/b2b/pdf_ponuda/{{ $web_b2b_narudzbina_id }}" class="btn btn-primary">Ponuda</a>
			    		<a target="_blank" href="/admin/b2b/pdf_predracun/{{ $web_b2b_narudzbina_id }}" class="btn btn-primary">Predračun</a>
			    		@elseif($stornirano == 1)
			    		<a target="_blank" href="/admin/b2b/pdf_racun/{{ $web_b2b_narudzbina_id }}" class="btn btn-primary">Račun</a>
			    		@else
			    		<a target="_blank" href="/admin/b2b/pdf_ponuda/{{ $web_b2b_narudzbina_id }}" class="btn btn-primary">Ponuda</a>
			    		<a target="_blank" href="/admin/b2b/pdf_predracun/{{ $web_b2b_narudzbina_id }}" class="btn btn-primary">Predračun</a>
			    		@endif
			    		@endif
			    	</div> 
			    </div>
			</div> 
		</form>
	</div>

	@if(!$errors->first() AND $web_b2b_narudzbina_id != 0)
	<div class="row"> 
		<div class="flat-box">
			<div class="row">

				<div class="columns medium-12 large-12">
					<div class="add-to-order">
						<label> Dodaj artikal narudžbini</label>
					</div>
					<div class="articles relative">
						<input class="" autocomplete="off" data-timer="" type="text" id="JSsearch_porudzbine" data-id="" placeholder="Unesite id ili ime artikla ili prozvođača ili krajnju grupu" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div id="search_content"></div>
					</div>

					<div class="items-order">
						<table class="table table_narudzbina">
							<thead>
								<tr>
									<th class="tab-width-1">ROBA ID</th>
									<th class="tab-width-1">KOLIČINA</th>
									<th class="tab-width-1">BARKOD</th>
									<th class="tab-width-2">NAZIV</th>
									<th class="hide-for-small-only tab-width-1">LAGER</th>
									<th class="tab-width-1" data-coll="cena">CENA</th>
									@if($realizovano != 1 and $stornirano != 1)
									<th class="tab-width-0"></th>
									@endif
								</tr>
							</thead>
							@foreach($narudzbina_stavke as $row)
							<tr>
								<td>
									&nbsp;{{ $row->roba_id}}
								</td>
								<td>
									<?php $max=AdminB2BSupport::lager($row->roba_id)+AdminB2BNarudzbina::kolicina($web_b2b_narudzbina_id,$row->roba_id); ?>
									<input class="kolicina ammount-input" data-stavka-id="{{ $row->web_b2b_narudzbina_stavka_id }}" type="number" min="1" max="{{ $max }}" value="{{ round($row->kolicina) }}" data-old-kolicina="{{ round($row->kolicina) }}" {{($realizovano == 1 or $stornirano == 1) ? 'disabled' : ''}} {{ Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')) == false ? 'disabled' : '' }}>	
								</td>
								<td>
									&nbsp;{{ ltrim(AdminB2BSupport::barkod($row->roba_id)) }}
								</td>
								<td class="narudzbina-td-naziv">
									<span class="inline-block max-width-450 no-white-space">
										{{ AdminSupport::naziv($row->roba_id) }}
									</span>
								</td>
								<td class="JSLager hide-for-small-only">
									{{ AdminB2BSupport::lager($row->roba_id) }}
								</td>	

								@if($realizovano != 1 and $stornirano != 1)
								<td class="cena relative">
									<span class="JSCenaVrednost">
										{{ number_format($row->jm_cena, 2, '.', '') }}
									</span>
									<span class="JSEditCenaBtnSpan">
										<input type="text" data-stavka-id="{{ $row->web_b2b_narudzbina_stavka_id }}" class="JSCenaInput" value="{{ number_format($row->jm_cena, 2, '.', '') }} " >
										@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
										<i class="fa fa-pencil JSEditCenaBtn" aria-hidden="true"></i>
										@endif

									</span>
								</td>
								@else
								<td>
									{{ number_format($row->jm_cena, 2, '.', '') }}
								</td>
								@endif			
								@if(Admin_model::check_admin(array('B2B_NARUDZBINE_AZURIRANJE')))
								@if($realizovano != 1 and $stornirano != 1)
								<td class="tab-width-0">
									<button data-stavka-id="{{ $row->web_b2b_narudzbina_stavka_id }}" class="JSDeleteStavka name-ul__li__remove button-option tooltipz-left" aria-label="Obrši"><i class="fa fa-times" aria-hidden="true"></i></button>
								</td>
								@endif
								@endif
							</tr>
							@endforeach


							<tr>
								<td colspan="4" class="hide-for-small-only"></td><td>Cena artikala: </td>
								<td colspan="3" id="cena-artikla">{{ number_format(floatval($ukupna_cena->cena_sa_rabatom),2,'.','') }}</td> 
							</tr>
							<tr>
								<td colspan="4" class="hide-for-small-only"></td><td>Porez: </td>
								<td colspan="3" id="troskovi-isporuke">{{ number_format(floatval($ukupna_cena->porez_cena),2,'.','') }}</td> 
							</tr>
							<tr>
								<td colspan="4" class="hide-for-small-only"></td><td>Avans <input type="checkbox" id="JSAvans" name="avans" {{ ($avans > 0) ? 'checked' : '' }}>: </td>
								<td colspan="3" id="troskovi-isporuke">{{ number_format(floatval($ukupna_cena->avans),2,'.','') }}</td> 
							</tr>
							<tr>
								<td colspan="4" class="hide-for-small-only"></td><td>Troškovi isporuke: </td>
								<td colspan="3" id="troskovi-isporuke">{{ $troskovi_isporuke }}</td> 
							</tr>
							<tr>
								<td colspan="4" class="hide-for-small-only"><td><b>Ukupno za uplatu: </b></td></td>

								<td colspan="3" id="ukupna-cena"><b> {{ number_format(floatval($ukupna_cena->ukupna_cena+$troskovi_isporuke),2,'.','') }}</b></td>
							</tr>

						</table>  
					</div>
				</div>

			</div> 
		</div>
	</div>
	@endif 
</section> 
@endsection