@extends('adminb2b.defaultlayout')
@section('content')
<section id="main-content" class="pages-page row">

	<div class="medium-3 columns">
		<div class="flat-box">
			<label class="text-center">{{ AdminLanguage::transAdmin('Strane') }}:</label>
			<ul class="page-list lista @if(Admin_model::check_admin(array('STRANICE_AZURIRANJE'))) JSPagesSortable" @endif parent="0">
				@if(Admin_model::check_admin(array('STRANICE_AZURIRANJE')))
				<li class="new-page new-elem relative" id="0">
					<a href="{{AdminOptions::base_url()}}admin/b2b/b2b_stranice/0">{{ AdminLanguage::transAdmin('Nova strana') }}</a>
					@if(Admin_model::check_admin(array('STRANICE_AZURIRANJE')))
					<span class="absolute-right padding-h-8 text-gray"><i class="fa fa-plus" aria-hidden="true"></i></span>
					@endif
				</li>
				@endif
				@foreach(AdminB2BSupport::pages() as $row)
				<li class="ui-state-default @if($row->web_b2b_seo_id == $web_b2b_seo_id) active @endif" id="{{$row->web_b2b_seo_id}}" {{$row->disable == 1 ? 'style="background-color: #ddd"' : ''}}>
					<a href="{{AdminB2BOptions::base_url()}}admin/b2b/b2b_stranice/{{$row->web_b2b_seo_id}}">{{$row->title}}</a>
					<ul class="page-list lista JSPagesSortable lvl-2-ul" parent="{{$row->web_b2b_seo_id}}">
						@foreach(AdminB2BSupport::pages($row->web_b2b_seo_id) as $row2)
						<li class="ui-state-default @if($row2->web_b2b_seo_id == $web_b2b_seo_id) active @endif" id="{{$row2->web_b2b_seo_id}}" {{$row2->disable == 1 ? 'style="background-color: #ddd"' : ''}}>
							<a href="{{AdminB2BOptions::base_url()}}admin/b2b/b2b_stranice/{{$row2->web_b2b_seo_id}}">{{$row2->title}}</a>
							<ul class="page-list lista JSPagesSortable" parent="{{$row2->web_b2b_seo_id}}">
								@foreach(AdminB2BSupport::pages($row2->web_b2b_seo_id) as $row3)
								<li class="ui-state-default @if($row3->web_b2b_seo_id == $web_b2b_seo_id) active @endif" id="{{$row3->web_b2b_seo_id}}" {{$row3->disable == 1 ? 'style="background-color: #ddd"' : ''}}>
									<a href="{{AdminB2BOptions::base_url()}}admin/b2b/b2b_stranice/{{$row3->web_b2b_seo_id}}">{{$row3->title}}</a>
								</li>

								@endforeach
							</ul>
						</li>
						@endforeach
					</ul>
				</li>
				@endforeach
			</ul>
		</div> 
	</div>	
 
	<section class="medium-9 columns"> 
		<form name="form_pages" action="{{AdminOptions::base_url()}}admin/b2b/pages" method="post">
			<input type="hidden" name="jezik_id" value="{{ $jezik_id }}">
			<section {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
				
				<div class="flat-box padding-v-8"> 
					<label class="text-center">{{ AdminLanguage::transAdmin('Sadržaj') }}:</label>
					
					<div class="row">
						<div class="medium-5 columns {{ $errors->first('naslov') ? 'error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Naziv strane') }}:</label>
							<input type="text" id="naziv_stranice" name="naziv_stranice" value="{{Input::old('naziv_stranice') ? Input::old('naziv_stranice') : $naziv}}" {{$disable==1?'disabled="disabled"':''}} onchange="check_fileds('naziv_stranice')" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>

						</div>

						<div class="medium-7 columns">

							<label>{{ AdminLanguage::transAdmin('Prikaži u') }}:</label>
							
							@if(!in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(9)))
							<label class="checkbox-label medium-4 columns">                 
								<input type="checkbox" id="top_menu" name="top_menu" {{ Input::old('top_menu') ? 'checked="checked"' : ($menu_top ? 'checked="checked"' : '') }} {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<span class="label-text">{{ AdminLanguage::transAdmin('Top meniju') }}</span>
							</label>
							@endif

							<label class="checkbox-label medium-4 columns">					
								<input type="checkbox" id="header_menu" name="header_menu" {{ Input::old('header_menu') ? 'checked="checked"' : ($header_menu ? 'checked="checked"' : '') }} {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<span class="label-text">{{ AdminLanguage::transAdmin('Header meniju') }}</span>
							</label>

							<label class="checkbox-label medium-4 columns">	
								<input type="checkbox" id="footer_menu" name="footer_menu" {{ Input::old('footer_menu') ? 'checked="checked"' : ($footer ? 'checked="checked"' : '') }} {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<span class="label-text">{{ AdminLanguage::transAdmin('Footer meniju') }}</span>
							</label>
						</div> 
					</div>
				</div> 


				<div class="flat-box padding-v-8"> 
					<div class="row">

						@if($disable==0 AND AdminOptions::is_shop())
						<div class="medium-2 columns">
							<label>{{ AdminLanguage::transAdmin('Listaj artikle grupe') }}:</label>  

							<select name="grupa_pr_id" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value="-1"></option>
								@foreach(DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2b_prikazi'=>1))->orderBy('redni_broj','asc')->get() as $grupa)
								@if(Input::old('grupa_pr_id') == $grupa->grupa_pr_id)
								<option value="{{ $grupa->grupa_pr_id }}" selected>{{ $grupa->grupa }}</option>
								@else
								@if($grupa_pr_id == $grupa->grupa_pr_id)
								<option value="{{ $grupa->grupa_pr_id }}" selected>{{ $grupa->grupa }}</option>
								@else
								<option value="{{ $grupa->grupa_pr_id }}">{{ $grupa->grupa }}</option>
								@endif
								@endif
								@endforeach
							</select>
						</div>

						<div class="medium-3 columns">
							<label>{{ AdminLanguage::transAdmin('Listaj artikle akcije ili tipova') }}:</label>  
							
							<select name="tip_artikla_id" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value="-1"></option>
								@if(!is_null(Input::old('tip_artikla_id')) AND Input::old('tip_artikla_id') == 0)
								<option value="0" selected>Akcija</option>
								@else
								@if($tip_artikla_id == 0)
								<option value="0" selected>{{ AdminLanguage::transAdmin('Akcija') }}</option>
								@else
								<option value="0">{{ AdminLanguage::transAdmin('Akcija') }}</option>
								@endif
								@endif
								@foreach(AdminSupport::getTipoviB2C(1) as $tip)
								@if(Input::old('tip_artikla_id') == $tip->tip_artikla_id)
								<option value="{{ $tip->tip_artikla_id }}" selected>{{ $tip->naziv }}</option>
								@else
								@if($tip_artikla_id == $tip->tip_artikla_id)
								<option value="{{ $tip->tip_artikla_id }}" selected>{{ $tip->naziv }}</option>
								@else
								<option value="{{ $tip->tip_artikla_id }}">{{ $tip->naziv }}</option>
								@endif
								@endif
								@endforeach
							</select>
						</div>
						@endif

						<div class="medium-2 columns">
							<label>{{ AdminLanguage::transAdmin('Otvori stranicu') }}:</label> 

							<select name="redirect_web_b2b_seo_id" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value=""></option>
								@foreach(DB::table('web_b2b_seo')->orderBy('web_b2b_seo_id','asc')->get() as $stranica)
								@if(Input::old('redirect_web_b2b_seo_id') == $stranica->web_b2b_seo_id)
								<option value="{{ $stranica->web_b2b_seo_id }}" selected>{{ $stranica->title }}</option>
								@else
								@if($redirect_web_b2b_seo_id == $stranica->web_b2b_seo_id)
								<option value="{{ $stranica->web_b2b_seo_id }}" selected>{{ $stranica->title }}</option>
								@else
								<option value="{{ $stranica->web_b2b_seo_id }}">{{ $stranica->title }}</option>
								@endif
								@endif
								@endforeach
							</select>
						</div>

						<div class="medium-3 columns">
							<label>{{ AdminLanguage::transAdmin('Osnovna stranica') }} </label>  

							<select name="parent_id" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value="0">{{ AdminLanguage::transAdmin('Izaberite nad stranicu') }}</option>
								@foreach(AdminB2BSupport::parent_pages(0,$web_b2b_seo_id) as $strana1)
								@if((1 + AdminB2BSupport::page_childs_depth(array($web_b2b_seo_id))) < 3)
								<option value="{{ $strana1->web_b2b_seo_id }}" {{($parent_id == $strana1->web_b2b_seo_id) ? 'selected' : ''}}>{{ $strana1->title }}</option>
								@foreach(AdminB2BSupport::parent_pages($strana1->web_b2b_seo_id,$web_b2b_seo_id) as $strana2)
								@if((2 + AdminB2BSupport::page_childs_depth(array($web_b2b_seo_id))) < 3)
								<option value="{{ $strana2->web_b2b_seo_id }}" {{($parent_id == $strana2->web_b2b_seo_id) ? 'selected' : ''}}>&nbsp;&nbsp;&nbsp;&nbsp; {{ $strana2->title }}</option>
								@endif
								@endforeach
								@endif
								@endforeach
							</select>
						</div>

					</div>
				</div> 


				@if(Admin_model::check_admin(array('STRANICE_AZURIRANJE'))) 
				@if($web_b2b_seo_id != 0 && count($jezici) > 1)
				<div class="languages">
					<ul>	
						@foreach($jezici as $jezik)
						<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/b2b/b2b_stranice/{{ $web_b2b_seo_id }}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
						@endforeach
					</ul>
				</div>
				@endif 
				@endif


				<!-- SEO SETTINGS -->
				<div class="flat-box padding-v-8"> 
					<div class="seo-settings row">

						<div class="medium-12 columns">
							<h3 class="center seo-info tooltipz" aria-label="{{ AdminLanguage::transAdmin('SEO (Search Engine Optimization) polja je potrebno popuniti jednostavnim jezikom, kako bi Vas kupci lakše pronašli na internetu - ovi opisi nisu vidljivi direktno u prodavnici, već samo kako bi Google prepoznao Vaš proizvod i povezao Vas sa ljudima koji ga traže') }}.">{{ AdminLanguage::transAdmin('SEO') }}</h3>
						</div>

						<div class="medium-6 columns field-group {{ $errors->first('seo_title') ? 'error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Naslov (title) do 60 karaktera') }}:</label>
							<input id="seo-title" type="text" value="{{Input::old('seo_title') ? Input::old('seo_title') : $seo_title}}"  name="seo_title" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="medium-6 columns field-group {{ $errors->first('keywords') ? 'error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Ključne reči (keywords)') }}</label>
							<input id="seo-keywords" type="text" value="{{Input::old('keywords') ? Input::old('keywords') : $keywords}}" name="keywords" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div>

						<div class="medium-12 columns {{ $errors->first('description') ? 'error' : '' }}">
							<label>{{ AdminLanguage::transAdmin('Opis (description) do 158 karaktera') }}:</label>
							<input id="seo-description" type="text" value="{{Input::old('description') ? Input::old('description') : $desription}}" name="description" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'readonly' : '' }}>
						</div> 

					</div>
				</div> 
			</section>

			<input type="hidden" name="web_b2b_seo_id" value="{{$web_b2b_seo_id}}" />
			<input type="hidden" name="status" value="{{$status}}" />
			<input type="hidden" name="flag_page" id="flag_page" value="{{$flag_page}}" />
			<input type="hidden" name="flag_b2b_show" id="flag_b2b_show" value="{{$flag_b2b}}" />

			
			@if(Admin_model::check_admin(array('STRANICE_AZURIRANJE')))
			<div class="btn-container center">
				<a href="#" class="page-edit-save btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</a>
			</div>
			@endif
			
		</form>
		@if($web_b2b_seo_id != 0)
		<a href="{{ AdminB2BOptions::base_url() }}b2b/{{AdminB2BSupport::page_slug($naziv_stranice,$jezik_id)->slug }}" target="_blank" class="btn btn-primary" value="{{$web_b2b_seo_id}}">{{ AdminLanguage::transAdmin('Vidi stranicu') }}</a>
		@endif

		@if($disable==0 AND Admin_model::check_admin(array('STRANICE_AZURIRANJE')) AND $naziv != 'pocetna')
		<form class="pages-form" method="post" action="{{AdminOptions::base_url()}}admin/b2b/delete_page">
			<input type="hidden" name="web_b2b_seo_id" value="{{$web_b2b_seo_id}}" />				
			<input type="submit" value="Obriši stranicu" class="btn btn-danger">
		</form>
		@endif


		@if($web_b2b_seo_id != 0 && $disable == 0)
			@include('adminb2b.partials.b2b_sekcija_stranice')
		@endif

		<!-- ====================== -->

		<div class="text-center">
			<a href="#" class="video-manual" data-reveal-id="pages-manual">{{ AdminLanguage::transAdmin('Uputstvo') }} <i class="fa fa-film"></i></a>
			<div id="pages-manual" class="reveal-modal medium" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
				<div class="video-manual-container"> 
					<p><span class="video-manual-title">{{ AdminLanguage::transAdmin('Stranice') }}</span></p>
					<iframe src="https://player.vimeo.com/video/271253202" width="840" height="426" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				</div>
				<a class="close-reveal-modal" aria-label="Close">&#215;</a>
			</div>
		</div>
		<!-- ====================== -->
	</section>
</section>

@endsection
