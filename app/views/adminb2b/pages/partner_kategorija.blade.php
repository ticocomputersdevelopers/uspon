@extends('adminb2b.defaultlayout')
@section('content')
<div id="main-content" >
@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
@endif
	@include('adminb2b.partials.partner_rabat_tabs')
	
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">Izaberi kategoriju</h3>
				<select class="JSeditSupport search-select">
					<option value="{{ AdminB2BOptions::base_url() }}admin/b2b/partner_kategorija">Dodaj novu</option>
					@foreach(AdminB2BSupport::getKategorije() as $row)
						<option value="{{ AdminB2BOptions::base_url() }}admin/b2b/partner_kategorija/{{ $row->id_kategorije }}" @if($row->id_kategorije == $id_kategorije) {{ 'selected' }} @endif>{{ $row->naziv }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<section class="small-12 medium-12 large-5 large-centered columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1>
				<!-- <h1 id="info"></h1> -->			
				<form method="POST" action="{{ AdminB2BOptions::base_url() }}admin/b2b/partner_kategorija-edit" enctype="multipart/form-data">
					  <input type="hidden" name="id_kategorije" value="{{ $id_kategorije }}"> 

						<div class="row">
							<div class="columns medium-6 field-group{{ $errors->first('naziv') ? ' error' : '' }}">
								<label for="naziv">Naziv partner kategorije</label>
								<input type="text" name="naziv" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')) == false ? 'readonly' : '' }}> 
							</div>
						 
							<div class="columns medium-6 field-group{{ $errors->first('rabat') ? ' error' : '' }}">
								<label for="rabat">Rabat</label>
								<input type="number" name="rabat" value="{{ Input::old('rabat') ? Input::old('rabat') : $rabat }}" {{ Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')) == false ? 'readonly' : '' }}> 
							</div>

						</div>

						<div class="row">
							<div class="columns medium-12 field-group">
								<input name="active" type="checkbox" @if($active == 1 or $id_kategorije==0) checked @endif {{ Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')) == false ? 'disabled' : '' }}> Aktivan
							</div>
						</div>

						@if(Admin_model::check_admin(array('B2B_RABAT_AZURIRANJE')))
							<div class="row">
								<div class="btn-container center">
									<button type="submit" class="btn btn-primary save-it-btn">Sačuvaj</button>
									@if($id_kategorije != null)
										<a id="JSDeletePartnerKategorije" class="btn btn-primary btn-danger" data-link="{{ AdminB2BOptions::base_url() }}admin/b2b/partner_kategorija-delete/{{ $id_kategorije }}">Obriši
										</a>
									@endif
								</div>
							</div>
						@endif
				 </form>
				</div>
		    </section>
		 </div>  
	  </div> <!-- end of .row -->

	  @endsection
 