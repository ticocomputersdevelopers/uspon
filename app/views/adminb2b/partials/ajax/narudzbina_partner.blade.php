
<div class="field-group columns medium-12 large-6">
    <label for="">Adresa</label>
    <input name="adresa" type="text" value="{{ $kupac->adresa }}" disabled {{ Admin_model::check_admin(array('B2B Narudžbine ažuriranje')) == false ? 'readonly' : '' }}>
</div>
<div class="field-group columns medium-12 large-6">
    <label for="">Mesto</label> 
    <input name="mesto" type="text" value="{{ $kupac->mesto }}" disabled {{ Admin_model::check_admin(array('B2B Narudžbine ažuriranje')) == false ? 'readonly' : '' }}>
</div>
<div class="field-group columns medium-12 large-6">
    <label for="">Telefon</label>
    <input name="telefon" type="text" value="{{ $kupac->telefon }}" disabled {{ Admin_model::check_admin(array('B2B Narudžbine ažuriranje')) == false ? 'readonly' : '' }}>
</div>
