<div class="flat-box">     
	<div class="row"> 

		<div class="columns medium-3"> 
			@if(Admin_model::check_admin(array('STRANICE_AZURIRANJE')))
			<div style="max-width: calc(100% - 6px);" class="margin-v-auto margin-h-10"> 
				
				<label class="text-center margin-h-10">{{ AdminLanguage::transAdmin('Dodaj sekciju na strani') }}</label>

				<select id="JSB2BPageSectionsAdd" data-web_b2b_seo_id="{{ $web_b2b_seo_id }}">
					<option value="0"></option>
					@foreach($sekcije as $sekcija)
					<option value="{{ $sekcija->b2b_sekcija_stranice_id }}">{{ $sekcija->naziv }}</option>
					@endforeach
				</select>
			</div>
			@endif

			<ul class="page-list @if(Admin_model::check_admin(array('STRANICE_AZURIRANJE'))) JSPageSectionsSortable" @endif data-web_b2b_seo_id="{{ $web_b2b_seo_id }}">

				@foreach($b2b_stranica_sekcije as $stranica_sekcija)
				<li class="ui-state-default relative" data-b2b_sekcija_stranice_id="{{ $stranica_sekcija->b2b_sekcija_stranice_id }}">
					<span>{{ $stranica_sekcija->naziv }}</span>

					@if(Admin_model::check_admin(array('STRANICE_AZURIRANJE')))
					<span class="absolute-right text-gray padding-h-8 JSbtn-delete tooltipz" aria-label="{{ AdminLanguage::transAdmin('Obriši') }}" data-link="{{AdminOptions::base_url()}}admin/b2b/page-section-remove/{{ $web_b2b_seo_id }}/{{$stranica_sekcija->b2b_sekcija_stranice_id}}"><i class="fa fa-times" aria-hidden="true"></i></span>
					@endif
				</li>
				@endforeach
			</ul>  
		</div>
	</div>
</div>


<div class="flat-box" id="page_id"> 
	
	<div class="text-center padding-v-8">
		<button class="btn btn-secondary JStoggle_btn" data-section_id="1">{{ AdminLanguage::transAdmin('Kreiraj sekciju') }}</button>
	</div>

	<div class="row JStoggle_content" data-section_id="1">  
		<div class="medium-3 columns"> 

			<label class="text-center">{{ AdminLanguage::transAdmin('Sekcije') }}</label>

			<ul @if(Admin_model::check_admin(array('STRANICE_AZURIRANJE'))) class="banner-list" @endif data-table="2">
				@if(Admin_model::check_admin(array('STRANICE_AZURIRANJE')))
				<li id="0" class="new-banner new-elem relative">
					<a href="{{AdminOptions::base_url()}}admin/b2b/b2b_stranice/{{ $web_b2b_seo_id }}/{{ $jezik_id }}/0#page_id">{{ AdminLanguage::transAdmin('Nova sekcija') }}</a>
					<span class="absolute-right padding-h-8 text-gray"><i class="fa fa-plus" aria-hidden="true"></i></span>
				</li>
				@endif

				@foreach($sections as $section)
				<li class="ui-state-default relative @if($section->b2b_sekcija_stranice_id == $page_section->b2b_sekcija_stranice_id) active @endif" id="{{$section->b2b_sekcija_stranice_id}}">
					<a href="{{AdminOptions::base_url()}}admin/b2b/b2b_stranice/{{ $web_b2b_seo_id }}/{{ $jezik_id }}/{{$section->b2b_sekcija_stranice_id}}#page_id">{{ $section->naziv }}</a>
					@if(Admin_model::check_admin(array('STRANICE_AZURIRANJE')) AND ($section->b2b_sekcija_stranice_id != $page_section->b2b_sekcija_stranice_id))
					<a class="absolute-right padding-h-8 tooltipz JSbtn-delete" aria-label="Obriši" data-link="{{AdminOptions::base_url()}}admin/b2b/page-section-delete/{{$section->b2b_sekcija_stranice_id}}"><i class="fa fa-times  text-gray" aria-hidden="true"></i></a>
					@endif
				</li>
				@endforeach
			</ul>   
		</div>


		<div class="medium-9 columns no-padd"> 
			<form action="{{AdminOptions::base_url()}}admin/b2b/page-section-edit" method="post" accept-charset="utf-8" enctype="multipart/form-data">

				<label class="text-center">{{ AdminLanguage::transAdmin('Sekcija') }}</label>

				<div class="row">

					<div class="medium-3 columns">
						<label>{{ AdminLanguage::transAdmin('Tip') }}</label>

						<select name="b2b_sekcija_stranice_tip_id" id="JSB2BSekcijaStraniceTip" data-b2b_sekcija_stranice_id="{{ $page_section->b2b_sekcija_stranice_id }}" data-web_b2b_seo_id="{{ $web_b2b_seo_id }}" data-jezik_id="{{ $jezik_id }}" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
							@foreach(AdminB2BSupport::sectionTypes() as $section_type)
							<option value="{{$section_type->b2b_sekcija_stranice_tip_id}}" 
							{{ ($page_section_type->b2b_sekcija_stranice_tip_id == intval($section_type->b2b_sekcija_stranice_tip_id) ? 
							'selected' :  '') }}
							>{{ ($section_type->naziv == 'Slajder') ? 'Slajder / Baneri' : $section_type->naziv }}
							</option>
						@endforeach
						</select>
					</div>

					<div class="medium-3 columns">
						<label>{{ AdminLanguage::transAdmin('Naziv') }}</label>
						<input id="naziv" value="{{ Input::old('naziv') ? Input::old('naziv') : trim($page_section->naziv) }}" type="text" name="naziv" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'readonly' : '' }}>
						<div class="error">{{ $errors->first('naziv') ? $errors->first('naziv') : '' }}</div>
					</div>

					@if($page_section_type->naziv == 'Slajder')

					<div class="medium-3 columns">
						<label>{{ AdminLanguage::transAdmin('Slajder') }}</label>

						<div class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('Slajder napravite u delu *Baneri i slajderi*') }}"> 
							<select name="b2b_slajder_id" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value=""></option>
								@foreach(AdminB2BSupport::sliders() as $b2b_slider)
								<option value="{{$b2b_slider->b2b_slajder_id}}" 
								{{ ($b2b_slider->b2b_slajder_id == intval(Input::old('b2b_slajder_id')) ? 
								'selected' : 
								((is_null(Input::old('b2b_slajder_id')) AND 
								($b2b_slider->b2b_slajder_id == $page_section->b2b_slajder_id)) ? 'selected' : '')) }}
								>{{ $b2b_slider->naziv }}</option>
								@endforeach
							</select>
						</div>

						<div class="error">{{ $errors->first('b2b_slajder_id') ? $errors->first('b2b_slajder_id') : '' }}</div>
					</div>
					@endif

					@if($page_section_type->naziv == 'Lista artikala')
					<div class="medium-3 columns">
						<label>{{ AdminLanguage::transAdmin('Lista artikala') }}</label>

						<div class="tooltipz" aria-label="{{ AdminLanguage::transAdmin('Tip napraviti u delu *Šifarnici -> Tipovi*') }}"> 
							<select name="tip_artikla_id" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
								<option value=""></option>
								<option value="-1" 
								{{ (-1 == intval(Input::old('tip_artikla_id')) ? 
								'selected' : 
								((is_null(Input::old('tip_artikla_id')) AND 
								(-1 == $page_section->tip_artikla_id)) ? 'selected' : '')) }}
								>{{ AdminLanguage::transAdmin('Najnoviji') }}</option>
								<option value="-2" 
								{{ (-2 == intval(Input::old('tip_artikla_id')) ? 
								'selected' : 
								((is_null(Input::old('tip_artikla_id')) AND 
								(-2 == $page_section->tip_artikla_id)) ? 'selected' : '')) }}
								>{{ AdminLanguage::transAdmin('Najpopularniji') }}</option>
								<option value="-3" 
								{{ (-3 == intval(Input::old('tip_artikla_id')) ? 
								'selected' : 
								((is_null(Input::old('tip_artikla_id')) AND 
								(-3 == $page_section->tip_artikla_id)) ? 'selected' : '')) }}
								>{{ AdminLanguage::transAdmin('Najprodavaniji') }}</option>
								<option value="0" 
								{{ ('0' == strval(Input::old('tip_artikla_id')) ? 
								'selected' : 
								((is_null(Input::old('tip_artikla_id')) AND 
								('0' == strval($page_section->tip_artikla_id))) ? 'selected' : '')) }}
								>Akcija</option>
								@foreach(AdminB2BSupport::articleTypes() as $tip)
								<option value="{{$tip->tip_artikla_id}}" 
								{{ ($tip->tip_artikla_id == intval(Input::old('tip_artikla_id')) ? 
								'selected' : 
								((is_null(Input::old('tip_artikla_id')) AND 
								($tip->tip_artikla_id == $page_section->tip_artikla_id)) ? 'selected' : '')) }}
								>{{ $tip->naziv }}</option>
								@endforeach
							</select>
						</div>

						<div class="error">{{ $errors->first('tip_artikla_id') ? $errors->first('tip_artikla_id') : '' }}</div>
					</div>
					@endif
				</div>

				<div class="row">
					<div class="medium-3 columns">
						<label>{{ AdminLanguage::transAdmin('Aktivna') }}</label>
						<select name="flag_aktivan" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
							<option value="1">{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" {{ (Input::old('flag_aktivan') == '0') ? 'selected' : (($page_section->flag_aktivan == 0) ? 'selected' : '') }}>{{ AdminLanguage::transAdmin('NE') }}
							</option>
						</select>
					</div>

					<div class="medium-3 columns">
						<label>{{ AdminLanguage::transAdmin('Puna širina') }}</label>
						<select name="puna_sirina" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
							<option value="1">{{ AdminLanguage::transAdmin('DA') }}</option>
							<option value="0" {{ (Input::old('puna_sirina') == '0') ? 'selected' : (($page_section->puna_sirina == 0) ? 'selected' : '') }}>{{ AdminLanguage::transAdmin('NE') }}
							</option>
						</select>
					</div>

					<div class="medium-3 columns chooseColor">
						<label>{{ AdminLanguage::transAdmin('Boja pozadine') }}</label>
						<input type="color" name="boja_pozadine" value="{{ (Input::old('boja_pozadine') == '#000000') ? Input::old('boja_pozadine') : $page_section->boja_pozadine }}" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'disabled' : '' }}>
					</div>
				</div> 

				@if(in_array($page_section_type->naziv,['Text']))

					@if($page_section->b2b_sekcija_stranice_id != 0 && count($jezici) > 1)
					<br>

					<div class="row">
						<div class="columns"> 
							<div class="languages">
								<ul>	
									@foreach($jezici as $jezik)
									<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/b2b/b2b_stranice/{{ $web_b2b_seo_id }}/{{ $jezik->jezik_id }}/{{ $page_section->b2b_sekcija_stranice_id }}/{{ $page_section_type->b2b_sekcija_stranice_tip_id }}#page_id">{{ $jezik->naziv }}</a></li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					@endif 

					<br>

					<div class="row">
						<div class="columns"> 
							<div id="JSFooterSectionContent">
								<label>{{ AdminLanguage::transAdmin('Sadržaj') }}</label>
								<textarea @if(Admin_model::check_admin(array('STRANICE_AZURIRANJE'))) class="special-textareas" @endif name="sadrzaj" {{ Admin_model::check_admin(array('STRANICE_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('sadrzaj') ? Input::old('sadrzaj') : trim($page_section_lang->sadrzaj) }}</textarea>
							</div> 
						</div>
					</div>
				@endif

				<br>

				<input type="hidden" name="b2b_sekcija_stranice_id" value="{{ $page_section->b2b_sekcija_stranice_id }}" />
				<input type="hidden" name="jezik_id" value="{{$jezik_id}}" />

				@if(Admin_model::check_admin(array('STRANICE_AZURIRANJE')))
				<div class="btn-container center">
					<button class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
					@if($page_section->b2b_sekcija_stranice_id > 0)
					<!-- <a href="{{AdminOptions::base_url()}}admin/page-section-delete/{{$page_section->b2b_sekcija_stranice_id}}" class="btn btn-danger">{{ AdminLanguage::transAdmin('Obriši') }}</a> -->
					@endif
				</div>
				@endif 

			</form>
 
		</div>
	</div>		 
</div>