<header id="admin-header" class="text-center">
	
	<!-- WIDTH TOGGLE BUTTON -->
	<div class="header-width-toggle"><i class="fa fa-chevron-left" aria-hidden="true"></i></div>

	<div class="logo-wrapper">
	    <a class="logo" href="{{ AdminB2BOptions::base_url()}}admin" title="Selltico">
			<img src="{{ AdminB2BOptions::base_url()}}/images/admin/logo-selltico-white.png" alt="Selltico">
		</a>
	</div> 
	 
	<nav class="main-menu">
		<div class="logout-wrapper">
			<div>
				<a href="{{ AdminB2BOptions::base_url()}}admin/b2b/login-to-portal" target="_blank" class="menu-item__link">
					<div class="menu-item__icon">
						<img class="shop-logo" src="{{ AdminB2BOptions::base_url()}}images/admin/wbp-logo.png" alt="{{Options::company_name()}}" />
						<span class="menu-tooltip">VIDI B2B PORTAL</span>
					</div>
					<div class="menu-item__text">VIDI B2B PORTAL</div>
				</a>
			</div>
		</div>

		<ul>
			<li class="logged-user text-white">Ulogovan: {{ AdminB2BOptions::get_admin() }}</li>

			@if(in_array(AdminB2BOptions::web_options(130),array(1,2)))
			<li>
				<a href="{{ AdminB2BOptions::base_url()}}admin{{ AdminOptions::web_options(130) == 2 ? '/stranice/0' : '' }}" class="menu-item__link">
					<div class="menu-item__icon">
						B2C
						<span class="menu-tooltip">B2C Admin</span>
					</div>
					<div class="menu-item__text">B2C Admin</div>
				</a>
			</li>
			@endif	

			@if(AdminB2BOptions::check_admin(array('B2B_NARUDZBINE')) && AdminB2BOptions::check_admin(array('B2B_NARUDZBINE_PREGLED')))
			<li>
				<a href="{{ AdminB2BOptions::base_url()}}admin/b2b/narudzbine/0/0/0/0/0" class="menu-item__link  @if($strana == 'b2b_narudzbine') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-ticket" aria-hidden="true"></i>
						<span class="notify-num-small">{{ B2bBasket::getNew() }}</span>
						<span class="menu-tooltip">Narudžbine</span>	
					</div>
					<div class="menu-item__text">Narudžbine
						<span class="notify-num">{{ B2bBasket::getNew() }}</span>
					</div>
				</a>
			</li>
			@endif

			@if((AdminB2BOptions::web_options(130) == 2 OR (AdminB2BOptions::web_options(130) == 1 AND !AdminOptions::is_shop())) && Admin_model::check_admin(array('ARTIKLI')) && Admin_model::check_admin(array('ARTIKLI_PREGLED')))
			<li>
				<a href="{{ AdminB2BOptions::base_url()}}admin/artikli/0/0/0/0/0/0/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-nn-1-nn-nn-nn-nn-nn-nn/0/0/nn-nn" class="menu-item__link  @if($strana == 'artikli') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cubes" aria-hidden="true"></i>
						<span class="menu-tooltip">Artikli</span>
					</div>
					<div class="menu-item__text">Artikli</div>
				</a>
			</li>
			@endif

			@if((AdminB2BOptions::web_options(130) == 2 OR !AdminOptions::is_shop()) OR AdminB2BOptions::web_options(130) == 1 AND Admin_model::check_admin(array('B2B_PARTNERI')) AND Admin_model::check_admin(array('B2B_PARTNERI_PREGLED')))
			<li>
				<a href="{{ AdminB2BOptions::base_url()}}admin/b2b/kupci_partneri/partneri" class="menu-item__link @if(in_array($strana,array('kupci','partneri'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-users" aria-hidden="true"></i>
						<span class="menu-tooltip">Partneri</span>
					</div>
					<div class="menu-item__text">Partneri</div>
				</a>
			</li>
			@endif

			@if((AdminB2BOptions::web_options(130) == 2 OR !AdminOptions::is_shop()) OR AdminB2BOptions::web_options(130) == 1 AND Admin_model::check_admin(array('B2B_PARTNERI')) AND Admin_model::check_admin(array('B2B_PARTNERI_PREGLED')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/web_import/0/0/0/nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn-nn/0/nn-nn" class="menu-item__link  @if($strana == 'web_import') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cloud-upload" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Web import') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Web import') }}</div>
				</a>
			</li>
			@endif

			@if(AdminB2BOptions::check_admin(array('B2B_ANALITIKA')))
			<li>
				<a href="{{ AdminB2BOptions::base_url()}}admin/b2b/analitika" class="menu-item__link  @if($strana == 'b2b_analitika') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-line-chart" aria-hidden="true"></i>
						<span class="menu-tooltip">Analitika</span>
					</div>
					<div class="menu-item__text">Analitika</div>
				</a>
			</li>
			@endif

			@if(AdminB2BOptions::check_admin(array('B2B_STRANICE')) && AdminB2BOptions::check_admin(array('B2B_STRANICE_PREGLED')))
			<li>
				<a href="{{ AdminB2BOptions::base_url()}}admin/b2b/b2b_stranice/0" class="menu-item__link  @if($strana == 'b2b_stranice') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-file-text" aria-hidden="true"></i> 
						<span class="menu-tooltip">Stranice</span>
					</div>
					<div class="menu-item__text">Stranice</div>
				</a>
			</li>
			@endif

			@if(AdminB2BOptions::check_admin(array('B2B_BANERI_I_SLAJDER')) && AdminB2BOptions::check_admin(array('B2B_BANERI_I_SLAJDER_PREGLED')))
			<li>
				<a href="{{ AdminB2BOptions::base_url()}}admin/b2b/slider/0" class="menu-item__link  @if($strana == 'b2b_baneri_slajderi') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-picture-o" aria-hidden="true"></i>
						<span class="menu-tooltip">Baneri i slajder</span>
					</div>
					<div class="menu-item__text">Baneri i slajder</div>
				</a>
			</li>
			@endif

			@if(AdminB2BOptions::check_admin(array('B2B_VESTI')) && AdminB2BOptions::check_admin(array('B2B_VESTI_PREGLED')))
			<li>
				<a href="{{ AdminB2BOptions::base_url()}}admin/b2b/vesti" class="menu-item__link  @if($strana == 'b2b_vesti') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-newspaper-o" aria-hidden="true"></i>
						<span class="menu-tooltip">Vesti</span>
					</div>
					<div class="menu-item__text">Vesti</div>
				</a>
			</li>
			@endif

			@if((AdminB2BOptions::web_options(130) == 2 OR (AdminB2BOptions::web_options(130) == 1 AND !AdminOptions::is_shop())) AND Admin_model::check_admin(array('SIFARNICI')) && Admin_model::check_admin(array('SIFARNICI_PREGLED')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/grupe/0" class="menu-item__link  @if(in_array($strana,array('grupe','proizvodjac','mesto','tip_artikla','jedinica_mere','poreske_stope','stanje_artikla','status_narudzbine','kurirska_sluzba','konfigurator','osobine','nacin_placanja','nacin_isporuke','troskovi_isporuke','kurs'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-industry" aria-hidden="true"></i>
						<span class="menu-tooltip">Šifarnici</span>
					</div>
					<div class="menu-item__text">Šifarnici</div>
				</a>
			</li>
			@endif
			
			@if(Admin_model::check_admin(array('B2B_RABAT')) AND Admin_model::check_admin(array('B2B_RABAT_PREGLED')) AND ( Session::get('b2c_admin'.AdminOptions::server()) == 942 OR Session::get('b2c_admin'.AdminOptions::server()) == 1) )
			<li>
				<a href="{{ AdminB2BOptions::base_url()}}admin/b2b/access-list-manufacturer/0" class="menu-item__link" >
					<div class="menu-item__icon">
						<i class="fa fa-handshake-o" aria-hidden="true"></i>
						<span class="menu-tooltip">Nivo pristupa</span>
					</div>
					<div class="menu-item__text">Nivo pristupa</div>
				</a>
			</li>
			@endif
			
			@if(Admin_model::check_admin(array('B2B_RABAT')) AND Admin_model::check_admin(array('B2B_RABAT_PREGLED')))
			<li>
				<a href="{{ AdminB2BOptions::base_url()}}admin/b2b/article-list/0" class="menu-item__link  @if(in_array($strana,array('b2b_artikli','proizvodjaci','rabat_partnera','rabat_kombinacije','partner_kategorija'))) active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cubes" aria-hidden="true"></i>
						<span class="menu-tooltip">Rabat</span>
					</div>
					<div class="menu-item__text">Rabat</div>
				</a>
			</li>
			@endif

			@if(AdminB2BOptions::check_admin(array(10033)) AND (AdminB2BOptions::info_sys('excel') OR AdminB2BOptions::info_sys('xml') OR AdminB2BOptions::info_sys('wings') OR AdminB2BOptions::info_sys('calculus') OR AdminB2BOptions::info_sys('infograf') OR AdminB2BOptions::info_sys('logik') OR AdminB2BOptions::info_sys('roaming') OR AdminB2BOptions::info_sys('sbcs') OR AdminB2BOptions::info_sys('skala') OR AdminB2BOptions::info_sys('gsm') OR AdminB2BOptions::info_sys('promobi') OR AdminB2BOptions::info_sys('softcom') OR AdminB2BOptions::info_sys('panteon') OR AdminB2BOptions::info_sys('nssport')))
			<li>
				<a href="{{ AdminB2BOptions::base_url()}}admin/b2b/is" class="menu-item__link  @if($strana == 'informacioni_sistem') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cloud-upload" aria-hidden="true"></i>
						<span class="menu-tooltip">Informacioni sistem</span>
					</div>
					<div class="menu-item__text">Informacioni sistem</div>
				</a>
			</li>
			@endif

			@if(Admin_model::check_admin(array('DOKUMENTI','DOKUMENTI_PREGLED')))
			<li>
				<a href="{{ AdminOptions::base_url()}}admin/dokumenti" class="menu-item__link">
					<div class="menu-item__icon">
						<i class="fa fa-file-text" aria-hidden="true"></i>
						<span class="menu-tooltip">{{ AdminLanguage::transAdmin('Dokumenti') }}</span>
					</div>
					<div class="menu-item__text">{{ AdminLanguage::transAdmin('Dokumenti') }}</div>
				</a>
			</li>
			@endif		

			@if(AdminB2BOptions::check_admin(array('B2B_PODESAVANJA')))
			<li>
				<a href="{{ AdminB2BOptions::base_url()}}admin/b2b/podesavanja" class="menu-item__link  @if($strana == 'b2b_podesavanja') active @endif">
					<div class="menu-item__icon">
						<i class="fa fa-cogs" aria-hidden="true"></i>
						<span class="menu-tooltip">Podešavanja</span>
					</div>
					<div class="menu-item__text">Podešavanja</div>
				</a>
			</li>
			@endif
		</ul>
	</nav>
	
	<div class="logout-wrapper"> 
		<a href="{{ AdminB2BOptions::base_url()}}admin-logout" class="menu-item__link">
			<div class="menu-item__icon">
				<i class="fa fa-sign-out" aria-hidden="true"></i>
				<span class="menu-tooltip">Odjavi se</span>
			</div>
			<div class="menu-item__text">Odjavi se</div>
		</a> 
		<p class="footnote">TiCo &copy; {{ date('Y') }} - All rights reserved</p>
	</div>
</header>

