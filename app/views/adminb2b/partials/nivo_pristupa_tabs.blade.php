<div class="m-tabs clearfix">
	<div class="m-tabs__tab{{ $strana=='nivo_pristupa_proizvodjaci' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminB2BOptions::base_url()}}admin/b2b/access-list-manufacturer/0">{{AdminLanguage::transAdmin('Proizvođači')}}</a></div>

	<div class="m-tabs__tab{{ $strana=='nivo_pristupa_grupe' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminB2BOptions::base_url()}}admin/b2b/access-list-group-article/0/0">{{AdminLanguage::transAdmin('Grupe')}}</a></div>

	<div class="m-tabs__tab{{ $strana=='nivo_pristupa_lager' ? ' m-tabs__tab--active' : '' }}"><a href="{{ AdminB2BOptions::base_url()}}admin/b2b/access-list-warehouse/0">{{AdminLanguage::transAdmin('Magacin')}}</a></div>


</div>