<section class="shopping-information">
    <h2><span class="heading-background">Završena kupovina</span></h2>

    <table>
        <tbody>
        <th colspan="2">Informacije o nardžbini:</th>
        <tr>
            <td>Broj porudžbine:</td>
            <td>{{$order->broj_dokumenta}}</td>
        </tr>
        <tr>
            <td>Datum porudžbine:</td>
            <td>{{date('d.m.Y',strtotime($order->datum_dokumenta))}}</td>
        </tr>
        <tr>
            <td>Način isporuke:</td>
            <td>{{B2bBasket::getNameNacinIsporuke($order->web_nacin_isporuke_id)}}</td>
        </tr>
        <tr>
            <td>Način plaćanja:</td>
            <td>{{B2bBasket::getNameNacinPlacanja($order->web_nacin_placanja_id)}}</td>
        </tr>
        <tr>
            <td>Napomena:</td>
            <td>{{$order->napomena}}</td>
        </tr>

        </tbody> 
        <tbody>
        <th colspan="2">Informacije o kupcu:</th>
        <tr>
            <td>Naziv:</td>
            <td>{{$partner->naziv}}</td>
        </tr>

        <tr>
            <td>PIB:</td>
            <td>{{$partner->pib}}</td>
        </tr>
        <tr>
            <td>Matični broj:</td>
            <td>{{$partner->broj_maticni}}</td>
        </tr>
        <tr>
            <td>Adresa:</td>
            <td>{{$partner->adresa}}</td>
        </tr>
        <tr>
            <td>Mesto:</td>
            <td>{{$partner->mesto}}</td>
        </tr>
        <tr>
            <td>Telefon:</td>
            <td>{{$partner->telefon}}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>{{$partner->mail}}</td>
        </tr>
        </tbody>


        <tbody>
        <th colspan="2">Informacije o prodavcu:</th>
        <tr>
            <td>Naziv prodavca:</td>
            <td>{{B2bOptions::company_name()}}</td>
        </tr>
        <tr>
            <td>Adresa:</td>
            <td>{{B2bOptions::company_adress()}}</td>
        </tr>
        <tr>
            <td>Telefon:</td>
            <td>{{B2bOptions::company_phone()}}</td>
        </tr>
        <tr>
            <td>Fax:</td>
            <td>{{B2bOptions::company_fax()}}</td>
        </tr>
        <tr>
            <td>PIB:</td>
            <td>{{B2bOptions::company_pib()}}</td>
        </tr>
        <tr>
            <td>Šifra delatnosti:</td>
            <td>{{B2bOptions::company_delatnost_sifra()}}</td>
        </tr>
        <tr>
            <td>Žiro račun:</td>
            <td>{{B2bOptions::company_ziro()}}</td>
        </tr>
        <tr>
            <td>E-mail:</td>
            <td>{{B2bOptions::company_email()}}</td>
        </tr>
        </tbody>


    </table>
    <?php
    $basicPrice = B2bBasket::b2bOrderBasicPrice($order->web_b2b_narudzbina_id);
    $tax = B2bBasket::b2bOrderTaxPrice($order->web_b2b_narudzbina_id);
    $total = $basicPrice + $tax;
    ?>

    <table>
        <tbody>
        <th colspan="7">Proizvodi:</th>
        <tr>
            <td>Naziv </td>
            <td>Cena </td>
            <td>Rabat </td>
            <td>Cena sa rab. </td>
            <td>PDV </td>
            <td>Količina </td>
            <td>Ukupno </td>
        </tr>

        @foreach($orderItems as $row)
            <?php
            $rab_p = B2bArticle::b2bPartnerRabatProduct($row->roba_id, $partner->partner_id);
            $priceDiscount = B2bArticle::procenat_m($row->jm_cena,$rab_p);
            $tax = B2bArticle::b2bTax($row->tarifna_grupa_id);
            $totalItem = B2bArticle::procenat_p($priceDiscount, $tax)*$row->kolicina;
            ?>
            <tr>
                <td>{{B2bArticle::getName($row->roba_id)}} </td>
                <td>{{number_format($row->jm_cena,2)}} </td>
                <td>{{number_format($rab_p,2)}} % </td>
                <td>{{number_format($priceDiscount,2)}} </td>
                <td>{{number_format($tax,2)}} %</td>
                <td>{{(int)$row->kolicina }} </td>
                <td>{{number_format($totalItem,2)}} </td>
            </tr>

        @endforeach

        <tr>
            <th colspan="3"></th>
            <th colspan="2">Osnovica:</th>
            <th colspan="2">{{B2bBasket::cena($basicPrice)}}</th>
        </tr>
        <tr>
            <th colspan="3"></th>
            <th colspan="2">PDV:</th>
            <th colspan="2">{{B2bBasket::cena($tax)}}</th>
        </tr>
        <tr>
            <th colspan="3"></th>
            <th colspan="2">Ukupno:</th>
            <th colspan="2">{{B2bBasket::cena($total)}}</th>
        </tr>



        </tbody>
    </table>
</section>