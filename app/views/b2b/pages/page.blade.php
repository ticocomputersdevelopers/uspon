@extends('b2b.templates.main')
@section('content') 	
	<!-- page.blade -->
	<div class="row"> 
	    <div class="col-md-12 col-sm-12 col-xs-12">
	    	<br> 
	        {{ $web_content}}
	    </div>
	</div> 		 
	<!-- page.blade end-->  
	
	<input type="hidden" id="base_url" value="{{B2bOptions::base_url()}}" />
@endsection 