@extends('b2b.templates.main')
@section('content')  

<!-- user_card.blade -->

<div class="row">
    <div class="col-xs-12"> 
        
        <br>

        <h2><span class="section-title">Vaša Kartica</span></h2>

        <div class="row">

            <div class="col-md-3 col-sm-3 col-xs-6 no-padding"> 
                <span>Filtriraj: </span>
                <label>Datum od:</label>
                <input type="text" value="{{ !is_null($date_start) ? $date_start : '' }}" id="date_start">
            </div>

            <div class="col-md-3 col-sm-3 col-xs-6">
                <label>Datum do:</label>
                <input type="text" id="date_end" value="{{ !is_null($date_end) ? $date_end : '' }}">
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="lineh"><label>&nbsp;</label></div>
                 
                <button class="button" id="JSFilterDate">Potvrdi</button>
                
                <a href="{{B2bOptions::base_url()}}b2b/user/card" class="button" role="button">Poništi filtere</a>
               
                @if(B2bOptions::info_sys('calculus') || B2bOptions::info_sys('infograf') || B2bOptions::info_sys('logik'))
                <form method="POST" action="{{B2bOptions::base_url()}}b2b/user/card-refresh">
                    <button onClick="this.form.submit(); this.disabled=true;">Osveži karticu</button>
                </form>
                @endif
            </div>  
            
        </div>

        <br>

        <div class="table-responsive user-card-table"> 
            <table class="table table-condensed table-striped table-bordered table-hover"> 
                <thead> 
                    <tr> 
                        <th>Vrsta dokumenta</th>
                        <th>Datum dokumenta</th> 
                        <th>Datum dospeća</th>
                        <th>Duguje</th>
                        <th>Potražuje</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody> 
                    @foreach($cart_items as $item)
                    <tr> 
                        <td>{{$item->vrsta_dokumenta .' - '. $item->opis}}</td>
                        <td>{{date("d-m-Y",strtotime($item->datum_dokumenta))}}</td>
                        <td>{{date("d-m-Y",strtotime($item->datum_dospeca))}}</td>
                        <td>{{number_format($item->duguje,2)}}</td>
                        <td>{{number_format($item->potrazuje,2)}}</td>
                        <td class="text-center JSPartnerCartItems pointer" data_kartica_id="{{ $item->web_b2b_kartica_id }}"><i class="fa fa-search-plus"></i></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="row"> 
            <!-- CSS FROM CART.BLADE -->
            <div class="cart-summary clearfix col-md-4 col-sm-6 col-xs-12 pull-right">
                <div>Ukupno dugovanje : <span class="pull-right">{{number_format($sumDuguje,2)}}</span></div>
                <div>Ukupno potraživanje :  <span class="pull-right">{{number_format($sumPotrazuje,2)}}</span></div>
                <div>Saldo :  <span class="pull-right">{{number_format($saldo,2)}}</span></div>
            </div>
        </div>

        <div class="row">
            {{ $cart_items->links() }}
        </div> 


    <!-- Modal -->
        <div id="JSPartnerCartItemsModal" class="modal fade user-card-modal">
            <div class="modal-dialog modal-lg">
 
                <div class="modal-content"> 
                    <div class="modal-body" id="JSPartnerCartItems"></div>
                    
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal">Zatvori</button>
                    </div>
                </div> 
            </div>
        </div>  

    </div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="{{Options::domain() }}js/bootstrap.min.js"></script>
<script>
    $(function() {
        $( "#date_start, #date_end").datepicker({
            dateFormat: 'yy-mm-dd'
        });
    });

    $('#date_start, #date_end').keydown(false);

    $('#JSFilterDate').click(function(){
        var datum_start = $('#date_start').val();
        var datum_end = $('#date_end').val();
        if(datum_start == ''){
            datum_start = 'null';
        }
        if(datum_end == ''){
            datum_end = 'null';               
        }
        location.href = "{{ B2bOptions::base_url() }}b2b/user/card/"+datum_start+"/"+datum_end;
    });

    $('.JSPartnerCartItems').click(function(){
        var karica_id = $(this).attr('data_kartica_id');

        $.ajax({
            type: "POST",
            url: base_url+'b2b/user/partner-cart-items',
            data: {karica_id:karica_id},
            success: function(response) {

                $('#JSPartnerCartItems').html(response);
            }
        });    
        $('#JSPartnerCartItemsModal').modal('show');
    });
</script>

<!-- user_card.blade end -->

@endsection