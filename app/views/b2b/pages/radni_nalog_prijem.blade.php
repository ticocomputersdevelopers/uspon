@extends('b2b.templates.main')

@section('content')
<section id="main-content">
	<div class="row">
        <div class="col-xs-12 complaints">
			<form class="" method="POST" action="{{RmaOptions::base_url()}}b2b/radni-nalog-prijem-post"> 
        		<h2 class="section-title"> {{ 'Novi tiket' }} </h2>
				<div class="row"> 
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label for="">Uređaj</label>
						<div>
							<input name="uredjaj" class="{{ $errors->first('uredjaj') ? 'error' : '' }}" type="text" value="{{ Input::old('uredjaj') ? Input::old('uredjaj') : $radni_nalog->uredjaj }}" autocomplete="off">
							<div class="error red-dot-error">{{ $errors->first('uredjaj') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('uredjaj') : "" }}</div>
						</div>
					<!-- 	<select name="roba_id" id="JSRoba" class="{{ $errors->first('roba_id') ? 'error' : '' }}">
							{{ RMA::uredjajiSelect(array(Input::old('roba_id') ? Input::old('roba_id') : $radni_nalog->roba_id),false) }}
						</select> -->
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12">
						<label for="">Grupa</label>
						<select name="roba" class="{{ $errors->first('roba') ? 'error' : '' }}">
							<option value="1">Roba</option>
							<option value="0" {{ (Input::old('roba')=='0') ? 'selected' : '' }}>Usluga</option>
						</select>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12"> 
						<label for="">Serijski broj</label>
						<div>
							<input name="serijski_broj" class="{{ $errors->first('serijski_broj') ? 'error' : '' }}" type="text" value="{{ Input::old('serijski_broj') ? Input::old('serijski_broj') : $radni_nalog->serijski_broj }}" autocomplete="off"><div class="error red-dot-error">{{ $errors->first('serijski_broj') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('serijski_broj') : "" }}</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12"> 
						<label for="">Broj računa</label>
						<div>
							<input name="broj_fiskalnog_racuna" class="{{ $errors->first('broj_fiskalnog_racuna') ? 'error' : '' }}" type="text" value="{{ Input::old('broj_fiskalnog_racuna') ? Input::old('broj_fiskalnog_racuna') : $radni_nalog->broj_fiskalnog_racuna }}" autocomplete="off"><div class="error red-dot-error">{{ $errors->first('broj_fiskalnog_racuna') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('broj_fiskalnog_racuna') : "" }}</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12"> 
						<label for="">Proizvođač</label>
						<div>
							<input name="proizvodjac" class="{{ $errors->first('proizvodjac') ? 'error' : '' }}" type="text" value="{{ Input::old('proizvodjac') ? Input::old('proizvodjac') : $radni_nalog->proizvodjac }}" autocomplete="off"><div class="error red-dot-error">{{ $errors->first('proizvodjac') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('proizvodjac') : "" }}</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label for="">Hitno</label>
						<select name="hitno" class="{{ $errors->first('hitno') ? 'error' : '' }}">
							<option value="0">Ne</option>
							<option value="1" {{ (Input::old('hitno')==1) ? 'selected' : '' }}>Da</option>
						</select>
					</div> 

					<div class="col-md-6 col-sm-6 col-xs-12">
						<label for="">Opis kvara</label>
						<div> <textarea name="opis_kvara" rows="2" class="{{ $errors->first('opis_kvara') ? 'error' : '' }}">{{ Input::old('opis_kvara') ? Input::old('opis_kvara') : $radni_nalog->opis_kvara }}</textarea></div> 
						<div class="error red-dot-error">{{ $errors->first('opis_kvara') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('opis_kvara') : "" }}</div>
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12">
						<label for="">Napomena</label>
						<div> <textarea name="napomena" rows="2" class="{{ $errors->first('napomena') ? 'error' : '' }}">{{ !is_null(Input::old('napomena')) ? Input::old('napomena') : $radni_nalog->napomena }}</textarea> </div> 
						<div class="error red-dot-error">{{ $errors->first('napomena') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('napomena') : "" }}</div>
					</div>

				<input type="hidden" name="check_serijski_broj" value="{{ !is_null(Input::old('check_serijski_broj')) ? Input::old('check_serijski_broj') : 1 }}">
				<input type="hidden" name="radni_nalog_id" value="{{ $radni_nalog->radni_nalog_id }}">

				<div class="col-xs-12"> 
					<button type="submit" class="button">Pošalji</button>
				</div>
				
				@if(Session::get('message'))
				<div class="ticket-title text-right" style="padding: 4px;">{{ Language::trans('Vaši podaci su uspešno poslati') }}!</div>
				@endif

				</div>
			</form>
		</div>	
	</div>
</section>
@endsection