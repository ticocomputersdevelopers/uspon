@extends('b2b.templates.main')
@section('content')

<!-- cart.blade -->

<div class="row">
    <div class="col-xs-12"> 

        <br>
        
        <h2><span class="section-title">Vaša Korpa</span></h2>

        <br>
        
        @if(Session::has('b2b_cart') and B2bBasket::b2bCountItems() > 0)
        <div class="cart-labels table-responsive">
            <table class="table table-condensed table-striped"> 
                <thead> 
                    <tr> 
                        <th colspan="2">Proizvod</th>
                        <th>Cena</th>
                        <th>Rabat</th>
                        <th>Cena sa rab.</th>
                        <th>PDV %</th>
                        <th>Količina </th> 
                        <th>Ukupno</th>
                        <th>Ukloni</th> 
                    </tr>
                </thead>

                <tbody> 
                    @foreach($items as $row)
                    <tr>
                        <td class="cart-image">
                            <img class="img-responsive" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ ($row->roba_id) }}">
                        </td>

                        <td>
                            <a class="xs-wrap inline-block text-left" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::slugify(B2bArticle::seo_title($row->roba_id))}}">
                                {{B2bArticle::short_title($row->roba_id)}}
                            </a>
                        </td>

                        <?php
                        $rabatCene = B2bArticle::b2bRabatCene($row->roba_id);
                        $totalItem = $rabatCene->ukupna_cena*$row->kolicina;
                        $lager = B2bArticle::lagerObj($row->roba_id);
                        $quantity = $lager->kolicina - $lager->rezervisano;
                        ?>
                        <td>{{B2bBasket::cena($rabatCene->osnovna_cena)}}</td>
                        <td>{{number_format($rabatCene->ukupan_rabat,2)}}%</td>
                        <td>{{B2bBasket::cena($rabatCene->cena_sa_rabatom)}}</td>
                        <td>{{number_format($rabatCene->porez,2)}}%</td>
                      
                        <td>
                            <div class="inline-block quantity-change"> 
                                <a class="cart-less" data-roba-id="{{ $row->roba_id }}" href="javascript:void(0)"><i class="fas fa-minus"></i></a>
                                <input class="add-amount JScart-amount" id="quantity-{{ $row->roba_id }}" data-roba-id="{{ $row->roba_id }}" data-old-kol="{{ (int)$row->kolicina }}" id="{{ $row->web_b2b_korpa_stavka_id }}" data-max-kol="{{ $quantity }}" type="text" value="{{(int)$row->kolicina}}">
                                <a class="cart-more" data-roba-id="{{ $row->roba_id }}" data-max-kol="{{ $quantity }}" href="javascript:void(0)"><i class="fas fa-plus"></i></a> 
                            </div>
                        </td>
                      
                        <td> {{B2bBasket::cena($totalItem)}}</td>
                      
                        <td class="cart-remove">
                            <a href="javascript:deleteCartItem({{ $row->web_b2b_korpa_stavka_id }})" title="Ukloni artikal" class="close"><i class="fas fa-times"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <button class="cart-remove-all button">Isprazni korpu</button>
        <br><br>
        <!-- PAYMENT METHOD -->
        <form class="payment-method" id="JSSubmitForm" action="{{ route('b2b.check_out') }}" method="POST">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <label>Mesto isporuke:</label>
                    <select id="adresa_isporuke" name="adresa_isporuke" class="JSCartLocation">
                        {{ B2bCommon::getPartnerLocation() }}
                    </select>
                </div>
                
                <div class="col-md-4 col-sm-6 col-xs-12 pull-right">
                    <?php $cartTotal = B2bBasket::total($avans ? 2.00 : 0.00);
                    $limit= DB::table('partner')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->pluck('limit_p');  ?>
                     @if($limit > 1 && $limit < $cartTotal->cena_sa_rabatom)
                        <span class="red-dot-error help-block">Narudžbinu nije moguće proslediti.
                            <br>Prekoračili ste limit. Za dodatne informacije kontaktirajte svog komercijalistu. </span>
                    @endif
                    <div class="cart-summary"> 
                        <div class="clearfix">Osnovica: <span class="pull-right JSosnovica" data-osnovica="{{ $cartTotal->cena_sa_rabatom }}"> {{ B2bBasket::cena($cartTotal->cena_sa_rabatom) }} </span></div> 
                        <div class="clearfix">PDV: <span class="JSPdv pull-right" data-pdv="{{ $cartTotal->porez_cena }}"> {{ B2bBasket::cena($cartTotal->porez_cena) }} </span></div>
                        @if($avans)
                        <div class="clearfix">Avans: <span class="JSAvans pull-right" data-avans="{{ $cartTotal->avans }}"> - {{ B2bBasket::cena($cartTotal->avans) }} </span></div>
                        @endif

                        <?php $troskovi = B2bBasket::troskovi(); ?>
                        @if($nacin_isporuke_id != 2 AND $troskovi > 0)
                        <div class="clearfix">Troškovi isporuke: <span class="pull-right JStroskovi" data-troskovi="{{ $troskovi }}"> {{ B2bBasket::cena($troskovi) }} </span></div>
                        @endif

                        <div class="clearfix">Ukupno: <span class="pull-right JSUkupno" data-ukupno="{{ ($cartTotal->ukupna_cena) }}"> {{ B2bBasket::cena($cartTotal->ukupna_cena+($nacin_isporuke_id != 2 ? $troskovi : 0)) }} </span></div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 text-right" hidden>
                    <label for="adresa_isporuke">Avans rabat 2%</label>
                    <input type="checkbox" name="avans_check" {{ $avans ? 'checked' : '' }}> 
                </div>
            </div>
            <div class="row"> <br>
                <div class="form-group col-md-4 col-sm-4 col-xs-12 hidden">
                    <label for="nacin_isporuke">Način isporuke:</label>
                    <select id="nacin_isporuke" name="nacin_isporuke" class="form-control">
                        {{B2bBasket::nacin_isporuke($nacin_isporuke_id)}}
                    </select>
                </div>

                <div class="form-group col-md-4 col-sm-4 col-xs-12 hidden">
                    <label for="nacin_placanja">Način plaćanja:</label>
                    <select id="nacin_placanja" name="nacin_placanja" class="form-control">
                        {{B2bBasket::nacin_placanja()}}
                    </select>
                </div>

                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                    <label for="napomena">Napomena:</label>
                    <textarea class="form-control" name="napomena" id="napomena"></textarea>
                </div>
                

                <!-- <div class="col-md-12 col-sm-12 col-xs-12 text-right"> 
                    @if($limit > 1 && $limit < $cartTotal->cena_sa_rabatom)
                        <button type="button" class="button" onclick="limitExceededJS();">Prosledi porudžbinu</button>
                    @else
                        <button type="button" class="submit-order-button button">Prosledi porudžbinu</button>
                    @endif
                </div>  -->
             
                @if($cartTotal->ukupna_cena >= 14400)
                    @if($limit > 1 && $limit < $cartTotal->cena_sa_rabatom)
                            <button type="button" class="button" onclick="limitExceededJS();">Prosledi porudžbinu</button>
                        @else
                            <button type="submit" class="submit-order-button button">Prosledi porudžbinu</button>
                        @endif
                    @else
                    <button type="button" class="custom-order-button button">Prosledi porudžbinu</button>
                @endif


                <div class="text-right custom_modal"> 
                    <div class="close-modal">
                        <i class="fas fa-times"></i>
                    </div>
                    <h4 class="text-bold text-left">Troškovi dostave</h4>
                    <h5 class="text-left" style="padding: 2rem 0;">Iznos Vaše narudžbine je manji od 12000 RSD + PDV, za besplatnu isporuku Vam nedostaje <span id="JSdifference"> </span> RSD + PDV. Troškovi isporuke su 179 RSD + PDV po paketu.</h5>
                    <!-- <a href="javascript:void(0)"> test </a> -->

                    <div class="text-right">
                    <a class="btn-default btn" href="<?php $_SERVER['PHP_SELF']; ?>">Nastavite kupovinu</a>
            
                       @if($limit > 1 && $limit < $cartTotal->cena_sa_rabatom)
                            <button type="button" class="button" style="float: right" onclick="limitExceededJS();">Prosledi porudžbinu</button>
                        @else
                            <button type="submit" class="submit-order-button btn btn-success">Prosledi porudžbinu</button>
                        @endif
                   </div>
                   
                </div>
            </div>
        </form>
        @else
        <p class="empty-page">Trenutno nemate artikle u korpi.</p>
        @endif

    </div>
</div>

<script>
    $(document).ready(function(){
        $('.cart-less').click(function(){
            var roba_id = $(this).data('roba-id');
            var quantity = $('#quantity-'+roba_id);
            if(quantity.val()==1){
                $('.info-popup').fadeIn().delay(1200).fadeOut();
                $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
                quantity.val(quantity.data('old-kol'));
            }
            else {
                $.ajax({
                    type: "POST",
                    url:'{{route('b2b.cart_add')}}',
                    cache: false,
                    data:{roba_id:roba_id, status:3, quantity:1},
                    success:function(res){
                        location.reload();
                    }
                });
            }
        });

        $('.cart-more').click(function(){
            var roba_id = $(this).data('roba-id');
            var max = $(this).data('max-kol');
            var quantity = $('#quantity-'+roba_id);
            if(quantity.val()==max){
                $('.info-popup').fadeIn().delay(1200).fadeOut();
                $('.info-popup .popup-inner').html("<p class='p-info'>Poručili ste veću količinu od raspoložive. Kontaktirajte svog Komercijalistu.</p>");
                quantity.val(quantity.data('old-kol'));
            }
            else {
                $.ajax({
                    type: "POST",
                    url:'{{route('b2b.cart_add')}}',
                    cache: false,
                    data:{roba_id:roba_id, status:2, quantity:1},
                    success:function(res){
                        location.reload();
                    }
                });
            }
        });

        $('.JScart-amount').change(function(){
            var roba_id = $(this).data('roba-id');
            var old_kol = $(this).data('old-kol');
            var max = $(this).data('max-kol');
            var quantity = $(this).val();
            if(quantity < 1){
                $('.info-popup').fadeIn().delay(1200).fadeOut();
                $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
                $(this).val(old_kol);
            }
            else if(quantity > max){
                $('.info-popup').fadeIn().delay(1200).fadeOut();
                $('.info-popup .popup-inner').html("<p class='p-info'>Poručili ste veću količinu od raspoložive. Kontaktirajte svog Komercijalistu.</p>");
                $(this).val(old_kol);
            }
            else {
                $.ajax({
                    type: "POST",
                    url:'{{route('b2b.cart_add')}}',
                    cache: false,
                    data:{roba_id:roba_id, status:1, quantity:quantity},
                    success:function(res){
                        location.reload();
                    }
                });
            }
        });

        $('.cart-remove-all').click(function() {
            $.ajax({
                type: "POST",
                url:'{{route('b2b.cart_delete_all')}}',
                cache: false,
                success:function(){
                    location.reload();
                }
            });
        });

        $('#nacin_isporuke').on('change', function(){
            var params = getUrlVars();
            params['nacin_isporuke_id'] = $(this).val();
            window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
        });

        $('input[name="avans_check"]').click(function(){
            var params = getUrlVars();
            if($(this).is(':checked')){
                params['avans'] = 'true';
            }else{
                delete params['avans'];
            }
            window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
        });
    });

    // MODAL INFO PDV AND OSNOVICA
    (function(){
        var JSosnovica = +$('.JSosnovica').attr('data-osnovica');
        var difference = parseInt(14400 - JSosnovica*1.2);
        $('#JSdifference').text(difference);  
        
            $('.custom-order-button').on('click', function() {
                $('.custom_modal').show();
                $('body').addClass('body-after')
            });

            $('.close-modal i').on('click', function() {
                $('.custom_modal').hide();
            })
        
    }());

    function deleteCartItem(cart_id){
        $.ajax({
            type: "POST",
            url:'{{route('b2b.cart_delete')}}',
            cache: false,
            data:{cart_id:cart_id},
            success:function(res){
                location.reload();
            }
        });
    }

    function limitExceededJS(){
        $('.info-popup').fadeIn().delay(2000).fadeOut();
         $('.info-popup .popup-inner').html("<p class='p-info'>Prekoračili ste limit. Za dodatne informacije kontaktirajte svog komercijalistu.</p>");
    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }



</script>

<!-- cart.blade end -->

@endsection