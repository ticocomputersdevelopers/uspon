@extends('b2b.templates.login')
@section('content')
 
<div class="wrapper">
   <div class="wrapper-inner">
        <div class="row flex"> 
            <div class="col-md-5 col-sm-5 col-xs-12 text-center"> 
                <a href="{{ B2bOptions::base_url()}}" title="{{ B2bOptions::company_name()}}">
                    <img class="logo" src="{{ B2bOptions::base_url()}}{{ B2bOptions::company_logo()}}" alt="{{ B2bOptions::company_name()}}">
                </a>
            </div>  

            <div class="col-md-7 col-sm-7 col-xs-12">  
                <form action="{{route('b2b.forgot_password_post')}}" method="post" class="bg-fff"> 
                    <br>
                    <div class="box-gap"> 
                        <label for="mail" class="label">Email</label>
                        <input id="mail" name="mail" type="text" class="form-control" value="{{ Input::old('mail') }}">
                        <div class="error">{{$errors->first('mail')}}</div>

                        <button type="submit" class="sign-in-btn">Potvrdi</button>

                        <div class="text-right"> 
                            Nazad na 
                            <a class="new-psw-btn" href="login">prijavu</a> /
                            <a class="new-psw-btn" href="{{route('b2b.registration')}}">registraciju</a>  
                        </div> 
                    </div>   
                    <!--   <a class="submit btn became-partner" href="login">Otkaži</a>
                    <a class="submit btn became-partner" href="{{route('b2b.registration')}}" >Postanite partner</a>  -->
                </form>  
            </div> 
        </div>

        <div class="text-right"> 
            <a class="logo-selltico" href="https://www.selltico.com/" target="_blank"> 
                B2B ENGINE BY: 
                <img src="{{ Options::base_url()}}images/logo-selltico.png">    
            </a>
        </div>  
    </div>
</div>

<script type="text/javascript">
    @if(Session::has('forgot_message'))
    bootbox.alert({
        message: "{{ Session::get('forgot_message') }}"
    });
    @endif
</script>
@endsection