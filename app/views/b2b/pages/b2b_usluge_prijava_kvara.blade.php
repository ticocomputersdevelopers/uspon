@extends('b2b.templates.main')
@section('content') 

<!-- RMA.blade -->
<h2><span class="page-title-with-desc text-bold center-block text-center"><i class="fas fa-desktop"></i> {{ Language::trans('Obrazac za prijavu kvara') }}</span></h2> 
<div class="text-center page-desc">{{ Language::trans('Ukoliko imate problem sa nekim uređajem i želite da prijavite kvar. Popunite obrazac i mi ćemo organizovati preuzimanje istog od Vas.') }}</div>

<div class="login-title text-center">
	@if(Input::old('flag_vrsta_kupca') == 1)
		<span class="inline-block pointer active-user company-user">{{ Language::trans('pravno lice') }}</span>
		<span class="inline-block hidden pointer private-user">{{ Language::trans('fizičko lice') }}</span>
	@else
		<span class="inline-block pointer active-user company-user">{{ Language::trans('pravno lice') }}</span>
		<span class="inline-block hidden pointer private-user">{{ Language::trans('fizičko lice') }}</span>
	@endif
</div> 
<?php 
  $partner_id=Session::get('b2b_user_'.B2bOptions::server());
  $partner = DB::table('partner')->where('partner_id',$partner_id)->first();

 ?>

<form action="{{ Options::base_url() }}b2b/prijava-kvara" method="post" class="rma-form margin-auto" autocomplete="off"> 
	<div class="row">
		
		<div class="col-xs-12">
			<h3>{{ Language::trans('Podaci o kupcu') }}</h3>
		</div>

		<input type="hidden" name="flag_vrsta_kupca" value="1"> 

		<div class="col-xs-6">
			<label for="ime">{{ Language::trans('Ime') }}</label>
			<input id="ime" name="ime" type="text" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" >
			<div class="error red-dot-error">{{ $errors->first('ime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('ime') : "" }}
			</div>
		</div>

		<div class="col-xs-6">
			<label for="prezime">{{ Language::trans('Prezime') }}</label>
			<input id="prezime" name="prezime" type="text" value="{{ Input::old('prezime') ? Input::old('prezime') : '' }}" >
			<div class="error red-dot-error">{{ $errors->first('prezime') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('prezime') : "" }}</div>
		</div>

		<div class="col-xs-6">
			<label for="naziv">{{ Language::trans('Naziv firme') }}</label>
			<input id="naziv" name="naziv" type="text" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : trim($partner->naziv) ) }}" >
			<div class="error red-dot-error">{{ $errors->first('naziv') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('naziv') : "" }}
			</div>
		</div>

		<div class="col-xs-6">
			<label for="pib">{{ Language::trans('PIB') }}</label>
			<input id="pib" name="pib" type="text" value="{{ Input::old('pib') ? Input::old('pib') : trim($partner->pib) }}" >
			<div class="error red-dot-error">{{ $errors->first('pib') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('pib') : "" }}</div>
		</div>	

		<div class="col-xs-6">
			<label for="maticni_br">{{ Language::trans('Matični broj') }}</label>
			<input id="maticni_br" name="maticni_br" type="text" value="{{ Input::old('maticni_br') ? Input::old('maticni_br') : trim($partner->broj_maticni) }}" >
			<div class="error red-dot-error">{{ $errors->first('maticni_br') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('maticni_br') : "" }}</div>
		</div>	  

		<div class="col-xs-6">
			<label for="email">{{ Language::trans('E-mail') }}</label>
			<input id="email" autocomplete="off" name="email" type="text" value="{{ Input::old('email') ? Input::old('email') : trim($partner->mail) }}" >
			<div class="error red-dot-error">{{ $errors->first('email') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('email') : "" }}
			</div>
		</div>	

		<div class="col-xs-6">
			<label for="lozinka">{{ Language::trans('Ponovite e-mail adresu') }}</label>
			<input id="email-potvrda" name="email-potvrda" type="text" value="{{ htmlentities(Input::old('email-potvrda') ? Input::old('email-potvrda') : trim($partner->mail)) }}">
			<div class="error red-dot-error">{{ $errors->first('email-potvrda') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('email-potvrda') : "" }}</div>
		</div>

		<div class="col-xs-6">
			<label for="telefon">{{ Language::trans('Telefon') }}</label>
			<input id="telefon" name="telefon" type="text" value="{{ Input::old('telefon') ? Input::old('telefon') : trim($partner->telefon) }}" >
			<div class="error red-dot-error">{{ $errors->first('telefon') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('telefon') : "" }}</div>
		</div>

		<div class="col-xs-6">
			<label for="adresa">{{ Language::trans('Adresa') }}</label>
			<input id="adresa" name="adresa" type="text" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : trim($partner->adresa)) }}" >
			<div class="error red-dot-error">{{ $errors->first('adresa') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('adresa') : "" }}</div>
		</div>

		<div class="col-xs-6">
			<label for="mesto"><span class="red-dot"></span> {{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }} </label>
			<input id="mesto" type="text" name="mesto" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : trim($partner->mesto) ) }}" >
			<div class="error red-dot-error">{{ $errors->first('mesto') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('mesto') : "" }}</div>
		</div> 

		<div class="col-xs-12">
			<h3>{{ Language::trans('Podaci o kvaru') }}</h3>
		</div>
		
		<div class="col-xs-12">
			<label for="proizvodjac"><span class="red-dot"></span> {{ Language::trans('Proizvođač') }} </label>
			<input id="proizvodjac" type="text" name="proizvodjac" value="{{ htmlentities(Input::old('proizvodjac') ? Input::old('proizvodjac') : '') }}" >
			<div class="error red-dot-error">{{ $errors->first('proizvodjac') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('proizvodjac') : "" }}</div>
		</div>

		<div class="col-xs-12">
			<label for="mesto"><span class="red-dot"></span> {{ Language::trans('Model') }} </label>
			<input id="model" type="text" name="model" value="{{ htmlentities(Input::old('model') ? Input::old('model') : '') }}" >
			<div class="error red-dot-error">{{ $errors->first('model') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('model') : "" }}</div>
		</div>

		<div class="col-xs-12">
			<label for="serijski"><span class="red-dot"></span> {{ Language::trans('Serijski broj uređaja (IMEI broj za GSM uređaje)') }} </label>
			<input id="serijski" type="text" name="serijski" value="{{ htmlentities(Input::old('serijski') ? Input::old('serijski') : '') }}" >
			<div class="error red-dot-error">{{ $errors->first('serijski') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('serijski') : "" }}</div>
		</div>

		<div class="col-xs-12">
			<label for="opis"><span class="red-dot"></span> {{ Language::trans('Opis kvara') }} </label>
			<textarea rows="5" tabindex="9" name="opis">{{ htmlentities(Input::old('opis') ? Input::old('opis') : '') }}</textarea>
			<div class="error red-dot-error">{{ $errors->first('opis') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('opis') : "" }}</div>
		</div>

		<div class="col-xs-12">
			<label for="preuzeti"><span class="red-dot"></span> {{ Language::trans('Preuzeti uređaj na adresi korisnika:') }} </label>
			<div class="flex">
				<input type="hidden" name="preuzeti" value="0"> 
				<input id="preuzeti" type="checkbox" > <span>{{ Language::trans('Da') }}</span>
				<div class="error red-dot-error">{{ $errors->first('preuzeti') ? '<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>'.' '. $errors->first('preuzeti') : "" }}</div>
			</div>
			<span>{{ Language::trans('Besplatnu dostavu organizujemo isključivo za uređaje u garanciji, a koji su iz Uspon distribucije.') }}</span>
		</div>

		<div class="capcha text-center col-xs-12"> 
			{{ Captcha::img(5, 160, 50) }}<br>
			<span>{{ Language::trans('Unesite kod sa slike') }}</span>
			<input type="text" name="captcha-string" autocomplete="off">
			<div class="error red-dot-error">{{ $errors->first('captcha') ? $errors->first('captcha') : "" }}</div>
		</div>

		<div class="text-center col-xs-12">   
			<button type="submit" class="button">{{ Language::trans('Pošalji') }}</button>
		</div> 
	</div>
</form>


<script>
@if(Session::get('message'))
	$(document).ready(function(){     
 	
	bootboxDialog({ message: "<p>" + trans('Poslali smo Vam potvrdu o registraciji na e-mail koji ste uneli. Molimo Vas da potvrdite Vašu e-mail adresu klikom na link iz e-mail poruke') + "!</p>", size: 'large', closeButton: true }, 6000); 
 
	});
@endif
</script>

<br>

<!-- RMA.blade -->
 
@endsection