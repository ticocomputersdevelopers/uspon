@foreach($query_products as $row)

<div class="product-content-list">
    <div class="row"> 

        <div class="col-md-3 col-sm-3 col-xs-3 sm-nopadd"> 

            <a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::slugify(B2bArticle::seo_title($row->roba_id))}}" class="product-image-wrapper flex">
                <img class="img-responsive margin-auto" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ B2bArticle::seo_title($row->roba_id) }}" />
            </a>
        </div>

        <div class="col-md-9 col-sm-9 col-xs-9"> 
            <a class="product-title" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::slugify(B2bArticle::seo_title($row->roba_id))}}">
                {{ B2bArticle::short_title($row->roba_id) }}
            </a>

            <div class="addon-title">{{ B2bArticle::addon_title($row->roba_id) }}</div>
          
            <?php
            $rabatCene = B2bArticle::b2bRabatCene($row->roba_id);
            $lager = B2bArticle::lagerObj($row->roba_id);
            $cartAmount = B2bBasket::getB2bQuantityItem($row->roba_id);
            $quantity = $lager->kolicina - ($lager->rezervisano + $cartAmount);
            $razlika = B2bArticle::price_diff($row->roba_id);               

            ?>

            <div class="product-price"> 
                @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                <span> {{B2bBasket::cena($rabatCene->cena_sa_rabatom,2)}} </span>
                <!-- @if($razlika > 0)<i class="price-change fa fa-arrow-up"></i>@elseif($razlika < 0)<i class="price-change fa fa-arrow-down"></i>@endif --> 
                @endif
            </div>
               
                <div class="add-to-cart-container"> 
                    @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                        @if($quantity>0)
                        <div class="inline-block quantity-change"> 
                            <a class="JSProductListCartLess" href="javascript:void(0)"><i class="fas fa-minus"></i></a>
                            <input class="JSProductListCartAmount add-amount" id="quantity-{{$row->roba_id}}" type="text" value="1">
                            <a class="JSProductListCartMore" href="javascript:void(0)"><i class="fas fa-plus"></i></a> 
                        </div> 

                        <button class="button add-to-cart-products" data-max-quantity="{{$quantity}}" data-roba-id="{{$row->roba_id}}"> 
                            U korpu
                        </button>
                        @if(!is_null(B2bPartner::dokumentiUser()))
                        <button class="button add-to-offer JSadd-to-offer-products" data-roba-id="{{$row->roba_id}}">Dodaj u ponudu</button>
                        @endif

                        @else
                        <button class="button not-available">Nije dostupan</button>
                        @endif
                        
                        @else

                        <button class="button">{{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($row->roba_id),'naziv') }}</button>
                    @endif
                </div>

                <div class="quant-container"> 
                    @if(B2bArticle::barkod($row->roba_id)) 
                        <span>
                            EAN: {{ B2bArticle::barkod($row->roba_id) }}
                        </span>
                    @endif

                    @if(null != ( B2bArticle::sifra_is($row->roba_id) ) )
                    
                    <span>Ident:{{ Product::get_id_is($row->roba_id) }}</span>
                    
                    @endif

                    <!-- <span>Lager {{$quantity <= 10 ? $quantity : ($quantity <= 50 ? '> 10' : '> 50')}}</span> --> 
                    <span>Lager {{$quantity <= 10 ? '< 10' : ($quantity <= 50 ? '> 10' : '> 50')}}</span>  

                    <span>Rezerv. {{($lager->rezervisano + $cartAmount )}}</span>
                     
                </div>  
            </div>

            @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
            @if(Session::has('b2c_admin'.B2bOptions::server()))
            <a class="article-edit-btn" target="_blank" href="{{ B2bOptions::base_url() }}admin/product/{{ $row->roba_id }}">IZMENI ARTIKAL</a>
            @endif
            @endif
        </div> 
    </div>
    @endforeach

