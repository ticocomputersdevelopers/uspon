
<!-- <div class="popup JSinfo-popup">
    <div class="popup-wrapper">	
	    <div class="JSpopup-inner">
		 
	    </div>	
	</div>
</div> -->

@if(B2bOptions::popup_banners())
	<div class="popup info-confirm-popup info-popup">
	    <div class="popup-wrapper">
	        <div class="popup-inner"></div>
	    </div> 
	</div>
	 

	<div class="JSfirst-popup first-popup">
		<div class="first-popup-inner relative">  
			 <a href="{{ B2bOptions::popup_banners_link() }}" class="relative inline-block" target="_blank">  
			 	<img class="popup-img" src="{{AdminB2BOptions::base_url()}}{{ B2bOptions::popup_banners() }}">	
			 </a> 
			 <span class='JSclose-me-please'>&times;</span>
	 	</div> 
	</div>
@endif

@foreach(B2bOptions::pozadinska_slika() as $slika)
	<a href="{{ $slika->link }}"  class="JSleft-body-link inline-block"></a> 
	<a href="{{ $slika->link2 }}" class="JSright-body-link inline-block"></a>
@endforeach

