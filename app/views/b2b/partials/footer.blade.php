<div class="container">  
    <footer>
        <div class="row newsletter-foot flex sm-text-center"> 
            <div class="col-md-6 col-sm-6 col-xs-12"> Prijavite se na naš Newsletter </div>
            <div class="col-md-6 col-sm-6 col-xs-12"> 
                <div class="newsletter-input relative">
                    {{-- @if(Options::newsletter()==1) --}}
                    <input type="text" class="" placeholder="Upišite Vašu email adresu" id="newsletter" />
                    <button class="button" onclick="newsletter()"><span class="far fa-envelope"> </span></button>
                    {{-- @endif --}}
                </div>
            </div>  
        </div>
        <div class="row flex foot-row-padd"> 
            <div class="col-md-4 col-sm-4 col-xs-12">     
                <h4>Uspon DOO 
                    @if(Options::company_map() != '' && Options::company_map() != ';') 
                    <span class="JStoggMap inline-block pointer relative">Prikaži mapu <i class="fas fa-map-marker-alt"></i></span>
                    @endif 
                </h4>  
                <ul class="foot-contact"> 
                    <li>
                    </li>
                    <li>
                        {{ Options::company_adress() }}
                    </li>

                    <li>
                        <!-- {{ Options::company_city() }} -->
                        32000 Čačak, Srbija
                    </li>
                    <li>Tel. {{ Options::company_phone() }}</li> 
                    <li class="hidden">{{ Options::company_fax() }}</li> 
                        
                    <li> 
                        <a href="mailto:{{ Options::company_email() }}"> {{ Options::company_email() }} </a>
                    </li> 

                    <li> <a href="{{ B2bOptions::base_url()}}"> www.uspon.rs </a> </li>
                </ul>   
            </div>  

            <div class="col-md-2 col-sm-3 col-xs-12 text-center">
                <br class="visible-xs">
                
                <h4>Pratite nas </h4>
                <div class="social-icons"> 
                    {{Options::social_icon()}}
                </div>
            </div>

            <div class="col-md-6 col-sm-5 col-xs-12"> 
                <h4> &nbsp; </h4>
				<img src="{{ B2bOptions::base_url() }}images/uspon-b2b-logo-footer.png" alt="{{ B2bOptions::company_name() }}">
                <!-- <img class="img-responsive center-block"src="{{ B2bOptions::base_url()}}images/foot-b2b.png"/>  -->
            </div> 

            <div class="col-md-4 col-sm-4 col-xs-12">    
                <!-- <a class="logo" href="{{ B2bOptions::base_url()}}b2b" title="{{B2bOptions::company_name()}}">
                    <img class="img-responsive" src="{{ B2bOptions::base_url()}}{{B2bOptions::company_logo()}}" alt="{{B2bOptions::company_name()}}" />
                </a> -->
                <ul class="footer-lists">
                    @foreach (B2bCommon::menu_footer() as $row)
                    <li>
                        <a class="inline-block" href="{{ B2bOptions::base_url().'b2b/'.$row->naziv_stranice }}">{{ $row->title }}</a>
                    </li>
                    @endforeach
                </ul> 
            </div>
        </div>  
      

        @if(Options::company_map() != '' && Options::company_map() != ';')
        <div class="map-frame JSmap relative">
            
            <div class="map-info">
                <h5>{{ Options::company_name() }}</h5>
                <h6>{{ Options::company_adress() }}, {{ Options::company_city() }}</h6> 
            </div>

            <iframe src="https://maps.google.com/maps?q={{ All::lat_long()[0] }},{{ All::lat_long()[1] }}&output=embed" width="100%" height="100%" frameborder="0" aria-hidden="false" tabindex="0"></iframe> 

        </div> 
        @endif
        

        <div class="text-center foot-note"> 
            <span>{{B2bOptions::company_name() }}  {{ date('Y') }}. Sva prava zadržana <i class="far fa-copyright"></i></span>
            <a href="https://www.selltico.com/" target="_blank">- Izrada B2B portala</a>  
            <a href="https://www.selltico.com/" target="_blank">- Selltico.</a> 
        </div>


        <a class="JSscroll-top" href="javascript:void(0)">
            <i class="fa fa-angle-up"></i>
        </a> 
    </footer>
</div> 

