
    <div class="table-responsive"> 
        <table class="table table-condensed table-striped table-bordered table-hover">
            <thead> 
                <tr> 
                    <th>Rb</th>
                    <th>Šifra</th> 
                    <th>Naziv</th>
                    <th>Količina</th>
                    <th>Vrednost</th> 
                </tr>
            </thead>
            <tbody> 
             
            @foreach($stavke as $stavka)
            <tr> 
                <td class="text-center">{{ $stavka->broj_stavke }}</td>
                <td class="text-center">{{ $stavka->id_is }}</td> 
                <td>{{ $stavka->naziv_web }}</td>
                <td class="text-center">{{ $stavka->kolicina }}</td>
                <td>{{ number_format($stavka->pcena,2,",",".") }}</td> 
            </tr>
            @endforeach

            </tbody>
        </table>
    </div>