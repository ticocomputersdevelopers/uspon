<header class="relative"> 
	<section class="popup info-confirm-popup info-popup">
        <div class="popup-wrapper">
            <section class="popup-inner">
            </section>
        </div>
    </section>
	<div class="JSfixed-hdr">
		<div id="JSfixed_header"> 
			<div class="container"> 
	 
				<div class="row flex"> 
					<div class="col-md-3 col-sm-3 col-xs-12">
						<a class="logo inline-block" href="{{ B2bOptions::base_url()}}b2b" title="{{B2bOptions::company_name()}}">
							<img src="{{ B2bOptions::base_url() }}images/uspon-b2b-logo.png" alt="{{ B2bOptions::company_name() }}">
							<!-- <img class="img-responsive" src="{{ B2bOptions::base_url()}}{{B2bOptions::company_logo()}}" alt="{{B2bOptions::company_name()}}" /> -->
						</a>
					</div>
		 
					<div class="col-md-5 col-sm-4 col-xs-12">
						<form action="{{route('b2b.search')}}" method="GET" id="JSlive-search" class="search-form">
							<div class="row"> 
								<div class="col-xs-4 no-padding">
									<select name="search_grupa_pr_id">
										{{ B2bCommon::selectGroups(!is_null(Input::get('search_grupa_pr_id')) ? Input::get('search_grupa_pr_id') : null) }}
									</select>
								</div>
								<div class="col-xs-8 no-padding"> 
									<input class="search-field" type="text" id="search" autocomplete="off" name="q" value="{{Input::get('q')?Input::get('q'):''}}" placeholder="Pretraga" />
									<button class="button JSsearch-button"> <i class="fa fa-search"></i> </button>
								</div> 
							</div>
						</form>
					</div>

					<div class="col-md-4 col-sm-4 col-xs-12 text-center flex-between">
						<div class="inline-block user-info text-left">
							<?php $komercijalista = !is_null($user = B2bCommon::getLoggedAdminUser('KOMERCIJALISTA')) ? $user : B2bPartner::komercijalista(); ?>
							@if(!is_null($komercijalista))
							<div class="inline-block vert-align text-center">
								<i class="trans-user inline-block"></i>
							</div>

							<div class="inline-block vert-align user-info-ch relative">
								<div class="text-uppercase foo-1"><span>{{ $komercijalista->ime.' '.$komercijalista->prezime }}</span></div> 
								<div class="foo-2"><span>{{ $komercijalista->login }}</span></div> 
								<div class="foo-3">{{ $komercijalista->mobilni }}</div>
							</div> 
							@endif
						</div>
				  
				  
						<div class="mini-cart-container inline-block relative text-left"> 
							<a class="header-cart inline-block" href="{{ B2bOptions::base_url() }}b2b/{{ B2bCommon::get_korpa() }}">
								 
								<i class="inline-block fa fa-shopping-cart"></i> 
						 
								<span id="header_total_amount_show" class="cart-amount-price">
									{{ B2bBasket::cena(B2bBasket::total()->ukupna_cena) }}
								</span>   
								
								<span>({{ B2bBasket::b2bCountItems() }})</span> 
							</a> 
							@include('b2b.partials.cart_top')
						</div>
					</div>
				</div>
			</div>
		</div> 
	</div>

	<!-- CATEGORIES -->
	<div class="menu-background">
		<div class="container">
			<div class="row" id="responsive-nav"> 

				<div class="col-md-3 col-sm-12 col-xs-12 sm-nopadd p-left relative"> 
					@include('b2b.partials.categories')
				</div>
				
				<div class="col-md-9 col-sm-12 col-xs-12">
					<ul class="list-inline pages">  
						<!--    <li>
						<a href="{{B2bOptions::base_url()}}b2b/o-nama">O nama</a>
						</li>  -->
<!-- 						<li>
							<a href="{{B2bOptions::base_url()}}b2b/reklamacije">RMA</a>
						</li>  -->
						<li>
							<a href="{{B2bOptions::base_url()}}b2b/blog">Blog</a>
						</li>
						@if(is_null(B2bPartner::getPartnerUserObject()))
						<li>
							<a href="{{B2bOptions::base_url()}}b2b/user/card">Kartica</a>
						</li>
						<li>
							<a href="{{B2bOptions::base_url()}}b2b/user/orders">Narudžbine</a>
						</li>
						<li>
							<a href="{{B2bOptions::base_url()}}b2b/user/users">Korisnici</a>
						</li>
						@endif
						<li>
							<a href="{{B2bOptions::base_url()}}b2b/akcija">Akcija</a> 
						</li> 
						<!-- <li>
							<a href="{{B2bOptions::base_url()}}b2b/price_diff">Promena cene</a>
						</li> -->
						<!-- <li>
							<a href="{{B2bOptions::base_url()}}b2b/bestseller">Najprodavaniji Artikli</a>
						</li> -->
						@if(!is_null(B2bPartner::dokumentiUser()))
						<li>
							<a href="{{B2bOptions::base_url()}}b2b/dokumenti/ponude">Ponude</a>
						</li>
						@endif
						<!--   <li>
						<a href="{{B2bOptions::base_url()}}b2b/brendovi">Brendovi</a>
						</li>  -->
						<!--   <li>
						<a href="{{B2bOptions::base_url()}}b2b/usluge">Usluge</a>
						</li>   
						<li>
						<a href="{{B2bOptions::base_url()}}b2b/kontakt">Kontakt</a>
						</li> 
						--> 
	            		@foreach(B2bCommon::header_menu_pages() as $row)
		                <li>
		                    @if(B2bCommon::count_childs_pages($row->web_b2b_seo_id) > 0)  
		                    <a href="{{ B2bOptions::base_url().'b2b/'.$row->naziv_stranice }}">{{$row->title}}</a>
		                    <ul class="drop-2">
		                        @foreach(B2bCommon::header_menu_pages($row->web_b2b_seo_id) as $row2)
		                        <li> 
		                            <a href="{{ B2bOptions::base_url().'b2b/'.$row2->naziv_stranice }}">{{$row2->title}}</a>
		                            <ul class="drop-3">
		                                @foreach(B2bCommon::header_menu_pages($row2->web_b2b_seo_id) as $row3)
		                                <li> 
		                                    <a href="{{ B2bOptions::base_url().'b2b/'.$row3->naziv_stranice }}">{{$row3->title}}</a>
		                                </li>
		                                @endforeach
		                            </ul>
		                        </li>
		                        @endforeach
		                    </ul>
		                    @else   
		                    <a href="{{ B2bOptions::base_url().'b2b/'.$row->naziv_stranice }}">{{$row->title}}</a> 
		                    @endif                    
		                </li>                     
		                @endforeach 
					</ul>
				</div> 
			</div>
		</div>	
	</div>
	
	<div class="resp-nav-btn hidden-md hidden-lg"><span class="fas fa-bars"></span></div>
</header>