@extends('b2b.layouts.b2bpage')

@section('content')
<div class="medium-7 columns"> 
    <form action="{{route('b2b.user_update')}}" method="post" >
        <h2><span class="heading-background">Registracija </span></h2>
        <label for="naziv">Naziv firme*</label>
        <input id="naziv" name="naziv" type="text" value="{{$partner->naziv}}" required {{$is_is ? 'disabled="disabled"' : ''}}>

        <label for="pib">PIB*</label>
        <input id="pib" name="pib" type="text" value="{{$partner->pib}}" required {{$is_is ? 'disabled="disabled"' : ''}}>

        <label for="broj_maticni">Matični broj*</label>
        <input id="broj_maticni" name="broj_maticni" value="{{$partner->broj_maticni}}" type="text" required {{$is_is ? 'disabled="disabled"' : ''}}>

        <label for="adresa">Adresa *</label>
        <input id="adresa" name="adresa" value="{{$partner->adresa}}" type="text" required {{$is_is ? 'disabled="disabled"' : ''}}>
       
        <label for="mesto">Mesto</label>
        <input name="mesto" type="text" value="{{ Input::old('mesto') ? Input::old('mesto') : $partner->mesto }}" {{$is_is ? 'disabled="disabled"' : ''}}>

        <h2><span class="heading-background">Kontakt osoba </span></h2>
        <label for="komercijalista">Ime i prezime *</label>
        <input id="komercijalista" name="komercijalista" value="{{$partner->komercijalista}}" type="text" required {{$is_is ? 'disabled="disabled"' : ''}}>

        <label for="telefon">Telefon *</label>
        <input id="telefon" name="telefon" value="{{$partner->telefon}}" type="text" required {{$is_is ? 'disabled="disabled"' : ''}}>

        <label for="mail">Email *</label>
        <input id="mail" name="mail" value="{{$partner->mail}}" type="email" required {{$is_is ? 'disabled="disabled"' : ''}}>

        <h2><span class="heading-background">Pristupni podaci </span></h2>
        <label for="login">Korisnik *</label>
        <input id="login" name="login" value="{{$partner->login}}" type="text" required {{$is_is ? 'disabled="disabled"' : ''}}>

        <label for="password">Lozinka *</label>
        <input id="password" name="password" value="{{$partner->password}}" type="password" required {{$is_is ? 'disabled="disabled"' : ''}}>
        @if(!$is_is)
         <button class="submit admin-login" >Sačuvaj </button>
         @endif

    </form>
    </div>
 <script src="{{ B2bOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/b2b/main_function.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript" ></script>
    
@endsection