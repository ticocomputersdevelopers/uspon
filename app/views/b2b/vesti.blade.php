@extends('b2b.layouts.b2bpage')
@section('content')
<section class="news-section">
	@foreach($news as $row)
		<article class="news">
			<div class="row">
				<div class="columns medium-12 large-4">
					<a href="{{ B2bOptions::base_url() }}b2b/vesti/{{ $row->web_vest_b2c_id }}"><img src="{{ B2bOptions::base_url() . $row->slika }}" alt=""></a>
				</div>
				<div class="columns medium-12 large-8">
					<a href="{{ B2bOptions::base_url() }}b2b/vesti/{{ $row->web_vest_b2c_id }}"><h2 class="news-title-list">{{ $row->naslov }}</h2></a>
					<p>{{ B2bVesti::shortNewDesc($row->tekst) }}</p>
					<div class="btn-container right">
						<a class="btn btn-primary news__more" href="{{ B2bOptions::base_url() }}b2b/vesti/{{ $row->web_vest_b2c_id }}">Pročitaj članak</a>
					</div>
				</div>
			</div> <!--  end of .row -->
		</article>
	@endforeach
	<div>
		{{ $news->links() }}
	</div>
</section>
@endsection