@extends('b2b.layouts.b2bpage')
@if($strana=='artikli')
@if(B2b::checkCategoryLevel($grupa_pr_id))
    @include('b2b/filters')
    @else
@section('breadcrumbs')
    <aside id="sidebar-left" class="medium-3 columns product-list-page">
          <!-- CATEGORIES -->
            @if(B2bOptions::category_view()==1)
            @include('b2b.categories')
            @endif
        <ul class="breadcrumbs clearfix">

                {{ B2bUrl::b2bBreadcrumbs() }}

        </ul>
    </aside>
@endsection
    @endif
@else
@section('breadcrumbs')
    <aside id="sidebar-left" class="medium-3 columns product-list-page">
          <!-- CATEGORIES -->
            @if(B2bOptions::category_view()==1)
            @include('b2b.categories')
            @endif
        <ul class="breadcrumbs clearfix">

                <li><a href="{{ B2bOptions::base_url() }}">{{ B2bCommon::get_title_page_start() }}</a></li>
                <li><h1>{{$seo['title']}}</h1></li>

        </ul>
    </aside>


@endsection
@endif


@section('content')

    <section class="product-list-options clearfix">
        <section class="medium-6 small-12 columns">
            <span class="product-number">Ukupno: {{ $count_products }}</span>
            @if(B2bOptions::product_number()==1)
                <section class="per-page select-field">

							<span class="per-page-active">
								@if(Session::has('limit'))
                                    {{Session::get('limit')}}
                                @else
                                    20
                                @endif
							</span>
                    <ul>
                        <li><a href="{{ B2bOptions::base_url() }}limit/20">20</a></li>
                        <li><a href="{{ B2bOptions::base_url() }}limit/30">30</a></li>
                        <li><a href="{{ B2bOptions::base_url() }}limit/50">50</a></li>
                    </ul>
                </section>
            @endif
            @if(Route::getCurrentRoute()->getName()=='b2b.search')
                {{ $query_products->appends(['q'=>Input::get('q')])->links() }}
            @else
                {{ $query_products->links() }}
            @endif

        </section>



        <section class="medium-6 small-12 columns">
          <!--   @if(B2bOptions::product_currency()==1)
                <section class="currency select-field">
                    <span class="currency-active">{{B2bCommon::get_valuta()}}</span>
                    <ul>
                        <li><a href="{{ B2bOptions::base_url() }}valuta/1">Din</a></li>
                        <li><a href="{{ B2bOptions::base_url() }}valuta/2">Eur</a></li>
                    </ul>
                </section>
            @endif -->
            @if(B2bOptions::product_sort()==1)
                <section class="sort-products select-field">
                    <span class="sort-active">{{B2bCommon::get_sort()}}</span>
                    <ul>
                        <li><a href="{{route('b2b.ordering',['price_asc'])}}">Cena - Rastuće</a></li>
                        <li><a href="{{route('b2b.ordering',['price_desc'])}}">Cena - Opadajuće</a></li>
                        <li><a href="{{route('b2b.ordering',['news'])}}">Najnovije</a></li>
                        <li><a href="{{route('b2b.ordering',['name'])}}">Prema nazivu</a></li>
                    </ul>
                </section>
            @endif
        </section>
    </section>





    <!-- Lista proucts -->
    <section class="product-list list-view row">
		<ul class="small-12 columns first-column">
			<li class="big columns">Naziv</li>
			<li class="medium columns">Slika</li>
            <li class="medium columns">Cena</li>
            <li class="small columns">Rabat</li>
            <li class="medium columns">Cena sa rabatom</li>
            <li class="small columns">PDV Stopa</li>
            <li class="medium columns">Cena sa PDV-om</li>
            <li class="kolicina columns">Količina</li>
			<li class="medium columns">Korpa</li>
        </ul>
        <!-- PRODUCT -->
        @foreach($query_products as $row)
            <ul class="small-12 columns second-column">
				<li class="big columns">
					<a class="product-title" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}">{{ B2bArticle::short_title($row->roba_id) }}</a>
				</li>
				<li class="medium columns">
					<a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}" class="product-image-wrapper">
					<img class="product-image" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ B2bArticle::seo_title($row->roba_id) }}" />
					</a>
				</li>
                <?php
                    $rabatCene = B2bArticle::b2bRabatCene($row->roba_id);
                    $quantity = B2bArticle::quantityB2b($row->roba_id) - B2bBasket::getB2bQuantityItem($row->roba_id);
                ?>
                <li class="medium columns">{{B2bBasket::cena($rabatCene->osnovna_cena)}}</li>
                <li class="small columns">{{number_format($rabatCene->ukupan_rabat,2)}} %</li>
                <li class="medium columns">{{B2bBasket::cena($rabatCene->cena_sa_rabatom)}}</li>
                <li class="small columns">{{number_format($rabatCene->porez,2)}} %</li>
                <li class="medium columns">{{B2bBasket::cena($rabatCene->ukupna_cena,2)}}</li>
                <li class="kolicina columns">{{$quantity <= 10 ? $quantity : ($quantity > 50 ? '> 50' : '> 10')}}</li>
				<li class="medium columns">
				    <!-- Ovde ubaciti za kolicinu kod. -->
                @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                    @if($quantity>0)
                    <input class="add-amount" id="quantity-{{$row->roba_id}}" type="text" value="1">
                    <button class="add-to-cart-products" data-max-quantity="{{$quantity}}" data-roba-id="{{$row->roba_id}}"> <i class="fa fa-cart-plus"></i> U korpu</button>
                    @else
                        <span>Nije dostupan</span>
                    @endif
                @else
                <span>{{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($row->roba_id),'naziv') }}</span>
                @endif
			    </li>
        </ul>
        @endforeach
    </section>



    @if($count_products == 0)
        <p class="empty-page"> Trenutno nema artikla za date kategorije</p>
    @endif
    @if(Route::getCurrentRoute()->getName()=='b2b.search')
    {{ $query_products->appends('q',Input::get('q'))->links() }}
    @else
    {{ $query_products->links() }}
    @endif


        </section>
@endsection

@section('footer')
    <section class="popup info-confirm-popup info-popup">

        <div class="popup-wrapper">

            <section class="popup-inner">



            </section>

        </div>

    </section>

    <script src="{{ B2bOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/jquery.fancybox.pack.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript" ></script>

@endsection
