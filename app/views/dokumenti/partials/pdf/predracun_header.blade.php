<div class="row"> 
	<div class="logo col-3">
		<img src="{{ AdminOptions::base_url()}}{{$podesavanja->logo}}" alt="logo">
	</div>
	<div class="col-4 company-info">
		<p class="comp-name">{{ is_null($saradnik) ? AdminOptions::company_name() : $saradnik->naziv }}</p>  
		<p>{{ is_null($saradnik) ? AdminOptions::company_adress() : $saradnik->adresa }}, {{ is_null($saradnik) ? AdminOptions::company_mesto() : $saradnik->mesto }}</p>
		<p>Telefon: {{ is_null($saradnik) ? AdminOptions::company_phone() : $saradnik->telefon }}{{ is_null($saradnik) ? ', Fax: '.AdminOptions::company_fax() : '' }}</p>
		<p>PIB: {{ is_null($saradnik) ? AdminOptions::company_pib() : $saradnik->pib }}</p>
	    <p>E-mail: {{ is_null($saradnik) ? AdminOptions::company_email() : $saradnik->mail }}</p>
	</div>
	
	<div class="col-4 kupac-info">
		<table>
			<tr>
	            <td>Kupac:</td>
	            <td>{{$partner->naziv}}</td>
	        </tr>
            <tr>
	            <td>Adresa:</td>
	            <td>{{$partner->adresa}}</td>
            </tr>
            <tr>
	            <td>Mesto:</td>
	            <td>{{$partner->mesto}}</td>
            </tr>
            <tr>
	            <td>Telefon:</td>
	            <td>{{$partner->telefon}}</td>
            </tr>
	        <tr>
	            <td>PIB:</td>
	            <td>{{$partner->pib}}</td>
	        </tr>
            <tr>
	            <td>Email:</td>
	            <td>{{$partner->mail}}</td>
            </tr>
        </table>
	</div>
	</div>		

	<div class="row"> 
	<p class="ziro">{{ AdminLanguage::transAdmin('Žiro racun') }}: {{AdminOptions::company_ziro()}}</p>
 </div>

	<div class="row">
	<h4 class="racun-br">{{ AdminLanguage::transAdmin('Racun broj') }}: {{$predracun->broj_dokumenta}}</h4>
 </div>

<div class="row"> 
	<table class="info-1">
		<thead>
			<tr>
				<td>{{ AdminLanguage::transAdmin('Datum izdavanja') }}</td>
				<td>{{ AdminLanguage::transAdmin('Važi do') }}</td>
				<td>{{ AdminLanguage::transAdmin('Rok isporuke') }}</td>
				<td>{{ AdminLanguage::transAdmin('Nacin plaćanja') }}</td>
				<td>{{ AdminLanguage::transAdmin('Cene su u') }}</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{AdminNarudzbine::formatDate($predracun->datum_predracuna)}}</td>
				<td>{{AdminNarudzbine::formatDate($predracun->vazi_do)}}</td>
				<td>{{AdminNarudzbine::formatDate($predracun->rok_isporuke)}}</td>
				<td>{{ $predracun->nacin_placanja }}</td>
				<td>din.</td>
			</tr>
		</tbody>
	</table>
</div>