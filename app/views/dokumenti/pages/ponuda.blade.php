@extends(($isb2b?'b2b':'dokumenti').'.templates.main')
@section('content')
<div id="main-content" class="kupci-page col-xs-12 no-padding"> 
	<div class="offer">
		<div class="flat-box">
			<div class="row flex">

			@if($isb2b)
				<div class="columns medium-3 inline-block col-sm-6 col-xs-12 no-padding heading-offer text-right sm-text-left">
					<a href="{{DokumentiOptions::base_url()}}b2b/dokumenti/ponude"><h1 class="inline-block ponuda btn no-padding">Ponude</h1></a>
				</div>
			@endif
				<div class="columns medium-3 inline-block relative  col-sm-6 col-xs-12 no-padding">

					<h1 class="inline-block ponuda title">
						Broj {{ $ponuda->broj_dokumenta }} 
						@if(!$isb2b)
							@if(!is_null($predracun))
							<a href="{{DokumentiOptions::base_url()}}dokumenti/predracun/{{$predracun->predracun_id}}">{{$predracun->broj_dokumenta}}</a>
							@endif
							@if(!is_null($racun))
							<a href="{{DokumentiOptions::base_url()}}dokumenti/racun/{{$racun->racun_id}}">{{$racun->broj_dokumenta}}</a> 
							@endif
						@endif
					</h1>
				</div>
			</div>
		</div>

		<form method="POST" action="{{DokumentiOptions::base_url()}}{{($isb2b?'b2b/':'')}}dokumenti/ponuda-post" id="JSPonudaForm"> 
			<div class="flat-box">
				<div class="row">
					<div class="field-group columns medium-6 col-xs-12 no-padding">
						<div class="row"> 
							<div class="columns medium-12 col-md-6 col-sm-12 col-xs-12 no-padding">
								<!-- <h2 class="title-med hidden">Informacije o kupcu</h2> -->
								<label for="">Kupac</label>
								<input type="hidden" name="partner_id" id="JSPartnerId" value="{{ (Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id) }}">
								<input type="text" name="naziv" id="JSPartnerNaziv" value="{{ (Input::old('naziv') ? Input::old('naziv') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'naziv')) }}" class="{{ $errors->first('naziv') ? 'error' : '' }}" autocomplete="off">
								<div id="JSPartnerSearchContent" class="rma-custom-select"></div>
							</div>
							<div class="columns medium-12 sm-nopadd col-md-6 col-sm-12 col-xs-12">
								<label for="">PIB</label>
								<input type="text" name="pib" id="JSPartnerPib" value="{{ trim(Input::old('pib') ? Input::old('pib') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'pib')) }}" class="{{ $errors->first('pib') ? 'error' : '' }}" autocomplete="off">
							</div>
						</div>

						<div class="row"> 
							<div class="columns medium-12 col-md-6 col-sm-12 col-xs-12 no-padding">
								<label for="">Ulica i broj</label>
								<input type="text" name="adresa" id="JSPartnerAdresa" value="{{ (Input::old('adresa') ? Input::old('adresa') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'adresa')) }}" class="{{ $errors->first('adresa') ? 'error' : '' }}" autocomplete="off">
							</div>
							<div class="columns medium-12 sm-nopadd col-md-6 col-sm-12 col-xs-12">
								<label for="">Mesto</label>
								<input type="text" name="mesto" id="JSPartnerMesto" value="{{ (Input::old('mesto') ? Input::old('mesto') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'mesto')) }}" class="{{ $errors->first('mesto') ? 'error' : '' }}" autocomplete="off">
							</div>
						</div> 
					</div>
					<div class="field-group columns medium-6 col-xs-12 no-padding">
						<div class="row"> 
							<div class="columns medium-12 col-md-6 col-sm-12 col-xs-12 no-padding">
								<!-- <h2 class="title-med hidden">Kontakt informacije</h2> -->
								<label for="">Kontakt Osoba</label>
								<input type="text" name="kontakt_osoba" id="JSPartnerKontaktOsoba" value="{{ (Input::old('kontakt_osoba') ? Input::old('kontakt_osoba') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'kontakt_osoba')) }}" class="{{ $errors->first('kontakt_osoba') ? 'error' : '' }}" autocomplete="off">
							</div>
							<div class="columns medium-12 sm-nopadd col-md-6 col-sm-12 col-xs-12">
								<label for="">Email</label>
								<input type="text" name="mail" id="JSPartnerMail" value="{{ (Input::old('mail') ? Input::old('mail') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'mail')) }}" class="{{ $errors->first('mail') ? 'error' : '' }}" autocomplete="off">
							</div>
							<div class="columns medium-12 col-md-6 col-sm-12 col-xs-12 no-padding">
								<label for="">Telefon</label>
								<input type="text" name="telefon" id="JSPartnerTelefon" value="{{ (Input::old('telefon') ? Input::old('telefon') : Dokumenti::getPartner((Input::old('partner_id') ? Input::old('partner_id') : $partner->partner_id),'telefon')) }}" class="{{ $errors->first('telefon') ? 'error' : '' }}" autocomplete="off">
							</div>
						</div>
					</div>
					<div class="field-group columns medium-12 col-xs-12 no-padding">
						<div class="row"> 
							<div class="field-group columns medium-2 no-padding col-md-6 col-sm-12 col-xs-12">
								<label for="">Datum izdavanja</label>
								<input name="datum_ponude" class="datum-val has-tooltip {{ $errors->first('datum_ponude') ? 'error' : '' }}" type="text" value="{{ Input::old('datum_ponude') ? Input::old('datum_ponude') : Dokumenti::dateInversion($ponuda->datum_ponude) }}" autocomplete="off">
							</div>
							<div class="field-group columns medium-2 sm-nopadd col-md-6 col-sm-12 col-xs-12">
								<label for="">Važi do</label>
								<input name="vazi_do" class="datum-val has-tooltip {{ $errors->first('vazi_do') ? 'error' : '' }}" type="text" value="{{ Input::old('vazi_do') ? Input::old('vazi_do') : Dokumenti::dateInversion($ponuda->vazi_do) }}" autocomplete="off">
							</div>
							<div class="field-group columns medium-2 no-padding col-md-6 col-sm-12 col-xs-12">
								<label for="">Rok isporuke</label>
								<input name="rok_isporuke" class="datum-val has-tooltip {{ $errors->first('rok_isporuke') ? 'error' : '' }}" type="text" value="{{ Input::old('rok_isporuke') ? Input::old('rok_isporuke') : Dokumenti::dateInversion($ponuda->rok_isporuke) }}" autocomplete="off">
							</div>
							<div class="field-group columns medium-2 sm-nopadd col-md-6 col-sm-12 col-xs-12">
								<label for="">Način plaćanja</label>
								<input name="nacin_placanja" class="datum-val has-tooltip {{ $errors->first('nacin_placanja') ? 'error' : '' }}" type="text" value="{{ Input::old('nacin_placanja') ? Input::old('nacin_placanja') : $ponuda->nacin_placanja }}" autocomplete="off">
							</div>
							<div class="field-group columns medium-2 no-padding col-md-6 col-sm-12 col-xs-12">
								<label for="">Status</label>
								<select name="dokumenti_status_id" id="JSPonudaStatus" class="field-status">
									{{ Dokumenti::statusSelect((Input::old('dokumenti_status_id') ? Input::old('dokumenti_status_id') : $ponuda->dokumenti_status_id)) }}
								</select>
							</div> 

							<div class="field-group columns medium-2 no-padd"> 
							@if(!$isb2b)
								<div> 
									<input name="predracun" class="datum-val has-tooltip {{ $errors->first('predracun') ? 'error' : '' }}" type="checkbox" {{ ((count(Input::old()) > 0 ? (Input::old('predracun')=="on" ? 1 : 0) : $ponuda->predracun)==1) ? 'checked' : '' }}>
									<label class="no-margin" for="">Predračun</label>
								</div> 
							@endif

							@if(AdminOptions::gnrl_options(3050) AND !$isb2b)  
								<div> 
									<input name="racun" class="datum-val has-tooltip {{ $errors->first('racun') ? 'error' : '' }}" type="checkbox" {{ ((count(Input::old()) > 0 ? (Input::old('racun')=="on" ? 1 : 0) : 0)==1) ? 'checked' : '' }}">
									<label class="no-margin" for="">Račun</label>
								</div> 
							@endif
		<!-- 					@if($ponuda->ponuda_id > 0)
							<div class="field-group columns medium-2 no-padd sm-nopadd col-md-2">
								<br>
								<div class="inline-block"> 
									<input name="narudzbina" id="JSNarudzbina" class="datum-val has-tooltip {{ $errors->first('narudzbina') ? 'error' : '' }}" type="checkbox" {{ ((count(Input::old()) > 0 ? (Input::old('narudzbina')=="on" ? 1 : 0) : 0)==1) ? 'checked' : '' }}">
									<label class="no-margin" for="">Narudžbina</label>
								</div>
							</div>
							@endif -->
							@if($ponuda->ponuda_id > 0)  
								<div class="inline-block"> 
									<input name="bezpdv" id="JSBezPdv" class="datum-val has-tooltip {{ $errors->first('bezpdv') ? 'error' : '' }}" type="checkbox" >
									<label class="no-margin" for="">0% PDV</label>
								</div>
							@endif
							</div>
							
							@if($ponuda->ponuda_id > 0 AND $isb2b)
							<div class="field-group columns medium-2 no-padd sm-nopadd col-md-3">
								<br>
								<div> 
									<input name="default_ponuda" class="datum-val has-tooltip {{ $errors->first('default_ponuda') ? 'error' : '' }}" type="checkbox" {{ ((count(Input::old()) > 0 ? (Input::old('default_ponuda')=="on" ? 1 : 0) : $default_ponuda_id)==1) ? 'checked' : '' }}>
									<label class="no-margin" for="">Ponuda na b2b-u</label>
								</div>
								<br>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>

			<div class="flat-box">
				<div id="JSPonudaSabloniLink" class="sabloni">&nbsp; Šabloni</div>
				<div class="row" id="JSPonudaSabloni" hidden="hidden">
					<div class="field-group columns medium-12">
						<label for="">Šablon</label>
						<textarea name="tekst" class="special-textareas">{{ $ponuda->tekst }}</textarea>
					</div>
					<div class="field-group columns medium-12">
						<label for="">Footer</label>
						<textarea name="tekst_footer" class="special-textareas">{{ $ponuda->tekst_footer }}</textarea>
					</div>
				</div>
				<input type="hidden" name="ponuda_id" value="{{ $ponuda->ponuda_id }}">
				<input type="hidden" name="u_korpu" value="0">
			</div>

			@if($ponuda->ponuda_id > 0)
			<div class="flat-box" id="JSStavke">
				<div class="row">
					<div class="field-group columns medium-12 table-responsive">
						<table class="table">
							<thead>
								<tr>
									<td>ID</td>
									<td>Naziv</td>
									<td>Kolicina</td>
									<td>Cena po jedinici</td>
									<td>Rabat (%)</td>
									<td>Dodatni rabat (%)</td>
									<td>Cena sa rabatom</td>
									<td>Iznos PDV-a (%)</td>
									<td>Ukupno (din.)</td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								@foreach($stavke as $stavka)
								<tr class="JSStavkaRow" data-roba_id="{{$stavka->roba_id}}" data-ponuda_stavka_id="{{$stavka->ponuda_stavka_id}}" data-ponuda_id="{{$ponuda->ponuda_id}}">
									<td><span class="JSRobaIdText">{{$stavka->roba_id}}</span></td>
									<td>
										@if(!is_null($stavka->roba_id))
										<select class="JSRobaId" name="stavka_id_{{$stavka->ponuda_stavka_id}}">
											{{ Dokumenti::artikliSelect($stavka->roba_id) }}
										</select>
										@else
										<input type="text" class="JSStavkaNaziv" value="{{$stavka->naziv_stavke}}">    
										@endif
									</td>
									<td>
										<input type="text" class="JSKolicina" value="{{ $stavka->kolicina }}">
									</td>
									<td>
										<input type="text" class="JSNsbCena" value="{{round($stavka->nab_cena,2)}}">
									</td>
									<td>
										<input type="text" class="JSRabat" value="{{$stavka->rabat}}">
									</td>
									<td>
										<input type="text" class="JSAddedRabat" value="{{$stavka->added_rabat}}">
									</td>
									<td>
										<input type="text" class="JSRabatCena" value="{{round($stavka->nab_cena*(1-$stavka->rabat/100)*(1-$stavka->added_rabat/100),2)}}" disabled>
									</td>
									<td><input type="text" class="JSPdv" value="{{$stavka->pdv}}"></td>
									<td><input type="text" class="JSUkupno" value="{{ number_format(round(((($stavka->nab_cena*(1-$stavka->rabat/100)*(1-$stavka->added_rabat/100))*$stavka->kolicina)*(1+$stavka->pdv/100)),2),2,'.','') }}" disabled></td>
									@if(!$checkRacuni AND !$checkPredracuni)
									<td><a href="{{DokumentiOptions::base_url()}}{{($isb2b?'b2b/':'')}}dokumenti/ponuda-stavka/{{$stavka->ponuda_stavka_id}}/delete"><i class="fa fa-times remove"></i></a></td>
									@else
									<td></td>
									@endif
								</tr>
								@endforeach
								@if($novaStavka)
								<tr class="JSStavkaRow" data-ponuda_stavka_id="0" data-ponuda_id="{{$ponuda->ponuda_id}}">
									<td><span class="JSRobaIdText">0</span></td>
									<td>
										<select class="JSRobaId">
											{{ Dokumenti::artikliSelect(0,$roba_ids) }}
										</select>
									</td>
									<td>
										<input type="text" class="JSKolicina" value="1">
									</td>
									<td>
										<input type="text" class="JSNsbCena" value="0">
									</td>
									<td>
										<input type="text" class="JSRabat" value="0">
									</td>
									<td>
										<input type="text" class="JSAddedRabat" value="0">
									</td>
									<td>
										<input type="text" class="JSRabatCena" value="0.00" disabled>
									</td>
									<td><input type="text" class="JSPdv" value="20"></td>
									<td><input type="text" class="JSUkupno" value="0.00" disabled></td>
									<td><a href="{{DokumentiOptions::base_url()}}{{($isb2b?'b2b/':'')}}dokumenti/ponuda/{{$ponuda->ponuda_id}}#JSStavke"><i class="fa fa-times remove"></i></a></td>
								</tr>
								@endif
								<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td id="JSPonudaUkupanIznos">{{ round($stavkeUkupanIznos,2) }}</td>
									<td></td>
								</tr>						
								@if(!$novaStavka AND !$checkRacuni AND !$checkPredracuni)
								<tr>
									<td></td>
									<td><a class="btn" href="{{DokumentiOptions::base_url()}}{{($isb2b?'b2b/':'')}}dokumenti/ponuda/{{$ponuda->ponuda_id}}?nova_stavka=1#JSStavke">Dodaj novu stavku</a></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								@endif
							</tbody>
						</table>
					</div>      	
				</div>
			</div>
			@endif

			<div class="flat-box">
				<div class="row">
					<div class="columns medium-12 large-12 text-center">
						<div class="btn-container"> 
							@if(!$checkRacuni AND !$checkPredracuni)
							<input type="submit" name="submit" id="JSPonudaSubmit" class="btn save-it-btn" style="{{(count(Input::old()) > 0) ? 'background-color: red;':''}}" value="Sačuvaj">
							<input type="submit" name="submit_clone" id="JSPonudaSubmitClone" class="btn save-it-btn" style="{{(count(Input::old()) > 0) ? 'background-color: red;':''}}" value="Kloniraj">
							@if(!is_null(DokumentiOptions::user('saradnik')))
							<input type="submit" name="submit_order" id="JSPonudaSubmitOrder" class="btn save-it-btn" value="Prebaci u korpu">
							@endif
							@endif 
				 			@if(!$checkRacuni AND !$checkPredracuni AND $ponuda->ponuda_id > 0)
							<a class="btn JSbtn-delete" data-link="{{DokumentiOptions::base_url()}}{{($isb2b?'b2b/':'')}}dokumenti/ponuda/{{$ponuda->ponuda_id}}/delete">Obriši</a>
							@endif
							@if($ponuda->ponuda_id > 0)
							<a href="{{DokumentiOptions::base_url()}}dokumenti/ponuda-pdf/{{$ponuda->ponuda_id}}" class="btn btn-primary save-it-btn" target="_blank">PDF</a>
							@endif
						</div>				
					</div>
				</div>
			</div>
		</form> 
	</div>
</div>
@endsection