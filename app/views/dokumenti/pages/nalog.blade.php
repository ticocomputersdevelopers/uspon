@extends('dokumenti.templates.main')
@section('content')
<div id="main-content" class="kupci-page">
	<div class="row">
		<form action="{{ DokumentiOptions::base_url() }}dokumenti/nalog-save" method="post">
		    <br>
		    <h2><span class="heading-background">Registracija </span></h2>
		    <div class="row"> 
		        <div class="form-group col-md-4 col-sm-4 col-xs-12">
		            <label for="naziv">Naziv firme*</label>
		            <input class="form-control" id="naziv" name="naziv" type="text" value="{{$partner->naziv}}" {{$is_is ? 'disabled="disabled"' : ''}}>
		            <div class="error red-dot-error">{{ $errors->first('naziv') }}</div>
		        </div>

		        <div class="form-group col-md-4 col-sm-4 col-xs-12">
		            <label for="pib">PIB*</label>
		            <input class="form-control" id="pib" name="pib" type="text" value="{{trim($partner->pib)}}" {{$is_is ? 'disabled="disabled"' : ''}}>
		            <div class="error red-dot-error">{{ $errors->first('pib') }}</div>
		        </div>

		        <div class="form-group col-md-4 col-sm-4 col-xs-12">
		            <label for="broj_maticni">Matični broj*</label>
		            <input class="form-control" id="broj_maticni" name="broj_maticni" value="{{$partner->broj_maticni}}" type="text" {{$is_is ? 'disabled="disabled"' : ''}}>
		            <div class="error red-dot-error">{{ $errors->first('broj_maticni') }}</div>
		        </div>
		    </div>

		    <div class="row"> 
		        <div class="form-group col-md-4 col-sm-4 col-xs-12">
		            <label for="adresa">Adresa *</label>
		            <input class="form-control" id="adresa" name="adresa" value="{{$partner->adresa}}" type="text" {{$is_is ? 'disabled="disabled"' : ''}}>
		            <div class="error red-dot-error">{{ $errors->first('adresa') }}</div>
		        </div>

		        <div class="form-group col-md-4 col-sm-4 col-xs-12">
		            <label for="mesto">Mesto</label>
		            <input class="form-control" name="mesto" type="text" value="{{ Input::old('mesto') ? Input::old('mesto') : $partner->mesto }}" {{$is_is ? 'disabled="disabled"' : ''}}>
		        </div>
		    </div>

		    <h2><span class="heading-background">Kontakt osoba </span></h2>
		    <div class="row"> 
		        <div class="form-group col-md-4 col-sm-4 col-xs-12">
		            <label for="kontakt_osoba">Ime i prezime *</label>
		            <input class="form-control" id="kontakt_osoba" name="kontakt_osoba" value="{{$partner->kontakt_osoba}}" type="text" {{$is_is ? 'disabled="disabled"' : ''}}>
		            <div class="error red-dot-error">{{ $errors->first('kontakt_osoba') }}</div>
		        </div>

		        <div class="form-group col-md-4 col-sm-4 col-xs-12">
		            <label for="telefon">Telefon *</label>
		            <input class="form-control" id="telefon" name="telefon" value="{{$partner->telefon}}" type="text" {{$is_is ? 'disabled="disabled"' : ''}}>
		            <div class="error red-dot-error">{{ $errors->first('telefon') }}</div>
		        </div>

		        <div class="form-group col-md-4 col-sm-4 col-xs-12">
		            <label for="mail">Email *</label>
		            <input class="form-control" id="mail" name="mail" value="{{$partner->mail}}" type="text" {{$is_is ? 'disabled="disabled"' : ''}}>
		            <div class="error red-dot-error">{{ $errors->first('mail') }}</div>
		        </div>
		    </div>

		    <h2><span class="heading-background">Pristupni podaci </span></h2>
		    <div class="row"> 
		        <div class="form-group col-md-4 col-sm-4 col-xs-12">
		            <label for="login">Korisnik *</label>
		            <input class="form-control" id="login" name="login" value="{{$partner->login}}" type="text" {{$is_is ? 'disabled="disabled"' : ''}}>
		            <div class="error red-dot-error">{{ $errors->first('login') }}</div>
		        </div>

		        <div class="form-group col-md-4 col-sm-4 col-xs-12">
		            <label for="password">Lozinka *</label>
		            <input class="form-control" id="password" name="password" value="{{$partner->password}}" type="password" {{$is_is ? 'disabled="disabled"' : ''}}>
		            <div class="error red-dot-error">{{ $errors->first('password') }}</div>
		        </div>
		    </div>
		    
		    @if(!$is_is)
		    <div class="row"> 
		        <div class="col-md-12 col-sm-12 col-xs-12"> 
		            <button class="submit admin-login">Sačuvaj </button>
		        </div>
		    </div>
		    @endif
		</form>
	</div>
</div>
@endsection