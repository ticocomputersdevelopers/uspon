@extends(($isb2b?'b2b':'dokumenti').'.templates.main')
@section('content')
<div id="main-content" class="kupci-page col-xs-12 no-padding"> 
	<div class="offer relative">
		<div class="flat-box">
			@if($isb2b)
			<div class="row">
				<div class="columns medium-3 col-sm-6 col-xs-12 no-padding">
					<h1 class="inline-block title">Ponude</h1>
				</div>
				<div class="columns medium-3 col-sm-6 col-xs-12 no-padding text-right sm-text-left">
					<a href="{{DokumentiOptions::base_url()}}dokumenti/ponude"><h1 class="btn inline-block">Dokumenti</h1></a>
				</div>
			</div>
			@endif
			<div class="row flex"> 
				<div class="columns medium-3 col-md-4 col-sm-4 col-xs-12 no-padding">  
					<form method="GET" action="{{DokumentiOptions::base_url()}}{{ $isb2b?'b2b/':'' }}dokumenti/ponude" class="columns medium-10 no-padd">
						<div class="search-field flex relative">
							<label class="no-margin">Pretraga</label>
							<input type="text" name="search" value="{{ urldecode($search) }}" placeholder="{{ AdminLanguage::transAdmin('Pretraga') }}..." class="m-input-and-button__input">
							<button class="btn btn-small btn-pon-pred" type="submit"><i class="fa fa-search"></i> </button>
							<!-- <input class="btn btn-primary btn-small" value="Pretraga" type="submit"> <i class="fas fa-search"></i>  -->
						</div> 
					</form>
				</div>

				@if(!$isb2b)
				<div class="columns medium-3">
					<input type="radio" name="vrsta_dokumenta" class="JSVrstaDokumenta" value="ponuda" {{($vrsta_dokumenta == 'ponuda') ? 'checked' : ''}}> <label class="no-margin">Ponude</label>
					<input type="radio" name="vrsta_dokumenta" class="JSVrstaDokumenta" value="predracun" {{($vrsta_dokumenta == 'predracun') ? 'checked' : ''}}> <label class="no-margin">Predračuni</label>
					@if(AdminOptions::gnrl_options(3050))
					<input type="radio" name="vrsta_dokumenta" class="JSVrstaDokumenta" value="racun"> <label class="no-margin">Računi</label>
					<input type="radio" name="vrsta_dokumenta" class="JSVrstaDokumenta" value="avansni_racun"><label class="no-margin">Avansni računi</label>
					@endif
				</div>
				@endif

				<div class="columns medium-2 col-md-4 col-sm-4 col-xs-12 sm-no-padding">
					<div class="m-input-and-button">
						<label class="no-margin">Status</label>&nbsp;
						<select id="JSStatusSearch" class="field-status">
							<option value="all">Svi statusi</option>
							{{ Dokumenti::statusSelect($dokumenti_status_id) }}
						</select>
					</div> 
				</div>

				<div class="columns medium-2 col-md-4 col-sm-4 col-xs-12 no-padding">
					<div class="m-input-and-button">
						<label class="no-margin">Kupac</label>&nbsp;
						<select id="JSParnerSearch" class="field-status">
							<option value="0">Svi kupci</option>
							{{ Dokumenti::partnerSelect($partner_id) }}
						</select>
					</div> 
				</div>

				<div class="columns medium-2">
					<a href="{{DokumentiOptions::base_url()}}{{ $isb2b?'b2b/':'' }}dokumenti/ponuda/0" class="btn btn-primary btn-small new_doc">{{ AdminLanguage::transAdmin('Novi dokument') }}</a>
				</div>
			</div>
		</div>

		<div class="flat-box total"> 
			<div class="row"> 
				<div class="columns medium-12 large-12 table-responsive">
					<label class="summary-label">{{ AdminLanguage::transAdmin('Ukupno') }}: {{ $count }}</label>
					<table class="table table-ponude">
						<tr>
							<th class="JSSort" data-sort_column="broj_dokumenta" data-sort_direction="{{ $sort_column == 'broj_dokumenta' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Broj Dokumenta') }}</th>
							<th class="JSSort" data-sort_column="naziv_partnera" data-sort_direction="{{ $sort_column == 'naziv_partnera' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Kupac') }}</th>
							<th class="JSSort" data-sort_column="datum_ponude" data-sort_direction="{{ $sort_column == 'datum_ponude' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Datum izdavanja') }}</th>
							<th class="JSSort" data-sort_column="vazi_do" data-sort_direction="{{ $sort_column == 'vazi_do' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Važi do') }}</th>
							<th class="JSSort" data-sort_column="iznos" data-sort_direction="{{ $sort_column == 'iznos' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Ukupno') }} (din.)</th>
							<th class="JSSort" data-sort_column="dokumenti_status_id" data-sort_direction="{{ $sort_column == 'dokumenti_status_id' ? ($sort_direction == 'asc' ? 'desc' : 'asc') : 'asc' }}">{{ AdminLanguage::transAdmin('Status') }}</th>
							<th></th>
						</tr>
						@foreach($ponude as $row)
						<tr {{($isb2b AND Session::has('dokumenti_ponuda_id'.Options::server()) AND $row->ponuda_id==Session::get('dokumenti_ponuda_id'.Options::server()))?'style="border: 1px solid red;"':''}}>
							<td>{{ $row->broj_dokumenta }}</td>
							<td>{{ $row->naziv_partnera }}</td>
							<td>{{ Dokumenti::dateInversion($row->datum_ponude) }}</td>
							<td>{{ Dokumenti::dateInversion($row->vazi_do) }}</td>
							<td>{{ number_format($row->iznos,2,",",".") }}</td>
							<td>{{ !is_null($row->status) ? $row->status : 'Bez statusa' }}</td>
							<td><a href="{{DokumentiOptions::base_url()}}{{ $isb2b?'b2b/':'' }}dokumenti/ponuda/{{$row->ponuda_id}}">{{ AdminLanguage::transAdmin('Izmeni') }}</a></td>
						</tr>
						@endforeach

					</table>
					{{ Paginator::make($ponude,$count,$limit)->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection