<?php
use Service\TranslatorService;
use Service\ElasticSearchService;

class AdminSupportController extends Controller {

    //PROIZVODJACI
    function proizvodjac($proizvodjac_id,$jezik_id=1)
    {    
        $seo = AdminSeo::proizvodjac($proizvodjac_id,$jezik_id);

        $data=array(
            "strana"=>'proizvodjac',
            "title"=>$proizvodjac_id != 0 ? AdminSupport::find_proizvodjac($proizvodjac_id, 'naziv') : 'Novi proizvođač',
            "proizvodjac_id"=>$proizvodjac_id,
            "naziv"=> $proizvodjac_id != 0 ? AdminSupport::find_proizvodjac($proizvodjac_id, 'naziv') : null,
            "brend_prikazi"=> $proizvodjac_id != 0 ? AdminSupport::find_proizvodjac($proizvodjac_id, 'brend_prikazi') : 1,
            "rbr"=> $proizvodjac_id != 0 ? AdminSupport::find_proizvodjac($proizvodjac_id, 'rbr') : DB::select("SELECT MAX(proizvodjac_id) AS max FROM proizvodjac")[0]->max + 1,
            "opis"=> $proizvodjac_id != 0 ? AdminSupport::find_proizvodjac($proizvodjac_id, 'opis') : null,
            "keywords"=> $proizvodjac_id != 0 ? AdminSupport::find_proizvodjac($proizvodjac_id, 'keywords') : null,
            "slika"=> $proizvodjac_id != 0 ? AdminSupport::find_proizvodjac($proizvodjac_id, 'slika') : null,
            "produzena_garancija"=> $proizvodjac_id != 0 ? AdminSupport::find_proizvodjac($proizvodjac_id, 'produzena_garancija') : 0,
            "produzena_garancija_slika"=> $proizvodjac_id != 0 ? AdminSupport::find_proizvodjac($proizvodjac_id, 'produzena_garancija_slika') : null,
            "produzena_garancija_link"=> $proizvodjac_id != 0 ? AdminSupport::find_proizvodjac($proizvodjac_id, 'produzena_garancija_link') : null,
            "jezik_id"=>$jezik_id,
            "jezici" => DB::table('jezik')->where('aktivan',1)->get(),
            "seo_title" => $proizvodjac_id != 0 ? $seo->title : '',
            "keywords" => $proizvodjac_id != 0 ? $seo->keywords : '',
            "description" => $proizvodjac_id != 0 ? $seo->description : ''
        );
            
            return View::make('admin/page', $data);
    }

    function proizvodjac_edit()
    {
        $inputs = Input::get();

        if($inputs['proizvodjac_id'] != 0){
            $ignore = '|unique:proizvodjac,naziv,'.$inputs['proizvodjac_id'].',proizvodjac_id';
        }else{
            $ignore = '|unique:proizvodjac,naziv';
        }

        $validator = Validator::make($inputs, array(
            'naziv' => 'required|regex:'.AdminSupport::regex().'|max:100'.$ignore,
            'seo_title' => 'max:60',
            'description' => 'max:320',
            'keywords' => 'max:159',
            'rbr' => 'numeric|digits_between:1,10'));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;
            if($inputs['proizvodjac_id'] == 0){                
                $proizvodjac_id = DB::select("SELECT MAX(proizvodjac_id) AS max FROM proizvodjac")[0]->max + 1;
                $general_data['proizvodjac_id'] = $proizvodjac_id;
            }

            if($general_data['rbr'] == ''){
                $general_data['rbr'] = null;
            }
            unset($general_data['jezik_id']);
            unset($general_data['seo_title']);
            unset($general_data['description']);
            unset($general_data['keywords']);

            $slika = Input::file('slika');
            if(isset($slika)){
                $extension = $slika->getClientOriginalExtension();
                $putanja = $general_data['proizvodjac_id'].'.'.$extension;
                $slika->move('images/manufacturer/', $putanja);
                $general_data['slika'] = 'images/manufacturer/'.$putanja;
            }
            $produzena_garancija_slika = Input::file('produzena_garancija_slika');
            if(isset($produzena_garancija_slika)){
                $extension = $produzena_garancija_slika->getClientOriginalExtension();
                $destination = 'garancija_'.$general_data['proizvodjac_id'].'.'.$extension;
                $produzena_garancija_slika->move('images/', $destination);
                $general_data['produzena_garancija_slika'] = 'images/'.$destination;
            }

            if($inputs['proizvodjac_id'] != 0){
                if(isset($general_data['slika_delete']) && isset($general_data['slika_delete']) == 'on'){
                    $putanja_slika = AdminSupport::find_proizvodjac($general_data['proizvodjac_id'], 'slika');
                    if(isset($putanja_slika)){
                        if(file_exists(trim($putanja_slika))==true){
                            unlink(trim($putanja_slika));
                        }
                    } 
                    $general_data['slika'] = null;               
                }
                unset($general_data['slika_delete']);
                DB::table('proizvodjac')->where('proizvodjac_id',$inputs['proizvodjac_id'])->update($general_data);
                AdminSupport::saveLog('PROIZVODJAC_IZMENI', array($inputs['proizvodjac_id']));
            }else{
                $inputs['proizvodjac_id'] = $proizvodjac_id;
                DB::table('proizvodjac')->insert($general_data);
                AdminSupport::saveLog('PROIZVODJAC_DODAJ', array(DB::table('proizvodjac')->max('proizvodjac_id')));
                DB::statement("SELECT setval('proizvodjac_proizvodjac_id_seq', (SELECT MAX(proizvodjac_id) FROM proizvodjac), FALSE)");
            }

            $query = DB::table('proizvodjac_jezik')->where(array('proizvodjac_id'=>$inputs['proizvodjac_id'], 'jezik_id'=>$inputs['jezik_id']));
            $jezik_data = array('title'=>$inputs['seo_title'],'description'=>$inputs['description'],'keywords'=>$inputs['keywords']);
            if(!is_null($query->first())){
                $query->update($jezik_data);
            }else{
                if($inputs['seo_title'] || $inputs['description'] || $inputs['keywords']){
                    $jezik_data['proizvodjac_id'] = $inputs['proizvodjac_id'];
                    $jezik_data['jezik_id'] = $inputs['jezik_id'];
                    DB::table('proizvodjac_jezik')->insert($jezik_data);

                    if($inputs['jezik_id']==1){
                        foreach(DB::table('jezik')->where('aktivan',1)->where('jezik_id','!=',1)->orderBy('izabrani','desc')->get() as $jezik){
                            $translator = new TranslatorService(DB::table('jezik')->where('jezik_id',1)->pluck('kod'),$jezik->kod);
                            $jezik_data = array('proizvodjac_id' => $inputs['proizvodjac_id'], 'jezik_id' => $jezik->jezik_id, 'title'=>$translator->translate($inputs['seo_title']),'description'=>$translator->translate(html_entity_decode($inputs['description'])),'keywords'=>$translator->translate($inputs['keywords']));

                            DB::table('proizvodjac_jezik')->insert($jezik_data);                 
                        }
                    }
                }
            }

            if(Options::gnrl_options(3055) == 1 && Options::gnrl_options(3056) == 0){
                $elastic = new ElasticSearchService();
                $elastic->updateManufacturers();
                $elastic->updateArticles();
            }

            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/proizvodjac/'.$inputs['proizvodjac_id'].($inputs['jezik_id']==1 ? '' : '/'.$inputs['jezik_id']))->with('message',$message);
        }
    }

    function proizvodjac_delete($proizvodjac_id)
    {   
        $putanja_slika = AdminSupport::find_proizvodjac($proizvodjac_id, 'slika');
        if(isset($putanja_slika)){
            if(file_exists(trim($putanja_slika))==true){
                unlink(trim($putanja_slika));
            }
        }
        DB::table('roba')->where('proizvodjac_id',$proizvodjac_id)->update(array('proizvodjac_id'=>-1));
        DB::statement("update dobavljac_cenovnik set proizvodjac_id = -1 where proizvodjac_id = ".strval($proizvodjac_id)."");
        AdminSupport::saveLog('PROIZVODJAC_OBRISI', array($proizvodjac_id));
        DB::table('partner_proizvodjac')->where('proizvodjac_id',$proizvodjac_id)->delete();
        DB::table('proizvodjac_jezik')->where('proizvodjac_id',$proizvodjac_id)->delete();
        DB::table('proizvodjac')->where('proizvodjac_id',$proizvodjac_id)->delete();

        if(Options::gnrl_options(3055) == 1 && Options::gnrl_options(3056) == 0){
            $elastic = new ElasticSearchService();
            $elastic->updateManufacturers();
            $elastic->updateArticles();
        }

        return Redirect::to(AdminOptions::base_url().'admin/proizvodjac/0')->with('message','Uspešno ste obrisali sadržaj.');
    }

    //TIPOVI
    function tip_artikla($tip_artikla_id)
    {      
        
        $data=array(
            "strana"=>'tip_artikla',
            "title"=>$tip_artikla_id != 0 ? AdminSupport::find_tip_artikla($tip_artikla_id, 'naziv') : 'Novi tip artikla',
            "tip_artikla_id"=>$tip_artikla_id,
            "naziv"=> $tip_artikla_id != 0 ? AdminSupport::find_tip_artikla($tip_artikla_id, 'naziv') : null,
            "active"=> $tip_artikla_id != 0 ? AdminSupport::find_tip_artikla($tip_artikla_id, 'active') : null,
            "prikaz"=> $tip_artikla_id != 0 ? AdminSupport::find_tip_artikla($tip_artikla_id, 'prikaz') : null,
            "rbr"=> $tip_artikla_id != 0 ? AdminSupport::find_tip_artikla($tip_artikla_id, 'rbr') : null
        );
            
            return View::make('admin/page', $data);
    }

    function tip_artikla_edit()
    {   
        $inputs = Input::get();

        $validator = Validator::make($inputs, array('naziv' => 'required|max:100', 'rbr' => 'numeric|digits_between:1,10'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/tip/'.$inputs['tip_artikla_id'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['tip_artikla_id'] == 0){                
                $general_data['tip_artikla_id'] = DB::select("SELECT MAX(tip_artikla_id) AS max FROM tip_artikla")[0]->max + 1;
            }
            if($general_data['rbr'] == ''){
                $general_data['rbr'] = DB::select("SELECT MAX(rbr) AS max FROM tip_artikla")[0]->max + 1;
            }

            if($inputs['tip_artikla_id'] != 0){
                DB::table('tip_artikla')->where('tip_artikla_id',$inputs['tip_artikla_id'])->update($general_data);
                AdminSupport::saveLog('TIP_ARTIKLA_IZMENI', array($general_data['tip_artikla_id']));

            }else{
                DB::table('tip_artikla')->insert($general_data);
                AdminSupport::saveLog('TIP_ARTIKLA_DODAJ', array(DB::table('tip_artikla')->max('tip_artikla_id')));
            }
            AdminTranslator::addToTranslatorAll($general_data['naziv']);

            AdminTranslator::addToTranslatorAll($general_data['naziv']);

            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/tip/'.$general_data['tip_artikla_id'])->with('message',$message);
        }
    }

    function tip_artikla_delete($tip_artikla_id)
    {   

        DB::table('roba')->where('tip_cene',$tip_artikla_id)->update(array('tip_cene'=>null));
        AdminSupport::saveLog('TIP_ARTIKLA_OBRISI', array($tip_artikla_id));
        DB::table('tip_artikla')->where('tip_artikla_id',$tip_artikla_id)->delete();
        
        return Redirect::to(AdminOptions::base_url().'admin/tip/0')->with('message','Uspešno ste obrisali sadržaj.');
    }
//Kupovina na rate
    function rata($rata_id)
    {      
        $broj_rata = DB::table('kupovina_na_rate')->orderBy('br_meseca','asc')->get();


        $data=array(
            "strana"=>'kupovina_na_rate',
            'title'=>'Kupovina na rate',
            'broj_rata'=>$broj_rata

        );
            
            return View::make('admin/page', $data);
    }

    function rata_edit()
    {   
        $inputs = Input::get();

        $validator = Validator::make($inputs, array('kamata' => 'numeric', 'br_meseca' => 'numeric'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/kupovina_na_rate/'.$inputs['rata_id'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['rata_id'] == 0){                
                $general_data['rata_id'] = DB::select("SELECT MAX(rata_id) AS max FROM kupovina_na_rate")[0]->max + 1;
            }

            if($inputs['rata_id'] != 0){
                DB::table('kupovina_na_rate')->where('rata_id',$inputs['rata_id'])->update($general_data);
                AdminSupport::saveLog('Kupovina na rate Edit', array($general_data['rata_id']));

            }else{
                DB::table('kupovina_na_rate')->insert($general_data);
              
            }
            AdminTranslator::addToTranslatorAll($general_data['br_meseca']);

            AdminTranslator::addToTranslatorAll($general_data['br_meseca']);

            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/kupovina_na_rate/'.$general_data['rata_id'])->with('message',$message);
        }
    }

    function rata_delete($rata_id)
    {   

        DB::table('kupovina_na_rate')->where('rata_id',$rata_id)->delete();
        AdminSupport::saveLog('RATA_OBRISI', array($rata_id));
        
        return Redirect::to(AdminOptions::base_url().'admin/kupovina_na_rate/0')->with('message','Uspešno ste obrisali sadržaj.');
    }
    //  jedinice mere
    function jedinica_mere($jedinica_mere_id)
    {      
        
        $data=array(
            "strana"=>'jedinica_mere',
            "title"=>$jedinica_mere_id != 0 ? AdminSupport::find_jedinica_mere($jedinica_mere_id, 'naziv') : 'Nova jedinica mere',
            "jedinica_mere_id"=>$jedinica_mere_id,
            "naziv"=> $jedinica_mere_id != 0 ? AdminSupport::find_jedinica_mere($jedinica_mere_id, 'naziv') : null
        );
            
            return View::make('admin/page', $data);
    }

    function jedinica_mere_edit()
    {   
        $inputs = Input::get();

        $validator = Validator::make($inputs, array('naziv' => 'required|regex:'.AdminSupport::regex().'|max:100'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/jedinica-mere/'.$inputs['jedinica_mere_id'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['jedinica_mere_id'] == 0){                
                $general_data['jedinica_mere_id'] = DB::select("SELECT MAX(jedinica_mere_id) AS max FROM jedinica_mere")[0]->max + 1;
            }

            if($inputs['jedinica_mere_id'] != 0){
                DB::table('jedinica_mere')->where('jedinica_mere_id',$inputs['jedinica_mere_id'])->update($general_data);
                AdminSupport::saveLog('JEDINICA_MERE_IZMENI', array($general_data['jedinica_mere_id']));
            }else{
                DB::table('jedinica_mere')->insert($general_data);
                AdminSupport::saveLog('JEDINICA_MERE_DODAJ', array(DB::table('jedinica_mere')->max('jedinica_mere_id')));
            }
            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/jedinica-mere/'.$general_data['jedinica_mere_id'])->with('message',$message);
        }
    }

    function jedinica_mere_delete($jedinica_mere_id)
    {
        DB::table('roba')->where('jedinica_mere_id',$jedinica_mere_id)->update(array('jedinica_mere_id'=>-1));
        AdminSupport::saveLog('JEDINICA_MERE_OBRISI', array($jedinica_mere_id));
        DB::table('jedinica_mere')->where('jedinica_mere_id',$jedinica_mere_id)->delete();
        
        return Redirect::to(AdminOptions::base_url().'admin/jedinica-mere/0')->with('message','Uspešno ste obrisali sadržaj.');
    }

    //poreske stope
    function poreske_stope($tarifna_grupa_id=null)
    {      
        $data=array(
            "strana"=>'poreske_stope',
            "title"=>$tarifna_grupa_id != null ? AdminSupport::find_tarifna_grupa($tarifna_grupa_id, 'naziv') : 'Nova poreska stopa',
            "tarifna_grupa_id"=>$tarifna_grupa_id,
            "naziv"=> $tarifna_grupa_id != null ? AdminSupport::find_tarifna_grupa($tarifna_grupa_id, 'naziv') : null,
            "porez"=> $tarifna_grupa_id != null ? AdminSupport::find_tarifna_grupa($tarifna_grupa_id, 'porez') : null,
            "sifra_connect"=> $tarifna_grupa_id != null ? AdminSupport::find_tarifna_grupa($tarifna_grupa_id, 'sifra_connect') : null,
            "tip"=> $tarifna_grupa_id != null ? AdminSupport::find_tarifna_grupa($tarifna_grupa_id, 'tip') : null,
            "active"=>$tarifna_grupa_id != null ? AdminSupport::find_tarifna_grupa($tarifna_grupa_id, 'active') : null,
            "default_tarifna_grupa"=>$tarifna_grupa_id != null ? AdminSupport::find_tarifna_grupa($tarifna_grupa_id, 'default_tarifna_grupa') : null
        );
   
        return View::make('admin/page', $data);
    }

   

    function poreske_stope_edit()
    {   
        $inputs = Input::get();

        if($inputs['tarifna_grupa_id'] != 0){
            $ignore = '|unique:tarifna_grupa,naziv,'.$inputs['tarifna_grupa_id'].',tarifna_grupa_id';
        }else{
            $ignore = '|unique:tarifna_grupa,naziv';
        }

        $validator = Validator::make($inputs, array(
            'naziv' => 'required|max:100'.$ignore, 
            'porez' => 'required|digits_between:1,10', 
            // 'sifra_connect' => 'digits_between:1,10', 
             'tip' => 'between: 2, 25|regex:'.Support::regex().''
            ));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/poreske-stope/'.$inputs['tarifna_grupa_id'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['tarifna_grupa_id'] == 0){                
                $general_data['tarifna_grupa_id'] = DB::select("SELECT MAX(tarifna_grupa_id) AS max FROM tarifna_grupa")[0]->max + 1;
            }

            $general_data['sifra'] = sprintf("%02d", $general_data['porez']);
            // if($general_data['sifra_connect'] == ''){
            //     $general_data['sifra_connect'] = 0;
            // }

            if(isset($general_data['active'])){
                $general_data['active'] = 1;
            }else{
                $general_data['active'] = 0; 
            }

            if(isset($general_data['default_tarifna_grupa'])){
                DB::table('tarifna_grupa')->update(['default_tarifna_grupa' => 0]);
                $general_data['default_tarifna_grupa'] = 1;
            }else{
                $general_data['default_tarifna_grupa'] = 0; 
            }


            if($inputs['tarifna_grupa_id'] != 0){
                DB::table('tarifna_grupa')->where('tarifna_grupa_id',$inputs['tarifna_grupa_id'])->update($general_data);
                AdminSupport::saveLog('PORESKA_STOPA_IZMENI', array($inputs['tarifna_grupa_id']));
            }else{
                DB::table('tarifna_grupa')->insert($general_data);
                AdminSupport::saveLog('PORESKA_STOPA_DODAJ', array(DB::table('tarifna_grupa')->max('tarifna_grupa_id')));
            }

            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/poreske-stope/'.$general_data['tarifna_grupa_id'])->with('message',$message);
        }
    }

  

    function poreske_stope_delete($tarifna_grupa_id)
    {   
        $check_query_self = DB::table('tarifna_grupa')->where('tarifna_grupa_id','!=',-1)->where('tarifna_grupa_id',$tarifna_grupa_id);
        $checkself = $check_query_self->count();
        $check_query_web_b2c_korpa_stavka = DB::table('web_b2c_korpa_stavka')->where('tarifna_grupa_id','!=',-1)->where('tarifna_grupa_id',$tarifna_grupa_id);
        $checkweb_b2c_korpa_stavka = $check_query_web_b2c_korpa_stavka->count();
        $check_query_web_b2c_narudzbina_stavka = DB::table('web_b2c_narudzbina_stavka')->where('tarifna_grupa_id','!=',-1)->where('tarifna_grupa_id',$tarifna_grupa_id);
        $checkweb_b2c_narudzbina_stavka = $check_query_web_b2c_narudzbina_stavka->count();
        $check_query_web_b2b_korpa_stavka = DB::table('web_b2b_korpa_stavka')->where('tarifna_grupa_id','!=',-1)->where('tarifna_grupa_id',$tarifna_grupa_id);
        $checkweb_b2b_korpa_stavka = $check_query_web_b2b_korpa_stavka->count();
        $check_query_web_b2b_narudzbina_stavka = DB::table('web_b2b_narudzbina_stavka')->where('tarifna_grupa_id','!=',-1)->where('tarifna_grupa_id',$tarifna_grupa_id);
        $checkweb_b2b_narudzbina_stavka = $check_query_web_b2b_narudzbina_stavka->count();
        $check_query_roba = DB::table('roba')->where('tarifna_grupa_id','!=',-1)->where('tarifna_grupa_id',$tarifna_grupa_id);
        $checkroba = $check_query_roba->count();
        $check_query_dobavljac_cenovnik = DB::table('dobavljac_cenovnik')->where('tarifna_grupa_id','!=',-1)->where('tarifna_grupa_id',$tarifna_grupa_id);
        $checkdobavljac_cenovnik = $check_query_dobavljac_cenovnik->count();
        if($checkself == 0){
            return Redirect::to(AdminOptions::base_url().'admin/poreske-stope/'.$tarifna_grupa_id)->with('message','Ova tarifna grupa ne postoji!');
        }
        if($checkweb_b2c_korpa_stavka > 0){
            return Redirect::to(AdminOptions::base_url().'admin/poreske-stope/'.$tarifna_grupa_id)->with('message','Ova tarifna grupa je vezana za korpu nekog od korisnika!');
        }
        if($checkweb_b2c_narudzbina_stavka > 0){
            return Redirect::to(AdminOptions::base_url().'admin/poreske-stope/'.$tarifna_grupa_id)->with('message','Ova tarifna grupa je vezana za B2B naruđzbinu nokog od korisnika!');
        }
        if($checkweb_b2b_korpa_stavka > 0){
            return Redirect::to(AdminOptions::base_url().'admin/poreske-stope/'.$tarifna_grupa_id)->with('message','Ova tarifna grupa je vezana za B2B korpu nokog od korisnika!');
        }
        if($checkweb_b2b_narudzbina_stavka > 0){
            return Redirect::to(AdminOptions::base_url().'admin/poreske-stope/'.$tarifna_grupa_id)->with('message','Ova tarifna grupa je vezana za naruđzbinu nokog od korisnika!');
        }
        if($checkroba > 0){
            return Redirect::to(AdminOptions::base_url().'admin/poreske-stope/'.$tarifna_grupa_id)->with('message','Ova tarifna grupa je vezana za artikle!');
        }
        if($checkdobavljac_cenovnik > 0){
            return Redirect::to(AdminOptions::base_url().'admin/poreske-stope/'.$tarifna_grupa_id)->with('message','Ova tarifna grupa je vezana za artikle dobavljača!');
        }
        AdminSupport::saveLog('PORESKA_STOPA_OBRISI', array($tarifna_grupa_id));

        DB::table('tarifna_grupa')->where('tarifna_grupa_id',$tarifna_grupa_id)->delete();
        
        return Redirect::to(AdminOptions::base_url().'admin/poreske-stope')->with('message','Uspešno ste obrisali sadržaj.');
    } 


    // stanja artikla
    function stanje_artikla($roba_flag_cene_id=null)
    {      
        
        $data=array(
            "strana"=>'stanje_artikla',
            "title"=>$roba_flag_cene_id != null ? AdminSupport::find_flag_cene($roba_flag_cene_id, 'naziv') : 'Novo stanje artikla',
            "roba_flag_cene_id"=>$roba_flag_cene_id,
            "naziv"=> $roba_flag_cene_id != null ? AdminSupport::find_flag_cene($roba_flag_cene_id, 'naziv') : null,
            "prikaz_cene"=> $roba_flag_cene_id != null ? AdminSupport::find_flag_cene($roba_flag_cene_id, 'prikaz_cene') : null,
            "selected"=> $roba_flag_cene_id != null ? AdminSupport::find_flag_cene($roba_flag_cene_id, 'selected') : null,
            "b2b_selected"=> $roba_flag_cene_id != null ? AdminSupport::find_flag_cene($roba_flag_cene_id, 'b2b_selected') : null
        );
        return View::make('admin/page', $data);
    }

    function stanje_artikla_edit()
    {   
        $inputs = Input::get();

        $validator = Validator::make($inputs, array('naziv' => 'required|regex:'.AdminSupport::regex().'|max:30'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/stanje-artikla/'.$inputs['roba_flag_cene_id'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['roba_flag_cene_id'] == 0){                
                $general_data['roba_flag_cene_id'] = DB::select("SELECT MAX(roba_flag_cene_id) AS max FROM roba_flag_cene")[0]->max + 1;
            }

            if($inputs['roba_flag_cene_id'] != 0){
                DB::table('roba_flag_cene')->where('roba_flag_cene_id',$inputs['roba_flag_cene_id'])->update($general_data);
                AdminSupport::saveLog('STANJE_ARTIKLA_IZMENI', array($inputs['roba_flag_cene_id']));
            }else{
                DB::table('roba_flag_cene')->insert($general_data);
                AdminSupport::saveLog('STANJE_ARTIKLA_DODAJ', array(DB::table('roba_flag_cene')->max('roba_flag_cene_id')));
            }
            AdminTranslator::addToTranslatorAll($general_data['naziv']);

            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/stanje-artikla/'.$general_data['roba_flag_cene_id'])->with('message',$message);
        }
    }

    function stanje_artikla_delete($roba_flag_cene_id)
    {   
        if($roba_flag_cene_id != 1){        
            DB::table('roba')->where('roba_flag_cene_id',$roba_flag_cene_id)->update(array('roba_flag_cene_id'=>1));
        AdminSupport::saveLog('STANJE_ARTIKLA_OBRISI', array($roba_flag_cene_id));
            DB::table('roba_flag_cene')->where('roba_flag_cene_id',$roba_flag_cene_id)->delete();
        }
        
        return Redirect::to(AdminOptions::base_url().'admin/stanje-artikla')->with('message','Uspešno ste obrisali sadržaj.');
    }

    function status_narudzbine($narudzbina_status_id=null)
    {      
        
        $data=array(
            "strana"=>'status_narudzbine',
            "title"=>$narudzbina_status_id != null ? AdminSupport::find_status_narudzbine($narudzbina_status_id, 'naziv') : 'Novi status narudzbine',
            "narudzbina_status_id"=>$narudzbina_status_id,
            "naziv"=> $narudzbina_status_id != null ? AdminSupport::find_status_narudzbine($narudzbina_status_id, 'naziv') : null,
            "selected"=> $narudzbina_status_id != null ? AdminSupport::find_status_narudzbine($narudzbina_status_id, 'selected') : null
        );
        return View::make('admin/page', $data);
    }

    function status_narudzbine_edit()
    {   
        $inputs = Input::get();

        $validator = Validator::make($inputs, array('naziv' => 'required|regex:'.AdminSupport::regex().'|max:30'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/status_narudzbine/'.$inputs['narudzbina_status_id'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['narudzbina_status_id'] == 0){                
                $general_data['narudzbina_status_id'] = DB::select("SELECT MAX(narudzbina_status_id) AS max FROM narudzbina_status")[0]->max + 1;
            }

            if($inputs['narudzbina_status_id'] != 0){
                DB::table('narudzbina_status')->where('narudzbina_status_id',$inputs['narudzbina_status_id'])->update($general_data);
                AdminSupport::saveLog('STATUS_NARUDZBINE_IZMENI', array($inputs['narudzbina_status_id']));
            }else{
                DB::table('narudzbina_status')->insert($general_data);
                AdminSupport::saveLog('STATUS_NARUDZBINE_DODAJ', array(DB::table('narudzbina_status')->max('narudzbina_status_id')));
            }

            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/status_narudzbine/'.$general_data['narudzbina_status_id'])->with('message',$message);
        }
    }

    function status_narudzbine_delete($narudzbina_status_id)
    {   
        if($narudzbina_status_id != 1){        
            
        AdminSupport::saveLog('STATUS_NARUDZBINE_OBRISI', array($narudzbina_status_id));
            DB::table('narudzbina_status')->where('narudzbina_status_id',$narudzbina_status_id)->delete();
        }
        
        return Redirect::to(AdminOptions::base_url().'admin/status_narudzbine')->with('message','Uspešno ste obrisali sadržaj.');
    }

    function kurirska_sluzba($posta_slanje_id=null)
    {      
        
        $data=array(
            "strana"=>'kurirska_sluzba',
            "title"=>$posta_slanje_id != null ? AdminSupport::find_kurirska_sluzba($posta_slanje_id, 'naziv') : 'Nova kurirska služba',
            "posta_slanje_id"=>$posta_slanje_id,
            "naziv"=> $posta_slanje_id != null ? AdminSupport::find_kurirska_sluzba($posta_slanje_id, 'naziv') : null,
            "aktivna"=> $posta_slanje_id != null ? AdminSupport::find_kurirska_sluzba($posta_slanje_id, 'aktivna') : null,
            "difolt"=>$posta_slanje_id != null ? AdminSupport::find_kurirska_sluzba($posta_slanje_id, 'difolt') : null,
            "api_aktivna"=>$posta_slanje_id != null ? AdminSupport::find_kurirska_sluzba($posta_slanje_id, 'api_aktivna') : null,
            "nasa_sifra"=>$posta_slanje_id != null ? AdminSupport::find_kurirska_sluzba($posta_slanje_id, 'nasa_sifra') : null
        );
        return View::make('admin/page', $data);
    }

    function kurirska_sluzba_edit()
    {   
        $inputs = Input::get();

        $validator = Validator::make($inputs, array('naziv' => 'required|regex:'.AdminSupport::regex().'|max:30'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/kurirska_sluzba/'.$inputs['posta_slanje_id'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if(isset($general_data['difolt'])){
                DB::table('posta_slanje')->update(['difolt' => 0]);
                $general_data['difolt'] = 1;
            }else{
                $general_data['difolt'] = 0; 
            }
            if(isset($general_data['api_aktivna'])){
                $general_data['api_aktivna'] = 1;
            }else{
                $general_data['api_aktivna'] = 0; 
            }

            if($inputs['posta_slanje_id'] == 0){                
                $general_data['posta_slanje_id'] = DB::select("SELECT MAX(posta_slanje_id) AS max FROM posta_slanje")[0]->max + 1;
                DB::table('posta_slanje')->insert($general_data);
                AdminSupport::saveLog('KURIRSKA_SLUZBA_DODAJ', array(DB::table('posta_slanje')->max('posta_slanje_id')));
            }else{
                DB::table('posta_slanje')->where('posta_slanje_id',$inputs['posta_slanje_id'])->update($general_data);
                AdminSupport::saveLog('KURIRSKA_SLUZBA_IZMENI', array($inputs['posta_slanje_id']));
            }

            return Redirect::to(AdminOptions::base_url().'admin/kurirska_sluzba/'.$general_data['posta_slanje_id'])->with('message','Uspešno ste sačuvali podatke');
        }
    }

    function kurirska_sluzba_delete($posta_slanje_id)
    {   
        if($posta_slanje_id != 1){        
        AdminSupport::saveLog('KURIRSKA_SLUZBA_OBRISI', array($posta_slanje_id));
            DB::table('posta_slanje')->where('posta_slanje_id',$posta_slanje_id)->delete();
        }
        
        return Redirect::to(AdminOptions::base_url().'admin/kurirska_sluzba');
    }



    // konfigurator
    function konfigurator($konfigurator_id=null)
    {      

        if($konfigurator_id!=null){    
            $konfigurator = DB::table('konfigurator')->where('konfigurator_id',$konfigurator_id)->first();
        }
        $data=array(
            "strana"=>'konfigurator',
            "title"=>$konfigurator_id != null ? 'Konfigurator: '.$konfigurator->naziv : 'Konfigurator',
            "konfiguratori" => DB::select("SELECT * FROM konfigurator ORDER BY konfigurator_id ASC"),
            "konfigurator_id"=> $konfigurator_id != null ? $konfigurator_id : null,
            "naziv"=> $konfigurator_id != null ? $konfigurator->naziv : null,
            "dozvoljen"=> $konfigurator_id != null ? $konfigurator->dozvoljen : 1,
            "konfigurator_grupe" => $konfigurator_id != null ? DB::table('konfigurator_grupe')->where('konfigurator_id',$konfigurator_id)->orderBy('rbr','asc')->get() : array()
        );

        return View::make('admin/page', $data);
    }

    function konfigurator_edit()
    { 
        $inputs = Input::get();

        $validator = Validator::make($inputs, array('naziv' => 'required|regex:'.AdminSupport::regex().'|max:40'));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
            if($inputs['konfigurator_id'] == ''){
                $konfigurator_id = DB::select("SELECT MAX(konfigurator_id) AS max FROM konfigurator")[0]->max + 1;
                $inputs['konfigurator_id'] = $konfigurator_id;
                DB::table('konfigurator')->insert($inputs);
                AdminSupport::saveLog('KONFIGURATOR_DODAJ', array(DB::table('konfigurator')->max('konfigurator_id')));

            }else{
                $konfigurator_id = $inputs['konfigurator_id'];
                unset($inputs['konfigurator_id']);
                DB::table('konfigurator')->where('konfigurator_id',$konfigurator_id)->update($inputs);
                AdminSupport::saveLog('KONFIGURATOR_IZMENI', array($konfigurator_id));
            }

            AdminTranslator::addToTranslatorAll($inputs['naziv']);

            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/konfigurator/'.$konfigurator_id)->with('message',$message);
        }
    }
    function konfigurator_delete($konfigurator_id)
    {      
        AdminSupport::saveLog('KONFIGURATOR_OBRISI', array($konfigurator_id));
        DB::table('konfigurator_grupe')->where('konfigurator_id',$konfigurator_id)->delete();
        DB::table('konfigurator')->where('konfigurator_id',$konfigurator_id)->delete();

        return Redirect::to(AdminOptions::base_url().'admin/konfigurator')->with('message','Uspešno ste obrisali sadržaj.');
    }

    // mesta
    function mesto($mesto_id=null)
    {      

        if($mesto_id!=null){    
            $mesto = DB::table('mesto')->where('mesto_id',$mesto_id)->first();
        }
        $data=array(
            "strana"=>'mesto',
            "title"=>$mesto_id != null ? 'Mesto: '.$mesto->mesto : 'Mesto',
            "mesta" => DB::select("SELECT * FROM mesto WHERE mesto_id <> -1 ORDER BY mesto_id ASC"),
            "mesto_id"=> $mesto_id != null ? $mesto_id : null,
            "mesto"=> $mesto_id != null ? $mesto->mesto : null,
            "ptt"=> $mesto_id != null ? $mesto->ptt : null
        );

        return View::make('admin/page', $data);
    }

    function mesto_edit()
    {      
        $inputs = Input::get();

        $mesto_unique = '';
        $ptt_unique = '';
        if($inputs['mesto_id'] == ''){
            $mesto_unique = '|unique:mesto,mesto';
            $ptt_unique = '|unique:mesto,ptt';
        }
        $validator = Validator::make($inputs, array('mesto' => 'required|regex:'.AdminSupport::regex().'|max:50'.$mesto_unique,'ptt' => 'required|numeric|digits_between:1,6'.$ptt_unique));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
            if($inputs['mesto_id'] == ''){
                $mesto_id = DB::select("SELECT MAX(mesto_id) AS max FROM mesto")[0]->max + 1;
                $inputs['mesto_id'] = $mesto_id;
                $old_value = DB::table('mesto')->max('mesto_id');
                DB::table('mesto')->insert($inputs);
                $new_value = DB::table('mesto')->max('mesto_id');
                AdminSupport::saveLog('MESTO_DODAJ', range($old_value+1, $new_value));

            }else{
                $mesto_id = $inputs['mesto_id'];
                unset($inputs['mesto_id']);
                DB::table('mesto')->where('mesto_id',$mesto_id)->update($inputs);
                AdminSupport::saveLog('MESTO_IZMENI', $mesto_id);
            }
            return Redirect::to(AdminOptions::base_url().'admin/mesto/'.$mesto_id);
        }
    }

    function mesto_delete($mesto_id)
    {
        if(DB::table('web_kupac')->where('mesto_id',$mesto_id)->count() > 0 && DB::table('partner')->where('mesto_id',$mesto_id)->count() > 0){
            return Redirect::to(AdminOptions::base_url().'admin/mesto/'.$mesto_id)->with('message',true);
        }else{
        AdminSupport::saveLog('MESTO_OBRISI', array($mesto_id));
            DB::table('mesto')->where('mesto_id',$mesto_id)->delete();
        }

        return Redirect::to(AdminOptions::base_url().'admin/mesto');
    }

    // KOMENTARI
    function komentari()
    {      
        
        $data=array(
            "strana"=>'komentari',
            "title"=> 'Komentari',
            "comments"=> AdminSupport::allComments()
        );
            
        return View::make('admin/page', $data);
    }

    function komentar($solution,$id) {
        $data=array(
            "strana"=>'komentar',
            "title"=> 'Komentar',
            "comment"=> AdminSupport::getComment($id,$solution),
            "solution"=> $solution
        );
            
        return View::make('admin/page', $data);
    }

    function deleteComment($id,$solution) {
        AdminSupport::saveLog('KOMENTAR_OBRISI', array($id));
        if($solution == 'b2c'){
        DB::table('web_b2c_komentari')->where('web_b2c_komentar_id', $id)->delete();
        }else{
        DB::table('web_b2b_komentari')->where('web_b2b_komentar_id', $id)->delete();    
        }
        return Redirect::to(AdminOptions::base_url().'admin/komentari')->with('message','Uspešno ste obrisali sadržaj.');
    }

    function updateComment($solution){

        $inputs = Input::get();

        if(isset($inputs['komentar_odobren'])){
            $odobreno = 1; 
        } else {
            $odobreno = 0;
        }

        if(isset($inputs['odgovoreno'])){
            $odgovoreno = 1; 
        } else {
            $odgovoreno = 0;
        }

        $validator = Validator::make($inputs, array());
        if($validator->fails()){
            /*return Redirect::to(AdminOptions::base_url().'admin/komentari/'.$inputs['web_b2c_komentar_id'])->withInput()->withErrors($validator->messages());*/
            return 'error';
        }else{
            if($solution == 'b2c'){
                $odobravanje = DB::table('web_b2c_komentari')->where('web_b2c_komentar_id', $inputs['web_b2c_komentar_id'])->pluck('komentar_odobren');
                DB::table('web_b2c_komentari')->where('web_b2c_komentar_id', $inputs['web_b2c_komentar_id'])->update(array('ime_osobe'=>$inputs['ime_osobe'], 'pitanje'=>$inputs['pitanje'], 'odgovor'=>$inputs['odgovor'], 'komentar_odobren'=>$odobreno, 'odgovoreno'=>$odgovoreno, 'ocena'=>$inputs['ocena']));
                
                $message='Uspešno ste sačuvali podatke.';

                if($odobravanje == $odobreno){
                    AdminSupport::saveLog('KOMENTAR_IZMENI', array($inputs['web_b2c_komentar_id']));
                }else{
                    AdminSupport::saveLog('KOMENTAR_IZMENI_ODOBRAVANJE', array($inputs['web_b2c_komentar_id']));
                }
            }else{
                $odobravanje = DB::table('web_b2b_komentari')->where('web_b2b_komentar_id', $inputs['web_b2b_komentar_id'])->pluck('komentar_odobren');
                DB::table('web_b2b_komentari')->where('web_b2b_komentar_id', $inputs['web_b2b_komentar_id'])->update(array('ime_osobe'=>$inputs['ime_osobe'], 'pitanje'=>$inputs['pitanje'], 'odgovor'=>$inputs['odgovor'], 'komentar_odobren'=>$odobreno, 'odgovoreno'=>$odgovoreno, 'ocena'=>$inputs['ocena']));
                
                $message='Uspešno ste sačuvali podatke.';

                if($odobravanje == $odobreno){
                    AdminSupport::saveLog('KOMENTAR_IZMENI', array($inputs['web_b2b_komentar_id']));
                }else{
                    AdminSupport::saveLog('KOMENTAR_IZMENI_ODOBRAVANJE', array($inputs['web_b2b_komentar_id']));
                } 
            }
            return Redirect::to(AdminOptions::base_url().'admin/komentari')->with('message',$message);
        }
    }

    function bodovi($web_kupac_id=null)
        {              
        $data=array(
            "strana"=>'bodovi',
            "title"=>'Bodovi korisnika',
            "web_kupac_id"=>$web_kupac_id,
            "ime"=> $web_kupac_id != null ? AdminSupport::find_kupac($web_kupac_id, 'ime') : null,
            "prezime"=> $web_kupac_id != null ? AdminSupport::find_kupac($web_kupac_id, 'prezime') : null,
            "email"=>$web_kupac_id != null ? AdminSupport::find_kupac($web_kupac_id, 'email') : null,
            "telefon"=>$web_kupac_id != null ? AdminSupport::find_kupac($web_kupac_id, 'telefon') : null,
            "adresa"=>$web_kupac_id != null ? AdminSupport::find_kupac($web_kupac_id, 'adresa') : null,
            "bodovi"=>$web_kupac_id != null ? AdminSupport::find_kupac($web_kupac_id, 'bodovi') : null
        );
        return View::make('admin/page', $data);
    }

    function bodovi_edit()
    {   
        $inputs = Input::get();

        $validator = Validator::make($inputs, array(
            'ime' => 'required|regex:'.AdminSupport::regex().'|max:30',
            'bodovi'=>'numeric|digits_between:1,3'    ));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/bodovi/'.$inputs['web_kupac_id'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['bodovi'] <= 100){
                DB::table('web_kupac')->where('web_kupac_id',$inputs['web_kupac_id'])->update($general_data);
            }else{
            return Redirect::to(AdminOptions::base_url().'admin/bodovi/'.$general_data['web_kupac_id'])->with('message','Maksimalni broj bodova je 100');
            }
        }

            AdminSupport::saveLog('Bodovi, INSERT/EDIT web_kupac_id -> '.$general_data['web_kupac_id']);
            return Redirect::to(AdminOptions::base_url().'admin/bodovi/'.$general_data['web_kupac_id'])->with('message','Uspešno ste sačuvali podatke');
    }
     // Katalog
    function katalog($katalog_id)
    {      
        $katalog_vrsta_id =AdminSupport::getKatalogVrsta(true);
        $data=array(
            "strana"=>'katalog',
            "title"=>$katalog_id != 0 ? AdminSupport::find_katalog($katalog_id, 'naziv') : 'Novi katalog',
            "katalog_id"=>$katalog_id,
            "naziv"=> $katalog_id != 0 ? AdminSupport::find_katalog($katalog_id, 'naziv') : null,
            "vazi_od"=> $katalog_id != 0 ? AdminSupport::find_katalog($katalog_id, 'vazi_od') : null,
            "sort"=> $katalog_id != 0 ? AdminSupport::find_katalog($katalog_id, 'sort') : null,
            "katalog_vrsta_id"=> $katalog_vrsta_id,
            "aktivan"=> $katalog_id != 0 ? AdminSupport::find_katalog($katalog_id, 'aktivan') : null
           
        );
            
            return View::make('admin/page', $data);
    }
     function katalog_edit()
       {   
        $inputs = Input::get();

        $validator = Validator::make($inputs, array('naziv' => 'required|max:20'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/katalog/'.$inputs['katalog_id'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['katalog_id'] == 0){                
                $general_data['katalog_id'] = DB::select("SELECT MAX(katalog_id) AS max FROM katalog")[0]->max + 1;
            }
            if($general_data['vazi_od'] == ''){
                $general_data['vazi_od'] = null;
            }
            
            if($inputs['katalog_id'] != 0){
                DB::table('katalog')->where('katalog_id',$inputs['katalog_id'])->update($general_data);
                AdminSupport::saveLog('katalog_IZMENI', array($general_data['katalog_id']));
            }else{
                DB::table('katalog')->insert($general_data);
                AdminSupport::saveLog('katalog_DODAJ', array(DB::table('katalog')->max('katalog_id')));
            }



            AdminTranslator::addToTranslatorAll($general_data['naziv']);
            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/katalog/'.$general_data['katalog_id'])->with('message',$message);
        }
        }
    // LABELE
    function labela_artikla($labela_artikla_id)
    {      
        
        $data=array(
            "strana"=>'labela_artikla',
            "title"=>$labela_artikla_id != 0 ? AdminSupport::find_labela_artikla($labela_artikla_id, 'naziv') : 'Nova labela artikla',
            "labela_artikla_id"=>$labela_artikla_id,
            "naziv"=> $labela_artikla_id != 0 ? AdminSupport::find_labela_artikla($labela_artikla_id, 'naziv') : null,
            "active"=> $labela_artikla_id != 0 ? AdminSupport::find_labela_artikla($labela_artikla_id, 'active') : null,
            "slika_desno"=> $labela_artikla_id != 0 ? AdminSupport::find_labela_artikla($labela_artikla_id, 'slika_desno') : null,
            "slika_levo"=> $labela_artikla_id != 0 ? AdminSupport::find_labela_artikla($labela_artikla_id, 'slika_levo') : null,
            "rbr"=> $labela_artikla_id != 0 ? AdminSupport::find_labela_artikla($labela_artikla_id, 'rbr') : null
        );
            
            return View::make('admin/page', $data);
    }

    function labela_artikla_edit()
       {   
        $inputs = Input::get();

        $validator = Validator::make($inputs, array('naziv' => 'required|max:100', 'rbr' => 'numeric|digits_between:1,10'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/labela/'.$inputs['labela_artikla_id'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['labela_artikla_id'] == 0){                
                $general_data['labela_artikla_id'] = DB::select("SELECT MAX(labela_artikla_id) AS max FROM labela_artikla")[0]->max + 1;
            }
            if($general_data['rbr'] == ''){
                $general_data['rbr'] = DB::select("SELECT MAX(rbr) AS max FROM labela_artikla")[0]->max + 1;
            }         
            
            $slika_desno = Input::file('slika_desno');
            if(isset($slika_desno)){
                $extension = $slika_desno->getClientOriginalExtension();
                $putanja = $general_data['labela_artikla_id'].'.'.'jpg';
                $slika_desno->move('images/labels/', $putanja);
                $general_data['slika_desno'] = 'images/labels/'.$putanja;
            }            
            $slika_levo = Input::file('slika_levo');
            if(isset($slika_levo)){
                $extension = $slika_levo->getClientOriginalExtension();
                $putanja = $general_data['labela_artikla_id'].'.'.'png';
                $slika_levo->move('images/labels/', $putanja);
                $general_data['slika_levo'] = 'images/labels/'.$putanja;
            } 
            if($inputs['labela_artikla_id'] != 0){
                if(isset($general_data['slika_desno_delete']) && isset($general_data['slika_desno_delete']) == 'on'){
                    $putanja_slika_desno = AdminSupport::find_labela_artikla($general_data['labela_artikla_id'], 'slika_desno');
                    if(isset($putanja_slika_desno)){
                        if(file_exists(trim($putanja_slika_desno))==true){
                            unlink(trim($putanja_slika_desno));
                        }
                    } 
                    $general_data['slika_desno'] = null;               
                }                 
                
                if(isset($general_data['slika_levo_delete']) && isset($general_data['slika_levo_delete']) == 'on'){
                    $putanja_slika_levo = AdminSupport::find_labela_artikla($general_data['labela_artikla_id'], 'slika_levo');
                    if(isset($putanja_slika_levo)){
                        if(file_exists(trim($putanja_slika_levo))==true){
                            unlink(trim($putanja_slika_levo));
                        }
                    } 
                    $general_data['slika_levo'] = null;               
                } 
                unset($general_data['slika_desno_delete']);
                unset($general_data['slika_levo_delete']);

                DB::table('labela_artikla')->where('labela_artikla_id',$inputs['labela_artikla_id'])->update($general_data);
                AdminSupport::saveLog('Labela_IZMENI', array($inputs['labela_artikla_id']));

            }else{
                DB::table('labela_artikla')->insert($general_data);
            }
            AdminTranslator::addToTranslatorAll($general_data['naziv']);

            AdminSupport::saveLog('Labela artikla, INSERT/EDIT labela_artikla_id -> '.$general_data['labela_artikla_id']);
            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/labela/'.$general_data['labela_artikla_id'])->with('message',$message);
        }
    }

    function labela_artikla_delete($labela_artikla_id)
    {   

        DB::table('labela_artikla')->where('labela_artikla_id',$labela_artikla_id)->delete();
        
        AdminSupport::saveLog('labelaovi artikla, DELETE labela_artikla_id -> '.$labela_artikla_id);
        return Redirect::to(AdminOptions::base_url().'admin/labela/0')->with('message','Uspešno ste obrisali sadržaj.');
    }
    
}
