<?php
use Service\TranslatorService;

class AdminFooterController extends Controller {

    public function footer($id,$jezik_id=null){
        if(is_null($jezik_id)){
            $jezik_id = DB::table('jezik')->where(array('aktivan'=>1,'izabrani'=>1))->pluck('jezik_id');
        }
        $sections = DB::select("SELECT * FROM futer_sekcija fs LEFT JOIN futer_sekcija_tip fst ON fs.futer_sekcija_tip_id = fst.futer_sekcija_tip_id WHERE fst.aktivan = 1 ORDER BY fs.rbr ASC");
        $lang_data = DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id'=>$id,'jezik_id'=>$jezik_id))->first();

        if(!($id != 0 && !is_null($lang_data))){
            $lang_data = (object) array('sadrzaj'=>'','naslov'=>'');
        }
        $page_ids = array_map('current',DB::table('futer_sekcija_strana')->select('web_b2c_seo_id')->where('futer_sekcija_id',$id)->get());
        if(count($page_ids) > 0){
            $pages = DB::table('web_b2c_seo')->whereNotIn('web_b2c_seo_id',$page_ids)->get();
            $linked_pages = DB::table('web_b2c_seo')->whereIn('web_b2c_seo_id',$page_ids)->get();
        }else{
            $pages = DB::table('web_b2c_seo')->get();
            $linked_pages = array();
        }


        $data = array(
            "strana"=>'footer',
            "title"=> 'Futer',
            "id"=> $id,
            "jezik_id"=> $jezik_id,
            "sections"=> $sections,
            "pages"=> $pages,
            "linked_pages"=> $linked_pages,
            "jezici" => DB::table('jezik')->where('aktivan',1)->get(),
            "item"=> $id != 0 ? DB::table('futer_sekcija')->where('futer_sekcija_id',$id)->first() : (object) array("futer_sekcija_id" => 0,"slika" => "","link" => "#","aktivan" => "","futer_sekcija_tip_id" => 1,"aktivan" => 1),
            "lang_data" => $lang_data
            );

        return View::make('admin/page',$data);
    }

    public function footer_edit(){
        $inputs = Input::get();
        //All::dd($inputs);
        $inputs['slika'] = Input::file('slika');
        $max_image_size = 10000;
        $rules = array(
                'naslov' => 'required|regex:'.AdminSupport::regex().'|max:200',
                'link' => 'max:200',
                'slika' => 'mimes:jpg,png,jpeg|max:'.strval($max_image_size)
                );

        $validator = Validator::make($inputs, $rules,
            array(
                'required' => 'Niste popunili polje.',
                'regex' => 'Polje sadrži nedozvoljene karaktere.',
                'max' => 'Sadržaj polja je predugačak.',
                'mimes' => 'Neodgovarajući format slike. Dozvoljeni formati su jpg, png i jpeg.'
                )
            );

        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{

            if($inputs['futer_sekcija_id'] == 0){
                $data = array(
                    'futer_sekcija_tip_id' => $inputs['futer_sekcija_tip_id'],
                    'aktivan' => $inputs['aktivan'],
                    'rbr' => DB::select("SELECT MAX(rbr) as current_rbr FROM futer_sekcija")[0]->current_rbr + 1
                
                );
                if($inputs['futer_sekcija_tip_id'] != 1){
                    unset($data['link']);
                }
                DB::table('futer_sekcija')->insert($data);
                $id = DB::select("SELECT currval('futer_sekcija_futer_sekcija_id_seq') as current_id")[0]->current_id;

                //lang data
                $lang_data = array(
                    'futer_sekcija_id' => $id,
                    'jezik_id' => $inputs['jezik_id'],
                    'naslov' => $inputs['naslov'],
                    'sadrzaj' => $inputs['sadrzaj']
                );

                if(!in_array($inputs['futer_sekcija_tip_id'],array(1,2))){
                    unset($inputs['sadrzaj']);
                }
                DB::table('futer_sekcija_jezik')->insert($lang_data);
                if($inputs['jezik_id']==1){
                    foreach(DB::table('jezik')->where('aktivan',1)->where('jezik_id','!=',1)->orderBy('izabrani','desc')->get() as $jezik){
                        $translator = new TranslatorService(DB::table('jezik')->where('jezik_id',1)->pluck('kod'),$jezik->kod);
                        $lang_data = array(
                            'futer_sekcija_id' => $id,
                            'jezik_id' => $jezik->jezik_id,
                            'naslov' => $translator->translate($inputs['naslov']),
                            'sadrzaj' => isset($inputs['sadrzaj']) ? $translator->translate(html_entity_decode($inputs['sadrzaj'])) : null
                        );
                        DB::table('futer_sekcija_jezik')->insert($lang_data);                 
                    }
                }
                AdminSupport::saveLog('FUTER_DODAJ', array(DB::table('futer_sekcija_jezik')->max('futer_sekcija_id')));
            }else{
                $id = $inputs['futer_sekcija_id'];
                $data = array(
                    'futer_sekcija_tip_id' => $inputs['futer_sekcija_tip_id'],
                    'aktivan' => $inputs['aktivan']
                  
                );
                if($inputs['futer_sekcija_tip_id'] != 1){
                    $data['link'] = null;
                    $data['slika'] = null;
                }
                DB::table('futer_sekcija')->where('futer_sekcija_id',$id)->update($data);

                //lang data
                $lang_data = array(
                    'naslov' => $inputs['naslov'],
                    'sadrzaj' => $inputs['sadrzaj']
                );
                if(!in_array($inputs['futer_sekcija_tip_id'],array(1,2))){
                    $inputs['sadrzaj'] = null;
                }
                DB::table('futer_sekcija_jezik')->where(array('futer_sekcija_id' => $id,'jezik_id' => $inputs['jezik_id']))->update($lang_data);
            AdminSupport::saveLog('FUTER_IZMENI', array($inputs['futer_sekcija_id']));
            }
            if($inputs['futer_sekcija_tip_id'] == 3){
                if($inputs['aktivan'] == 1 && $inputs['web_b2c_seo_id'] > 0){
                    DB::table('futer_sekcija_strana')->insert(array('futer_sekcija_id' => $id,'web_b2c_seo_id' => $inputs['web_b2c_seo_id']));
                }
            }else{
                DB::table('futer_sekcija_strana')->where('futer_sekcija_id',$id)->delete();
            }

            $path = 'images/upload/';
            $image = Input::file('slika');
            if($image){
                $name = $id.'.'.$image->getClientOriginalExtension();
                $image->move($path, $name);
                DB::table('futer_sekcija')->where('futer_sekcija_id',$id)->update(array('slika'=>$path.$name)); 
            }
            return Redirect::to(AdminOptions::base_url().'admin/futer/'.$id.'/'.$inputs['jezik_id'])->with('message','Uspešno ste sačuvali podatke.');
        }
    }

    public function footer_delete($id){
        AdminSupport::saveLog('FUTER_OBRISI', array($id));
        DB::table('futer_sekcija_strana')->where('futer_sekcija_id',$id)->delete();
        DB::table('futer_sekcija_jezik')->where('futer_sekcija_id',$id)->delete();
        DB::table('futer_sekcija')->where('futer_sekcija_id',$id)->delete();


        return Redirect::to(AdminOptions::base_url().'admin/futer/0/1')->with('message','Uspešno ste obrisali sadržaj.');
    }

    public function footer_page_delete($id, $web_b2c_seo_id){
        AdminSupport::saveLog('FUTER_STRANICA_OBRISI', array($id));
        DB::table('futer_sekcija_strana')->where(array('futer_sekcija_id'=>$id,'web_b2c_seo_id'=>$web_b2c_seo_id))->delete();


        return Redirect::to(AdminOptions::base_url().'admin/futer/'.$id)->with('message','Uspešno ste obrisali link.');
    }
    public function position_footer_section(){
        $moved = Input::get('moved');
        $order_arr = Input::get('order');

        foreach($order_arr as $key => $val){
            DB::table('futer_sekcija')->where('futer_sekcija_id',$val)->update(array('rbr'=>$key));
            $log_ids_arr[] = $val;
        }

        AdminSupport::saveLog('POZICIJA_FUTER_SEKCIJA', array($moved));

        
    }

}