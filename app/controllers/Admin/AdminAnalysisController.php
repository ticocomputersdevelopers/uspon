<?php

class AdminAnalysisController extends Controller {

    function analitikaGrupa() {
        $datum_od_din = Input::get('datum_od_din') ? Input::get('datum_od_din') : null;
        $datum_do_din = Input::get('datum_do_din') ? Input::get('datum_do_din') : null;
        $statusi = Input::get('status') ? explode('-',Input::get('status')) : [];
        
            $where_status = "";
            if (in_array('nove', $statusi)) {
            $where_status.= "OR (prihvaceno=0 AND realizovano=0 AND stornirano=0) ";
            }
            if (in_array('prihvacene', $statusi)) {
            $where_status.= "OR (prihvaceno=1 AND realizovano=0 AND stornirano=0) ";
            }
            if (in_array('realizovane', $statusi)) {
            $where_status.= "OR (realizovano=1 AND stornirano=0) ";
            }
            if (in_array('stornirane', $statusi)) {
            $where_status.= "OR stornirano=1 ";
            }

            if ($where_status != '')
            {
                $where_status = substr($where_status, 3);
            }



        $postojeNarudzbine = DB::table('web_b2c_narudzbina')->where(array('realizovano'=>1,'stornirano'=>0))->count() > 0;

        $whereDatum = "";
        if(!is_null($datum_od_din) && !empty($datum_od_din) && !is_null($datum_do_din) && !empty($datum_do_din)){
            $whereDatum = "and datum_dokumenta >= '".$datum_od_din."' and datum_dokumenta<= '".$datum_do_din."'";
        }elseif(!is_null($datum_od_din) && !empty($datum_od_din)){
            $whereDatum = "and datum_dokumenta >= '".$datum_od_din."'";
        }elseif(!is_null($datum_do_din) && !empty($datum_do_din)){
            $whereDatum = "and datum_dokumenta<= '".$datum_do_din."'";
        }

        // $between = "";
        // if(isset($datum_od_din) && isset($datum_do_din)) {
        //     $between .= " AND datum_dokumenta BETWEEN '".$datum_od_din."' AND '".$datum_do_din."'";
        // }

        $grupa_pr_ids1=array();
        $analitikaSkraceno=array();
        if($postojeNarudzbine){
            $analitikaSkraceno=DB::select("SELECT (SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
                (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1) ),
                sum(kolicina*jm_cena) AS ukupno
                FROM web_b2c_narudzbina_stavka wbns WHERE web_b2c_narudzbina_id IN 
                (SELECT web_b2c_narudzbina_id FROM web_b2c_narudzbina WHERE realizovano = 1 and stornirano=0 ".$whereDatum.")
                GROUP BY grupa_pr_id, grupa
                order by ukupno desc
                limit 5
                ");

                $grupa_pr_ids1=array_map('current', $analitikaSkraceno);
        }

        //$ukupno1 = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('realizovano', 0)->where('prihvaceno', 0)->where('stornirano', '!=', 1);
        $ukupno1 = DB::select("SELECT count(*) from web_b2c_narudzbina where web_b2c_narudzbina_id != -1 and realizovano = 0 and prihvaceno = 0 and stornirano != 1");
        if(isset($datum_od_din) || isset($datum_do_din)) {
            //$ukupno1 = $ukupno1->whereBetween('datum_dokumenta', array($datum_od_din, $datum_do_din));
            $ukupno1 = DB::select("SELECT count(*) from web_b2c_narudzbina where web_b2c_narudzbina_id != -1 and realizovano = 0 and prihvaceno = 0 and stornirano != 1 ".$whereDatum." ");
        }
        //$ukupno1 = $ukupno1->count();
        $ukupno1 = (int) $ukupno1[0]->count; 
        
    
        //$ukupno2 = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('stornirano', '=', 1);
        $ukupno2 = DB::select("SELECT count(*) from web_b2c_narudzbina where web_b2c_narudzbina_id != -1 and stornirano = 1");
        if(isset($datum_od_din) || isset($datum_do_din)) {
            //$ukupno2 = $ukupno2->whereBetween('datum_dokumenta', array($datum_od_din, $datum_do_din));
            $ukupno2 = DB::select("SELECT count(*) from web_b2c_narudzbina where web_b2c_narudzbina_id != -1 and stornirano = 1 ".$whereDatum." ");
        }
        //$ukupno2 = $ukupno2->count();
        $ukupno2 = (int) $ukupno2[0]->count;

   
        //$ukupno3 = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('realizovano', 1)->where('stornirano', '!=', 1);
        $ukupno3 = DB::select("SELECT count(*) from web_b2c_narudzbina where web_b2c_narudzbina_id != -1 and realizovano = 1 and stornirano != 1");
        if(isset($datum_od_din) || isset($datum_do_din)) {
            //$ukupno3 = $ukupno3->whereBetween('datum_dokumenta', array($datum_od_din, $datum_do_din));
            $ukupno3 = DB::select("SELECT count(*) from web_b2c_narudzbina where web_b2c_narudzbina_id != -1 and realizovano = 1 and stornirano != 1 ".$whereDatum." ");
        }
        //$ukupno3 = $ukupno3->count();
        $ukupno3 = (int) $ukupno3[0]->count;


        //$ukupno4 = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('prihvaceno', 1)->where('realizovano','!=', 1)->where('stornirano', '!=', 1);
        $ukupno4 = DB::select("SELECT count(*) from web_b2c_narudzbina where web_b2c_narudzbina_id != -1 and prihvaceno = 1 and realizovano != 1 and stornirano != 1");
        if(isset($datum_od_din) || isset($datum_do_din)) {
            //$ukupno4 = $ukupno4->whereBetween('datum_dokumenta', array($datum_od_din, $datum_do_din));
            $ukupno4 = DB::select("SELECT count(*) from web_b2c_narudzbina where web_b2c_narudzbina_id != -1 and prihvaceno = 1 and realizovano != 1 and stornirano != 1 ".$whereDatum." ");
        }
        //$ukupno4 = $ukupno4->count();
        $ukupno4 = (int) $ukupno4[0]->count;

        $analitikaOstalo=array();
        if($postojeNarudzbine && count($grupa_pr_ids1) > 0){
            $analitikaOstalo=DB::select(
                "SELECT sum(kolicina*jm_cena) AS ostalo FROM web_b2c_narudzbina_stavka wbns WHERE wbns.web_b2c_narudzbina_id IN (SELECT web_b2c_narudzbina_id FROM web_b2c_narudzbina WHERE realizovano = 1 and stornirano=0 ".$whereDatum.") AND roba_id IN (SELECT roba_id FROM roba WHERE grupa_pr_id NOT IN (".implode(",",array_filter($grupa_pr_ids1))."))");
        }
        $ostalo = 0;
        if(isset($analitikaOstalo[0])){
            $ostalo = $analitikaOstalo[0]->ostalo;
        }

        $analitikaRuc=array();
        if($postojeNarudzbine){
            $analitikaRuc=DB::select("SELECT (SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
                (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1) ),
                sum(CASE WHEN racunska_cena_nc > 0 THEN (kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina ELSE 0 END)  AS razlika
                FROM web_b2c_narudzbina_stavka wbns WHERE web_b2c_narudzbina_id IN 
                (SELECT web_b2c_narudzbina_id FROM web_b2c_narudzbina WHERE realizovano = 1 and stornirano=0 ".$whereDatum.")
                GROUP BY grupa_pr_id, grupa
                order by razlika desc
                limit 5");
        }
        $analitikaGrupaPregled=DB::select("SELECT grupa_pr_id, (select(select grupa from grupa_pr  where grupa_pr.grupa_pr_id=roba.grupa_pr_id)) as grupa1, sum(pregledan_puta) from roba
            group by grupa_pr_id
            order by sum desc
            limit 5
            ");

        $grupa_pr_ids2=array_map('current', $analitikaRuc);
        $rucOstalo=array();
        if($postojeNarudzbine && count($grupa_pr_ids2) > 0){
            $rucOstalo=DB::select("SELECT sum(CASE WHEN racunska_cena_nc > 0 THEN (kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina ELSE 0 END)  AS razlika FROM web_b2c_narudzbina_stavka wbns WHERE wbns.web_b2c_narudzbina_id IN (SELECT web_b2c_narudzbina_id FROM web_b2c_narudzbina WHERE realizovano = 1 and stornirano=0 ".$whereDatum.") AND roba_id IN (SELECT roba_id FROM roba WHERE grupa_pr_id NOT IN (".implode(",",array_filter($grupa_pr_ids2))."))");
        }

        $ostalo1 = 0;
        if(isset($rucOstalo[0])){
            $ostalo1 = $rucOstalo[0]->razlika;

        }

        $artikli = DB::select("
            SELECT roba_id, SUM(kolicina) as count 
            FROM web_b2c_narudzbina_stavka ns LEFT JOIN web_b2c_narudzbina n 
            ON n.web_b2c_narudzbina_id = ns.web_b2c_narudzbina_id 
            WHERE realizovano = 1 AND stornirano != 1 ".$whereDatum." 
            AND roba_id in (select roba_id from roba)
            GROUP BY roba_id 
            ORDER BY count DESC LIMIT 10");

        $analitikaGrupa=array();
        if($postojeNarudzbine){
            $analitikaGrupa=DB::select("SELECT (SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
                (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
                sum(kolicina), sum(kolicina*jm_cena) AS ukupno,
                sum(CASE WHEN racunska_cena_nc > 0 THEN (kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina ELSE 0 END)  AS razlika,
                sum(CASE WHEN racunska_cena_nc > 0 THEN kolicina ELSE 0 END) as nc_cena
                FROM web_b2c_narudzbina_stavka wbns WHERE web_b2c_narudzbina_id IN 
                (SELECT web_b2c_narudzbina_id FROM web_b2c_narudzbina WHERE realizovano = 1 and stornirano=0 ".$whereDatum.")
                GROUP BY grupa_pr_id, grupa
                ORDER BY ukupno DESC");
        }

        // $prihod = DB::select("
        //     SELECT SUM(kolicina * jm_cena) as count 
        //     FROM web_b2c_narudzbina_stavka ns LEFT JOIN web_b2c_narudzbina n 
        //     ON n.web_b2c_narudzbina_id = ns.web_b2c_narudzbina_id 
        //     WHERE realizovano = 1 AND stornirano != 1 
        //     ".$between."
        //     ");
        $prihod = AdminAnalitika::prihod($datum_od_din,$datum_do_din,$where_status);

        // $razlika = DB::select("
        //     SELECT SUM((kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina) as count 
        //     FROM web_b2c_narudzbina_stavka ns LEFT JOIN web_b2c_narudzbina n 
        //     ON n.web_b2c_narudzbina_id = ns.web_b2c_narudzbina_id
        //     WHERE realizovano = 1 and racunska_cena_nc>0
        //     ".$between."
        //     ");
        $razlika = AdminAnalitika::razlikaRUC($datum_od_din,$datum_do_din,$where_status);

        $realizovano = DB::select("
            SELECT COUNT(realizovano) as count 
            FROM web_b2c_narudzbina           
            WHERE realizovano = 1 and stornirano !=1
            ".$whereDatum."
            ");

        $stornirano = DB::select("
            SELECT COUNT(web_b2c_narudzbina_id) as count 
            FROM web_b2c_narudzbina            
            WHERE stornirano =1
            ".$whereDatum."
            ");

        $prihvaceno = DB::select("
            SELECT COUNT(web_b2c_narudzbina_id) as count 
            FROM web_b2c_narudzbina            
            WHERE prihvaceno =1 AND stornirano != 1 AND realizovano != 1
            ".$whereDatum."
            ");

        $nove = DB::select("
            SELECT COUNT(web_b2c_narudzbina_id) as count 
            FROM web_b2c_narudzbina            
            WHERE prihvaceno =0 AND stornirano = 0 AND realizovano = 0
            ".$whereDatum."
            ");

        $ukupno = DB::select("
            SELECT COUNT(web_b2c_narudzbina_id) as count 
            FROM web_b2c_narudzbina            
            WHERE web_b2c_narudzbina_id != -1
            ".$whereDatum."
            ");

        $prikazGrupa='';

        $data=array(
            'strana' => 'analitika',
            'title' => 'Analitika',
            'datum_od' => '',
            'datum_do' => '',
            'od' => isset($datum_od_din) ? $datum_od_din : '',
            'do' =>  isset($datum_do_din) ? $datum_do_din : '',
            'artikli' => $artikli,
            'analitikaGrupa'=>$analitikaGrupa,
            //'razlika' => $razlika[0]->count,
            'razlika' => $razlika,
            'realizovano' => $realizovano[0]->count,
            'stornirano' => $stornirano[0]->count,
            'prihvaceno' => $prihvaceno[0]->count,
            'nove' => $nove[0]->count,
            'ukupno' => $ukupno[0]->count,
            'prikazGrupa'=>$prikazGrupa,
            //'prihod' => $prihod[0]->count,
            'prihod' => $prihod,
            'analitikaSkraceno'=>$analitikaSkraceno,
            'analitikaRuc'=>$analitikaRuc,
            'analitikaGrupaPregled'=>$analitikaGrupaPregled,
            'ostalo'=>$ostalo,
             'ostalo1'=>$ostalo1,
             'ukupno1'=>$ukupno1,
             'ukupno2'=>$ukupno2,
             'ukupno3'=>$ukupno3,
             'ukupno4'=>$ukupno4,
             'statusi'=>$statusi,
             'where_status'=>$where_status,

            'anketa_id' => Input::get('anketa_id'),
            'web_kupac_id' => Input::get('web_kupac_id'),
            'web_b2c_narudzbina_id' => Input::get('web_b2c_narudzbina_id'),
            'anketa_datum_od' => Input::get('anketa_datum_od'),
            'anketa_datum_do' => Input::get('anketa_datum_do'),
            'anketaPitanjaCheckAnalitika' => Input::get('anketa_id') > 0 ? AdminAnkete::anketaPitanjaCheckAnalitika(Input::get('anketa_id'),Input::get('web_kupac_id'),Input::get('anketa_datum_od'),Input::get('anketa_datum_do'),Input::get('web_b2c_narudzbina_id')) : [],
            'anketaPitanjaTextAnalitika' => Input::get('anketa_id') > 0 ? AdminAnkete::anketaPitanjaTextAnalitika(Input::get('anketa_id'),Input::get('web_kupac_id'),Input::get('anketa_datum_od'),Input::get('anketa_datum_do'),Input::get('web_b2c_narudzbina_id')) : [],

            'anketaKupci' => Input::get('anketa_id') > 0 ? (AdminAnkete::find(Input::get('anketa_id'),'narudzbina_izabran') == 1 ? AdminAnkete::anketaKupci(Input::get('anketa_id')) : AdminAnkete::anketaKupciDatum(Input::get('anketa_id'))) : [],

            'anketaNarudzbine' => Input::get('anketa_id') > 0 ? AdminAnkete::anketaNarudzbine(Input::get('anketa_id'),(Input::get('web_kupac_id') > 0 ? Input::get('web_kupac_id') : 0)) : [],
            );

            return View::make('admin/page', $data);
    }
    
    // public function analitikaGrupa(){

    //     $analitikaSkraceno=DB::select("SELECT (SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
    //         (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1) ),
    //         sum(kolicina*jm_cena) AS ukupno
    //         FROM web_b2c_narudzbina_stavka wbns WHERE web_b2c_narudzbina_id IN 
    //         (SELECT web_b2c_narudzbina_id FROM web_b2c_narudzbina WHERE realizovano = 1 and stornirano=0)
    //         GROUP BY grupa_pr_id, grupa
    //         order by ukupno desc
    //         limit 5
    //         ");

    //         $grupa_pr_ids1=array_map('current', $analitikaSkraceno);

    //     $analitikaOstalo=DB::select(
    //         "SELECT sum(kolicina*jm_cena) AS ostalo FROM web_b2c_narudzbina_stavka wbns WHERE wbns.web_b2c_narudzbina_id IN (SELECT web_b2c_narudzbina_id FROM web_b2c_narudzbina WHERE realizovano = 1 and stornirano=0) AND roba_id IN (SELECT roba_id FROM roba WHERE grupa_pr_id NOT IN (".implode(",",$grupa_pr_ids1)."))");
    //     $ostalo = 0;
    //     if(isset($analitikaOstalo[0])){
    //         $ostalo = $analitikaOstalo[0]->ostalo;
    //     }

    //      $ukupno1 = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('realizovano', 0)->where('prihvaceno', 0)->where('stornirano', '!=', 1)->count();  
  
    //     $ukupno2 = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('stornirano', '=', 1)->count();
   
    //     $ukupno3 = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('realizovano', 1)->where('stornirano', '!=', 1)->count();


    //     $ukupno4 = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', '!=', -1)->where('prihvaceno', 1)->where('realizovano','!=', 1)->where('stornirano', '!=', 1)->count();

    //     $analitikaRuc=DB::select("SELECT (SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
    //         (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1) ),
    //         sum(CASE WHEN racunska_cena_nc > 0 THEN (kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina ELSE 0 END)  AS razlika
    //         FROM web_b2c_narudzbina_stavka wbns WHERE web_b2c_narudzbina_id IN 
    //         (SELECT web_b2c_narudzbina_id FROM web_b2c_narudzbina WHERE realizovano = 1 and stornirano=0)
    //         GROUP BY grupa_pr_id, grupa
    //         order by razlika desc
    //         limit 5");

    //     $grupa_pr_ids2=array_map('current', $analitikaRuc);

    //     $rucOstalo=DB::select("SELECT sum(CASE WHEN racunska_cena_nc > 0 THEN (kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina ELSE 0 END)  AS razlika FROM web_b2c_narudzbina_stavka wbns WHERE wbns.web_b2c_narudzbina_id IN (SELECT web_b2c_narudzbina_id FROM web_b2c_narudzbina WHERE realizovano = 1 and stornirano=0) AND roba_id IN (SELECT roba_id FROM roba WHERE grupa_pr_id NOT IN (".implode(",",$grupa_pr_ids2)."))");

    //     $ostalo1 = 0;
    //         if(isset($rucOstalo[0]) ){
    //         $ostalo1 = $rucOstalo[0]->razlika;

    //     }

    //      $artikli = DB::select("
    //         SELECT roba_id, SUM(kolicina) as count 
    //         FROM web_b2c_narudzbina_stavka ns LEFT JOIN web_b2c_narudzbina n 
    //         ON n.web_b2c_narudzbina_id = ns.web_b2c_narudzbina_id 
    //         WHERE realizovano = 1 AND stornirano != 1
    //         GROUP BY roba_id 
    //         ORDER BY count DESC LIMIT 10");

    //      $analitikaGrupa=DB::select("SELECT (SELECT grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
    //         (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT grupa_pr_id FROM roba WHERE roba_id = wbns.roba_id LIMIT 1)),
    //         sum(kolicina), sum(kolicina*jm_cena) AS ukupno, sum(kolicina), sum(kolicina*jm_cena) AS ukupno,
    //         sum(CASE WHEN racunska_cena_nc > 0 THEN (kolicina*jm_cena/1.2) - racunska_cena_nc*kolicina ELSE 0 END)  AS razlika,
    //         sum(CASE WHEN racunska_cena_nc > 0 THEN kolicina ELSE 0 END) as nc_cena
    //         FROM web_b2c_narudzbina_stavka wbns WHERE web_b2c_narudzbina_id IN 
    //         (SELECT web_b2c_narudzbina_id FROM web_b2c_narudzbina WHERE realizovano = 1 and stornirano=0)
    //         GROUP BY grupa_pr_id, grupa
    //         ORDER BY ukupno DESC");


    //       $analitikaGrupaPregled=DB::select("SELECT grupa_pr_id, (select(select grupa from grupa_pr  where grupa_pr.grupa_pr_id=roba.grupa_pr_id)) as grupa1, sum(pregledan_puta) from roba

    //         group by grupa_pr_id
    //         order by sum desc
    //         limit 5
    //         ");

    //        $data=array(
    //         'strana'=>'analitika',
    //         'title'=>'Analitika',
    //         'datum_od'=>isset($datum_od) ? $datum_od : '',
    //         'datum_do'=>isset($datum_do) ? $datum_do : '',
    //         'artikli'=>$artikli,
    //         'analitikaGrupa'=>$analitikaGrupa,
    //         'analitikaSkraceno'=>$analitikaSkraceno,
    //         'analitikaRuc'=>$analitikaRuc,
    //         'ostalo'=>$ostalo,
    //         'ostalo1'=>$ostalo1,
    //         'ukupno1'=>$ukupno1,
    //         'ukupno2'=>$ukupno2,
    //         'ukupno3'=>$ukupno3,
    //         'ukupno4'=>$ukupno4,
    //         'analitikaGrupaPregled'=>$analitikaGrupaPregled,
    //         'od' => '',
    //         'do' => ''
    //         );
            
    //         return View::make('admin/page', $data);
    // }
    
}