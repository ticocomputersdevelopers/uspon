<?php

class AdminKatalogController extends Controller {
     // Katalog
    public function katalog($katalog_id)
    {      
        $katalog_vrste =AdminKatalog::getKatalogVrsta(true);
        $katalog_polja =AdminKatalog::getKatalogPolja($katalog_id);
        $katalog_sva_polja = array(
            'web_cena' => 'Cena',
            'web_cena_pdv' => 'Cena sa PDV-om',
            'slika' => 'Slika',
            'naziv' => 'Naziv',
            'opis' => 'Opis',
            'karakteristike' => 'Karakteristike'
        );

        $katalog_dostupna_polja = array_filter($katalog_sva_polja,function($value) use ($katalog_sva_polja,$katalog_id) {
            if(DB::table('katalog_polja')->where('katalog_id',$katalog_id)->where('naziv',array_search($value,$katalog_sva_polja))->count() == 0){
                return true;
            } 
        });

        $data=array(
            "strana"=>'katalog',
            "title"=>$katalog_id != 0 ? AdminKatalog::find_katalog($katalog_id, 'naziv') : 'Novi katalog',
            "katalog_id"=>$katalog_id,
            "naziv"=> $katalog_id != 0 ? AdminKatalog::find_katalog($katalog_id, 'naziv') : null,
            "vazi_od"=> $katalog_id != 0 ? AdminKatalog::find_katalog($katalog_id, 'vazi_od') : date('Y-m-d'),
            "sort"=> $katalog_id != 0 ? AdminKatalog::find_katalog($katalog_id, 'sort') : null,
            "katalog_vrsta_id"=> $katalog_id != 0 ? AdminKatalog::find_katalog($katalog_id, 'katalog_vrsta_id') : null,
            "aktivan"=> $katalog_id != 0 ? AdminKatalog::find_katalog($katalog_id, 'aktivan') : null,
            "b2c_aktivno"=> $katalog_id != 0 ? AdminKatalog::find_katalog($katalog_id, 'b2c_aktivno') : 0,
            "b2b_aktivno"=> $katalog_id != 0 ? AdminKatalog::find_katalog($katalog_id, 'b2b_aktivno') : 0,
            "katalog_vrste"=> $katalog_vrste,
            "katalog_polja"=> $katalog_polja,
            "katalog_dostupna_polja"=> $katalog_dostupna_polja,
            "putanja_slika"=> $katalog_id != 0 ? AdminKatalog::find_katalog($katalog_id, 'putanja_slika') : null,
            "putanja_pdf"=> $katalog_id != 0 ? AdminKatalog::find_katalog($katalog_id, 'putanja_pdf') : null,
        );
            
            return View::make('admin/page', $data);
    }
    public function katalog_edit()
    {   
        $inputs = Input::get();
        $image = Input::file('putanja_slika');
        $catalog_file = Input::file('putanja_pdf');
        $grupa_pr_id = Input::get('grupa_pr_id');
        $grupa_pr_naziv_id = Input::get('grupa_pr_naziv_id');
// All::dd($inputs);
        $validator = Validator::make($inputs, array('naziv' => 'required|max:200'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/katalog/'.$inputs['katalog_id'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['katalog_id'] == 0){                
                $general_data['katalog_id'] = DB::select("SELECT MAX(katalog_id) AS max FROM katalog")[0]->max + 1;
            }
            if($general_data['vazi_od'] == ''){
                $general_data['vazi_od'] = null;
            }
            if(isset($inputs['b2c_aktivno']) && $inputs['b2c_aktivno'] == 'on'){
                $general_data['b2c_aktivno'] = 1;
            }else{
                $general_data['b2c_aktivno'] = 0;
            }
            if(isset($inputs['b2b_aktivno']) && $inputs['b2b_aktivno'] == 'on'){
                $general_data['b2b_aktivno'] = 1;
            }else{
                $general_data['b2b_aktivno'] = 0;
            }
            
            unset($general_data['grupa_pr_id']);
            unset($general_data['grupa_pr_naziv_id']);

            if(isset($image)){
                $extension = $image->getClientOriginalExtension();
                if($inputs['katalog_id'] != 0) {
                    $image_name = $inputs['katalog_id'];
                } else {
                    $image_name = DB::select("SELECT MAX(katalog_id) AS max FROM katalog")[0]->max + 1;
                }
                $putanja = $image_name.'.'.$extension;
                $image->move('images/catalogs/', $putanja);
                $general_data['putanja_slika'] = 'images/catalogs/'.$putanja;
            }

            if(isset($catalog_file)) {
                $extension = $catalog_file->getClientOriginalExtension();
                if($inputs['katalog_id'] != 0) {
                    $catalog_name = $catalog_file->getClientOriginalName();
                } else {
                    $catalog_name = DB::select('SELECT MAX(katalog_id) AS max FROM katalog') [0]->max + 1;
                }
                $putanja = $catalog_name;
                $catalog_file->move('images/catalogs/files/', $putanja);
                $general_data['putanja_pdf'] = 'images/catalogs/files/'.$putanja;
            }

            if($inputs['katalog_id'] != 0){
                if(isset($inputs['slika_delete'])) {
                    $general_data['putanja_slika'] = null;
                    $old_image = DB::table('katalog')->where('katalog_id',$inputs['katalog_id'])->pluck('putanja_slika');
                    if(File::exists($old_image)){
                        File::delete($old_image);
                    }
                    unset($general_data['slika_delete']);
                }
                if(is_null($general_data['putanja_pdf'])) {
                    $general_data['putanja_pdf'] = DB::table('katalog')->where('katalog_id', $general_data['katalog_id'])->pluck('putanja_pdf');
                }

                DB::table('katalog')->where('katalog_id',$inputs['katalog_id'])->update($general_data);
                AdminSupport::saveLog('katalog_IZMENI', array($general_data['katalog_id']));
            }else{
                DB::table('katalog')->insert($general_data);
                AdminSupport::saveLog('katalog_DODAJ', array(DB::table('katalog')->max('katalog_id')));
            }

            if(!is_null($grupa_pr_id) && !is_null($grupa_pr_naziv_id)){
                if(DB::table('katalog_grupe_sort')->where(array('katalog_id'=>$general_data['katalog_id'],'grupa_pr_id'=>$grupa_pr_id))->count() > 0){
                    DB::table('katalog_grupe_sort')->where(array('katalog_id'=>$general_data['katalog_id'],'grupa_pr_id'=>$grupa_pr_id))->update(array('grupa_pr_naziv_id'=>$grupa_pr_naziv_id));
                }else{
                    DB::table('katalog_grupe_sort')->insert(array('katalog_id'=>$general_data['katalog_id'],'grupa_pr_id'=>$grupa_pr_id,'grupa_pr_naziv_id'=>$grupa_pr_naziv_id));
                }
            }

            AdminTranslator::addToTranslatorAll($general_data['naziv']);
            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/katalog/'.$general_data['katalog_id'])->with('message',$message);
        }
    }

    public function katalog_delete($katalog_id)
    {
        $old_file = DB::table('katalog')->where('katalog_id', $katalog_id)->pluck('putanja_pdf');
        if(File::exists($old_file)) {
            File::delete($old_file);
        }
        
        DB::table('katalog')->where('katalog_id',$katalog_id)->delete();
        return Redirect::to(AdminOptions::base_url().'admin/katalog/0');
    }

    public function katalog_polje_dodaj()
    {
        $data = Input::get();
        if($data['katalog_id'] > 0){
            if(DB::table('katalog_polja')->where(array('katalog_id'=>$data['katalog_id'],'naziv'=>$data['naziv']))->count() == 0){
                DB::table('katalog_polja')->insert(array('katalog_id'=>$data['katalog_id'],'naziv'=>$data['naziv'],'title'=>$data['title'],'rbr'=>(DB::table('katalog_polja')->max('rbr')+1)));
            }
        }
    }

    public function katalog_polje_obrisi($katalog_polja_id)
    {
        DB::table('katalog_polja')->where('katalog_polja_id',$katalog_polja_id)->delete();
        return Redirect::back();
    }

    public function katalog_polja_pozicija(){
        $katalog_id = Input::get('katalog_id');
        $order_arr = Input::get('order');
        foreach($order_arr as $key => $val){
            DB::table('katalog_polja')->where(array('katalog_id'=>$katalog_id,'katalog_polja_id'=>$val))->update(array('rbr'=>$key));
        }
    }

    public function grupa_karakteristike_nazivi(){
        $grupa_pr_id = Input::get('grupa_pr_id');
        $karakteristikeNazivi = DB::table('grupa_pr_naziv')->where('grupa_pr_id',$grupa_pr_id)->orderBy('rbr','asc')->get();
        return json_encode($karakteristikeNazivi);
    }

    public function grupa_karakteristika_delete(){
        $data = Input::get();
        DB::table('katalog_grupe_sort')->where($data)->delete();
    }

}
