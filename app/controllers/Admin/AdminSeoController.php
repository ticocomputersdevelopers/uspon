<?php

class AdminSeoController extends Controller {

	public function sitemap_generate(){

    	$jezici = DB::table('jezik')->where('aktivan',1)->orderBy('izabrani','desc')->get();

        $pages = DB::table('web_b2c_seo')->whereIn('tip_artikla_id',array(-1,0))->get();

        $products_count =DB::select("SELECT count(*) FROM roba r".AdminOptions::checkImage('join').AdminOptions::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND r.roba_id <> -1".AdminOptions::checkImage().AdminOptions::checkPrice().AdminOptions::checkDescription().AdminOptions::checkCharacteristics());



        $groups = DB::table('grupa_pr')->where('web_b2c_prikazi',1)->where('grupa_pr_id','>',0)->orderBy('grupa_pr_id','asc')->get();
        $manufacturers = DB::table('proizvodjac')->where('proizvodjac_id','!=',0)->where('proizvodjac_id','!=',-1)->where(array('brend_prikazi'=>1))->get();

        $tip_artikla_ids = DB::table('web_b2c_seo')->select('tip_artikla_id')->where('tip_artikla_id','!=',0)->where('tip_artikla_id','!=',-1)->get();
        $types = array();
        if(count($tip_artikla_ids) > 0){
	        $types = DB::table('tip_artikla')->where('tip_artikla_id','!=',0)->where('tip_artikla_id','!=',-1)->where('active',1)->whereIn('tip_artikla_id',array_map('current',$tip_artikla_ids))->get();
	    }

	    $limit = 5000;
	    $offset = 0;

	   
	    foreach($jezici as $key_jezik => $jezik){

		   	$tags = array();
	    	for ($i=1; $i <= (int) ceil($products_count[0]->count/$limit)  ; $i++) { 
		    	$products_query = "SELECT DISTINCT r.roba_id, r.naziv_web, r.tags, (SELECT putanja FROM web_slika WHERE roba_id = r.roba_id AND flag_prikazi = 1 ORDER BY akcija DESC LIMIT 1) as image FROM roba r".AdminOptions::checkImage('join').AdminOptions::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND r.roba_id <> -1".AdminOptions::checkImage().AdminOptions::checkPrice().AdminOptions::checkDescription().AdminOptions::checkCharacteristics()." order by r.roba_id asc limit ".$limit." offset ".$offset."";

		    	$products = DB::select($products_query);

				$xml = new DOMDocument("1.0","UTF-8");
				$root = $xml->createElement("urlset");
		        $root->setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
		        $root->setAttribute("xmlns:image", "http://www.google.com/schemas/sitemap-image/1.1");

				$xml->appendChild($root);

				foreach($products as $product){
					$url = $xml->createElement("url");
					self::xml_node($xml,"loc",AdminArticles::article_link($product->roba_id,$jezik->kod),$url);
					self::xml_node($xml,"changefreq",'weekly',$url);

					if($product->image){
						$image = $xml->createElement("image:image");
						self::xml_node($xml,"image:loc",AdminOptions::base_url().$product->image,$image);
						self::xml_node($xml,"image:caption",$product->naziv_web,$image);
						$url->appendChild($image);
					}
					$root->appendChild($url);

					self::tags($tags,$product->tags);
				}

				$xml->formatOutput = true;
				$store_path = 'files/sitemap/sitemap_'.$key_jezik.'_'.$i.'.xml';
				$xml->save($store_path) or die("Error");

				$offset = $offset + $limit;
	    	}

	    	$xml = new DOMDocument("1.0","UTF-8");
			$root = $xml->createElement("urlset");
	        $root->setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
	        $root->setAttribute("xmlns:image", "http://www.google.com/schemas/sitemap-image/1.1");

			$xml->appendChild($root);

			$base_url = AdminOptions::live_base_url($jezik->kod);

			//stranice
			foreach($pages as $page){
				$url = $xml->createElement("url");
				self::xml_node($xml,"loc",AdminOptions::base_url().AdminOptions::page_slug($page->naziv_stranice,$jezik->jezik_id)->slug,$url);
				self::xml_node($xml,"changefreq",'weekly',$url);
				$root->appendChild($url);
			}

			foreach($groups as $group){
				$groupLink = AdminGroups::group_link($group->grupa_pr_id,$jezik->kod);
				if(!is_null($groupLink)){
					$url = $xml->createElement("url");
					self::xml_node($xml,"loc",$base_url.$groupLink,$url);
					self::xml_node($xml,"changefreq",'weekly',$url);
					$root->appendChild($url);
				}
			}

			foreach($manufacturers as $manufacturer){
				$url = $xml->createElement("url");
				self::xml_node($xml,"loc",$base_url.AdminOptions::slug_trans('proizvodjac',$jezik->kod).'/'.AdminOptions::slug_trans($manufacturer->naziv,$jezik->kod),$url);
				self::xml_node($xml,"changefreq",'weekly',$url);
				$root->appendChild($url);
			}

			foreach($types as $type){
				$url = $xml->createElement("url");
				self::xml_node($xml,"loc",$base_url.AdminOptions::slug_trans('tip',$jezik->kod).'/'.AdminOptions::slug_trans($type->naziv,$jezik->kod),$url);
				self::xml_node($xml,"changefreq",'weekly',$url);
				$root->appendChild($url);
			}


			$news = DB::table('web_vest_b2c')->join('web_vest_b2c_jezik','web_vest_b2c_jezik.web_vest_b2c_id','=','web_vest_b2c.web_vest_b2c_id')->select('*','web_vest_b2c_jezik.sadrzaj as tekst')->where(array('aktuelno'=>1,'jezik_id'=>$jezik->jezik_id))->orderBy('rbr','asc')->get();
			foreach($news as $new){
				$url = $xml->createElement("url");
				self::xml_node($xml,"loc",AdminVesti::news_link($new->web_vest_b2c_id),$url);
				self::xml_node($xml,"changefreq",'weekly',$url);
				$root->appendChild($url);
			}

			foreach($tags as $tag){
				$url = $xml->createElement("url");
				self::xml_node($xml,"loc",$base_url.AdminOptions::slug_trans('tagovi',$jezik->kod).'/'.AdminOptions::slug_trans($tag,$jezik->kod),$url);
				self::xml_node($xml,"changefreq",'weekly',$url);
				$root->appendChild($url);
			}

			$xml->formatOutput = true;
			$store_path = 'files/sitemap/sitemap_'.$key_jezik.'_'.$i.'.xml';
			$xml->save($store_path) or die("Error");
	    	
	    }

	    $xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("sitemapindex");
        $root->setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");

		$xml->appendChild($root);

	    foreach ($jezici as $key_jezik => $jezik) {
	    	$base_url = AdminOptions::live_base_url($jezik->kod);
	    	for ($m=1; $m <= $i ; $m++) { 
	    		$url = $xml->createElement("sitemap");
				self::xml_node($xml,"loc",$base_url.'files/sitemap/sitemap_'.$key_jezik.'_'.$m.'.xml',$url);
				self::xml_node($xml,"lastmod", date("c", filemtime('files/sitemap/sitemap_'.$key_jezik.'_'.$m.'.xml')),$url);
				$root->appendChild($url);
	    	}
	    }


	    $xml->formatOutput = true;
		$store_path = 'sitemap.xml';
		$xml->save($store_path) or die("Error");


		AdminSupport::saveLog('SITEMAP_GENERISI');

        return Redirect::to(AdminOptions::base_url().'admin/podesavanja');
    }
	
    public function sitemap_generate_old(){
    	$jezici = DB::table('jezik')->where('aktivan',1)->orderBy('izabrani','desc')->get();

        $pages = DB::table('web_b2c_seo')->whereIn('tip_artikla_id',array(-1,0))->get();

        $products=DB::select("SELECT DISTINCT r.roba_id, r.naziv_web, r.tags, (SELECT putanja FROM web_slika WHERE roba_id = r.roba_id AND flag_prikazi = 1 ORDER BY akcija DESC LIMIT 1) as image FROM roba r".AdminOptions::checkImage('join').AdminOptions::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND r.roba_id <> -1".AdminOptions::checkImage().AdminOptions::checkPrice().AdminOptions::checkDescription().AdminOptions::checkCharacteristics());

        $groups = DB::table('grupa_pr')->where('web_b2c_prikazi',1)->where('grupa_pr_id','>',0)->orderBy('grupa_pr_id','asc')->get();
        $manufacturers = DB::table('proizvodjac')->where('proizvodjac_id','!=',0)->where('proizvodjac_id','!=',-1)->where(array('brend_prikazi'=>1))->get();

        $tip_artikla_ids = DB::table('web_b2c_seo')->select('tip_artikla_id')->where('tip_artikla_id','!=',0)->where('tip_artikla_id','!=',-1)->get();
        $types = array();
        if(count($tip_artikla_ids) > 0){
	        $types = DB::table('tip_artikla')->where('tip_artikla_id','!=',0)->where('tip_artikla_id','!=',-1)->where('active',1)->whereIn('tip_artikla_id',array_map('current',$tip_artikla_ids))->get();
	    }



		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("urlset");
        $root->setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
        $root->setAttribute("xmlns:image", "http://www.google.com/schemas/sitemap-image/1.1");

		$xml->appendChild($root);

		foreach($jezici as $jezik){
			$base_url = AdminOptions::live_base_url($jezik->kod);

			//stranice
			foreach($pages as $page){
				$url = $xml->createElement("url");
				self::xml_node($xml,"loc",AdminOptions::base_url().AdminOptions::page_slug($page->naziv_stranice,$jezik->jezik_id)->slug,$url);
				self::xml_node($xml,"changefreq",'weekly',$url);
				$root->appendChild($url);
			}

			$tags = array();
			foreach($products as $product){
				$url = $xml->createElement("url");
				self::xml_node($xml,"loc",AdminArticles::article_link($product->roba_id,$jezik->kod),$url);
				self::xml_node($xml,"changefreq",'weekly',$url);

				if($product->image){
					$image = $xml->createElement("image:image");
					self::xml_node($xml,"image:loc",AdminOptions::base_url().$product->image,$image);
					self::xml_node($xml,"image:caption",$product->naziv_web,$image);
					$url->appendChild($image);
				}
				$root->appendChild($url);

				self::tags($tags,$product->tags);
			}

			foreach($groups as $group){
				$groupLink = AdminGroups::group_link($group->grupa_pr_id,$jezik->kod);
				if(!is_null($groupLink)){
					$url = $xml->createElement("url");
					self::xml_node($xml,"loc",$base_url.$groupLink,$url);
					self::xml_node($xml,"changefreq",'weekly',$url);
					$root->appendChild($url);
				}
			}

			foreach($manufacturers as $manufacturer){
				$url = $xml->createElement("url");
				self::xml_node($xml,"loc",$base_url.AdminOptions::slug_trans('proizvodjac',$jezik->kod).'/'.AdminOptions::slug_trans($manufacturer->naziv,$jezik->kod),$url);
				self::xml_node($xml,"changefreq",'weekly',$url);
				$root->appendChild($url);
			}

			foreach($types as $type){
				$url = $xml->createElement("url");
				self::xml_node($xml,"loc",$base_url.AdminOptions::slug_trans('tip',$jezik->kod).'/'.AdminOptions::slug_trans($type->naziv,$jezik->kod),$url);
				self::xml_node($xml,"changefreq",'weekly',$url);
				$root->appendChild($url);
			}


			$news = DB::table('web_vest_b2c')->join('web_vest_b2c_jezik','web_vest_b2c_jezik.web_vest_b2c_id','=','web_vest_b2c.web_vest_b2c_id')->select('*','web_vest_b2c_jezik.sadrzaj as tekst')->where(array('aktuelno'=>1,'jezik_id'=>$jezik->jezik_id))->orderBy('rbr','asc')->get();
			foreach($news as $new){
				$url = $xml->createElement("url");
				self::xml_node($xml,"loc",AdminVesti::news_link($new->web_vest_b2c_id),$url);
				self::xml_node($xml,"changefreq",'weekly',$url);
				$root->appendChild($url);
			}

			foreach($tags as $tag){
				$url = $xml->createElement("url");
				self::xml_node($xml,"loc",$base_url.AdminOptions::slug_trans('tagovi',$jezik->kod).'/'.AdminOptions::slug_trans($tag,$jezik->kod),$url);
				self::xml_node($xml,"changefreq",'weekly',$url);
				$root->appendChild($url);
			}
		}

		$xml->formatOutput = true;
		$store_path = 'sitemap.xml';
		$xml->save($store_path) or die("Error");

		AdminSupport::saveLog('SITEMAP_GENERISI');

        return Redirect::to(AdminOptions::base_url().'admin/podesavanja');
    }

    private static function xml_node($xml,$name,$value,$parent){
		$xmltag = $xml->createElement($name);
	    $xmltagText = $xml->createTextNode($value);
	    $xmltag->appendChild($xmltagText);
	    $parent->appendChild($xmltag);
    }

    private static function tags(&$tags,$tags_string){
    	if(!is_null($tags_string) && $tags_string != ''){
    		$product_tags = explode(',', $tags_string);
    		foreach($product_tags as $product_tag){
    			if(!in_array(trim($product_tag), $tags)){
    				$tags[] = trim($product_tag);
    			}
    		}
    	}

    }

}