<?php
use Import\Support;
use Import\Ewe;
use Import\Computerland;
use Import\Velteh;
use Import\Kimtec;
use Import\CT;
use Import\Asbis;
use Import\Moda;
use Import\Gama;
use Import\Dudico;
use Import\ZoomImpexXML; 
use Import\Tnt;
use Import\Uspon;
use Import\Cudo;
use Import\PCCentar;
use Import\CobraToys;
use Import\Westaco;
use Import\Yuglob;
use Import\Kosa;
use Import\Laluco;
use Import\HappyDog;
use Import\Xplorer;
use Import\AudioPlaneta;
use Import\Agromarket;
use Import\Zorbal;
use Import\Printcom;
use Import\AtomPartner;
use Import\Bosh;
use Import\Elmark;
use Import\MobilneKamere;
use Import\Capriolo;
use Import\CTexcel;
use Import\CTCUnit;
use Import\Digimax;
use Import\DS;
use Import\MKA;
use Import\EuromDenis;
use Import\FormaVs;
use Import\Element;
use Import\Gume;
use Import\Pirot;
use Import\Dunlop;
use Import\Fulda;
use Import\GoodYear;
use Import\Urban;
use Import\Hankook;
use Import\Michelin;
use Import\Lassa;
use Import\Pirelli;
use Import\Riken;
use Import\Sava;
use Import\Starfire;
use Import\Tigar;
use Import\Profitech;
use Import\Keno;
use Import\Keprom;
use Import\Kama;
use Import\KimtecExcel;
use Import\Mcg;
use Import\Milenijum;
use Import\Mison;
use Import\Oxygen;
use Import\PCCentarExcel;
use Import\PetProtector;
use Import\PinComputers;
use Import\Computerline;
use Import\Roaming;
use Import\Sbalsan;
use Import\SvetlostMka;
use Import\Telit;
use Import\TsMod;
use Import\Nelt;
use Import\Vox;
use Import\Whirlpool;
use Import\Woobyhouse;
use Import\ZoomImpex;
use Import\NTCompany;
use Import\Refot;
use Import\Virtikom;
use Import\Wolfcraft;
use Import\Bebindo;
use Import\Recaro;
use Import\Vana;
use Import\Mogly;
use Import\AsbisOld;
use Import\CTXml;
use Import\SvetlostVka;
use Import\GorenjeMka;
use Import\GorenjeVka;
use Import\Ibg;
use Import\Gembird;
use Import\Josipovic;
use Import\Akumulatori;
use Import\OFY;
use Import\Epi;
use Import\ERG;
use Import\Venco;
use Import\HomeJungle;
use Import\Keller;
use Import\Ekka;
use Import\Sensa;
use Import\Stotex;
use Import\Sapin;
use Import\Domel;
use Import\Demark;
use Import\MaliKucniAparati;
use Import\BCGroup;

class AdminImportFileController extends Controller {

    function web_import_ajax()
    {
        $partner_id = Input::get('partner_id');
        $kurs = Input::get('kurs');
        $upload_file = Input::get('upload_file');
        $extension = Input::get('upload_file_extension');
        $roba_insert = Input::get('roba_insert');

        $response = array(
            'success' => '0',
            'message' => ''
            );

        if ($partner_id!=null && $kurs != null) {

            $import_name = DB::table('dobavljac_cenovnik_kolone')->where('partner_id',$partner_id)->pluck('import_naziv_web');

            try {
                if($import_name == 'import_ewe'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        Ewe::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    // }
                }
               
                elseif($import_name == 'import_velteh'){ 
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                    Velteh::execute($partner_id,$extension);
                    $response['success'] = 1;
                // }
                }
                elseif($import_name == 'import_epi'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                    Epi::execute($partner_id,$kurs,$extension);
                    $response['success'] = 1;
                    // }
                }
                elseif($import_name == 'import_erg'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                    ERG::execute($partner_id,$kurs,$extension);
                    $response['success'] = 1;
                    // }
                }
                elseif($import_name == 'import_uspon'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                    Uspon::execute($partner_id,$kurs,$extension);
                    $response['success'] = 1;
                    // }
                }
                elseif($import_name == 'import_tnt'){
                    if($upload_file == '0'){    
                        Tnt::execute($partner_id);
                    }else{
                        Tnt::execute($partner_id,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_pccentar'){
                    if($upload_file == '0'){    
                        PCCentar::execute($partner_id);
                    }else{
                        PCCentar::execute($partner_id,$extension);
                    }
                    $response['success'] = 1;
                }
                elseif($import_name == 'import_moda'){
                    if($upload_file == '0'){    
                        Moda::execute($partner_id);
                    }else{
                        Moda::execute($partner_id,$kurs,$extension);
                    }
                    $response['success'] = 1;
                }
                elseif($import_name == 'import_gama'){
                    if($upload_file == '0'){    
                        Gama::execute($partner_id);
                    }else{
                        Gama::execute($partner_id,$extension);
                    }
                    $response['success'] = 1;
                }
                elseif($import_name == 'import_dudico'){
                    if($upload_file == '0'){    
                        Dudico::execute($partner_id);
                    }else{
                        Dudico::execute($partner_id,$extension);
                    }
                    $response['success'] = 1;
                }
                elseif($import_name == 'import_zoomimpexxml'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                    ZoomImpexXML::execute($partner_id,$kurs,$extension);
                    $response['success'] = 1;
                    // }
                }
                elseif($import_name == 'import_element'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                    Element::execute($partner_id,$kurs,$extension);
                    $response['success'] = 1;
                    // }
                }
                
                elseif($import_name == 'import_josipovic'){
                    // if($upload_file == '0'){
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                    Josipovic::execute($partner_id,$kurs,$extension);
                    $response['success'] = 1;
                    // }
                }
                elseif($import_name == 'import_printcom'){
                    // if($upload_file == '0'){
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                    Printcom::execute($partner_id,$kurs,$extension);
                    $response['success'] = 1;
                    // }
                }
                elseif($import_name == 'import_mogly'){
                    if($upload_file == '0'){    
                        Mogly::execute($partner_id);
                    }else{
                        Mogly::execute($partner_id,$extension);
                    }
                    $response['success'] = 1;
                }
                elseif($import_name == 'import_ds'){
                    if($upload_file == '0'){    
                    DS::execute($partner_id);
                    }else{
                    DS::execute($partner_id,$extension);
                    }
                    $response['success'] = 1;
                }

                //services
                elseif($import_name == 'import_asbis'){ 
                    Asbis::execute($partner_id,$kurs);
                    $response['success'] = 1;
                }
                // elseif($import_name == 'import_ct'){ 
                //     CT::execute($partner_id,$kurs);
                        // $response['success'] = 1;
                // }
                
                elseif($import_name == 'import_ct'){ 
                    $response = CT::execute($partner_id);
                    if($response != null){

                        // AdminSupport::saveLog('IMPORT_GRESKA', $partner_id);    
                        $response['message'] = '<p class="error">'.$response.'</p>';
                    }else{
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_kimtec'){ 
                    $response = Kimtec::execute($partner_id);
                    if($response != null){

                        // AdminSupport::saveLog('IMPORT_GRESKA', $partner_id);    
                        $response['message'] = '<p class="error">'.$response.'</p>';
                    }else{
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_cobratoys'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        CobraToys::execute($partner_id,$extension);
                        $response['success'] = 1;
                    }
                }

                elseif($import_name == 'import_westaco'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Westaco::execute($partner_id,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_yuglob'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Yuglob::execute($partner_id,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_kosa'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Kosa::execute($partner_id,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_laluco'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Laluco::execute($partner_id,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_happydog'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        HappyDog::execute($partner_id,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_cudo'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Cudo::execute($partner_id,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_xplorer'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        Xplorer::execute($partner_id,$extension);
                        $response['success'] = 1;
                    // }
                }
                elseif($import_name == 'import_audioplaneta'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        AudioPlaneta::execute($partner_id,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_agromarket'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Agromarket::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_zorbal'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Zorbal::execute($partner_id,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_atompartner'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        AtomPartner::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    // }
                }
                elseif($import_name == 'import_vox'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        Vox::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                   // }
                }
                elseif($import_name == 'import_bosh'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Bosh::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_capriolo'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Capriolo::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_ctexcel'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        CTexcel::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_ctcunit'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        CTCUnit::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_digimax'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Digimax::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }

                elseif($import_name == 'import_euromdenis'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        EuromDenis::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    // }
                }
                elseif($import_name == 'import_formavs'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        FormaVs::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }

                elseif($import_name == 'import_gume'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Gume::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_pirot'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Pirot::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                 elseif($import_name == 'import_dunlop'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Dunlop::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_fulda'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Fulda::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_urban'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Urban::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_elmark'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Elmark::execute($partner_id,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_goodyear'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        GoodYear::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_hankook'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Hankook::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_lassa'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Lassa::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_kama'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Kama::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_michelin'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Michelin::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_pirelli'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Pirelli::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_riken'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Riken::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_sava'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Sava::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_starfire'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Starfire::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_tigar'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Tigar::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_profitech'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        Profitech::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    // }
                }
                elseif($import_name == 'import_keno'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Keno::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_keprom'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Keprom::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_kimtecexcel'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        KimtecExcel::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_mcg'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Mcg::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_milenijum'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Milenijum::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_mison'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Mison::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_oxygen'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Oxygen::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_pccentarexcel'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        PCCentarExcel::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_petprotector'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        PetProtector::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_pincomputers'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        PinComputers::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    // }
                }
                 elseif($import_name == 'import_computerland'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        Computerland::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    // }
                }
                elseif($import_name == 'import_roaming'){
                        Roaming::execute($partner_id,$extension);
                        $response['success'] = 1;                    
                }
                elseif($import_name == 'import_sbalsan'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Sbalsan::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_svetlostmka'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        SvetlostMka::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_mka'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        MKA::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_telit'){

                    Telit::execute($partner_id,$kurs);
                }
                elseif($import_name == 'import_tsmod'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        TsMod::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_nelt'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Nelt::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }

                elseif($import_name == 'import_whirlpool'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Whirlpool::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_mobilnekamere'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        MobilneKamere::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_woobyhouse'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Woobyhouse::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_zoomimpex'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        ZoomImpex::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_ntcompany'){
                    // if($upload_file == '0'){    
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        NTCompany::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    // }
                }
                elseif($import_name == 'import_refot'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Refot::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_virtikom'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Virtikom::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_wolfcraft'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Wolfcraft::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_bebindo'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Bebindo::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_recaro'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Recaro::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_vana'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Vana::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_keller'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Keller::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_asbisold'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        AsbisOld::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_ctxml'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        CTXml::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_svetlostvka'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        SvetlostVka::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_gorenjemka'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        GorenjeMka::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_gorenjevka'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        GorenjeVka::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_ibg'){
                    if($upload_file == '0'){    
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Ibg::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_gembird'){
                    if($upload_file == '0'){
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Gembird::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_ofy'){ 
                    // if($upload_file == '0'){
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                    OFY::execute($partner_id,$kurs,$extension);
                    $response['success'] = 1;
                // }
                }
                elseif($import_name == 'import_venco'){
                    if($upload_file == '0'){
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Venco::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_akumulatori'){
                    if($upload_file == '0'){
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Akumulatori::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_homejungle'){
                    if($upload_file == '0'){
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        HomeJungle::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_ekka'){
                    // if($upload_file == '0'){
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        Ekka::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    
                }
                elseif($import_name == 'import_domel'){
                    // if($upload_file == '0'){
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        Domel::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    
                }
                elseif($import_name == 'import_demark'){
                    // if($upload_file == '0'){
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        Demark::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    
                }
                elseif($import_name == 'import_malikucniaparati'){
                    // if($upload_file == '0'){
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        MaliKucniAparati::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    
                }
                elseif($import_name == 'import_bcgroup'){
                    // if($upload_file == '0'){
                    //     $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    // }else{
                        BCGroup::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    
                }
                elseif($import_name == 'import_sensa'){
                    if($upload_file == '0'){
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Sensa::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_stotex'){
                    if($upload_file == '0'){
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Stotex::execute($partner_id,$kurs,$extension);

                        $response['success'] = 1;
                    }
                }
                elseif($import_name == 'import_sapin'){
                    if($upload_file == '0'){
                        $response['message'] = '<p class="error">Izaberite fajl!</p>';
                    }else{
                        Sapin::execute($partner_id,$kurs,$extension);
                        $response['success'] = 1;
                    }
                }

                else{
                    if(File::exists('files/import.'.$extension)){
                        File::delete('files/import.'.$extension);
                    }else{
                        $response['message'] = "<p class='error'>Ovaj web import nije podrzan!</p>" ;
                    }
                }

                if($response['success'] == 1){
                    if(AdminOptions::gnrl_options(3030)==1){
                        if($roba_insert == 1){
                            $roba_ids = AdminImport::instant_import_to_roba($partner_id);
                            AdminImport::saveArticlesImages($roba_ids);
                        }else{
                            AdminImport::dc_mapped_import($partner_id);
                        }
                    }

                    AdminSupport::saveLog('WEB_IMPORT_IMPORTUJ_CENOVNIK');
                }else{
                    $response['message'] = "<p class='error'>Import nije izvršen!</p>" ;
                }
            }
            catch (Exception $e){
                
                Support::initQueryExecute('UTF8');

                if(File::exists('files/import.'.$extension)){
                    File::delete('files/import.'.$extension);
                }

                // AdminSupport::saveLog('IMPORT_GRESKA', $partner_id);
                $response['message'] = "<p class='error'>Doslo je do greske prilikom importa!<br>".$e->getMessage()."</p>";
            }

        }else{
            $response['message'] = "<p class='error'>Popunite sva potrebna polja!</p>";
        }
        return json_encode($response);
    }

    function auto_full_import($key,$roba_insert=0)
    {
        $auto_imports = DB::table('dobavljac_cenovnik_kolone')->select('partner_id')->where('auto_import',1)->get();
        $error_check = false;
        $report = '';
        foreach($auto_imports as $import){
            $partner_id = $import->partner_id;
            $partner = DB::table('partner')->where('partner_id',$partner_id)->pluck('naziv');
            try{
                if(Support::checkPartner($partner_id,'import_ewe')){
                    Ewe::execute($partner_id);
                }
                
                elseif(Support::checkPartner($partner_id,'import_velteh')){
                    Velteh::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_epi')){
                    Epi::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_kimtec')){
                    Kimtec::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_ct')){
                    CT::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_asbis')){
                    Asbis::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_tnt')){
                    Tnt::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_pccentar')){
                    PCCentar::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_moda')){
                    Moda::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_gama')){
                    Gama::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_dudico')){
                    Dudico::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_zoomimpexxml')){
                    ZoomImpexXML::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_element')){
                    Element::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_mogly')){
                    Mogly::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_cobratoys')){
                    CobraToys::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_westaco')){
                    Westaco::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_yuglob')){
                    Yuglob::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_kosa')){
                    Kosa::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_laluco')){
                    Laluco::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_happydog')){
                    HappyDog::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_xplorer')){
                    Xplorer::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_audioplaneta')){
                    AudioPlaneta::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_agromarket')){
                    Agromarket::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_zorbal')){
                    Zorbal::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_atompartner')){
                    AtomPartner::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_bosh')){
                    Bosh::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_capriolo')){
                    Capriolo::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_ctexcel')){
                    CTexcel::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_ctcunit')){
                    CTCUnit::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_digimax')){
                    Digimax::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_ds')){
                    DS::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_euromdenis')){
                    EuromDenis::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_formavs')){
                    FormaVs::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_gume')){
                    Gume::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_pirot')){
                    Pirot::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_profitech')){
                    Profitech::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_keno')){
                    Keno::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_keprom')){
                    Keprom::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_kimtecexcel')){
                    KimtecExcel::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_mcg')){
                    Mcg::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_milenijum')){
                    Milenijum::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_mison')){
                    Mison::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_oxygen')){
                    Oxygen::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_pccentarexcel')){
                    PCCentarExcel::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_petprotector')){
                    PetProtector::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_pincomputers')){
                    PinComputers::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_computerland')){
                    Computerland::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_roaming')){
                    Roaming::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_sbalsan')){
                    Sbalsan::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_svetlostmka')){
                    SvetlostMka::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_mka')){
                    MKA::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_telit')){
                    Telit::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_tsmod')){
                    TsMod::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_nelt')){
                    Nelt::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_vox')){
                    Vox::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_whirlpool')){
                    Whirlpool::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_mobilnekamere')){
                    MobilneKamere::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_woobyhouse')){
                    Woobyhouse::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_zoomimpex')){
                    ZoomImpex::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_ntcompany')){
                    NTCompany::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_refot')){
                    Refot::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_virtikom')){
                    Virtikom::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_wolfcraft')){
                    Wolfcraft::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_bebindo')){
                    Bebindo::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_recaro')){
                    Recaro::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_vana')){
                    Vana::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_keller')){
                    Keller::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_asbisold')){
                    AsbisOld::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_ctxml')){
                    CTXml::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_svetlostvka')){
                    SvetlostVka::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_gorenjemka')){
                    GorenjeMka::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_gorenjevka')){
                    GorenjeVka::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_ibg')){
                    Ibg::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_ofy')){
                    OFY::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_gembird')){
                    Gembird::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_josipovic')){
                    Josipovic::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_printcom')){
                    Printcom::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_venco')){
                    Venco::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_homejungle')){
                    HomeJungle::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_ekka')){
                    Ekka::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_domel')){
                    Domel::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_demark')){
                    Demark::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_malikucniaparati')){
                    MaliKucniAparati::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_bcgroup')){
                    BCGroup::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_sensa')){
                    Sensa::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_stotex')){
                    Stotex::execute($partner_id);
                }
                elseif(Support::checkPartner($partner_id,'import_sapin')){
                    Sapin::execute($partner_id);
                }

                if(AdminOptions::gnrl_options(3030)==1){
                    if($roba_insert == 1){
                        $roba_ids = AdminImport::instant_import_to_roba($partner_id);
                        AdminImport::saveArticlesImages($roba_ids);
                    }else{
                        AdminImport::dc_mapped_import($partner_id);
                    }
                }


                $report .= $partner.": OK, \r\n";
            }catch(Exception $e){

                Support::initQueryExecute('UTF8');

                $error_check = true;
                $report .= $partner.": GREŠKA, \r\n"; 

                //$body = 'Doslo je do greske prilikom automatskog importa: '.$e->getMessage();
                // Support::sendMail($body,$partner); 
                
            }

        }
            Support::sendMail($report);
        // if($error_check){ 
            $myfile = fopen("files/logs/full_import.txt", "a");
            $line = date("d-m-Y H:i:s").'   '.trim($report)."\r\n";
            fwrite($myfile, $line);
            fclose($myfile);
        // }
    }

    function auto_short_import($key, $type = null)
    {        
        $auto_imports = DB::table('dobavljac_cenovnik_kolone')->select('partner_id')->where('auto_import_short',1)->get();
        $error_check = false;
        $report = '';
        foreach($auto_imports as $import){
            $partner_id = $import->partner_id;
            $partner = DB::table('partner')->where('partner_id',$partner_id)->pluck('naziv');

            try{
                if(Support::checkPartner($partner_id,'import_ewe')){
                    Ewe::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_computerland')){
                    Computerland::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_velteh')){
                    Velteh::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_epi')){
                    Epi::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_kimtec')){
                    try {
                        Kimtec::executeShort($partner_id);
                        Support::queryShortExecute($partner_id, $type);
                    } catch (Exception $e) {
                    }
                }
                elseif(Support::checkPartner($partner_id,'import_ct')){
                    $success = CT::executeShort($partner_id);
                    if($success){
                        Support::queryShortExecute($partner_id, $type);
                        $report .= $partner.": OK, \r\n";
                    }else{
                        $report .= $partner.": GREŠKA (POSLAT PRAZAN FAJL), \r\n";
                    }
                }
                elseif(Support::checkPartner($partner_id,'import_asbis')){
                    Asbis::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_tnt')){
                    Tnt::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_pccentar')){
                    PCCentar::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_moda')){
                    Moda::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_gama')){
                    Gama::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_dudico')){
                    Dudico::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_zoomimpexxml')){
                    ZoomImpexXML::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_element')){
                    Element::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_mogly')){
                    Mogly::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_cobratoys')){
                    CobraToys::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_westaco')){
                    Westaco::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_yuglob')){
                    Yuglob::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_kosa')){
                    Kosa::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_laluco')){
                    Laluco::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_happydog')){
                    HappyDog::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_xplorer')){
                    Xplorer::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_audioplaneta')){
                    AudioPlaneta::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_agromarket')){
                    Agromarket::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_zorbal')){
                    Zorbal::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_atompartner')){
                    AtomPartner::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_bosh')){
                    Bosh::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_capriolo')){
                    Capriolo::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_ctexcel')){
                    CTexcel::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_ctcunit')){
                    CTCUnit::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_digimax')){
                    Digimax::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_ds')){
                    DS::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_euromdenis')){
                    EuromDenis::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_formavs')){
                    FormaVs::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_gume')){
                    Gume::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_pirot')){
                    Pirot::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_profitech')){
                    Profitech::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_keno')){
                    Keno::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_keprom')){
                    Keprom::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_kimtecexcel')){
                    KimtecExcel::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_mcg')){
                    Mcg::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_milenijum')){
                    Milenijum::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_mison')){
                    Mison::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_oxygen')){
                    Oxygen::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_pccentarexcel')){
                    PCCentarExcel::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_petprotector')){
                    PetProtector::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_pincomputers')){
                    PinComputers::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_computerland')){
                    Computerland::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_roaming')){
                    Roaming::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_sbalsan')){
                    Sbalsan::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_svetlostmka')){
                    SvetlostMka::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_mka')){
                    MKA::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_telit')){
                    Telit::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_tsmod')){
                    TsMod::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_nelt')){
                    Nelt::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_vox')){
                    Vox::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_whirlpool')){
                    Whirlpool::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_mobilnekamere')){
                    MobilneKamere::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_woobyhouse')){
                    Woobyhouse::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_zoomimpex')){
                    ZoomImpex::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_ntcompany')){
                    NTCompany::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_refot')){
                    Refot::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_virtikom')){
                    Virtikom::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_wolfcraft')){
                    Wolfcraft::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_bebindo')){
                    Bebindo::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_recaro')){
                    Recaro::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_vana')){
                    Vana::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_keller')){
                    Keller::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_asbisold')){
                    AsbisOld::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_ctxml')){
                    CTXml::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_svetlostvka')){
                    SvetlostVka::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_gorenjemka')){
                    GorenjeMka::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_gorenjevka')){
                    GorenjeVka::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_ibg')){
                    Ibg::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_gembird')){
                    Gembird::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_ofy')){
                    OFY::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_josipovic')){
                    Josipovic::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_venco')){
                    Venco::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_homejungle')){
                    HomeJungle::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_ekka')){
                    Ekka::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_domel')){
                    Domel::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_demark')){
                    Demark::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_malikucniaparati')){
                    MaliKucniAparati::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_bcgroup')){
                    BCGroup::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_sensa')){
                    Sensa::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_stotex')){
                    Stotex::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                elseif(Support::checkPartner($partner_id,'import_sapin')){
                    Sapin::executeShort($partner_id);
                    Support::queryShortExecute($partner_id, $type);
                }
                if(!Support::checkPartner($partner_id,'import_ct')){
                    $report .= $partner.": OK, \r\n";
                }
            }catch(Exception $e){
                
                Support::initQueryExecute('UTF8');

                $error_check = true;
                $report .= $partner.": GREŠKA, \r\n";

                // $body = 'Doslo je do greske prilikom automatskog azuriranja cena i kolicina: '.$e->getMessage();
                // Support::sendMail($body,$partner);                               
            }
        }

        $roba_ids= DB::table('roba')->where('dobavljac_id',131808)->whereNull('energetska_klasa')->get();
        foreach ($roba_ids as $roba_id) {
            
        $klase = DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id',$roba_id->roba_id)->where('karakteristika_naziv','Energetska klasa')->pluck('karakteristika_vrednost');
        DB::table('roba')->where('dobavljac_id',131808)->where('roba_id',$roba_id->roba_id)->update(array('energetska_klasa'=>$klase));
        }
        //Support::autoQueryExecute();
        if(isset($type) && $type == 'lager') {
            Support::autoLager();
        } else if(isset($type) && $type == 'cene') {
            Support::autoCene();
        } else {
            Support::autoQueryExecute();
        }
           
        // if($error_check){        
            $myfile = fopen("files/logs/short_import.txt", "a");
            $line = date("d-m-Y H:i:s").'   '.trim($report)."\r\n";
            fwrite($myfile, $line);
            fclose($myfile);
        // }
    }

    function auto_import_3() {
        // Sklanja artikle sa web-a
        DB::table('dobavljac_cenovnik')->update(['flag_prikazi_u_cenovniku' => 0]);

        //Selectovanje IDs
        $s = DB::select('SELECT dobavljac_cenovnik_id, povezan, kolicina, web_cena, web_marza, cena_nc, pmp_cena, flag_prikazi_u_cenovniku, mp_marza, mpcena, flag_aktivan, flag_zakljucan, cena_a, a_marza FROM dobavljac_cenovnik dc WHERE dobavljac_cenovnik_id <> -1 AND flag_aktivan = 1 AND kolicina > 0 AND dc.roba_id <> -1 AND flag_zakljucan = 0 AND dc.dobavljac_cenovnik_id = (SELECT dobavljac_cenovnik_id FROM dobavljac_cenovnik WHERE roba_id = dc.roba_id AND kolicina > 0 ORDER BY cena_nc ASC LIMIT 1)');

        foreach ($s as $row) {
            $ids[] = $row->dobavljac_cenovnik_id;
        }
        
        // NE GLEDA KOLIČINU
        $bez_kolicine = DB::select('SELECT dobavljac_cenovnik_id FROM dobavljac_cenovnik dc WHERE dobavljac_cenovnik_id <> -1 AND flag_aktivan = 1 AND dc.roba_id <> -1 AND flag_zakljucan = 0');

        foreach ($bez_kolicine as $row) {
            $ids_bez_kolicine[] = $row->dobavljac_cenovnik_id;
        }
        
        // Preračunavanje cena
        $skip_ids = DB::table('partner')->select('partner_id')->where('preracunavanje_cena', 0)->get();
        
        foreach ($ids as $id) {
            DB::statement("UPDATE dobavljac_cenovnik dc SET web_cena = cena_nc*(1+web_marza/100)*(SELECT 1+porez/100 FROM tarifna_grupa WHERE tarifna_grupa_id = dc.tarifna_grupa_id), mpcena = cena_nc*(1+mp_marza/100)*(SELECT 1+porez/100 FROM tarifna_grupa WHERE tarifna_grupa_id = dc.tarifna_grupa_id) WHERE dobavljac_cenovnik_id = ".$id." AND flag_zakljucan = 0 AND partner_id NOT IN (SELECT partner_id FROM partner WHERE preracunavanje_cena = 0)");
        }

        // Prihvatanje cena
        $new_ids = DB::table('dobavljac_cenovnik')->select('roba_id')->distinct()->where(array('flag_aktivan'=>1,'flag_zakljucan'=>0,'povezan'=>1))->whereIn('dobavljac_cenovnik_id',$ids)->where('roba_id','!=',-1)->where('web_cena','!=',0)->get();
        
        $partner_id = 0;
        $is_kolicina = 1;
        $is_cena = 1;

        foreach ($new_ids as $row) {
            $roba_id = $row->roba_id;
            $same_obj = DB::table('dobavljac_cenovnik')->select('dobavljac_cenovnik_id')->where('roba_id',$roba_id)->where('web_cena','!=',0);
            if($same_obj->count() > 0){
                if($is_kolicina==1){
                    if(DB::table('dobavljac_cenovnik')->where('roba_id',$roba_id)->where('web_cena','!=',0)->where('kolicina','>',0)->count()>0){
                        $same_obj->where('kolicina','>',0);
                    }
                }
                if($is_cena==1){
                    $same_obj->orderBy('web_cena','asc');
                }
                $dobavljac_cenovnik_id = $same_obj->first()->dobavljac_cenovnik_id;

                DB::statement("UPDATE roba r SET racunska_cena_nc=dc.cena_nc,racunska_cena_a=dc.cena_a,racunska_cena_end=dc.cena_end,mpcena=dc.mpcena,web_cena=dc.web_cena,promena=1, a_marza=dc.a_marza, end_marza=dc.end_marza, mp_marza=dc.mp_marza, web_marza=dc.web_marza, dobavljac_id = dc.partner_id, sifra_d = dc.sifra_kod_dobavljaca FROM dobavljac_cenovnik dc WHERE dc.dobavljac_cenovnik_id = ".$dobavljac_cenovnik_id." AND r.roba_id = ".$roba_id."");
            }
        }

        // Prihvatanje lagera
        $lager_ids = DB::table('dobavljac_cenovnik')->select('roba_id')->distinct()->whereIn('dobavljac_cenovnik_id', $ids_bez_kolicine)->where('roba_id','!=',-1)->where('flag_zakljucan','!=',1)->get();
        $new_roba_ids_lager = array();
        $godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
        $magacin_id = 1;

        //  STAVLJAMO LAGER NA 0 KOD SVIH ARTIKALA
        //DB::statement("UPDATE lager l SET kolicina = 0");

        foreach ($lager_ids as $row) {
            $roba_id = $row->roba_id;
            $same_obj = DB::table('dobavljac_cenovnik')->select('dobavljac_cenovnik_id')->where('roba_id',$roba_id);
            $new_roba_ids_lager[] = $roba_id;

            if($same_obj->count() > 0){
            
                if(DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$godina_id,'orgj_id'=>1))->count() > 0){
                    DB::statement("UPDATE lager l SET kolicina=dcn.kolicina FROM (SELECT roba_id, SUM(kolicina) AS kolicina FROM dobavljac_cenovnik dc WHERE dc.roba_id=".$roba_id." AND dc.flag_zakljucan=0 GROUP BY dc.roba_id) dcn WHERE l.roba_id=dcn.roba_id");
                }else{
                    DB::statement("INSERT INTO lager (roba_id,orgj_id,poslovna_godina_id,kolicina) SELECT dcn.roba_id, ".$magacin_id.", ".$godina_id.",dcn.kolicina FROM (SELECT roba_id, SUM(kolicina) AS kolicina FROM dobavljac_cenovnik dc WHERE dc.roba_id=".$roba_id." AND dc.flag_zakljucan=0 GROUP BY dc.roba_id) dcn");
                }
            }
        }

        // Prikazivanje artikala na web-u
        $roba_ids = DB::table('dobavljac_cenovnik')->select('roba_id')->distinct()->where(array('flag_aktivan'=>1,'flag_zakljucan'=>0))->whereIn('dobavljac_cenovnik_id',$ids)->where('roba_id','!=',-1)->where('web_cena','!=',0)->get();

        foreach ($ids as $id) {
            DB::statement("UPDATE dobavljac_cenovnik SET flag_prikazi_u_cenovniku = 1 WHERE dobavljac_cenovnik_id = ".$id." AND flag_zakljucan = 0");
        }

        $roba_id = array();
        foreach ($roba_ids as $row) {
            $roba_id[] = $row->roba_id;
        }
        $roba_ids_string = implode(", ", $roba_id);

        DB::statement("UPDATE roba SET flag_prikazi_u_cenovniku = 0 WHERE roba_id NOT IN (".$roba_ids_string.") AND flag_zakljucan = false");
        DB::statement("UPDATE roba SET flag_prikazi_u_cenovniku = 1 WHERE roba_id IN (".$roba_ids_string.") AND flag_zakljucan = false");

        Support::sendMail($content);
    }



}
