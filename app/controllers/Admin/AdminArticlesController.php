<?php
use Service\TranslatorService;
use Service\ElasticSearchService;
use Service\Drip;

class AdminArticlesController extends Controller {

    function artikli($grupa_pr_id, $proizvodjac = null, $dobavljac = null, $tip = null, $labela = null, $karakteristika = null, $magacin = null, $exporti = null, $katalog = null,$filteri = null, $flag = null, $search = null, $nabavna = null, $order = null, $garancije = null)
    {
            $criteria = array("grupa_pr_id" => $grupa_pr_id, "proizvodjac"=>$proizvodjac, "karakteristika"=>$karakteristika, "dobavljac"=>$dobavljac, "tip"=>$tip, "labela"=>$labela,"magacin"=>$magacin, "exporti"=>$exporti, "katalog"=>$katalog, "filteri"=>$filteri, "flag"=>$flag, "nabavna"=>$nabavna, "search"=>$search, "garancije"=>$garancije);
            $limit = AdminOptions::limit_liste_robe();
            $pagination = array(
                'limit' => $limit,
                'offset' => Input::get('page') ? (Input::get('page')-1)*$limit : 0
                );
            $artikli = AdminArticles::fetchAll($criteria, $pagination, $order);
            $grupa_title = AdminGroups::find($grupa_pr_id,'grupa');
            $all_articles = array_map('current',AdminArticles::fetchAll($criteria));
            $count = count($all_articles);

            $grupa_parents = array();
            AdminSupport::grupaParents($grupa_pr_id,$grupa_parents);
            date_default_timezone_get();
            $time = date('Y-m-d');
            $data=array(
                "strana"=>'artikli',
                "title"=> $grupa_pr_id != 0 ? 'Artikli iz kategorije '.$grupa_title : 'Artikli iz svih kategorija',
                "grupa_pr_id"=>$grupa_pr_id,
                "grupa_parents"=>$grupa_parents,
                "naziv_grupe"=>$grupa_pr_id != 0 ? $grupa_title : 'Artikli iz svih grupa',
                "criteria"=>$criteria,
                "articles"=>$artikli,
                "count_products"=>$count,
                "limit"=>$limit,
                "time"=>$time,
                "all_ids" => json_encode($all_articles),
                "naziv_width" => AdminOptions::admin_artikli_kolone('naziv','width')
            );
  
            return View::make('admin/page', $data);
    }

    function product($id,$clone_id=null)
    {
        isset($clone_id) ? $id_exe = $clone_id : $id_exe = $id;

            $data=array(
                "strana"=>'product',
                "title"=>$id_exe != 0 ? AdminArticles::find($id_exe, 'naziv') : 'novi artikal',
                "naziv"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'naziv') : null, 
                "naziv_web"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'naziv_web') : null,
                "flag_usluga"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'flag_usluga') : null,
                "grupa_pr_grupa"=> $id_exe != 0 ? AdminGroups::find(AdminArticles::find($id_exe,'grupa_pr_id'),'grupa') : null,
                "proizvodjac"=> $id_exe != 0 ? DB::table('proizvodjac')->where('proizvodjac_id',AdminArticles::find($id_exe, 'proizvodjac_id'))->pluck('naziv') : null,
                "jedinica_mere"=> $id_exe != 0 ? DB::table('jedinica_mere')->where('jedinica_mere_id',AdminArticles::find($id_exe, 'jedinica_mere_id'))->pluck('naziv') : null,
                "roba_flag_cene"=> $id_exe != 0 ? DB::table('roba_flag_cene')->where('roba_flag_cene_id',AdminArticles::find($id_exe, 'roba_flag_cene_id'))->pluck('naziv') : null,
                "tip_artikla"=> $id_exe != 0 ? DB::table('tip_artikla')->where('tip_artikla_id',AdminArticles::find($id_exe, 'tip_cene'))->pluck('naziv') : null,
                "roba_grupe"=> $id_exe != 0 ? AdminGroups::moreGroups($id_exe) : array(),
                "web_cena"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'web_cena')) : null,
                "kolicina"=> $id_exe != 0 ? round(AdminArticles::find($id_exe, 'kolicina',DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'))) : 0,
                "flag_aktivan"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'flag_aktivan') : 1,
                "flag_prikazi_u_cenovniku"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'flag_prikazi_u_cenovniku') : 1,
                "flag_zakljucan" => $id_exe != 0 ? AdminArticles::find($id_exe, 'flag_zakljucan') : false,
                "dobavljac_id"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'dobavljac_id') : null,
                "sifra_d"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'sifra_d') : null,
                "sifra_is"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'sifra_is') : null,
                "model"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'model') : '',
                "tags"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'tags') : null,
                "energetska_klasa"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'energetska_klasa') : null,
                "energetska_klasa_link"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'energetska_klasa_link') : null,
                "check_link"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'check_link') : null,
                "check_old_class"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'check_old_class') : null,
                "tezinski_faktor"=> $id_exe != 0 ? round(AdminArticles::find($id_exe, 'tezinski_faktor'),2) : null,
                "visina"=> $id_exe != 0 ? round(AdminArticles::find($id_exe, 'visina'),2) : null,
                "sirina"=> $id_exe != 0 ? round(AdminArticles::find($id_exe, 'sirina'),2) : null,
                "duzina"=> $id_exe != 0 ? round(AdminArticles::find($id_exe, 'duzina'),2) : null,
                "bruto_masa"=> $id_exe != 0 ? round(AdminArticles::find($id_exe, 'bruto_masa'),2) : null,
                "jm_mase"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'jm_mase') : null,
                "tarifna_grupa_id"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'tarifna_grupa_id') : 2,
                "garancija"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'garancija') : null,
                "sku"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'sku') : null,
                "barkod"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'barkod') : null,
                "design_id"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'design_id') : null,
                "mpcena"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'mpcena')) : null,
                "racunska_cena_nc"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'racunska_cena_nc')) : null,
                "web_marza"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'web_marza'),2) : 0,
                "mp_marza"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'mp_marza'),2) : 0,
                "orgj_id"=> DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'),
                "web_flag_karakteristike"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'web_flag_karakteristike') : 0,
                "osobine"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'osobine') : 0,
                "flag_ambalaza"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'flag_ambalaza') : 0,
                "ambalaza"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'ambalaza') : 0,
                "produzena_garancija"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'produzena_garancija') : 0,
                "rbr"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'rbr') : null,
                "racunska_cena_end"=>$id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'racunska_cena_end')) : null,
                "end_marza"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'end_marza'),2) : 0,
                "clone_id"=> $clone_id,
                "roba_id"=> $id
            );

        return View::make('admin/page', $data);

    }

    function product_edit()
    {
            $data = Input::get();
            $rules = array(
                'naziv_web' => 'required|regex:'.AdminSupport::regex().'|between:1,200|unique:roba,naziv_web,'.$data['roba_id'].',roba_id',
                //'sku' => 'regex:'.AdminSupport::regex().'|max:255|unique:roba,sku,'.$data['roba_id'].',roba_id',
                'sifra_d' => 'max:100',
                'garancija' => 'numeric|digits_between:1,7',
                'racunska_cena_nc' => 'numeric|digits_between:1,15',
                'racunska_cena_end' => 'numeric|digits_between:1,15',
                'mpcena' => 'numeric|digits_between:1,15',
                'mp_marza' => 'required|numeric|digits_between:1,7',
                'end_marza' => 'required|numeric|digits_between:1,7',
                'web_cena' => 'numeric|digits_between:1,15',
                'web_marza' => 'required|numeric|digits_between:1,7',
                'kolicina' => 'numeric|digits_between:1,7',
                'model' => 'regex:'.AdminSupport::regex().'|max:100',
                'barkod' => 'regex:'.AdminSupport::regex().'|max:100',
                'tezinski_faktor' => 'numeric|digits_between:1,15',
                'ambalaza' => 'numeric|digits_between:1,16',
                'grupa_pr_grupa' => 'regex:'.AdminSupport::regex().'|max:100',
                'proizvodjac' => 'regex:'.AdminSupport::regex().'|max:100',
                'jedinica_mere' => 'regex:'.AdminSupport::regex().'|max:100',
                'roba_flag_cene' => 'regex:'.AdminSupport::regex().'|max:100',
                'tip_artikla' => 'regex:'.AdminSupport::regex().'|max:100',
                'rbr' => 'integer|digits_between:1,7'
            );
            $validator_messages = array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Polje sadrži neodgovarajuće karaktere!',
                'email' => 'E-mail adresa je neodgovarajuća!',
                'between' => 'Dužina sadžaja nije odgovarajuća!',
                'unique' => 'Polje je već zauzeto!',
                'max'=>'Dužina sadžaja je neodgovarajuća!',
                'numeric'=>'Polje može sadržati samo brojeve!',
                'digits_between' => 'Dužina sadžaja nije odgovarajuća!',
                'integer' => 'Polje može sadržati samo brojeve!'
            );
            $validator = Validator::make($data, $rules, $validator_messages);
            if($validator->fails()){
                return Redirect::to(AdminOptions::base_url().'admin/product/'.$data['roba_id'])->withInput()->withErrors($validator->messages());
            }else{
                if($data['roba_id'] == 0 && AdminOptions::gnrl_options(3025) == 1 && DB::table('roba')->count() >= AdminOptions::gnrl_options(3026)){
                    return Redirect::back()->with('limit_message',true);
                }

                if(isset($data['flag_aktivan']) && $data['flag_aktivan'] == 0){
                    $data['flag_prikazi_u_cenovniku'] = 0;
                }

                if($data['clone_id'] != 0){
                    $clone_id = $data['clone_id'];
                }
                $slike_clone = false;
                if(isset($data['slike_clone'])){
                    $slike_clone = true;
                }

                unset($data['clone_id']);
                unset($data['slike_clone']);

                $data['grupa_pr_id'] = AdminGroups::grupa_pr_id($data['grupa_pr_grupa']);
                $data['proizvodjac_id'] = AdminSupport::proizvodjac_id($data['proizvodjac']);
                $data['jedinica_mere_id'] = $data['jedinica_mere'] == '' ? 1 : AdminSupport::jedinica_mere_id($data['jedinica_mere']);
                $data['roba_flag_cene_id'] = AdminSupport::roba_flag_cene_id($data['roba_flag_cene']);
                $data['tip_cene'] = $data['tip_artikla'] == '' ? null : AdminSupport::tip_artikla_id($data['tip_artikla']);
                unset($data['grupa_pr_grupa']);
                unset($data['proizvodjac']);
                unset($data['jedinica_mere']);
                unset($data['roba_flag_cene']);
                unset($data['tip_artikla']);

                if(!isset($data['roba_grupe'])){
                    $data['roba_grupe'] = array();
                }
                foreach($data['roba_grupe'] as $key => $grupa_id){
                    if($data['grupa_pr_id'] == $grupa_id){
                        unset($data['roba_grupe'][$key]);
                    }
                }
                AdminArticles::saveMoreGroups($data['roba_id'],$data['roba_grupe']);
                unset($data['roba_grupe']);

                $roba_id = AdminArticles::save($data);

                $check_link = input::get('check_link');
                $link = Input::get('energetska_klasa_link');
                if(Input::hasFile('energetska_klasa_pdf')){    
                    $pdf = Input::file('energetska_klasa_pdf');
                    $originalName = $pdf->getClientOriginalName();
                    $extension = $pdf->getClientOriginalExtension();
                }
                if($check_link == 0) {
                    unset($link);
                    if(Input::hasFile('energetska_klasa_pdf')){    
                        $pdf->move('images/energetska_klasa/',$roba_id.'-'.$originalName);
                        DB::table('roba')->where('roba_id', $roba_id)->update(['energetska_klasa_pdf' => 'images/energetska_klasa/'.$roba_id.'-'.$originalName]);
                    }
                } elseif($check_link == 1) {
                    unset($pdf);
                    DB::table('roba')->where('roba_id', $roba_id)->update(['energetska_klasa_link' => $link]);
                }

                if(isset($clone_id)){
                    if($slike_clone){
                        AdminArticles::cloneImages($clone_id,$roba_id);
                    }
                    AdminArticles::cloneKarak($clone_id,$roba_id);
                }

                if(Options::gnrl_options(3055) == 1 && Options::gnrl_options(3056) == 0){
                    $elastic = new ElasticSearchService();
                    $elastic->updateGroups();
                    $elastic->updateManufacturers();
                    $elastic->updateArticles();
                }

                $message="Uspešno ste sačuvali podatke";
                return Redirect::to(AdminOptions::base_url().'admin/product/'.$roba_id)->with('message_success',$message);
           }
  
    }

    function product_short($id,$clone_id=null)
    {
        if(AdminOptions::gnrl_options(3029)==0){
            return Redirect::to(AdminOptions::base_url().'admin/product/'.$id);
        }

        isset($clone_id) ? $id_exe = $clone_id : $id_exe = $id;

            $data=array(
                "strana"=>'product_short',
                "title"=>$id_exe != 0 ? AdminArticles::find($id_exe, 'naziv') : 'novi artikal',
                "naziv"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'naziv') : null,
                "naziv_web"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'naziv_web') : null,
                "grupa_pr_id"=> $id_exe != 0 ? AdminArticles::find($id_exe,'grupa_pr_id') : null,
                "grupa_pr_grupa"=> $id_exe != 0 ? AdminGroups::find(AdminArticles::find($id_exe,'grupa_pr_id'),'grupa') : null,
                "jedinica_mere_id"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'jedinica_mere_id') : 0,
                "orgj_id"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'orgj_id') : DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'),
                "roba_flag_cene"=> $id_exe != 0 ? DB::table('roba_flag_cene')->where('roba_flag_cene_id',AdminArticles::find($id_exe, 'roba_flag_cene_id'))->pluck('naziv') : null,
                "tip_artikla"=> $id_exe != 0 ? DB::table('tip_artikla')->where('tip_artikla_id',AdminArticles::find($id_exe, 'tip_cene'))->pluck('naziv') : null,
                "akcija_flag_primeni"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'akcija_flag_primeni') : 0,
                "web_cena"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'web_cena')) : null,
                "kolicina"=> $id_exe != 0 ? round(AdminArticles::find($id_exe, 'kolicina',DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id'))) : 0,
                "flag_prikazi_u_cenovniku"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'flag_prikazi_u_cenovniku') : 1,
                "tarifna_grupa_id"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'tarifna_grupa_id') : 2,
                "akcijska_cena"=> $id_exe != 0 ? AdminSupport::cena_round(AdminArticles::find($id_exe, 'akcijska_cena'),2) : 0,
                "web_opis"=> $id_exe != 0 ? AdminArticles::find($id_exe, 'web_opis') : null,
                "datum_akcije_od"=> $id != 0 ? AdminArticles::find($id, 'datum_akcije_od') : null,
                "datum_akcije_do"=> $id != 0 ? AdminArticles::find($id, 'datum_akcije_do') : null,
                "sku"=> $id != 0 ? AdminArticles::find($id, 'sku') : null,
                "clone_id"=> $clone_id,
                "roba_id"=> $id
            );

        return View::make('admin/page', $data);

    }

    function product_edit_short()
    {
            $data = Input::get();
            $rules = array(
                'naziv_web' => 'required|regex:'.AdminSupport::regex().'|between:1,200|unique:roba,naziv_web,'.$data['roba_id'].',roba_id',
                'web_cena' => 'numeric|digits_between:1,20',
                'kolicina' => 'numeric|digits_between:1,10',
                //'sku' => 'regex:'.AdminSupport::regex().'|max:255|unique:roba,sku,'.$data['roba_id'].',roba_id',
                'grupa_pr_grupa' => 'regex:'.AdminSupport::regex().'|max:100',
                'roba_flag_cene' => 'regex:'.AdminSupport::regex().'|max:100',
                'tip_artikla' => 'regex:'.AdminSupport::regex().'|max:100'
            );
            $validator_messages = array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Polje sadrži neodgovarajuće karaktere!',
                'email' => 'E-mail adresa je neodgovarajuća!',
                'between' => 'Dužina sadžaja nije odgovarajuća!',
                'unique' => 'Vrednost već postoji u bazi!',
                'max'=>'Dužina sadžaja je neodgovarajuća!',
                'numeric'=>'Polje može sadržati samo brojeve!',
                'digits_between' => 'Dužina sadžaja nije odgovarajuća!'
            );
            $validator = Validator::make($data, $rules, $validator_messages);
            if($validator->fails()){
                if(!Request::ajax()){
                    return Redirect::to(AdminOptions::base_url().'admin/product-short/'.$data['roba_id'])->withInput()->withErrors($validator->messages());
                }else{
                    return Response::json(array('success_message' => "", 'error_messages' => $validator->messages()));
                }
            }else{
                if($data['roba_id'] == 0 && AdminOptions::gnrl_options(3025) == 1 && DB::table('roba')->count() >= AdminOptions::gnrl_options(3026)){
                    if(!Request::ajax()){
                        return Redirect::back()->with('limit_message',true);
                    }else{
                        return Response::json(array('success_message' => '', 'error_messages' => array()));
                    }
                }
                if(isset($data['flag_aktivan']) && $data['flag_aktivan'] == 0){
                    $data['flag_prikazi_u_cenovniku'] = 0;
                }

                if($data['clone_id'] != 0){
                    $clone_id = $data['clone_id'];
                }
                $slike_clone = false;
                if(isset($data['slike_clone'])){
                    $slike_clone = true;
                }
                unset($data['clone_id']);
                unset($data['slike_clone']);


                if($data['roba_id'] != 0 && isset($data['akcija']) && is_numeric($data['akcija'])){
                    DB::table('web_slika')->where('roba_id',$data['roba_id'])->update(array('akcija'=>0));
                    DB::table('web_slika')->where('web_slika_id',$data['akcija'])->update(array('akcija'=>1));
                }
                unset($data['akcija']);
                
                // $grupa_pr_vrednost_ids = array();
                // foreach($data as $name => $input){
                //     if(strpos($name,'grupa_pr_naziv_id_') !== false){
                //         $grupa_pr_vrednost_ids[] = $input;
                //         unset($data[$name]);
                //     }
                // }
                unset($data['slika']);


                $data['grupa_pr_id'] = AdminGroups::grupa_pr_id((Request::ajax() ? AdminLanguage::convert($data['grupa_pr_grupa'],AdminLanguage::lang()) : $data['grupa_pr_grupa']));
                $data['roba_flag_cene_id'] = AdminSupport::roba_flag_cene_id((Request::ajax() ? AdminLanguage::convert($data['roba_flag_cene'],AdminLanguage::lang()) : $data['roba_flag_cene']));
                $data['tip_cene'] = $data['tip_artikla'] == '' ? null : AdminSupport::tip_artikla_id((Request::ajax() ? AdminLanguage::convert($data['tip_artikla'],AdminLanguage::lang()) : $data['tip_artikla']));
                unset($data['grupa_pr_grupa']);
                unset($data['roba_flag_cene']);
                unset($data['tip_artikla']);

                $roba_id = AdminArticles::save($data);

                // //karakteristike
                // $old_vrednost_ids = array_map('current',DB::table('web_roba_karakteristike')->select('grupa_pr_vrednost_id')->where('roba_id',$roba_id)->get());
                // $delete_vrednost_ids = array_diff($old_vrednost_ids, $grupa_pr_vrednost_ids); 
                // $new_vrednost_ids = array_diff($grupa_pr_vrednost_ids, $old_vrednost_ids);
                // DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->whereIn('grupa_pr_vrednost_id',$delete_vrednost_ids)->delete();
                // foreach($new_vrednost_ids as $key => $new_vrednost_id){
                //     $grupa_pr_vrednost = DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id',$new_vrednost_id)->first();
                //     DB::table('web_roba_karakteristike')->insert(array('roba_id'=>$roba_id,'grupa_pr_vrednost_id'=>$new_vrednost_id,'grupa_pr_naziv_id'=>$grupa_pr_vrednost->grupa_pr_naziv_id,'vrednost'=>$grupa_pr_vrednost->naziv,'rbr'=>($key+1)));
                // }

                //slike
                if(count(Input::file('slike'))>0){
                    $images = Input::file('slike');
                    $max_image_size = 10000;
                    $max_images = 10;
                    $images_success = true;

                    foreach($images as $image){
                        $image_validator = Validator::make(
                            array('slika' => $image),
                            array('slika' => 'required|mimes:jpg,png,jpeg,webp|max:'.strval($max_image_size)),
                            array(
                                'required' => 'Niste izabrali fajl.',
                                'mimes' => 'Neodgovarajući format slike. Dozvoljeni formati su jpg, png, webp i jpeg.',
                                'max' => 'Maksimalna veličina slike je '.strval($max_image_size).'.',
                                )
                            );
                        if($image_validator->fails()){
                            $images_success = false;
                            $error_message = $image_validator->messages()->first();
                            break;
                        }
                    }
                    if($images_success){
                        foreach($images as $key => $image){
                            $slika_data['roba_id'] = $roba_id;
                            $slika_data['web_slika_id'] = 0;
                            $slika_data['slika'] = $image;
                            AdminArticles::saveImage($slika_data);
                        }                        
                    }else{
                        $validator->getMessageBag()->add('slike',$error_message);
                        if(!Request::ajax()){
                            return Redirect::to(AdminOptions::base_url().'admin/product-short/'.$data['roba_id'])->withInput()->withErrors($validator->messages());
                        }else{
                            return Response::json(array('success_message' => "", 'error_messages' => $validator->messages()));
                        }
                    }
                }

                if(isset($clone_id)){
                    if($slike_clone){
                        AdminArticles::cloneImages($clone_id,$roba_id);
                    }
                    AdminArticles::cloneKarak($clone_id,$roba_id);
                }

                if(Options::gnrl_options(3055) == 1 && Options::gnrl_options(3056) == 0){
                    $elastic = new ElasticSearchService();
                    $elastic->updateGroups();
                    $elastic->updateManufacturers();
                    $elastic->updateArticles();
                }

                $message="Uspešno ste sačuvali podatke";
                if(!Request::ajax()){
                    return Redirect::to(AdminOptions::base_url().'admin/product-short/'.$roba_id)->with('message_success',$message);
                }else{
                    return Response::json(array('success_message' => $message, 'error_messages' => array(),'roba_id'=>$roba_id));
                }
           }
  
    }

    function product_delete($roba_id, $check=null)
    {
        AdminSupport::saveLog('ARTIKLI_OBRISI', array($roba_id));

        $result = AdminArticles::delete($roba_id, $check);

        if(!Request::ajax()){
            return Redirect::to($result->redirect)->with('message',true);
        }else{
            if(!$result->success){
                $message = $result->message;
            }else{
                if(Options::gnrl_options(3055) == 1 && Options::gnrl_options(3056) == 0){
                    $elastic = new ElasticSearchService();
                    $elastic->updateArticles();
                }
                $message = 'Uspešno ste obrisali artikal';
            }
            return Response::json(array('success' => $result->success, 'message' => $message));
        }
    }

    function products_delete($roba_ids, $check=null)
    {

        $roba_ids = explode(',', $roba_ids);
        $mesages = '';
        
        foreach ($roba_ids as $roba_id) {        
            $result = AdminArticles::delete($roba_id, $check);
            $mesages .= $result->message;
        }

        if(Options::gnrl_options(3055) == 1 && Options::gnrl_options(3056) == 0){
            $elastic = new ElasticSearchService();
            $elastic->updateArticles();
        }
        AdminSupport::saveLog('ARTIKLI_OBRISI', $roba_ids);

        echo trim($mesages);
        die;
    }
    function products_delete_post($check=null)
    {

        $roba_ids = Input::get('roba_ids');
        $check = Input::get('check');

        $mesages = '';
        
        foreach ($roba_ids as $roba_id) {        
            $result = AdminArticles::delete($roba_id, $check);
            $mesages .= $result->message;
        }

        if(Options::gnrl_options(3055) == 1 && Options::gnrl_options(3056) == 0){
            $elastic = new ElasticSearchService();
            $elastic->updateArticles();
        }
        AdminSupport::saveLog('ARTIKLI_OBRISI', $roba_ids);

        echo trim($mesages);
        die;
    }

    function product_slike($id,$web_slika_id=0)
    {
        $web_slika_id = AdminOptions::web_options(310) == 1 ? $web_slika_id : 0;
        $slike = AdminArticles::getSlike($id,$web_slika_id);

        $data=array(
            'strana'=>'slike',
            'title'=>'slike',
            'roba_id'=>$id,
            'parent_id'=>$web_slika_id,
            'web_slika_parent'=> $web_slika_id > 0 ? DB::table('web_slika')->where('web_slika_id',$web_slika_id)->first() : null,
            'slike'=>$slike
        );
        return View::make('admin/page', $data);
    }

    function slika_edit()
    {

        $data = Input::get();
        $images = Input::file('slika');
        $max_image_size = 10000;
        $max_images = 10;
        $success = false;

        if(count($images) == 0){
            $error_message = 'Niste izabrali fajl.';
        }elseif(count($images) > $max_images){
            $error_message = 'Maksimalni broj slika je '.strval($max_images).'!';
        }else{
            foreach($images as $image){
                $extension = $image->getClientOriginalExtension();
                // if($extension=='jfif' || $extension == 'webp')
                // {
                //      return Redirect::back()->with('error','Dozvoljeni formati su jpg, png i jpeg.');
                // }
                $validator = Validator::make(
                    array('slika' => $image),
                    array('slika' => 'required|max:'.strval($max_image_size)),
                    array(
                        'required' => 'Niste izabrali fajl.',
                        'mimes' => 'Neodgovarajući format slike. Dozvoljeni formati su jpg, png i jpeg.',
                        'max' => 'Maksimalna veličina slike je '.strval($max_image_size / 1000).' MB.',
                        )
                    );
                if($validator->fails()){
                    $success = false;
                    $error_message = $validator->messages()->first();
                    break;
                }else{
                    $success = true;
                }
            }
        }

        if($success){
            foreach($images as $key => $image){
                $data['web_slika_id'] = 0;
                $data['slika'] = $image;
                AdminArticles::saveImage($data);
                // $id = DB::table('web_slika')->where('web_slika_id', $data['web_slika_id'])->pluck('roba_id');
            }
            AdminSupport::saveLog('ARTIKLI_DODAJ_SLIKE', array($data['roba_id']));
            return Redirect::back()->with('message','Uspešno ste sačuvali slike.');
        }
        if(count($data) == 0){
            $error_message = 'Maksimalna veličina slike je '.strval($max_image_size / 1000).' MB.';
        }
        return Redirect::back()->withInput()->with('error',$error_message);
    }

    function slika_delete()
    {
        $data = Input::get();
        AdminArticles::deleteImageFile($data['web_slika_id']);

        $akcija = DB::table('web_slika')->where('web_slika_id',$data['web_slika_id'])->pluck('akcija');
        if($akcija == 1){
            $upd_query = DB::table('web_slika')->where('roba_id',$data['roba_id'])->where('web_slika_id','!=',$data['web_slika_id']);

            if($upd_query->count() > 0){
                $upd_slika_id = $upd_query->first()->web_slika_id;
                DB::table('web_slika')->where('web_slika_id',$upd_slika_id)->update(array('akcija'=>1));
            }
        }

        // AdminSupport::saveLog('ARTIKLI_OBRISI_SLIKE', array($data['web_slika_id']));

        DB::table('web_slika')->where('web_slika_id',$data['web_slika_id'])->delete();

        return Redirect::to(AdminOptions::base_url().'admin/product_slike/'.$data['roba_id'].'/0')->with('message','Uspešno ste obrisali sliku.');;
    }
    function slike_more_delete()
    {
        $web_slika_ids = Input::get('web_slika_ids');
        $roba_id = Input::get('roba');

        foreach($web_slika_ids as $web_slika_id){
            AdminArticles::deleteImageFile($web_slika_id);
        }

        $slika_akcija = DB::table('web_slika')->whereIn('web_slika_id',$web_slika_ids)->where('akcija',1)->first();
        // DB::table('web_slika')->whereIn('web_slika_id',$web_slika_ids)->where('akcija',1)->pluck('roba_id');
        if(!is_null($slika_akcija)){
            $upd_query = DB::table('web_slika')->where('roba_id',$slika_akcija->roba_id)->whereNotIn('web_slika_id',$web_slika_ids);

            if($upd_query->count() > 0){
                $upd_slika_id = $upd_query->first()->web_slika_id;
                DB::table('web_slika')->where('web_slika_id',$upd_slika_id)->update(array('akcija'=>1));
            }
        }


        DB::table('web_slika')->whereIn('web_slika_id',$web_slika_ids)->delete();

        //drip
        if(Config::get('app.livemode') && AdminOptions::gnrl_options(3060) == 1){
            $dbProduct = DB::table('roba')->where('roba_id',$roba_id)->first();
            $drip = new Drip();
            $drip->addOrUpdateProduct($dbProduct,'updated');
        }

        AdminSupport::saveLog('ARTIKLI_OBRISI_SLIKE', array($roba_id));
    }
    function slika_delete_get($id,$web_slika_id,$short=false)
    {
        $data = Input::get();
        AdminArticles::deleteImageFile($web_slika_id);

        $akcija = DB::table('web_slika')->where('web_slika_id',$web_slika_id)->pluck('akcija');
        if($akcija == 1){
            $upd_query = DB::table('web_slika')->where('roba_id',$id)->where('web_slika_id','!=',$web_slika_id);

            if($upd_query->count() > 0){
                $upd_slika_id = $upd_query->first()->web_slika_id;
                DB::table('web_slika')->where('web_slika_id',$upd_slika_id)->update(array('akcija'=>1));
            }
        }
        AdminSupport::saveLog('ARTIKLI_OBRISI_SLIKE_GET', array($id));

        DB::table('web_slika')->where('web_slika_id',$web_slika_id)->delete();

        if(Request::ajax()){
            exit;
        }

        if($short){
            return Redirect::to(AdminOptions::base_url().'admin/product-short/'.$id)->with('message_delete','Uspešno ste obrisali sliku.');
        }else{
            return Redirect::to(AdminOptions::base_url().'admin/product_slike/'.$id)->with('message','Uspešno ste obrisali sliku.');
        }

    }
    function grupa_karakteristike()
    {
        $data = Request::all();
        $data=array(
            "grupa_pr_id"=>$data['grupa_pr_id'],
            'grupa_pr_vrednost_ids'=> $data['roba_id'] != 0 ? array_map('current',DB::table('web_roba_karakteristike')->select('grupa_pr_vrednost_id')->where('roba_id',$data['roba_id'])->get()) : array()
        );

        return View::make('admin/partials/ajax/grupa_generisane', $data);
    }
    
    function product_opis($id)
    {
        $data=array(
            'strana'=>'opis',
            'title'=>'opis',
            'roba_id'=>$id,
            'opis'=> $id != 0 ? AdminArticles::find($id, 'opis') : null,
            'web_opis'=> $id != 0 ? AdminArticles::find($id, 'web_opis') : null,
            'deklaracija'=> $id != 0 ? AdminArticles::find($id, 'deklaracija') : null
        );

        return View::make('admin/page', $data);
    }

    function opis_edit()
    {
        $data = Input::get();

        // $data['web_opis'] = str_replace("\n", "<br>",$data['web_opis']);
        DB::table('roba')->where('roba_id', $data['roba_id'])->update(array('web_opis'=>$data['web_opis'],'opis'=>$data['opis'],'deklaracija'=>$data['deklaracija']));
        
        AdminSupport::saveLog('ARTIKLI_OPIS', array($data['roba_id']));
        return Redirect::to(AdminOptions::base_url().'admin/product_opis/'.$data['roba_id']);
    }
    function product_seo($id,$jezik_id=1)
    {

        $roba=DB::table('roba')->where('roba_id',$id)->first();

        $seo = AdminSeo::article($id,$jezik_id);
        if($jezik_id == 1){
            $tags = strip_tags($roba->tags);
        }else{
            $tags = '';
            foreach(explode(',',$roba->tags) as $tag){
                $tags .= AdminLanguage::trans(trim($tag),DB::table('jezik')->where('jezik_id',$jezik_id)->pluck('kod')).', ';
            }
            if($tags != ''){
                $tags = substr($tags,0,-2);
            }
        }

        $data=array(
            'strana'=>'seo',
            'title'=>'seo',
            'roba_id'=>$id,
            'seo_title'=> $id != 0 ? $seo->title : '',
            "description"=> $id != 0 ? $seo->description : '',
            "keywords"=> $id != 0 ? $seo->keywords : '',
            'tags'=> $tags,
            "jezik_id"=>$jezik_id,
            "jezici" => DB::table('jezik')->where('aktivan',1)->get()
        );

        return View::make('admin/page', $data);
    }

    function seo_edit()
    {
        $data = Input::get();
        $roba_id = $data['roba_id'];
        $jezik_id = $data['jezik_id'];
        
        $validator = Validator::make($data,array('seo_title' => 'max:500','description' => 'max:320', 'keywords' => 'max:159', 'tags' => 'max:159'));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/product_seo/'.$roba_id)->withInput()->withErrors($validator->messages());
        }else{
            $data['title'] = str_replace("\n", "",$data['seo_title']);
            $data['description'] = str_replace("\n", "",$data['description']);
            $data['keywords'] = str_replace("\n", "",$data['keywords']);
            if($jezik_id == 1){
                $tags = str_replace("\n", "",$data['tags']);
                DB::table('roba')->where('roba_id', $roba_id)->update(array('tags' => $tags));
                foreach(explode(',',$tags) as $tag){
                    AdminTranslator::addToTranslatorAll(trim($tag));
                }
            }

            $query = DB::table('roba_jezik')->where(array('roba_id'=>$roba_id, 'jezik_id'=>$jezik_id));

            unset($data['seo_title']);
            unset($data['tags']);
            if(!is_null($query->first())){
                unset($data['roba_id']);
                unset($data['jezik_id']);
                $query->update($data);
            }else{
                if($data['title'] || $data['description'] || $data['keywords']){
                    DB::table('roba_jezik')->insert($data);
                    if($jezik_id==1){
                        foreach(DB::table('jezik')->where('aktivan',1)->where('jezik_id','!=',1)->orderBy('izabrani','desc')->get() as $jezik){
                            $translator = new TranslatorService(DB::table('jezik')->where('jezik_id',1)->pluck('kod'),$jezik->kod);
                            $data = array('roba_id' => $roba_id, 'jezik_id' => $jezik->jezik_id, 'title'=> $translator->translate($data['title']), 'description'=> $translator->translate($data['description']), 'keywords'=> $translator->translate($data['keywords']));
                            DB::table('roba_jezik')->insert($data);                 
                        }
                    }                    
                }
            }

            AdminSupport::saveLog('ARTIKLI_IZMENA_SEO', array($roba_id));
            return Redirect::to(AdminOptions::base_url().'admin/product_seo/'.$roba_id.($jezik_id==1 ? '' : '/'.$jezik_id));
        }
    }
    function product_karakteristike($id)
    {
        $data=array(
            'strana'=>'karakteristike',
            'title'=>'Karakteristike',
            'roba_id'=>$id,
            'web_karakteristike'=> AdminArticles::find($id, 'web_karakteristike')
        );

        return View::make('admin/page', $data);
    }

    function karakteristike_edit()
    {
        $data = Input::get();

        AdminArticles::karakteristike_save($data);

        AdminSupport::saveLog('ARTIKLI_KARAKTERISTIKE_IZMENI', array($data['roba_id']));
        return Redirect::to(AdminOptions::base_url().'admin/product_karakteristike/'.$data['roba_id']);
    }
    
    //GENERISANE
    function product_generisane($id)
    {
        $data=array(
            'strana'=>'generisane',
            'title'=>'Karakteristike',
            'roba_id'=>$id,
            'nazivi'=>AdminSupport::getGrupaKarak(AdminArticles::find($id, 'grupa_pr_id'))
        );

        return View::make('admin/page', $data);
    }

    //OD DOBAVLJACA
    function product_od_dobavljaca($id)
    {
        $data=array(
            'strana'=>'dobavljac_karakteristike',
            'title'=>'Karakteristike',
            'roba_id'=>$id,
            'grupe_karakteristika'=>AdminSupport::getGrupeKarak($id)
        );
        
        return View::make('admin/page', $data);
    }

    function position_GrupeNaziv()
    {   
        $karakteristika_grupa = Input::get('moved');

        $grupa = Input::get('grupa');
        $naziv = Input::get('naziv');
        $vrednost = Input::get('vrednost');

        $is_grupa = Input::get('is_grupa');
        $roba_id = Input::get('roba_id');
        $order_arr = Input::get('order');
        
        if($is_grupa == 1){
            // foreach ($order_arr as $key => $value) {
            //     DB::table('dobavljac_cenovnik_karakteristike')->where(array('roba_id'=>$roba_id,'karakteristika_grupa'=>$value))->update(array('redni_br_grupe'=>($key+1)));
            // }
            // $grupe = DB::select("SELECT karakteristika_grupa from dobavljac_cenovnik_karakteristike");
            $br = 1;
            foreach ($order_arr as $grupa){
                $naziv_grupe = DB::select("SELECT karakteristika_naziv from dobavljac_cenovnik_karakteristike where karakteristika_grupa = '".$grupa."' AND roba_id = ".$roba_id." ORDER BY redni_br_grupe ASC");
                foreach ($naziv_grupe as $naziv_grupa) {
                    DB::table('dobavljac_cenovnik_karakteristike')->where(array('karakteristika_naziv'=>$naziv_grupa->karakteristika_naziv, 'roba_id'=>$roba_id))->update(['redni_br_grupe' => $br]);
                    $br++;
                } 
            }

            $moved = DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id', $roba_id)->where('karakteristika_grupa', $karakteristika_grupa)->pluck('dobavljac_cenovnik_karakteristike_id');
            AdminSupport::saveLog('ARTIKAL_KARAKTERISTIKE_POZICIJA', array($moved));
        }else if($is_grupa == 0){
            if(!is_null(Input::get('grupa'))){
                $grupa = Input::get('grupa');
                $minRbr = DB::table('dobavljac_cenovnik_karakteristike')->where(array('karakteristika_grupa'=>$grupa, 'roba_id'=>$roba_id))->min('redni_br_grupe');

                foreach ($order_arr as $key => $value) {
                    DB::table('dobavljac_cenovnik_karakteristike')->where(array('karakteristika_grupa'=>$grupa, 'roba_id'=>$roba_id,'karakteristika_naziv'=>$value))->update(array('redni_br_grupe'=>($key+$minRbr)));
                }
            }else{
                foreach ($order_arr as $key => $value) {
                    DB::table('dobavljac_cenovnik_karakteristike')->where(array('roba_id'=>$roba_id,'karakteristika_naziv'=>$value))->update(array('redni_br_grupe'=>($key+1)));
                }
            }
            $moved = DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id', $roba_id)->where('karakteristika_grupa', $grupa)->where('karakteristika_naziv',$naziv)->where('karakteristika_vrednost',$vrednost)->pluck('dobavljac_cenovnik_karakteristike_id');
            AdminSupport::saveLog('ARTIKAL_KARAKTERISTIKE_NAZIV_POZICIJA', array($moved));
        }
        
    }

    //VEZANI ARTIKLI
    function vezani_artikli($id,$vezani=null)
    {
        if(!is_null($vezani)){
            $check_exist = DB::table('vezani_artikli')->where(array('roba_id'=>$id,'vezani_roba_id'=>$vezani))->count();
            if($check_exist == 0){
                $flag_cena = AdminOptions::web_options(129);
                $web_cena = DB::table('roba')->where('roba_id', $vezani)->pluck('web_cena');
                DB::table('vezani_artikli')->insert(array('roba_id'=>$id,'vezani_roba_id'=>$vezani,'flag_cena'=>$flag_cena,'cena'=>$web_cena));
                AdminSupport::saveLog('ARTIKLI_VEZI', array($vezani));
            }
        }

        $data=array(
            'strana'=>'vezani_artikli',
            'title'=>'Vezani artikli',
            'roba_id'=>$id,
            'vezani_artikli'=>DB::table('vezani_artikli')->where('roba_id',$id)->orderBy('vezani_roba_id','asc')->get()
        );
        
        return View::make('admin/page', $data);
    }

    //SRODNI ARTIKLI
    function srodni_artikli($id,$srodni=null)
    {
        if(!is_null($srodni)){
            $check_exist = DB::table('srodni_artikli')->where(array('roba_id'=>$id,'srodni_roba_id'=>$srodni))->count();
            if($check_exist == 0){
                $flag_cena = AdminOptions::web_options(129);
                DB::table('srodni_artikli')->insert(array('roba_id'=>$id,'srodni_roba_id'=>$srodni, 'flag_cena'=>$flag_cena));
                AdminSupport::saveLog('SRODNI_ARTIKLI_DODAJ', array($srodni));
            }
        }

        $data=array(
            'strana'=>'srodni_artikli',
            'title'=>'Srodni artikli',
            'roba_id'=>$id,
            'srodni_artikli'=>DB::table('srodni_artikli')->where('roba_id',$id)->orderBy('srodni_roba_id','asc')->get()
        );
        
        return View::make('admin/page', $data);
    }

    function export_all_xls()
    {
        $roba_ids = Input::get('roba_ids');
        $roba_ids = !is_null($roba_ids) ? explode('-',$roba_ids) : array();
        $vrsta_sifre = AdminOptions::web_options(201);

        header("Content-Type: application/vnd.ms-excel");
        header("Content-disposition: attachment; filename=export_roba.xls");
       

        echo 'Naziv'."\t" . 'Grupa'. "\t" . 'grupa_pr_id'. "\t" . 'Web cena'."\t" . 'Nabavna cena'."\t" . 'Lager'."\t" . 'sifra'."\t\n";
        $roba = DB::table('roba')->where('roba_id', '!=', -1);
        if(count($roba_ids) > 0){
            $roba = $roba->whereIn('roba_id',$roba_ids);
        }
        $artikli = $roba->get();

        $roba_all = '';
        foreach($artikli as $row)
        {
            $sifra = $row->barkod;
            // if($vrsta_sifre == 1){
            //     $sifra = $row->roba_id;
            // }else if($vrsta_sifre == 2){
            //     $sifra = (!is_null($row->sifra_is) && $row->sifra_is != '') ? $row->sifra_is : '';
            // }else if($vrsta_sifre == 3){
            //     $sifra = (!is_null($row->sku) && $row->sku != '') ? $row->sku : '';
            // }else if($vrsta_sifre == 4){
            //     $sifra = (!is_null($row->sifra_d) && $row->sifra_d != '') ? $row->sifra_d : '';
            // }          
            $roba_all .= $row->naziv ."\t". ($row->grupa_pr_id != -1 ? AdminGroups::find($row->grupa_pr_id,'grupa') : '') . "\t" . $row->grupa_pr_id . "\t" . $row->web_cena . "\t" . $row->racunska_cena_nc ."\t". round(AdminArticles::find($row->roba_id,'kolicina')) ."\t". $sifra ."\t\n";
        }

        echo iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE", $roba_all);

    }

    function export_xls()
    {
        $roba_ids = Input::get('roba_ids');
        $roba_ids = !is_null($roba_ids) ? explode('-',$roba_ids) : array();
        $vrsta_sifre = AdminOptions::web_options(201);

        // header("Content-Type: application/vnd.ms-excel");
        // header("Content-disposition: attachment; filename=export_roba.xls");
        
        $filename = 'files/export_roba.xls';

        $naslov_xls = 'Naziv'."\t" . 'Grupa'. "\t" . 'grupa_pr_id'. "\t" . 'Web cena'."\t" . 'Nabavna cena'."\t" . 'Lager'."\t" . 'sifra'."\t\n";
        $roba = DB::table('roba')->where('roba_id', '!=', -1);
        if(count($roba_ids) > 0){
            $roba = $roba->whereIn('roba_id',$roba_ids);
        }
        $artikli = $roba->get();

        $roba_all = '';
        foreach($artikli as $row)
        {
            $sifra = $row->barkod;
            // if($vrsta_sifre == 1){
            //     $sifra = $row->roba_id;
            // }else if($vrsta_sifre == 2){
            //     $sifra = (!is_null($row->sifra_is) && $row->sifra_is != '') ? $row->sifra_is : '';
            // }else if($vrsta_sifre == 3){
            //     $sifra = (!is_null($row->sku) && $row->sku != '') ? $row->sku : '';
            // }else if($vrsta_sifre == 4){
            //     $sifra = (!is_null($row->sifra_d) && $row->sifra_d != '') ? $row->sifra_d : '';
            // }          
            $roba_all .= $row->naziv ."\t". ($row->grupa_pr_id != -1 ? AdminGroups::find($row->grupa_pr_id,'grupa') : '') . "\t" . $row->grupa_pr_id . "\t" . $row->web_cena . "\t" . $row->racunska_cena_nc ."\t". round(AdminArticles::find($row->roba_id,'kolicina')) ."\t". $sifra ."\t\n";
        }

        file_put_contents($filename, $naslov_xls.iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE", $roba_all));
        // echo iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE", $roba_all);

    }


    function uploadImageArticle(){
        $images = scandir('images/upload_image');
        unset($images[0]);
        unset($images[1]);

        $data = array(
            'strana'=>'upload_image',
            'title'=>'Upload Image',
            'images'=>$images
            );
        return View::make('admin/page', $data);
    }
   
    function product_akcija($id)
    {
        $redni_broj = AdminArticles::find($id, 'akcija_redni_broj');
        if($redni_broj == 0){
            $redni_broj = DB::select('SELECT MAX(akcija_redni_broj) FROM roba')[0]->max + 1;
        }
        
        $data=array(
            'strana'=>'product_akcija',
            'title'=>'Product Akcija',
            'roba_id'=>$id,
            'naslov'=> $id != 0 ? AdminArticles::find($id, 'akcija_naslov') : null,
            'opis'=> $id != 0 ? strip_tags(AdminArticles::find($id, 'akcija_opis')) : null,
            'redni_broj'=> $id != 0 ? $redni_broj : null,
            'akcija_primeni'=> $id != 0 ? AdminArticles::find($id, 'akcija_flag_primeni') : null,
            "akcijska_cena"=> $id != 0 ? AdminArticles::find($id, 'akcijska_cena') : null,
            "akcija_popust"=> $id != 0 ? AdminArticles::find($id, 'akcija_popust') : 0,
            "web_cena"=> $id != 0 ? AdminArticles::find($id, 'web_cena') : null,
            "mpcena"=> $id != 0 ? AdminArticles::find($id, 'mpcena') : null,
            "datum_akcije_od"=> $id != 0 ? AdminArticles::find($id, 'datum_akcije_od') : null,
            "datum_akcije_do"=> $id != 0 ? AdminArticles::find($id, 'datum_akcije_do') : null,

        );

        return View::make('admin/page', $data);
    }

    function akcija_edit()
    {
        $data = Input::get();
        $validator = Validator::make($data, array(
            'akcija_naslov' => 'regex:'.AdminSupport::regex().'|max:100',
            'akcija_opis' => 'regex:'.AdminSupport::regex().'|max:1000',
            'akcija_redni_broj' => 'numeric|digits_between:1,10',
            'akcija_popust' => 'numeric|digits_between:1,10',
            'akcijska_cena' => 'numeric|digits_between:1,20',
            'web_cena' => 'numeric|digits_between:1,20'
            ));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/product_akcija/'.$data['roba_id'])->withInput()->withErrors($validator->messages());
        }else{
            if($data['akcijska_cena'] > $data['web_cena'] || $data['akcijska_cena'] < 0){
                return Redirect::to(AdminOptions::base_url().'admin/product_akcija/'.$data['roba_id'])->withInput()->with('message',true);
            }else{
                // $data['akcija_opis'] = str_replace("\n", "<br>",$data['akcija_opis']);
                $update_data = $data;
                unset($update_data['roba_id']);
                if($update_data['datum_akcije_od'] == ''){
                    $update_data['datum_akcije_od'] = null;
                }
                if($update_data['datum_akcije_do'] == ''){
                    $update_data['datum_akcije_do'] = null;
                }
                if($update_data['akcija_redni_broj'] == ''){
                    $update_data['akcija_redni_broj'] = null;
                }
                if(AdminOptions::web_options(132)==0){
                    $update_data['mpcena'] = $data['web_cena'];
                    unset($update_data['web_cena']);
                }
                DB::table('roba')->where('roba_id', $data['roba_id'])->update($update_data);

                AdminSupport::saveLog('ARTIKLI_AKCIJA_IZMENI', array($data['roba_id']));
                return Redirect::to(AdminOptions::base_url().'admin/product_akcija/'.$data['roba_id']);
            }
        }
    }

    function product_akcija_b2b($id)
    {
        $redni_broj = AdminArticles::find($id, 'b2b_akcija_redni_broj');
        if($redni_broj == 0){
            $redni_broj = DB::select('SELECT MAX(b2b_akcija_redni_broj) FROM roba')[0]->max + 1;
        }
        
        $data=array(
            'strana'=>'product_akcija_b2b',
            'title'=>'Product Akcija B2B',
            'roba_id'=>$id,
            'b2b_akcija_redni_broj'=> $id != 0 ? $redni_broj : null,
            'b2b_akcija_flag_primeni'=> $id != 0 ? AdminArticles::find($id, 'b2b_akcija_flag_primeni') : null,
            "b2b_akcijska_cena"=> $id != 0 ? AdminArticles::find($id, 'b2b_akcijska_cena') : null,
            "b2b_akcija_popust"=> $id != 0 ? AdminArticles::find($id, 'b2b_akcija_popust') : 0,
            "racunska_cena_end"=> $id != 0 ? AdminArticles::find($id, 'racunska_cena_end') : null,
            "b2b_datum_akcije_od"=> $id != 0 ? AdminArticles::find($id, 'b2b_datum_akcije_od') : null,
            "b2b_datum_akcije_do"=> $id != 0 ? AdminArticles::find($id, 'b2b_datum_akcije_do') : null,

        );

        return View::make('admin/page', $data);
    }
    function b2b_akcija_edit()
    {
        $data = Input::get();
        $validator = Validator::make($data, array(
            'b2b_akcija_redni_broj' => 'numeric|digits_between:1,10',
            'b2b_akcija_popust' => 'numeric|digits_between:1,10',
            'b2b_akcijska_cena' => 'numeric|digits_between:1,20',
            'racunska_cena_end' => 'numeric|digits_between:1,20'
            ));
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/product_akcija/'.$data['roba_id'])->withInput()->withErrors($validator->messages());
        }else{
            if($data['b2b_akcijska_cena'] > $data['racunska_cena_end'] || $data['b2b_akcijska_cena'] < 0){
                return Redirect::to(AdminOptions::base_url().'admin/product_akcija/'.$data['roba_id'])->withInput()->with('message',true);
            }else{
                $update_data = $data;
                unset($update_data['roba_id']);
                if($update_data['b2b_datum_akcije_od'] == ''){
                    $update_data['b2b_datum_akcije_od'] = null;
                }
                if($update_data['b2b_datum_akcije_do'] == ''){
                    $update_data['b2b_datum_akcije_do'] = null;
                }
                if($update_data['b2b_akcija_redni_broj'] == ''){
                    $update_data['b2b_akcija_redni_broj'] = null;
                }

                DB::table('roba')->where('roba_id', $data['roba_id'])->update($update_data);

                AdminSupport::saveLog('ARTIKLI_B2B_AKCIJA_IZMENI', array($data['roba_id']));
                return Redirect::to(AdminOptions::base_url().'admin/product_akcija_b2b/'.$data['roba_id']);
            }
        }
    }

    function dodatni_fajlovi($id,$extension_id=null)
    {
        $extension = "";
        if($extension_id!=null){
            $extension = " AND vrsta_fajla_id = ".$extension_id."";
        }
        $fajlovi = DB::select("SELECT * FROM web_files WHERE roba_id = ".$id."".$extension." ORDER BY vrsta_fajla_id ASC");

        $data=array(
            'strana'=>'dodatni_fajlovi',
            'title'=>'Dodatni Fajlovi',
            'roba_id'=>$id,
            'vrste_fajlova'=> $id != 0 ? AdminSupport::artikalFajlovi($id) : null,
            'extension_id'=> $id != 0 ? $extension_id : null,
            'fajlovi' => $id != 0 ? $fajlovi : null
        );

        return View::make('admin/page', $data);
    }

    function dodatni_fajlovi_upload()
    {
        $inputs = Input::get();
        $fileUpl = Input::file('upl_file');

        $usefull_arr = array(
            'roba_id' => $inputs['roba_id'],
            'naziv' => $inputs['naziv'],
            'putanja_etaz' => $inputs['putanja'],
            'vrsta_fajla_id' => $inputs['vrsta_fajla'],
            'file' => $fileUpl
        );
        $validation_arr = array(
            'naziv' => 'required|regex:'.AdminSupport::regex().'|max:200',
            'file' => 'required'
        );

        if($inputs['check_link'] == 1){
            $validation_arr = array(
                'naziv' => 'required|regex:'.AdminSupport::regex().'|max:200',
                'putanja_etaz' => 'required|max:300'
            );
        }

        $validator = Validator::make($usefull_arr, $validation_arr);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/dodatni_fajlovi/'.$inputs['roba_id'])->withInput()->withErrors($validator->messages());
        }else{
            if($inputs['check_link'] == 0){

                $size = $fileUpl->getSize();
                if($size > 1000000){
                    return Redirect::to(AdminOptions::base_url().'admin/dodatni_fajlovi/'.$inputs['roba_id'])->with('mess',true);
                }
                
                $originalName = $fileUpl->getClientOriginalName();
                $fileUpl->move('images/files/', $originalName);

                unset($usefull_arr['putanja_etaz']);
                unset($usefull_arr['file']);
                $usefull_arr['putanja'] = 'images/files/'.$originalName;

                DB::table('web_files')->insert($usefull_arr);
                $new_values = DB::table('web_files')->max('web_file_id');
            }else{
                unset($usefull_arr['file']);
                DB::table('web_files')->insert($usefull_arr);
                $new_values = DB::table('web_files')->max('web_file_id');                
            }

            AdminSupport::saveLog('ARTIKLI_FAJLOVI_DODAJ', array($inputs['roba_id']));
            return Redirect::to(AdminOptions::base_url().'admin/dodatni_fajlovi/'.$inputs['roba_id']);
        }
    }

    function dodatni_fajlovi_delete($roba_id,$web_file_id)
    {
        $putanja = DB::table('web_files')->where('web_file_id',$web_file_id)->first()->putanja;
        $uses = DB::table('web_files')->where('putanja',$putanja)->count();
        if($uses == 1){
            File::delete($putanja);
        }
        AdminSupport::saveLog('ARTIKLI_OBRISI_FAJLOVI', array($roba_id));
        DB::table('web_files')->where('web_file_id',$web_file_id)->delete();

        return Redirect::to(AdminOptions::base_url().'admin/dodatni_fajlovi/'.$roba_id);
    }
    
    public function ajaxArticles(){
        $action = Input::get('action');
        $roba_id = Input::get('roba_id');
        $web_slika_id = Input::get('web_slika_id');

        if ($action == 'magacin_change') {
            $roba_id = Input::get('roba_id');
            $magacin_id = Input::get('magacin_id');
            echo AdminArticles::find($roba_id,'kolicina',$magacin_id);
        }
        elseif ($action == 'execute') {

            $roba_ids = Input::get('roba_ids');
            $data = Input::get('data');
            foreach($data as $row){               
                if($row['val'] == 'null'){
                    $row['val'] = null;
                }
                if($row['column'] == 'flag_zakljucan') {
                    DB::table($row['table'])->whereIn('roba_id', $roba_ids)->update(array($row['column']=>$row['val']));
                }elseif($row['column'] == 'flag_aktivan' && $row['val'] == '0') {
                    DB::table($row['table'])->whereIn('roba_id', $roba_ids)->where('flag_zakljucan', 0)->update(array($row['column']=>$row['val'],'flag_prikazi_u_cenovniku'=>0));
                }elseif($row['column'] == 'flag_prikazi_u_cenovniku' && $row['val'] == '1') {
                    DB::table($row['table'])->whereIn('roba_id', $roba_ids)->where('flag_zakljucan', 0)->where('flag_aktivan', 1)->update(array($row['column']=>$row['val'])); 
                }else {
                    DB::table($row['table'])->whereIn('roba_id', $roba_ids)->where('flag_zakljucan', 0)->update(array($row['column']=>$row['val']));
                }
                

            }

            if($row['column'] == 'flag_prikazi_u_cenovniku' && $row['val'] == '1') {
                AdminSupport::saveLog('ARTIKLI_PRIKAZI_NA_WEBU', $roba_ids);
            }
            if($row['column'] == 'flag_prikazi_u_cenovniku' && $row['val'] == '0') {
                AdminSupport::saveLog('ARTIKLI_SKLONI_SA_WEBA', $roba_ids);
            }
            if($row['column'] == 'flag_cenovnik' && $row['val'] == '1') {
                AdminSupport::saveLog('ARTIKLI_PRIKAZI_NA_B2B', $roba_ids);
            }
            if($row['column'] == 'flag_cenovnik' && $row['val'] == '0') {
                AdminSupport::saveLog('ARTIKLI_SKLONI_SA_B2B', $roba_ids);
            }
            if($row['column'] == 'flag_zakljucan' && ($row['val'] == '0' || $row['val'] == 'false')) {
                AdminSupport::saveLog('ARTIKLI_OTKLJUCAJ', $roba_ids);
            }
            if($row['column'] == 'flag_zakljucan' && ($row['val'] == '1' || $row['val'] == 'true')) {
                AdminSupport::saveLog('ARTIKLI_ZAKLJUCAJ', $roba_ids);
            }   
            if(($row['column'] == 'akcija_flag_primeni' && $row['val'] == '0') || ($row['column'] == 'akcija_popust' && $row['val'] == '0') || ($row['column'] == 'akcijska_cena' && $row['val'] == '0') ) {
                AdminSupport::saveLog('ARTIKLI_SKLONI', $roba_ids);
            }
            if($row['column'] == 'flag_aktivan' && $row['val'] == '0') {
                AdminSupport::saveLog('ARTIKLI_NEAKTIVNO', $roba_ids);
            }
            if($row['column'] == 'flag_aktivan' && $row['val'] == '1') {
                AdminSupport::saveLog('ARTIKLI_AKTIVNO', $roba_ids);
            }
            if($row['column'] == 'web_flag_karakteristike') {
                AdminSupport::saveLog('ARTIKLI_DODAJ_KARAKTERISTIKE', $roba_ids);
            }
            if($row['column'] == 'grupa_pr_id') {
                AdminSupport::saveLog('ARTIKLI_IZMENI_GRUPU', $roba_ids);
            }
            if($row['column'] == 'roba_flag_cene_id') {
                AdminSupport::saveLog('ARTIKLI_IZMENI_VRSTU_CENE', $roba_ids);
            }
            // custom for inputs
            if($data[0]['table'] == 'roba' && $data[0]['column'] == 'tip_cene'){
                if($data[0]['val'] != 'null'){
                    AdminSupport::saveLog('ARTIKLI_IZMENI_TIP', $roba_ids);
                    return AdminSupport::tip_naziv($data[0]['val']);
                }else{
                    return '';
                }
            }
            elseif($data[0]['table'] == 'roba' && $data[0]['column'] == 'proizvodjac_id'){
                if($data[0]['val'] != 'null'){
                    AdminSupport::saveLog('ARTIKLI_IZMENI_PROIZVODJAC', $roba_ids);
                    return AdminSupport::find_proizvodjac($data[0]['val'],'naziv');
                }else{
                    return '';
                }
            }
          
        }
        elseif ($action == 'preracunaj_cene') {
            $roba_ids = Input::get('roba_ids');
            $response = array();
            foreach($roba_ids as $roba_id){
                DB::statement("UPDATE roba r SET web_cena = racunska_cena_nc*(1+web_marza/100)*(SELECT porez FROM tarifna_grupa WHERE tarifna_grupa_id=r.tarifna_grupa_id) WHERE roba_id = ".$roba_id."");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena');
            }
            echo json_encode($response);

        }
        elseif ($action == 'zaokruzi_webcenu') {
            $roba_ids = Input::get('roba_ids');
            $dec = Input::get('dec');
            //var_dump($webcena);die;

            $response = array();
            foreach($roba_ids as $roba_id){
            $webcena = DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena');
            $webcena = substr($webcena,0,-5).$dec;
                DB::statement("UPDATE roba r SET web_cena = ".$webcena." WHERE roba_id = ".$roba_id." AND web_cena > 99");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena');
            }
            echo json_encode($response);

        }
        elseif ($action == 'zaokruzi_mpcenu') {
            $roba_ids = Input::get('roba_ids');
            $dec = Input::get('dec');
            //var_dump($webcena);die;

            $response = array();
            foreach($roba_ids as $roba_id){
            $mpcena = DB::table('roba')->where('roba_id',$roba_id)->pluck('mpcena');
            $mpcena = substr($mpcena,0,-5).$dec;
                DB::statement("UPDATE roba r SET mpcena = ".$mpcena." WHERE roba_id = ".$roba_id." AND mpcena > 99");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('mpcena');
            }
            echo json_encode($response);

        }

         elseif ($action == 'zaokruzi_endcenu') {
            $roba_ids = Input::get('roba_ids');
            $dec = Input::get('dec');
            //var_dump($webcena);die;

            $response = array();
            foreach($roba_ids as $roba_id){
            $racunska_cena_end = DB::table('roba')->where('roba_id',$roba_id)->pluck('racunska_cena_end');
            $racunska_cena_end = substr($racunska_cena_end,0,-5).$dec;
                DB::statement("UPDATE roba r SET racunska_cena_end = ".$racunska_cena_end." WHERE roba_id = ".$roba_id." AND racunska_cena_end > 99");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('racunska_cena_end');
            }
            echo json_encode($response);

        }
        elseif ($action == 'preracunaj_webcenu') {
            $roba_ids = Input::get('roba_ids');
            $response = array();
            foreach($roba_ids as $roba_id){
                DB::statement("UPDATE roba r SET web_cena = (racunska_cena_nc*(1+(web_marza/100))*(SELECT 1+porez/100 FROM tarifna_grupa WHERE tarifna_grupa_id = r.tarifna_grupa_id) ) WHERE roba_id = ".$roba_id."");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena');
            }
            AdminSupport::saveLog('ARTIKLI_PRERACUNAJ_WEB_CENU', $roba_ids);
            echo json_encode($response);

        }
        elseif ($action == 'preracunaj_mpcenu') {
            $roba_ids = Input::get('roba_ids');
            $response = array();
            foreach($roba_ids as $roba_id){
                DB::statement("UPDATE roba r SET mpcena = (racunska_cena_nc*(1+(mp_marza/100))*(SELECT 1+porez/100 FROM tarifna_grupa WHERE tarifna_grupa_id = r.tarifna_grupa_id) ) WHERE roba_id = ".$roba_id."");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('mpcena');
            }
            AdminSupport::saveLog('ARTIKLI_PRERACUNAJ_MP_CENU', $roba_ids);
            echo json_encode($response);

        }
         elseif ($action == 'preracunaj_endcenu') {
            $roba_ids = Input::get('roba_ids');
            $response = array();
            foreach($roba_ids as $roba_id){
                DB::statement("UPDATE roba r SET racunska_cena_end = (racunska_cena_nc*(1+(end_marza/100))) WHERE roba_id = ".$roba_id."");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('racunska_cena_end');
            }
            AdminSupport::saveLog('ARTIKLI_PRERACUNAJ_END_CENU', $roba_ids);
            echo json_encode($response);

        }
        elseif ($action == 'poreska_stopa') {
            $roba_ids = Input::get('roba_ids');
            $check = Input::get('check');
            $val = Input::get('val');
            $porez = AdminSupport::find_tarifna_grupa($val,'porez');
            $stopa = 1 + $porez / 100;
            $response = array();
            $response['porez'] = $porez;
            if($check == 0){            
                foreach($roba_ids as $roba_id){
                    DB::statement("UPDATE roba SET tarifna_grupa_id=".$val." WHERE roba_id = ".$roba_id."");
                    DB::statement("UPDATE dobavljac_cenovnik SET tarifna_grupa_id=".$val." WHERE roba_id = ".$roba_id."");
                }
                echo json_encode($response);
            }
            elseif($check == 1){            
                $response['web_cena'] = array();
                foreach($roba_ids as $roba_id){
                    DB::statement("UPDATE roba SET tarifna_grupa_id=".$val.", web_cena = ROUND(racunska_cena_nc*(1+web_marza/100)*".$stopa.") WHERE roba_id = ".$roba_id."");
                    DB::statement("UPDATE dobavljac_cenovnik SET tarifna_grupa_id=".$val.", web_cena = cena_nc*(1+web_marza/100)*".$stopa." WHERE roba_id = ".$roba_id."");
                    $response['web_cena'][$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena');
                }
                AdminSupport::saveLog('ARTIKLI_IZMENI_PORESKA_STOPA', $roba_ids);
                echo json_encode($response);
            }
        }
        elseif ($action == 'promena_kolicine') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            $magacin_id = Input::get('magacin_id');
            $godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');     


            if($magacin_id == 0){
                $magacin_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
            }

            $redovi = "";

            foreach($roba_ids as $roba_id) {
                $redovi .= "(".$roba_id.", ". $magacin_id .", ". $val .", 0, 1, ". $godina_id ."),";
            }

            $redovi = substr($redovi, 0, -1);

            DB::statement("INSERT INTO lager (roba_id, orgj_id, kolicina, nabavna_po, valuta_id, poslovna_godina_id) SELECT * FROM (values ". $redovi .") redovi(roba_id, orgj_id, kolicina, nabavna_po, valuta_id, poslovna_godina_id) WHERE roba_id NOT IN (SELECT roba_id FROM lager WHERE orgj_id = ".$magacin_id.")");

            DB::statement("UPDATE lager SET kolicina=".$val." WHERE roba_id IN (".implode(',',$roba_ids).") AND poslovna_godina_id = ".$godina_id." AND orgj_id = ".$magacin_id."");

            AdminSupport::saveLog('ARTIKLI_IZMENI_KOLICINA', $roba_ids);

        }
        elseif ($action == 'akcija') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            $akc_od = Input::get('akc_od');            
            $akc_do = Input::get('akc_do');
            
            
            $do = date_create($akc_do);
            $danasnji_dan=date_create();
            $raz =date_diff($danasnji_dan,$do);
           
            if(!empty($akc_do) && !empty($akc_od)){
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('akcija_popust'=>$val, 'akcija_flag_primeni'=>1, 'datum_akcije_od'=>$akc_od,'datum_akcije_do'=>$akc_do));
            }
            elseif(!empty($akc_do)){
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('akcija_popust'=>$val, 'akcija_flag_primeni'=>1, 'datum_akcije_od'=>null,'datum_akcije_do'=>$akc_do));
            }
            elseif(!empty($akc_od)){
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('akcija_popust'=>$val, 'akcija_flag_primeni'=>1, 'datum_akcije_od'=>$akc_od,'datum_akcije_do'=>null));
            }
            elseif(empty($akc_od) && empty($akc_do)){
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('akcija_popust'=>$val, 'akcija_flag_primeni'=>1, 'datum_akcije_od'=>null,'datum_akcije_do'=>null));
            }
            $response = array();
            foreach($roba_ids as $roba_id){
                $redni_broj = DB::select('SELECT MAX(akcija_redni_broj) FROM roba')[0]->max + 1;
                DB::statement("UPDATE roba r SET akcijska_cena = (web_cena-(akcija_popust/100)*web_cena ) WHERE roba_id = ".$roba_id."");
                DB::statement("UPDATE roba r SET akcija_redni_broj = ".$redni_broj." WHERE roba_id = ".$roba_id."");
                
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('akcijska_cena');
            }
            AdminSupport::saveLog('ARTIKLI_AKCIJA_POPUST', $roba_ids);
            echo json_encode($response);
           
        }
        elseif ($action == 'akcijaB2B') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            $akc_od = Input::get('akc_od');            
            $akc_do = Input::get('akc_do');
            
            
            $do = date_create($akc_do);
            $danasnji_dan=date_create();
            $raz =date_diff($danasnji_dan,$do);
           
            if(!empty($akc_do) && !empty($akc_od)){
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('b2b_akcija_popust'=>$val, 'b2b_akcija_flag_primeni'=>1, 'b2b_datum_akcije_od'=>$akc_od,'b2b_datum_akcije_do'=>$akc_do));
            }
            elseif(!empty($akc_do)){
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('b2b_akcija_popust'=>$val, 'b2b_akcija_flag_primeni'=>1, 'b2b_datum_akcije_od'=>null,'b2b_datum_akcije_do'=>$akc_do));
            }
            elseif(!empty($akc_od)){
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('b2b_akcija_popust'=>$val, 'b2b_akcija_flag_primeni'=>1, 'b2b_datum_akcije_od'=>$akc_od,'b2b_datum_akcije_do'=>null));
            }
            elseif(empty($akc_od) && empty($akc_do)){
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('b2b_akcija_popust'=>$val, 'b2b_akcija_flag_primeni'=>1, 'b2b_datum_akcije_od'=>null,'b2b_datum_akcije_do'=>null));
            }
            $response = array();
            foreach($roba_ids as $roba_id){
                DB::statement("UPDATE roba r SET b2b_akcijska_cena = (racunska_cena_end-(b2b_akcija_popust/100)*racunska_cena_end ) WHERE roba_id = ".$roba_id."");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('b2b_akcijska_cena');
            }
            AdminSupport::saveLog('ARTIKLI_AKCIJA_POPUST', $roba_ids);
            echo json_encode($response);
           
        }
        elseif ($action == 'promena_tezine') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('tezinski_faktor'=>$val));
            AdminSupport::saveLog('ARTIKLI_IZMENI_TEZINU', $roba_ids);
        }
                elseif ($action == 'promena_visine') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('visina'=>$val));
            AdminSupport::saveLog('ARTIKLI_IZMENI_VISINU', $roba_ids);
        }
        elseif ($action == 'promena_sirine') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('sirina'=>$val));
            AdminSupport::saveLog('ARTIKLI_IZMENI_SIRINU', $roba_ids);
        }
        elseif ($action == 'promena_duzine') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('duzina'=>$val));
            AdminSupport::saveLog('ARTIKLI_IZMENI_DUZINU', $roba_ids);
        }
        elseif ($action == 'promena_bruto_mase') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('bruto_masa'=>$val));
            AdminSupport::saveLog('ARTIKLI_IZMENI_BRUTO_MASU', $roba_ids);
        }
        elseif ($action == 'promena_jedinice_mere_mase') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('jm_mase'=>$val));
            AdminSupport::saveLog('ARTIKLI_IZMENI_JEDINICU_MERE_MASE', $roba_ids);
        }
        elseif ($action == 'promena_webcene') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('web_cena'=>$val));
            AdminSupport::saveLog('ARTIKLI_IZMENI_WEB_CENA', $roba_ids);
        }
        elseif ($action == 'promena_mpcene') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('mpcena'=>$val));
            AdminSupport::saveLog('ARTIKLI_IZMENI_MP_CENA', $roba_ids);
        }
        elseif ($action == 'promena_endcene') {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('racunska_cena_end'=>$val));
            AdminSupport::saveLog('ARTIKLI_IZMENI_END_CENA', $roba_ids);
        }
        elseif ($action == 'promena_tag' ) {
            $roba_ids = Input::get('roba_ids');
            $val = Input::get('val');
            DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('tags'=>$val));
            AdminSupport::saveLog('ARTIKLI_DODAJ_TAG', $roba_ids);
        }
        elseif ($action == 'vrednost_poreza') {
            $tarifna_grupa_id = Input::get('tarifna_grupa_id');
            echo DB::table('tarifna_grupa')->where('tarifna_grupa_id',$tarifna_grupa_id)->pluck('porez');
        }

        elseif ($action == 'exporti') {
            $kind = Input::get('kind');
            $roba_ids = Input::get('roba_ids');
            $export_id = Input::get('export_id');
            if($kind=='export_dodeli'){
                $redovi_data = '';
                foreach($roba_ids as $roba_id){
                    $redovi_data .= '('.$roba_id.','.$export_id.'),';
                }
                $redovi_data = substr($redovi_data,0,-1);
                DB::statement('INSERT INTO roba_export SELECT * FROM (VALUES '.$redovi_data.') redivi(roba_id,export_id) WHERE roba_id NOT IN (SELECT roba_id FROM roba_export WHERE export_id='.$export_id.')');
                AdminSupport::saveLog('ARTIKLI_EKSPORT_DODELA', $roba_ids);
            }
            elseif($kind=='export_ukloni'){
                $roba_ids = Input::get('roba_ids');
                $export_id = Input::get('export_id');
                DB::table('roba_export')->where('export_id',$export_id)->whereIn('roba_id',$roba_ids)->delete();
                AdminSupport::saveLog('ARTIKLI_EKSPORT_SKLONI', $roba_ids);
            }
        }
         elseif ($action == 'katalog') {
            $kind = Input::get('kind');
            $roba_ids = Input::get('roba_ids');
            $katalog_id = Input::get('katalog_id');
            if($kind=='katalog_dodeli'){
                $redovi_data = '';
                foreach($roba_ids as $roba_id){
                    $redovi_data .= '('.$katalog_id.','.$roba_id.'),';
                }
                $redovi_data = substr($redovi_data,0,-1);

                DB::statement('INSERT INTO katalog_roba SELECT * FROM (VALUES '.$redovi_data.') redivi(katalog_id,roba_id) WHERE roba_id NOT IN (SELECT roba_id FROM katalog_roba WHERE katalog_id='.$katalog_id.')');
                AdminSupport::saveLog('ARTIKLI_KATALOG_DODELA', $roba_ids);
            }
            elseif($kind=='katalog_ukloni'){
                $roba_ids = Input::get('roba_ids');
                $katalog_id = Input::get('katalog_id');
                DB::table('katalog_roba')->where('katalog_id',$katalog_id)->whereIn('roba_id',$roba_ids)->delete();
                AdminSupport::saveLog('ARTIKLI_KATALOG_SKLONI', $roba_ids);
            }
        }
        elseif ($action == 'assign_selected_image') {
            $roba_ids = Input::get('roba_ids');
            $selectedImages = Input::get('images');

            foreach($roba_ids as $roba_id){
                foreach($selectedImages as $selectedImage){
                    AdminArticles::saveSelectImage($roba_id,$selectedImage['image'],$selectedImage['alt']=null);
                }
            }
            AdminSupport::saveLog('ARTIKLI_SLIKE_DODAJ', $roba_ids);
        }
        elseif ($action == 'update_slika') {
            DB::table('web_slika')->where('roba_id', $roba_id)->update(array('akcija'=>0));
            DB::table('web_slika')->where('web_slika_id', $web_slika_id)->update(array('akcija'=>1));

            //drip
            if(Config::get('app.livemode') && AdminOptions::gnrl_options(3060) == 1){
                $dbProduct = DB::table('roba')->where('roba_id',$roba_id)->first();
                $drip = new Drip();
                $drip->addOrUpdateProduct($dbProduct,'updated');
            }
            AdminSupport::saveLog('ARTIKLI_SLIKA_GLAVNA', array($roba_id));

        }
        elseif ($action == 'update_slika_alt') {
            $web_slika_alt = Input::get('web_slika_alt');
            $roba_id = Input::get('roba');
            DB::table('web_slika')->where('web_slika_id', $web_slika_id)->update(array('alt'=>$web_slika_alt));

            AdminSupport::saveLog('ARTIKLI_SLIKA_OPIS', array($roba_id));

        }
        elseif ($action == 'prikazi') {
            $prikaz = Input::get('prikaz');
            $web_slika_ids = Input::get('web_slika_ids');
            $roba_id = Input::get('roba');

            DB::table('web_slika')->whereIn('web_slika_id', $web_slika_ids)->update(array('flag_prikazi'=>$prikaz));

            //drip
            if(Config::get('app.livemode') && AdminOptions::gnrl_options(3060) == 1){
                $dbProduct = DB::table('roba')->where('roba_id',$roba_id)->first();
                $drip = new Drip();
                $drip->addOrUpdateProduct($dbProduct,'updated');
            }
            if($prikaz){
            AdminSupport::saveLog('ARTIKLI_SLIKE_PRIKAZI_PRODAVNICA', array($roba_id));
            }else {
            AdminSupport::saveLog('ARTIKLI_SLIKE_SKLONI_PRODAVNICA', array($roba_id));    
            }

        }

        elseif ($action == 'definisane_marze_primena'){
            $roba_ids = Input::get('roba_ids');

            DB::statement("update roba set web_marza = definisana_web_marza(racunska_cena_nc), mp_marza = definisana_mp_marza(racunska_cena_nc) where roba_id in (".implode(',',$roba_ids).") and flag_zakljucan = 'false'");

            DB::statement("UPDATE roba r SET web_cena = racunska_cena_nc*(1+web_marza/100)*(1+(SELECT porez FROM tarifna_grupa WHERE tarifna_grupa_id=r.tarifna_grupa_id LIMIT 1)/100), mpcena = racunska_cena_nc*(1+mp_marza/100)*(1+(SELECT porez FROM tarifna_grupa WHERE tarifna_grupa_id=r.tarifna_grupa_id LIMIT 1)/100) WHERE roba_id in (".implode(',',$roba_ids).") AND flag_zakljucan = 'false'");

            AdminSupport::saveLog('ARTIKLI_DEFINISANE_MARZE_DODELI', $roba_ids); 

            return json_encode(DB::table('roba')->select('roba_id','web_marza','mp_marza','web_cena','mpcena')->whereIn('roba_id',$roba_ids)->get());

        }

        elseif ($action == 'mass_edit_marza') {
            $roba_ids = Input::get('roba_ids');
            $cena_od = Input::get('cena_od');
            $cena_za = Input::get('cena_za');
            $marza = Input::get('marza');
            $marza_check = Input::get('marza_check');

            $cena_resp_roba = "";
            $marza_query = "";
            if($cena_za=="nc"){
                $cena_resp_roba = "racunska_cena_nc";
                $cena_resp_dc = "cena_nc";
            }
            elseif($cena_za=="wc"){
                $cena_resp_roba = "web_cena";
                $cena_resp_dc = $cena_resp_roba;
                $marza_query = "web_marza";
            }
            elseif($cena_za=="mc"){
                $cena_resp_roba = "mpcena";
                $cena_resp_dc = $cena_resp_roba;
                $marza_query = "mp_marza";
            }
            elseif($cena_za=="ec"){
                $cena_resp_roba = "racunska_cena_end";
                $cena_resp_dc = "cena_end";
                $marza_query = "end_marza";
            }

            if($cena_od=="nc"){
                $value_roba = "ROUND(racunska_cena_nc*(1+".number_format($marza,2)."/100)*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1))";
                $value_dc = "ROUND(cena_nc*(1+".number_format($marza,2)."/100)*((select porez from tarifna_grupa where tarifna_grupa_id=dc.tarifna_grupa_id)/100+1))";
            }
            elseif($cena_od=="wc"){
                $value_roba = "ROUND(web_cena*(1+".number_format($marza,2)."/100))";
                $value_dc = $value_roba;
            }
            elseif($cena_od=="mc"){
                $value_roba = "ROUND(mpcena*(1+".number_format($marza,2)."/100))";
                $value_dc = $value_roba;
            }

             elseif($cena_od=="ec"){
                $value_roba = "ROUND(racunska_cena_end*(1+".number_format($marza,2)."/100))";
                $value_dc = "ROUND(cena_end*(1+".number_format($marza,2)."/100))";
            }

            $response = array();

            if($marza_check == 1 && $marza_query != ""){
                foreach($roba_ids as $roba_id){
                    DB::statement("UPDATE roba r SET ".$cena_resp_roba."=".$value_roba.", ".$marza_query."=".$marza." WHERE roba_id = ".$roba_id." AND flag_zakljucan = 'false'");
                    DB::statement("UPDATE dobavljac_cenovnik dc SET ".$cena_resp_dc."=".$value_dc.", ".$marza_query."=".$marza." WHERE roba_id = ".$roba_id." AND flag_zakljucan = 1");
                    $response[] = array(
                        "roba_id" => $roba_id,
                        $cena_resp_roba => DB::table('roba')->where('roba_id',$roba_id)->pluck($cena_resp_roba),
                        $marza_query => number_format($marza,2)
                        );
                }
            }else{
                foreach($roba_ids as $roba_id){
                    DB::statement("UPDATE roba r SET ".$cena_resp_roba."=".$value_roba." WHERE roba_id = ".$roba_id." AND flag_zakljucan = 'false'");
                    DB::statement("UPDATE dobavljac_cenovnik dc SET ".$cena_resp_dc."=".$value_dc." WHERE roba_id = ".$roba_id." AND flag_zakljucan = 1");
                    $response[] = array(
                        "roba_id" => $roba_id,
                        $cena_resp_roba => DB::table('roba')->where('roba_id',$roba_id)->pluck($cena_resp_roba)
                        );
                }
            }

            AdminSupport::saveLog('ARTIKLI_IZMENI_CENE_MARZE', $roba_ids);
            echo json_encode($response); 
            // All::dd($response);           
        }
        elseif ($action == 'mass_edit_rabat') {
            $roba_ids = Input::get('roba_ids');
            $cena_od = Input::get('cena_od');
            $cena_za = Input::get('cena_za');
            $rabat = Input::get('rabat');

            $cena_resp_roba = "";
            if($cena_za=="nc"){
                $cena_resp_roba = "racunska_cena_nc";
                $cena_resp_dc = "cena_nc";
            }
            elseif($cena_za=="wc"){
                $cena_resp_roba = "web_cena";
                $cena_resp_dc = $cena_resp_roba;
            }
            elseif($cena_za=="mc"){
                $cena_resp_roba = "mpcena";
                $cena_resp_dc = $cena_resp_roba;
            }
            elseif($cena_za=="ec"){
                $cena_resp_roba = "racunska_cena_end";
                $cena_resp_dc = $cena_resp_roba;
            }

            if($cena_od=="nc"){
                $value_roba = "ROUND(racunska_cena_nc*(1-".number_format($rabat,2)."/100)*((select porez from tarifna_grupa where tarifna_grupa_id=r.tarifna_grupa_id)/100+1))";
                $value_dc = "ROUND(cena_nc*(1-".number_format($rabat,2)."/100)*((select porez from tarifna_grupa where tarifna_grupa_id=dc.tarifna_grupa_id)/100+1))";
            }
            elseif($cena_od=="wc"){
                $value_roba = "ROUND(web_cena*(1-".number_format($rabat,2)."/100))";
                $value_dc = $value_roba;
            }
            elseif($cena_od=="mc"){
                $value_roba = "ROUND(mpcena*(1-".number_format($rabat,2)."/100))";
                $value_dc = $value_roba;
            }
            elseif($cena_od=="ec"){
                $value_roba = "ROUND(racunska_cena_end*(1-".number_format($rabat,2)."/100))";
                $value_dc = $value_roba;
            }

            $response = array();

            foreach($roba_ids as $roba_id){
                DB::statement("UPDATE roba r SET ".$cena_resp_roba."=".$value_roba." WHERE roba_id = ".$roba_id." AND flag_zakljucan = 'false'");
                DB::statement("UPDATE dobavljac_cenovnik dc SET ".$cena_resp_dc."=".$value_dc." WHERE roba_id = ".$roba_id." AND flag_zakljucan = 1");
                $response[] = array(
                    "roba_id" => $roba_id,
                   ($cena_resp_roba == 'web_cena' ? 'JSWebCenaVrednost' : $cena_resp_roba) => DB::table('roba')->where('roba_id',$roba_id)->pluck($cena_resp_roba)
                    );
            }

            AdminSupport::saveLog('ARTIKLI_IZMENI_CENE_RABATA', $roba_ids);
            echo json_encode($response);

        }
        //Edit texta
        elseif ($action == 'text_edit') {
            $roba_ids = Input::get('roba_ids');
            $insert = Input::get('insert');
            $naziv_check = Input::get('naziv_check');
            $end_check = Input::get('end_check');

            // $response = array();

            foreach($roba_ids as $roba_id){
            if($naziv_check && !$end_check){
                DB::statement("UPDATE roba set naziv = ('".$insert."' || ' ' || naziv), naziv_web = ('".$insert."' || ' ' || naziv_web) WHERE roba_id = $roba_id");
                }
            elseif($naziv_check && $end_check){
                DB::statement("UPDATE roba set naziv = (naziv || ' ' || '".$insert."'), naziv_web = (naziv_web || ' ' || '".$insert."') where roba_id = $roba_id");
                }
            elseif(!$naziv_check && !$end_check){
                DB::statement("UPDATE roba set web_opis = ('".$insert."' || ' ' || web_opis) where roba_id = $roba_id");
                }
            elseif(!$naziv_check && $end_check){
                DB::statement("UPDATE roba set web_opis = (web_opis || ' ' || '".$insert."') where roba_id = $roba_id");
                }
            }

            if($naziv_check){
                AdminSupport::saveLog('ARTIKLI_DODAJ_TEKST_NAZIV', $roba_ids);
            }else{
                AdminSupport::saveLog('ARTIKLI_DODAJ_TEKST_OPIS', $roba_ids);
            }


        }
        //Replace texta
        elseif ($action == 'text_replace') {
            $roba_ids = Input::get('roba_ids');
            $replaceFrom = Input::get('replaceFrom');
            $replaceTo = Input::get('replaceTo');
            $naziv_check = Input::get('naziv_check');
            $deoTeksta = Input::get('deoTeksta');

            $response = array('roba_ids'=>array(),'counter'=>0);

            if ($deoTeksta) {
                foreach($roba_ids as $roba_id){

                    if($naziv_check){ 
                        $new_value = DB::table('roba')->where('roba_id', $roba_id)->pluck('naziv');
                    }elseif (!$naziv_check) {
                        $new_value = DB::table('roba')->where('roba_id', $roba_id)->pluck('web_opis');
                    }

                    $new_value = str_ireplace($replaceFrom, $replaceTo, $new_value, $count);

                    $response['counter'] = $response['counter'] + $count;

                    if($naziv_check){       
                        DB::statement("UPDATE roba set naziv = '" .$new_value. "', naziv_web = '" .$new_value. "' where roba_id = '".$roba_id."' ");
                        $response['roba_ids'][$roba_id] = $new_value;
                    }elseif (!$naziv_check) {
                        DB::statement("UPDATE roba set web_opis = '" .$new_value. "' where roba_id = '".$roba_id."' ");
                    }
                } 
            }elseif (!$deoTeksta) {
                
                $replace_from_arr = explode(' ',trim($replaceFrom));

                foreach($roba_ids as $roba_id){

                    if($naziv_check){ 
                        $db_value = DB::table('roba')->where('roba_id', $roba_id)->pluck('naziv');
                    }elseif (!$naziv_check) {
                        $db_value = DB::table('roba')->where('roba_id', $roba_id)->pluck('web_opis');
                    }

                    $db_value_arr = explode(" ", trim($db_value));

                    for ($i=0; $i<count($db_value_arr); $i++){
                        if(strtoupper($db_value_arr[$i]) == strtoupper($replace_from_arr[0])){
                            $j=1;
                            $k=$i+1;
                            while ($j<count($replace_from_arr) && $k<count($db_value_arr) && strtoupper($db_value_arr[$k]) == strtoupper($replace_from_arr[$j])){
                                $k++;
                                $j++;
                            }
                            if($j == count($replace_from_arr)){
                                $db_value_arr[$i] = $replaceTo;
                                array_splice($db_value_arr, $i+1, count($replace_from_arr) - 1);
                                $response['counter']++;                               
                            }                       
                        }
                    }
                
                    $new_value = implode(" ",$db_value_arr);

                    if($naziv_check){       
                        DB::statement("UPDATE roba set naziv = '" .$new_value. "', naziv_web = '" .$new_value. "' where roba_id = '".$roba_id."' ");
                        $response['roba_ids'][$roba_id] = $new_value;
                    }elseif (!$naziv_check) {
                        DB::statement("UPDATE roba set web_opis = '" .$new_value. "' where roba_id = '".$roba_id."' ");
                    }   
                }
            }

            if ($naziv_check) {
                AdminSupport::saveLog('ARTIKLI_IZMENI_TEKST_NAZIV', $roba_ids);
            }else{
                AdminSupport::saveLog('ARTIKLI_IZMENI_TEKST_OPIS', $roba_ids);     
            }     

            return json_encode($response);
            
        }
        elseif ($action == 'data-generisane') {
            $grupa_pr_id = Input::get('grupa_pr_id');
            $count = DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$grupa_pr_id,'web_b2c_prikazi'=>1))->count();
            $response = array();
            if($count > 0){
                $response['status'] = 'error';
            }else{
                $data = array(
                    'nazivi' => AdminSupport::getGrupaKarak($grupa_pr_id)
                    );
                $response['table_content'] = View::make('admin/partials/ajax/generisane-table-content',$data)->render();
                $response['status'] = 'success';
            }
            echo json_encode($response);
        }
        elseif ($action == 'add-generisane') {
            $roba_ids = Input::get('roba_ids');
            $naziv_id = Input::get('karakteristika_naziv_id');
            $vrednost_id = Input::get('karakteristika_vrednost_id');

            DB::table('web_roba_karakteristike')->whereIn('roba_id',$roba_ids)->where('grupa_pr_naziv_id',$naziv_id)->update(array('grupa_pr_vrednost_id' => $vrednost_id,'vrednost' => DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id',$vrednost_id)->pluck('naziv')));

            foreach($roba_ids as $roba_id){
                $check =  DB::table('web_roba_karakteristike')->where(array('roba_id'=>$roba_id,'grupa_pr_naziv_id'=>$naziv_id))->count();
                $active =  DB::table('roba')->where('roba_id',$roba_id)->pluck('flag_aktivan');
                if($check == 0 && $active==1){
                    $rbr_max = DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->max('rbr');
                    $insert_data = array(
                        'roba_id' => $roba_id,
                        'rbr' => isset($rbr_max) ? $rbr_max + 1 : 1,
                        'grupa_pr_naziv_id' => $naziv_id,
                        'grupa_pr_vrednost_id' => $vrednost_id,
                        'vrednost' => DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id',$vrednost_id)->pluck('naziv')
                        );
                    // $old_values = DB::table('web_roba_karakteristike')->max('web_roba_karakteristike_id');
                    DB::table('web_roba_karakteristike')->insert($insert_data);
                    // $new_values = DB::table('web_roba_karakteristike')->max('web_roba_karakteristike_id');
                }
            }
            AdminSupport::saveLog('ARTIKLI_DODAJ_G_KARAKTERISTIKE', $roba_ids);
        }
        elseif ($action == 'delete-generisane') {
            $roba_ids = Input::get('roba_ids');
            $naziv_id = Input::get('karakteristika_naziv_id');
            AdminSupport::saveLog('ARTIKLI_OBRISI_G_KARAKTERISTIKE', $roba_ids);
            DB::table('web_roba_karakteristike')->whereIn('roba_id',$roba_ids)->where('grupa_pr_naziv_id',$naziv_id)->delete();
        }
        elseif ($action == 'delete-karakteristike') {
            $roba_ids = Input::get('roba_ids');
            $kind = Input::get('kind');

            AdminSupport::saveLog('ARTIKLI_OBRISI_KARAKTERISTIKE', $roba_ids);
            if($kind=='0'){
                DB::table('roba')->whereIn('roba_id',$roba_ids)->update(array('web_karakteristike'=>null));
            }
            elseif($kind=='1'){
                DB::table('web_roba_karakteristike')->whereIn('roba_id',$roba_ids)->delete();
            }
            elseif($kind=='2'){
                DB::table('dobavljac_cenovnik_karakteristike')->whereIn('roba_id',$roba_ids)->update(array('roba_id'=>-1));
            }


        }
        elseif ($action == 'html-transfer') {
            $roba_ids = Input::get('roba_ids');
            // DB::statement("UPDATE roba SET web_opis = web_opis||'<br>'||web_karakteristike WHERE roba_id IN (".implode(', ',$roba_ids).") AND web_opis NOT LIKE '%web_karakteristike%'");
            foreach ($roba_ids as $roba_id) {
                $obj = DB::table('roba')->where('roba_id',$roba_id)->first();
                if(strpos($obj->web_opis,$obj->web_karakteristike) !== false){
                }else{
                    DB::table('roba')->where('roba_id',$roba_id)->update(array('web_opis'=>$obj->web_opis.'<br>'.$obj->web_karakteristike));
                }
            }
            AdminSupport::saveLog('ARTIKLI_PREBACI_HTML_KARAKTERISTIKE', $roba_ids);
        }
        elseif ($action == 'garancija_save') {
            $roba_ids = Input::get('roba_ids');
            $broj_meseci = Input::get('broj_meseci');
            $produzena_garancija = Input::get('produzena_garancija');

            $data = array('garancija'=>$broj_meseci);
            if($produzena_garancija == 1){
                $data['produzena_garancija'] = 1;
            }

            DB::table('roba')->whereIn('roba_id',$roba_ids)->where('flag_zakljucan',false)->update($data);

            AdminSupport::saveLog('ARTIKLI_PRODUZENE_GARANCIJE_DODAJ', $roba_ids);
        }
        elseif ($action == 'garancija_delete') {
            $roba_ids = Input::get('roba_ids');
            $produzena_garancija = Input::get('produzena_garancija');

            if($produzena_garancija == 1){
                DB::table('roba')->whereIn('roba_id',$roba_ids)->where('flag_zakljucan',false)->update(array('produzena_garancija'=>0));
            }

            AdminSupport::saveLog('ARTIKLI_PRODUZENE_GARANCIJE_OBRISI', $roba_ids);
        }

    //upis dodatnih grupa
        elseif ($action == 'dodela_dodatnih_grupa') {
            $dodela_dodatnih_grupa_akcija = Input::get('dodela_dodatnih_grupa_akcija');
            $roba_ids = Input::get('roba_ids');
            $grupe_ids = Input::get('grupe_ids');
            
            if($dodela_dodatnih_grupa_akcija == 'dodela'){
                foreach($roba_ids as $roba_id){
                    $roba_grupe = array_map('current',DB::table('roba')->select('grupa_pr_id')->where('roba_id', $roba_id)->get());
                    array_push($roba_grupe,DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id'));

                    $diff_grupa_pr_ids=array_diff($grupe_ids,$roba_grupe);

                    foreach($diff_grupa_pr_ids as $diff_grupa_pr_id){
                        $pom = DB::table('roba_grupe')->where('roba_id', $roba_id)->pluck('grupa_pr_id');
                        if($pom != $diff_grupa_pr_id){
                            DB::table('roba_grupe')->insert(array('roba_id'=>$roba_id,'grupa_pr_id'=>$diff_grupa_pr_id));
                        }
                    }
                }

                AdminSupport::saveLog('ARTIKLI_DODATNE_GRUPE_DODELI', $roba_ids);
            }else if($dodela_dodatnih_grupa_akcija == 'brisanje'){
                if(count($grupe_ids) > 0){
                AdminSupport::saveLog('ARTIKLI_DODATNE_GRUPE_OBRISI', $roba_ids);
                    DB::table('roba_grupe')->whereIn('roba_id',$roba_ids)->whereIn('grupa_pr_id',$grupe_ids)->delete();
                }
            }else if($dodela_dodatnih_grupa_akcija == 'obrisi_sve'){
                AdminSupport::saveLog('ARTIKLI_DODATNE_GRUPE_OBRISI_SVE', $roba_ids);
                DB::table('roba_grupe')->whereIn('roba_id',$roba_ids)->delete();
            }
        }
        elseif($action == 'promena_sifre_artikal') {
            $art_id = Input::get('art_id');
            $val = Input::get('val');

            $response = array();
            if(strlen($val) > 255){
                $response['message'] = 'Vrednost je neodgovarajuće dužine';
            }else{
                if(AdminOptions::sifra_view()==2){
                    DB::table('roba')->where('roba_id', $art_id)->update(['sifra_is' => $val]);
                }elseif(AdminOptions::sifra_view()==3){
                    DB::table('roba')->where('roba_id', $art_id)->update(['sku' => $val]);
                }elseif(AdminOptions::sifra_view()==4){
                    DB::table('roba')->where('roba_id', $art_id)->update(['sifra_d' => $val]);
                }
                $response['message'] = 'Uspešno ste promenili vrednost';
            }
            $response['value'] = $val;
            AdminSupport::saveLog('ARTIKLI_SIFRA_BRZA_IZMENA', array($art_id));

            return Response::json($response);
        }
        elseif($action == 'povezi_u_srodne'){
            $selected_roba_ids = Input::get('selected_roba_ids');
            if(sizeof($selected_roba_ids) > 1) {
                foreach ($selected_roba_ids as $roba_id) {
                    foreach (array_diff($selected_roba_ids,array($roba_id)) as $srodni_roba_id) {
                        if(!DB::table('srodni_artikli')->where(array('roba_id' => $roba_id,'srodni_roba_id' => $srodni_roba_id))->first()) {
                            DB::table('srodni_artikli')->insert(array('roba_id' => $roba_id,'srodni_roba_id' => $srodni_roba_id));
                            AdminSupport::saveLog('SRODNI_ARTIKLI_DODAJ', array($srodni_roba_id));
                        }
                    }
                }
            }
        }
        elseif($action == 'razvezi_srodne'){
            $selected_roba_ids = Input::get('selected_roba_ids');
            if(sizeof($selected_roba_ids) > 1) {
                foreach ($selected_roba_ids as $roba_id) {
                    foreach (array_diff($selected_roba_ids,array($roba_id)) as $srodni_roba_id) {
                        if(DB::table('srodni_artikli')->where(array('roba_id' => $roba_id,'srodni_roba_id' => $srodni_roba_id))->first()) {
                            DB::table('srodni_artikli')->where(array('roba_id' => $roba_id,'srodni_roba_id' => $srodni_roba_id))->delete();
                            AdminSupport::saveLog('SRODNI_ARTIKLI_OBRISI', array($srodni_roba_id));
                        }
                    }
                }
            }
        }
    }
    function products_import()
        {
        if(AdminOptions::gnrl_options(3025) == 1 && DB::table('roba')->count() >= AdminOptions::gnrl_options(3026)){
            return Redirect::back()->withErrors(array('import_file'=>'Unošenje novih artikala nije dozvoljeno!'));
        }

        $file = Input::file('import_file');

        $validator = Validator::make(array('import_file' => Input::file('import_file')), array('import_file' => 'required'), array('required' => 'Niste izabrali fajl.'));
        if($validator->fails()){
            return Redirect::back()->withErrors($validator->messages());
        }else{
            $extension = $file->getClientOriginalExtension();
            if(Input::hasFile('import_file') && $file->isValid() && ($extension == 'xml' || $extension == 'xls' || $extension == 'xlsx' || $extension == 'csv' || $extension == 'zip')){
                
                if($extension != 'zip'){
                    $file->move("./files/", 'direct_import.'.$extension);
                }else{
                    $file->move("./files/", 'backup_images.'.$extension);
                }

                $old_value = DB::table('roba')->max('roba_id');

                $success = false;
                if($extension == 'xml'){
                    $success = AdminDirectImport::xml_execute();
                }elseif($extension == 'xls'){
                    $success = AdminDirectImport::xls_execute();
                }elseif($extension == 'xlsx'){
                    $success = AdminDirectImport::xlsx_execute();
                }elseif($extension == 'csv'){
                    $success = AdminDirectImport::csv_execute();
                }elseif($extension == 'zip'){
                    $success = AdminSupport::extract_images();
                }

                $new_value = DB::table('roba')->max('roba_id');
                
                AdminSupport::saveLog('ARTIKLI_IMPORT', range($old_value+1, $new_value));
                if($extension != 'zip'){
                    File::delete('files/direct_import.'.$extension);
                }else{
                    File::delete('files/backup_images.'.$extension);
                }

                if(!$success){
                    return Redirect::back()->withErrors(array('import_file'=>'Fajl je neispravan.'));
                }
                return Redirect::back();
            }
            return Redirect::back()->withErrors(array('import_file'=>'Import iz ovog fajla nije podrzan.'));
        }
    }

    function products_warranty_import(){
        
        $file = Input::file('import_warranty_file');
        $validator = Validator::make(array('import_warranty_file' => Input::file('import_warranty_file')), array('import_warranty_file' => 'required'), array('required' => 'Niste izabrali fajl.'));
        if($validator->fails()){
            return Redirect::back()->withErrors($validator->messages());
        }else{
            $extension = $file->getClientOriginalExtension();
            if(Input::hasFile('import_warranty_file') && $file->isValid() && ($extension == 'xls' || $extension == 'xlsx')){
                

                $file->move("./files/", 'lista_garancija.'.$extension);

                $success = false;
                
                $success = AdminSupport::upload_warranties($extension);

                if(!$success){
                    return Redirect::back()->withErrors(array('import_warranty_file'=>'Fajl je neispravan.'));
                }
                return Redirect::back();
            }
            return Redirect::back()->withErrors(array('import_warranty_file'=>'Import iz ovog fajla nije podrzan.'));
        }
    }

    function check_warranty(){
        $serijski_broj = Input::get('serijski_broj');

        $result = DB::table('garancija_datum')
                ->where('serijski_broj', '=', $serijski_broj)
                ->first();  

        if ($result) {
            $responseText = $result->naziv_artikla . '<br>Datum garancije: ' . $result->datum_garancije;
            return Response::json($responseText);
        } else {
            return Response::json(['error' => 'Za uneti serijski broj ne postoji garancija'], 404);
        }
    }

}