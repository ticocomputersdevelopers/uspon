<?php
use Service\TranslatorService;

class Admin extends Controller {
   function index(){
        if(AdminOptions::web_options(130) == 2){
            if(!is_null(AdminSupport::getLoggedUser('KOMERCIJALISTA'))){
                return Redirect::to('admin/b2b/kupci_partneri/partneri');
            }else{
                if(!AdminOptions::is_shop()){
                    return Redirect::to('admin/b2b/b2b_stranice/0');
                }
                return Redirect::to('admin/b2b/narudzbine/0/0/0/0/0');
            }
        }else{
            if(!is_null(AdminSupport::getLoggedUser('KOMERCIJALISTA'))){
                return Redirect::to('admin/kupci_partneri/partneri'); 
            }else{
                if(!AdminOptions::is_shop()){
                    return Redirect::to('admin/stranice/0');
                }
                return Redirect::to('admin/porudzbine/sve/0/0/0/0/0');
            }
        }
    }

    function admin_login(){
        AdminSupport::saveLog('LOGOVANJE', array());
        return View::make('admin/login');     
    }

    function admin_login_store() {
        $username=Input::get('username');
        $password=Input::get('password');
        $username=addslashes($username);
        $password=addslashes($password);
    //    $password=md5($password);

        $validator = Validator::make(array('username'=>$username,'password'=>$password),
        array(
            'username' => 'required|between:3,255',
            'password' => 'required|between:3,255|exists:imenik,password,login,'.$username.',flag_aktivan,1'
        ),
        array(
            'required' => 'Niste popunili polje!',
            'between' => 'Broj karaktera mora biti izmedju 3 i 20!',
            'alpha_num' => 'Polje sme sadrzati samo slova i cifre!',
            'exists' => 'Uneli ste pogresno korisnicko ime ili lozinku!',
        ));

        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin-login')->withInput()->withErrors($validator->messages());
        }
        else {

            $imenik_id=DB::table('imenik')->where(array('login'=>addslashes($username),'password'=>addslashes($password)))->pluck('imenik_id');
            Session::put('b2c_admin'.AdminOptions::server(),intval($imenik_id));

            // $filename = "files/logs/users/administrators.txt";
            // $myfile = fopen($filename, "r");
            // $size = filesize($filename);
            // $rows = explode("\n",fread($myfile,$size));
            // fclose($myfile);

            // $count = count($rows);
            // if($size > 500000000){
            //     unlink($filename);
            //     for($i=$count/2;$i<=$count;$i++){
            //         $myfile = fopen($filename, "a");
            //         $line = $rows[$i];
            //         fwrite($myfile, $line);
            //         fclose($myfile);  
            //     }
            // }
            
            if(in_array(AdminOptions::web_options(130),array(0,1))){
                AdminSupport::saveLog('LOGOVANJE', array($imenik_id));
                return Redirect::to(AdminOptions::base_url().'admin');
            }else{
                AdminSupport::saveLog('LOGOVANJE', array($imenik_id));

                return Redirect::to(AdminOptions::base_url().'admin/b2b');
            }
        }
        
    }

    public function sso_login($hash,$shop=false){
        $emailAndPassword = AdminSupport::custom_decrypt(urldecode($hash));
        $email = explode(' ',$emailAndPassword)[0];
        $password = explode(' ',$emailAndPassword)[1];

        $imenik=DB::table('imenik')->where(array('login'=>addslashes($email),'password'=>addslashes($password)))->first();
        if($imenik){
            Session::put('b2c_admin'.AdminOptions::server(),intval($imenik->imenik_id));

            // $filename = "files/logs/users/administrators.txt";
            // $myfile = fopen($filename, "r");
            // $size = filesize($filename);
            // $rows = explode("\n",fread($myfile,$size));
            // fclose($myfile);

            // $count = count($rows);
            // if($size > 500000000){
            //     unlink($filename);
            //     for($i=$count/2;$i<=$count;$i++){
            //         $myfile = fopen($filename, "a");
            //         $line = $rows[$i];
            //         fwrite($myfile, $line);
            //         fclose($myfile);  
            //     }
            // }
            if($shop == false){
                AdminSupport::saveLog('LOGOVANJE', array($imenik->imenik_id));
                return Redirect::to(AdminOptions::base_url().'admin');
            }else{
                AdminSupport::saveLog('LOGOVANJE', array($imenik->imenik_id));
                return Redirect::to(AdminOptions::base_url());
            }
        }
        return Redirect::to(AdminOptions::base_url());       
    }

    //LOGIN ON AOP
    public function account_settings($select_plan=null){
        $admin = DB::table('imenik')->where('imenik_id',Session::get('b2c_admin'.AdminOptions::server()))->first();
        $hash = AdminSupport::custom_encrypt($admin->login.' '.$admin->password);
        $ssoUrl = 'http://www.selltico.com/sso-login/'.$hash. (!is_null($select_plan) ? '/'.$select_plan : '');
        AdminSupport::saveLog('LOGOVANJE', array($admin->imenik_id));
        return Redirect::to($ssoUrl);        
    }

    function logout($confirm=null){
        AdminSupport::saveLog('LOGOVANJE_OUT', array());
        Session::forget('b2c_admin'.AdminOptions::server());

        Session::forget('rma_user_'.Options::server());
        Session::forget('rma_user_kind_'.Options::server());

        Session::forget('dokumenti_user_'.Options::server());
        Session::forget('dokumenti_user_kind_'.Options::server());
        Session::forget('dokumenti_ponuda_id'.Options::server());

        if(AdminOptions::gnrl_options(3013)){
            return Redirect::to(AdminOptions::base_url());
        }

        return Redirect::to(AdminOptions::base_url().'admin-login');
    }
    
    public function stranice($stranica_id,$jezik_id=null,$page_section_id=0,$page_section_type_id=null){
        if(is_null($jezik_id)){
            $jezik_id = DB::table('jezik')->where(array('aktivan'=>1,'izabrani'=>1))->pluck('jezik_id');
        }
        if(AdminOptions::is_shop()){
            $query_list_pages=DB::table('web_b2c_seo')->orderBy('rb_strane','asc')->get();
        }else{
            $query_list_pages=DB::table('web_b2c_seo')->whereNotIn('naziv_stranice',array('korpa','brendovi','konfigurator','registracija','pravno_lice','fizicko_lice','sve-kategorije','akcija','login','sve-o-kupovini'))->orderBy('rb_strane','asc')->get();
        }
        $query_stranica=DB::table('web_b2c_seo')->where('web_b2c_seo_id',$stranica_id)->first();
        $stranice=DB::table('web_b2c_seo')->where('web_b2c_seo_id',$stranica_id)->get();
        if(!is_null($query_stranica)){
            $stranica_jezik=DB::table('web_b2c_seo_jezik')->where(array('web_b2c_seo_id'=>$query_stranica->web_b2c_seo_id, 'jezik_id'=>$jezik_id))->first();
            $seo = AdminSeo::page($query_stranica->naziv_stranice,$jezik_id);
        }      

        $stranica_sekcije = $stranica_id > 0 ? AdminStranice::pageSections($stranica_id) : [];
        $sekcije = count($stranica_sekcije) > 0 ?  DB::table('sekcija_stranice')->where('flag_aktivan',1)->whereNotIn('sekcija_stranice_id',array_map(function($item){ return $item->sekcija_stranice_id; },$stranica_sekcije))->get() : DB::table('sekcija_stranice')->where('flag_aktivan',1)->get();


        if(is_null($page_section_type_id)){
            if(!is_null($page_section_id) && $page_section_id > 0){
                $page_section_type_id = DB::table('sekcija_stranice')->where('sekcija_stranice_id',$page_section_id)->pluck('sekcija_stranice_tip_id');
            }else{
                $page_section_type_id = DB::table('sekcija_stranice_tip')->orderBy('rbr','asc')->pluck('sekcija_stranice_tip_id');
            }
        }

        $data=array(
		"strana"=>'stranice',
		"title"=>"Stranice",
		"query_list_pages"=>$query_list_pages,
        "stranice"=>$stranice,
        "stranica"=>!is_null($query_stranica) ? $query_stranica->naziv_stranice : 'nova',
        "jezik_id"=>$jezik_id,
        "jezici" => DB::table('jezik')->where('aktivan',1)->get(),
        'sekcije' => $sekcije,
        'stranica_sekcije' => $stranica_sekcije,
        'sections' => DB::table('sekcija_stranice')->orderBy('sekcija_stranice_id','asc')->get(),
        'page_section' => $page_section_id > 0 ? DB::table('sekcija_stranice')->where('sekcija_stranice_id',$page_section_id)->first() : (object) ['sekcija_stranice_id' => 0, 'sekcija_stranice_tip_id' => $page_section_type_id, 'naziv'=>'', 'flag_aktivan' => 1, 'boja_pozadine' => null, 'puna_sirina' => 0, 'slajder_id' => 0, 'tip_artikla_id' => null],
        'page_section_lang' => $page_section_id > 0 && !is_null($page_section_lang=AdminStranice::sectionLang($page_section_id,$jezik_id)) ? $page_section_lang : (object) ['sadrzaj' => ''],
        'page_section_type' => AdminStranice::getSectionType($page_section_type_id)
		);

        if($stranica_id==0 || is_null($query_stranica)){
            $data['jezik_id']=$jezik_id;
            $data['naziv_stranice']='';
            $data['naziv']='';
            $data['seo_title']='';
            $data['content']='';
            $data['keywords']='';
            $data['tekst']='';
            $data['desription']='';
            $data['status']=1;
            $data['flag_page']=0;
            $data['web_b2c_seo_id']=0;
            $data['naziv_stranice']='';
            $data['flag_b2b'] = 0;
            $data['tip_artikla_id']=-1;
            $data['parrent_id']=0;
            $data['grupa_pr_id']=-1;
            $data['redirect_web_b2c_seo_id']=null;
            $data['anketa_id']=null;
            $data['disable']=0;
            $data['menu_top']=false;
            $data['header_menu']=false;
            $data['footer']=false;
            $data['b2b_header']=false;
            $data['b2b_footer']=false;
        } else {
            $data['jezik_id']=$jezik_id;
            $data['naziv_stranice']=$query_stranica->naziv_stranice;
            $data['naziv']= ($stranica_jezik && !is_null($stranica_jezik->naziv) && $stranica_jezik->naziv != '') ? $stranica_jezik->naziv : $query_stranica->title;
            $data['tekst']=$query_stranica->tekst;
            $data['seo_title']= $seo->title;
            $data['content']= $stranica_jezik ? $stranica_jezik->sadrzaj : '';
            $data['keywords']= $seo->keywords;
            $data['desription'] = $seo->description;
            $data['status']=2;
            $data['flag_page']=$query_stranica->flag_page;
            $data['web_b2c_seo_id']=$query_stranica->web_b2c_seo_id;
            $data['naziv_stranice']=$query_stranica->naziv_stranice;
            $data['flag_b2b']=$query_stranica->flag_b2b_show;
            $data['tip_artikla_id']=$query_stranica->tip_artikla_id;
            $data['parrent_id']=$query_stranica->parrent_id;
            $data['grupa_pr_id']=$query_stranica->grupa_pr_id;
            $data['redirect_web_b2c_seo_id']=$query_stranica->redirect_web_b2c_seo_id;
            $data['anketa_id']=$query_stranica->anketa_id;
            $data['disable']=$query_stranica->disable;
            $data['menu_top']=$query_stranica->menu_top ? true : false;
            $data['header_menu']=$query_stranica->header_menu ? true : false;
            $data['footer']=$query_stranica->footer ? true : false;
            $data['b2b_header']=$query_stranica->b2b_header ? true : false;
            $data['b2b_footer']=$query_stranica->b2b_footer ? true : false;
        }        
    return View::make('admin/page',$data);
 
    }
    public function delete_page(){

        $web_b2c_seo_id = Input::get('web_b2c_seo_id');
        $web_b2c_seo = DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id))->first();
        if($web_b2c_seo && $web_b2c_seo->disable == 0 && $web_b2c_seo->web_b2c_seo_id != 1){
            AdminSupport::saveLog('STRANICA_OBRISI', array($web_b2c_seo_id));
            DB::table('web_b2c_seo')->where(array('parrent_id'=>$web_b2c_seo_id))->update(array('parrent_id'=>0)); 
            DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id,'disable'=>0))->delete();            
        }
        return Redirect::to('admin/stranice/0')->with('message','Uspešno ste obrisali stranicu.');

    }
        
    public function baneri($strana){
          
        $query_banner_list=DB::table('baneri')->where('tip_prikaza',1)->orderBy('redni_broj','asc')->get();  
        $query_slider_list=DB::table('baneri')->where('tip_prikaza',2)->orderBy('redni_broj','asc')->get();  
          $stranice=array(0);
         if($strana=="new-banner")
                {
                $row=array(
                
                'naziv'=>"",
                'link'=>"",
                'img'=>"images/no-image.jpg",
                'tip_prikaza'=>1,
                'flag'=>1
                
                );
                $label=array(
                'naslov'=>"Novi baner : ",
                'naziv'=>"Naziv banera : ",
                'link'=>"Link banera : ",
                'img'=>"Dodajte baner : ",
                'dugme'=>"Dodajte baner",
                 
                );
               
                }
               
            
         else if($strana=="new-slider")
                {
                
                $row=array(
                'naziv'=>"",
                'link'=>"",
                'img'=>"images/no-image.jpg",
                'tip_prikaza'=>2,
                'flag'=>1,
               
                
                );
                
                 $label=array(
                 
                 'naslov'=>"Novi slajder: ",
                'naziv'=>"Naziv slajdera : ",
                'link'=>"Link slajdera : ",
                 'img'=>"Dodajte slajd :  ",
                  'dugme'=>"Dodajte slajd",
                );
             
        }
        else {
            
            foreach(DB::table('baneri')->where('baneri_id',$strana)->get() as $izmena){
                $row=array(
                'baneri_id'=>$izmena->baneri_id,
                'naziv'=>$izmena->naziv,
                'link'=>$izmena->link,
                'img'=>$izmena->img,
                'tip_prikaza'=>$izmena->tip_prikaza,
                'flag'=>2,
                );
            }
            $web_b2c_seo_id=DB::table('baneri')->where('baneri_id',$strana)->pluck("web_b2c_seo_id");
            $tip_prikaza=DB::table('baneri')->where('baneri_id',$strana)->pluck("tip_prikaza");
            if($tip_prikaza==2){
                $label=array(
                 'naslov'=>"Izmeni slajder : ",
                 'naziv'=>"Naziv slajdera : ",
                 'link'=>"Link slajdera : ",
                 'img'=>"Dodajte slajd :  ",
                 'dugme'=>"Sačuvaj",
                  
                );
            }
            else {
                $label=array(
                'naslov'=>"Izmeni baner : ",
                'naziv'=>"Naziv banera : ",
                'link'=>"Link banera : ",
                'img'=>"Dodajte baner : ",
                'dugme'=>"Sačuvaj",
                );
            }
            
            $stranice=explode(',',$web_b2c_seo_id);
        }
        
        $query_stranice=DB::table('web_b2c_seo')->orderBy('web_b2c_seo_id','asc')->get();
          
        $data=array(
		"strana"=>'baneri-slajderi',
		"title"=>"Baneri i slajdovi",
        "query_banner_list"=>$query_banner_list,
        "query_slider_list"=>$query_slider_list,
        "query_stranice"=>$query_stranice,
        "row"=>$row,
        "label"=>$label,
        "stranice"=>$stranice,
        "stranica"=>$strana
		);
    return View::make('admin/page',$data);

    }
    
    function podesavanja(){
    	$data=array(
    		"strana"=>'podesavanja',
    		"title"=>"Podešavanja"
    		);
        return View::make('admin/page',$data);

    }
    public function css(){
        $data=array(
            "strana"=>'css',
            "title"=>"css",
            "custom_css_id"=>1,
            "flag_aktivan"=>DB::table('custom_css')->where('custom_css_id', 1)->pluck('flag_aktivan'),
            "custom_css"=>DB::table('custom_css')->where('custom_css_id', 1)->get()
            );
        return View::make('admin/page',$data);

    }
    public function css_save($id=1){
        $css=Input::get();
        if(isset($css['flag_aktivan'])){
            $flag_aktivan=$css['flag_aktivan'];
        }else{
            $flag_aktivan=0;
        }
        //All::dd($css);
         DB::table('custom_css')->where('custom_css_id',$id)->update(array('sadrzaj'=>$css['sadrzaj'], 'flag_aktivan'=>$flag_aktivan));
        file_put_contents('css/themes/bsmodern/client_custom.css',$css['sadrzaj']);
        AdminSupport::saveLog('CSS_IZMENI_CUSTOM', (array)$id);
        return Redirect::to(AdminOptions::base_url().'admin/css/')->with('message','Uspešno ste dodali css.');
    }
    public function css_delete($id=1){
         DB::table('custom_css')->where('custom_css_id',$id)->update(array('sadrzaj'=>'/* enter your css here */', 'flag_aktivan'=>1));
         file_put_contents('css/themes/bsmodern/client_custom.css','/* enter your css here */');
         AdminSupport::saveLog('CSS_OBRISI_CUSTOM',(array)$id);
        return Redirect::to(AdminOptions::base_url().'admin/css/')->with('message','Uspešno ste obrisali css.');

    }
                
    public function pages(){
        $web_b2c_seo_id=Input::get('web_b2c_seo_id');
        $disable = 0;
        if($web_b2c_seo_id != 0){
            $web_b2c_seo = DB::table('web_b2c_seo')->where('web_b2c_seo_id',$web_b2c_seo_id)->first();
            if($web_b2c_seo){
                $disable = $web_b2c_seo->disable;
            }else{
                return Redirect::to(AdminOptions::base_url().'admin/stranice/nova');
            }
        }
        $jezik_id=Input::get('jezik_id');
        $query_jezik = DB::table('web_b2c_seo_jezik')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id, 'jezik_id'=>$jezik_id));

        $original_naslov = Strip_tags(Input::get('page_name'));
        $naslov= $disable==0 ? $original_naslov : ($jezik_id == 1 ? $web_b2c_seo->title : (!is_null($query_jezik->first()) ? $query_jezik->first()->naziv : $web_b2c_seo->title));
        Strip_tags($naslov);

        $status=Input::get('status');
        $content= null;
        $seo_title=Input::get('seo_title');
        $keywords=Input::get('keywords');
        $description=Input::get('description');
        $header_menu=Input::get('header_menu');
        $footer_menu=Input::get('footer_menu');
        $top_menu=Input::get('top_menu');
        $b2b_header_menu=Input::get('b2b_header_menu');
        $b2b_footer_menu=Input::get('b2b_footer_menu');
        $tekst=Input::get('tekst');
        $parrent_id= Input::get('parrent_id');
        $naziv_stranice=AdminOptions::slugify($naslov);
        $tip_artikla_id= $disable==0 ? Input::get('tip_artikla_id') : -1;
        $grupa_pr_id= $disable==0 ? Input::get('grupa_pr_id') : -1;
        $redirect_web_b2c_seo_id= $disable==0 && is_numeric(Input::get('redirect_web_b2c_seo_id')) && Input::get('redirect_web_b2c_seo_id') > 0 ? Input::get('redirect_web_b2c_seo_id') : null;
        $anketa_id= $disable==0 && is_numeric(Input::get('anketa_id')) && Input::get('anketa_id') > 0 ? Input::get('anketa_id') : null;
           
        $data=array(
        'naziv_stranice'=>$naziv_stranice,
        'flag_page'=>0,
		'title'=>$naslov,
		'flag_b2b_show'=>0,
        'tip_artikla_id' => $tip_artikla_id,
        'grupa_pr_id' => $grupa_pr_id,
        'parrent_id' => $parrent_id,
        'tekst' => $tekst,
        'menu_top' => isset($top_menu) ? 1 : 0,
        'header_menu' => isset($header_menu) ? 1 : 0,
        'footer' => isset($footer_menu) ? 1 : 0,
        'b2b_header' => isset($b2b_header_menu) ? 1 : 0,
        'b2b_footer' => isset($b2b_footer_menu) ? 1 : 0,
        'redirect_web_b2c_seo_id' => $redirect_web_b2c_seo_id,
        'anketa_id' => $anketa_id,
        );

        $seo_data = array('naslov'=>$naslov,'seo_title'=>$seo_title,'description'=>$description,'keywords'=>$keywords);
        $validator_arr = array(
            'seo_title' => 'regex:'.AdminSupport::regex().'|max:60',
            'description' => 'regex:'.AdminSupport::regex().'|max:320',
            'keywords' => 'regex:'.AdminSupport::regex().'|max:159'
        );
        if($disable==0){
            $validator_arr['naslov'] = 'required|regex:'.AdminSupport::regex().'|max:255|unique:web_b2c_seo,title,'.$web_b2c_seo_id.',web_b2c_seo_id';
        }
        $validator = Validator::make($seo_data,$validator_arr);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/stranice/'.$web_b2c_seo_id.($jezik_id != 1 ? '/'.$jezik_id : ''))->withInput()->withErrors($validator->messages());
        }

        if(!($disable==0 AND AdminOptions::is_shop())){
            $data['tip_artikla_id'] = -1;
            $data['grupa_pr_id'] = -1;
        }
        if($web_b2c_seo_id==0){
            $old_value = DB::table('web_b2c_seo')->max('web_b2c_seo_id');
            DB::table('web_b2c_seo')->insert($data);
            $new_value = DB::table('web_b2c_seo')->max('web_b2c_seo_id');
            $web_b2c_seo_id = DB::table('web_b2c_seo')->where('naziv_stranice',$naziv_stranice)->pluck('web_b2c_seo_id');
            AdminSupport::saveLog('STRANICA_DODAJ', array(DB::table('web_b2c_seo')->max('web_b2c_seo_id')));
        }
        else {
            if($jezik_id==1 || DB::table('jezik')->where(array('aktivan'=>1))->count() == 1){
                DB::table('web_b2c_seo')->where('web_b2c_seo_id',$web_b2c_seo_id)->update($data);
            }

            AdminSupport::saveLog('STRANICA_IZMENI', array($web_b2c_seo_id));
        }

        //lang
        $jezik_data = array('naziv'=> $naslov, 'slug' => AdminOptions::slugify($naslov), 'sadrzaj'=>$content,'title'=>$seo_title,'description'=>$description,'keywords'=>$keywords);
        if(!is_null($query_jezik->first())){
            $query_jezik->update($jezik_data);

            if((!is_null($current_jezik = DB::table('jezik')->where(array('aktivan'=>1,'kod'=>'sr','jezik_id'=>$jezik_id))->first()) && !is_null($jezik = DB::table('jezik')->where(array('aktivan'=>1,'kod'=>'cir'))->where('jezik_id','!=',$jezik_id)->first()))
                || (!is_null($current_jezik = DB::table('jezik')->where(array('aktivan'=>1,'kod'=>'cir','jezik_id'=>$jezik_id))->first()) && !is_null($jezik = DB::table('jezik')->where(array('aktivan'=>1,'kod'=>'sr'))->where('jezik_id','!=',$jezik_id)->first()))
            ){
                $translator = new TranslatorService($current_jezik->kod,$jezik->kod);
                $naslov = $translator->translate($naslov);
                $jezik_data = array('naziv'=> $naslov, 'slug' => AdminOptions::slugify($naslov), 'sadrzaj'=>$translator->translate(html_entity_decode($content)),'title'=>$translator->translate($seo_title),'description'=>$translator->translate($description),'keywords'=>$translator->translate($keywords));
                DB::table('web_b2c_seo_jezik')->where(array('web_b2c_seo_id' => $web_b2c_seo_id, 'jezik_id' => $jezik->jezik_id))->update($jezik_data);
            }            
        }else{
            $jezik_data['web_b2c_seo_id'] = $web_b2c_seo_id;
            $jezik_data['jezik_id'] = $jezik_id;
            DB::table('web_b2c_seo_jezik')->insert($jezik_data);

            if($jezik_id==1){
                foreach(DB::table('jezik')->where('aktivan',1)->where('jezik_id','!=',$jezik_id)->orderBy('izabrani','desc')->get() as $jezik){
                    $translator = new TranslatorService(DB::table('jezik')->where('jezik_id',$jezik_id)->pluck('kod'),$jezik->kod);
                    $naslov = $translator->translate($naslov);
                    $jezik_data = array('web_b2c_seo_id' => $web_b2c_seo_id, 'jezik_id' => $jezik->jezik_id, 'naziv'=> $naslov, 'slug' => AdminOptions::slugify($naslov), 'sadrzaj'=>$translator->translate(html_entity_decode($content)),'title'=>$translator->translate($seo_title),'description'=>$translator->translate($description),'keywords'=>$translator->translate($keywords));
                    DB::table('web_b2c_seo_jezik')->insert($jezik_data);                 
                }
            }
        }

        return Redirect::to(AdminOptions::base_url().'admin/stranice/'.$web_b2c_seo_id.($jezik_id != 1 ? '/'.$jezik_id : ''))->with('message','Uspešno ste sačuvali podatke.');
    }
    
    function position(){
        $moved = Input::get('moved');
        $parent_id = Input::get('parent_id');
        $order_arr = Input::get('order');
        $log_ids_arr = array();

        foreach($order_arr as $key => $val){
            DB::table('web_b2c_seo')->where(array('parrent_id'=>$parent_id, 'web_b2c_seo_id'=>$val))->update(array('rb_strane'=>$key));
            $log_ids_arr[] = $val;
        }

        AdminSupport::saveLog('POZICIJA_STRANICA', array($moved));
        
    }
    
    function page_section_position(){
        $web_b2c_seo_id = Input::get('web_b2c_seo_id');
        $order_arr = Input::get('order');

        foreach($order_arr as $rbr => $sekcija_stranice_id){
            DB::table('stranica_sekcija')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id, 'sekcija_stranice_id' => $sekcija_stranice_id))->update(array('rbr'=>$rbr));
            $log_ids_arr[] = $sekcija_stranice_id;
        }

        AdminSupport::saveLog('SEKCIJA_STRANICE_POZICIJA', array($web_b2c_seo_id));
    }
    
    function page_section_add(){
        $web_b2c_seo_id = Input::get('web_b2c_seo_id');
        $sekcija_stranice_id = Input::get('sekcija_stranice_id');
        $log_ids_arr = array();

        $rbr = count(DB::table('stranica_sekcija')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id))->get()) > 0 ? (DB::table('stranica_sekcija')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id))->max('rbr')+1) : 1;

        DB::table('stranica_sekcija')->insert(array('web_b2c_seo_id'=>$web_b2c_seo_id, 'sekcija_stranice_id' => $sekcija_stranice_id, 'rbr'=>$rbr));

        AdminSupport::saveLog('SEKCIJA_STRANICE_DODAVANJE', array($web_b2c_seo_id));
    }
    
    function page_section_remove($page_id,$section_id){
        DB::table('stranica_sekcija')->where(array('web_b2c_seo_id'=>$page_id, 'sekcija_stranice_id' => $section_id))->delete();

        AdminSupport::saveLog('SEKCIJA_STRANICE_UKLANJANJE', array($page_id));

        return Redirect::to('/admin/stranice/'.$page_id);
    }

    function setings_update(){
        
        $pre_data = DB::table('web_options')->where('web_options_id',Input::get('id'))->pluck('int_data');
        $data=array(
            "int_data"=>Input::get('status')
        );
        DB::table('web_options')->where('web_options_id',Input::get('id'))->update($data);

        if($data['int_data'] != $pre_data){
            AdminSupport::saveLog('PODESAVANJA_IZMENI', array(Input::get('id')));
        }

        
    }

    function options_setings_update(){
        
       $data=array(
        "int_data"=>Input::get('status')
        );
        DB::table('options')->where('options_id',Input::get('id'))->update($data);

        AdminSupport::saveLog('PODESAVANJA_OPTIONS_IZMENI', array(Input::get('id')));

    }    
    function options_change_str_data(){
        

        DB::table('web_options')->where('web_options_id',Input::get('id'))->update(['str_data' => Input::get('value')]);

        // AdminSupport::saveLog('PODESAVANJA_OPTIONS_IZMENI', array(Input::get('id')));

    } 

    function css_edit(){
        $css_class_atribute_id=Input::get('id_promene');
        $vrednost_activ=Input::get('vrednost_css');

            $data=array('vrednost_active' => $vrednost_activ);
            DB::table('css_class_atribute')->where('css_class_atribute_id',$css_class_atribute_id)->update($data);
            AdminSupport::saveLog('CSS_IZMENI', $css_class_atribute_id);
       //echo "Promenjeno je za".$css_class_atribute_id." vrednost ".$vrednost_activ;

    }

    function css_reset(){

            foreach (DB::table('css_class_atribute')->get() as $row) {

                 DB::table('css_class_atribute')->where('css_class_atribute_id',$row->css_class_atribute_id)->update(array('vrednost_active' => $row->vrednost_old ));
            }

    }
    
 function css_submit(){
          
          
        $fp=fopen("css/custom.css",'w+');
       
     
        $separator="";
        foreach(DB::table('css_class')->get() as $row_class){
            $separator.=$row_class->naziv." {\n";
           foreach (DB::table('css_class_atribute')->where('css_class_id',$row_class->css_class_id)->get() as $row) {
                    $separator.=$row->naziv_promenjive." : ".$row->vrednost_active.";\n";
                 DB::table('css_class_atribute')->where('css_class_atribute_id',$row->css_class_atribute_id)->update(array('vrednost_old' => $row->vrednost_active ));
            } 
            $separator.="}\n\n";
        }
         fputs($fp, $separator);
        
        fclose($fp);
          
          //echo "Povezano";
            
            
    }
    
    function image_upload(){
            $slika_ime=$_FILES['img']['name'];
            $slika_tmp_ime=$_FILES['img']['tmp_name'];
            move_uploaded_file($slika_tmp_ime,"images/upload/$slika_ime");
            AdminSupport::saveLog('SLIKA_POSTAVI');
            return Redirect::back();
          // echo Admin_model::upload_directory();
    }

    function image_upload_opis(){
            $slika_ime=$_FILES['img']['name'];
            $slika_tmp_ime=$_FILES['img']['tmp_name'];
            if (move_uploaded_file($slika_tmp_ime, "./images/upload/" . $slika_ime)) {
            copy("./images/upload/" . $slika_ime, "./images/upload_opis/" . $slika_ime);
            }
            // move_uploaded_file($slika_tmp_ime,array("./images/upload/$slika_ime", "./images/upload1/$slika_ime"));
            AdminSupport::saveLog('SLIKA_OPIS_POSTAVI');
            return Redirect::back();
          // echo Admin_model::upload_directory();
    }

    function delete_image(){
        $link=".".Input::get('url');
		if(File::exists($link)){
        AdminSupport::saveLog('UPLOADOVANE_SLIKE_OBRISI');
        File::delete($link);
        echo $link." Ok";
		}
		else {
		echo $link." Nije uredu";
		}
        
		
    }
    function upload_images_pages(){
        echo Admin_model::upload_directory();
    }
	
	  function uload_eponuda(){
         $slika_ime=$_FILES['link']['name'];
            $slika_tmp_ime=$_FILES['link']['tmp_name'];
            move_uploaded_file($slika_tmp_ime,"./cenovnici/eponuda/$slika_ime");
            AdminSupport::saveLog('EPONUDA_POSTAVI', array());
         return Redirect::back();
          // echo Admin_model::upload_directory();
    }
	
	 function uload_shopmania(){
         $slika_ime=$_FILES['link']['name'];
            $slika_tmp_ime=$_FILES['link']['tmp_name'];
            move_uploaded_file($slika_tmp_ime,"./cenovnici/shopmania/$slika_ime");
            AdminSupport::saveLog('SHOPMANIA_POSTAVI', array());
         return Redirect::back();
          // echo Admin_model::upload_directory();
    }

    function save_email_options() {
        
        // process the form here

        // create the validation rules ------------------------
        $rules = array(
            'email_port'        => 'required|integer',
            'email_host'        => 'required',
            'email_username'    => 'required|email',
            'email_password'    => 'required',
            'email_subject'     => 'required',
            'email_name'        => 'required',
        );

        // do the validation ----------------------------------
        // validate against the inputs from our form
        $validator = Validator::make(Input::all(), $rules);

        // check if the validator failed -----------------------
        if ($validator->fails()) {
            $messages = $validator->messages();
            Session::set('error_msg', 'Sva polja su obavezna');
            return Redirect::to('admin/podesavanja');
        } else {
            // upisivanje u bazu
            AdminOptions::updateSelected(Input::all());
            Session::forget('error_msg');
            AdminSupport::saveLog('SWIFT_MAILER_KONFIGURISI');
            return Redirect::to('admin/podesavanja');
        }
    }

    function aktivan_modul(){
        $inputs = Input::all();
        if($inputs['action'] == 'options_active'){
            DB::table('options')->where('options_id',$inputs['id'])->update(array('int_data'=>$inputs['aktivan']));
            if ($inputs['aktivan']) {
                AdminSupport::saveLog('PRODAVNICA_PODESAVANJA_PRIKAZ_ARTIKALA_DA', array($inputs['id']));
            }else{
                AdminSupport::saveLog('PRODAVNICA_PODESAVANJA_PRIKAZ_ARTIKALA_NE', array($inputs['id']));
            }
        }
        elseif($inputs['action'] == 'check_swift'){
            DB::table('options')->where('options_id',3003)->update(array('int_data'=>$inputs['aktivan']));
            if ($inputs['aktivan']) {
                AdminSupport::saveLog('PRODAVNICA_PODESAVANJA_SWIFT_MAILER_AKT');
            }else{
                AdminSupport::saveLog('PRODAVNICA_PODESAVANJA_SWIFT_MAILER_NEAKT');
            }
        }
        elseif($inputs['action'] == 'b2b_active'){
            $aktivan = $inputs['aktivan'];
            DB::table('web_options')->where('web_options_id',$inputs['id'])->update(array('int_data'=>$aktivan));

            if($aktivan == 1){
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_PORTAL_AKTIVAN_OBA'); 
            }elseif ($aktivan == 2){
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_PORTAL_AKTIVAN_B2B');
            }elseif($aktivan == 0){
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_PORTAL_AKTIVAN_B2C');
            }

            if(in_array($aktivan,array(1,0))){
                DB::table('prodavnica_tema')->where('prodavnica_tema_id',9)->update(array('aktivna'=>1, 'shop'=>1));
            }
            // DB::table('ac_module')->where('ac_module_id',10000)->update(array('aktivan'=>$aktivan));
            // DB::table('ac_group_module')->whereNotIn('ac_group_id',array(-1,0))->where('ac_module_id',10000)->update(array('alow'=>$aktivan));

            // if($aktivan == 1){
            //     DB::statement('INSERT INTO ac_group_module (ac_group_id,ac_module_id,alow,alow_tico) SELECT ag.ac_group_id, 10000, 1, 1 FROM ac_group ag WHERE ag.ac_group_id > -1 AND NOT EXISTS(SELECT * FROM ac_group_module agm WHERE agm.ac_group_id = ag.ac_group_id AND ac_module_id = 10000)');
            // }
            
            if(AdminOptions::is_shop() && $inputs['aktivan'] == 2){
                DB::table('prodavnica_stil')->update(array('izabrana'=>0));
                DB::table('prodavnica_stil')->where('prodavnica_stil_id',19)->update(array('izabrana'=>1));
                DB::table('prodavnica_tema')->where('prodavnica_tema_id',9)->update(array('aktivna'=>0, 'shop'=>0));                
            }
        }
        elseif($inputs['action'] == 'nacin_placanja'){
            DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$inputs['id'])->update(array('selected'=>$inputs['aktivan']));
            AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_NACIN_PLACANJA', array($inputs['id']));
        }
        elseif($inputs['action'] == 'nacin_isporuke'){
            DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$inputs['id'])->update(array('selected'=>$inputs['aktivan']));
            AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_NACIN_ISPORUKE', array($inputs['id']));
        }
        elseif($inputs['action'] == 'magacin_enable'){
            DB::table('orgj')->where('orgj_id',$inputs['id'])->update(array('b2c'=>$inputs['aktivan']));
            if($inputs['aktivan']){
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_MAGACIN', array($inputs['id']));
            }else{
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_NE_MAGACIN', array($inputs['id']));
            }
        }
        elseif($inputs['action'] == 'magacin_primary'){
            DB::table('imenik_magacin')->where('imenik_magacin_id',10)->update(array('orgj_id'=>$inputs['id']));
            $id = DB::table('imenik_magacin')->where('imenik_magacin_id',10)->pluck('orgj_id');
            AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_MAGACIN_PRIMARNI', array($id));
        }
        elseif($inputs['action'] == 'vrsta_cena'){
            DB::table('vrsta_cena')->where('vrsta_cena_id',$inputs['id'])->update(array('valuta_id'=>$inputs['valuta_id']));
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_VRSTA_CENE', array($inputs['id']));
        }
        elseif($inputs['action'] == 'vrsta_cena_select'){
            DB::table('vrsta_cena')->where('vrsta_cena_id',$inputs['id'])->update(array('selected'=>$inputs['aktivan']));
            if ($inputs['aktivan']) {
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_VRSTA_CENE_OMOGUCI', array($inputs['id']));
            }else{
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_VRSTA_CENE_ONEMOGUCI', array($inputs['id']));
            }
        }
        elseif($inputs['action'] == 'export_enable'){
            DB::table('export')->where('export_id',$inputs['id'])->update(array('dozvoljen'=>$inputs['aktivan']));
            if($inputs['aktivan']){
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_EKSPORT_OMOGUCI', array($inputs['id']));
            }else{
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_EKSPORT_ONEMOGUCI', array($inputs['id']));
            }
        }
        elseif($inputs['action'] == 'auto_import_active'){
            $aktivan = $inputs['aktivan'];
            DB::table('options')->where('options_id',$inputs['id'])->update(array('int_data'=>$aktivan));
            if ($aktivan) {
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_A_IMPORT_AKTIVAN');
            }else{
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_A_IMPORT_NEAKTIVAN');
            }
            echo $aktivan;
        }
        elseif($inputs['action'] == 'theme_select'){
            DB::table('prodavnica_stil')->update(array('izabrana'=>0));
            DB::table('prodavnica_stil')->where('prodavnica_stil_id',$inputs['id'])->update(array('izabrana'=>1));

            if(AdminOptions::is_shop()){
                DB::table('web_options')->where('web_options_id',130)->where('int_data',2)->update(array('int_data'=>1));
            }else{
                DB::table('web_b2c_seo')->whereNotIn('web_b2c_seo_id',array(24,25,27))->update(array('menu_top'=>0,'header_menu'=>0,'footer'=>0));
            }
            AdminSupport::saveLog('PODESAVANJA_TEME', array($inputs['id']));
        }
        elseif($inputs['action'] == 'theme_confirm'){
            DB::table('prodavnica_stil')->where('izabrana',1)->update(array('zakljucana'=>1));
            AdminSupport::saveLog('TEMA_ZAKLJUCAJ', array(DB::table('prodavnica_stil')->where('izabrana', 1)->pluck('prodavnica_stil_id')));
        }
        elseif($inputs['action'] == 'jezik_active'){
            DB::table('jezik')->where('jezik_id',$inputs['id'])->update(array('aktivan'=>$inputs['aktivan']));
            if ($inputs['aktivan']) {
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_JEZIK_DA', array($inputs['id']));
            }else{
                AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_JEZIK_NE', array($inputs['id']));
            }
        }
        elseif($inputs['action'] == 'jezik_primary'){
            DB::table('jezik')->update(array('izabrani'=>0));
            DB::table('jezik')->where('jezik_id',$inputs['id'])->update(array('izabrani'=>1));
            AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_JEZIK_PRIMARNI',array($inputs['id']));
        }
    }

    function bgImgDelete() {
        DB::table('baneri')->where('tip_prikaza', 3)->delete();
        return Redirect::to('admin/baneri-slajdovi/new-banner');
    }

    function bgImgInsert() {
            $slika_ime='1';
            $slika_tmp_ime=$_FILES['bgImg']['tmp_name'];
            move_uploaded_file($slika_tmp_ime,"images/upload/" . $slika_ime);

            $naziv = Input::get('naziv');
            $data = array(
                'naziv' => $naziv,
                'img' => "images/upload/" . $slika_ime,
                'tip_prikaza' => 3

            );
        DB::table('baneri')->insert($data);
        return Redirect::to('admin/baneri-slajdovi/new-banner');
    }

    function save_google_analytics() {
        $data = (object) Input::all();

        $rules = array(
            'google_id' => 'max:50',
        );

        $validator = Validator::make(Input::all(), $rules, array('max'=>'Sadrzaj polja je predugačak.'));

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to('admin/podesavanja')->withInput()->withErrors($validator->messages());
        } else {

            DB::table('options')->where('options_id',3019)->update(array('str_data'=>$data->google_id,'int_data'=>isset($data->active) && $data->active == 'on' ? 1 : 0));
            AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_GOOGLE_ANALITIKA');
            return Redirect::to('admin/podesavanja');
        }        
    }

    function save_facebook_pixel() {
        $data = (object) Input::all();

        $rules = array(
            'facebook_id' => 'max:50',
        );

        $validator = Validator::make(Input::all(), $rules, array('max'=>'Sadrzaj polja je predugačak.'));

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to('admin/podesavanja')->withInput()->withErrors($validator->messages());
        } else {

            DB::table('options')->where('options_id',3036)->update(array('str_data'=>$data->facebook_id,'int_data'=>isset($data->active) && $data->active == 'on' ? 1 : 0));
            AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_FACEBOOK_PIXEL');
            return Redirect::to('admin/podesavanja');
        }        
    }
    function save_loadbee() {
        $data = (object) Input::all();

        $rules = array(
            'loadbee_id' => 'max:50',
        );

        $validator = Validator::make(Input::all(), $rules, array('max'=>'Sadrzaj polja je predugačak.'));

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to('admin/podesavanja')->withInput()->withErrors($validator->messages());
        } else {

            DB::table('options')->where('options_id',3058)->update(array('str_data'=>$data->loadbee_id,'int_data'=>isset($data->active) && $data->active == 'on' ? 1 : 0));
            AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_LOADBEE');
            return Redirect::to('admin/podesavanja');
        }        
    }
    function save_flixmedia() {
        $data = (object) Input::all();

        $rules = array(
            'flixmedia_id' => 'max:50',
        );

        $validator = Validator::make(Input::all(), $rules, array('max'=>'Sadrzaj polja je predugačak.'));

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to('admin/podesavanja')->withInput()->withErrors($validator->messages());
        } else {

            DB::table('options')->where('options_id',3059)->update(array('str_data'=>$data->flixmedia_id,'int_data'=>isset($data->active) && $data->active == 'on' ? 1 : 0));
            AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_FLIXMEDIA');
            return Redirect::to('admin/podesavanja');
        }        
    }
    function save_pitchprint() {
        $data = (object) Input::all();

        $rules = array(
            'pitchprint_id' => 'max:50',
        );

        $validator = Validator::make(Input::all(), $rules, array('max'=>'Sadrzaj polja je predugačak.'));

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to('admin/podesavanja')->withInput()->withErrors($validator->messages());
        } else {

            DB::table('options')->where('options_id',3062)->update(array('str_data'=>$data->pitchprint_id,'int_data'=>isset($data->active) && $data->active == 'on' ? 1 : 0));
            AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_PITCHPRINT');
            return Redirect::to('admin/podesavanja');
        }        
    }
    function save_chat() {
        $data = (object) Input::all();

        $rules = array(
            'key' => 'max:150',
        );

        $validator = Validator::make(Input::all(), $rules, array('key'=>'Sadrzaj polja je predugačak.'));

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to('admin/podesavanja')->withInput()->withErrors($validator->messages());
        } else {

            DB::table('options')->where('options_id',3020)->update(array('str_data'=>$data->key,'int_data'=>isset($data->active) && $data->active == 'on' ? 1 : 0));
            AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_CET');
            return Redirect::to('admin/podesavanja');
        }        
    }

    function save_color() {
        $data = (object) Input::all();

        DB::table('options')->where('options_id',3021)->update(array('int_data'=>isset($data->active) && $data->active == 'on' ? 1 : 0));
        unset($data->active);

        foreach((array) $data as $id => $boja){
            DB::table('prodavnica_boje')->where('prodavnica_boje_id', $id)->update(array('kod'=>$boja));
        }

        AdminSupport::saveLog('PODESAVANJA_PRODAVNICE_BOJE');
        return Redirect::to('admin/podesavanja');
       
    }

    function save_intesa_keys() {
        $data = (object) Input::all();

        DB::table('options')->where('options_id',3023)->update(array('str_data'=>trim($data->client_id)));
        DB::table('options')->where('options_id',3024)->update(array('str_data'=>trim($data->store_key)));

        AdminSupport::saveLog('PODESAVANJA_INTESA_KLJUCEVI');
        return Redirect::to('admin/podesavanja');
    }
    
    public function footer(){        
        $logo = DB::table('footer_data')->where('footer_data_type_id',1)->pluck('logo');
        $dm = DB::table('footer_data')->where('footer_data_type_id',1)->pluck('logo');

        $data=array(
            "strana"=>'footer_setting',
            "title"=> 'Uređivanje footer-a',
            "logo"=>$logo,
            "drustvene_mreze" =>$dm
            
            );
        return View::make('admin/page', $data);
    }
    
    public function footer_edit(){
        $inputs = Input::get();

        $validator = Validator::make($inputs, array('naziv' => 'required|alpha_num|max:150'));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs; 

        if(isset($general_data['drustvene_mreze'])){
                $general_data['drustvene_mreze'] = 1;
                DB::table('footer_data')->update(['drustvene_mreze' => 1]);
            }else{
                $general_data['drustvene_mreze'] = 0; 
                DB::table('footer_data')->update(['drustvene_mreze' => 0]);
            }
        if(isset($general_data['logo'])){
                DB::table('footer_data')->update(['logo' => 0]);
                $general_data['logo'] = 1;
            }else{
                $general_data['logo'] = 0; 
            }

        } 
    }

    public function rma(){
        Session::put('rma_user_'.Options::server(),Session::get('b2c_admin'.AdminOptions::server()));
        Session::put('rma_user_kind_'.Options::server(),'admin');
        return Redirect::to(AdminOptions::base_url().'rma');
    }
    public function dokumenti(){
        if(Admin_model::check_admin(array('DOKUMENTI','DOKUMENTI_PREGLED'))){        
            Session::put('dokumenti_user_'.Options::server(),Session::get('b2c_admin'.AdminOptions::server()));
            Session::put('dokumenti_user_kind_'.Options::server(),'admin');
            Session::put('b2b_user_'.B2bOptions::server(),1);
        }
        return Redirect::to(AdminOptions::base_url().'dokumenti');
    }

}