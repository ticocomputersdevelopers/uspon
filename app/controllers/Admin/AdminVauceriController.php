<?php

class AdminVauceriController extends BaseController {

    public function index()
    {
        $vauceri = array();
        $vauceri = DB::table('vaucer')->orderBy('realizovan', 'DESC')->orderBy('iznos', 'DESC')->get();
        $vauceri_count = count($vauceri);

        if($vauceri_count>0){
            $vauceri[$vauceri_count] = (object) array('vaucer_id'=>0, 'vaucer_broj'=>'', 'iznos'=>'', 'datum_od'=>'', 'datum_do'=>'', 'realizovan'=>'0', 'web_kupac_id'=>-1, 'vaucer_tip_id'=>-1, 'naziv'=>'', 'grupa_pr_id'=>0, 'web_nacin_placanja_id'=>0);
        }else{
            $vauceri[] = (object) array('vaucer_id'=>0, 'vaucer_broj'=>'', 'iznos'=>'', 'datum_od'=>'', 'datum_do'=>'', 'realizovan'=>'0', 'web_kupac_id'=>-1, 'vaucer_tip_id'=>-1, 'naziv'=>'', 'grupa_pr_id'=>0, 'web_nacin_placanja_id'=>0);
        }

        $vauc_isp = DB::table('vauceri_popust')->orderBy('vauceri_popust_id','asc')->get();
        $vauc_count = count($vauc_isp);

        $vauceri_popust = array();
        if($vauc_count>0){      
            for($i=0;$i<$vauc_count;$i++){
                if($i==0){
                    $vauceri_popust[$i] = (object) array('vauceri_popust_id'=>$vauc_isp[$i]->vauceri_popust_id,
                        'web_cena_od'=>0,
                        'web_cena_do'=>$vauc_isp[$i]->web_cena,
                        'popust'=>$vauc_isp[$i]->popust);
                }else{
                    $vauceri_popust[$i] = (object) array('vauceri_popust_id'=>$vauc_isp[$i]->vauceri_popust_id,
                        'web_cena_od'=>$vauc_isp[$i-1]->web_cena,
                        'web_cena_do'=>$vauc_isp[$i]->web_cena,
                        'popust'=>$vauc_isp[$i]->popust);
                }
            }
            $vauceri_popust[$vauc_count] = (object) array('vauceri_popust_id'=>0,
                'web_cena_od'=>$vauc_isp[$vauc_count-1]->web_cena,
                'web_cena_do'=>'','popust'=>'');
        }else{
            $vauceri_popust[] = (object) array('vauceri_popust_id'=>0,'web_cena_od'=>0,'web_cena_do'=>'','popust'=>'');
        }
        $data = array(
                    'strana'=>'vauceri',
                    'title'=> 'Vaučeri',
                    'vauceri'=>$vauceri,
                    'vauceri_popust'=>$vauceri_popust
                );

        return View::make('admin/page', $data);
    }


    public function save()  
    {
        $data = Input::get();

        if($data['vaucer_broj'] == '') {
            $data['vaucer_broj'] = 'G-'.rand(1, 100000);
        }

        $validator_messages = array('required'=>'Polje ne sme biti prazno!',
                                    'numeric'=>'Polje sme da sadrži samo brojeve!',
                                    'digits'=>'Neodgovarajuća dužina vaučer broja!',
                                    'min'=>'Minimalna vrednost polja je neodgovarajuća!',
                                    'max'=>'Prekoračili ste maksimalnu vrednost polja!',
                                    'unique'=>'Vaučer broj mora biti jednistven!'
                                );

        $validator_rules = array(
                'vaucer_broj' => 'required|unique:vaucer,vaucer_broj,'.$data['vaucer_id'].',vaucer_id',
                'iznos' => 'numeric',
                'datum_od' => 'required',
                'datum_do' => 'required'
            );
        
        

        $validator = Validator::make($data, $validator_rules, $validator_messages);     
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/vauceri')->withInput()->withErrors($validator->messages());
        }
        if($data['iznos']==''){
            $iznos = 0;
        }else{
            $iznos = $data['iznos']; 
        }

        if($data['vaucer_id']==0){
            if (!isset($data['kupac']) || $data['kupac'] == -1) {
                $data['kupac'] = null;
            }
            $max_id = DB::table('vaucer')->max('vaucer_id');
            $data['vaucer_id'] = $max_id+1;
            DB::table('vaucer')->insert(array('vaucer_id'=>$data['vaucer_id'], 'vaucer_broj'=>$data['vaucer_broj'],'iznos'=>$iznos,'datum_od'=>$data['datum_od'], 'datum_do'=>$data['datum_do'], 'realizovan'=>$data['realizovan'], 'web_kupac_id'=>$data['kupac'], 'vaucer_tip_id'=>$data['vaucer_tip_id'], 'naziv'=>$data['naziv'], 'grupa_pr_id'=>$data['grupa_pr_id'], 'web_nacin_placanja_id'=>$data['web_nacin_placanja_id']));
            // AdminSupport::saveLog('SIFARNIK_DEFINISANE_MARZE_DODAJ', array(DB::table('vaucer')->max('vaucer_id')));
        }else{
            if (!isset($data['kupac'])  || $data['kupac'] == -1) {
                $data['kupac'] = null;
            }
            DB::table('vaucer')->where('vaucer_id',$data['vaucer_id'])->update(array('vaucer_broj'=>$data['vaucer_broj'], 'iznos'=>$iznos,'datum_od'=>$data['datum_od'], 'datum_do'=>$data['datum_do'], 'realizovan'=>$data['realizovan'], 'web_kupac_id'=>$data['kupac'], 'vaucer_tip_id'=>$data['vaucer_tip_id'], 'naziv'=>$data['naziv'], 'grupa_pr_id'=>$data['grupa_pr_id'], 'web_nacin_placanja_id'=>$data['web_nacin_placanja_id']));
            // AdminSupport::saveLog('SIFARNIK_DEFINISANE_MARZE_IZMENI', array($data['vaucer_id']));
        }
        return Redirect::to(AdminOptions::base_url().'admin/vauceri')->with('success',true);
    }


    public function delete($id)
    {
        // AdminSupport::saveLog('SIFARNIK_DEFINISANE_MARZE_OBRISI', array($id));
        DB::table('vaucer')->where('vaucer_id',$id)->delete();
        return Redirect::to(AdminOptions::base_url().'admin/vauceri')->with('success-delete',true);
    }

    function vauceri_import()
        {
        if(AdminOptions::gnrl_options(3025) == 1 && DB::table('vauceri')->count() >= AdminOptions::gnrl_options(3026)){
            return Redirect::back()->withErrors(array('import_file'=>'Unošenje novih vaučera nije dozvoljeno!'));
        }

        $file = Input::file('import_file');

        $validator = Validator::make(array('import_file' => Input::file('import_file')), array('import_file' => 'required'), array('required' => 'Niste izabrali fajl.'));
        if($validator->fails()){
            return Redirect::back()->withErrors($validator->messages());
        }else{
            $extension = $file->getClientOriginalExtension();
            if(Input::hasFile('import_file') && $file->isValid() && $extension == 'csv'){
                
                    $file->move("./files/", 'direct_import.'.$extension);
                

                $old_value = DB::table('vaucer')->max('vaucer_id');

                $success = false;
                
                    $success = AdminDirectImport::csv_execute_vaucer();

                $new_value = DB::table('vaucer')->max('vaucer_id');
                
                // AdminSupport::saveLog('ARTIKLI_IMPORT', range($old_value+1, $new_value));
                // if($extension != 'zip'){
                    File::delete('files/direct_import.'.$extension);
                // }else{
                //     File::delete('files/backup_images.'.$extension);
                // }

                if(!$success){
                    return Redirect::back()->withErrors(array('import_file'=>'Fajl je neispravan.'));
                }
                return Redirect::back();
            }
            return Redirect::back()->withErrors(array('import_file'=>'Import iz ovog fajla nije podrzan.'));
        }
    }

public function vauceri_save()  
    {
        $data = Input::get();
        $max_id = DB::table('vauceri_popust')->max('vauceri_popust_id');

        $validator_messages = array('required'=>'Polje ne sme biti prazno!',
                                    'numeric'=>'Polje sme da sadrži samo brojeve!',
                                    'digits_between'=>'Dužina unetih cifara je predugačka!',
                                    'min'=>'Minimalna vrednost polja je neodgovarajuća!',
                                    'max'=>'Prekoračili ste maksimalnu vrednost polja!'
                                );

        $validator_rules = array(
                'popust' => 'required|numeric|digits_between:0,10',
                'web_cena' => 'required|numeric|digits_between:0,20'
            );
        
        if($data['vauceri_popust_id']!=0 && $data['vauceri_popust_id'] < $max_id){
            $validator_rules['web_cena'] = 'required|numeric|digits_between:0,20|min:'. DB::table('vauceri_popust')->where('vauceri_popust_id', $data['vauceri_popust_id']-1)->pluck('web_cena');

        }elseif($data['vauceri_popust_id'] == 0){
            $validator_rules['web_cena'] = 'required|numeric|digits_between:0,20|min:'. DB::table('vauceri_popust')->where('vauceri_popust_id', $max_id)->pluck('web_cena');
        }

        $validator = Validator::make($data, $validator_rules, $validator_messages);     
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/vauceri')->withInput()->withErrors($validator->messages());
        }


        if($data['vauceri_popust_id']==0){
            $data['vauceri_popust_id'] = $max_id+1;
            DB::table('vauceri_popust')->insert(array('vauceri_popust_id'=>$data['vauceri_popust_id'],'web_cena'=>$data['web_cena'],'popust'=>$data['popust']));
            AdminSupport::saveLog('SIFARNIK_VAUCERI_POPUST_DODAJ', array(DB::table('vauceri_popust')->max('vauceri_popust_id')));
        }else{
            DB::table('vauceri_popust')->where('vauceri_popust_id',$data['vauceri_popust_id'])->update(array('web_cena'=>$data['web_cena'],'popust'=>$data['popust']));
            AdminSupport::saveLog('SIFARNIK_VAUCERI_POPUST_IZMENI', array($data['vauceri_popust_id']));
        }
        return Redirect::to(AdminOptions::base_url().'admin/vauceri')->with('success',true);
    }


    public function vauceri_delete($id) 
    {
        AdminSupport::saveLog('SIFARNIK_VAUCERI_OBRISI', array($id));
        DB::table('vauceri_popust')->where('vauceri_popust_id',$id)->delete();
        return Redirect::to(AdminOptions::base_url().'admin/vauceri')->with('success-delete',true);
    }





}