<?php

class AdminPermissionController extends Controller {

    function administratori($id=null)
    {
        if(!Admin_model::check_admin()){
            if($id == 0){
                $id = null;
            }
            $admin = "AND imenik_id <> 0";
        }else{
            $admin = "";
        }

        if($id != null){
            $adninistrator = DB::table('imenik')->where('imenik_id',$id)->first();
        }
        $data=array(
            "strana"=>'administratori',
            "title"=> 'Administratori',
            "administratori" => DB::select("SELECT * FROM imenik WHERE imenik_id <> -1".$admin." ORDER BY imenik_id ASC"),
            "imenik_id"=> $id==null ? null : $id,
            "ime"=> $id!=null ? $adninistrator->ime : null,
            "prezime"=> $id!=null ? $adninistrator->prezime : null,
            "login"=> $id!=null ? $adninistrator->login : null,
            "mobilni"=> $id!=null ? $adninistrator->mobilni : null,
            "password"=> $id!=null ? $adninistrator->password : null,
            "kvota"=> $id!=null ? intval($adninistrator->kvota) : null
        );

        return View::make('admin/page', $data);
    }

    function administrator_edit()
    {
        $inputs = Input::get();
        $login_unique = '';
        if($inputs['imenik_id'] != ''){
            $login_unique = ','.$inputs['imenik_id'].',imenik_id';
        }

        Validator::extend('one_digit', 'AdminSupport@one_digit');
        
        $validator_array = array(
            'ime' => 'required|regex:'.AdminSupport::regex().'',
            'prezime' => 'required|regex:'.AdminSupport::regex().'',
            'login' => 'required|email|unique:imenik,login'.$login_unique,
            'password' => 'required|between:8,20|one_digit'
            );
        $messages = array(
            'between' => 'Lozinka mora da sadrži 8 do 20 karaktera.'
        );
        
        $validator = Validator::make($inputs, $validator_array,$messages);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        }else{
            if($inputs['imenik_id'] == ''){
                $imenik_id = DB::select("SELECT MAX(imenik_id) FROM imenik")[0]->max + 1;
                $inputs['imenik_id'] = $imenik_id;
                $inputs['jmbg'] = '';
                $inputs['pol'] = 'm';
                $inputs['password'] = $inputs['password']; //md5
                DB::table('imenik')->insert($inputs);
                AdminSupport::saveLog('ADMINISTRATOR_DODAJ', array(DB::table('imenik')->max('imenik_id')));
            }else{
                $imenik_id = $inputs['imenik_id'];
                $user = DB::table('imenik')->where('imenik_id',$imenik_id)->first();
                if(!is_null($user->aop_user_id)){
                    $inputs['aop_user_id'] = $user->aop_user_id;
                    $updated = AdminSupport::updateAOPUser((object) $inputs);
                    if($updated){
                        $inputs['password'] = $inputs['password']; //md5
                        unset($inputs['imenik_id']);
                        DB::table('imenik')->where('imenik_id',$imenik_id)->update($inputs);
                    }else{
                        $validator->getMessageBag()->add('login', 'Email already used.');
                        return Redirect::back()->withInput()->withErrors($validator->messages());
                    }
                }else{
                    $inputs['password'] = $inputs['password']; //md5
                    unset($inputs['imenik_id']);
                    DB::table('imenik')->where('imenik_id',$imenik_id)->update($inputs);
                    AdminSupport::saveLog('ADMINISTRATOR_IZMENI', array($imenik_id));                    
                }

            }

            return Redirect::to(AdminOptions::base_url().'admin/administratori/'.$imenik_id)->with('message','Uspešno ste sačuvali podatke.');            
        }        
    }

    function administrator_delete($id)
    {
        if($id != 0){            
            AdminSupport::saveLog('ADMINISTRATOR_OBRISI', array($id));
            DB::table('log_korisnik')->where('korisnik_id',$id)->delete();
            DB::table('imenik')->where('imenik_id',$id)->delete();
        }
        return Redirect::to(AdminOptions::base_url().'admin/administratori');
    }

    function grupa_modula($id=null,$glavni_modul_id = null)
    {
        if(!Admin_model::check_admin()){
            if($id == 0){
                $id = null;
            }
            $administrator = "AND ac_group_id <> 0";
        }else{
            $administrator = "";
        }

        if($id!=null){    
            $grupa = DB::table('ac_group')->where('ac_group_id','!=',-1)->where('ac_group_id',$id)->first();
        }
        $data=array(
            "strana"=>'grupa_modula',
            "title"=>$id != null ? $grupa->naziv : 'Novi nivo',
            "grupe_modula" => DB::select("SELECT * FROM ac_group WHERE ac_group_id <> -1".$administrator." ORDER BY ac_group_id ASC"),
            "ac_group_id"=> $id != null ? $id : null,
            "naziv"=> $id != null ? $grupa->naziv : null,
            "opis"=> $id != null ? $grupa->opis : null,
            "moduli"=> $id != null ? array_map('current',DB::select("SELECT ac_module_id FROM ac_group_module WHERE ac_group_id=".$id." AND alow=1")) : array(),
            "glavni_modul_id"=> $glavni_modul_id != null ? $glavni_modul_id : null
        );

        return View::make('admin/page', $data);
    }

    function modul_grupa_edit()
    {
        $inputs = Input::get();

        $validator = Validator::make($inputs, array('naziv' => 'required'));
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
            if($inputs['ac_group_id'] == ''){
                $ac_group_id = DB::select("SELECT nextval('ac_group_ac_group_id_seq')")[0]->nextval;
                $inputs['ac_group_id'] = $ac_group_id;
                DB::table('ac_group')->insert($inputs);
                DB::statement("INSERT INTO ac_group_module (ac_group_id, ac_module_id, alow, alow_tico) SELECT ".intval($inputs['ac_group_id']).", ac_module_id, 0, 1 FROM ac_module WHERE aktivan = 1");
                AdminSupport::saveLog('NIVOI_PRISTUPA_DODAJ', array(DB::table('ac_group')->max('ac_group_id')));
            }else{
                $ac_group_id = $inputs['ac_group_id'];
                unset($inputs['ac_group_id']);
                DB::table('ac_group')->where('ac_group_id',$ac_group_id)->update($inputs);
                AdminSupport::saveLog('NIVOI_PRISTUPA_IZMENI', array($ac_group_id));
            }

            return Redirect::to(AdminOptions::base_url().'admin/grupa-modula/'.$ac_group_id)->with('message','Uspešno ste sačuvali podatke.');
        }
    }

    function modul_grupa_delete($id)
    {
        if($id != 0){
        AdminSupport::saveLog('NIVOI_PRISTUPA_OBRISI', array($id));
            DB::table('ac_group_module')->where('ac_group_id',$id)->delete();
            DB::table('ac_group')->where('ac_group_id',$id)->delete();
        }

        return Redirect::to(AdminOptions::base_url().'admin/grupa-modula');
    }


//logovanja
    function logovi($DatumOd=0, $DatumDo=0, $korisnik_id='SVI', $akcija='SVE', $statistics='')
    {
        $whereStatisticsInsert = "WHERE la.sifra = 'ARTIKLI_DODAJ'";
        $whereStatisticsUpdate = "WHERE la.sifra = 'ARTIKLI_IZMENI'";
        $whereStatisticsDelete = "WHERE la.sifra = 'ARTIKLI_OBRISI'";

        $where = "";
        if(Session::get('b2c_admin'.AdminOptions::server()) != 0){
            $where .= " WHERE korisnik_id <> 0 AND";
        }
        if(!is_null($DatumOd) && !is_null($DatumDo) && $DatumOd!=0 && $DatumDo!=0){
            $where .= $where == "" ? " WHERE" : "";
            $where .= " datum BETWEEN '".$DatumOd."' AND '".$DatumDo."' AND";
            $whereStatisticsInsert .= "AND datum BETWEEN '".$DatumOd."' AND '".$DatumDo."'";
            $whereStatisticsUpdate .= "AND datum BETWEEN '".$DatumOd."' AND '".$DatumDo."'";
            $whereStatisticsDelete .= "AND datum BETWEEN '".$DatumOd."' AND '".$DatumDo."'";

        }
        if(isset($korisnik_id) && $korisnik_id!='SVI'){
            $where .= $where == "" ? " WHERE" : "";
            $where .= " korisnik_id =  '".$korisnik_id."' AND";
        }
      
        if(isset($akcija) && $akcija!='SVE'){
            $where .= $where == "" ? " WHERE" : "";
            $where .= " la.sifra LIKE '%".$akcija."%' AND"; 
        }

        $limit = 50;
        $page = 1;
        if(Input::get('page')){
            $page = Input::get('page');
        }
        $logovi_query = "SELECT datum, ids, naziv_proizvoda, korisnik_id, la.sifra, ime, prezime, naziv
            FROM log_akcija la RIGHT JOIN log_korisnik lk ON la.log_akcija_id = lk.log_akcija_id
            left JOIN imenik i ON lk.korisnik_id = i.imenik_id".substr($where,0,-4)."
            ORDER BY datum DESC";
        $count = count(DB::select($logovi_query));
        $logovi = DB::select($logovi_query." LIMIT ".$limit." OFFSET ".strval(($page-1)*$limit)."");

        if($where == ""){
            $where = "WHERE";
        }
        $logovanja = DB::select("SELECT korisnik_id, ime, prezime, datum as start FROM  log_akcija la RIGHT JOIN log_korisnik lk ON la.log_akcija_id = lk.log_akcija_id LEFT JOIN imenik i ON lk.korisnik_id = i.imenik_id ".$where." la.sifra = 'LOGOVANJE' ORDER BY lk.datum ASC");

        $logovanja_arr = array();
        foreach($logovanja as $log){
            $log_arr = (array) $log;
            $logsOut = DB::select("SELECT datum as finish FROM log_korisnik lk LEFT JOIN log_akcija la ON la.log_akcija_id = lk.log_akcija_id AND lk.korisnik_id = ".$log->korisnik_id." WHERE ".(Session::get('b2c_admin'.AdminOptions::server()) != 0 ? "korisnik_id <> 0 AND" : "")." (la.sifra = 'LOGOVANJE_OUT' or la.sifra = 'LOGOVANJE') AND lk.datum > '".$log->start."' ORDER BY lk.datum ASC");
            if(count($logsOut) > 0){
                $log_arr['finish'] = $logsOut[0]->finish;
            }else{
                $log_arr['finish'] = date('Y-m-d H:i:s'); 
            }
            $logovanja_arr[] = (object) $log_arr;
        }

        $statisticsInsert = DB::select("SELECT korisnik_id, STRING_AGG(naziv_proizvoda,'>+<') as proizvodi, ime from log_akcija la left join log_korisnik lk on (la.log_akcija_id=lk.log_akcija_id) right join imenik i on (lk.korisnik_id = i.imenik_id) ".$whereStatisticsInsert." group by korisnik_id, ime");

        $statisticsUpdate = DB::select("SELECT korisnik_id, STRING_AGG(naziv_proizvoda,'>+<') as proizvodi, ime from log_akcija la left join log_korisnik lk on (la.log_akcija_id=lk.log_akcija_id) right join imenik i on (lk.korisnik_id = i.imenik_id) ".$whereStatisticsUpdate." group by korisnik_id, ime");

        $statisticsDelete = DB::select("SELECT korisnik_id, STRING_AGG(naziv_proizvoda,'>+<') as proizvodi, ime from log_akcija la left join log_korisnik lk on (la.log_akcija_id=lk.log_akcija_id) right join imenik i on (lk.korisnik_id = i.imenik_id) ".$whereStatisticsDelete." group by korisnik_id, ime");



        

        // $nazivi_p = array();
        // $broj = $logovi->ids;
        // echo $broj[1];
        // die;
        // foreach ($logovi->ids as $id) {
        //     $promenljiva = DB::table('roba')->where('roba_id', $id)->pluck('naziv');
        //     if(is_null($promenljiva)){
        //         $nazivi_p[] = 'Nema naziv';
        //     }else{
        //         $nazivi_p[] = $promenljiva;
        //     }
        // }
        // $logovi->ids = implode(',', $logovi->ids);
        
        $data=array(
            "strana"=>'logovi',
            "title"=>'Logovi',           
            "korisnici" => DB::select("SELECT * FROM imenik ".(Session::get('b2c_admin'.AdminOptions::server()) != 0 ? "WHERE imenik_id <> 0 " : "")."ORDER BY imenik_id ASC"),
            "od"=> $DatumOd != 0 ? $DatumOd : '',
            "do"=> $DatumDo != 0 ? $DatumDo : '',
            "korisnik_id"=>$korisnik_id,
            "akcija"=>$akcija,
            "logovanja"=>$logovanja_arr,
            "limit"=>$limit,
            "count"=>$count,
            "logovi"=>$logovi,
            "statisticsInsert"=>$statisticsInsert,
            "statisticsUpdate"=>$statisticsUpdate,
            "statisticsDelete"=>$statisticsDelete,
            "statistics"=>$statistics
        );

        return View::make('admin/page', $data);
    }
 
    public static function komercijalista_narudzbine_export($imenik_id){
        
        $datumOd = Input::get('datum_od');
        $datumDo = Input::get('datum_do');

        $narudzbine = DB::table('web_b2b_narudzbina as wbn')
            ->join('web_b2b_narudzbina_stavka as wbs', 'wbn.web_b2b_narudzbina_id', '=', 'wbs.web_b2b_narudzbina_id')
            ->where('wbn.imenik_id', $imenik_id)
            ->whereBetween(DB::raw("wbn.datum_dokumenta::date"), [$datumOd, $datumDo])
            ->select(
                'wbn.web_b2b_narudzbina_id',
                'wbn.broj_dokumenta',
                'wbn.partner_id',
                DB::raw("TO_CHAR(wbn.datum_dokumenta, 'DD-MM-YYYY') as datum_dokumenta"),
                'wbs.roba_id',
                'wbs.kolicina',
                'wbs.jm_cena',
                DB::raw('(wbs.kolicina * wbs.jm_cena) as ukupno')
            )
            ->orderBy('wbn.broj_dokumenta')
            ->get();

        if (empty($narudzbine)) {
            Session::flash('message', 'Nema podataka za izabrani period.');
            return Redirect::back();
        }

    // Grupiši podatke po broju dokumenta
    $grupisaneNarudzbine = [];
    foreach ($narudzbine as $narudzbina) {
        $grupisaneNarudzbine[$narudzbina->broj_dokumenta]['header'] = [
            'broj_dokumenta' => $narudzbina->broj_dokumenta,
            'partner_id' => $narudzbina->partner_id,
            'datum_dokumenta' => $narudzbina->datum_dokumenta
        ];
        $grupisaneNarudzbine[$narudzbina->broj_dokumenta]['stavke'][] = [
            'roba_id' => $narudzbina->roba_id,
            'kolicina' => $narudzbina->kolicina,
            'jm_cena' => $narudzbina->jm_cena,
            'ukupno' => $narudzbina->ukupno
        ];
    }

    // Kreiraj PHPExcel objekat
    $excel = new PHPExcel();
    $excel->setActiveSheetIndex(0);
    $sheet = $excel->getActiveSheet();

    // Postavi $imenik_id na vrh dokumenta
    $sheet->setCellValue('A1', "Komercijalista ID: " . Admin_model::get_komercijalista($imenik_id));

    // Postavi header-e kolona u drugom redu
    $headers = ['Broj narudžbine', 'Partner', 'Datum narudžbine', 'Artikli', 'Količina', 'Cena', 'Ukupno'];
    $sheet->fromArray([$headers], NULL, 'A3');

    // Popunjavanje podataka
    $row = 4;
    foreach ($grupisaneNarudzbine as $brojDokumenta => $podaci) {
        // Unesi zaglavlje narudžbine
        $sheet->setCellValue("A$row", $podaci['header']['broj_dokumenta']);
        $sheet->setCellValue("B$row", AdminPartneri::partner_naziv($podaci['header']['partner_id']));
        $sheet->setCellValue("C$row", $podaci['header']['datum_dokumenta']);

        $prviRed = $row; // Sačuvaj referencu na prvi red gde počinje narudžbina

        // Unesi stavke
        foreach ($podaci['stavke'] as $stavka) {
            $sheet->setCellValue("D$row", Product::seo_title($stavka['roba_id']));
            $sheet->setCellValue("E$row", $stavka['kolicina']);
            $sheet->setCellValue("F$row", $stavka['jm_cena']);
            $sheet->setCellValue("G$row", $stavka['ukupno']);
            $row++;
        }

        // Spoji ćelije za zaglavlje narudžbine
        $sheet->mergeCells("A{$prviRed}:A" . ($row - 1));
        $sheet->mergeCells("B{$prviRed}:B" . ($row - 1));
        $sheet->mergeCells("C{$prviRed}:C" . ($row - 1));

        // Dodaj ukupan iznos narudžbine ispod stavki
        $ukupnoNarudzbina = array_sum(array_column($podaci['stavke'], 'ukupno'));
        $sheet->setCellValue("G$row", "UKUPNO:");
        $sheet->setCellValue("H$row", $ukupnoNarudzbina);
        $row += 2; // Ostavi prazan red između narudžbina
        }

        // Sačuvaj fajl i pošalji za preuzimanje
        $filename = "narudzbine_{$imenik_id}_{$datumOd}_{$datumDo}.xlsx";
        $filePath = storage_path($filename);
        $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        $writer->save($filePath);

        return Response::download($filePath);
        unlink($filePath);
    }
}

