<?php
use Service\TranslatorService;
use Service\ElasticSearchService;

class AdminGroupsController extends Controller {

    function grupe($id,$karak_id = null,$jezik_id = null)
    {
        if(is_null($jezik_id)){
            $jezik_id = 1;
        }

        $karak_id = $karak_id=='null' ? null : $karak_id;
        $grupa_parents = array();
        AdminSupport::grupaParents($id,$grupa_parents);

        $stranica_jezik = null;
        $seo = null;
        if(!is_null(DB::table('grupa_pr')->where('grupa_pr_id', $id)->first())){
            $stranica_jezik=DB::table('grupa_pr_jezik')->where(array('grupa_pr_id'=>$id, 'jezik_id'=>$jezik_id))->first();
            $jezik_kod = DB::table('jezik')->where('jezik_id',$jezik_id)->pluck('kod');
            $seo = AdminSeo::grupa($id,$jezik_id);
        }
        $prikaz_b2b= null;
        if(AdminB2BOptions::checkB2B())
        {
             $prikaz_b2b =  $id != 0 ? AdminGroups::find($id, 'web_b2b_prikazi') : 1;
        }

        $data=array(
            "strana"=>'grupe',
            "title"=>$id != 0 ? AdminGroups::find($id, 'grupa') : 'Nova grupa',
            "grupa_pr_id"=>$id,
            "grupa_parents"=>$grupa_parents,
            "grupa"=> $id != 0 ? AdminLanguage::trans(AdminGroups::find($id, 'grupa'),$jezik_kod) : null,
            "parrent_grupa_pr_id"=> $id != 0 ? AdminGroups::find($id, 'parrent_grupa_pr_id') : null,
            "sifra"=> $id != 0 ? AdminGroups::find($id, 'sifra') : null,
            "web_b2c_prikazi"=> $id != 0 ? AdminGroups::find($id, 'web_b2c_prikazi') : 1,
            "web_b2b_prikazi"=>$prikaz_b2b,
            "putanja_slika"=> $id != 0 ? AdminGroups::find($id, 'putanja_slika') : null,
            "pozadinska_slika"=> $id != 0 ? AdminGroups::find($id, 'pozadinska_slika') : null,
            "redni_broj"=> AdminArticles::findGrupe($id, 'redni_broj'),
            "nazivi"=> AdminGroups::karakteristikeNaziv($id),
            "vrednost"=> $karak_id!=null ? AdminSupport::getVrednostKarak($karak_id,1) : null,
            "karak_id"=> $karak_id,
            "sablon_opis" => $id != 0 ? AdminGroups::find($id, 'sablon_opis') : null,
            "jezik_id" => $jezik_id,
            "baner_id" => $id != 0 ? AdminGroups::find($id, 'baner_id') : null,
            "jezici" => DB::table('jezik')->where('aktivan',1)->get(),
            "seo_title" => !is_null($seo) ? $seo->title : '',
            "seo_description" => !is_null($seo) ? $seo->description : '',
            "seo_keywords" => !is_null($seo) ? $seo->keywords : '',
            "sablon_opis" => $stranica_jezik ? $stranica_jezik->sablon_opis : ''
        );
               

            return View::make('admin/page', $data);
    }

    function grupa_edit()
    {   
        $inputs = Input::get();

        if($inputs['grupa_pr_id'] == 0){
            $ignore = '|unique:grupa_pr,grupa,NULL,id,parrent_grupa_pr_id,'.$inputs['parrent_grupa_pr_id'].'';
        }else{
            $ignore = '';
        }

        $validator = Validator::make($inputs,
            array(
                'grupa' => 'required|regex:'.AdminSupport::regex().'|max:50'.$ignore,
                'seo_title' => 'regex:'.AdminSupport::regex().'|max:60',
                'seo_description' => 'regex:'.AdminSupport::regex().'|max:320',
                'seo_keywords' => 'regex:'.AdminSupport::regex().'|max:159'
                )
            );
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{

            $general_data = $inputs;
            foreach($general_data as $key => $val){
                if($val == '' || $key == 'premestanje_karakteristika' || $key == 'import'){
                    unset($general_data[$key]);
                }
            }
            unset($general_data['karak_id']);
            unset($general_data['jezik_id']);
            unset($general_data['seo_title']);
            unset($general_data['seo_description']);
            unset($general_data['seo_keywords']);
            unset($general_data['sablon_opis']);
            unset($general_data['pozadinska_slika']);

            if($inputs['grupa_pr_id'] == 0){                
                $general_data['grupa_pr_id'] = DB::select("SELECT MAX(grupa_pr_id) AS max FROM grupa_pr")[0]->max + 1;
                $general_data['sifra'] = DB::select("SELECT MAX((sifra)::integer) AS max FROM grupa_pr")[0]->max + 1;
                $general_data['redni_broj'] = DB::select("SELECT MAX(redni_broj) AS max FROM grupa_pr WHERE parrent_grupa_pr_id = ".$general_data['parrent_grupa_pr_id']."")[0]->max + 1;
            }
            
            $general_data['grupa'] = $inputs['jezik_id'] == 1 ? $inputs['grupa'] : AdminGroups::find($inputs['grupa_pr_id'], 'grupa');
            $general_data['baner_id'] = isset($general_data['baner_id']) && $general_data['baner_id'] > 0 ? $general_data['baner_id'] : null;
        //    $general_data['sifra'] = AdminGroups::sifraGenerate($general_data['parrent_grupa_pr_id']);

            $slika = Input::file('slika');
            if(isset($slika)){
                $extension = $slika->getClientOriginalExtension();
                $putanja = $general_data['grupa_pr_id'].'.webp';
                $putanja_to_convert = $general_data['grupa_pr_id'].'.'.$extension;
                $slika->move('images/groups/', $putanja_to_convert);
                
                self::imagesToWebp('images/groups/'.$putanja_to_convert);   
                $extension=explode('.',$putanja_to_convert);
            
                    if($extension[1] !='webp'){
                        File::delete('images/groups/'.$putanja_to_convert);
                    }

                $general_data['putanja_slika'] = 'images/groups/'.$putanja;
            }
            $pozadinska_slika = Input::file('pozadinska_slika');
            if(isset($pozadinska_slika)){
                $extension = $pozadinska_slika->getClientOriginalExtension();
                $putanja = $general_data['grupa_pr_id'].'-bgimg.webp';
                $putanja_to_convert = $general_data['grupa_pr_id'].'-bgimg.'.$extension;
                $pozadinska_slika->move('images/groups/', $putanja_to_convert);
                
                self::imagesToWebp('images/groups/'.$putanja_to_convert);   
                $extension=explode('.',$putanja_to_convert);
            
                    if($extension[1] !='webp'){
                        File::delete('images/groups/'.$putanja_to_convert);
                    }

                $general_data['pozadinska_slika'] = 'images/groups/'.$putanja;
            }

            unset($general_data['slika']);

            if($inputs['grupa_pr_id'] != 0){
                if(isset($inputs['premestanje_karakteristika']) && $inputs['premestanje_karakteristika'] > 0){

            // kopiranje naziva karakteristika
                DB::statement("INSERT INTO grupa_pr_naziv (SELECT nextval('grupa_pr_naziv_grupa_pr_naziv_id_seq') as grupa_pr_naziv_id, ".$general_data['grupa_pr_id']." as grupa_pr_id ,active,css,naziv,rbr FROM grupa_pr_naziv WHERE  grupa_pr_id= ".$inputs['premestanje_karakteristika']." AND naziv NOT IN( SELECT naziv FROM grupa_pr_naziv WHERE grupa_pr_id = ".$general_data['grupa_pr_id']." ))");

            // kopiranje Vrednosti karakteristika
                DB::statement("INSERT INTO grupa_pr_vrednost (SELECT nextval('grupa_pr_vrednost_grupa_pr_vrednost_id_seq') as grupa_pr_vrednost_id,( SELECT grupa_pr_naziv_id FROM grupa_pr_naziv WHERE grupa_pr_id = ".$general_data['grupa_pr_id']." AND naziv=( SELECT naziv FROM grupa_pr_naziv WHERE grupa_pr_naziv_id= gpv.grupa_pr_naziv_id ) ) as grupa_pr_naziv_id,active,css,naziv,rbr,boja FROM grupa_pr_vrednost gpv WHERE grupa_pr_naziv_id IN ( SELECT grupa_pr_naziv_id FROM grupa_pr_naziv WHERE grupa_pr_id = ".$inputs['premestanje_karakteristika']." )  AND naziv NOT IN (SELECT naziv FROM grupa_pr_vrednost WHERE grupa_pr_naziv_id IN (SELECT grupa_pr_naziv_id FROM grupa_pr_naziv WHERE grupa_pr_id =".$general_data['grupa_pr_id']." ) ))");

                }

                //delete slika
                if(isset($general_data['slika_delete']) && isset($general_data['slika_delete']) == 'on'){
                    $putanja_slika = AdminGroups::find($inputs['grupa_pr_id'], 'putanja_slika');
                    if(isset($putanja_slika)){
                        if(file_exists(trim($putanja_slika))==true){
                            unlink(trim($putanja_slika));
                        }
                    }
                    $general_data['putanja_slika'] = null;               
                }
                unset($general_data['slika_delete']);

                //delete pozadinska slika
                if(isset($general_data['pozadinska_slika_delete']) && isset($general_data['pozadinska_slika_delete']) == 'on'){
                    $pozadinska_slika = AdminGroups::find($inputs['grupa_pr_id'], 'pozadinska_slika');
                    if(isset($pozadinska_slika)){
                        if(file_exists(trim($pozadinska_slika))==true){
                            unlink(trim($pozadinska_slika));
                        }
                    }
                    $general_data['pozadinska_slika'] = null;               
                }
                unset($general_data['pozadinska_slika_delete']);

                DB::table('grupa_pr')->where('grupa_pr_id',$inputs['grupa_pr_id'])->update($general_data);
                if($general_data['web_b2c_prikazi']==0){
                   DB::table('grupa_pr')->where('parrent_grupa_pr_id',$inputs['grupa_pr_id'])->update(array('web_b2c_prikazi'=>0));
                   $parent_id= DB::table('grupa_pr')->where('parrent_grupa_pr_id',$inputs['grupa_pr_id'])->pluck('grupa_pr_id');
                   DB::table('grupa_pr')->where('parrent_grupa_pr_id',$parent_id)->update(array('web_b2c_prikazi'=>0));

                }
                AdminSupport::saveLog('SIFARNICI_GRUPA_IZMENI', array($inputs['grupa_pr_id']));
            }else{
                if(isset($inputs['premestanje_karakteristika']) && $inputs['premestanje_karakteristika'] > 0){
                    DB::table('grupa_pr_naziv')->where('grupa_pr_id',$inputs['premestanje_karakteristika'])->update(array('grupa_pr_id' => $general_data['grupa_pr_id']));
                }
                DB::table('grupa_pr')->insert($general_data);
                $inputs['grupa_pr_id'] = DB::table('grupa_pr')->max('grupa_pr_id');
            AdminSupport::saveLog('SIFARNICI_GRUPA_DODAJ', array($inputs['grupa_pr_id']));
            }
            //translate to all languages
            AdminTranslator::updateTranslator($inputs['jezik_id'],AdminGroups::find($inputs['grupa_pr_id'], 'grupa'),$inputs['grupa']);

            $query = DB::table('grupa_pr_jezik')->where(array('grupa_pr_id'=>$inputs['grupa_pr_id'], 'jezik_id'=>$inputs['jezik_id']));
            $jezik_data = array('sablon_opis'=>$inputs['sablon_opis'],'title'=>$inputs['seo_title'],'description'=>$inputs['seo_description'],'keywords'=>$inputs['seo_keywords']);
            // $new_keywords = implode(", ", preg_split("/[\s]+/", $inputs['grupa']));
            // $jezik_data = array('sablon_opis'=>$inputs['sablon_opis'],'title'=>$inputs['grupa'],'description'=>$inputs['grupa'],'keywords'=>$new_keywords);
            
            if(!is_null($query->first())){
                $query->update($jezik_data);
            }else{
                if($inputs['sablon_opis'] || $inputs['seo_title'] || $inputs['seo_description'] || $inputs['seo_keywords']){
                    $jezik_data['grupa_pr_id'] = $inputs['grupa_pr_id'];
                    $jezik_data['jezik_id'] = $inputs['jezik_id'];
                    DB::table('grupa_pr_jezik')->insert($jezik_data);

                    if($inputs['jezik_id']==1){
                        foreach(DB::table('jezik')->where('aktivan',1)->where('jezik_id','!=',1)->orderBy('izabrani','desc')->get() as $jezik){
                            $translator = new TranslatorService(DB::table('jezik')->where('jezik_id',1)->pluck('kod'),$jezik->kod);
                            $jezik_data = array('grupa_pr_id' => $inputs['grupa_pr_id'], 'jezik_id' => $jezik->jezik_id, 'sablon_opis'=>$translator->translate(html_entity_decode($inputs['sablon_opis'])),'title'=>$translator->translate($inputs['seo_title']),'description'=>$translator->translate($inputs['seo_description']),'keywords'=>$translator->translate($inputs['seo_keywords']));

                            DB::table('grupa_pr_jezik')->insert($jezik_data);                 
                        }
                    }
                }
            }

            // AdminSupport::saveLog('Grupe, INSERT/EDIT grupa_pr_id -> '.$general_data['grupa_pr_id']);
            if(Options::gnrl_options(3055) == 1 && Options::gnrl_options(3056) == 0){
                $elastic = new ElasticSearchService();
                $elastic->updateGroups();
                $elastic->updateArticles();
            }
            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/grupe/'.$general_data['grupa_pr_id'].'/'.($inputs['karak_id'] ? $inputs['karak_id'] : 0).($inputs['jezik_id']==1 ? '' : '/'.$inputs['jezik_id']))->with('message',$message);
        }
    }

    function grupa_delete($grupa_pr_id)
    {   
        $childs = DB::table('grupa_pr')->where('parrent_grupa_pr_id',$grupa_pr_id)->count();
        if($childs > 0){
             return Redirect::to(AdminOptions::base_url().'admin/grupe/'.$grupa_pr_id)->with('message','Grupu je nemoguce obrisati jer sadrzi podgrupe!');
        }

        $roba = DB::table('roba')->where('grupa_pr_id',$grupa_pr_id)->count();
        if($roba > 0){
            return Redirect::to(AdminOptions::base_url().'admin/grupe/'.$grupa_pr_id)->with('message','Grupu je nemoguce obrisati jer su za nju vezani artikli!');
        }
                
        DB::statement("update dobavljac_cenovnik set grupa_pr_id = -1 where grupa_pr_id = ".strval($grupa_pr_id)."");
        $query_naziv = DB::table('grupa_pr_naziv')->select('grupa_pr_naziv_id')->where('grupa_pr_id',$grupa_pr_id)->get();
        $naziv_arr = array_map('current', $query_naziv);
        if(count($naziv_arr) > 0){
            DB::table('web_roba_karakteristike')->whereIn('grupa_pr_naziv_id',$naziv_arr)->delete();
            DB::table('grupa_pr_vrednost')->whereIn('grupa_pr_naziv_id',$naziv_arr)->delete();
            DB::table('grupa_pr_naziv')->where('grupa_pr_id',$grupa_pr_id)->delete();
        }
        DB::table('konfigurator_grupe')->where('grupa_pr_id',$grupa_pr_id)->delete();
        DB::table('roba_grupe')->where('grupa_pr_id',$grupa_pr_id)->delete();
        DB::table('partner_grupa')->where('grupa_pr_id',$grupa_pr_id)->delete();
        DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->delete();
        AdminSupport::saveLog('SIFARNICI_GRUPA_OBRISI', array($grupa_pr_id));


        if(Options::gnrl_options(3055) == 1 && Options::gnrl_options(3056) == 0){
            $elastic = new ElasticSearchService();
            $elastic->updateGroups();
            $elastic->updateArticles();
        }

        return Redirect::to(AdminOptions::base_url().'admin/grupe/0')->with('message','Uspešno ste obrisali sadržaj.');

        // $sve_grupe = array();
        // AdminCommon::allGroups($sve_grupe,$grupa_pr_id);

        // $query_naziv = DB::table('grupa_pr_naziv')->select('grupa_pr_naziv_id')->whereIn('grupa_pr_id',$sve_grupe)->get();
        // $naziv_arr = array();
        // foreach($query_naziv as $row){
        //     $naziv_arr[] = $row->grupa_pr_naziv_id;
        // }
        // if(count($naziv_arr) > 0){
        //     DB::table('web_roba_karakteristike')->whereIn('grupa_pr_naziv_id',$naziv_arr)->delete();
        //     DB::table('grupa_pr_vrednost')->whereIn('grupa_pr_naziv_id',$naziv_arr)->delete();
        //     DB::table('grupa_pr_naziv')->whereIn('grupa_pr_id',$sve_grupe)->delete();
        // }

        // $slike_query = DB::table('grupa_pr')->select('putanja_slika')->whereIn('grupa_pr_id',$sve_grupe)->get();
        // foreach($slike_query as $row){
        //     if(isset($row->putanja_slika)){
        //         if(file_exists(trim($row->putanja_slika))==true){
        //             unlink(trim($row->putanja_slika));
        //         }
        //     }
        // }

        // foreach(DB::table('roba')->select('roba_id')->whereIn('grupa_pr_id',$sve_grupe)->get() as $row){
        //     DB::table('web_roba_karakteristike')->where('roba_id',$row->roba_id)->delete();
        //     $slike_query = DB::table('web_slika')->where('roba_id',$row->roba_id)->get();
        //     if(count($slike_query)>0){
        //         foreach($slike_query as $row){
        //             if(file_exists(trim($row->putanja))==true){
        //                 unlink(trim($row->putanja));
        //             }
        //         }
        //     }
        //     DB::table('web_slika')->where('roba_id',$row->roba_id)->delete();
        //     DB::table('lager')->where('roba_id',$row->roba_id)->delete();            
        // }

        // DB::table('roba')->whereIn('grupa_pr_id',$sve_grupe)->delete();
        // DB::table('grupa_pr')->whereIn('grupa_pr_id',$sve_grupe)->delete();
    }

    public function ajaxGroups(){
        $action = Input::get('action');
        $grupa_pr_naziv_id = Input::get('grupa_pr_naziv_id');

        if ($action == 'sacuvaj-novu-karak') {
            $grupa_pr_id = Input::get('grupa_pr_id');
            $naziv = Input::get('naziv');
            
            if(is_numeric(Input::get('karak_rbr'))){
                $karak_rbr = Input::get('karak_rbr');
            }else{
                $karak_rbr = DB::select("SELECT MAX(rbr) AS max FROM grupa_pr_naziv WHERE grupa_pr_id=".$grupa_pr_id."")[0]->max + 1;
            }

            DB::table('grupa_pr_naziv')->insert(array('naziv'=>$naziv, 'grupa_pr_id'=>$grupa_pr_id,'rbr'=>$karak_rbr));
            AdminSupport::saveLog('SIFARNICI_GRUPA_KARAKTERISTIKE_DODAJ', array(DB::table('grupa_pr_naziv')->max('grupa_pr_naziv_id')));


        } elseif ($action == 'sacuvaj'){
            $naziv = Input::get('naziv');
            // $naziv = addslashes($naziv);
            $grupa_pr_naziv_id = Input::get('grupa_pr_naziv_id');
            
            DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id', $grupa_pr_naziv_id)->update(array('naziv'=>$naziv));
            
            AdminSupport::saveLog('SIFARNICI_GRUPA_NAZIV_IZMENI', array($grupa_pr_naziv_id));


        } elseif($action == 'aktivnaKarak') {

            $active = DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id', $grupa_pr_naziv_id)->pluck('active');

            if($active) {
                $state = 0;
            } else {
                $state = 1;
            }
            DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id', $grupa_pr_naziv_id)->update(array('active'=>$state));
            AdminSupport::saveLog('SIFARNICI_GRUPA_KARAKTERISTIKA_AKTIVNA_IZMENI', array($grupa_pr_naziv_id));

        } elseif ($action == 'obrisi-karak') {

            $grupa_pr_naziv_id = Input::get('grupa_pr_naziv_id');
            AdminSupport::saveLog('GRUPA_KARAKTERISTIKA_OBRISI', array($grupa_pr_naziv_id));
            DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id', $grupa_pr_naziv_id)->delete();
            DB::table('grupa_pr_vrednost')->where('grupa_pr_naziv_id', $grupa_pr_naziv_id)->delete();
            DB::table('web_roba_karakteristike')->where('grupa_pr_naziv_id', $grupa_pr_naziv_id)->delete();

            
        } elseif ($action == 'nova-vred'){
            $vrednost = Input::get('vrednost');
            $grupa_pr_naziv_id = Input::get('grupa_pr_naziv_id');
            // if(is_numeric(Input::get('vrednost_rbr'))){
            //     $vrednost_rbr = Input::get('vrednost_rbr');
            // }else{
            //     $vrednost_rbr = DB::select("SELECT MAX(rbr) AS max FROM grupa_pr_vrednost WHERE grupa_pr_naziv_id=".$grupa_pr_naziv_id."")[0]->max + 1;
            // }

            DB::table('grupa_pr_vrednost')->insert(array('naziv'=>$vrednost, 'grupa_pr_naziv_id'=>$grupa_pr_naziv_id));
            AdminSupport::saveLog('SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_DODAJ', array(DB::table('grupa_pr_vrednost')->max('grupa_pr_vrednost_id')));


        } elseif ($action == 'sacuvaj-vrednost'){
            $vrednost = Input::get('vrednost');
            $vrednost = pg_escape_string($vrednost);
            $grupa_pr_vrednost_id = Input::get('grupa_pr_vrednost_id');
           
            DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id', $grupa_pr_vrednost_id)->update(array('naziv'=>$vrednost));

            AdminSupport::saveLog('SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_IZMENI', array($grupa_pr_vrednost_id));


        } elseif($action == 'aktivnaVred') {
            $grupa_pr_vrednost_id = Input::get('grupa_pr_vrednost_id');
            $active = DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id', $grupa_pr_vrednost_id)->pluck('active');

            if($active) {
                $state = 0;
            } else {
                $state = 1;
            }
            DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id', $grupa_pr_vrednost_id)->update(array('active'=>$state));
            AdminSupport::saveLog('SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_AKTIVNA_IZMENI', array($grupa_pr_vrednost_id));

        } elseif ($action == 'obrisi-vred') {

            $grupa_pr_vrednost_id = Input::get('grupa_pr_vrednost_id');
            AdminSupport::saveLog('SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_OBRISI', array($grupa_pr_vrednost_id));
            DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id', $grupa_pr_vrednost_id)->delete();
            DB::table('web_roba_karakteristike')->where('grupa_pr_vrednost_id', $grupa_pr_vrednost_id)->delete();

        } elseif ($action == 'grupa_rbr') {
            $categoryId = Input::get('target_id');
            $parentId = Input::get('parent_id');
            $childsIds = Input::get('childs_ids') ? Input::get('childs_ids') : array();            
            DB::table('grupa_pr')->where('grupa_pr_id', $categoryId)->update(array('parrent_grupa_pr_id' => $parentId));

            if(count($childsIds) == 0){
                $childs = DB::table('grupa_pr')->select('grupa_pr_id')->where('grupa_pr_id','<>',$categoryId)->where('parrent_grupa_pr_id', $parentId)->orderBy('redni_broj','ASC')->get();
                if(count($childs) > 0){
                    $childsIds = array_map('current',$childs);
                }else{
                    $childsIds = array();
                }
                DB::table('grupa_pr')->where('grupa_pr_id', $categoryId)->update(array('redni_broj' => 1));
                foreach($childsIds as $key => $childId){
                    DB::table('grupa_pr')->where('grupa_pr_id', $childId)->update(array('redni_broj' => $key+2));
                }
            }else{
                foreach($childsIds as $key => $childId){
                    DB::table('grupa_pr')->where('grupa_pr_id', $childId)->update(array('redni_broj' => $key+1));
                }
            }
            AdminSupport::saveLog('GRUPA_REDNI_BROJ_IZMENI', array($categoryId));
        }
    }
    function position_value(){
        $moved = Input::get('moved');
        $order_arr = Input::get('order');
        $grupa_pr_vrednost_id = Input::get('grupa_pr_vrednost_id');
        
        $log_ids_arr = array();
        foreach($order_arr as $key => $val){
            DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id',$val)->update(array('rbr'=>$key));
            $log_ids_arr = $val;
        }

        AdminSupport::saveLog('SIFARNICI_GRUPA_KARAKTERISTIKA_VREDNOST_POZICIJA', array($moved));

        
    }
    function position_char(){
        $moved = Input::get('moved');
        $order_arr = Input::get('order');
        $grupa_pr_vrednost_id = Input::get('grupa_pr_vrednost_id');
        
        $log_ids_arr = array();
        foreach($order_arr as $key => $val){
            DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$val)->update(array('rbr'=>$key));
            $log_ids_arr = $val;
        }

        AdminSupport::saveLog('SIFARNICI_GRUPA_KARAKTERISTIKA_POZICIJA', array($moved));

        
    }
public static function imagesToWebp($file){

    $compression_quality = 35;
    if (!file_exists($file)) {
        return false;
    }
    $file_type = exif_imagetype($file);
    //https://www.php.net/manual/en/function.exif-imagetype.php
    //exif_imagetype($file);
    // 1    IMAGETYPE_GIF
    // 2    IMAGETYPE_JPEG
    // 3    IMAGETYPE_PNG
    // 6    IMAGETYPE_BMP
    // 15   IMAGETYPE_WBMP
    // 16   IMAGETYPE_XBM
    $output_file =  str_replace(array('.png','.jpg','.jepg','.PNG','.JPG','.JEPG'), array('','','','','',''), $file) . '.webp';
    if (file_exists($output_file)) {
        return $output_file;
    }
    if (function_exists('imagewebp')) {
        switch ($file_type) {
            case '1': //IMAGETYPE_GIF
                $image = imagecreatefromgif($file);
                break;
            case '2': //IMAGETYPE_JPEG
                $image = imagecreatefromjpeg($file);
                break;
            case '3': //IMAGETYPE_PNG
                    $image = imagecreatefrompng($file);
                    imagepalettetotruecolor($image);
                    imagealphablending($image, true);
                    imagesavealpha($image, true);
                    break;
            case '6': // IMAGETYPE_BMP
                $image = imagecreatefrombmp($file);
                break;
            case '15': //IMAGETYPE_Webp
               return false;
                break;
            case '16': //IMAGETYPE_XBM
                $image = imagecreatefromxbm($file);
                break;
            default:
                return false;
        }
        // Save the image
        $result = imagewebp($image, $output_file, $compression_quality);
       // imagedestroy($image);
        if (false === $result) {
            return false;
        }

        // Free up memory
        imagedestroy($image);
        return $output_file;
    } elseif (class_exists('Imagick')) {
        $image = new Imagick();
        $image->readImage($file);
        if ($file_type === "3") {
            $image->setImageFormat('webp');
            $image->setImageCompressionQuality($compression_quality);
            $image->setOption('webp:lossless', 'true');
        }
        $image->writeImage($output_file);
        
    }
    return false;
    }

}

