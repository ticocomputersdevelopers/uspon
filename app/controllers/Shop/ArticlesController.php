<?php 

class ArticlesController extends Controller {
	public function artikli_first(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;
		$grupa1 = Request::segment(1+$offset);

		$proizvodjac = Request::get('m');
		$karakteristike = Request::get('ch');
		$cene = Request::get('pr');

		$grupa_pr_id=Url_mod::get_grupa_pr_id(array($grupa1));
		All::check_category($grupa_pr_id);

		if($grupa_pr_id==0){
            if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
                return Redirect::to($new_link);
            }
            $data=array(
                "strana"=>'Not found',
                "org_strana"=>'Not found',
                "title"=>'Not found',
                "description"=>'Not found',
                "keywords"=>'Not found'
            );
            $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
            return Response::make($content, 404);
		}
              
		    $level2=array();
		    Groups::allGroups($level2,$grupa_pr_id);

		    $select="SELECT DISTINCT r.roba_id";
		    $roba=" FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join').Product::checkCharacteristics('join')."";
			$where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkNaStanju().Product::checkLager().Product::checkActiveGroup().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND ( r.grupa_pr_id IN(".implode(",",$level2).") OR rg.grupa_pr_id IN(".implode(",",$level2)."))";
			$where_init=$where;
            $proiz_array=array();
            $krak_array=array();

            if(!is_null($proizvodjac) && !is_null($karakteristike)){
				if($proizvodjac!="0" && $karakteristike!="0"){

					$krak_array=explode("-",$karakteristike);
					$proiz_array=explode("-",$proizvodjac);

					if(Options::filters_type()==0){
						$query_proz=""; 
						foreach($proiz_array as $proiz_id){
							$query_proz .= $proiz_id.",";
						}
						$where .= " AND proizvodjac_id IN (".substr($query_proz,0,-1).")";

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}

						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) >= ".$i.")";					

					}else{
						$where .= " AND proizvodjac_id = ".$proizvodjac."";
						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}
						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) = ".$i.")";					

					}		

				}

				if($proizvodjac!="0" && $karakteristike=="0"){
					$proiz_array=explode("-",$proizvodjac);
					$krak_array[]="-1";
					if(Options::filters_type()==0){
						$query_proz="";
						foreach($proiz_array as $proiz_id){
							$query_proz .= $proiz_id.",";
						}
						$where .= " AND proizvodjac_id IN (".substr($query_proz,0,-1).")";
					}else{
						$where .= " AND proizvodjac_id = ".$proizvodjac."";
					}	

				}

				if($proizvodjac=="0" && $karakteristike!="0"){
					$proiz_array[]="0";		

					$krak_array=explode("-",$karakteristike);

					if(Options::filters_type()==0){

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}

						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) >= ".$i.")";					

					}else{

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}
						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) = ".$i.")";					

					}	

				}
				
			}


			if(count(Groups::deca($grupa_pr_id)) == 0){
				$select_max = "SELECT MAX(web_cena) AS max_web_cena";
				$artikli_cena = DB::select($select_max.$roba.$where);
				$max_web_cena = $artikli_cena[0] ? $artikli_cena[0]->max_web_cena : 0;

				$select_min = "SELECT MIN(web_cena) AS min_web_cena";
				$artikli_cena = DB::select($select_min.$roba.$where);
				$min_web_cena = $artikli_cena[0] ? $artikli_cena[0]->min_web_cena : 0;
			}
			if(!is_null($cene)){
				$cene_arr = explode('-',$cene);
				if($cene_arr[1]>0 && $cene_arr[1]>=$cene_arr[0]){
					$where .= " AND web_cena BETWEEN ".$cene_arr[0]." AND ".ceil($cene_arr[1])."";
				}
			}

			$count=count(DB::select($select.$roba.$where));

	        	if(Session::has('limit')){
					$limit=Session::get('limit');
				}
				else {
					$limit=20;
				}

			    if(Input::get('page')){
			    	$pageNo = Input::get('page');
			    }else{
			    	$pageNo = 1;
			    }

			    $offset = ($pageNo-1)*$limit;
				
				if(Session::has('order')){
					if(Session::get('order')=='price_asc')
					{
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='price_desc'){
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='news'){
					$artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='name'){
					$artikli=DB::select($select.", r.naziv_web".$roba.$where." ORDER BY r.naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='rbr'){
					$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order') == 'quantity') {
						$artikli=DB::select($select.", CASE WHEN (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) is null then 0 ELSE (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) END as kol".$roba.$where." ORDER BY kol DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
				}
				else {
			    	$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
				}

		$filter_prikazi = count(Groups::deca($grupa_pr_id));
		$seo_grupa = Seo::grupa($grupa_pr_id);
		$data=array(
		"strana"=>'artikli',
		"grupa_pr_id"=>$grupa_pr_id,
		"grupa_pr_ids"=>$level2,
		"title"=>$seo_grupa->title,
		"description"=>$seo_grupa->description,
		"keywords"=>$seo_grupa->keywords,
		"sub_cats" => Articles::subGroups($grupa_pr_id),
		"articles"=>$artikli,
		"limit"=>$limit,
		"count_products"=>$count,
		"url"=>$grupa1,
		"niz_karakteristike"=>$krak_array,
		"niz_proiz"=>$proiz_array,
		"proizvodjac_ids"=>!empty($proizvodjac) ? $proizvodjac : "0",
		"kara"=>!empty($karakteristike) ? $karakteristike : "0",
		"cene"=> !is_null($cene) ? $cene : '0-0',
		"filter_prikazi" => $filter_prikazi ? 0 : 1,
		"max_web_cena" => $filter_prikazi ? 0 : $max_web_cena,
		"min_web_cena" => $filter_prikazi ? 0 : $min_web_cena,
		"baner_id" => DB::table('grupa_pr')->where('grupa_pr_id', $grupa_pr_id)->pluck('baner_id'),
		"characteristics"=>Options::filters_type() ? All::getKarakteristike($select.$roba.$where) : All::getKarakteristike($select.$roba.$where_init),
		"manufacturers"=>Options::filters_type() ? All::getProizvodjaci($select.$roba.$where) : All::getProizvodjaci($select.$roba.$where_init)
		);
		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);
	}


	public function artikli_second(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;
		$grupa1 = Request::segment(1+$offset);
		$grupa2 = Request::segment(2+$offset);

		$proizvodjac = Request::get('m');
		$karakteristike = Request::get('ch');
		$cene = Request::get('pr');

		$grupa_pr_id=Url_mod::get_grupa_pr_id(array($grupa1,$grupa2));

		All::check_category($grupa_pr_id);
		
		if($grupa_pr_id==0){
            if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
                return Redirect::to($new_link);
            }
            $data=array(
                "strana"=>'Not found',
                "org_strana"=>'Not found',
                "title"=>'Not found',
                "description"=>'Not found',
                "keywords"=>'Not found'
            );
            $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
            return Response::make($content, 404);
		}
              
		    $level2=array();
		    Groups::allGroups($level2,$grupa_pr_id);  

		    $select="SELECT DISTINCT r.roba_id";
		    $roba=" FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join').Product::checkCharacteristics('join')."";
			$where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkNaStanju().Product::checkLager().Product::checkActiveGroup().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND ( r.grupa_pr_id IN(".implode(",",$level2).") OR rg.grupa_pr_id IN(".implode(",",$level2)."))";
			$where_init=$where;
            $proiz_array=array();
            $krak_array=array();

            if(!is_null($proizvodjac) && !is_null($karakteristike)){
				if($proizvodjac!="0" && $karakteristike!="0"){

					$krak_array=explode("-",$karakteristike);
					$proiz_array=explode("-",$proizvodjac);

					if(Options::filters_type()==0){
						$query_proz="";
						foreach($proiz_array as $proiz_id){
							$query_proz .= $proiz_id.",";
						}
						$where .= " AND proizvodjac_id IN (".substr($query_proz,0,-1).")";

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}

						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) >= ".$i.")";					

					}else{
						$where .= " AND proizvodjac_id = ".$proizvodjac."";
						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}
						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) = ".$i.")";					

					}		

				}

				if($proizvodjac!="0" && $karakteristike=="0"){
					$proiz_array=explode("-",$proizvodjac);
					$krak_array[]="-1";
					if(Options::filters_type()==0){
						$query_proz="";
						foreach($proiz_array as $proiz_id){
							$query_proz .= $proiz_id.",";
						}
						$where .= " AND proizvodjac_id IN (".substr($query_proz,0,-1).")";
					}else{
						$where .= " AND proizvodjac_id = ".$proizvodjac."";
					}	

				}

				if($proizvodjac=="0" && $karakteristike!="0"){
					$proiz_array[]="0";		

					$krak_array=explode("-",$karakteristike);

					if(Options::filters_type()==0){

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}

						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) >= ".$i.")";					

					}else{

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}
						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) = ".$i.")";					

					}	

				}


			}

			if(count(Groups::deca($grupa_pr_id)) == 0){
				$select_max = "SELECT MAX(web_cena) AS max_web_cena";
				$artikli_cena = DB::select($select_max.$roba.$where);
				$max_web_cena = $artikli_cena[0] ? $artikli_cena[0]->max_web_cena : 0;

				$select_min = "SELECT MIN(web_cena) AS min_web_cena";
				$artikli_cena = DB::select($select_min.$roba.$where);
				$min_web_cena = $artikli_cena[0] ? $artikli_cena[0]->min_web_cena : 0;
			}

			if(!is_null($cene)){
				$cene_arr = explode('-',$cene);
				if($cene_arr[1]>0 && $cene_arr[1]>=$cene_arr[0]){
					$where .= " AND web_cena BETWEEN ".$cene_arr[0]." AND ".ceil($cene_arr[1])."";
				}
			}

			$count=count(DB::select($select.$roba.$where));

	        	if(Session::has('limit')){
					$limit=Session::get('limit');
				}
				else {
					$limit=20;
				}

			    if(Input::get('page')){
			    	$pageNo = Input::get('page');
			    }else{
			    	$pageNo = 1;
			    }

			    $offset = ($pageNo-1)*$limit;
				
				if(Session::has('order')){
					if(Session::get('order')=='price_asc')
					{
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='price_desc'){
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='news'){
					$artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='name'){
					$artikli=DB::select($select.", r.naziv_web".$roba.$where." ORDER BY r.naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='rbr'){
					$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order') == 'quantity') {
						$artikli=DB::select($select.", CASE WHEN (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) is null then 0 ELSE (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) END as kol".$roba.$where." ORDER BY kol DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
				}
				else {
			    	$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
				}

		$filter_prikazi = count(Groups::deca($grupa_pr_id));
		$seo_grupa = Seo::grupa($grupa_pr_id);
		$data=array(
		"strana"=>'artikli',
		"grupa_pr_id"=>$grupa_pr_id,
		"grupa_pr_ids"=>$level2,
		"title"=>$seo_grupa->title,
		"description"=>$seo_grupa->description,
		"keywords"=>$seo_grupa->keywords,
		"sub_cats" => Articles::subGroups($grupa_pr_id),
		"articles"=>$artikli,
		"limit"=>$limit,
		"count_products"=>$count,
		"url"=>$grupa1."/".$grupa2,
		"niz_karakteristike"=>$krak_array,
		"niz_proiz"=>$proiz_array,
		"proizvodjac_ids"=>!empty($proizvodjac) ? $proizvodjac : "0",
		"kara"=>!empty($karakteristike) ? $karakteristike : "0",
		"cene"=> !is_null($cene) ? $cene : '0-0',
		"filter_prikazi" => $filter_prikazi ? 0 : 1,
		"max_web_cena" => $filter_prikazi ? 0 : $max_web_cena,
		"min_web_cena" => $filter_prikazi ? 0 : $min_web_cena,
		"baner_id" => DB::table('grupa_pr')->where('grupa_pr_id', $grupa_pr_id)->pluck('baner_id'),
		"characteristics"=>Options::filters_type() ? All::getKarakteristike($select.$roba.$where) : All::getKarakteristike($select.$roba.$where_init),
		"manufacturers"=>Options::filters_type() ? All::getProizvodjaci($select.$roba.$where) : All::getProizvodjaci($select.$roba.$where_init)
		);
		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);

	}

	public function artikli_third(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;
		$grupa1 = Request::segment(1+$offset);
		$grupa2 = Request::segment(2+$offset);
		$grupa3 = Request::segment(3+$offset);

		$proizvodjac = Request::get('m');
		$karakteristike = Request::get('ch');
		$cene = Request::get('pr');

		$grupa_pr_id=Url_mod::get_grupa_pr_id(array($grupa1,$grupa2,$grupa3));
		All::check_category($grupa_pr_id);

		if($grupa_pr_id==0){
            if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
                return Redirect::to($new_link);
            }
            $data=array(
                "strana"=>'Not found',
                "org_strana"=>'Not found',
                "title"=>'Not found',
                "description"=>'Not found',
                "keywords"=>'Not found'
            );
            $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
            return Response::make($content, 404);
		}
              
		    $level2=array();
		    Groups::allGroups($level2,$grupa_pr_id);  

		    $select="SELECT DISTINCT r.roba_id";
		    $roba=" FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join').Product::checkCharacteristics('join')."";
			$where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkNaStanju().Product::checkLager().Product::checkActiveGroup().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND ( r.grupa_pr_id IN(".implode(",",$level2).") OR rg.grupa_pr_id IN(".implode(",",$level2)."))";
			$where_init=$where;
            $proiz_array=array();
            $krak_array=array();

			if(!is_null($proizvodjac) && !is_null($karakteristike)){
				if($proizvodjac!="0" && $karakteristike!="0"){

					$krak_array=explode("-",$karakteristike);
					$proiz_array=explode("-",$proizvodjac);

					if(Options::filters_type()==0){

						$where .= " AND proizvodjac_id IN (".implode(',',$proiz_array).")";

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}

						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) >= ".$i.")";
						
					}else{
						$where .= " AND proizvodjac_id = ".$proizvodjac."";
						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}
						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) = ".$i.")";					

					}		

				}

				if($proizvodjac!="0" && $karakteristike=="0"){
					$proiz_array=explode("-",$proizvodjac);
					$krak_array[]="-1";
					if(Options::filters_type()==0){
						$query_proz="";
						foreach($proiz_array as $proiz_id){
							$query_proz .= $proiz_id.",";
						}
						$where .= " AND proizvodjac_id IN (".substr($query_proz,0,-1).")";
					}else{
						$where .= " AND proizvodjac_id = ".$proizvodjac."";
					}	

				}

				if($proizvodjac=="0" && $karakteristike!="0"){
					$proiz_array[]="0";		

					$krak_array=explode("-",$karakteristike);

					if(Options::filters_type()==0){

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}

						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) >= ".$i.")";					

					}else{

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}
						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) = ".$i.")";					

					}	

				}

			}

			if(count(Groups::deca($grupa_pr_id)) == 0){
				$select_max = "SELECT MAX(web_cena) AS max_web_cena";
				$artikli_cena = DB::select($select_max.$roba.$where);
				$max_web_cena = $artikli_cena[0] ? $artikli_cena[0]->max_web_cena : 0;

				$select_min = "SELECT MIN(web_cena) AS min_web_cena";
				$artikli_cena = DB::select($select_min.$roba.$where);
				$min_web_cena = $artikli_cena[0] ? $artikli_cena[0]->min_web_cena : 0;
			}

			if(!is_null($cene)){
				$cene_arr = explode('-',$cene);
				if($cene_arr[1]>0 && $cene_arr[1]>=$cene_arr[0]){
					$where .= " AND web_cena BETWEEN ".$cene_arr[0]." AND ".ceil($cene_arr[1])."";
				}
			}

			$count=count(DB::select($select.$roba.$where));

	        	if(Session::has('limit')){
					$limit=Session::get('limit');
				}
				else {
					$limit=20;
				}

			    if(Input::get('page')){
			    	$pageNo = Input::get('page');
			    }else{
			    	$pageNo = 1;
			    }

			    $offset = ($pageNo-1)*$limit;
				
				if(Session::has('order')){
					if(Session::get('order')=='price_asc')
					{
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='price_desc'){
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='news'){
					$artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='name'){
					$artikli=DB::select($select.", r.naziv_web".$roba.$where." ORDER BY r.naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='rbr'){
					$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order') == 'quantity') {
						$artikli=DB::select($select.", CASE WHEN (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) is null then 0 ELSE (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) END as kol".$roba.$where." ORDER BY kol DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
				}
				else {
			    	$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
				}

		$filter_prikazi = count(Groups::deca($grupa_pr_id));
		$seo_grupa = Seo::grupa($grupa_pr_id);
		$data=array(
		"strana"=>'artikli',
		"grupa_pr_id"=>$grupa_pr_id,
		"grupa_pr_ids"=>$level2,
		"title"=>$seo_grupa->title,
		"description"=>$seo_grupa->description,
		"keywords"=>$seo_grupa->keywords,
		"sub_cats" => Articles::subGroups($grupa_pr_id),
		"articles"=>$artikli,
		"limit"=>$limit,
		"count_products"=>$count,
		"url"=>$grupa1."/".$grupa2."/".$grupa3,
		"niz_karakteristike"=>$krak_array,
		"niz_proiz"=>$proiz_array,
		"proizvodjac_ids"=>!empty($proizvodjac) ? $proizvodjac : "0",
		"kara"=>!empty($karakteristike) ? $karakteristike : "0",
		"cene"=> !is_null($cene) ? $cene : '0-0',
		"filter_prikazi" => $filter_prikazi ? 0 : 1,
		"max_web_cena" => $filter_prikazi ? 0 : $max_web_cena,
		"min_web_cena" => $filter_prikazi ? 0 : $min_web_cena,
		"baner_id" => DB::table('grupa_pr')->where('grupa_pr_id', $grupa_pr_id)->pluck('baner_id'),
		"characteristics"=>Options::filters_type() ? All::getKarakteristike($select.$roba.$where) : All::getKarakteristike($select.$roba.$where_init),
		"manufacturers"=>Options::filters_type() ? All::getProizvodjaci($select.$roba.$where) : All::getProizvodjaci($select.$roba.$where_init)
		);
		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);
	}


	public function artikli_forth(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;
		$grupa1 = Request::segment(1+$offset);
		$grupa2 = Request::segment(2+$offset);
		$grupa3 = Request::segment(3+$offset);
		$grupa4 = Request::segment(4+$offset);

		$proizvodjac = Request::get('m');
		$karakteristike = Request::get('ch');
		$cene = Request::get('pr');

		$grupa_pr_id=Url_mod::get_grupa_pr_id(array($grupa1,$grupa2,$grupa3,$grupa4));
		All::check_category($grupa_pr_id);

		if($grupa_pr_id==0){
            if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
                return Redirect::to($new_link);
            }
            $data=array(
                "strana"=>'Not found',
                "org_strana"=>'Not found',
                "title"=>'Not found',
                "description"=>'Not found',
                "keywords"=>'Not found'
            );
            $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
            return Response::make($content, 404);
		}
              
		    $level2=array();
		    Groups::allGroups($level2,$grupa_pr_id);  

		    $select="SELECT DISTINCT r.roba_id";
		    $roba=" FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join').Product::checkCharacteristics('join')."";
			$where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkNaStanju().Product::checkLager().Product::checkActiveGroup().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND ( r.grupa_pr_id IN(".implode(",",$level2).") OR rg.grupa_pr_id IN(".implode(",",$level2)."))";
			$where_init=$where;
            $proiz_array=array();
            $krak_array=array();

            if(!is_null($proizvodjac) && !is_null($karakteristike)){
				if($proizvodjac!="0" && $karakteristike!="0"){

					$krak_array=explode("-",$karakteristike);
					$proiz_array=explode("-",$proizvodjac);

					if(Options::filters_type()==0){
						$query_proz="";
						foreach($proiz_array as $proiz_id){
							$query_proz .= $proiz_id.",";
						}
						$where .= " AND proizvodjac_id IN (".substr($query_proz,0,-1).")";

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}

						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) >= ".$i.")";					

					}else{
						$where .= " AND proizvodjac_id = ".$proizvodjac."";
						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}
						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) = ".$i.")";					

					}		

				}

				if($proizvodjac!="0" && $karakteristike=="0"){
					$proiz_array=explode("-",$proizvodjac);
					$krak_array[]="-1";
					if(Options::filters_type()==0){
						$query_proz="";
						foreach($proiz_array as $proiz_id){
							$query_proz .= $proiz_id.",";
						}
						$where .= " AND proizvodjac_id IN (".substr($query_proz,0,-1).")";
					}else{
						$where .= " AND proizvodjac_id = ".$proizvodjac."";
					}	

				}

				if($proizvodjac=="0" && $karakteristike!="0"){
					$proiz_array[]="0";		

					$krak_array=explode("-",$karakteristike);

					if(Options::filters_type()==0){

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}

						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) >= ".$i.")";					

					}else{

						$query_karak="";
						foreach($krak_array as $row){
							$query_karak .= " grupa_pr_vrednost_id = ".$row." OR";
						}
						$i = count(DB::select("SELECT DISTINCT (select naziv from grupa_pr_naziv where grupa_pr_naziv_id=gpv.grupa_pr_naziv_id) FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$krak_array).")"));

						$where .= " AND r.roba_id IN ( SELECT roba_id FROM web_roba_karakteristike WHERE ".substr($query_karak, 0, -3)." GROUP BY roba_id HAVING COUNT(roba_id) = ".$i.")";					

					}	

				}

			}

			if(count(Groups::deca($grupa_pr_id)) == 0){
				$select_max = "SELECT MAX(web_cena) AS max_web_cena";
				$artikli_cena = DB::select($select_max.$roba.$where);
				$max_web_cena = $artikli_cena[0] ? $artikli_cena[0]->max_web_cena : 0;

				$select_min = "SELECT MIN(web_cena) AS min_web_cena";
				$artikli_cena = DB::select($select_min.$roba.$where);
				$min_web_cena = $artikli_cena[0] ? $artikli_cena[0]->min_web_cena : 0;
			}

			if(!is_null($cene)){
				$cene_arr = explode('-',$cene);
				if($cene_arr[1]>0 && $cene_arr[1]>=$cene_arr[0]){
					$where .= " AND web_cena BETWEEN ".$cene_arr[0]." AND ".ceil($cene_arr[1])."";
				}
			}

			$count=count(DB::select($select.$roba.$where));

	        	if(Session::has('limit')){
					$limit=Session::get('limit');
				}
				else {
					$limit=20;
				}

			    if(Input::get('page')){
			    	$pageNo = Input::get('page');
			    }else{
			    	$pageNo = 1;
			    }

			    $offset = ($pageNo-1)*$limit;
				
				if(Session::has('order')){
					if(Session::get('order')=='price_asc')
					{
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='price_desc'){
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='news'){
					$artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='name'){
					$artikli=DB::select($select.", r.naziv_web".$roba.$where." ORDER BY r.naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='rbr'){
					$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order') == 'quantity') {
						$artikli=DB::select($select.", CASE WHEN (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) is null then 0 ELSE (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) END as kol".$roba.$where." ORDER BY kol DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
				}
				else {
			    	$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
				}

		$filter_prikazi = count(Groups::deca($grupa_pr_id));
		$seo_grupa = Seo::grupa($grupa_pr_id);
		$data=array(
		"strana"=>'artikli',
		"grupa_pr_id"=>$grupa_pr_id,
		"grupa_pr_ids"=>$level2,
		"title"=>$seo_grupa->title,
		"description"=>$seo_grupa->description,
		"keywords"=>$seo_grupa->keywords,
		"sub_cats" => Articles::subGroups($grupa_pr_id),
		"articles"=>$artikli,
		"limit"=>$limit,
		"count_products"=>$count,
		"url"=>$grupa1."/".$grupa2."/".$grupa3."/".$grupa4,
		"niz_karakteristike"=>$krak_array,
		"niz_proiz"=>$proiz_array,
		"proizvodjac_ids"=>!empty($proizvodjac) ? $proizvodjac : "0",
		"kara"=>!empty($karakteristike) ? $karakteristike : "0",
		"cene"=> !is_null($cene) ? $cene : '0-0',
		"filter_prikazi" => $filter_prikazi ? 0 : 1,
		"max_web_cena" => $filter_prikazi ? 0 : $max_web_cena,
		"min_web_cena" => $filter_prikazi ? 0 : $min_web_cena,
		"baner_id" => DB::table('grupa_pr')->where('grupa_pr_id', $grupa_pr_id)->pluck('baner_id'),
		"characteristics"=>Options::filters_type() ? All::getKarakteristike($select.$roba.$where) : All::getKarakteristike($select.$roba.$where_init),
		"manufacturers"=>Options::filters_type() ? All::getProizvodjaci($select.$roba.$where) : All::getProizvodjaci($select.$roba.$where_init)
		);
		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);
	}

	public function artikli_all(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;

		$grupa_pr_id=0;
		All::check_category($grupa_pr_id);
              
		    $level2=array();
		    Groups::allGroups($level2,$grupa_pr_id);

		    $select="SELECT DISTINCT r.roba_id";
		    $roba=" FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkLager('join').Product::checkActiveGroup('join').Product::checkImage('join').Product::checkCharacteristics('join')."";
			$where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkNaStanju().Product::checkLager().Product::checkActiveGroup().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND ( r.grupa_pr_id IN(".implode(",",$level2).") OR rg.grupa_pr_id IN(".implode(",",$level2)."))";
			$where_init=$where;;


			$count=count(DB::select($select.$roba.$where));

	        	if(Session::has('limit')){
					$limit=Session::get('limit');
				}
				else {
					$limit=20;
				}

			    if(Input::get('page')){
			    	$pageNo = Input::get('page');
			    }else{
			    	$pageNo = 1;
			    }

			    $offset = ($pageNo-1)*$limit;
				
				if(Session::has('order')){
					if(Session::get('order')=='price_asc')
					{
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='price_desc'){
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='news'){
					$artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='name'){
					$artikli=DB::select($select.", r.naziv_web".$roba.$where." ORDER BY r.naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='rbr'){
					$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order') == 'quantity') {
						$artikli=DB::select($select.", CASE WHEN (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) is null then 0 ELSE (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) END as kol".$roba.$where." ORDER BY kol DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
				}
				else {
			    	$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
				}

		$filter_prikazi = count(Groups::deca($grupa_pr_id));
		$seo_grupa = Seo::grupa($grupa_pr_id);
		$data=array(
		"strana"=>'artikli',
		"grupa_pr_id"=>0,
		"grupa_pr_ids"=>$level2,
		"title"=> "Svi artikli | ".Options::company_name(),
		"description"=>"Svi artikli",
		"keywords"=>"svi, artikli",
		"sub_cats" => Articles::subGroups($grupa_pr_id),
		"articles"=>$artikli,
		"limit"=>$limit,
		"count_products"=>$count,
		"url"=>"",
		"filter_prikazi" => 0
		);
		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);
	}

	public function limit()
	{
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;
		$limit = Request::segment(2+$offset);
		
		Session::put('limit',$limit);
		$pos = strpos(URL::previous(),'?');
		if($pos==0){
			return Redirect::back();
		}else{
			return Redirect::to(substr(URL::previous(),0,$pos));
		}
	}
	public function sortiranje()
	{
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;

		$sortiranje = Request::segment(2+$offset);
		   Session::put('order',$sortiranje);
		return Redirect::back();
	}
	public function valuta()
	{
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;

		$valuta = Request::segment(2+$offset);
		   Session::put('valuta',$valuta);
		return Redirect::back();
	}
	public function prikaz(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;
		
		$prikaz = Request::segment(2+$offset);

			if($prikaz=='list'){
				   Session::put('list',$prikaz);
			}
			else {
				Session::forget('list');
			}
	
		return Redirect::back();
	
	}
	
	


}