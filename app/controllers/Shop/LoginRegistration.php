<?php
use Service\Drip;

class LoginRegistration extends Controller{

    function login(){
        $lang = Language::multi() ? Request::segment(1) : null;

        if(!Options::user_registration()==1){
            return Redirect::to(Options::base_url());
        }
        return View::make('shop/pages/login');
    }

    function user_login(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $email=Input::get('email_login');
        $lozinka=Input::get('lozinka_login');
        $lozinka=base64_encode($lozinka);
        $validator = Validator::make(array('email_login'=>$email,'lozinka_login'=>$lozinka),
        array(
            'email_login' => 'required|email',
            'lozinka_login' => 'required|regex:'.Support::regex().'|between:3,50|exists:web_kupac,lozinka,email,'.$email.''
        ),
        Language::validator_messages()); 

        $success=false;
        $errors=array();

        if($validator->fails()){
            $errors=array(
                'email_error'=> $validator->errors()->first('email_login',':message'),
                'lozinka_error' =>$validator->errors()->first('lozinka_login',':message')
            );
        }
        else {
            if(Options::user_cnfirm_registration()==1){
                if(DB::table('web_kupac')->where(array('email'=>$email,'lozinka'=>$lozinka))->pluck('flag_potvrda') == 0){
                    return Redirect::to(Options::base_url().'prijava')->withInput()->with('confirm',true);
                }
            }

           $web_kupac_id = DB::table('web_kupac')->where(array('email'=>$email,'lozinka'=>$lozinka))->pluck('web_kupac_id');
           Session::put('b2c_kupac',$web_kupac_id);
           Session::put('ticketing_user',$web_kupac_id);
            Cart::korpa_id();
            $success=true;
         }  
            return Response::json(array(
            'success'=> $success,
            'errors'=> $errors
        )); 
    }

    function user_login_post(){
        $lang = Language::multi() ? Request::segment(1) : null;

       $email=Input::get('email_login');
        $lozinka=Input::get('lozinka_login');
        $lozinka=base64_encode($lozinka);
        $validator = Validator::make(array('email_login'=>$email,'lozinka_login'=>$lozinka),
        array(
            'email_login' => 'required|email',
            'lozinka_login' => 'required|regex:'.Support::regex().'|between:3,50|exists:web_kupac,lozinka,email,'.$email.''
        ),
        Language::validator_messages());

       if($validator->fails()){
            return Redirect::to(Options::base_url().'prijava')->withInput()->withErrors($validator->messages());
        }
        else {
            if(Options::user_cnfirm_registration()==1){
                if(DB::table('web_kupac')->where(array('email'=>$email,'lozinka'=>$lozinka))->pluck('flag_potvrda') == 0){
                    return Redirect::to(Options::base_url().'prijava')->withInput()->with('confirm',true);
                }
            }

           $web_kupac_id = DB::table('web_kupac')->where(array('email'=>$email,'lozinka'=>$lozinka))->pluck('web_kupac_id');
           Session::put('b2c_kupac',$web_kupac_id);
           Session::put('ticketing_user',$web_kupac_id);

            return Redirect::to(Options::base_url())->with('login_success',Language::trans('Uspešno ste se ulogovali.'));
        }  
    }

    function user_registracija(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $data=Input::get();

       $validator_arr = array(
            'flag_vrsta_kupca' => 'required|in:0,1',
            'email' => 'required|email|unique:web_kupac,email,NULL,id,status_registracije,1',
            'lozinka' => 'required|regex:'.Support::regex().'|between:3,20',
            'telefon' => 'required|regex:'.Support::regex().'|between:3,15',
            'mesto' => 'required|regex:'.Support::regex().'|between:2,50',
            'adresa' => 'required|regex:'.Support::regex().'|between:3,50'
            );
        $translate_mess = Language::validator_messages();
        if($data['flag_vrsta_kupca'] == 0){
            unset($data['naziv']);
            unset($data['pib']);
            unset($data['maticni_br']);
            $validator_arr['ime'] = 'required|regex:'.Support::regex().'|between:3,20';
            $validator_arr['prezime'] = 'required|regex:'.Support::regex().'|between:3,20';
        }
        if($data['flag_vrsta_kupca'] == 1){
            unset($data['ime']);
            unset($data['prezime']);
            $validator_arr['naziv'] = 'required|regex:'.Support::regex().'|between:3,200';
            $validator_arr['pib'] = 'required|digits_between:9,9|numeric';
            $validator_arr['maticni_br'] = 'required|numeric';
            $translate_mess['digits_between'] = 'Broj cifara mora biti 9!';
            $translate_mess['numeric'] = 'Polje sme da sadrzi samo brojeve!';
        }

        if(in_array(DB::table('prodavnica_stil')->where('izabrana',1)->pluck('prodavnica_tema_id'),array(2))){
            $redirect = $data['flag_vrsta_kupca'] == 0 ? 'fizicko_lice' : 'pravno_lice';
        }elseif(in_array(DB::table('prodavnica_stil')->where('izabrana',1)->pluck('prodavnica_tema_id'),array(5,6,7,8,10,12))){
            $redirect = 'registracija';
        }else{
            $redirect = 'prijava';
        }

       $validator = Validator::make($data,$validator_arr,$translate_mess);
        if($validator->fails() || !Captcha::check()){
            if(!Captcha::check()){
                $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
            }
            return Redirect::to(Options::base_url().Url_mod::slug_trans($redirect))->withInput()->withErrors($validator->messages());
        }
        else {
            unset($data['captcha-string']);
            $data['kod'] = All::userCodeGenerate();
            $data['lozinka'] = base64_encode($data['lozinka']);
            $data['status_registracije'] = 1;
            $data['datum_kreiranja'] = date('Y-m-d H:i:s');
            if(Options::user_cnfirm_registration()==1){
                $data['flag_potvrda'] = 0;
            }else{
                $data['flag_potvrda'] = 1;
            }

            DB::table('web_kupac')->insert($data);
            $user_id = DB::select("SELECT currval('web_kupac_web_kupac_id_seq')")[0]->currval;

           if(Options::user_cnfirm_registration()==1){
                WebKupac::send_user_request($user_id);
                return Redirect::to(Options::base_url().Url_mod::slug_trans($redirect))->withInput()->with('message',true);
            }else{
                Session::put('b2c_kupac', $user_id);

                if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1){
                    $drip = new Drip();
                    if(is_null($drip->getSubscriber($data['email']))){
                        $drip->addOrUpdateSubscriber(DB::table('web_kupac')->where('web_kupac_id',$user_id)->first());
                    }
                }
                return Redirect::to(Options::base_url())->with('registration_success',Language::trans('Uspešno ste se registrovali.'));
            }
        }

   }

    function  logout(){
        $lang = Language::multi() ? Request::segment(1) : null;

        Session::forget('b2c_kupac');
        Session::forget('ticketing_user');
        return Redirect::to(Options::base_url())->with('loggout_succes',Language::trans('Uspešno ste se odjavili.'));
    }

    function registracija(){
        $lang = Language::multi() ? Request::segment(1) : null;

        if(!Options::user_registration()==1){
            return Redirect::to(Options::base_url());
        }
        return View::make('shop/pages/registration');
    }

    public function confirm_user(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $kod = Request::segment(2+$offset);
        $web_kupac_id = Request::segment(3+$offset);

        $query=DB::table('web_kupac')->where(array('web_kupac_id'=>$web_kupac_id,'kod'=>$kod));
        if($query->count() == 1){
            if($query->first()->flag_potvrda != 1){         
                DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->update(array('flag_potvrda'=>1,'status_registracije'=>1));

                if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1){
                    $drip = new Drip();
                    $kupac = DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->first();
                    if(is_null($drip->getSubscriber($kupac->email))){
                        $drip->addOrUpdateSubscriber($kupac);
                    }
                }

                $mesaage="Registracija je uspešno izvršena.";
            }else{
                $mesaage="Već ste potvrdili registraciju!";
            }
            Session::put('b2c_kupac',$web_kupac_id);

        }
        else {
            $mesaage="Registracija nije uspela.";
        }
        
        return Redirect::to(Options::base_url())->with('confirm_registration_message',$mesaage);
        
    }

   public function forget_password(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $email=Input::get('email_fg');
        $email=addslashes($email);

        $validator = Validator::make(array('email_login'=>$email),
        array(
            'email_login' => 'required|email|exists:web_kupac,email,status_registracije,1'
        ),
        array(
            'required' => 'Niste popunili polje!',
            'email' => 'Uneli ste neispravnu e-mail adresu!',
            'exists' => 'Uneli ste pogrešnu e-mail adresu!',
        ));
        if($validator->fails()){
            return Redirect::to(Options::base_url().'prijava')->withInput()->withErrors($validator->messages());
        }
        else{
            $web_kupac_id=DB::table('web_kupac')->where('email',$email)->where('status_registracije',1)->pluck('web_kupac_id');
            $password=All::user_password();
            $data=array('lozinka'=>base64_encode($password));
            DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->update($data);
            $body="Vi ili neko sa Vašom e-mail adresom ".$email." je uputio zahtev za promenu lozinke sa <a href='".Options::domain()."'>".Options::domain()."</a>.Vaša nova lozinka je:<br />".$password;
            $subject="Promena lozinke";
            WebKupac::send_email_to_client($body,$email,$subject);

            return Redirect::to(Options::base_url().'prijava')->withInput()->with('message',true);
        }
    }

   public function forget_password_ajax(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $email=Input::get('email_fg');
        $email=addslashes($email);

        $validator = Validator::make(array('email_login'=>$email),
        array(
            'email_login' => 'required|email|exists:web_kupac,email,status_registracije,1'
        ), Language::validator_messages());
        
        if($validator->fails()){
            $email_fg = $validator->errors()->first('email_login',':message');
            return Response::json(['success' => false, 'email_fg' => $email_fg]);
        }
        else{
            $web_kupac_id=DB::table('web_kupac')->where('email',$email)->where('status_registracije',1)->pluck('web_kupac_id');
            $password=All::user_password();
            $data=array('lozinka'=>base64_encode($password));
            DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->update($data);
            $body="Vi ili neko sa Vašom e-mail adresom ".$email." je uputio zahtev za promenu lozinke sa <a href='".Options::domain()."'>".Options::domain()."</a>.Vaša nova lozinka je:<br />".$password;
            $subject="Promena lozinke";
            WebKupac::send_email_to_client($body,$email,$subject);

            return Response::json(['success' => true]);
        }
    }

    public function korisnik(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $username = Request::segment(2+$offset);

        $data=array(
            "strana"=>'user',
            "org_strana"=>'user',
            "title"=>Language::trans("Korisnički panel"),
            "description"=>"",
            "keywords"=>"",
            "web_kupac"=>DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'))->first(),
            'poll_exists' => !is_null(DB::table('anketa')->where(['narudzbina_izabran'=>1,'flag_aktivan'=>1])->first())
        );
        return View::make('shop/themes/'.Support::theme_path().'pages/user_panel',$data);
        
    }
    public function lista_zelja(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $username = Request::segment(2+$offset);

        $data=array(
            "strana"=>'user',
            "org_strana"=>'user',
            "title"=>Language::trans("Lista želja"),
            "description"=>"",
            "keywords"=>"",
            "web_kupac"=>DB::table('web_kupac')->where('web_kupac_id',Session::get('b2c_kupac'))->first()
        );
        return View::make('shop/themes/'.Support::theme_path().'pages/wish_list',$data);
        
    }


    public function korisnik_edit(){
        $lang = Language::multi() ? Request::segment(1) : null;
        
        $data=Input::get();
  
        $old_data = DB::table('web_kupac')->where('web_kupac_id',$data['web_kupac_id'])->first();
        $validator_arr = array(
            'flag_vrsta_kupca' => 'required|in:0,1',
            'email' => 'required|email|unique:web_kupac,email,'.$data['web_kupac_id'].',web_kupac_id,status_registracije,1',
            'lozinka' => 'required|regex:'.Support::regex().'|between:3,20',
            'telefon' => 'required|regex:'.Support::regex().'|between:3,15',
            'mesto' => 'required|regex:'.Support::regex().'|between:2,50',
            'adresa' => 'required|regex:'.Support::regex().'|between:3,50'
            );
        $translate_mess = Language::validator_messages();

        if($data['flag_vrsta_kupca'] == 0){
            $naziv = $old_data->ime.' '.$old_data->prezime;
            $new_naziv = $data['ime'].' '.$data['prezime'];
            $validator_arr['ime'] = 'required|regex:'.Support::regex().'|between:3,20';
            $validator_arr['prezime'] = 'required|regex:'.Support::regex().'|between:3,20';
        }
        if($data['flag_vrsta_kupca'] == 1){
            $naziv = $old_data->naziv;
            $new_naziv = $data['naziv'];
            $validator_arr['naziv'] = 'required|regex:'.Support::regex().'|between:3,20';
            $validator_arr['pib'] = 'required|digits_between:9,9|numeric';
            $translate_mess['digits_between'] = 'Broj cifara mora biti 9!';
            $translate_mess['numeric'] = 'Polje sme da sadrzi samo brojeve!';
        }


        $validator = Validator::make($data,$validator_arr,$translate_mess);
        if($validator->fails()){
            return Redirect::to(Options::base_url().''.Url_mod::slug_trans('korisnik').'/'.Url_mod::slug_trans($naziv))->withInput()->withErrors($validator->messages());
        }
        else {

            $data['lozinka'] = base64_encode($data['lozinka']);
            $data['status_registracije'] = 1;
            $data['flag_potvrda'] = 1;

            unset($data['web_kupac_id']);

            DB::table('web_kupac')->where('web_kupac_id',$old_data->web_kupac_id)->update($data);

            if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1){
                $drip = new Drip();
                $drip->addOrUpdateSubscriber(DB::table('web_kupac')->where('web_kupac_id',$old_data->web_kupac_id)->first());
            }

            return Redirect::to(Options::base_url().''.Url_mod::slug_trans('korisnik').'/'.Url_mod::slug_trans($new_naziv))->with('message',Language::trans('Vaši podaci su uspešno sačuvani.'));
        }       
    }
}


