<?php 
use Service\Mailer;

class MainController extends Controller {
    public function index(){
        if(Options::gnrl_options(3061) == 1 && !Session::has('b2c_admin'.Options::server())){
            return View::make('shop/site-unactive');
        }
        $custom_css = DB::table('custom_css')->where('custom_css_id',1)->pluck('flag_aktivan');

        $strana=All::get_page_start();
        $seo = Seo::page($strana);
        $data=array(
            "strana"=>$strana,
            "org_strana"=>$strana,
            "title"=>$seo->title,
            "description"=>$seo->description,
            "keywords"=>$seo->keywords
        );
        
        $web_b2c_seo_id = Seo::get_page_id($strana);
        $stranica=DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id))->first();
        if($stranica->grupa_pr_id > 0){
            $grupaSlug = Url_mod::slug_trans(DB::table('grupa_pr')->where('grupa_pr_id',$stranica->grupa_pr_id)->pluck('grupa'));
            return Redirect::to(Options::base_url().$grupaSlug);
        }elseif($stranica->tip_artikla_id != -1){
            $redirect = Options::base_url();
            if($stranica->tip_artikla_id == 0){
                $redirect .= Url_mod::slug_trans('akcija');
            }else{
                $redirect .= Url_mod::slug_trans('tip').'/'.Url_mod::slug_trans(Support::tip_naziv($stranica->tip_artikla_id));
            }
            return Redirect::to($redirect);
        }elseif(!is_null($stranica->redirect_web_b2c_seo_id)){
            $stranica=DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$stranica->redirect_web_b2c_seo_id))->first();
            return Redirect::to(Options::base_url().Url_mod::page_slug($stranica->naziv_stranice)->slug);
        }

        $data['sekcije'] = DB::table('stranica_sekcija')->select('sekcija_stranice.*','sekcija_stranice_tip.naziv as tip_naziv')->where('web_b2c_seo_id',$web_b2c_seo_id)->join('sekcija_stranice','sekcija_stranice.sekcija_stranice_id','=','stranica_sekcija.sekcija_stranice_id')->join('sekcija_stranice_tip','sekcija_stranice_tip.sekcija_stranice_tip_id','=','sekcija_stranice.sekcija_stranice_tip_id')->where('sekcija_stranice.flag_aktivan', 1)->orderBy('stranica_sekcija.rbr','asc')->get();

        $data['anketa_id'] = !is_null($stranica->anketa_id) ? $stranica->anketa_id : 0;
        $data['web_b2c_narudzbina_id'] = 0;
        $data['custom'] = 1;
        
        return View::make('shop/themes/'.Support::theme_path().'pages/content',$data);  
    }
    
	public function lang(){
        $lang = Language::multi() ? Request::segment(1) : null;
		return Redirect::to('/');
			
	}
	public function page(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $orgStrana = Request::segment(1+$offset);

        $strana = Url_mod::slug_convert_page($orgStrana);

        if($strana==""){
            $strana=All::get_page_start();
        }
        
        if(Seo::get_page_id($strana) != 0){
            $seo = Seo::page($strana);
            $data=array(
                "strana"=>$strana,
                "org_strana" => $orgStrana,
                "naziv"=>str_replace(' - '.Options::company_name(),'',$seo->title),
                "title"=>$seo->title,
                "description"=>$seo->description,
                "keywords"=>$seo->keywords
            );
        }else{

            if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
                return Redirect::to($new_link);
            }

            return App::make('ArticlesController')->artikli_first(array('grupa1' => Request::segment(1+$offset)));
            // $data=array(
            //     "strana"=>'Not found',
            //     "title"=>'Not found',
            //     "description"=>'Not found',
            //     "keywords"=>'Not found'
            // );
            // $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
            // return Response::make($content, 404);
        }

        $web_b2c_seo_id = Seo::get_page_id($strana);
        $stranica=DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$web_b2c_seo_id))->first();

        if($strana == 'kontakt'){
            $path = 'shop/themes/'.Support::theme_path().'pages/contact';
        }elseif($strana== 'korpa'){
            $articlesDetails = [];
            foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->get() as $stavka){
                $articlesDetails[] = Product::gtArticleDetails($stavka->roba_id,$stavka->kolicina);
            }
            $data['articles_details'] = $articlesDetails;
            $path = 'shop/themes/'.Support::theme_path().'pages/cart_content';
        }elseif($strana=='konfigurator' AND Options::web_options(121)){
            $konfigurator = DB::table('konfigurator')->where(array('dozvoljen'=>1))->first();
            if(!is_null($konfigurator) && Options::web_options(121)==1){
            	return Redirect::to(Options::base_url().Url_mod::slug_trans('konfigurator').'/'.$konfigurator->konfigurator_id);
            }else{
                return Redirect::to(Options::base_url());
            }
        }elseif($strana=='prijava'){
            if(Session::has('b2c_kupac') || !Options::user_registration()==1 || in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(2,5,9,10))){
                return Redirect::to(Options::base_url());
            }
            $path = 'shop/themes/'.Support::theme_path().'pages/login';
        }elseif($strana=='registracija'){
            if(!Options::user_registration()==1 || in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(9))){
                return Redirect::to(Options::base_url());
            }
            $path = 'shop/themes/'.Support::theme_path().'pages/registration';
        }elseif($strana=='pravno-lice'){
            if(!Options::user_registration()==1 || in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(6,8,9))){
                return Redirect::to(Options::base_url());
            }
            $path = 'shop/themes/'.Support::theme_path().'pages/company';
        }elseif($strana=='fizicko-lice'){
            if(!Options::user_registration()==1 || in_array(DB::table('prodavnica_stil')->where('izabrana',1)->first()->prodavnica_tema_id, array(6,8,9))){
                return Redirect::to(Options::base_url());
            }
            $path = 'shop/themes/'.Support::theme_path().'pages/private_user';
        }
        elseif($strana=='sve-kategorije'){
            $path = 'shop/themes/'.Support::theme_path().'pages/all_categories';
        }
        elseif($strana=='katalozi'){
            $path = 'shop/themes/'.Support::theme_path().'pages/catalogs';
        }else{
            if($stranica->grupa_pr_id > 0){
                $grupaSlug = Url_mod::slug_trans(DB::table('grupa_pr')->where('grupa_pr_id',$stranica->grupa_pr_id)->pluck('grupa'));
                return Redirect::to(Options::base_url().$grupaSlug);
            }elseif($stranica->tip_artikla_id != -1){
                $redirect = Options::base_url();
                if($stranica->tip_artikla_id == 0){
                    $redirect .= Url_mod::slug_trans('akcija');
                }else{
                    $redirect .= Url_mod::slug_trans('tip').'/'.Url_mod::slug_trans(Support::tip_naziv($stranica->tip_artikla_id));
                }
                return Redirect::to($redirect);
            }elseif(!is_null($stranica->redirect_web_b2c_seo_id)){
                $stranica=DB::table('web_b2c_seo')->where(array('web_b2c_seo_id'=>$stranica->redirect_web_b2c_seo_id))->first();
                return Redirect::to(Options::base_url().Url_mod::page_slug($stranica->naziv_stranice)->slug);
            }

            // $data['content'] = '';
            // $stranica_jezik=DB::table('web_b2c_seo_jezik')->where(array('web_b2c_seo_id'=>Seo::get_page_id($strana), 'jezik_id'=>Language::lang_id()))->first();
            // if(!is_null($stranica_jezik)){
            //     $data['content'] = $stranica_jezik->sadrzaj;
            // }

            $data['sekcije'] = DB::table('stranica_sekcija')->select('sekcija_stranice.*','sekcija_stranice_tip.naziv as tip_naziv')->where('web_b2c_seo_id',$web_b2c_seo_id)->join('sekcija_stranice','sekcija_stranice.sekcija_stranice_id','=','stranica_sekcija.sekcija_stranice_id')->join('sekcija_stranice_tip','sekcija_stranice_tip.sekcija_stranice_tip_id','=','sekcija_stranice.sekcija_stranice_tip_id')->where('sekcija_stranice.flag_aktivan', 1)->orderBy('stranica_sekcija.rbr','asc')->get();

            $data['anketa_id'] = !is_null($stranica->anketa_id) ? $stranica->anketa_id : 0;
            $data['web_b2c_narudzbina_id'] = 0;
            $data['custom'] = 1;

            $path = 'shop/themes/'.Support::theme_path().'pages/content';
        }
      
		return View::make($path,$data);
	} 

    function coment_add(){
        $lang = Language::multi() ? Request::segment(1) : null;

		$roba_id=Input::get('roba_id');
        $ime=Input::get('ime');
        $pitanje=Input::get('pitanje');
        $ocena=Input::get('ocena');
        
        $ip=All::ip_adress();
        $data=array(
        'roba_id'=>$roba_id,
        'ip_adresa'=>$ip,
        'ime_osobe'=>$ime,
        'pitanje'=>$pitanje,
        'komentar_odobren'=>0,
        'datum'=>date("Y-m-d"),
        'odgovoreno'=>0,
        'ocena'=>$ocena
        );
        DB::table('web_b2c_komentari')->insert($data);
		echo "Ok";
    }

    public function comment_add(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $data=Input::get();

       $validator_arr = array(
            'comment-name' => 'required|regex:'.Support::regex().'|max:200',
            'comment-review' => 'numeric',
            'comment-message' => 'required|regex:'.Support::regex().'|max:1000'
            );
        $translate_mess = Language::validator_messages();

       $validator = Validator::make($data,$validator_arr,$translate_mess);
        if($validator->fails() || !Captcha::check()){
            if(!Captcha::check()){
                $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
            }
            return Redirect::to(URL::previous().'#product-preview-tabs')->withInput()->withErrors($validator->messages())->with('contactError',true);
        }
        else {
            
            $ip=All::ip_adress();
            $data=array(
            'roba_id'=>$data['comment-roba_id'],
            'ip_adresa'=>$ip,
            'ime_osobe'=>$data['comment-name'],
            'pitanje'=>$data['comment-message'],
            'komentar_odobren'=>0,
            'datum'=>date("Y-m-d"),
            'odgovoreno'=>0,
            'ocena'=>$data['comment-review']
            );
            
            DB::table('web_b2c_komentari')->insert($data);

            return Redirect::back()->with('success_comment_message',true);
        }
    }

    public function newsletter_add(){
        $lang = Language::multi() ? Request::segment(1) : null;

	        $email=Input::get('email');
	        
            if(Session::has('newsletter_time_security') && (time() - intval(Session::get('newsletter_time_security'))) < (24*60*60)){
                echo Language::trans("E-mail je već registrovan").".";
            }else if(DB::table('web_b2c_newsletter')->where('email',$email)->count() > 0){
	            echo Language::trans("E-mail je već registrovan").".";
	        }
	        else {
                $kod = Support::custom_encrypt($email);

                 $body="Poštovani,<br><br>Prijavili ste se za prijem novosti iz naše prodavnice.<br>
                       Da biste aktivirali prijem novosti, molimo Vas da <a href='".Options::base_url().Url_mod::slug_trans('newsletter-potvrda')."/".$kod."' target='_blank'>KLIKNETE OVDE<a> kako biste potvrdili Vašu adresu.<br><br>
                       Ukoliko se niste Vi prijavili i ovu poruku poruku ste primili greškom, molimo Vas da je zanemarite.<br><br>
                       S poštovanjem,<br>
                       ". Options::company_name()   ."<br>"
                        . Options::company_adress() .", ". Options::company_city() ."<br>"    
                        ."mob: ". Options::company_phone()  ."<br>"
                        ."fax: ".   Options::company_fax();

                $subject="Prijava za newsletter na ".Options::company_name();

                WebKupac::send_email_to_client($body,$email,$subject);

                Session::put('newsletter_time_security',time());
                echo Language::trans("Potvrdite prijavu za newsletter preko vašeg mail-a").".";

	        }
	        
	}

    public function newsletter_confirm(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $kod = Request::segment(2+$offset);

        $email = Support::custom_decrypt($kod);
        $validator = Validator::make(array('email'=>$email),array('email' => 'required|email|unique:web_b2c_newsletter,email'),Language::validator_messages());

        if(!$validator->fails()){
            $data=array('email'=>$email,'aktivan'=>1);
            DB::table('web_b2c_newsletter')->insert($data);
        }
        
        return Redirect::to('/');
    }


    public function poll_answer(){
        $ip_adress = All::ip_adress();
        $web_b2c_narudzbina_id= Input::get('order_id') ? Input::get('order_id') : null;
        $inputs = Input::get();
        unset($inputs['order_id']);

        foreach($inputs as $name => $value){
            if(strpos($name, 'poll_text_answer_') !== false){
                $anketa_pitanje_id = intval(str_replace('poll_text_answer_','',$name));
                $validator = Validator::make(['poll_answer_text' => $value],['poll_answer_text' => 'regex:'.Support::regex().'|max:500'],Language::validator_messages());
                if($validator->fails() || is_null(DB::table('anketa_odgovor')->where('anketa_pitanje_id',$anketa_pitanje_id)->first())){
                    if($validator->fails()){
                        return Redirect::back()->withInput()->with('pollTextError',Language::trans('Polje sadrži ne doyvoljene karaktere!'));
                    }
                    if(is_null(DB::table('anketa_odgovor')->where('anketa_pitanje_id',$anketa_pitanje_id)->first())){
                        return Redirect::back()->withInput()->with('pollTextError',Language::trans('Niste popunili neko od polja!'));
                    }
                }
            }
        }

        $dateNow = date('Y-m-d H:i:s');
        foreach($inputs as $name => $value){
            $anketa_odgovor_text = null;
            $anketa_odgovor_id = null;
            if(strpos($name, 'poll_text_answer_') !== false){
                $anketa_pitanje_id = intval(str_replace('poll_text_answer_','',$name));
                $anketa_odgovor_text = $value;
                $anketa_odgovor_id = DB::table('anketa_odgovor')->where('anketa_pitanje_id',$anketa_pitanje_id)->pluck('anketa_odgovor_id');
            }else{
                $anketa_pitanje_id = intval(str_replace('poll_check_answer_','',$name));
                $anketa_odgovor_id = $value;
            }
            DB::table('anketa_odgovor_korisnik')->insert(array('anketa_pitanje_id'=>$anketa_pitanje_id, 'anketa_odgovor_id'=>$anketa_odgovor_id, 'anketa_odgovor_text' => $anketa_odgovor_text, 'ip'=>$ip_adress, 'web_kupac_id'=>(($kupac_id = Cart::kupac_id()) > 0 ? $kupac_id : null), 'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id, 'datum'=> $dateNow));
        }



        return Redirect::back()->with('pollAnswerSuccess',Language::trans('Vaša anketa je poslata').'.');        
    }

    public function poll_content(){
        $web_b2c_narudzbina_id= Input::get('order_id') ? Input::get('order_id') : null;
        $anketa = DB::table('anketa')->where('narudzbina_izabran',1)->first();
        if(is_null($anketa)){
            return '';
        }

        return View::make('shop/themes/'.Support::theme_path().'partials/poll',array('anketa_id'=>$anketa->anketa_id, 'web_b2c_narudzbina_id'=>$web_b2c_narudzbina_id));
    }

}


 