<?php 

class ConfiguratorController extends Controller {


	public function konfigurator(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;
		$id = Request::segment(2+$offset);

		$strana= 'konfigurator';
		$konfigurator = DB::table('konfigurator')->where(array('konfigurator_id'=>$id,'dozvoljen'=>1))->first();
		if(!isset($konfigurator->konfigurator_id)){
			return Redirect::to(Options::base_url());
		}
		$korpa_ukupno = 0;
		if($korpa_id = Cart::korpa_id()){
			$korpa_ukupno = intval(DB::select("SELECT SUM(jm_cena*kolicina) AS korpa_ukupno FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_id = ".$korpa_id."")[0]->korpa_ukupno);
		}

		$seo = Seo::page($strana);
		$data=array(
		"strana"=>$strana,
		"org_strana"=>$strana,
        "title"=>$seo->title,
        "description"=>$seo->description,
        "keywords"=>$seo->keywords,
		"konfigurator_id" => $id,
		"grupe"=>DB::select("SELECT grupa_pr_id, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = kg.grupa_pr_id) as grupa FROM konfigurator_grupe kg WHERE konfigurator_id = ".$id." ORDER BY rbr ASC"),
		"korpa_ukupno"=>$korpa_ukupno.'.00'
		);

		return View::make('shop/themes/'.Support::theme_path().'pages/configurator',$data);
					
	} 

}