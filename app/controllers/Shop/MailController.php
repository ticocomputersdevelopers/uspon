<?php 
use Service\Mailer;


class MailController extends Controller {

	public function meil_send(){
        $lang = Language::multi() ? Request::segment(1) : null;

    	if(Options::gnrl_options(3003)){   
    	 		
	    	$data = array(
	    		'name' => Input::get('name'), 
	    		'email' => Input::get('email'),
	    		'msg' => Input::get('message'),
	    		'url' => Options::base_url()
	    	);

	    	Mail::send('shop.email.contact', $data, function($message){
	    	    $message->from(Input::get('email'), Input::get('name'));

	    	    $message->to(Options::company_email())->subject(EmailOption::subject());
	    	});
    	}else{
 		
	        $body=" <p>Poštovani,<br /> Imate novu poruku sa kontakt forme sa sajta <a href='".Options::domain()."'>".Options::domain()."</a><br />
	        <b>Pošiljalac:</b> ".Input::get('name')." <br />
	        <b>Email:</b> ".Input::get('email')."<br />
	        <b>Tekst poruke:</b> 
	        </p> <p>".Input::get('message')."</p>";
	        $subject="Poruka sa ".parse_url(Options::domain(), PHP_URL_HOST);
	        Mailer::send(Options::company_email(),Options::company_email(),$subject, $body);
    	}

    } 

	public function contact_message_send(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $data=Input::get();

       $validator_arr = array(
            'contact-name' => 'required|regex:'.Support::regex().'|max:200',
            'contact-email' => 'required|email',
            'contact-message' => 'required|max:1000'
            );
        $translate_mess = Language::validator_messages();

       $validator = Validator::make($data,$validator_arr,$translate_mess);
        if($validator->fails() || !Captcha::check()){
            if(!Captcha::check()){
                $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
            }
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        else {
	        $body=" <p>Poštovani,<br /> Imate novu poruku sa kontakt forme sa sajta <a href='".Options::domain()."'>".Options::domain()."</a><br />
	        <b>Pošiljalac:</b> ".Input::get('contact-name')." <br />
	        <b>Email:</b> ".Input::get('contact-email')."<br />
	        <b>Tekst poruke:</b> 
	        </p> <p>".Input::get('contact-message')."</p>";
	        $subject="Poruka sa ".Options::server();
	        Mailer::send(Options::company_email(),Options::company_email(),$subject, $body);

	        return Redirect::back()->with('message',Language::trans('Vaša poruka je poslata').'!');
        }
	}

	public function enquiry_message_send(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $data=Input::get();
        // $id=Input::get('roba_id');
       $validator_arr = array(
            'contact-name' => 'required|regex:'.Support::regex().'|max:200',
            'contact-email' => 'required|email',
            'contact-phone' => 'required',
            'contact-message' => 'required|max:1000'
            );
        $translate_mess = Language::validator_messages();

       $validator = Validator::make($data,$validator_arr,$translate_mess);
        if($validator->fails() || !Captcha::check()) {
        	if(!Captcha::check()){ 
                $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
            }
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        else {
	        $body=" <p>Poštovani,<br /> Stigao je upit za artikal ".Product::getName(Input::get('roba_id'))." sa šifrom: ".Input::get('roba_id')." <br />
	        <b>Pošiljalac:</b> ".Input::get('contact-name')." <br />
	        <b>Email:</b> ".Input::get('contact-email')."<br />
	        <b>Telefon:</b> ".Input::get('contact-phone')."<br />
	        <b>Tekst poruke:</b> 
	        </p> <p>".Input::get('contact-message')."</p>";
	      
	        $subject="Upit za artikal sa šifrom: ".Input::get('roba_id');
	        Mailer::send('web@uspon.rs','web@uspon.rs',$subject, $body);

	        return Redirect::back()->with('message',Language::trans('Vaša poruka je poslata').'!');
        }
	}

}