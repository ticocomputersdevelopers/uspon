 <?php

class RaiffeisenBankingController extends BaseController{

    public function raiffeisen(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);

        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where(array('web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,'stornirano' => 1))->first();
        if(is_null($web_b2c_narudzbina)){
            return Redirect::to(Options::base_url());
        }
        $datum_dokumenta = explode(' ',$web_b2c_narudzbina->datum_dokumenta);
        $datum_isteka_rezervacije = date("d-m-Y", strtotime($datum_dokumenta[0]. ' + 3 days')); 
        $cartItems=DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get();

        $web_kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if(is_null($web_kupac)){
            return Redirect::to(Options::base_url());
        }
        $preduzece = DB::table('preduzece')->where('preduzece_id',1)->first();
        if(is_null($preduzece)){
            return Redirect::to(Options::base_url());
        }

        $orgOid = $web_b2c_narudzbina_id;

        $orgAmount = DB::select("SELECT round(SUM(jm_cena*kolicina), 2) AS suma from web_b2c_narudzbina_stavka where web_b2c_narudzbina_id=".$orgOid)[0]->suma;
        if($web_b2c_narudzbina->web_nacin_isporuke_id != 2) {
            $orgAmount += Cart::troskovi($web_b2c_narudzbina_id);
        }
        $orgAmount = number_format($orgAmount,2,",","");

        $okUrl =  Options::base_url()."raiffeisen-response-status/".Order::broj_dokumenta($orgOid)."/".$web_kupac->kod;
        $failUrl = Options::base_url()."raiffeisen-response-status/".Order::broj_dokumenta($orgOid)."/".$web_kupac->kod;
        $cancelUrl = Options::base_url()."korpa";
        
        $data = [
            'web_b2c_narudzbina_id' => $web_b2c_narudzbina_id,
        ];
        
        $pdf = App::make('dompdf');
        
        $pdf->loadView('admin.pdf_profaktura', $data);
        $pdf=$pdf->output();
        $profaktura=base64_encode($pdf);

        $data_status = array('web_b2c_narudzbina_id' => $web_b2c_narudzbina_id, 'narudzbina_status_id' => 2);
        if(!DB::table('narudzbina_status_narudzbina')->where($data_status)->first()){
            DB::table('narudzbina_status_narudzbina')->insert($data_status);
        }
        $data = array(
            'customer_first_name' => $web_kupac->ime,
            'customer_last_name' => $web_kupac->prezime,
            'customer_email' => $web_kupac->email,
            'customer_address' => $web_kupac->adresa,
            'customer_mobile_phone' => $web_kupac->telefon,
            'merchant_name' => Options::company_name(),
            'merchant_address1' => Options::company_adress(),
            'merchant_address2' => '32000 Čačak',
            'point_of_sale_code' => '79837',
            'merchant_registration_number' => '06084613',
            'order_number' => Order::broj_dokumenta($orgOid),
            'order_expiration_date' => date("d.m.Y", strtotime($datum_isteka_rezervacije)),
            'order_amount'=>$orgAmount,
            'order'=>array(),
            'return_url'=>$okUrl,
            'nok_return_url'=>$failUrl,
            'web_kupac'=>$web_kupac,
            'preduzece'=>$preduzece,
            'order_base64'=>$profaktura,
	    	'title' => 'Raiffeisen',
	    	'description' => 'raiffeisen',
	    	'keywords' => 'raiffeisen',
	    	'strana' => 'raiffeisen',
            "org_strana"=>'raiffeisen'
            );
            foreach($cartItems as $row){
                $roba = DB::table('roba')->where('roba_id',$row->roba_id)->first();
                
                    $data['order']['products'][]=[
                        'productName'=> $roba->naziv,
                        'productDetail'=> '',
                        'measurementUnits'=> Product::jedinica_mere($row->roba_id)->naziv,
                        'price'=> strval(round($row->jm_cena)),
                        'quantity'=> strval((int) $row->kolicina)
                    ];                
            }

            if($web_b2c_narudzbina->web_nacin_isporuke_id != 2 && Cart::troskovi($web_b2c_narudzbina_id) > 0) {
                    $data['order']['products'][]=[
                        'productName'=> 'Dostava',
                        'productDetail'=> 'Troškovi dostave',
                        'measurementUnits'=> '',
                        'price'=> strval(round(Cart::troskovi($web_b2c_narudzbina_id))),
                        'quantity'=> 1
                    ];  
            }

            //All::dd($data);
    	return View::make('shop/themes/'.Support::theme_path().'pages/raiffeisen',$data);
    }

    public function raiffeisen_response($broj_dokumenta,$kod_kupca){
        if(DB::table('web_b2c_narudzbina')->where(array('broj_dokumenta'=>$broj_dokumenta,'web_nacin_placanja_id'=>5))->first()) {
            $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where(array('broj_dokumenta'=>$broj_dokumenta,'web_nacin_placanja_id'=>5))->first();

            $web_kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
            if(is_null($web_kupac) || $web_kupac->kod != $kod_kupca){
                return Redirect::to(Options::base_url());
            }
            if ($web_b2c_narudzbina->stornirano) {
                $status = 0;
            } else {
                $status = 1;
            }
            $data = array(
                            'strana' => 'web_kredit_odgovor',
                            'title' => Language::trans('Rezultat apliciranja za kredit'),
                            'description' => '',
                            'keywords' => '',
                            'status' => $status,
                            'broj_porudzbine' => $broj_dokumenta,
                            'iznos' => Cart::cena(DB::select("SELECT round(SUM(jm_cena*kolicina), 2) AS suma from web_b2c_narudzbina_stavka where web_b2c_narudzbina_id=".$web_b2c_narudzbina->web_b2c_narudzbina_id)[0]->suma),
                            'web_kupac' => $web_kupac,
                            'broj_transakcije' => $broj_dokumenta,
                            'datum_transakcije' => date('d.m.Y. H:i:s',strtotime($web_b2c_narudzbina->post_date))
                        );
            
            return View::make('shop/themes/'.Support::theme_path().'pages/web_kredit_odgovor',$data);
        } else {
            return Redirect::to(Options::base_url());
        }
    }

    public function raiffeisen_failure(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $web_b2c_narudzbina_id = Request::segment(2+$offset);

        $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        if(is_null($web_b2c_narudzbina) && $web_b2c_narudzbina->web_nacin_placanja_id!=3){
            return Redirect::to(Options::base_url());
        }
        $kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
        if(is_null($kupac)){
            return Redirect::to(Options::base_url());
        }
        if(!Session::has('b2c_korpa')){
            return Redirect::to(Options::base_url());
        }
        Session::forget('b2c_korpa');


        $subject=Language::trans('wspay')." ".Order::broj_dokumenta($web_b2c_narudzbina_id);
        
        if($kupac->flag_vrsta_kupca == 0){
            $name = $kupac->ime.' '.$kupac->prezime;
        }else{
            $name = $kupac->naziv.' '.$kupac->pib;
        }
        $data=array(
            "strana"=>'order',
            "org_strana"=>'order',
            "title"=> $subject,
            "description"=>"",
            "keywords"=>"",
            "web_b2c_narudzbina_id"=>$web_b2c_narudzbina_id,
            "bank_result"=> json_decode($web_b2c_narudzbina->result_code),
            "kupac"=>$kupac
            );
        $body = View::make('shop/themes/'.Support::theme_path().'partials/order_details',$data)->render();
        WebKupac::send_email_to_client($body,$kupac->email,$subject);

        return View::make('shop/themes/'.Support::theme_path().'pages/order',$data);  
    }

    public static function representative_example() {
        $data = array(
                        'strana' => 'web_kredit',
                        'title' => Language::trans('Web Kredit Raiffeisen Banke'), 
                        'description' => '', 
                        'keywords' => ''
                    );
        return View::make('shop/themes/'.Support::theme_path().'pages/web_kredit',$data);
    }

    public function raiffeisen_response_servis($broj_dokumenta,$status) {
        // var_dump(All::ip_adress()); die;
        $ip_adrese = array('127.0.0.1','185.47.211.6','176.67.218.17','172.30.20.40','193.34.143.1','193.34.142.1','193.34.143.20','193.34.142.20');
        if(!in_array(All::ip_adress(),$ip_adrese)) {
            die('Pokusaj pristupa servisu sa nedozvoljene IP adrese!');
        }
        if(!in_array($status,array('0','1'))) {
            die('Uneli ste neodgovarajuce parametre!');
        } else if (!DB::table('web_b2c_narudzbina')->where(array('broj_dokumenta'=>$broj_dokumenta,'web_nacin_placanja_id'=>5))->first()) {
            die('Uneli ste neodgovarajuci broj narudzbine!');
        }

        $data_echo = array(
                    'status' => $status == 1 ? 'true' : 'false',
                    'broj dokumenta' => $broj_dokumenta
                        );
        echo json_encode($data_echo);

        if ($status == 1) {

           $web_b2c_narudzbina_id = DB::table('web_b2c_narudzbina')->where(array('broj_dokumenta'=>$broj_dokumenta,'web_nacin_placanja_id'=>5))->pluck('web_b2c_narudzbina_id');

            $bank_response = json_encode(array(
                'realized' => $request['Success'] = 1
            ));

            $data = array(  
                            'stornirano' => 0,
                            'result_code'=> $bank_response
                        );
           DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update($data);



                $subject=Language::trans('Raiffeisen odobren web kredit')." ".Order::broj_dokumenta($web_b2c_narudzbina_id);
                $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
                $kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
                $vauceri = DB::table('vaucer')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->get();
                $suma_vaucer = 0;
                if($vauceri){
                    foreach ($vauceri as $vaucer) {
                        $suma_vaucer += $vaucer->iznos;
                    }
                }

                    $data=array(
                        "strana"=>'order',
                        "org_strana"=>'order',
                        "title"=> $subject,
                        "description"=>"",
                        "keywords"=>"",
                        "web_b2c_narudzbina_id"=>$web_b2c_narudzbina_id,
                        "web_nacin_isporuke_id"=>$web_b2c_narudzbina->web_nacin_isporuke_id,
                        "bank_result"=> json_decode($web_b2c_narudzbina->result_code),
                        "suma_vaucer" => $suma_vaucer,
                        "kupac"=>$kupac,
                        "message" => !Session::has('b2c_kupac') ? true : false
                        );
                    $body = View::make('shop/themes/'.Support::theme_path().'partials/order_details',$data)->render();

                    WebKupac::send_email_to_client($body,$kupac->email,$subject);
                    All::send_email_to_admin($body,$kupac->email,null,$subject);
                    
        } else {
            $web_b2c_narudzbina_id = DB::table('web_b2c_narudzbina')->where(array('broj_dokumenta'=>$broj_dokumenta,'web_nacin_placanja_id'=>5))->pluck('web_b2c_narudzbina_id');


             $bank_response = json_encode(array(
                'realized' => $request['Success'] = 0
                ));

                    $data = array(  
                            'stornirano' => 1,
                            'result_code'=> $bank_response
                        );
                    DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->update($data);


                $subject=Language::trans('Raiffeisen odbijen web kredit')." ".Order::broj_dokumenta($web_b2c_narudzbina_id);
                $web_b2c_narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
                $kupac = DB::table('web_kupac')->where('web_kupac_id',$web_b2c_narudzbina->web_kupac_id)->first();
                $vauceri = DB::table('vaucer')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->get();
                $suma_vaucer = 0;
                if($vauceri){
                    foreach ($vauceri as $vaucer) {
                        $suma_vaucer += $vaucer->iznos;
                    }
                }

                    $data=array(
                        "strana"=>'order',
                        "org_strana"=>'order',
                        "title"=> $subject,
                        "description"=>"",
                        "keywords"=>"",
                        "web_b2c_narudzbina_id"=>$web_b2c_narudzbina_id,
                        "web_nacin_isporuke_id"=>$web_b2c_narudzbina->web_nacin_isporuke_id,
                        "bank_result"=> json_decode($web_b2c_narudzbina->result_code),
                        "suma_vaucer" => $suma_vaucer,
                        "kupac"=>$kupac,
                        "message" => !Session::has('b2c_kupac') ? true : false
                        );
                    $body = View::make('shop/themes/'.Support::theme_path().'partials/order_details',$data)->render();

                    WebKupac::send_email_to_client($body,$kupac->email,$subject);
                    All::send_email_to_admin($body,$kupac->email,null,$subject);
                    
        }
    }

}