<?php

class ArticleController extends Controller {

	public function article(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $article = Request::segment(2+$offset);
        $slika_id = Request::segment(3+$offset);

        $roba_id=Product::get_product_id($article);
        if($roba_id != 0){
          // if($roba_id>0 && Product::checkView($roba_id)){
           if($roba_id>0){
            All::articleView($roba_id);
            $seo = Seo::article($roba_id);
            $image = DB::table('web_slika')->where(array('roba_id'=>$roba_id, 'flag_prikazi'=>1))->orderBy('akcija','desc')->first();

            $slika_parent_id = 0;
            if(Options::web_options(310) == 1){
                $glavne_slike = Product::get_list_images($roba_id,true);
                if($slika_id){
                    $slika_parent_id = $slika_id;
                }else{
                    $slika_parent_id = isset($glavne_slike[0]) ? $glavne_slike[0]->web_slika_id : 0;
                }
                $slike = Product::get_list_child_images($roba_id,$slika_parent_id);
            }else{
                $slike = Product::get_list_images($roba_id);
                $glavne_slike = array();
            }

            $data=array(
            "strana"=>'artikal',
            "title"=>$seo->title,
            "description"=>$seo->description,
            "keywords"=>$seo->keywords,
            "og_image"=> !is_null($image) ? $image->putanja : null,
            "roba_id"=>$roba_id,
            "grupa_pr_id"=>DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id'),
            "proizvodjac_id"=>DB::table('roba')->where('roba_id',$roba_id)->pluck('proizvodjac_id'),
            "articles"=> Product::get_related($roba_id),
            "vezani_artikli"=>DB::table('vezani_artikli')->where('roba_id',$roba_id)->orderBy('vezani_roba_id','asc')->get(),
            "srodni_artikli"=>DB::table('srodni_artikli')->where('roba_id',$roba_id)->orderBy('srodni_roba_id','asc')->get(),
            "fajlovi" => DB::select("SELECT * FROM web_files WHERE roba_id = ".$roba_id." ORDER BY vrsta_fajla_id ASC"),
            "glavne_slike" => $glavne_slike,
            "slika_parent_id" => $slika_parent_id,
            "slike" => $slike,
            "slika_big" => Product::web_slika_big($roba_id,$slika_parent_id)
            );

            return View::make('shop/themes/'.Support::theme_path().'pages/article_details',$data);
           }
        }
        
        if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
            return Redirect::to($new_link);
        }
        $data=array(
            "strana"=>'Not found',
            "org_strana"=>'Not found',
            "title"=>'Not found',
            "description"=>'Not found',
            "keywords"=>'Not found'
        );
        $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
        return Response::make($content, 404);
	}

    public function product_deklaracija()
    {
        $offset = Language::segment_offset();
        $id = Request::segment(2+$offset);
        if(!DB::table('roba')->where('roba_id',$id)->exists()) {
            $data=array(
                "strana"=>'Not found',
                "org_strana"=>'Not found',
                "title"=>'Not found',
                "description"=>'Not found',
                "keywords"=>'Not found'
            );
            $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
            return Response::make($content, 404);
        }

        $data=array(
            'strana'=>'energetska_deklaracija',
            'title'=>'energetska_deklaracija',
            'roba_id'=>$id,
            "org_strana"=>'energetska_deklaracija',
            "description"=>'Energy class declaration',
            "keywords"=>'energy_class'
        );
        return View::make('shop/themes/'.Support::theme_path().'pages/energy_class', $data);
    }

    public function article_link(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $roba_id = Input::get('roba_id');

        return Url_mod::slug_trans('artikal').'/'.Url_mod::slugify(Product::seo_title($roba_id));
    }
    
    public function quick_view(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $roba_id = Input::get('roba_id');

        $roba = DB::table('roba')->where('roba_id',$roba_id)->first();
        $image = Product::web_slika($roba_id);
        $proizvodjac = DB::table('proizvodjac')->where('proizvodjac_id',$roba->proizvodjac_id)->first();

        $data = array(
            'roba_id' => $roba_id,
            'image' => $image,
            'proizvodjac' => $proizvodjac,
            );
        return View::make('shop/themes/'.Support::theme_path().'partials/products/ajax/quick_view_contant',$data)->render();
    }
    
    public function stampanje()
    {
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
       
        $roba_id = Request::segment(2+$offset);
        if(is_null(DB::table('roba')->where('roba_id',$roba_id)->first())){
            $data=array(
                "strana"=>'Not found',
                "org_strana"=>'Not found',
                "title"=>'Not found',
                "description"=>'Not found',
                "keywords"=>'Not found'
            );
            $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
            return Response::make($content, 404);
        }

        $data = [
            'roba_id' => $roba_id,
        ];
        $pdf = App::make('dompdf');
        //var_dump($pdf);die;
        
        $pdf->loadView('shop.themes.bsmodern.pages.stampanje', $data);
        try {
            return $pdf->stream();
        } catch (Exception $e) {
            return Redirect::to(Options::base_url().Url_mod::slug_trans('artikal').'/'.Url_mod::slugify(Product::seo_title($roba_id)));
        }


    }

}