<?php

class AkcijaController extends Controller {

	public function akcija_products(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;
		$grupa = Request::segment(2+$offset);

		$datum = date('Y-m-d');
		if($grupa==null){
			$check_grupa = "";
		}else{
			foreach(DB::table('grupa_pr')->where('grupa_pr_id','!=',-1)->where('grupa_pr_id','!=',0)->get() as $gr){
			
		    $count = DB::select("SELECT DISTINCT COUNT(r.roba_id) FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkLager('join').Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND akcija_flag_primeni = 1 AND CASE WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_od <= '".$datum."' AND datum_akcije_do >= '".$datum."' WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NULL THEN datum_akcije_od <= '".$datum."' WHEN datum_akcije_od IS NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_do >= '".$datum."' ELSE 1 = 1 END ".Product::checkLager().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND r.grupa_pr_id = ".$gr->grupa_pr_id."")[0]->count;
		    
				if(Url_mod::slug_trans($gr->grupa) == $grupa && $count > 0){
					$grupa_pr_id = $gr->grupa_pr_id;
					break;
				}
			}

			if(isset($grupa_pr_id)){
				$level2=Groups::vratiSveGrupe($grupa_pr_id);
			}else{
	            $data=array(
	                "strana"=>'Not found',
	                "org_strana"=>'Not found',
	                "title"=>'Not found',
	                "description"=>'Not found',
	                "keywords"=>'Not found'
	            );
	            $content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
	            return Response::make($content, 404);				
			}

			$check_grupa = "AND r.grupa_pr_id IN(".implode(",",$level2).")";
		}

		    $select="SELECT DISTINCT r.roba_id";
		    $roba=" FROM roba r".Product::checkLager('join').Product::checkImage('join').Product::checkCharacteristics('join')." LEFT JOIN grupa_pr gp ON r.grupa_pr_id = gp.grupa_pr_id";
			$where=" WHERE r.flag_aktivan = 1 AND gp.web_b2c_prikazi = 1 AND r.flag_prikazi_u_cenovniku = 1 AND akcija_flag_primeni = 1 AND CASE WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_od <= '".$datum."' AND datum_akcije_do >= '".$datum."' WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NULL THEN datum_akcije_od <= '".$datum."' WHEN datum_akcije_od IS NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_do >= '".$datum."' ELSE 1 = 1 END  ".Product::checkLager().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."".$check_grupa."";

			$count=count(DB::select($select.$roba.$where));

	        	if(Session::has('limit')){
					$limit=Session::get('limit');
				}
				else {
					$limit=20;
				}

			    if(Input::get('page')){
			    	$pageNo = Input::get('page');
			    }else{
			    	$pageNo = 1;
			    }

			    $offset = ($pageNo-1)*$limit;
				
				if(Session::has('order')){
					if(Session::get('order')=='price_asc')
					{
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='price_desc'){
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='news'){
					$artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='name'){
					$artikli=DB::select($select.", r.naziv_web".$roba.$where." ORDER BY r.naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='rbr'){
					$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order') == 'quantity') {
						$artikli=DB::select($select.", CASE WHEN (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) is null then 0 ELSE (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) END as kol".$roba.$where." ORDER BY kol DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
				}
				else {
			    	$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
				}

		$seo = Seo::page("akcija");
		$data=array(
			"strana"=>"akcija",
            "title"=>$seo->title,
            "description"=>$seo->description,
            "keywords"=>$seo->keywords,
            "url"=>"akcija".(!is_null($grupa) ? "/".$grupa : ""),
			"grupa"=>isset($grupa) ? $grupa : '',
			"articles"=>$artikli,
			"limit"=>$limit,
			"count_products"=>$count,
			"filter_prikazi" => 0
		);
 
		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);		
	}
}