<?php

class TipController extends Controller {

	public function tip_products(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $naziv = Request::segment(2+$offset);
        $grupa = Request::segment(3+$offset);
		$tipovi = DB::table('tip_artikla')->where('active',1)->where('tip_artikla_id','!=',0)->orderBy('rbr','asc')->get();

		$tip_obj = null;
		foreach($tipovi as $tip){
			if(Url_mod::slug_trans($tip->naziv) == $naziv){
				$tip_obj = $tip;
				break;
			}
		}

		if(is_null($tip_obj)){
			return Redirect::to(Options::base_url());
		}else{
			$strana = DB::table('web_b2c_seo')->where('tip_artikla_id',$tip_obj->tip_artikla_id)->first();
		}
		if($grupa==null){
			$check_grupa = "";
		}else{ 
			$grupa_pr_id = "";
			$check_grupa = ""; 
			foreach(DB::table('grupa_pr')->where('grupa_pr_id','!=',-1)->where('grupa_pr_id','!=',0)->get() as $gr){

			    $count = DB::select("SELECT DISTINCT COUNT(r.roba_id) FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkImage('join').Product::checkCharacteristics('join')." 
			    	WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND r.grupa_pr_id = ".$gr->grupa_pr_id." AND r.tip_cene = ".$tip_obj->tip_artikla_id."")[0]->count;
			    
					if(Url_mod::slug_trans($gr->grupa) == $grupa && $count > 0){
						$grupa_pr_id = $gr->grupa_pr_id;
						$description = Seo::grupa_description($grupa_pr_id);
						$keywords = Seo::grupa_keywods($grupa_pr_id);
						$level2=Groups::vratiSveGrupe($grupa_pr_id);

						$check_grupa = "AND r.grupa_pr_id IN(".implode(",",$level2).")";
						break;
					}
				}
			}

		    $select="SELECT DISTINCT r.roba_id";
		    $roba=" FROM roba r".Product::checkImage('join').Product::checkCharacteristics('join')."";
			$where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND tip_cene = ".$tip_obj->tip_artikla_id." ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."".$check_grupa."";

			$count=count(DB::select($select.$roba.$where));

	        	if(Session::has('limit')){
					$limit=Session::get('limit');
				}
				else {
					$limit=20;
				}

			    if(Input::get('page')){
			    	$pageNo = Input::get('page');
			    }else{
			    	$pageNo = 1;
			    }

			    $offset = ($pageNo-1)*$limit;
				
				if(Session::has('order')){
					if(Session::get('order')=='price_asc')
					{
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='price_desc'){
					$artikli=DB::select($select.", r.".Options::checkCena()."".$roba.$where." ORDER BY r.".Options::checkCena()." DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='news'){
					$artikli=DB::select($select.", r.roba_id".$roba.$where." ORDER BY r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='name'){
					$artikli=DB::select($select.", r.naziv_web".$roba.$where." ORDER BY r.naziv_web ASC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='rbr'){
					$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order') == 'quantity') {
						$artikli=DB::select($select.", CASE WHEN (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) is null then 0 ELSE (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) END as kol".$roba.$where." ORDER BY kol DESC LIMIT ".$limit." OFFSET ".$offset."");
					}
				}
				else {
			    	$artikli=DB::select($select.", r.rbr".$roba.$where." ORDER BY r.rbr DESC, r.roba_id DESC LIMIT ".$limit." OFFSET ".$offset."");
				}
		if(!is_null($strana)){
			$seo = Seo::page($strana->naziv_stranice);
		}else{
			$seo = (object) array('title'=>$tip_obj->naziv,'description'=>$tip_obj->naziv,'keywords'=>$tip_obj->naziv);
		}
		$data=array(
			"strana"=>"tip",
            "title"=>$seo->title,
            "description"=>$seo->description,
            "keywords"=>$seo->keywords,
			"url"=>"tip/".$naziv.(!is_null($grupa) ? "/".$grupa : ""),
			"tip_id"=>$tip_obj->tip_artikla_id,
			"tip"=>$tip_obj->naziv,
			"grupa"=>isset($grupa) ? $grupa : '',
			"articles"=>$artikli,
			"limit"=>$limit,
			"count_products"=>$count,
			"filter_prikazi" => 0
		);

		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);		
	}


	public function tip_ajax(){
        $lang = Language::multi() ? Request::segment(1) : null;

    	$tip_id = Input::get('tip_id');
    	$offset = intval(Input::get('offset'));
    	$limit = intval(Input::get('limit'));
    	$kind = Input::get('kind');

    	if($kind == 'tip'){
			$max_number = All::provera_tipa($tip_id,1);
		}else{
			$max_number = All::broj_akcije();
		}

		$max_offset = intval($max_number / $limit);
		$mod_offset = $max_number % $limit;

		if($mod_offset > 0){
			$max_offset = $max_offset + 1;
		}

		if($offset > $max_offset){
			$new_offset = 1;
		}else{
			if($offset == 0){
				$new_offset = $max_offset;
			}else{
				$new_offset = $offset;
			}	
		}

		if($kind == 'tip'){
			$array_articles = Articles::artikli_tip($tip_id,$limit,$new_offset);
		}else{
			$array_articles = Articles::akcija(null,$limit,$new_offset);
		}

		$data = array(
			'articles' => $array_articles
			);
    	$results = array(
    		'list' => View::make('shop/themes/'.Support::theme_path().'/partials/products/ajax/products_action_type',$data)->render(),
    		'offset' => $new_offset
    	);
    	echo json_encode($results);
    }

    
} 