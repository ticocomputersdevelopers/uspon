<?php

use Illuminate\Support\Facades\Response;
use Service\Mailer;

class B2bUserController extends Controller {

    public function userCard($date_start=null,$date_end=null){
        $cart_items = DB::table('web_b2b_kartica')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()));
        if(!is_null($date_start) && $date_start != 'null'){
            $cart_items = $cart_items->where('datum_dokumenta','>=',$date_start);
        }else{
            $date_start = null;
        }
        if(!is_null($date_end) && $date_end != 'null'){
            $cart_items = $cart_items->where('datum_dokumenta','<=',$date_end);
        }else{
            $date_end = null;
        }

        $sumDuguje = B2b::sumDuguje($cart_items);
        $sumPotrazuje = B2b::sumPotrazuje($cart_items);
        $cart_items = $cart_items->orderBy('datum_dokumenta','desc')->paginate(30);
        $seo =array(
            'title'=>"Kartica kupca",
            'description'=>"Kartica kupca",
            'keywords'=>"Kartica kupca"
        );
        $saldo =  $sumDuguje-$sumPotrazuje;
        $items = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->orderBy('broj_stavke','asc')->get();
        
        return View::make('b2b.pages.user_card',compact('items','cart_items','sumDuguje','sumPotrazuje','seo','saldo','date_start','date_end'));

    }

    public function partnerCartItems(){
        $karica_id = Input::get('karica_id');

        $content='';

        $sifra_dokumenta = DB::table('web_b2b_kartica')->where('web_b2b_kartica_id',$karica_id)->pluck('opis');
        if(substr($sifra_dokumenta,0,1) == 'R'){
            $racun_id = DB::table('racun')->where('broj_dokumenta',$sifra_dokumenta)->pluck('racun_id');
            if(!is_null($racun_id)){
                $stavke = DB::table('racun_stavka')->select('broj_stavke','id_is','naziv_web','kolicina','pcena')->join('roba','racun_stavka.roba_id','=','roba.roba_id')->where('racun_id',$racun_id)->orderBy('broj_stavke','asc')->get();

                $content = View::make('b2b/partials/ajax/partner_cart_items',array('stavke'=>$stavke))->render();
            }
        }
        echo $content;
    }

    public function cardRefresh(){
        if(B2bOptions::info_sys('calculus')){
            Calculus::updateCart();
        }elseif(B2bOptions::info_sys('infograf')){
            Infograf::updateCart();
        }elseif(B2bOptions::info_sys('logik')){
            Logik::updateCart();
        }
        
        return Redirect::back();    
    }

    public function userOrders($date_start=null,$date_end=null){
        $orders = DB::table('web_b2b_narudzbina')->select('*',DB::raw('(select sum(kolicina*jm_cena) from web_b2b_narudzbina_stavka where web_b2b_narudzbina_id = web_b2b_narudzbina.web_b2b_narudzbina_id) as iznos'))->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()));
        if(!is_null($date_start) && $date_start != 'null'){
            $orders = $orders->where('datum_dokumenta','>=',$date_start);
        }else{
            $date_start = null;
        }
        if(!is_null($date_end) && $date_end != 'null'){
            $orders = $orders->where('datum_dokumenta','<=',$date_end);
        }else{
            $date_end = null;
        }

        $orders = $orders->orderBy('datum_dokumenta','desc')->paginate(30);
        $seo =array(
            'title'=>"Narudžbine",
            'description'=>"Narudžbine",
            'keywords'=>"Narudžbine"
        );
        
        return View::make('b2b.pages.user_orders',compact('orders','seo','date_start','date_end'));

    }

    public function userOrder(){
        $web_b2b_narudzbina_id = Input::get('narudzbina_id');
        $order = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->first();
        $partner = DB::table('partner')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
        $orderItems = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$order->web_b2b_narudzbina_id)->orderBy('broj_stavke','asc')->get();
        $partnerKorisnik = null;
        return View::make('b2b.emails.order',compact('order','partner','partnerKorisnik','orderItems'))->render();;
    }
    
    public function userEdit(){

        $mesto = DB::table('mesto')->where('mesto_id','>','0')->orderBy('mesto','asc')->get();
        $seo=array(
            "title"=>"Informacije o korisniku",
            "description"=>"",
            "keywords"=>"",
        );
        $is_is = B2bOptions::info_sys('wings') || B2bOptions::info_sys('calculus') || B2bOptions::info_sys('infograf') || B2bOptions::info_sys('logik');
        $partner = DB::table('partner')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
        trim($partner->pib);
        $pib = explode(" ", $partner->pib);
        for ($i=0;$i<count($pib);$i++) {
            if(!is_numeric($pib[$i]))
            {
                array_splice($pib, $i, 1);
                $i--;
            }
        }
        $partner->pib = implode(" ", $pib);

        return View::make('b2b.pages.user_edit',compact('mesto','seo','partner','is_is'));
    }

    public function userUpdate(){
        $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        $inputs = Input::all();

        $rules = array(
                'naziv' => 'required|regex:'.Support::regex().'|between:2,50',
                'adresa' => 'required|regex:'.Support::regex().'|between:3,100',
                'mesto' => 'required|regex:'.Support::regex().'|between:2,100',
                'kontakt_osoba' => 'required|regex:'.AdminSupport::regex().'|max:255',
                'telefon' => 'required|regex:'.Support::regex().'|between:0,30',
                'mail' => 'required|email|max:50|unique:partner,mail,'.$partner_id.',partner_id',
                'pib' => 'required|numeric|digits_between:9,9',
                'broj_maticni' => 'numeric|digits_between:8,8',
                'telefon' => 'required|numeric',
                'login' => 'required',
                'password' => 'required'
               
            );
        $messages = array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Polje sadrži neodgovarajuće karaktere!',
                'email' => 'E-mail adresa je neodgovarajuća!',
                'between' => 'Dužina sadžaja nije odgovarajuća!',
                'max' => 'Dužina sadžaja je neodgovarajuća!',
                'numeric'=>'Polje može sadržati samo brojeve',
                'digits_between' => 'Dužina sadžaja nije odgovarajuća!'
            );

         $validator = Validator::make($inputs, $rules, $messages);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else{

        DB::table('partner')->where('partner_id',$partner_id)->update($inputs);
        AdminSupport::saveLog('B2B_PARTNER_IZMENI', array($partner_id));

        return Redirect::route('b2b.user_edit')->with('message','Uspešno ste sačuvali podatke!');;
        }
    }


    public function userKorisnici(){
        $partnerKorisnici = DB::table('partner_korisnik')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()));
        $partnerKorisnici = $partnerKorisnici->orderBy('naziv','asc')->paginate(20);

        $seo =array(
            'title'=>"Korsnici",
            'description'=>"Korsnici",
            'keywords'=>"korsnici"
        );

        return View::make('b2b.pages.parner_users',compact('seo','partnerKorisnici'));
    }

    public function userKorisnik($partner_korisnik_id){

        if($partner_korisnik_id > 0){
            $partnerKorisnik = DB::table('partner_korisnik')->where('partner_korisnik_id',$partner_korisnik_id)->first();
        }else{
            $partner = DB::table('partner')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
            $partnerKorisnik = (object) array(
                'partner_korisnik_id' => 0,
                'korisnicko_ime' => '',
                'lozinka' => '',
                'naziv' => '',
                'adresa' => $partner->adresa,
                'mesto' => $partner->mesto,
                'telefon' => $partner->telefon,
                'email' => $partner->mail,
                'aktivan' => 1
            );
        }

        $seo=array(
            "title"=>"Korsnik",
            "description"=>"Korsnik",
            "keywords"=>"korsnik",
        );


        return View::make('b2b.pages.partner_user',compact('seo','partnerKorisnik'));
    }
    public function userKorisnikEdit(){
        $inputs = Input::all();
        $inputs['partner_id'] = Session::get('b2b_user_'.B2bOptions::server());
        $partner_korisnik_id = $inputs['partner_korisnik_id'];

        $rules = array(
                'naziv' => 'required|regex:'.Support::regex().'|between:2,500',
                'adresa' => 'required|regex:'.Support::regex().'|between:3,100',
                'mesto' => 'required|regex:'.Support::regex().'|between:2,100',
                'telefon' => 'required|regex:'.Support::regex().'|between:5,30',
                'email' => 'required|email|max:50|unique:partner_korisnik,email,'.$partner_korisnik_id.',partner_korisnik_id',
                'korisnicko_ime' => 'required|between:2,255|unique:partner_korisnik,korisnicko_ime,'.$partner_korisnik_id.',partner_korisnik_id',
                'lozinka' => 'required|between:3,100'
               
            );
        $messages = array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Polje sadrži neodgovarajuće karaktere!',
                'email' => 'E-mail adresa je neodgovarajuća!',
                'between' => 'Dužina sadžaja nije odgovarajuća!',
                'max' => 'Dužina sadžaja je neodgovarajuća!',
                'numeric'=>'Polje može sadržati samo brojeve',
                'digits_between' => 'Dužina sadžaja nije odgovarajuća!',
                'unique' => 'Vrednost polja već postoji u bazi!'
            );

         $validator = Validator::make($inputs, $rules, $messages);
        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else{
            if($partner_korisnik_id > 0){
                DB::table('partner_korisnik')->where('partner_korisnik_id',$partner_korisnik_id)->update($inputs);
            }else{
                unset($inputs['partner_korisnik_id']);
                DB::table('partner_korisnik')->insert($inputs);
            }
            // AdminSupport::saveLog('B2B_PARTNER_IZMENI', array($partner_id));

            return Redirect::to('b2b/user/user/'.($partner_korisnik_id > 0 ? $partner_korisnik_id : DB::table('partner_korisnik')->max('partner_korisnik_id')))->with('message','Uspešno ste sačuvali podatke!');
        }
    }

    public function userKorisnikObrisi($partner_korisnik_id){
        $partnerKorisnik = DB::table('partner_korisnik')->where('partner_korisnik_id',$partner_korisnik_id)->first();
        if(!is_null($partnerKorisnik)){
            DB::table('partner_korisnik')->where('partner_korisnik_id',$partner_korisnik_id)->delete();
            return Redirect::to('b2b/user/users')->with('message','Uspešno ste obrisali korisnika!');            
        }
        return Redirect::back();
    }

}