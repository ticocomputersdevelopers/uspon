<?php

class B2bTipoviController extends Controller {

	public function tip($naziv,$grupa=null){

		$tipovi = DB::table('tip_artikla')->where('tip_artikla_id','!=',-1)->where(array('active'=>1))->get();

		foreach($tipovi as $type){
			if(B2bUrl::slugify($type->naziv) == $naziv){
				$tip = $type;
				break;
			}
		}

		if(!isset($tip)){
			return Redirect::to(Options::base_url());
		}
        $seo= array(
            "title"=>$tip->naziv,
            "description"=>$tip->naziv,
            "keywords"=>strtolower($tip->naziv)
        );

		$grupa_pr_id = null;
		if($grupa==null){
			$check_grupa = "";
		}else{
			foreach(DB::table('grupa_pr')->where('grupa_pr_id','!=',-1)->where('grupa_pr_id','!=',0)->where('web_b2b_prikazi',1)->get() as $gr){

		    $count = DB::select("SELECT DISTINCT COUNT(r.roba_id) FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id WHERE r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND r.grupa_pr_id = ".$gr->grupa_pr_id." AND r.tip_cene = ".$tip->tip_artikla_id."")[0]->count;
		    
				if(B2bUrl::slugify($gr->grupa) == $grupa && $count > 0){
					$grupa_pr_id = $gr->grupa_pr_id;
					break;
				}
			}
		}

        if(is_null($grupa_pr_id)){
			$query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('tip_cene',$tip->tip_artikla_id);
        }else{
            if(B2bCommon::broj_cerki($grupa_pr_id)>0)
            {	
                $grupa_pr_ids = array();
                B2bCommon::allGroups($grupa_pr_ids,$grupa_pr_id);
                $query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('tip_cene',$tip->tip_artikla_id)->whereIn('grupa_pr_id',$grupa_pr_ids);
            }
            else {
				$query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('tip_cene',$tip->tip_artikla_id)->where('grupa_pr_id',$grupa_pr_id);
            }
        }
        $query_products2=$query_products->count();

        if(Session::has('limit')){
            $limit=Session::get('limit');
        }
        else {
            $limit=20;
        }
        if(Session::has('order')){
            if(Session::get('order')=='price_asc')
            {
              $query_products=  $query_products->orderBy('racunska_cena_end','asc')->paginate($limit);
            }
            else if(Session::get('order')=='price_desc'){
              $query_products=  $query_products->orderBy('racunska_cena_end','desc')->paginate($limit);
            }
            else if(Session::get('order')=='news'){
              $query_products=  $query_products->orderBy('roba_id','desc')->paginate($limit);
            }
            else if(Session::get('order')=='name'){
              $query_products=  $query_products->orderBy('naziv_web','asc')->paginate($limit);
            }
        }
        else {

           $query_products= $query_products->orderBy('racunska_cena_end',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
        }

        $data=array(
            "strana"=>'tip',
            "tip"=>$tip->naziv,
            "tip_artikla_id"=>$tip->tip_artikla_id,
            "grupa_pr_id"=>$grupa_pr_id,
            "sub_cats" => !is_null($grupa_pr_id) ? B2bArticle::subGroups($grupa_pr_id) : array(),
            "grupa"=>$grupa,
            "seo"=>$seo,
            "query_products"=>$query_products,
            "count_products"=>$query_products2,
            "filters"=>array(),
            "filtersItems"=>array()
        );

        return View::make('b2b.pages.products',$data);		
	}
}