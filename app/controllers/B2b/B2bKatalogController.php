<?php

class B2bKatalogController extends Controller {
	
	public function katalog($katalog_id)
    {
        $katalog = DB::table('katalog')->where('katalog_id',$katalog_id)->whereRaw("(select count(katalog_polja_id) from katalog_polja where katalog_id=katalog.katalog_id) > 0")->first();
        if(is_null($katalog)){
            return Response::make('Forbidden', 403);
        }
        
        $katalog_vrsta_naziv = DB::table('katalog_vrsta')->where('katalog_vrsta_id',$katalog->katalog_vrsta_id)->pluck('naziv');
        $vrsta = 'grupe';
        if($katalog_vrsta_naziv == 'Grupe'){
        	$vrsta = 'grupe';
        }elseif($katalog_vrsta_naziv == 'Tipovi'){
        	$vrsta = 'tipovi';
        }elseif($katalog_vrsta_naziv == 'Karakteristike'){
            $vrsta = 'karakteristike';
        }

        $data = [
            'katalog' => $katalog,
            'katalog_polja' => DB::table('katalog_polja')->where('katalog_id',$katalog->katalog_id)->orderBy('rbr','asc')->get(),
            
        ];

        $pdf = App::make('dompdf');
        $pdf->loadView('shop/katalog/'.$vrsta, $data);
        return $pdf->stream();
    }	
}