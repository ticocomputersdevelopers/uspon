<?php

use Illuminate\Support\Facades\Response;
use Service\Mailer;

class B2bController extends Controller {
    public function __construct(){
        if(Session::has('b2b_user_'.B2bOptions::server())){
            $web_b2b_korpa_id = null;
            $currentCart = DB::table('web_b2b_korpa')->where(array('partner_id'=>Session::get('b2b_user_'.B2bOptions::server())))->orderBy('web_b2b_korpa_id','desc')->first();
            if(!is_null($currentCart) && $currentCart->naruceno == 0){
                $nowDate = new DateTime();
                $cartDate = new DateTime($currentCart->datum);
                if($nowDate > $cartDate){
                    $diff = $nowDate->diff($cartDate);
                    if($diff->y == 0 && $diff->m == 0 && $diff->d < 30){
                        $web_b2b_korpa_id = $currentCart->web_b2b_korpa_id;
                    }
                }
            }
            if(!is_null($web_b2b_korpa_id)){
                Session::put('b2b_cart',$web_b2b_korpa_id);
            }else{
                Session::forget('b2b_cart');
            }
        }
    }
 
    function index(){
        if(!B2bOptions::checkB2B()){
            return Redirect::to(B2bOptions::base_url());
        }
        if(Session::has('b2b_user_'.Options::server())){
            return Redirect::to(B2bOptions::base_url().'b2b/pocetna');
        }

        return Redirect::to(B2bOptions::base_url().'b2b/login');
    }

    function login(){
        if(Options::gnrl_options(3061) == 1 && !Session::has('b2c_admin'.Options::server())){
            return Redirect::to('/');
        }
        if(Session::has('b2b_user_'.Options::server())){
            return Redirect::to(B2bOptions::base_url().'b2b/pocetna');
        }
        $strana='login';
        $seo=array(
            "title"=>'Prijava',
            "description"=>'Prijava',
            "keywords"=>'prijava',

        );
        return View::make('b2b.pages.b2b-portal',compact('seo'));
    }
    public function logout(){
        if(B2bOptions::info_sys('wings')){
            Wings::logout(Session::get('wings_token_'.B2bOptions::server()));
            Session::forget('wings_token_'.B2bOptions::server());

        }

        AdminB2BSupport::saveB2BLog('PARTNER_B2B_LOGOUT', array(Session::get('b2b_user_'.B2bOptions::server())));
        Session::forget('b2b_user_'.B2bOptions::server());
        Session::forget('b2b_partner_user_'.B2bOptions::server());
        Session::forget('dokumenti_user_'.Options::server());
        Session::forget('dokumenti_user_kind_'.Options::server());
        Session::forget('dokumenti_ponuda_id'.Options::server());

        return Redirect::to(B2bOptions::base_url());
    }
    
    function loginStore(){
        $input = Input::all();

        //WINGS
        if(B2bOptions::info_sys('wings')){
            $validator = Validator::make($input,array('username' => 'required','password' => 'required'),array('required' => 'Niste popunili polje!'));
            if ($validator->fails()) {
                return Redirect::to('b2b/login')->withErrors($validator);
            }else{
                $result = Wings::autorize($input['username'],$input['password']);
         
                if($result == false || isset($result->errors)){
                    $validator->getMessageBag()->add('password', 'Uneli ste pogrešno korisničko ime ili lozinku!');
                    return Redirect::to('b2b/login')->withErrors($validator);  
                }else{
                    $token = $result->data[0]->attributes->token;
                    $result = Wings::kupac_info($token);

                    $id_is=$result->data[0]->id;
                    
                    $partner = DB::table('partner')->where('login',$input['username'])->where('password',$input['password'])->first();
                    if(is_null($partner)){
                        $data['login']=$input['username'];
                        $data['password']=$input['password'];
                        if(!is_null(DB::table('partner')->where('id_is',$id_is)->first())){
                            DB::table('partner')->where('id_is',$id_is)->update($data);
                        }else{
                            $wingsPartner = $result->data[0];
                            $sifra=isset($wingsPartner->attributes->sifra) ? $wingsPartner->attributes->sifra : null;
                            $naziv=isset($wingsPartner->attributes->naziv) ? $wingsPartner->attributes->naziv : null;
                            $mesto=isset($wingsPartner->attributes->adresa2) ? $wingsPartner->attributes->adresa2 : null;
                            $adresa=isset($wingsPartner->attributes->adresa1) ? $wingsPartner->attributes->adresa1 : null;
                            $telefon=isset($wingsPartner->attributes->telefon) ? $wingsPartner->attributes->telefon : null;
                            $pib=isset($wingsPartner->attributes->pib) ? $wingsPartner->attributes->pib : null;
                            $mail=isset($wingsPartner->attributes->mail) ? $wingsPartner->attributes->mail : null;
                            $rabat=isset($wingsPartner->attributes->rabat) ? $wingsPartner->attributes->rabat : 0;
                            $status=isset($wingsPartner->attributes->status) ? $wingsPartner->attributes->status : "K";
                            $data = array(
                                'id_is' => $id_is,
                                'drzava_id' => 0,
                                'sifra' => $sifra,
                                'naziv' => $naziv,
                                'mesto' => $mesto,
                                'adresa' => $adresa,
                                'telefon' => $telefon,
                                'pib' => $pib,
                                'rabat' => $rabat,
                                'stara_sifra' => $status,
                                'login' => $input['username'],
                                'password' => $input['password']
                                );
                            DB::table('partner')->insert($data);
                        }
                        
                        $partner = DB::table('partner')->where('login',$input['username'])->where('password',$input['password'])->first();
                    }

                    if(is_null($partner)){
                        $validator->getMessageBag()->add('password', 'Uneli ste pogrešno korisničko ime ili lozinku!');
                        return Redirect::to('b2b/login')->withErrors($validator); 
                    }else{
                        Session::put('wings_token_'.B2bOptions::server(),$token);
                        Session::put('b2b_user_'.B2bOptions::server(),$partner->partner_id);
                        Wings::updateKartica();
                        return Redirect::to('b2b'); 
                    }
                }               
            }
        }

        $validatorMessages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Uneli ste ne dozvoljene karaktere!',
            'between' => 'Vaš sadzaj polja je prekratak ili predugačak!',
            'exists' => 'Uneli ste pogrešno korisničko ime ili lozinku!',
        );
        $validatorPartner = Validator::make($input,array(
                'username' => 'required|between:3,50',
                'password' => 'required|between:3,50|exists:partner,password,login,'.$input['username'].''
            ),$validatorMessages);
        $validatorPartnerKorisnik = Validator::make($input,array(
                'username' => 'required|between:3,50',
                'password' => 'required|between:3,50|exists:partner_korisnik,lozinka,korisnicko_ime,'.$input['username'].',aktivan,1'
            ),$validatorMessages);

        if ($validatorPartner->fails() && $validatorPartnerKorisnik->fails()) {
            return Redirect::to('b2b/login')->withInput()->withErrors($validatorPartner);
        }else{
            if($validatorPartner->passes()){
                $partner = B2bPartner::where('login',$input['username'])->where('password',$input['password'])->first();
                Session::put('b2b_user_'.B2bOptions::server(), $partner->partner_id);

                if(B2bOptions::info_sys('infograf')){
                    if(!is_null($partner->id_is) && $partner->id_is != ''){
                        Session::put('b2b_user_'.B2bOptions::server().'_discounts', Infograf::discounts($partner->id_is));
                    }else{
                        Session::put('b2b_user_'.B2bOptions::server().'_discounts', array());
                    }
                }

                $dokumentiPartner = B2bPartner::join('partner_je','partner_je.partner_id','=','partner.partner_id')->whereIn('partner_vrsta_id',array(122))->where(array('login'=>$input['username'],'password'=>$input['password']))->first();
                if(!is_null($dokumentiPartner)){
                    Session::put('dokumenti_user_'.Options::server(),$partner->partner_id);
                    Session::put('dokumenti_user_kind_'.Options::server(),'saradnik');
                }
                            
                AdminB2BSupport::saveB2BLog('PARTNER_B2B_LOGIN', array(Session::get('b2b_user_'.AdminB2BOptions::server())));
            }elseif($validatorPartnerKorisnik->passes()){
                $partnerKorisnik = DB::table('partner_korisnik')->where('korisnicko_ime',$input['username'])->where('lozinka',$input['password'])->first();
                $partner = B2bPartner::where('partner_id',$partnerKorisnik->partner_id)->first();
                Session::put('b2b_user_'.B2bOptions::server(), $partner->partner_id);
                Session::put('b2b_partner_user_'.B2bOptions::server(), $partnerKorisnik->partner_korisnik_id);
                
                AdminB2BSupport::saveB2BLog('PARTNER_B2B_LOGIN', array(Session::get('b2b_user_'.AdminB2BOptions::server())));                
            }

            return Redirect::to('b2b/pocetna')->with('login_message',true);
        }

    }

    public function registration(){
        if(Options::gnrl_options(3061) == 1 && !Session::has('b2c_admin'.Options::server())){
            return Redirect::to('/');
        }
        if(Session::has('b2b_user_'.Options::server())){
            return Redirect::to(B2bOptions::base_url().'b2b/pocetna');
        }

        if(B2bOptions::info_sys('calculus')){
            return Redirect::to('b2b'); 
        }
        $strana='login';
        $seo=array(
            "title"=>'Registracija',
            "description"=>'Registracija',
            "keywords"=>'registracija',

        );

        return View::make('b2b.pages.registration',compact('seo'));

    }

    public function registrationStore(){
        $inputs = Input::all();

        $rules = array(
                'naziv' => 'required|regex:'.Support::regex().'|between:2,50',
                'adresa' => 'required|regex:'.Support::regex().'|between:3,100',
                'mesto' => 'required|regex:'.Support::regex().'|between:2,100',
                'kontakt_osoba' => 'required|regex:'.AdminSupport::regex().'|max:255',
                'telefon' => 'required|regex:'.Support::regex().'|between:0,30',
                'mail' => 'required|email|max:50|unique:partner,mail',
                'pib' => 'required|numeric|digits_between:9,9',
                'broj_maticni' => 'numeric|digits_between:8,8'
            );
        $messages = array(
                'required' => 'Niste popunili polje!',
                'regex' => 'Polje sadrži neodgovarajuće karaktere!',
                'email' => 'E-mail adresa je neodgovarajuća!',
                'between' => 'Dužina sadžaja nije odgovarajuća!',
                'unique' => 'E-mail adresa je zauzeta!',
                'max'=>'Dužina sadžaja je neodgovarajuća!',
                'numeric'=>'Polje može sadržati samo brojeve',
                'digits_between' => 'Dužina sadžaja nije odgovarajuća!',
                'captcha' => 'Dokazite da niste robot!'
            );

        $validator = Validator::make($inputs, $rules, $messages);
        if($validator->fails() || !Captcha::check()){
            if(!Captcha::check()){
                $validator->getMessageBag()->add('captcha', $messages['captcha']);
            }
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            if(false){
                $inputs['drzava_id']=0;
                DB::table('partner')->insert($inputs);
            }

            $body = '<h2>Zahtev za registraciju novog partnera na B2B portalu.</h2><br>';
            foreach($inputs as $key => $field){
                if($key != 'drzava_id'){
                    $body .= ($key == 'broj_maticni' ? 'Matični Broj' : $key).' : '.$field.'<br>';
                }
            }
            Mailer::send(B2bOptions::company_email(),B2bOptions::company_email(),'Registracija novog korisnika', $body);
            Mailer::send('veleprodaja@uspon.rs','veleprodaja@uspon.rs','Registracija novog korisnika', $body);
            
            return Redirect::to('b2b/registration')->with('registration_message',true);
        }

    }

    public function forgotPassword(){
        if(Options::gnrl_options(3061) == 1 && !Session::has('b2c_admin'.Options::server())){
            return Redirect::to('/');
        }
        $seo=array(
            "title"=>"Zaboravljena lozinka",
            "description"=>"Zaboravljena lozinka",
            "keywords"=>"zaboravljena, lozinka",
        );
        return View::make('b2b.pages.forgot_password',compact('seo'));
    }

    public function forgotPasswordPost(){
        $input = Input::get();

        $validator = Validator::make($input,array(
                'mail' => 'required|exists:partner,mail'
            ),
            array(
                'required' => 'Niste popunili polje!',
                'exists' => 'E-mail ne postoji u bazi!',
            )
        );

        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator);
        } else {
            B2bPartner::forgotPassword($input['mail']);
            return Redirect::back()->with('forgot_message',"Poruka sa novom lozinkom je poslata na ".$input['mail']);
        }

    }

    public function page($page){  
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $orgStrana = Request::segment(2+$offset);

        $strana = Url_mod::slug_convert_b2b_page($orgStrana);
        $web_b2b_seo_id = Seo::get_b2b_page_id($strana);
        $custom = 0;

        if($strana==""){
            $strana=All::get_page_start();
        }
        
        if(Seo::get_b2b_page_id($strana) != 0){
            $seo = Seo::b2b_page($strana);
            $naziv = str_replace(' - '.Options::company_name(),'',$seo->title);
            $title = $seo->title;
            $description = $seo->description;
            $keywords = $seo->keywords;
            $data=array(
                "strana"=>'strana',
                "org_strana" => 'orgStrana',
                "naziv"=>'naziv',
                "title"=>'title',
                "description"=>'description',
                "keywords"=>'keywords'
            );
        } else {
            if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
                return Redirect::to($new_link);
            }
            return App::make('ArticlesController')->artikli_first(array('grupa1' => Request::segment(1+$offset)));
        }

        if($page==B2bCommon::get_page_start()){
            $web_b2b_seo_id = 1;
            $strana=B2bCommon::get_page_start();
            $custom = 1;

            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),
            );
            $sekcije = DB::table('b2b_stranica_sekcija')->select('b2b_sekcija_stranice.*','b2b_sekcija_stranice_tip.naziv as tip_naziv')->where('web_b2b_seo_id',$web_b2b_seo_id)->join('b2b_sekcija_stranice','b2b_sekcija_stranice.b2b_sekcija_stranice_id','=','b2b_stranica_sekcija.b2b_sekcija_stranice_id')->join('b2b_sekcija_stranice_tip','b2b_sekcija_stranice_tip.b2b_sekcija_stranice_tip_id','=','b2b_sekcija_stranice.b2b_sekcija_stranice_tip_id')->where('b2b_sekcija_stranice.flag_aktivan', 1)->orderBy('b2b_stranica_sekcija.rbr','asc')->get();

            return View::make('b2b.pages.'.$strana,compact('seo',$data,'sekcije','strana','web_b2b_seo_id','custom'));
        }
        else if($page == B2bCommon::get_korpa()){
            $strana=B2bCommon::get_korpa();
            $items = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->orderBy('broj_stavke','asc')->get();
            $avans = Input::get('avans') ? (Input::get('avans') == 'true') : false;
            $nacin_isporuke_id = Input::get('nacin_isporuke_id') ? Input::get('nacin_isporuke_id') : DB::table('web_nacin_isporuke')->where('selected',1)->orderBy('web_nacin_isporuke_id','asc')->pluck('web_nacin_isporuke_id');
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.cart',compact('seo','custom','strana','items','avans','nacin_isporuke_id'));
        }
        else if($page == 'rma'){
            $strana= 'rma';
           
            $seo=array(
                "title"=>'Rma',
                "description"=>'Rma',
                "keywords"=>'rma',

            );
            return View::make('b2b.pages.servis',compact('seo','custom'));
        }
        else if($page == 'kontakt'){
            $strana= 'kontakt';
           
            $seo=array(
                "title"=>'Kontakt',
                "description"=>'Kontakt',
                "keywords"=>'kontakt',

            );
            return View::make('b2b.pages.contact',compact('seo','custom','strana'));
        }
        else if($page == 'katalozi'){
            $strana= 'katalozi';         
            $seo=array(
                "title"=>'Katalozi',
                "description"=>'Katalozi',
                "keywords"=>'katalozi',

            );
            $catalogs = DB::table('katalog')->select('katalog_id','naziv')->where(array('aktivan'=>1,'b2b_aktivno'=>1))->where('katalog_id','<>',-1)->whereRaw("(select count(katalog_polja_id) from katalog_polja where katalog_id=katalog.katalog_id) > 0")->where('katalog_id','<>',0)->orderBy('naziv','asc')->get();
            return View::make('b2b.pages.catalogs',compact('seo','custom','strana','catalogs'));
        }
        else if($page == 'bestseller'){
            $strana= 'bestseller';
            $seo=array(
                "title"=>'Najprodavaniji artikli',
                "description"=>'Najprodavaniji artikli',
                "keywords"=>'najprodavaniji, artikli'
            );

            $partner_id=Session::get('b2b_user_');
            $query_products = DB::table('web_b2b_prodata_roba')->select('roba_id')->where('partner_id',$partner_id);
            $count_products=$query_products->count();                    
            
            if(Session::has('limit')){
                $limit=Session::get('limit');
            }
            else {
                $limit=20;
            }
               $query_products= $query_products->orderBy('rbr','asc')->orderBy('broj','asc')->paginate($limit);

            return View::make('b2b.pages.articles',compact('seo','custom','strana','query_products','count_products'));
        }
        else if(in_array($page,array('akcija','price_diff'))){
            if($page == 'akcija'){
                $strana= 'akcija';
                $seo=array(
                    "title"=>'Akcija',
                    "description"=>'Akcija',
                    "keywords"=>'akcija'
                );
                $datum = date('Y-m-d');
                $query_products = DB::table('roba')->select('roba_id','web_cena')->whereRaw("flag_aktivan = 1 AND tip_cene NOT IN (select tip_artikla_id from web_b2b_seo where (menu_top=1 OR header_menu=1 OR footer=1) AND tip_artikla_id > 0 AND tip_artikla_id = tip_cene) AND flag_cenovnik = 1 AND b2b_akcija_flag_primeni = 1 AND (b2b_datum_akcije_od is NULL or b2b_datum_akcije_od <= '".$datum."' ) AND (b2b_datum_akcije_do is NULL OR b2b_datum_akcije_do >= '".$datum."') ");
            }else if($page == 'price_diff'){
                $strana= 'price_diff';
                $seo=array(
                    "title"=>'Promena cena',
                    "description"=>'Promena cena',
                    "keywords"=>'promena, cena'
                );
                $query_products = DB::table('roba')->select('roba.roba_id','web_cena')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('stara_cena','<>',0)->whereRaw('stara_cena <> racunska_cena_end');
            }
            $count_products=$query_products->count();                    
            
            if(Session::has('limit')){
                $limit=Session::get('limit');
            }
            else {
                $limit=20;
            }
            if(Session::has('order')){
                if(Session::get('order')=='price_asc'){
                    $query_products=  $query_products->orderBy('racunska_cena_end','asc')->paginate($limit);
                }
                else if(Session::get('order')=='price_desc'){
                  $query_products=  $query_products->orderBy('racunska_cena_end','desc')->paginate($limit);
                }
                else if(Session::get('order')=='news'){
                  $query_products=  $query_products->orderBy('roba_id','desc')->paginate($limit);
                }
                else if(Session::get('order')=='name'){
                  $query_products=  $query_products->orderBy('naziv_web','asc')->paginate($limit);
                }
            }else {

               $query_products= $query_products->orderBy('racunska_cena_end',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
            }
            return View::make('b2b.pages.articles',compact('seo','custom','strana','query_products','count_products'));
        }
        else {
            $content = DB::table('web_b2b_seo')->where('naziv_stranice',$page)->first();
            if(is_null($content)){
                return Redirect::to(B2bOptions::base_url().'b2b');
            }

            if($content->grupa_pr_id > 0){
                $redirect = B2bOptions::base_url() . 'b2b/artikli/' . B2bUrl::slugify(B2bCommon::getGrupa($content->grupa_pr_id));
                return Redirect::to($redirect);
            }
            else if(!is_null($content->tip_artikla_id) && $content->tip_artikla_id == 0){
                $redirect = B2bOptions::base_url() . 'b2b/akcija';
                return Redirect::to($redirect);
            }
            else if(!is_null($content->tip_artikla_id) && $content->tip_artikla_id > 0){
                $redirect = B2bOptions::base_url() . 'b2b/tip/' . B2bUrl::slugify(DB::table('tip_artikla')->where('tip_artikla_id',$content->tip_artikla_id)->pluck('naziv'));
                return Redirect::to($redirect);
            }

            $stranica_jezik=DB::table('web_b2b_seo_jezik')->where(array('web_b2b_seo_id'=>$content->web_b2b_seo_id, 'jezik_id'=>1))->first();
            $web_content = "";
            if(!is_null($stranica_jezik)){
                $web_content = $stranica_jezik->sadrzaj;
            }
            $custom = 1;
            $sekcije = DB::table('b2b_stranica_sekcija')->select('b2b_sekcija_stranice.*','b2b_sekcija_stranice_tip.naziv as tip_naziv')->where('web_b2b_seo_id',$web_b2b_seo_id)->join('b2b_sekcija_stranice','b2b_sekcija_stranice.b2b_sekcija_stranice_id','=','b2b_stranica_sekcija.b2b_sekcija_stranice_id')->join('b2b_sekcija_stranice_tip','b2b_sekcija_stranice_tip.b2b_sekcija_stranice_tip_id','=','b2b_sekcija_stranice.b2b_sekcija_stranice_tip_id')->where('b2b_sekcija_stranice.flag_aktivan', 1)->orderBy('b2b_stranica_sekcija.rbr','asc')->get();

            $seo=array(
                "title"=> !is_null($stranica_jezik) && $stranica_jezik->title ? $stranica_jezik->title : $page,
                "description"=> !is_null($stranica_jezik) && $stranica_jezik->description ? $stranica_jezik->description : $page,
                "keywords"=> !is_null($stranica_jezik) && $stranica_jezik->keywords ? $stranica_jezik->keywords : $page,
                "web_content"=>$web_content
            );
            return View::make('b2b.pages.pocetna',compact('seo','strana','page','content','web_content','sekcije','web_b2b_seo_id','custom'));

        }
        // return View::make('b2b.pages.'.$strana,compact('seo',$data,'sekcije'));

    }

    public function opisKvaraMail() {
       
       $partner = DB::table('partner')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->pluck('naziv');

        $input = Input::get();
        $naziv = Input::get('naziv');
        $datum = Input::get('datum');
        $brfakture = Input::get('brfakture');
        $serial = Input::get('serial');        
        $opis = Input::get('opis'); 

  

        $validator = Validator::make($input,array(
                'naziv' => 'required|between:3,50',
                'datum' => 'required',  
                'brfakture' => 'required',
                'serial' => 'required',                
                'opis' => 'required|between:3,50'
            )
        );
        if ($validator->fails()) {
            $messages = $validator->messages();

            return Redirect::to('b2b/servis')->withErrors($validator);

        } else {

            $body=" <p>Poštovani,<br /> Imate prijavu za servis<br />
            <b>Komitent:</b> ".$partner." <br />
            <b>Uređaj:</b> ".Input::get('naziv')." <br />
            <b>Datum kupovine:</b> ".Input::get('datum')." <br />
            <b>Broj fakture:</b> ".Input::get('brfakture')."<br />
            <b>Serijski broj:</b> ".Input::get('serial')."<br />
            <b>Opis kvara:</b> ".Input::get('opis')."<br />
            </p> <p>".Input::get('message')."</p>";

            

            Mailer::send(B2bOptions::company_email(),B2bOptions::company_email(),'Servis prijava', $body);

            return Redirect::to('b2b/servis')->withMessage('Uspešno ste poslali email.');
        }
 
    }

    public function servis(){       

        $data =array(
            "strana"=>'servis',
            'title'=>"Servis"
        );
        
        return View::make('b2b.pages.servis',$data);

    }
   
    public function brendovi(){       

        $brendovi = DB::table('proizvodjac')->where('proizvodjac_id','!=',-1)->where(array('brend_prikazi'=>1))->orderBy('rbr','asc')->orderBy('naziv','asc')->get();

        $data =array(
            "strana"=>'brendovi',
            'title'=>"brendovi",
            'brendovi'=>$brendovi
        );
        
        return View::make('b2b.pages.brendovi',$data);

    }

        public function kontakt(){       

        $data =array(
            "strana"=>'kontakt',
            'title'=>"kontakt"
        );
        
        return View::make('b2b.pages.contact',$data);

    }

    public function products_first($grupa1){
        $grupa_pr_id=B2bUrl::get_grupa_pr_id(array($grupa1));
        $partner_id=Session::get('b2b_user_'.B2bOptions::server());
        $filters=[];
        $filter_flag = 1;
        if($grupa_pr_id>0){
            AdminB2BSupport::saveB2BLog('PARTNER_B2B_GROUP_VIEW',array($grupa_pr_id));

            if(B2bCommon::broj_cerki($grupa_pr_id)>0)
            {   
                $grupa_pr_ids = array();
                B2bCommon::allGroups($grupa_pr_ids,$grupa_pr_id);

                $queryOsnovni=DB::table('roba')->select('roba.roba_id','proizvodjac_id','web_cena')->leftJoin('roba_grupe', 'roba.roba_id', '=', 'roba_grupe.roba_id')->leftJoin('lager', 'roba.roba_id', '=', 'lager.roba_id')->whereNotIn('proizvodjac_id',array_map('current',DB::table('nivo_pristupa_proizvodjac')->select('proizvodjac_id')->where('partner_id',$partner_id)->get()))->where('flag_aktivan',1)->where('kolicina','>',0)->where('orgj_id','!=',1)->where('flag_cenovnik',1)->where('racunska_cena_end','>',0)->groupBy('roba.roba_id')->where(function($q) use($grupa_pr_ids){
                    $q->whereIn('roba.grupa_pr_id',$grupa_pr_ids)->orWhereIn('roba_grupe.grupa_pr_id',$grupa_pr_ids);
                });
                if(Session::has('limit')){
                    $limit=Session::get('limit');
                }
                else {
                    $limit=20;
                }
                $query_products2=sizeof($queryOsnovni->get());

                if(Session::has('order')){
                    if(Session::get('order')=='price_asc')
                    {

                        $query_products=$queryOsnovni->orderBy('racunska_cena_end','asc')->paginate($limit);
                    }
                    else if(Session::get('order')=='price_desc'){
                        $query_products=$queryOsnovni->orderBy('racunska_cena_end','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='news'){
                        $query_products=$queryOsnovni->orderBy('roba_id','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='name'){
                        $query_products=$queryOsnovni->orderBy('naziv_web','asc')->paginate($limit);
                    }
                                     }
                else {

                    $query_products=$queryOsnovni->orderBy('racunska_cena_end',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
                }

                $filter_flag=0;

            }
            else {

                    foreach(Input::all() as $key => $row){
                        if($key != 'page'){
                            $filters[$key] = B2bCommon::get_filter_array(Input::get($key));
                        }
                    }
                    
                $grupa_pr_ids = array($grupa_pr_id);

                $query_products=DB::table('roba')->select('roba.roba_id','proizvodjac_id','web_cena')->leftJoin('roba_grupe', 'roba.roba_id', '=', 'roba_grupe.roba_id')->leftJoin('lager', 'roba.roba_id', '=', 'lager.roba_id')->whereNotIn('proizvodjac_id',array_map('current',DB::table('nivo_pristupa_proizvodjac')->select('proizvodjac_id')->where('partner_id',$partner_id)->get()))->where('flag_aktivan',1)->where('kolicina','>',0)->where('orgj_id','!=',1)->where('flag_cenovnik',1)->where('racunska_cena_end','>',0)->groupBy('roba.roba_id')->where(function($q) use($grupa_pr_ids){
                    $q->whereIn('roba.grupa_pr_id',$grupa_pr_ids)->orWhereIn('roba_grupe.grupa_pr_id',$grupa_pr_ids);
                    });
                if(isset($filters['proizvodjac'])){

                    $query_products=$query_products->whereIn('proizvodjac_id',$filters['proizvodjac']);
                }
                
                if((!isset($filters['proizvodjac']) and count($filters)>0) or (isset($filters['proizvodjac']) and count($filters)>1)) {
                   $br=0;
                    $filtersStr2 ="0";
                    $filtersStr ="";
                        foreach($filters as $key => $row){
                            if($key != 'proizvodjac'){
                            $filtersStr2 .=",".implode(',',$row);
                            $br++;
                        }
                        }

                    if($filtersStr2!=''){
                    $filtersStr = "grupa_pr_vrednost_id IN ( ".$filtersStr2.")";
                            $products = [0];
            $webRobaKarakteristike = DB::select("SELECT wrk1.roba_id,  (SELECT count(roba_id) FROM web_roba_karakteristike WHERE ".$filtersStr." AND roba_id=wrk1.roba_id)  as products  from web_roba_karakteristike wrk1
                                                WHERE wrk1.roba_id > 0 AND wrk1.roba_id in (SELECT roba_id from roba where wrk1.roba_id = roba.roba_id and flag_cenovnik = 1 and racunska_cena_end > 0 and flag_aktivan =1) AND roba_id in (SELECT roba_id FROM lager where wrk1.roba_id=lager.roba_id and kolicina > 0 AND orgj_id !=1 ) AND wrk1.".$filtersStr." GROUP BY wrk1.roba_id ",array());
            foreach($webRobaKarakteristike as $row){
                if($row->products == $br){
                    $products[]=$row->roba_id;
                }

            }


            $query_products=$query_products->whereIn('roba.roba_id',$products);

                        }
                    }

                    $query_products2=sizeof($query_products->get());
                    if(Session::has('limit')){
                        $limit=Session::get('limit');
                    }
                    else {
                        $limit=20;
                    }
                    if(Session::has('order')){
                        if(Session::get('order')=='price_asc')
                        {
                          $query_products=  $query_products->orderBy('racunska_cena_end','asc')->paginate($limit);
                        }
                        else if(Session::get('order')=='price_desc'){
                          $query_products=  $query_products->orderBy('racunska_cena_end','desc')->paginate($limit);
                        }
                        else if(Session::get('order')=='news'){
                          $query_products=  $query_products->orderBy('roba_id','desc')->paginate($limit);
                        }
                        else if(Session::get('order')=='name'){
                          $query_products=  $query_products->orderBy('naziv_web','asc')->paginate($limit);
                        }
                    }
                    else {

                       $query_products= $query_products->orderBy('racunska_cena_end',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
                    }



                }

                $seo= array(
                    "title"=>B2bCommon::grupa_title($grupa_pr_id),
                    "description"=>B2bCommon::grupa_description($grupa_pr_id),
                    "keywords"=>B2bCommon::grupa_keywods($grupa_pr_id)
                );

                $data=array(
                    "strana"=>'artikli',
                    "grupa_pr_id"=>$grupa_pr_id,
                    "sub_cats" => B2bArticle::subGroups($grupa_pr_id),
                    "url"=>$grupa1,
                    "seo"=>$seo,
                    "query_products"=>$query_products,
                    "count_products"=>$query_products2,
                    "filters"=>$filters,
                    "filter_flag"=>$filter_flag,
                    "filtersItems"=>B2b::getCategoryFilters($grupa_pr_id, $filters)
                );

                return View::make('b2b.pages.products',$data);


        }
        else {
            $strana=B2bCommon::get_page_start();
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.pocetna',compact('seo'));
        }
    }

    public function products_second($grupa1,$grupa2){

        $grupa_pr_id=B2bUrl::get_grupa_pr_id(array($grupa1,$grupa2));
        $filters=[];
        $filter_flag = 1;
        $partner_id=Session::get('b2b_user_'.B2bOptions::server());

        if($grupa_pr_id>0){
            AdminB2BSupport::saveB2BLog('PARTNER_B2B_GROUP_VIEW',array($grupa_pr_id));

            if(B2bCommon::broj_cerki($grupa_pr_id)>0)
            {   
                $grupa_pr_ids = array();
                B2bCommon::allGroups($grupa_pr_ids,$grupa_pr_id);

                $queryOsnovni=DB::table('roba')->select('roba.roba_id','proizvodjac_id','web_cena')->leftJoin('roba_grupe', 'roba.roba_id', '=', 'roba_grupe.roba_id')->leftJoin('lager', 'roba.roba_id', '=', 'lager.roba_id')->whereNotIn('proizvodjac_id',array_map('current',DB::table('nivo_pristupa_proizvodjac')->select('proizvodjac_id')->where('partner_id',$partner_id)->get()))->where('flag_aktivan',1)->where('kolicina','>',0)->where('orgj_id','!=',1)->whereNotNull('kolicina')->where('racunska_cena_end','>',0)->where('flag_cenovnik',1)->groupBy('roba.roba_id')->where(function($q) use($grupa_pr_ids){
                    $q->whereIn('roba.grupa_pr_id',$grupa_pr_ids)->orWhereIn('roba_grupe.grupa_pr_id',$grupa_pr_ids);
                });
                if(Session::has('limit')){
                    $limit=Session::get('limit');
                }
                else {
                    $limit=20;
                }

                $query_products2=sizeof($queryOsnovni->get());


                if(Session::has('order')){
                    if(Session::get('order')=='price_asc')
                    {
                        $query_products=$queryOsnovni->orderBy('racunska_cena_end','asc')->paginate($limit);
                    }
                    else if(Session::get('order')=='price_desc'){
                        $query_products=$queryOsnovni->orderBy('racunska_cena_end','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='news'){
                        $query_products=$queryOsnovni->orderBy('roba_id','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='name'){
                        $query_products=$queryOsnovni->orderBy('naziv_web','asc')->paginate($limit);
                    }
                }
                else {

                    $query_products=$queryOsnovni->orderBy('racunska_cena_end',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
                }

                $filter_flag=0;

            }

            else {
 
                foreach(Input::all() as $key => $row){
                    if($key != 'page'){
                        $filters[$key] = B2bCommon::get_filter_array(Input::get($key));
                    }
                }

                $grupa_pr_ids = array($grupa_pr_id);

                $query_products=DB::table('roba')->select('roba.roba_id','proizvodjac_id','web_cena')->leftJoin('roba_grupe', 'roba.roba_id', '=', 'roba_grupe.roba_id')->leftJoin('lager', 'roba.roba_id', '=', 'lager.roba_id')->whereNotIn('proizvodjac_id',array_map('current',DB::table('nivo_pristupa_proizvodjac')->select('proizvodjac_id')->where('partner_id',$partner_id)->get()))->where('flag_aktivan',1)->where('kolicina','>',0)->where('orgj_id','!=',1)->where('flag_cenovnik',1)->where('racunska_cena_end','>',0)->groupBy('roba.roba_id')->where(function($q) use($grupa_pr_ids){
                    $q->whereIn('roba.grupa_pr_id',$grupa_pr_ids)->orWhereIn('roba_grupe.grupa_pr_id',$grupa_pr_ids);
                    });
                if(isset($filters['proizvodjac'])){

                    $query_products=$query_products->whereIn('proizvodjac_id',$filters['proizvodjac']);
                }

                if((!isset($filters['proizvodjac']) and count($filters)>0) or (isset($filters['proizvodjac']) and count($filters)>1)) {
                    $br=0;
                    $filtersStr2 ="0";
                    $filtersStr ="";
                    foreach($filters as $key => $row){
                        if($key != 'proizvodjac'){
                            $filtersStr2 .=",".implode(',',$row);
                            $br++;
                        }
                    }

                    if($filtersStr2!=''){
                        $filtersStr = "grupa_pr_vrednost_id IN ( ".$filtersStr2.")";
                        $products = [0];
                        $webRobaKarakteristike = DB::select("SELECT wrk1.roba_id,  (SELECT count(roba_id) FROM web_roba_karakteristike WHERE ".$filtersStr." AND roba_id=wrk1.roba_id)  as products  from web_roba_karakteristike wrk1
                                                WHERE wrk1.roba_id > 0 AND wrk1.roba_id IN (SELECT roba_id FROM lager WHERE lager.roba_id = wrk1.roba_id AND kolicina > 0) AND wrk1.".$filtersStr." GROUP BY wrk1.roba_id ",array());
                        foreach($webRobaKarakteristike as $row){
                            if($row->products == $br){
                                $products[]=$row->roba_id;
                            }

                        }


                        $query_products=$query_products->whereIn('roba.roba_id',$products);

                    }
                }


                $query_products2=sizeof($query_products->get());
                if(Session::has('limit')){
                    $limit=Session::get('limit');
                }
                else {
                    $limit=20;
                }
                if(Session::has('order')){
                    if(Session::get('order')=='price_asc')
                    {
                        $query_products=  $query_products->orderBy('racunska_cena_end','asc')->paginate($limit);
                    }
                    else if(Session::get('order')=='price_desc'){
                        $query_products=  $query_products->orderBy('racunska_cena_end','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='news'){
                        $query_products=  $query_products->orderBy('roba_id','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='name'){
                        $query_products=  $query_products->orderBy('naziv_web','asc')->paginate($limit);
                    }
                }
                else {

                    $query_products= $query_products->orderBy('racunska_cena_end',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
                }



            }
            $seo= array(
                "title"=>B2bCommon::grupa_title($grupa_pr_id),
                "description"=>B2bCommon::grupa_description($grupa_pr_id),
                "keywords"=>B2bCommon::grupa_keywods($grupa_pr_id)
            );

            $data=array(
                "strana"=>'artikli',
                "grupa_pr_id"=>$grupa_pr_id,
                "sub_cats" => B2bArticle::subGroups($grupa_pr_id),
                "url"=>$grupa1."/".$grupa2,
                "seo"=>$seo,
                "query_products"=>$query_products,
                "count_products"=>$query_products2,
                "filters"=>$filters,
                "filter_flag"=>$filter_flag,
                "filtersItems"=>B2b::getCategoryFilters($grupa_pr_id, $filters)
            );


            return View::make('b2b.pages.products',$data);
        }
        else {
            $strana=B2bCommon::get_page_start();
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.pocetna',compact('seo'));
        }


    }

    public function products_third($grupa1,$grupa2,$grupa3){

        $grupa_pr_id=B2bUrl::get_grupa_pr_id(array($grupa1,$grupa2,$grupa3));
        $partner_id=Session::get('b2b_user_'.B2bOptions::server());
        $filters = [];
        $filter_flag = 1;
        if($grupa_pr_id>0){
            AdminB2BSupport::saveB2BLog('PARTNER_B2B_GROUP_VIEW',array($grupa_pr_id));

                foreach(Input::all() as $key => $row){
                    if($key != 'page'){
                        $filters[$key] = B2bCommon::get_filter_array(Input::get($key));
                    }
                }

                $grupa_pr_ids = array();
                B2bCommon::allGroups($grupa_pr_ids,$grupa_pr_id);

                $query_products=DB::table('roba')->select('roba.roba_id','proizvodjac_id','web_cena')->leftJoin('roba_grupe', 'roba.roba_id', '=', 'roba_grupe.roba_id')->leftJoin('lager', 'roba.roba_id', '=', 'lager.roba_id')->whereNotIn('proizvodjac_id',array_map('current',DB::table('nivo_pristupa_proizvodjac')->select('proizvodjac_id')->where('partner_id',$partner_id)->get()))->where('flag_aktivan',1)->where('kolicina','>',0)->where('orgj_id','!=',1)->whereNotNull('kolicina')->where('flag_cenovnik',1)->where('racunska_cena_end','>',0)->groupBy('roba.roba_id')->where(function($q) use($grupa_pr_ids){
                    $q->whereIn('roba.grupa_pr_id',$grupa_pr_ids)->orWhereIn('roba_grupe.grupa_pr_id',$grupa_pr_ids);
                });

                if(isset($filters['proizvodjac'])){

                    $query_products=$query_products->whereIn('proizvodjac_id',$filters['proizvodjac']);
                }

                if((!isset($filters['proizvodjac']) and count($filters)>0) or (isset($filters['proizvodjac']) and count($filters)>1)) {
                    $br=0;
                    $filtersStr2 ="0";
                    $filtersStr ="";
                    foreach($filters as $key => $row){
                        if($key != 'proizvodjac'){
                            $filtersStr2 .=",".implode(',',$row);
                            $br++;
                        }
                    }

                    if($filtersStr2!=''){
                        $filtersStr = "grupa_pr_vrednost_id IN ( ".$filtersStr2.")";
                        $products = [0];
                        $webRobaKarakteristike = DB::select("SELECT wrk1.roba_id,  (SELECT count(roba_id) FROM web_roba_karakteristike WHERE ".$filtersStr." AND roba_id=wrk1.roba_id)  as products  from web_roba_karakteristike wrk1
                                                WHERE wrk1.roba_id > 0 AND wrk1.roba_id in (SELECT roba_id from roba where wrk1.roba_id = roba.roba_id and flag_cenovnik = 1 and racunska_cena_end > 0 and flag_aktivan =1) AND roba_id in (SELECT roba_id FROM lager where wrk1.roba_id=lager.roba_id and kolicina > 0 AND orgj_id !=1 ) AND wrk1.".$filtersStr." GROUP BY wrk1.roba_id ",array());
                        foreach($webRobaKarakteristike as $row){
                            if($row->products == $br){
                                $products[]=$row->roba_id;
                            }

                        }


                        $query_products=$query_products->whereIn('roba.roba_id',$products);

                    }
                }


                $query_products2=sizeof($query_products->get());
                if(Session::has('limit')){
                    $limit=Session::get('limit');
                }
                else {
                    $limit=20;
                }
                if(Session::has('order')){
                    if(Session::get('order')=='price_asc')
                    {
                        $query_products=  $query_products->orderBy('racunska_cena_end','asc')->paginate($limit);
                    }
                    else if(Session::get('order')=='price_desc'){
                        $query_products=  $query_products->orderBy('racunska_cena_end','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='news'){
                        $query_products=  $query_products->orderBy('roba_id','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='name'){
                        $query_products=  $query_products->orderBy('naziv_web','asc')->paginate($limit);
                    }
                }
                else {

                    $query_products= $query_products->orderBy('racunska_cena_end',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
                }


            $seo= array(
                "title"=>B2bCommon::grupa_title($grupa_pr_id),
                "description"=>B2bCommon::grupa_description($grupa_pr_id),
                "keywords"=>B2bCommon::grupa_keywods($grupa_pr_id)
            );

            $data=array(
                "strana"=>'artikli',
                "grupa_pr_id"=>$grupa_pr_id,
                "sub_cats" => B2bArticle::subGroups($grupa_pr_id),
                "url"=>$grupa1."/".$grupa2."/".$grupa3,
                "seo"=>$seo,
                "query_products"=>$query_products,
                "count_products"=>$query_products2,
                "filters"=>$filters,
                "filter_flag"=>$filter_flag,
                "filtersItems"=>B2b::getCategoryFilters($grupa_pr_id, $filters)
            );


            return View::make('b2b.pages.products',$data);
            }


        else {
            $strana=B2bCommon::get_page_start();
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.pocetna',compact('seo'));
        }
    }

    public function products_fourth($grupa1,$grupa2,$grupa3,$grupa4){

        $grupa_pr_id=B2bUrl::get_grupa_pr_id(array($grupa1,$grupa2,$grupa3,$grupa4));
        $partner_id=Session::get('b2b_user_'.B2bOptions::server());
        $filters = [];
        $filter_flag = 1;
        if($grupa_pr_id>0){
                AdminB2BSupport::saveB2BLog('PARTNER_B2B_GROUP_VIEW',array($grupa_pr_id));

                foreach(Input::all() as $key => $row){
                    if($key != 'page'){
                        $filters[$key] = B2bCommon::get_filter_array(Input::get($key));
                    }
                }

                $grupa_pr_ids = array();
                B2bCommon::allGroups($grupa_pr_ids,$grupa_pr_id);

                $query_products=DB::table('roba')->select('roba.roba_id','proizvodjac_id','web_cena')->leftJoin('roba_grupe', 'roba.roba_id', '=', 'roba_grupe.roba_id')->leftJoin('lager', 'roba.roba_id', '=', 'lager.roba_id')->whereNotIn('proizvodjac_id',array_map('current',DB::table('nivo_pristupa_proizvodjac')->select('proizvodjac_id')->where('partner_id',$partner_id)->get()))->where('flag_aktivan',1)->where('kolicina','>',0)->where('orgj_id','!=',1)->whereNotNull('kolicina')->where('flag_cenovnik',1)->where('racunska_cena_end','>',0)->groupBy('roba.roba_id')->where(function($q) use($grupa_pr_ids){
                    $q->whereIn('roba.grupa_pr_id',$grupa_pr_ids)->orWhereIn('roba_grupe.grupa_pr_id',$grupa_pr_ids);
                });

                if(isset($filters['proizvodjac'])){

                    $query_products=$query_products->whereIn('proizvodjac_id',$filters['proizvodjac']);
                }

                if((!isset($filters['proizvodjac']) and count($filters)>0) or (isset($filters['proizvodjac']) and count($filters)>1)) {
                    $br=0;
                    $filtersStr2 ="0";
                    $filtersStr ="";
                    foreach($filters as $key => $row){
                        if($key != 'proizvodjac'){
                            $filtersStr2 .=",".implode(',',$row);
                            $br++;
                        }
                    }

                    if($filtersStr2!=''){
                        $filtersStr = "grupa_pr_vrednost_id IN ( ".$filtersStr2.")";
                        $products = [0];
                        $webRobaKarakteristike = DB::select("SELECT wrk1.roba_id,  (SELECT count(roba_id) FROM web_roba_karakteristike WHERE ".$filtersStr." AND roba_id=wrk1.roba_id)  as products from web_roba_karakteristike wrk1
                                                WHERE wrk1.roba_id > 0 AND wrk1.roba_id in (SELECT roba_id from roba where wrk1.roba_id = roba.roba_id and flag_cenovnik = 1 and racunska_cena_end > 0 and flag_aktivan =1) AND roba_id in (SELECT roba_id FROM lager where wrk1.roba_id=lager.roba_id and kolicina > 0 AND orgj_id !=1 ) AND wrk1.".$filtersStr." GROUP BY wrk1.roba_id ",array());
                        foreach($webRobaKarakteristike as $row){
                            if($row->products == $br){
                                $products[]=$row->roba_id;
                            }

                        }


                        $query_products=$query_products->whereIn('roba.roba_id',$products);

                    }
                }


                $query_products2=sizeof($query_products->get());
                if(Session::has('limit')){
                    $limit=Session::get('limit');
                }
                else {
                    $limit=20;
                }
                if(Session::has('order')){
                    if(Session::get('order')=='price_asc')
                    {
                        $query_products=  $query_products->orderBy('racunska_cena_end','asc')->paginate($limit);
                    }
                    else if(Session::get('order')=='price_desc'){
                        $query_products=  $query_products->orderBy('racunska_cena_end','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='news'){
                        $query_products=  $query_products->orderBy('roba_id','desc')->paginate($limit);
                    }
                    else if(Session::get('order')=='name'){
                        $query_products=  $query_products->orderBy('naziv_web','asc')->paginate($limit);
                    }
                }
                else {

                    $query_products= $query_products->orderBy('racunska_cena_end',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
                }


            $seo= array(
                "title"=>B2bCommon::grupa_title($grupa_pr_id),
                "description"=>B2bCommon::grupa_description($grupa_pr_id),
                "keywords"=>B2bCommon::grupa_keywods($grupa_pr_id)
            );

            $data=array(
                "strana"=>'artikli',
                "grupa_pr_id"=>$grupa_pr_id,
                "seo"=>$seo,
                "sub_cats" => B2bArticle::subGroups($grupa_pr_id),
                "url"=>$grupa1."/".$grupa2."/".$grupa3."/".$grupa4,
                "query_products"=>$query_products,
                "count_products"=>$query_products2,
                "filters"=>$filters,
                "filter_flag"=>$filter_flag,
                "filtersItems"=>B2b::getCategoryFilters($grupa_pr_id, $filters)
            );


            return View::make('b2b.pages.products',$data);
            }


        else {
            $strana=B2bCommon::get_page_start();
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.pocetna',compact('seo'));
        }
    }

    public function search(){
        $org_search = urldecode(Input::get('q'));
        $grupa_pr_id = Input::get('search_grupa_pr_id');
        $partner_id=Session::get('b2b_user_'.B2bOptions::server());
        $search = pg_escape_string(strtolower(urldecode($org_search)));

        $a = array('љ','њ','е','р','т','з','у','и','о','п','ш','ђ','а','с','д','ф','г','х','ј','к','л','ч','ћ','ж','ѕ','џ','ц','в','б','н','м', 'Љ','Њ','Е','Р','Т','З','У','И','О','П','Ш','Ђ','А','С','Д','Ф','Г','Х','Ј','К','Л','Ч','Ћ','Ж','Ѕ','Џ','Ц','В','Б','Н','М'); 
        $b = array('lj', 'nj', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'š', 'đ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'č', 'ć', 'ž', 'y', 'dž', 'c', 'v', 'b', 'n', 'm', 'lj', 'nj', 'e', 'r', 't', 'z', 'u', 'i', 'o', 'p', 'š', 'đ', 'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'č', 'ć', 'ž', 'y', 'dž', 'c', 'v', 'b', 'n', 'm'); 

        $search = str_replace($a, $b, $search); 

        if(Session::has('limit')){
            $limit=Session::get('limit');
        }
        else {
            $limit=20;
        }

        $reci = explode(" ", $search);
        
            $nazivFormatiran0 = '';
        // Za svaku pretrazenu rec dodaje deo query-ja
        foreach ($reci as $rec) {
            $nazivFormatiran0 .= "AND naziv_web ILIKE '%" . $rec . "%' ";
            // Brise AND samo na pocetku querija, a na ostalim recim ostaje
            $nazivFormatiran = substr($nazivFormatiran0, 4);
        }

        $naziv_char_translate0 = '';
        // Za svaku pretrazenu rec dodaje deo query-ja
        foreach ($reci as $rec) {
            $naziv_char_translate0 .= "AND translate (naziv_web, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
            // Brise AND samo na pocetku querija, a na ostalim recim ostaje
            $naziv_char_translate = substr($naziv_char_translate0, 4);
        }

        $tagsFormatiran0 = '';
        foreach ($reci as $rec) {
            $tagsFormatiran0 .= "AND tags ILIKE '%" . $rec . "%' ";
            $tagsFormatiran = substr($tagsFormatiran0, 4);
        }

        $tags_char_translate0 = '';
        foreach ($reci as $rec) {
            $tags_char_translate0 .= "AND translate (tags, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
            $tags_char_translate = substr($tags_char_translate0, 4);
        }

        $opisFormatiran0 = '';
        foreach ($reci as $rec) {
            $opisFormatiran0 .= "AND web_opis ILIKE '%" . $rec . "%' ";
            $opisFormatiran = substr($opisFormatiran0, 4);
        }

        $opis_char_translate0 = '';
        foreach ($reci as $rec) {
            $opis_char_translate0 .= "AND translate (web_opis, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
            $opis_char_translate = substr($opis_char_translate0, 4);
        }

        $karakFormatiran0 = '';
        foreach ($reci as $rec) {
            $karakFormatiran0 .= "AND karakteristika_naziv ILIKE '%" . $rec . "%' ";
            $karakFormatiran = substr($karakFormatiran0, 4);
        }

        $karak_char_translate0 = '';
        foreach ($reci as $rec) {
            $karak_char_translate0 .= "AND translate (karakteristika_naziv, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
            $karak_char_translate = substr($karak_char_translate0, 4);
        }

        $karakVrednostFormatiran0 = '';
        foreach ($reci as $rec) {
            $karakVrednostFormatiran0 .= "AND karakteristika_vrednost ILIKE '%" . $rec . "%' ";
            $karakVrednostFormatiran = substr($karakVrednostFormatiran0, 4);
        }

        $karakVrednost_char_translate0 = '';
        foreach ($reci as $rec) {
            $karakVrednost_char_translate0 .= "AND translate (karakteristika_vrednost, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
            $karakVrednost_char_translate = substr($karakVrednost_char_translate0, 4);
        }

        $dopunski_nazivFormatiran0 = '';
        foreach ($reci as $rec) {
            $dopunski_nazivFormatiran0 .= "AND naziv_dopunski ILIKE '%" . $rec . "%' ";
            $dopunski_nazivFormatiran = substr($dopunski_nazivFormatiran0, 4);
        }

        $dopunski_naziv_char_translate0 = '';
        foreach ($reci as $rec) {
            $dopunski_naziv_char_translate0 .= "AND translate (naziv_dopunski, 'ć,č,š,đ,ž,Ć,Č,Š,Đ,Ž', 'c,c,s,d,z,C,C,S,D,Z') ILIKE '%" . $rec . "%' ";
            $dopunski_naziv_char_translate = substr($dopunski_naziv_char_translate0, 4);
        }

        $sifraisFormatiran0 = '';
            foreach ($reci as $rec) {
                $sifraisFormatiran0 .= "AND sifra_is ILIKE '%" . $rec . "%' ";
                $sifraisFormatiran = substr($sifraisFormatiran0, 4);
        }

        $idisFormatiran0 = '';
            foreach ($reci as $rec) {
                $idisFormatiran0 .= "AND id_is ILIKE '%" . $rec . "%' ";
                $idisFormatiran = substr($idisFormatiran0, 4);
        }

        $SKUFormatiran0 = '';
        foreach ($reci as $rec) {
            $SKUFormatiran0 .= "AND sku ILIKE '%" . $rec . "%' ";
            $SKUFormatiran = substr($SKUFormatiran0, 4);
        }

        $EANFormatiran0 = '';
        foreach ($reci as $rec) {
            $EANFormatiran0 .= "AND barkod ILIKE '%" . $rec . "%' ";
            $EANFormatiran = substr($EANFormatiran0, 4);
        }

        $sifradFormatiran0 = '';
        foreach ($reci as $rec) {
            $sifradFormatiran0 .= "AND sifra_d ILIKE '%" . $rec . "%' ";
            $sifradFormatiran = substr($sifradFormatiran0, 4);
        }

        $proizvodjacFormatiran0 = '';
        foreach ($reci as $rec) {
            $proizvodjacFormatiran0 .= "AND proizvodjac_id= (select proizvodjac_id FROM proizvodjac where naziv ILIKE '%" . $rec . "%' LIMIT 1 )";
            $proizvodjacFormatiran = substr($proizvodjacFormatiran0, 4);
        }

        $grupa_pr_ids=array();
        $grupe_query = "";
        if($grupa_pr_id){
            Groups::allGroups($grupa_pr_ids,$grupa_pr_id);
            $grupe_query = "grupa_pr_id IN (".implode(",",$grupa_pr_ids).") AND ";
        }


        //NOVI QUERY SA KARAKTERISTIKAMA
        $query = DB::table('roba')
                ->selectRaw("roba_id, naziv_web, web_cena, proizvodjac_id")                               
                ->whereNotIn('proizvodjac_id',array_map('current',DB::table('nivo_pristupa_proizvodjac')->select('proizvodjac_id')->where('partner_id',$partner_id)->get()))
                ->whereRaw("flag_aktivan=1 AND flag_cenovnik=1  AND racunska_cena_end > 0 AND ".$grupe_query."(" . $nazivFormatiran . " OR " . $naziv_char_translate . " OR " . $tagsFormatiran . " OR " . $tags_char_translate . " OR " . $opisFormatiran . " OR " . $proizvodjacFormatiran . " OR " . $opis_char_translate . " OR " . $dopunski_nazivFormatiran . " OR " . $dopunski_naziv_char_translate . " OR " . $sifraisFormatiran . " OR " .$idisFormatiran. " OR " .$SKUFormatiran. " OR " .$EANFormatiran. " OR " .$sifradFormatiran. ") AND roba_id IN (SELECT roba_id FROM lager WHERE roba.roba_id=lager.roba_id and kolicina > 0 and orgj_id !=1 )");
                 
        $count_products = count($query->get());

        if(Session::has('order')){
            if(Session::get('order')=='price_asc')
            {
                $query_products= $query->orderBy("racunska_cena_end","asc")->paginate($limit);
            }
            else if(Session::get('order')=='price_desc'){
                $query_products=$query->orderBy("racunska_cena_end","desc")->paginate($limit);
            }
            else if(Session::get('order')=='news'){
                $query_products=$query->orderBy("roba_id","desc")->paginate($limit);
            }
            else if(Session::get('order')=='name'){
                $query_products=$query->orderBy("naziv_web","desc")->paginate($limit);
            }
        }
        else {
            $query_products = $query->orderBy('racunska_cena_end',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
        }       

        $seo = array(
            "title"=>"Pretraga",
            "description"=>"",
            "keywords"=>"");
        $data=array(
            "title"=>"Pretraga",
            "description"=>"",
            "keywords"=>"",
            "strana"=>'pretraga',
            "seo"=>$seo,
            "query_products"=>$query_products,
            "count_products"=>$count_products,
            "filter_flag"=>0,
        );
        
        return View::make('b2b.pages.products',$data);    
    
    }

    public function productsType($type,$page){
        $queryOsnovni=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('tip_cene',$type);
        $query_products2=DB::table('roba')->where('flag_aktivan',1)->where('flag_cenovnik',1)->where('tip_cene',$type)->count();
        $query_products=$queryOsnovni->get();
        
        if(Session::has('limit')){
            $limit=Session::get('limit');
        }
        else {
            $limit=20;
        }
        $query_products2=$queryOsnovni->remember(5)->count();

        if(Session::has('order')){
            if(Session::get('order')=='price_asc')
            {
                $query_products=$queryOsnovni->orderBy('racunska_cena_end','asc')->paginate($limit);
            }
            else if(Session::get('order')=='price_desc'){
                $query_products=$queryOsnovni->orderBy('racunska_cena_end','desc')->paginate($limit);
            }
            else if(Session::get('order')=='news'){
                $query_products=$queryOsnovni->orderBy('roba_id','desc')->paginate($limit);
            }
            else if(Session::get('order')=='name'){
                $query_products=$queryOsnovni->orderBy('naziv_web','asc')->paginate($limit);
            }
        }
        else {

            $query_products=$queryOsnovni->orderBy('racunska_cena_end',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
        }
        $seo = [
            "title"=>B2bCommon::seo_title($page),
            "description"=>B2bCommon::seo_description($page),
            "keywords"=>B2bCommon::seo_keywords($page),
        ];
        $data=array(
            "strana"=>$page,
            "seo"=>$seo,
            "query_products"=>$query_products,
            "count_products"=>$query_products2,
        );
        return View::make('b2b.pages.products',$data);
    }

    public function limit($limit)
    {
        Session::put('limit',$limit);
        $pos = strpos(URL::previous(),'?');
        if($pos==0){
            return Redirect::back();
        }else{
            return Redirect::to(substr(URL::previous(),0,$pos));
        }
    }

    public function product($artikal){

        $roba_id=B2bArticle::get_product_id($artikal);
        
        if($roba_id>0){
            AdminB2BSupport::saveB2BLog('PARTNER_B2B_PRODUCT_VIEW',array($roba_id));
            
            $seo = array(
                "title"=>B2bArticle::seo_title($roba_id),
                "description"=>B2bArticle::seo_description($roba_id),
                "keywords"=>B2bArticle::seo_keywords($roba_id)
            );
            $data=array(
                "strana"=>'artikal',
                "seo"=>$seo,
                "roba_id"=>$roba_id,
                "vezani_artikli"=>DB::table('vezani_artikli')->where('roba_id',$roba_id)->orderBy('vezani_roba_id','asc')->get(),
                "fajlovi"=>DB::select("SELECT * FROM web_files WHERE roba_id = ".$roba_id." ORDER BY vrsta_fajla_id ASC")

            );
            return View::make('b2b.pages.product',$data);
        }
        else {
            $strana=B2bCommon::get_page_start();
            $seo=array(
                "title"=>B2bCommon::seo_title($strana),
                "description"=>B2bCommon::seo_description($strana),
                "keywords"=>B2bCommon::seo_keywords($strana),

            );
            return View::make('b2b.pages.pocetna',compact('seo'));
        }
    }



    public function ordering($type){
        Session::put('order',$type);
        return Redirect::back();
    }


    /* =============== CART ==================== */

    public function cartAdd(){
        B2bBasket::addCartB2b(Input::get('roba_id'),Input::get('quantity'), Input::get('status'), Session::get('b2b_user_'.B2bOptions::server()));
        $lagerObj = B2bArticle::lagerObj(Input::get('roba_id'));
        $cartAvailable = ($lagerObj->kolicina - $lagerObj->rezervisano) - B2bBasket::getB2bQuantityItem(Input::get('roba_id'));
        $items = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->orderBy('broj_stavke','asc')->get();
        return Response::json([
            'countItems'      => B2bBasket::b2bCountItems(),
            'cartContent'     => View::make('b2b.partials.ajax.header_cart', compact('items'))->render(),
            'cartAvailable'    => $cartAvailable
        ]);
    }

    public function cartContent(){
        $items = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->orderBy('broj_stavke','asc')->get();
        return Response::json(['cartContent' => View::make('b2b.partials.ajax.header_cart', compact('items'))->render()]);
    }

    public function cartDelete(){

        AdminSupport::saveLog('SADRZAJ_KORPE_OBRISI', array(Input::get('cart_id')));
        DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_stavka_id',Input::get('cart_id'))->delete();

    }
    public function cartDeleteAll(){
        AdminSupport::saveLog('SADRZAJ_KORPE_OBRISI_SVE', array(Session::get('b2b_cart')));
        DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->delete();
    }

    /* =============== CHECK OUT =================== */

    public function checkOut(){
        if(!Session::has('b2b_cart')){
            return  Redirect::to('/b2b/korpa');
        }
        if(is_null(DB::table('web_b2b_korpa')->where(array('web_b2b_korpa_id'=>Session::get('b2b_cart'), 'naruceno'=>0))->first())){
            Session::forget('b2b_cart');
            Session::regenerate();
            return  Redirect::to('/b2b/korpa')->with('message','Sadržaj korpe je već naručen.');
        }
        $web_b2b_korpa_id = Session::get('b2b_cart');
        $cart = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',$web_b2b_korpa_id)->get();

        $pass = true;
        foreach($cart as $cartItem){
            if($cartItem->kolicina <= 0){
                $pass=false;
                DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_stavka_id',$cartItem->web_b2b_korpa_stavka_id)->delete();
            }
            $lagerObj = B2bArticle::lagerObj($cartItem->roba_id);
            if($cartItem->kolicina > ($raspolozivo = $lagerObj->kolicina - $lagerObj->rezervisano)){
                $pass=false;
                if($raspolozivo > 0){
                    DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_stavka_id',$cartItem->web_b2b_korpa_stavka_id)->update(array('kolicina'=>$raspolozivo));
                }else{
                    DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_stavka_id',$cartItem->web_b2b_korpa_stavka_id)->delete();
                }
            }
        }
        if(!$pass){
            return  Redirect::to('/b2b/korpa')->with('message','Stanje količina je promenjeno, proverite količinu vaših stavki.');
        }

        Session::forget('b2b_cart');
        Session::regenerate();
        

        $input = Input::all();
        date_default_timezone_set( 'Europe/Belgrade');

        $br_nar = !is_null($br_doc = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id','!=','-1')->orderBy('web_b2b_narudzbina_id','desc')->pluck('broj_dokumenta')) ? intval(str_replace( 'WNB','',$br_doc)) + 1 : 1;
        $num_nar = str_pad($br_nar, 4, '0', STR_PAD_LEFT);
        $napomena = $input['napomena'];
        if(isset($input['adresa_isporuke']) && !empty($input['adresa_isporuke']) ){
            $adresa_isporuke ="  -Adresa isporuke: ". $input['adresa_isporuke'] ;
        }else{
            $adresa_isporuke = '';
        }
        $data=array(
            'web_b2b_korpa_id' => $web_b2b_korpa_id,
            'partner_id'=>Session::get('b2b_user_'.B2bOptions::server()),
            'orgj_id'=>-1,
            'poslovna_godina_id'=>B2bOptions::poslovna_godina(),
            'vrsta_dokumenta_id'=>500,
            'broj_dokumenta'=>"WNB".$num_nar,
            'datum_dokumenta'=>date("Y-m-d H:i:s"),
            'valuta_id'=>1,
            'kurs'=>B2bOptions::kurs(),
            'web_nacin_placanja_id'=>$input['nacin_placanja'],
            'web_nacin_isporuke_id'=>$input['nacin_isporuke'],
            'iznos'=>0,
            'prihvaceno'=>0,
            'stornirano'=>0,
            'realizovano'=>0,
            'napomena'=> $napomena . $adresa_isporuke,
            'ip_adresa'=>B2bCommon::ip_adress(),
            'posta_slanje_id'=>-1,
            'posta_slanje_poslato'=>0,
            'promena'=>1,
            'partner_korisnik_id'=> !is_null($partnerUser = B2bPartner::getPartnerUserObject()) ? $partnerUser->partner_korisnik_id : null,
            'sifra_connect'=>0,
            'avans'=> (isset($input['avans_check']) && $input['avans_check'] == 'on') ? 2.00 : 0.00,
            'imenik_id' => !is_null($user = B2bCommon::getLoggedAdminUser('KOMERCIJALISTA')) ? $user->imenik_id : null
        );


        DB::table('web_b2b_korpa')->where('web_b2b_korpa_id',$web_b2b_korpa_id)->update(array('naruceno'=>1));
        DB::table('web_b2b_narudzbina')->insert($data);

        $order_id = DB::table('web_b2b_narudzbina')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->where('broj_dokumenta',"WNB".$num_nar)->pluck('web_b2b_narudzbina_id');

        $cartItems = array();
        foreach($cart as $row){
            $roba = DB::table('roba')->where('roba_id',$row->roba_id)->first();
            $rabatCene = B2bArticle::b2bRabatCene($row->roba_id);
            if(isset($roba->roba_id) && $roba->roba_id != null){
                $cartItems[]=[
                  'web_b2b_narudzbina_id'=>$order_id,
                  'broj_stavke'=>$row->broj_stavke,
                  'roba_id'=>$row->roba_id,
                  'kolicina'=>$row->kolicina,
                  'jm_cena'=>$rabatCene->cena_sa_rabatom,
                  'tarifna_grupa_id'=>$row->tarifna_grupa_id,
                  'racunska_cena_nc'=>$row->racunska_cena_nc,
                  'racunska_cena_end'=>$row->racunska_cena_end
                ];
            }
        }
        if(count($cartItems) > 0){
            B2bBasket::rezervacija($cartItems);
            DB::table('web_b2b_narudzbina_stavka')->insert($cartItems);
        }

        if(Config::get('app.livemode')){        
            $order = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$order_id)->first();
            $partner = DB::table('partner')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
            $partnerKorisnik = B2bPartner::getPartnerUserObject();
            $orderItems = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$order->web_b2b_narudzbina_id)->orderBy('broj_stavke','asc')->get();

            if(DB::table('partner_komercijalista')->where('partner_id',$partner->partner_id)->first()) {
                $komercijalista = DB::table('imenik')->where('imenik_id',DB::table('partner_komercijalista')->where('partner_id',$partner->partner_id)->first()->imenik_id)->first();
            }

            $body = View::make('b2b.emails.order',compact('order','partner','partnerKorisnik','orderItems'))->render();

            // B2bCommon::send_email_to_client($body, $partner->mail,"Porudžbina WNB".$num_nar);
            // B2bCommon::send_email_to_admin($body, $partner->mail, $partner->naziv, "Porudžbina WNB".$num_nar);
            
            if(!is_null($partner->komercijalista) && !empty($partner->komercijalista)) {
                $subject = "Porudžbina WNB".$num_nar." - ".$partner->naziv." / ".$partner->komercijalista;
            } else {
                $subject = "Porudžbina WNB".$num_nar." - ".$partner->naziv;
            }

            $validatorEmail = Validator::make(['email'=>$partner->mail],['email'=>'required|email'],[]);
            if($validatorEmail->fails()) {
                Mailer::send('b2b@uspon.rs','b2b@uspon.rs',$subject,$body);
            } else {
                Mailer::send('b2b@uspon.rs',$partner->mail,$subject,$body);
                Mailer::send($partner->mail,'b2b@uspon.rs',$subject,$body);
            }
           
            // if(isset($komercijalista) && !empty($komercijalista->email)) {
            //     Mailer::send('b2b@uspon.rs',$komercijalista->email,$subject,$body);
            // }
        

            //IS
            $success = true;
            $sifra_is = null;
            if(B2bOptions::info_sys('wings')){
                $response = Wings::wingsOrder($order_id);
                if($response->success){
                    $sifra_is = $response->order_id;
                }else{
                    $success = false;
                }
            }elseif(B2bOptions::info_sys('calculus')){
                $response = Calculus::createOrder($order_id);
                if($response->success){
                    $sifra_is = $response->order_id;
                }else{
                    $success = false;
                }            
            }elseif(B2bOptions::info_sys('minimax')){
                $response = Minimax::create_order($order_id);

                if($response->success){
                    $sifra_is = $response->OrderId;
                }else{
                    $success = false;
                }
            }elseif(B2bOptions::info_sys('infograf')){
                $response = Infograf::createOrder($order_id);
                if($response->success){
                    $sifra_is = $response->order_id;
                }else{
                    $success = false;
                }            
            }elseif(B2bOptions::info_sys('logik')){
                $response = Logik::createOrder($order_id);
                if($response->success){
                    $sifra_is = $response->order_id;
                }else{
                    $success = false;
                }            
            }elseif(B2bOptions::info_sys('sbcs')){
                $response = Sbcs::createOrder($order_id);
                if(!$response){
                    $success = false;
                }
            }elseif(B2bOptions::info_sys('xml')){
                $response = Xml::createOrder($order_id);
                if(!$response){
                    $success = false;
                }          
            }elseif(B2bOptions::info_sys('roaming')){
                $response = Roaming::createOrder($order_id);
                if(!$response){
                    $success = false;
                }           
            }elseif(B2bOptions::info_sys('promobi')){
                $response = Promobi::createOrder($order_id);
                if(!$response){
                    $success = false;
                }           
            }elseif(B2bOptions::info_sys('spsoft')){
                $response = SpSoft::createOrder($order_id);
                if(!$response){
                    $success = false;
                }           
            }

            if($success){
                if(!is_null($sifra_is)){
                    DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$order_id)->update(array('sifra_is'=>$sifra_is));
                }
            }else{
                if(!is_null($partner->id_is)){
                    $mailFrom=B2bOptions::company_email();
                    $mailTo=$mailFrom;
                    $subject='Greska pri slanju porudzbine u IS-u (WNB'.$num_nar.').';
                    $body='<h3>Došlo je do greske pri slanju narudžbine u vašem IS-u. Proverite vašu narudžbinu WNB'.$num_nar.' u admin panelu.</h3>';
                    Mailer::send($mailFrom,$mailTo,$subject,$body);
                }
            }
        }
        return  Redirect::route('b2b.order',["WNB".$num_nar]);

    }

    public function order($bill){
        $order = DB::table('web_b2b_narudzbina')->where('broj_dokumenta',$bill)->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
        if(! $order){
           return Redirect::to('b2b');
        }
        else {
            $partner = DB::table('partner')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->first();
            $partnerKorisnik = B2bPartner::getPartnerUserObject();
            $orderItems = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$order->web_b2b_narudzbina_id)->orderBy('broj_stavke','asc')->get();
            $seo=array(
                "title"=>"Porudžbina ".$order->broj_dokumenta,
                "description"=>"",
                "keywords"=>"",

            );
            return View::make('b2b.pages.order',compact('seo','order','partner','partnerKorisnik','orderItems'));
        }
    }

    /* ============ END CHECK OUT ================== */
    public function prikaz($prikaz){
       Session::put('b2b_prikaz',$prikaz);
    
        return Redirect::back();
    
    }
    public function b2b_valuta($valuta){
        if(B2bOptions::product_currency()==1){ 
           Session::put('b2b_valuta',$valuta);
        }else{
            Session::forget('b2b_valuta');
        }
    
       return Redirect::back();
    }
    
    public function newsletter_add(){
         // $lang = Language::multi() ? Request::segment(1) : null;

         //    $email=Input::get('email');
         //    $validator = Validator::make(array('email'=>$email),array('email' => 'required|email|unique:web_b2c_newsletter,email'),Language::validator_messages());
            
         //    if($validator->fails()){
         //        echo Language::trans("E-mail je već registrovan").".";
         //    }
         //    else {
         //        $data=array('email'=>$email,'aktivan'=>1);
         //        DB::table('web_b2c_newsletter')->insert($data);
                
         //        $body="Poštovani,<br><br>Prijavili ste se za prijem novosti iz naše prodavnice.<br>
                       
         //               <br>S poštovanjem,<br>
         //               ". B2bOptions::company_name()   ."<br>"
         //                . B2bOptions::company_adress() .", ". B2bOptions::company_city() ."<br>"    
         //                ."mob: ". B2bOptions::company_phone()  ."<br>"
         //                ."fax: ".   B2bOptions::company_fax();

         //        $subject="Prijava za newsletter na ".B2bOptions::company_name();
         //        echo Language::trans("Uspešno ste se prijavili").".";

         //        B2bCommon::send_email_to_client($body,$email,$subject);

         //        $body_admin="Poštovani,<br><br>Imate prijavu za newsletter sa e-maila: ".$email." .<br>
                       
         //               <br>S poštovanjem,<br>
         //               ". B2bOptions::company_name()   ."<br>"
         //                . B2bOptions::company_adress() .", ". B2bOptions::company_city() ."<br>"    
         //                ."mob: ". B2bOptions::company_phone()  ."<br>"
         //                ."fax: ".   B2bOptions::company_fax();

         //        $subject_admin="Prijava za newsletter ";
         //        $email_company = B2bOptions::company_email();
         //        B2bCommon::send_email_to_client($body_admin,$email_company,$subject_admin);             
         //    }            

            $lang = Language::multi() ? Request::segment(1) : null;
            $email=Input::get('email');       
            $broj_korisnika_istih=DB::table('web_b2c_newsletter')->where('email',$email)->count();
            if($broj_korisnika_istih > 0){
                echo Language::trans("E-mail je već registrovan").".";
            }
            else {
                $kod = Support::custom_encrypt($email);

                 $body="Poštovani,<br><br>Prijavili ste se za prijem novosti iz naše prodavnice.<br>
                       Da biste aktivirali prijem novosti, molimo Vas da <a href='".Options::base_url().Url_mod::slug_trans('newsletter-potvrda')."/".$kod."' target='_blank'>KLIKNETE OVDE<a> kako biste potvrdili Vašu adresu.<br><br>
                       Ukoliko se niste Vi prijavili i ovu poruku poruku ste primili greškom, molimo Vas da je zanemarite.<br><br>
                       S poštovanjem,<br>
                       ". Options::company_name()   ."<br>"
                        . Options::company_adress() .", ". Options::company_city() ."<br>"    
                        ."mob: ". Options::company_phone()  ."<br>"
                        ."fax: ".   Options::company_fax();

                $subject="Prijava za newsletter na ".Options::company_name();

                WebKupac::send_email_to_client($body,$email,$subject);
                echo Language::trans("Potvrdite prijavu za newsletter preko vašeg mail-a").".";

            }
    }

    public function newsletter_confirm(){
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $kod = Request::segment(2+$offset);

        $email = Support::custom_decrypt($kod);
        $validator = Validator::make(array('email'=>$email),array('email' => 'required|email|unique:web_b2c_newsletter,email'),Language::validator_messages());

        if(!$validator->fails()){
            $data=array('email'=>$email,'aktivan'=>1);
            DB::table('web_b2c_newsletter')->insert($data);
        }
        
        return Redirect::to('b2b/pocetna');
    }  
    public function tagovi() { 
        $offset = Language::segment_offset();
        $lang = Language::multi() ? Request::segment(1) : null;
        $tag = Request::segment(3+$offset);

        $tag_convert = $tag;

        $artikli = DB::table('roba')->select('tags')->where(array('flag_prikazi_u_cenovniku'=>1,'flag_aktivan'=>1))->get();
        foreach($artikli as $artikal){
            if(!is_null($artikal->tags) && trim($artikal->tags) != ''){
                $tags = explode(',', trim($artikal->tags));

                foreach($tags as $exists_tag){
                    if(Url_mod::slugify($exists_tag)==$tag){
                        $tag_convert = $exists_tag;
                        break;
                    }
                }
            }
        }
        
        if($tag_convert == '' || $tag_convert == null) {
            return Redirect::to(B2bOptions::base_url());
        }

        if(Session::has('limit')) {
            $limit = Session::get('limit');
        } else {
            $limit = 20;
        }

        if(Input::get('page')) {
            $pageNo = Input::get('page');
        } else {
            $pageNo = 1;
        }
        $offset = ($pageNo-1) * $limit;

        
        $query_products=DB::table('roba')->where('flag_aktivan',1)->where('flag_prikazi_u_cenovniku', 1)->where('tags','ILIKE', '%'.$tag_convert.'%');
     
        $query_products= $query_products->orderBy('racunska_cena_end',(B2bOptions::web_options(207) == 0 ? 'asc' : 'desc'))->paginate($limit);
        $seo= array(
            "title"=>$tag,
            "description"=>$tag,
            "keywords"=>$tag
        );


        $data=array( 
            "strana"=>'B2Btagovi',
            "title"=>$tag,
            "description"=>$tag.' | '.Options::company_name(),
            "keywords"=>$tag.', '.Seo::company_tag(Options::company_name()),
            "url"=>'b2b/B2Btagovi/'.$tag,
            "query_products"=>$query_products,
            "count_products"=>count($query_products),
            "filter_prikazi"=>0,
            "seo"=>$seo,
            "limit"=>$limit
        );
        // /All::dd($data);

        return View::make('b2b.pages.products',$data);
    }
    public function comment_add_b2b(){
        $lang = Language::multi() ? Request::segment(1) : null;

        $data=Input::get();

        $validator_arr = array(
            'comment-name' => 'required|regex:'.Support::regex().'|max:200',
            'comment-review' => 'numeric',
            'comment-message' => 'required|regex:'.Support::regex().'|max:1000'
            );
        $translate_mess = Language::validator_messages();

       $validator = Validator::make($data,$validator_arr,$translate_mess);
        if($validator->fails() || !Captcha::check()){
            if(!Captcha::check()){
                $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
            }
            return Redirect::to(URL::previous().'#product_preview_tabs')->withInput()->withErrors($validator->messages())->with('contactError',true);
        }
        else {
            
            $ip=B2b::ip_adress();
            $data=array(
            'roba_id'=>$data['comment-roba_id'],
            'ip_adresa'=>$ip,
            'ime_osobe'=>$data['comment-name'],
            'pitanje'=>$data['comment-message'],
            'komentar_odobren'=>0,
            'datum'=>date("Y-m-d"),
            'odgovoreno'=>0,
            'ocena'=>$data['comment-review']
            );
            
            DB::table('web_b2b_komentari')->insert($data);

            return Redirect::back()->with('success_comment_message',true);
        }
    }
    public function low_price_mail(){
        
        $lang = Language::multi() ? Request::segment(1) : null;

            $email=Input::get('email');
            $cena = Input::get('cena');
            $id = Input::get('id');
            $partner_id=Input::get('partner_id');
            
            $partner=DB::table('partner')->where('partner_id',$partner_id)->pluck('naziv');
            
            $kod = Support::custom_encrypt($email);

            $body=" <p>Poštovani,<br /> Partner: ".$partner.", sa mailom: ".$email." poslao je zahtev za nižu cenu za artikal:" . Product::seo_title($id)." .
            <b>Cena koja bi njemu odgovarala je:</b> ".$cena." RSD<br />";

            $subject="Zahtev za nižu cenu od strane partnera: ".$partner." ";
            Mailer::send($email,'b2b@uspon.rs',$subject, $body);   
            //Mailer::send('stefan@tico.rs','stefan@tico.rs',$subject, $body);  
    }
}