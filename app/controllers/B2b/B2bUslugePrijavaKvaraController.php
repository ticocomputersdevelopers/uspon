<?php
use Service\Mailer;

class B2bUslugePrijavaKvaraController extends Controller {
	public function usluge_prijava_kvara(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;
		$orgStrana = Request::segment(2+$offset);
		$strana= 'usluge-prijava-kvara';
        $web_b2b_seo_id = Seo::get_b2b_page_id($strana);
		$custom = 0;

		// $check_page = DB::table('web_b2c_seo')->where('naziv_stranice',$strana)->count();
		// if($check_page==0){
		// 	return Redirect::to(B2bOptions::base_url().'b2b');
		// }

		$seo=array(
            "title"=>'Usluge prijava kvara',
            "description"=>'Usluge prijava kvara',
            "keywords"=>'usluge, prijava, kvara'
        );
		$data=array(
			"strana"=>$strana,
			"org_strana"=>$strana
		);

		return View::make('b2b.pages.b2b_usluge_prijava_kvara', compact('seo','data','custom'));
	}

	public function prijava_kvara(){
		$data=Input::get();
		$general_data = $data;

		unset($general_data['email-potvrda']);
		unset($general_data['captcha-string']);

		$validator_arr = array(
            'flag_vrsta_kupca' => 'required|in:0,1',
            'email' => 'required|email',
            'email-potvrda' => 'required|email|same:email',
            'telefon' => 'required',
            'mesto' => 'required',
            'adresa' => 'required',

            'proizvodjac' => 'required',
            'model' => 'required',
            'serijski' => 'required',
            'opis' => 'required'
            );

        $translate_mess =  Language::validator_messages();
        
        if($data['flag_vrsta_kupca'] == 0){
            unset($data['naziv']);
            unset($data['pib']);
            unset($data['maticni_br']);
            $validator_arr['ime'] = 'required';
            $validator_arr['prezime'] = 'required';
        }
        if($data['flag_vrsta_kupca'] == 1){
            unset($data['ime']);
            unset($data['prezime']);
            $validator_arr['naziv'] = 'required';
            $validator_arr['pib'] = 'required|digits_between:9,9|numeric';
            $validator_arr['maticni_br'] = 'required|digits_between:8,8|numeric';
        }
        $translate_mess['digits_between'] = 'Broj cifara mora biti :max!';
        $translate_mess['numeric'] = 'Polje sme da sadrzi samo brojeve!';
        $translate_mess['same'] = 'Email mora biti isti!';

        $validator = Validator::make($data,$validator_arr,$translate_mess);
        if($validator->fails() || !Captcha::check()){
            if(!Captcha::check()){
                $validator->getMessageBag()->add('captcha', $translate_mess['captcha']);
            }
            return Redirect::to(Options::base_url().'b2b/'.Url_mod::slug_trans('usluge-prijava-kvara'))->withInput()->withErrors($validator->messages());
        }
        else {
            DB::table('web_b2c_rma')->insert($general_data);

        	$kupac = $data['email'];
	        
	        if($data['flag_vrsta_kupca'] == 0){
	            $name = $data['ime'].' '.$data['prezime'];
	        }else{
	            $name = $data['naziv'].' '.$data['pib'];
	        }

	        $subject=Language::trans('Uspon DOO - prijava kvara')." - ".$name;

	        $data=array(
	            "strana"=>'order',
	            "org_strana"=>'oreder',
	            "title"=> $subject,
	            "description"=>"",
	            "keywords"=>"",
	            "kupac"=>$data['email'],
	            "message" => !Session::has('b2c_kupac') ? true : false
	            );

	        $body=" <p>Poštovani,<br /> Imate prijavu za servis<br />";
            if($general_data['flag_vrsta_kupca'] == 0) {
            	$body.= "<b>Ime i prezime:</b> ".$general_data['ime']." ".$general_data['prezime']."<br>";
            } else {
            	$body.= "<b>Naizv:</b> ".$general_data['naziv']."<br>";
            	$body.= "<b>PIB:</b> ".$general_data['pib']."<br>";
            	$body.= "<b>Matični broj:</b> ".$general_data['maticni_br']."<br>";
            }
        	$body.= "<b>Email:</b> ".$general_data['email']."<br>";
        	$body.= "<b>Telefon:</b> ".$general_data['telefon']."<br>";
        	$body.= "<b>Adresa:</b> ".$general_data['adresa']."<br><br>";
        	$body.= "<b>Mesto/grad:</b> ".$general_data['mesto']."<br><br>";

        	$body.= "<b>Proizvođač:</b> ".$general_data['proizvodjac']."<br>";
        	$body.= "<b>Model:</b> ".$general_data['model']."<br>";
        	$body.= "<b>Serijski broj uređaja (IMEI broj za GSM uređaje):</b> ".$general_data['serijski']."<br>";
        	$body.= "<b>Opis kvara:</b> ".$general_data['opis']."<br>";
        	if($general_data['preuzeti'] == 0) {
        		$body.= "<b>Preuzeti uređaj na adresi korisnika:</b> "."Ne"."<br>";
        	} else {
        		$body.= "<b>Preuzeti uređaj na adresi korisnika:</b> "."Da"."<br>";
        	}
        	$body .= "<br><b>Napomena:</b> Uređaje je potrebno zapakovati u zaštitnu ambalažu. Ukoliko uređaj nije propisno zapakovan, kurirska služba može da ne preuzme isti. U ovom slučaju prijava se mora ponovo popuniti nakon pripreme uređaja za preuzimanje.<br>";
    		$body .= "Radno vreme servisa je svaki radni dan od 08.00-16.00.";

	        // $body = View::make('shop/themes/'.Support::theme_path().'partials/rma_details',$data)->render();
	        // WebKupac::send_email_to_client($body,$kupac,$subject);
	        // All::send_email_to_admin($body,$kupac,$name,$subject);
	        Mailer::send($kupac,'rma@uspon.rs', $subject, $body);
	        Mailer::send('rma@uspon.rs',$kupac, $subject, $body);
	        if(!Session::has('b2c_kupac')){
	            $kupac_id = Session::get('b2c_kupac_temp');
	            Session::put('b2c_kupac',$kupac_id);
	            Session::forget('b2c_kupac_temp');
	            Session::forget('b2c_kupac');
	        }
	        return Redirect::to(Options::base_url().'b2b/pocetna')->with('prijava_kvara',Language::trans('Prijava kvara poslata.'));
        }
	}
}