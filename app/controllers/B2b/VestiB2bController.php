<?php

class VestiB2bController extends Controller {


	public function getNews(){

		$strana = 'blog';
		$check_page = DB::table('web_b2c_seo')->where('naziv_stranice',$strana)->count();

		if($check_page==0){
			return Redirect::to(B2bOptions::base_url().'b2b');
		}
        $seo=array(
            "strana"=> $strana,
            "title"=> B2bCommon::seo_title($strana),
            "description"=>B2bCommon::seo_description($strana),  
            "keywords"=>B2bCommon::seo_keywords($strana)
        );
        $news=B2bVesti::getNews();
		return View::make('b2b.pages.blogs', compact('seo','news'));
	}

	public function oneNew($id){

        $vest = DB::table('web_vest_b2b')->where(array('web_vest_b2b_id'=>$id))->first();
        if(is_null($vest)){
        	return Redirect::to(Options::base_url().'b2b');
        }
		$vesti_jezik=DB::table('web_vest_b2b_jezik')->where(array('web_vest_b2b_id'=>$id, 'jezik_id'=>1))->first();

        $seo=array(
            "strana"=> "vest",
            "title"=>$vesti_jezik->title ? $vesti_jezik->title : $vesti_jezik->naslov,
            "description"=>$vesti_jezik->description ? $vesti_jezik->description : $vesti_jezik->naslov,
            "keywords"=>$vesti_jezik->keywords ? $vesti_jezik->keywords : $vesti_jezik->naslov
        );
        $data = array(
        	'seo' => $seo,
        	'vest' => (object) array(
                'slika' => $vest && $vest->slika ? $vest->slika : '',
                'naslov' => $vesti_jezik && $vesti_jezik->naslov ? $vesti_jezik->naslov : '',
                'sadrzaj' => $vesti_jezik && $vesti_jezik->sadrzaj ? $vesti_jezik->sadrzaj : '',
	            )
        	);

		return View::make('b2b.pages.blog', $data);
	}	
}