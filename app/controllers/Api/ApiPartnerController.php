<?php

class ApiPartnerController extends Controller {

	public function partners(){
        $data = Input::get();
        $rules = array(
        	'page' => 'integer|min:1',
        	'limit' => 'integer|max:1000',
            'id' => 'integer|min:1',
            'name' => 'regex:'.AdminSupport::regex().'|max:300',
            'address' => 'regex:'.AdminSupport::regex().'|max:255',
            'city' => 'regex:'.AdminSupport::regex().'|max:200',
            'email' => 'email|max:100',
            'tax_id' => 'numeric|digits_between:9,9',
            'company_registration_number' => 'numeric|digits_between:8,8',
            'external_code' => 'max:255'
        );
        $validator = Validator::make($data,$rules);
        if($validator->fails()){
        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
        }

        $page = Input::get('page') ? intval(Input::get('page')) : 1;
        $limit = Input::get('limit') ? intval(Input::get('limit')) : 100;


		$partnersQuery = PartnerApi::with('cards');
		if(!empty($id = Input::get('id'))){
			$partnersQuery = $partnersQuery->whereRaw("partner_id = ".$id."");
		}
		if(!empty($name = Input::get('name'))){
			$partnersQuery = $partnersQuery->whereRaw("naziv ilike '%".$name."%'");
		}
		if(!empty($address = Input::get('address'))){
			$partnersQuery = $partnersQuery->whereRaw("adresa ilike '%".$address."%'");
		}
		if(!empty($city = Input::get('city'))){
			$partnersQuery = $partnersQuery->whereRaw("mesto ilike '%".$city."%'");
		}
		if(!empty($email = Input::get('email'))){
			$partnersQuery = $partnersQuery->whereRaw("mail = '".$email."'");
		}
		if(!empty($taxId = Input::get('tax_id'))){
			$partnersQuery = $partnersQuery->whereRaw("pib = '".$taxId."'");
		}
		if(!empty($companyRegistrationNumber = Input::get('company_registration_number'))){
			$partnersQuery = $partnersQuery->whereRaw("broj_maticni = '".$companyRegistrationNumber."'");
		}
		if(!empty($external_code = Input::get('external_code'))){
			$partnersQuery = $partnersQuery->whereRaw("id_is = '".$external_code."'");
		}

		$partners = $partnersQuery->limit($limit)->offset((($page-1)*$limit))->get()->toArray();

		return Response::json(['success'=>true,'partners'=>$partners],200);
	}

	public function partnersSave(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(count($data) > 20000){
        	return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }
        if(DB::table('partner')->count() > 5000){
        	return Response::json(['success'=>false,'messages'=>'Max number of categories is 5000.'],200);
        }

        foreach($data as $row){
	        $rules = array(
	        	'id' => 'integer|exists:grupa_pr,grupa_pr_id',
		    	'external_code' => 'max:255',
		    	'name' => ((isset($row['id']) || isset($row['external_code'])) ? '' : 'required|').'regex:'.$regex.'|max:255',
		        'address' => 'regex:'.$regex.'|max:255',
		        'city' => 'regex:'.$regex.'|max:200',
		        'phone' => 'regex:'.$regex.'|max:255',
		        'email' => 'max:100|email',
		        'tax_id' => 'numeric|digits_between:9,9',
		        'company_registration_number' => 'numeric|digits_between:8,8',
		        'bank_account_number' => 'regex:'.$regex.'|max:255',
		        'rebate' => 'numeric|digits_between:0,7'
	        );
	        $validator = Validator::make($row,$rules);
	        if($validator->fails()){
	        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
	        	break;
	        }
        }

        $mappedPartners = PartnerApi::mappedByExternalCode();
        foreach($data as $row){
        	$partner = null;
        	if(isset($row['id'])){
	        	$partner = PartnerApi::where('grupa_pr_id',$row['id'])->first();
        	}
        	else if(isset($row['external_code'])){
	        	$partner = PartnerApi::where('id_is',$row['external_code'])->first();
        	}

        	if(is_null($partner)){
	            $partner = new PartnerApi();
	            $partner->drzava_id = 1;
	        	$partner->naziv = !empty($row['name']) ? $row['name'] : (isset($row['external_code']) ? $row['external_code'] : '');
		    	$partner->adresa = isset($row['address']) ? $row['address'] : null;
		    	$partner->mesto = isset($row['city']) ? $row['city'] : null;
		    	$partner->telefon = isset($row['phone']) ? $row['phone'] : null;
		    	$partner->mail = isset($row['email']) ? $row['email'] : null;
		    	$partner->pib = isset($row['tax_id']) ? $row['tax_id'] : null;
		    	$partner->broj_maticni = isset($row['company_registration_number']) ? $row['company_registration_number'] : null;
		    	$partner->racun = isset($row['bank_account_number']) ? $row['bank_account_number'] : null;
		    	$partner->rabat = isset($row['rebate']) ? $row['rebate'] : 0;

        	}else{
        		if(isset($row['name'])){
		    		$partner->naziv = $row['name'];
        		}
        		if(isset($row['address'])){
		    		$partner->adresa = $row['address'];
        		}
        		if(isset($row['city'])){
		    		$partner->mesto = $row['city'];
        		}
        		if(isset($row['phone'])){
		    		$partner->telefon = $row['phone'];
        		}
        		if(isset($row['email'])){
		    		$partner->mail = $row['email'];
        		}
        		if(isset($row['tax_id'])){
		    		$partner->pib = $row['tax_id'];
        		}
        		if(isset($row['company_registration_number'])){
		    		$partner->broj_maticni = $row['company_registration_number'];
        		}
        		if(isset($row['bank_account_number'])){
		    		$partner->racun = $row['bank_account_number'];
        		}
        		if(isset($row['rebate'])){
		    		$partner->rabat = $row['rebate'];
        		}
        	}
        	
			if(isset($row['external_code'])){
	        	$partner->id_is = $row['external_code'];
        	}

        	$partner->save();

        }

        return Response::json(['success'=>true],200);
	}

	public function partnerCardsSave(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(count($data) > 20000){
        	return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }
        if(DB::table('partner')->count() > 200000){
        	return Response::json(['success'=>false,'messages'=>'Max number of categories is 200000.'],200);
        }

        foreach($data as $row){
	        $rules = array(
	        	'id' => 'integer|exists:web_b2b_kartica,web_b2b_kartica_id',
		    	'external_code' => 'max:255',
	        	'partner_id' => (isset($row['partner_external_code']) ? '' : 'required|').'integer|exists:partner,partner_id',
		    	'partner_external_code' => (isset($row['partner_id']) ? '' : 'required|').'max:255|exists:partner,id_is',
		        'document_kind' => ((isset($row['id']) || isset($row['external_code'])) ? '' : 'required|').'regex:'.$regex.'|max:200',
		        'document_date' => ((isset($row['id']) || isset($row['external_code'])) ? '' : 'required|').'date_format:Y-m-d',
		        'incoming_date' => 'date_format:Y-m-d',
		        'description' => 'regex:'.$regex.'|max:300',
		        'debit' => 'numeric|digits_between:1,15',
		        'credit' => 'numeric|digits_between:1,15'
	        );
	        $validator = Validator::make($row,$rules);
	        if($validator->fails()){
	        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
	        	break;
	        }
        }

        $mappedPartners = PartnerApi::mappedByExternalCode();
        foreach($data as $row){
        	$partnerCard = null;
        	if(isset($row['id'])){
	        	$partnerCard = PartnerCard::where('web_b2b_kartica_id',$row['id'])->first();
        	}
        	else if(isset($row['external_code'])){
	        	$partnerCard = PartnerCard::where('id_is',$row['external_code'])->first();
        	}

        	if(is_null($partnerCard)){
	            $partnerCard = new PartnerCard();
	        	$partnerCard->vrsta_dokumenta = !empty($row['document_kind']) ? $row['document_kind'] : (isset($row['external_code']) ? $row['external_code'] : '');
	        	$partnerCard->datum_dokumenta = !empty($row['document_date']) ? $row['document_date'] : date('Y-m-d');
	        	$partnerCard->datum_dospeca = isset($row['incoming_date']) ? $row['incoming_date'] : null;
	        	$partnerCard->opis = isset($row['description']) ? $row['description'] : null;
	        	$partnerCard->duguje = isset($row['debit']) ? $row['debit'] : 0;
	        	$partnerCard->potrazuje = isset($row['credit']) ? $row['credit'] : 0;
        		if(isset($row['partner_id'])){
        			$partnerCard->partner_id = $row['partner_id'];
        		}else if(isset($row['partner_external_code']) && isset($mappedPartners[$row['partner_external_code']])){
        			$partnerCard->partner_id = $mappedPartners[$row['partner_external_code']];
        		}

        	}else{
        		if(isset($row['document_kind'])){
		    		$partnerCard->vrsta_dokumenta = $row['document_kind'];
        		}
        		if(isset($row['document_date'])){
		    		$partnerCard->datum_dokumenta = $row['document_date'];
        		}
        		if(isset($row['incoming_date'])){
		    		$partnerCard->datum_dospeca = $row['incoming_date'];
        		}
        		if(isset($row['description'])){
		    		$partnerCard->opis = $row['description'];
        		}
        		if(isset($row['debit'])){
		    		$partnerCard->duguje = $row['debit'];
        		}
        		if(isset($row['credit'])){
		    		$partnerCard->potrazuje = $row['credit'];
        		}
        		if(isset($row['partner_id'])){
        			$partnerCard->partner_id = $row['partner_id'];
        		}else if(isset($row['partner_external_code']) && isset($mappedPartners[$row['partner_external_code']])){
        			$partnerCard->partner_id = $mappedPartners[$row['partner_external_code']];
        		}
        	}
        	
    		$partnerCard->saldo = $partnerCard->duguje - $partnerCard->potrazuje;

			if(isset($row['external_code'])){
	        	$partnerCard->id_is = $row['external_code'];
        	}

        	$partnerCard->save();

        }

        return Response::json(['success'=>true],200);
	}


    public function partnersDelete(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(count($data) > 20000){
            return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }

        if(!isset($data[0])){
            return Response::json(['success'=>false,'messages'=>'Array of inputs is required.'],200);
        }

        if(isset($data[0]['id'])){
            foreach($data as $row){
                $rules = array(
                    'id' => 'required|integer'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }
        else if(isset($data[0]['external_code'])){
            foreach($data as $row){
                $rules = array(
                    'external_code' => 'required|max:255'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }else{
            return Response::json(['success'=>false,'messages'=>'Element require id or external_code.'],200);
        }


        if(isset($data[0]['id'])){
            $ids = array_map(function($item){ return $item['id']; },$data);
            DB::table('partner')->whereIn('partner_id',$ids)->delete();
        }
        else if(isset($data[0]['external_code'])){
            $codes = array_map(function($item){ return $item['external_code']; },$data);
            DB::table('partner')->whereIn('id_is',$codes)->delete();
        }

        return Response::json(['success'=>true],200);
    }

    public function partnerCardsDelete(){
        $data = Input::get();

        if(count($data) > 20000){
            return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }

        if(!isset($data[0])){
            return Response::json(['success'=>false,'messages'=>'Array of inputs is required.'],200);
        }

        if(isset($data[0]['id'])){
            foreach($data as $row){
                $rules = array(
                    'id' => 'required|integer'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }
        else if(isset($data[0]['external_code'])){
            foreach($data as $row){
                $rules = array(
                    'external_code' => 'required|max:255'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }else{
            return Response::json(['success'=>false,'messages'=>'Element require id or external_code.'],200);
        }


        if(isset($data[0]['id'])){
            $ids = array_map(function($item){ return $item['id']; },$data);
            DB::table('web_b2b_kartica')->whereIn('web_b2b_kartica_id',$ids)->delete();
        }
        else if(isset($data[0]['external_code'])){
            $codes = array_map(function($item){ return $item['external_code']; },$data);
            DB::table('web_b2b_kartica')->whereIn('id_is',$codes)->delete();
        }

        return Response::json(['success'=>true],200);
    }

}