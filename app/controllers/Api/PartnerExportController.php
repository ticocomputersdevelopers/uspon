<?php

class PartnerExportController extends Controller {

    public function xml(){
        $korisnickoime = Input::get('korisnickoime');
        $password = Input::get('lozinka');
        $currency = Input::get('valuta');
        $description = Input::get('opis');
        $chars = Input::get('karakteristike');
        $imgs = Input::get('slike');
        $word = Input::get('kljucnarec');
        $category = Input::get('kategorija');
        $parentCategory = Input::get('nadkategorija');
        $manufacturer = Input::get('proizvodjac');
        $code = Input::get('sifra');
        $quantity = Input::get('kolicina');
        $short = Input::get('kratak');

        $baseUrl = Options::domain();
        $partner = DB::table('partner')->where(array('api_username'=>$korisnickoime,'api_password'=>$password,'aktivan_api'=>1))->whereNotNull('api_username')->whereNotNull('api_password')->first();
        $partnerId=$partner->partner_id;
        $kurs = 1;
        $valuta = 'RSD';
        if(!empty($currency) && strtolower($currency) == 'eur'){
            $kurs = B2bOptions::kurs();
            $valuta = 'EUR';
        }
        $checkDescription = false;
        if(!empty($description) && $description == '1'){
            $checkDescription = true;
        }
        $checkCharacteristics = false;
        if(!empty($chars) && $chars == '1'){
            $checkCharacteristics = true;
        }
        $checkImages = false;
        if(!empty($imgs) && $imgs == '1'){
            $checkImages = true;
        }
        $checkQuantity = false;
        if(!empty($quantity) && $quantity == '1'){
            $checkQuantity = true;
        }
        $checkShort = false;
        if(!empty($short) && $short == '1'){
            $checkShort = true;
        }


        if(!$checkShort){
			if($partner->partner_id == 127238 OR $partner->partner_id == 750){
            	
            	$query = "SELECT * from (SELECT r.roba_id, sifra_is, web_cena, mpcena, racunska_cena_nc, naziv_web, web_flag_karakteristike, web_karakteristike, energetska_klasa, energetska_klasa_pdf, energetska_klasa_link, deklaracija, tarifna_grupa_id, web_opis, grupa_pr_id, barkod, model,(SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT parrent_grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id LIMIT 1) AND grupa_pr_id > 0) AS nadgrupa,
                (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id) AS grupa,(SELECT naziv FROM jedinica_mere WHERE jedinica_mere_id = r.jedinica_mere_id) AS jedinica_mere, 
                (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = r.proizvodjac_id) AS proizvodjac,(SELECT kolicina-rezervisano FROM lager l WHERE l.roba_id=r.roba_id  AND orgj_id = 11) AS kolicina, (SELECT kolicina-rezervisano FROM lager l WHERE l.roba_id=r.roba_id  AND orgj_id = 26) AS kolicinamp 
                 FROM roba r WHERE r.flag_aktivan = 1 and proizvodjac_id !=947 AND proizvodjac_id!=822 AND r.grupa_pr_id != 2914 AND r.grupa_pr_id != 2440 AND r.sifra_is !='' AND r.flag_cenovnik = 1 AND r.racunska_cena_end > 0 ) t where (t.kolicina > 1 OR t.kolicinamp > 1)
                    AND (roba_id  IN (select roba_id from roba where grupa_pr_id = 2660 and proizvodjac_id =1275 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2838 and proizvodjac_id =1235 ) 
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2840 and proizvodjac_id =1235 ) 
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2842 and proizvodjac_id =1235 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3545 and proizvodjac_id =1030 ) 
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2498 and proizvodjac_id =1030 ) 

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3506 and proizvodjac_id =961 ) 

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3542 and proizvodjac_id =550 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3543 and proizvodjac_id =550 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2572 and proizvodjac_id =550 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2704 and proizvodjac_id =550 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2618 and proizvodjac_id =550 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2572 and proizvodjac_id =552 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2704 and proizvodjac_id =552 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3537 OR grupa_pr_id =3538 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2572 and proizvodjac_id =1278 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2704 and proizvodjac_id =1278 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2572 and proizvodjac_id =1134 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2704 and proizvodjac_id =1134 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2572 and proizvodjac_id =892 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2704 and proizvodjac_id =892 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2452 and proizvodjac_id =368 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2896 and proizvodjac_id =368 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2894 and proizvodjac_id =368 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2904 and proizvodjac_id =368 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2468 and proizvodjac_id =218 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3008 and proizvodjac_id =218 )
                    OR roba_id  IN (select roba_id from roba where get_main_grupa_pr_id(grupa_pr_id) =  2442 and proizvodjac_id =218 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2468 and proizvodjac_id =426 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3008 and proizvodjac_id =426 )
                    OR roba_id  IN (select roba_id from roba where get_main_grupa_pr_id(grupa_pr_id) =  2442 and proizvodjac_id =426 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2468 and proizvodjac_id =904 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3008 and proizvodjac_id =904 )
                    OR roba_id  IN (select roba_id from roba where get_main_grupa_pr_id(grupa_pr_id) =  2442 and proizvodjac_id =904 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2962 and proizvodjac_id =218 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2960 and proizvodjac_id =218 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2848 and proizvodjac_id =218 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2850 and proizvodjac_id =218 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3579 and proizvodjac_id =218 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3547 and proizvodjac_id =218 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2452 and proizvodjac_id =218 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2626 and proizvodjac_id =218 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2896 and proizvodjac_id =218 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2894 and proizvodjac_id =218 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2452 and proizvodjac_id =1009 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2626 and proizvodjac_id =1009 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2896 and proizvodjac_id =1009 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2894 and proizvodjac_id =1009 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2904 and proizvodjac_id =1009 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3492 and proizvodjac_id =760 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2468 and proizvodjac_id =760 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3530 and proizvodjac_id =760 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2618 and proizvodjac_id =350 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2510 and proizvodjac_id =350 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3520 and proizvodjac_id =350 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3540 and proizvodjac_id =350 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3539 and proizvodjac_id =350 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2618 and proizvodjac_id =705 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2510 and proizvodjac_id =705 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3520 and proizvodjac_id =705 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3540 and proizvodjac_id =705 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3539 and proizvodjac_id =705 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2618 and proizvodjac_id =308 OR proizvodjac_id =83 OR proizvodjac_id =270 OR proizvodjac_id =30 OR proizvodjac_id =705 OR proizvodjac_id =350 OR proizvodjac_id =1020 OR proizvodjac_id =1019 OR proizvodjac_id =1017 OR proizvodjac_id =363 OR proizvodjac_id =139)

                    OR roba_id  IN (select roba_id from roba where (grupa_pr_id = 2722 or grupa_pr_id =2880 or grupa_pr_id =2882 or grupa_pr_id =2886) AND proizvodjac_id =58 OR proizvodjac_id =630 OR proizvodjac_id =54 OR proizvodjac_id =406 OR proizvodjac_id =618 OR proizvodjac_id =384 OR proizvodjac_id =32)
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2626  AND proizvodjac_id =904 OR proizvodjac_id =426 OR proizvodjac_id =218 OR proizvodjac_id =1009 OR proizvodjac_id =585 OR proizvodjac_id =590 OR proizvodjac_id =363 OR proizvodjac_id =139 OR proizvodjac_id =58 OR proizvodjac_id =871 OR proizvodjac_id =55)
                    
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3540 and proizvodjac_id =308 OR proizvodjac_id =83 OR proizvodjac_id =1020 OR proizvodjac_id =1019 OR proizvodjac_id =1017 OR proizvodjac_id =705 OR proizvodjac_id =350)
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3539 and proizvodjac_id =308 OR proizvodjac_id =83 OR proizvodjac_id =1020 OR proizvodjac_id =1019 OR proizvodjac_id =1017 OR proizvodjac_id =705 OR proizvodjac_id =350)

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2452 AND proizvodjac_id =904 OR proizvodjac_id =426 OR proizvodjac_id =218 OR proizvodjac_id =762 OR proizvodjac_id =590 OR proizvodjac_id =585 OR proizvodjac_id =1009 OR proizvodjac_id =871 OR proizvodjac_id =55 OR proizvodjac_id =363 OR proizvodjac_id =139 OR proizvodjac_id =58)

                    OR roba_id  IN (select roba_id from roba where ( grupa_pr_id = 2704 OR grupa_pr_id = 2572 ) AND proizvodjac_id =1134 OR proizvodjac_id =892 OR proizvodjac_id =1278 OR proizvodjac_id =521 OR proizvodjac_id =58 OR proizvodjac_id =363 OR proizvodjac_id =139 OR proizvodjac_id =799 OR proizvodjac_id =1397 OR proizvodjac_id =550 )

                    OR roba_id  IN (select roba_id from roba where ( grupa_pr_id = 2894 OR grupa_pr_id = 2896 OR grupa_pr_id = 2898 OR grupa_pr_id = 2900 OR grupa_pr_id = 2902 OR grupa_pr_id = 2904) AND proizvodjac_id =1009 OR proizvodjac_id =871 OR proizvodjac_id =55 OR proizvodjac_id =904 OR proizvodjac_id =426 OR proizvodjac_id =218 OR proizvodjac_id =590 OR proizvodjac_id =1356 OR proizvodjac_id =368 OR proizvodjac_id =872 )

                    OR roba_id  IN (select roba_id from roba where ( grupa_pr_id = 3542 OR grupa_pr_id = 3543) AND proizvodjac_id =363 OR proizvodjac_id =139 OR proizvodjac_id =58 OR proizvodjac_id =1397 OR proizvodjac_id =550 )

                    OR roba_id  IN (select roba_id from roba where ( grupa_pr_id = 2702 OR grupa_pr_id = 3376 OR grupa_pr_id = 3332 OR grupa_pr_id = 2618 OR grupa_pr_id = 3520) AND proizvodjac_id =1423 OR proizvodjac_id =64 OR proizvodjac_id =700 OR proizvodjac_id =95 OR proizvodjac_id =369 OR proizvodjac_id =712 OR proizvodjac_id =35 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3334 and proizvodjac_id =852 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3010 and proizvodjac_id =852 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3008 and proizvodjac_id =852 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3532 and proizvodjac_id =852 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3496 and proizvodjac_id =872 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3506 and proizvodjac_id =872 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3502 and proizvodjac_id =872 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3500 and proizvodjac_id =872 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2452 and proizvodjac_id =872 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2626 and proizvodjac_id =872 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2894 and proizvodjac_id =872 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2896 and proizvodjac_id =872 )

                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3126 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2470 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3496 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3506 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3498 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where get_main_grupa_pr_id(grupa_pr_id) =  2380 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3500 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3502 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3122 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2646 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2554 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3532 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2848 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2850 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2648 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2918 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2920 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 2922 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3134 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3476 and proizvodjac_id =501 )
                    OR roba_id  IN (select roba_id from roba where grupa_pr_id = 3581 and proizvodjac_id =501 )     

                    OR roba_id  IN (select roba_id from roba where proizvodjac_id =1017 )       
                    OR roba_id  IN (select roba_id from roba where proizvodjac_id =1018 )
                    OR roba_id  IN (select roba_id from roba where proizvodjac_id =1019 )
                    OR roba_id  IN (select roba_id from roba where proizvodjac_id =1020 )
                    OR roba_id  IN (select roba_id from roba where proizvodjac_id =1021 )     

                    OR roba_id  IN (select roba_id from roba where proizvodjac_id =247 )   
                    OR roba_id  IN (select roba_id from roba where proizvodjac_id =172 )   
                    OR roba_id  IN (select roba_id from roba where proizvodjac_id =1619 )   
                    OR roba_id  IN (select roba_id from roba where proizvodjac_id =1607 )   
                    OR roba_id  IN (select roba_id from roba where proizvodjac_id =355 )   
                    OR roba_id  IN (select roba_id from roba where proizvodjac_id =1584 )   
                     )";
            } elseif($partner->partner_id == 750){
            $query = "SELECT * FROM (SELECT r.roba_id, sifra_is, web_cena, mpcena, racunska_cena_nc, naziv_web, web_flag_karakteristike, web_karakteristike, energetska_klasa, energetska_klasa_pdf, energetska_klasa_link, deklaracija, tarifna_grupa_id, web_opis, grupa_pr_id, barkod, model, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT parrent_grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id LIMIT 1) AND grupa_pr_id > 0) AS nadgrupa, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id) AS grupa, (SELECT naziv FROM jedinica_mere WHERE jedinica_mere_id = r.jedinica_mere_id) AS jedinica_mere, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = r.proizvodjac_id) AS proizvodjac, ( SELECT SUM(kolicina-rezervisano) FROM lager l WHERE l.roba_id=r.roba_id AND orgj_id = 11 GROUP BY roba_id) AS kolicina FROM roba r WHERE r.flag_aktivan = 1 AND r.grupa_pr_id != 2440 AND r.sifra_is !='' AND r.flag_cenovnik = 1 AND r.racunska_cena_end > 0 AND proizvodjac_id !=956 and proizvodjac_id !=947 AND proizvodjac_id!=822
                AND r.roba_id IN (select roba_id from roba where proizvodjac_id = 426 OR proizvodjac_id = 892 OR proizvodjac_id = 350 OR proizvodjac_id = 760 OR proizvodjac_id = 550 OR proizvodjac_id = 1030 OR proizvodjac_id = 951 OR proizvodjac_id = 1009 OR proizvodjac_id = 552 OR proizvodjac_id = 103 OR proizvodjac_id = 501)) t where t.kolicina > 0 ";
            }
            else{
			$query = " SELECT * FROM ( SELECT r.roba_id, sifra_is, web_cena, mpcena, racunska_cena_nc, naziv_web, web_flag_karakteristike, web_karakteristike, energetska_klasa, energetska_klasa_pdf, energetska_klasa_link, deklaracija, tarifna_grupa_id, web_opis, grupa_pr_id, barkod, model, r.proizvodjac_id, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = (SELECT parrent_grupa_pr_id FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id LIMIT 1) AND grupa_pr_id > 0) AS nadgrupa, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id) AS grupa, (SELECT naziv FROM jedinica_mere WHERE jedinica_mere_id = r.jedinica_mere_id) AS jedinica_mere, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = r.proizvodjac_id) AS proizvodjac, (SELECT SUM(kolicina-rezervisano) FROM lager l WHERE l.roba_id = r.roba_id AND (orgj_id = 26 OR orgj_id = 11)  GROUP BY roba_id) AS kolicina , (SELECT SUM(kolicina - rezervisano) 
    FROM lager l 
    WHERE l.roba_id = r.roba_id  
    AND orgj_id != 1 
    AND orgj_id IN (27, 24, 10, 9, 8, 6, 5) 
    AND NOT EXISTS (
        SELECT 1 FROM lager l2 
        WHERE l2.roba_id = r.roba_id  
        AND l2.orgj_id IN (27, 24, 10, 9, 8, 6, 5) 
        AND l2.kolicina <= 1
    )) 
AS kolicinamp FROM roba r WHERE r.flag_aktivan = 1 AND r.sifra_is != '' AND r.flag_cenovnik = 1 AND r.racunska_cena_end > 0 ) t WHERE (t.kolicina > 0 OR t.kolicinamp > 2) AND t.proizvodjac_id NOT IN (SELECT proizvodjac_id FROM nivo_pristupa_proizvodjac WHERE partner_id = " . DB::getPdo()->quote($partnerId) . " OR partner_id = -2) AND t.grupa_pr_id NOT IN (SELECT grupa_pr_id FROM nivo_pristupa_grupa WHERE partner_id = " . DB::getPdo()->quote($partnerId) . " OR partner_id = -2) ";

			}
        }else{
                $query = "SELECT * FROM (SELECT r.roba_id, sifra_is,r.proizvodjac_id,r.grupa_pr_id, naziv_web, barkod, ( SELECT SUM(kolicina-rezervisano) FROM lager l WHERE l.roba_id=r.roba_id AND orgj_id != 1 AND orgj_id NOT IN (SELECT orgj_id FROM nivo_pristupa_lager WHERE partner_id = " . DB::getPdo()->quote($partnerId) . " OR partner_id = -2) GROUP BY roba_id) AS kolicina, (SELECT kolicina-rezervisano FROM lager l WHERE l.roba_id=r.roba_id  AND orgj_id = 26) AS kolicinamp  FROM roba r WHERE r.flag_aktivan = 1 AND r.sifra_is !=''  AND r.flag_cenovnik = 1 AND r.racunska_cena_end > 0) t where (t.kolicina > 0 OR t.kolicinamp > 1) AND t.proizvodjac_id NOT IN (SELECT proizvodjac_id FROM nivo_pristupa_proizvodjac WHERE partner_id = " . DB::getPdo()->quote($partnerId) . " OR partner_id = -2) AND t.grupa_pr_id NOT IN (SELECT grupa_pr_id FROM nivo_pristupa_grupa WHERE partner_id = " . DB::getPdo()->quote($partnerId) . " OR partner_id = -2) ";
            
        }

        if(!empty($word)){
            $query .= " AND naziv_web ILIKE '%".strval($word)."%'";
        }
        if(!empty($category)){
            $query .= " AND grupa_pr_id = (SELECT grupa_pr_id FROM grupa_pr WHERE grupa ILIKE '%".strval($category)."%' LIMIT 1)";
        }
        if(!empty($parentCategory)){
            $query .= " AND grupa_pr_id IN (SELECT grupa_pr_id FROM grupa_pr WHERE parrent_grupa_pr_id = (SELECT grupa_pr_id FROM grupa_pr WHERE grupa ILIKE '%".strval($parentCategory)."%' LIMIT 1))";
        }
        if(!empty($manufacturer)){
            $query .= " AND proizvodjac_id = (SELECT proizvodjac_id FROM proizvodjac WHERE naziv ILIKE '%".strval($manufacturer)."%' LIMIT 1)";
        }
        if($checkQuantity){
            $query .= " AND r.roba_id IN (SELECT roba_id FROM lager WHERE (kolicina-rezervisano) > 0 AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE imenik_magacin_id = 20 LIMIT 1))";
        }
        if(!empty($code)){
            $query .= " AND t.sifra_is = '".strval($code)."'";
        }

        if($partner->partner_id == 1119) {
            $query .= " AND t.grupa_pr_id in (2518,2974,3506,2680,2704,2572,3540,3539,3538,3537,2506) ";
        }
        
    //  try {
            $products = DB::select($query);
        // } catch (Exception $e) {
        //  return Response::make('Bad Request', 400);
        // }

        $xml = new DOMDocument("1.0","UTF-8");
        $root = $xml->createElement("artikli");
        $xml->appendChild($root);
        foreach($products as $article){
            $b2bRabatCene = B2bArticle::b2bRabatCene($article->roba_id,$partner->partner_id);

            $product   = $xml->createElement("artikal");

            PartnerExport::xml_node($xml,"sifra",$article->sifra_is,$product);
            PartnerExport::xml_node($xml,"barkod",$article->barkod,$product);
            PartnerExport::xml_node($xml,"naziv",PartnerExport::string_format($article->naziv_web),$product);
            if(!$checkShort){
              
                PartnerExport::xml_node($xml,"pdv",AdminSupport::find_tarifna_grupa($article->tarifna_grupa_id,'porez'),$product);
                PartnerExport::xml_node($xml,"nadgrupa",PartnerExport::string_format($article->nadgrupa),$product);
                PartnerExport::xml_node($xml,"grupa",PartnerExport::string_format($article->grupa),$product);
                PartnerExport::xml_node($xml,"proizvodjac",PartnerExport::string_format($article->proizvodjac),$product);
                PartnerExport::xml_node($xml,"jedinica_mere",PartnerExport::string_format($article->jedinica_mere),$product);
                PartnerExport::xml_node($xml,"model",PartnerExport::string_format($article->model),$product);
            }
            if(isset($article->kolicinamp)){
                PartnerExport::xml_node($xml,"kolicina",(($article->kolicina + $article->kolicinamp) > 10 ? '10+' : round($article->kolicina+$article->kolicinamp)),$product);
            }else{
                PartnerExport::xml_node($xml,"kolicina",($article->kolicina > 10 ? '10+' : round($article->kolicina)),$product);
            }
            // PartnerExport::xml_node($xml,"osnovna_cena", ($b2bRabatCene->osnovna_cena / $kurs) ,$product);
            // PartnerExport::xml_node($xml,"rabat", $b2bRabatCene->ukupan_rabat ,$product);
            // PartnerExport::xml_node($xml,"cena_rabat", ($b2bRabatCene->cena_sa_rabatom / $kurs) ,$product);
            // PartnerExport::xml_node($xml,"cena_pdv", ($b2bRabatCene->ukupna_cena / $kurs) ,$product);
            PartnerExport::xml_node($xml,"b2bcena", round(($b2bRabatCene->cena_sa_rabatom / $kurs),2) ,$product);
            if(!$checkShort){
            PartnerExport::xml_node($xml,"valuta",$valuta,$product);
            PartnerExport::xml_node($xml,"flag_akcijska_cena", B2bCommon::provera_akcija($article->roba_id) ,$product);
            PartnerExport::xml_node($xml,"web_cena", round(($article->web_cena / $kurs),2) ,$product);
            PartnerExport::xml_node($xml,"mpcena", ($article->mpcena / $kurs) ,$product);
            PartnerExport::xml_node($xml,"energetska_klasa", $article->energetska_klasa ,$product);
            PartnerExport::xml_node($xml,"energetska_klasa_link", $article->energetska_klasa_link ,$product);
            PartnerExport::xml_node($xml,"energetska_klasa_pdf", !empty($article->energetska_klasa_pdf) ? Options::base_url().$article->energetska_klasa_pdf : '' ,$product);
            PartnerExport::xml_node($xml,"deklaracija", $article->deklaracija ,$product);
            }
            if(!$checkShort){
                if($checkDescription){
                    PartnerExport::xml_node($xml,"opis",($article->web_flag_karakteristike == 0 ? PartnerExport::string_format($article->web_karakteristike) : ''),$product);
                }

                if($checkImages){
                    $images   = $xml->createElement("slike");
                    foreach(PartnerExport::slike($article->roba_id) as $slika){
                        PartnerExport::xml_node($xml,"slika",$baseUrl.$slika,$images);
                    }
                    $product->appendChild($images);
                }

                if($checkCharacteristics){
                    $characteristics   = $xml->createElement("karakteristike");
                    foreach(PartnerExport::characteristics($article->roba_id,2) as $karakteristika){
                        $characteristic_group = $xml->createElement("karakteristike_grupa");
                        $characteristic_group->setAttribute("ime", PartnerExport::string_format($karakteristika->group));
                        
                        foreach($karakteristika->characteristic as $karak){
                            $karakteristika_xml = $xml->createElement("karakteristika");
                            $karakteristika_xml->setAttribute("ime", PartnerExport::string_format($karak->name));
                            PartnerExport::xml_node($xml,"vrednost",PartnerExport::string_format($karak->value),$karakteristika_xml);
                            $characteristic_group->appendChild($karakteristika_xml);
                        }
                        $characteristics->appendChild($characteristic_group);
                    }
                    $product->appendChild($characteristics);
                }

                $characteristics = $xml->createElement("filteri");
                foreach(PartnerExport::characteristics($article->roba_id,1) as $karakteristika){
                        $characteristic_group = $xml->createElement("filter_grupa");
                        $characteristic_group->setAttribute("ime", PartnerExport::string_format($karakteristika->group));
                        
                        foreach($karakteristika->characteristic as $karak){
                            $karakteristika_xml = $xml->createElement("filter");
                            $karakteristika_xml->setAttribute("ime", PartnerExport::string_format($karak->name));
                            PartnerExport::xml_node($xml,"vrednost",PartnerExport::string_format($karak->value),$karakteristika_xml);
                            $characteristic_group->appendChild($karakteristika_xml);
                        }
                        $characteristics->appendChild($characteristic_group);
                    }
                    $product->appendChild($characteristics);


            }

            $root->appendChild($product);

        }

        $xml->formatOutput = true;
        $content = $xml->saveXML();

        $response = Response::make($content,200);
        $response->header('Content-Type', 'text/xml');
        return $response;
    }

}