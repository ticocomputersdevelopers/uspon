<?php

class ApiCategoryController extends Controller {

	public function categories(){
		$categories = Category::where('grupa_pr_id','>',0)->where(['web_b2c_prikazi'=>1,'parrent_grupa_pr_id'=>0])->with('childs.childs.childs')->orderBy('redni_broj','asc')->get();

		return Response::json(['success'=>true,'categories'=>$categories],200);
	}

	public function categoriesSave(){
        $data = Input::get();
        $regex = AdminSupport::regex();

        if(count($data) > 20000){
        	return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }
        if(DB::table('grupa_pr')->count() > 5000){
        	return Response::json(['success'=>false,'messages'=>'Max number of categories is 5000.'],200);
        }

        foreach($data as $row){
	        $rules = array(
	        	'id' => 'integer|exists:grupa_pr,grupa_pr_id',
		    	'external_code' => 'max:255',
		    	'name' => ((isset($row['id']) || isset($row['external_code'])) ? '' : 'required|').'regex:'.$regex.'|max:200',
	        	'parent_id' => 'integer|exists:grupa_pr,grupa_pr_id',
		    	'parent_external_code' => 'max:255|exists:grupa_pr,sifra_is',
		    	'show' => 'in:0,1',
		    	'show_b2b' => 'in:0,1'
	        );
	        $validator = Validator::make($row,$rules);
	        if($validator->fails()){
	        	return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
	        	break;
	        }
        }

        $mappedCategories = Category::mappedByExternalCode();
        foreach($data as $row){
        	$category = null;
        	if(isset($row['id'])){
	        	$category = Category::where('grupa_pr_id',$row['id'])->first();
        	}
        	else if(isset($row['external_code'])){
	        	$category = Category::where('sifra_is',$row['external_code'])->first();
        	}

        	if(is_null($category)){
	            $nextId = Category::max('grupa_pr_id')+1;
	            $category = new Category();
	            $category->grupa_pr_id = $nextId;
            	$category->sifra = $nextId;
	        	$category->grupa = !empty($row['name']) ? $row['name'] : (isset($row['external_code']) ? $row['external_code'] : '');
		    	$category->web_b2c_prikazi = isset($row['show']) ? $row['show'] : 0;
		    	$category->web_b2b_prikazi = isset($row['show_b2b']) ? $row['show_b2b'] : 0;
        		if(isset($row['parent_id'])){
        			$category->parrent_grupa_pr_id = $row['parent_id'];
        		}else if(isset($row['parent_external_code']) && isset($mappedCategories[$row['parent_external_code']])){
        			$category->parrent_grupa_pr_id = $mappedCategories[$row['parent_external_code']];
        		}else{
        			$category->parrent_grupa_pr_id = 0;
        		}
        	}else{
        		if(isset($row['name'])){
		    		$category->grupa = $row['name'];
        		}
        		if(isset($row['show'])){
		    		$category->web_b2c_prikazi = $row['show'];
        		}
        		if(isset($row['show_b2b'])){
		    		$category->web_b2b_prikazi = $row['show_b2b'];
        		}
        		if(isset($row['parent_id'])){
        			$category->parrent_grupa_pr_id = $row['parent_id'];
        		}else if(isset($row['parent_external_code']) && isset($mappedCategories[$row['parent_external_code']])){
        			$category->parrent_grupa_pr_id = $mappedCategories[$row['parent_external_code']];
        		}
        	}
        	
			if(isset($row['external_code'])){
	        	$category->sifra_is = $row['external_code'];
                $category->id_is = $row['external_code'];
        	}

        	$category->save();
        }

        return Response::json(['success'=>true],200);
	}

    public function categoriesDelete(){
        $data = Input::get();

        if(count($data) > 20000){
            return Response::json(['success'=>false,'messages'=>'Max input vars is 20000.'],200);
        }

        if(!isset($data[0])){
            return Response::json(['success'=>false,'messages'=>'Array of inputs is required.'],200);
        }

        if(isset($data[0]['id'])){
            foreach($data as $row){
                $rules = array(
                    'id' => 'required|integer'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }
        else if(isset($data[0]['external_code'])){
            foreach($data as $row){
                $rules = array(
                    'external_code' => 'required|max:255'
                );
                $validator = Validator::make($row,$rules);
                if($validator->fails()){
                    return Response::json(['success'=>false,'messages'=>$validator->messages()],200);
                    break;
                }
            }
        }else{
            return Response::json(['success'=>false,'messages'=>'Element require id or external_code.'],200);
        }


        if(isset($data[0]['id'])){
            $ids = array_map(function($item){ return $item['id']; },$data);
            DB::table('grupa_pr')->whereIn('grupa_pr_id',$ids)->delete();
        }
        else if(isset($data[0]['external_code'])){
            $codes = array_map(function($item){ return $item['external_code']; },$data);
            DB::table('grupa_pr')->whereIn('sifra_is',$codes)->delete();
        }

        return Response::json(['success'=>true],200);
    }
    public static function category_ajax() {
        echo View::make('shop/themes/'.Support::theme_path().'partials/categories/category',array());
    }


}