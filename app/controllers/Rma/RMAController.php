<?php
use Service\Mailer;

class RMAController extends Controller {

    public function rma(){
        $page=Input::get('page');
        $datum_od=Input::get('datum_od');
        $datum_do=Input::get('datum_do');
        $search=Input::get('search');
        $radni_nalog_status_id=Input::get('status_id');
        $partner_id=Input::get('partner_id');
        $serviser_id=Input::get('serviser_id');
        $rezervni_deo_id=Input::get('rezervni_deo_id');
        $otpis=Input::get('otpis');
        $ostecen=Input::get('ostecen');
        $izvestaj=Input::get('izvestaj');
        $izvestaj_sve=Input::get('izvestaj_sve');


        $sort_column = Input::get('sort_column') != null && Input::get('sort_column') != '' ? Input::get('sort_column') : 'radni_nalog_id';
        $sort_direction = Input::get('sort_direction') != null && Input::get('sort_direction') != '' ? Input::get('sort_direction') : 'DESC';


        if(is_null($datum_od) || $datum_od=='' || $datum_od=='null'){
            $datum_od=null;
        }
        if(is_null($datum_do) || $datum_do=='' || $datum_do=='null'){
            $datum_do=null;
        }
        if(is_null($search) || $search=='' || $search=='null'){
            $search=null;
        }
        if(is_null($radni_nalog_status_id) || $radni_nalog_status_id=='' || $radni_nalog_status_id=='0'){
            $radni_nalog_status_id=null;
        }
        if(is_null($otpis) || $otpis=='' || $otpis=='null'){
            $otpis=null;
        }
        if(is_null($ostecen) || $ostecen=='' || $ostecen=='null'){
            $ostecen=null;
        }
        if(is_null($partner_id) || $partner_id==''){
            $partner_ids=array();
        }else{
            $partner_ids=explode('-',$partner_id);
        }
        if(is_null($serviser_id) || $serviser_id==''){
            $serviser_ids=array();
        }else{
            $serviser_ids=explode('-',$serviser_id);
        }

        $limit = 20;
        $radni_nalozi=RMA::getRadniNalozi($datum_od,$datum_do,$search,$radni_nalog_status_id,$otpis,$ostecen,$partner_ids,$serviser_ids,$sort_column,$sort_direction,$limit,!is_null($page) ? $page : 1);

        if($izvestaj==1){
            $data = array(
                array('BROJ','SRN','MODEL UREĐAJA','SN','DATUM KUPOVINE','NAZIV MALOPRODAJE','DATUM REKLAMACIJE','OPIS KVARA','OPIS RADA','REZERVNI DEO','NC+PDV','KOMENTAR')
            );
            foreach($radni_nalozi->all as $key => $item){
                $radni_nalog_cene = RMA::radni_nalog_cene($item->radni_nalog_id);

                $rezervni_delovi = array();
                foreach(DB::select("SELECT (SELECT naziv_web FROM roba WHERE roba_id = radni_nalog_rezervni_deo.roba_id) as rezervni_deo_naziv FROM radni_nalog_rezervni_deo WHERE radni_nalog_id = ".$item->radni_nalog_id."") as $radni_nalog_rezervni_deo){
                    $rezervni_delovi[] = $radni_nalog_rezervni_deo->rezervni_deo_naziv;
                }

                $data[] = array(
                    ($key+1),$item->broj_naloga,$item->uredjaj,$item->serijski_broj,$item->datum_kupovine,$item->maloprodaja,$item->datum_prijema,$item->opis_kvara,$item->napomena_operacije,implode("\n",$rezervni_delovi),$radni_nalog_cene->rezervni_delovi,''
                );
            }
            
            $doc = new PHPExcel();
            $doc->setActiveSheetIndex(0);
            $doc->getActiveSheet()->fromArray($data);
             
            $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

            $store_path = 'files/rn_izvestaj_'.date('Y-m-d').'.xls';
            $objWriter->save($store_path);
            return Redirect::to(RmaOptions::base_url().$store_path);
        }elseif ($izvestaj_sve==1) {
            $data = array(
                array('SRN','NAZIV SERVISA','PIB','ADRESA','E-MAIL','TELEFON','IME KUPCA','ADRESA','E-MAIL','TELEFON','SERVISER','STATUS','MODEL UREĐAJA','SN','BROJ FISKALNOG RAČUNA','DATUM KUPOVINE','DATUM REKLAMACIJE','DATUM IZLASKA NA SERVIS','DATUM ZAVRŠETKA','OPIS KVARA','NAPOMENA','UZROK KVARA','OPIS RADA','OTPIS','OŠTEĆEN','OPERACIJE','IZNOS','REZERVNI DEO','NC+PDV')
            );
            foreach($radni_nalozi->all as $item){
                $radni_nalog_cene = RMA::radni_nalog_cene($item->radni_nalog_id);
                $servis = RMA::partner($item->partner_id);
                $serviser = RMA::serviser($item->primio_serviser_id);

                if(!is_null($item->b2b_partner_id)){
                    $kupac = RMA::partner($item->b2b_partner_id);
                    $kupac->ime = $kupac->naziv;
                    $kupac->prezime = $kupac->pib;
                    $kupac->email = $kupac->mail;
                }elseif(!is_null($item->kupac_id)){
                    $kupac = RMA::kupac($item->kupac_id);
                    if($kupac->flag_vrsta_kupca == 1){
                        $kupac->ime = $kupac->naziv;
                        $kupac->prezime = $kupac->pib;
                    }
                }else{
                    $kupac = (object) [
                        'ime' => '',
                        'prezime' => '',
                        'email' => '',
                        'adresa' => '',
                        'telefon' => ''
                    ];
                }

                $operacije = array();
                foreach(DB::select("SELECT (SELECT (SELECT naziv FROM operacija WHERE operacija_id = operacija_servis.operacija_id) AS naziv FROM operacija_servis WHERE operacija_servis_id = radni_nalog_operacije.operacija_servis_id) as naziv, norma_sat FROM radni_nalog_operacije WHERE radni_nalog_id = ".$item->radni_nalog_id."") as $radni_nalog_operacije){
                    $operacije[] = $radni_nalog_operacije->naziv.' x'.$radni_nalog_operacije->norma_sat;
                }
                $rezervni_delovi = array();
                foreach(DB::select("SELECT (SELECT naziv_web FROM roba WHERE roba_id = radni_nalog_rezervni_deo.roba_id) as rezervni_deo_naziv, kolicina FROM radni_nalog_rezervni_deo WHERE radni_nalog_id = ".$item->radni_nalog_id."") as $radni_nalog_rezervni_deo){
                    $rezervni_delovi[] = $radni_nalog_rezervni_deo->rezervni_deo_naziv.' x'.$radni_nalog_rezervni_deo->kolicina;
                }

                $data[] = array(
                    $item->broj_naloga,$servis->naziv,$servis->pib,$servis->adresa,$servis->mail,$servis->telefon,($kupac->ime.' '.$kupac->prezime),$kupac->adresa,$kupac->email,$kupac->telefon,(!is_null($serviser) ? $serviser->ime.' '.$serviser->prezime : ''),RMA::getStatusTitle($item->status_id),$item->uredjaj,$item->serijski_broj,$item->broj_fiskalnog_racuna,$item->datum_kupovine,$item->datum_prijema,$item->datum_servisiranja,$item->datum_zavrsetka,$item->opis_kvara,$item->rn_napomena,$item->uzrok_kvara,$item->napomena_operacije,($item->otpis == 1 ? 'DA' : 'NE'),($item->ostecen == 1 ? 'DA' : 'NE'),implode("\n",$operacije),$radni_nalog_cene->cena_rada,implode("\n",$rezervni_delovi),$radni_nalog_cene->rezervni_delovi
                );

            }
      
            $doc = new PHPExcel();
            $doc->setActiveSheetIndex(0);
            $doc->getActiveSheet()->fromArray($data);
             
            $objWriter = PHPExcel_IOFactory::createWriter($doc, 'Excel5');

            $store_path = 'files/rn_izvestaj_detaljni_'.date('Y-m-d').'.xls';
            $objWriter->save($store_path);
            return Redirect::to(RmaOptions::base_url().$store_path);            
        }

        $data=array(
            "strana"=>'rma',
            "title"=> 'RMA',
            "radni_nalozi" => $radni_nalozi->rows,
            "count" => $radni_nalozi->count,
            "limit" => $limit,
            "search" => !is_null($search) ? $search : '',
            "datum_od" => $datum_od,
            "datum_do" => $datum_do,
            "radni_nalog_status_id" => $radni_nalog_status_id,
            "partner_ids" => $partner_ids,
            "serviser_ids" => $serviser_ids,
            "otpis" => $otpis,
            "ostecen" => $ostecen,
            "sort_column" => $sort_column,
            "sort_direction" => $sort_direction
        );
        return View::make('rma/pages/rma', $data);
    }

    public function radni_nalog_partner_podaci(){
        $partner_id = Input::get('partner_id');

        $data = array(
            'partner_details'=>RMA::partnerDetails($partner_id),
            'serviseri_select'=>RMA::serviseriSelect($partner_id)
        );
        return json_encode($data);
    }

    public function radni_nalog_kupac_podaci(){
        $kupac_id = Input::get('kupac_id');

        if(Input::get('vrsta_kupca') == 'b2c'){
            return RMA::kupacDetails($kupac_id);
        }else{
            return RMA::partnerDetails($kupac_id);
        }
    }


    public function radni_nalog_prijem($id){

        $data=array(
            "strana"=>'radni_nalog_prijem',
            "title"=> 'Radni Nalog Prijem',
        );

        if($id == 0){
            $kupac = (object) array('web_kupac_id'=>0);
            $partner = DB::table('partner')->leftJoin('partner_je','partner_je.partner_id','=','partner.partner_id')->rightJoin('serviser','serviser.partner_id','=','partner.partner_id')->where('partner_je.partner_vrsta_id',118);
            if($user = RmaOptions::user('servis')){
                $partner = $partner->where('partner.partner_id',$user->partner_id);
            }
            $partner = $partner->orderBy('partner.partner_id','asc')->first();

            if(is_null($partner)){
                $partner = (object) array('partner_id'=>0);
            }
            $data["radni_nalog"] = RMA::radni_nalog_null((!is_null($kupac) ? $kupac->web_kupac_id : 0),(!is_null($partner) ? $partner->partner_id : 0));
            $data["radni_nalog_troskovi"] = array();
        }else{
            $radni_nalog = DB::table('radni_nalog')->where('radni_nalog_id',$id)->first();
            $radni_nalog->{'vrsta_kupca'} = !is_null($radni_nalog->kupac_id) ? 'b2c' : 'b2b';

            $partner = DB::table('partner')->where('partner_id',$radni_nalog->partner_id)->first();
            if(!is_null($radni_nalog->kupac_id)){
                $kupac = DB::table('web_kupac')->where('web_kupac_id',$radni_nalog->kupac_id)->first();
            }else{
                $kupac = DB::table('partner')->where('partner_id',$radni_nalog->b2b_partner_id)->first();
            }
            $data["radni_nalog"] = $radni_nalog;

            $data["radni_nalog_troskovi"] = DB::table('radni_nalog_trosak')->where('radni_nalog_id',$id)->orderBy('radni_nalog_trosak_id','asc')->get();
        }
        $data["partner"] = $partner;
        $data["kupac"] = $kupac;

        return View::make('rma/pages/radni_nalog_prijem', $data);
    }

    public function radni_nalog_prijem_dokument($id,$trosak=0){
        if($id <= 0){
            return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-prijem/0');
        }

        $radni_nalog = DB::table('radni_nalog')->where('radni_nalog_id',$id)->first();
        $data = array();
        $data["radni_nalog"] = $radni_nalog;
        $data["kupac"] = !is_null($radni_nalog->kupac_id) ? DB::table('web_kupac')->where('web_kupac_id',$radni_nalog->kupac_id)->first() : DB::table('partner')->where('partner_id',$radni_nalog->b2b_partner_id)->first();
        $data["radni_nalog_operacije"] = DB::table('radni_nalog_operacije')->leftJoin('operacija_servis','radni_nalog_operacije.operacija_servis_id','=','operacija_servis.operacija_servis_id')->leftJoin('tarifna_grupa','tarifna_grupa.tarifna_grupa_id','=','operacija_servis.tarifna_grupa_id')->where('radni_nalog_id',$id)->orderBy('radni_nalog_operacije_id','asc')->get();

        $data["radni_nalog_rezervni_delovi"] = DB::table('radni_nalog_rezervni_deo')->where('radni_nalog_id',$id)->orderBy('radni_nalog_rezervni_deo_id','asc')->get();

        $data["radni_nalog_troskovi"] = $trosak==1 ? DB::table('radni_nalog_trosak')->where('radni_nalog_id',$id)->orderBy('radni_nalog_trosak_id','asc')->get() : array();

        $data["cene"] = RMA::radni_nalog_cene($id,($trosak==1));

        $pdf = App::make('dompdf');
        $pdf->loadView('rma/pages/radni_nalog_prijem_dokument', $data);

        return $pdf->stream();
    }

    public function radni_nalog_prijem_post(){
        $data = Input::get();

        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'not_in' => 'Niste popunili polje!',
            'max' => 'Dužina sadržaja nije dozvoljena!',
            'integer' => 'Vrednost sadržaja nije dozvoljena!',
            'digits_between' => 'Dužina sadržaja nije dozvoljena!',
            'date' => 'Neodgovarajući format!'
        );

        $validator_arr = array(
            'partner_id' => 'required|integer',
            'kupac_ime' => 'required|regex:'.RmaSupport::regex().'|max:255',
            'primio_serviser_id' => 'required|integer|not_in:0',
            'datum_prijema' => 'required',
            'uredjaj' => 'required|regex:'.RmaSupport::regex().'|max:500',
            'proizvodjac' => 'regex:'.RmaSupport::regex().'|max:255',
            'serijski_broj' => 'regex:'.RmaSupport::regex().'|max:255',
            'napomena' => 'regex:'.RmaSupport::regex().'|max:500',
            'opis_kvara' => 'required|regex:'.RmaSupport::regex().'|max:1000',
            'datum_kupovine' => 'date',
            'broj_fiskalnog_racuna' => 'regex:'.RmaSupport::regex().'|max:500',
            'datum_servisiranja' => 'date',
            'telefon' => 'regex:'.RmaSupport::regex().'|between:3,15',
            'email' => 'email|unique:web_kupac,email,'.$data['kupac_id'].',web_kupac_id,status_registracije,1|between:5,50',
            'fax' => 'between: 3, 20|regex:'.RmaSupport::regex().'',
            'telefon_mobilni' => 'between: 3, 20|regex:'.RmaSupport::regex().''   
        );
        if($data['check_serijski_broj']==1){
            $validator_arr['serijski_broj'] = 'regex:'.RmaSupport::regex().'|max:255|unique:radni_nalog,serijski_broj,'.$data['radni_nalog_id'].',radni_nalog_id';
        }
        if($data['radni_nalog_id']>0){
            unset($validator_arr['kupac_ime']);
        }

        $validator = Validator::make($data, $validator_arr, $messages);
        if ($validator->passes()) {
            if(is_null($data['datum_kupovine']) || $data['datum_kupovine'] == ''){
                $data['datum_kupovine'] = null;
            }
            if(is_null($data['datum_servisiranja']) || $data['datum_servisiranja'] == ''){
                $data['datum_servisiranja'] = null;
            }
            // if($data['partner_proizvodjac_id'] == 'null'){
            //     $data['partner_proizvodjac_id'] = null;
            // }

            if(!(isset($data['kupac_id']) && $data['kupac_id'] > 0)){
                if($data['vrsta_kupca'] == 'b2c'){
                    $kupacImeArr = explode(' ',trim($data['kupac_ime']));
                    $kupac_data = array(
                        'ime' => $kupacImeArr[0],
                        'prezime' => isset($kupacImeArr[1]) && $kupacImeArr[1] != '' ? $kupacImeArr[1] : '',
                        'adresa' => $data['kupac_adresa'],
                        'mesto' => $data['kupac_mesto'],
                        'telefon' => $data['kupac_telefon'],
                        'email' => $data['kupac_email'],
                        'lozinka' => '' 
                    );
                    DB::table('web_kupac')->insert($kupac_data);
                    $data['kupac_id'] = DB::select("SELECT currval('web_kupac_web_kupac_id_seq') as new_id")[0]->new_id;
                    $data['b2b_partner_id'] = null;
                }
                else if($data['vrsta_kupca'] == 'b2b'){
                    $kupac_data = array(
                        'naziv' => $data['kupac_ime'],
                        'adresa' => $data['kupac_adresa'],
                        'mesto' => $data['kupac_mesto'],
                        'telefon' => $data['kupac_telefon'],
                        'mail' => $data['kupac_email'],
                        'password' => '',
                        'drzava_id' => 0
                    );
                    DB::table('partner')->insert($kupac_data);
                    $data['b2b_partner_id'] = DB::select("SELECT currval('partner_partner_id_seq') as new_id")[0]->new_id;
                    $data['kupac_id'] = null;
                }
            }

            if($data['vrsta_kupca'] == 'b2c'){
                $data['b2b_partner_id'] = null;
            }else if($data['vrsta_kupca'] == 'b2b'){
                $data['b2b_partner_id'] = $data['kupac_id'];
                $data['kupac_id'] = null;
            }
            $data['pregledan'] = 1;

            unset($data['kupac_ime']);
            unset($data['kupac_adresa']);
            unset($data['kupac_mesto']);
            unset($data['kupac_telefon']);
            unset($data['kupac_email']);
            unset($data['check_serijski_broj']);
            unset($data['vrsta_kupca']);

            if($data['radni_nalog_id']==0){
                $next_id = DB::select("SELECT nextval('radni_nalog_radni_nalog_id_seq') as new_id")[0]->new_id;
                $data['broj_naloga'] = 'SRN'.str_pad($next_id, 4, '0', STR_PAD_LEFT);
                $data['radni_nalog_id'] = $next_id;
                $data['status_id'] = 1;

                DB::table('radni_nalog')->insert($data);

                AdminSupport::saveLog('RADNI_NALOG_DODAJ', array(DB::table('radni_nalog')->max('radni_nalog_id')));
                $partner = RMA::partner($data['partner_id']);
                if(!empty($partner->mail)){
                    // Mailer::send('info@tico.rs',$partner->mail,'Novi radni nalog '.RmaOptions::company_name(),'Dobili ste novi radni nalog br: '.$data['broj_naloga']);
                    Mailer::send('rma@uspon.rs',$partner->mail,'Novi radni nalog '.RmaOptions::company_name(),'Dobili ste novi radni nalog br: '.$data['broj_naloga']);
                    Mailer::send($partner->mail,'rma@uspon.rs','Novi radni nalog '.RmaOptions::company_name(),'Dobili ste novi radni nalog br: '.$data['broj_naloga']);
                }
            }else{
                if(DB::table('radni_nalog')->where('radni_nalog_id',$data['radni_nalog_id'])->pluck('status_id') == 4){
                    return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-prijem/'.$data['radni_nalog_id'])->with('error_message','Radni nalog je realizovan.');                    
                }

                unset($data['kupac_id']);
                unset($data['b2b_partner_id']);
                DB::table('radni_nalog')->where('radni_nalog_id',$data['radni_nalog_id'])->update($data);
                AdminSupport::saveLog('RADNI_NALOG_IZMENI', array($data['radni_nalog_id']));
            }

            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-prijem/'.$data['radni_nalog_id'])->with('message',$message);
        }else{
            return Redirect::back()->withInput()->withErrors($validator);
        }
    }

    public function radni_nalog_opis_rada_post() {
        $data = Input::get();
        $radni_nalog_id = $data['radni_nalog_id'];
        
        if(DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_id)->pluck('status_id') == 4){
            return Redirect::back()->with('error_message','Radni nalog je realizovan.');                    
        }

        if(isset($data['otpis'])){
            $data['otpis'] = 1;
        }else{
            $data['otpis'] = 0;
        }
        if(isset($data['ostecen'])){
            $data['ostecen'] = 1;
        }else{
            $data['ostecen'] = 0;
        }

        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'in' => 'Niste popunili polje!',
            'max' => 'Dužina sadržaja nije dozvoljena!',
            'integer' => 'Vrednost sadržaja nije dozvoljena!'
        );
        $validator_arr = array(
            'uzrok_kvara' => 'regex:'.RmaSupport::regex().'|max:500',
            'napomena_operacije' => 'regex:'.RmaSupport::regex().'|max:500',
            'otpis' => 'required|integer|in:0,1',
            'ostecen' => 'required|integer|in:0,1'
        );

        $validator = Validator::make($data, $validator_arr, $messages);
        if ($validator->passes()) {
            unset($data['radni_nalog_id']);
            DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_id)->update($data);

            $message='Uspešno ste sačuvali podatke.';
            return Redirect::back()->with('message',$message);
        }
        return Redirect::back()->withInput()->withErrors($validator);
    }

    public function kupci_search() {
        $search = trim(Input::get('kupci'));
        $reci = explode(" ",$search);

        if(Input::get('vrsta_kupca') == 'b2c'){
            $imeFormatiran = "";
            $prezimeFormatiran = "";
            $emailFormatiran = "";
            $adresaFormatiran = "";

            foreach($reci as $rec){
                $imeFormatiran .= "ime ILIKE '%" . $rec . "%' OR ";
                $prezimeFormatiran .= "prezime ILIKE '%" . $rec . "%' OR ";
                $emailFormatiran .= "email ILIKE '%" . $rec . "%' OR ";
                $adresaFormatiran .= "adresa ILIKE '%" . $rec . "%' OR ";
            }

            $kupci = "SELECT * FROM web_kupac WHERE (" . substr($prezimeFormatiran,0,-3) . " OR " . substr($imeFormatiran,0,-3) . " OR " . substr($emailFormatiran,0,-3) . " OR " . substr($adresaFormatiran,0,-3) . ") ORDER BY ime ASC";


            header('Content-type: text/plain; charset=utf-8');          
            $list = "<ul class='kupac_list'>";
            foreach (DB::select($kupci) as $kupac) {
                $list .= "
                    <li class='kupac_list__item' data-web_kupac_id='".$kupac->web_kupac_id."'>
                        <div class='kupac_list__item__link'>"
                            ."<span class='kupac_list__item__link__text'>" . $kupac->ime." ". $kupac->prezime . "</span>"
                            ."<span class='kupac_list__item__link__cat'>" . $kupac->email . "</span>"
                            ."<span class='kupac_list__item__link__small'>" . $kupac->adresa . "</span>
                        </div>
                    </li>";
            }
            $list .= "</ul>";
        }else{
            $nazivFormatiran = "";
            $pibFormatiran = "";
            $emailFormatiran = "";
            $adresaFormatiran = "";

            foreach($reci as $rec){
                $nazivFormatiran .= "naziv ILIKE '%" . $rec . "%' OR ";
                $pibFormatiran .= "pib ILIKE '%" . $rec . "%' OR ";
                $emailFormatiran .= "mail ILIKE '%" . $rec . "%' OR ";
                $adresaFormatiran .= "adresa ILIKE '%" . $rec . "%' OR ";
            }

            $kupci = "SELECT * FROM partner WHERE (" . substr($pibFormatiran,0,-3) . " OR " . substr($nazivFormatiran,0,-3) . " OR " . substr($emailFormatiran,0,-3) . " OR " . substr($adresaFormatiran,0,-3) . ") ORDER BY naziv ASC";


            header('Content-type: text/plain; charset=utf-8');          
            $list = "<ul class='kupac_list'>";
            foreach (DB::select($kupci) as $kupac) {
                $list .= "
                    <li class='kupac_list__item' data-web_kupac_id='".$kupac->partner_id."'>
                        <div class='kupac_list__item__link'>"
                            ."<span class='kupac_list__item__link__text'>" . $kupac->naziv . "</span>"
                            ."<span class='kupac_list__item__link__cat'>" . $kupac->mail . "</span>"
                            ."<span class='kupac_list__item__link__small'>" . $kupac->adresa . "</span>
                        </div>
                    </li>";
            }
            $list .= "</ul>";
        }

        echo $list; 
    }

    public function radni_nalog_delete($radni_nalog_id){
        if(DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_id)->pluck('status_id') == 4){
            return Redirect::back()->with('error_message','Radni nalog je realizovan.');                    
        }

        DB::table('radni_nalog_operacije')->where('radni_nalog_id',$radni_nalog_id)->delete();
        if(DB::table('radni_nalog_trosak')->select('radni_nalog_trosak_id')->where('radni_nalog_id',$radni_nalog_id)->count() > 0){
            DB::table('radni_nalog_trosak_dokument')->whereIn('radni_nalog_trosak_id',array_map('current',DB::table('radni_nalog_trosak')->select('radni_nalog_trosak_id')->where('radni_nalog_id',$radni_nalog_id)->get()))->delete();
            DB::table('radni_nalog_trosak')->where('radni_nalog_id',$radni_nalog_id)->delete();
        }
        DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_id)->delete();
        return Redirect::back()->with('message','Uspešno ste obrisali stavku.');
    }
    
    public function radni_nalog_opis_rada($id){

        $data=array(
            "strana"=>'radni_nalog_opis_rada',
            "title"=> 'Radni Nalog Opis Rada',
        );

        if($id > 0){
            $data["radni_nalog"] = DB::table('radni_nalog')->where('radni_nalog_id',$id)->first();
            $data["radni_nalog_operacije"] = DB::table('radni_nalog_operacije')->where('radni_nalog_id',$id)->get();

            $radni_nalog_operacije_ukupno = DB::table('radni_nalog_operacije')->where('radni_nalog_id',$id)->sum('iznos');
            $data["radni_nalog_operacije_ukupno"] = $radni_nalog_operacije_ukupno;

            //rezervni delovi
            $data["radni_nalog_rezervni_delovi"] = DB::table('radni_nalog_rezervni_deo')->where('radni_nalog_id',$id)->get();
            $radni_nalog_rezervni_deo_ukupno = DB::table('radni_nalog_rezervni_deo')->where('radni_nalog_id',$id)->sum('iznos');
            $data["radni_nalog_rezervni_deo_ukupno"] = $radni_nalog_rezervni_deo_ukupno;

            $radni_nalog_trosak_ukupno = DB::table('radni_nalog_trosak')->where('radni_nalog_id',$id)->sum('vrednost_racuna');
            $data["radni_nalog_trosak_ukupno"] = (object) array('osnovica'=> $radni_nalog_trosak_ukupno,'pdv'=>$radni_nalog_trosak_ukupno * 0.2,'ukupno'=>$radni_nalog_trosak_ukupno * 1.2);
            $data["radni_nalog_ukupno"] = $radni_nalog_operacije_ukupno + ($radni_nalog_trosak_ukupno * 1.2) + ($radni_nalog_rezervni_deo_ukupno * 1.2);

            $data["radni_nalog_troskovi"] = DB::table('radni_nalog_trosak')->where('radni_nalog_id',$id)->orderBy('radni_nalog_trosak_id','asc')->get();
        }else{
            return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-prijem/0');
        }

        return View::make('rma/pages/radni_nalog_opis_rada', $data);
    }

    //operacije
    public function radni_nalog_operacija_modal_content(){
        $data = Input::get();
        $radni_nalog_operacije_id = $data['radni_nalog_operacije_id'];
        $radni_nalog_id = $data['radni_nalog_id'];

        $radni_nalog = RMA::getRadniNalog($radni_nalog_id);

        if($radni_nalog_operacije_id > 0){
            $radni_nalog_operacije = DB::table('radni_nalog_operacije')->where('radni_nalog_operacije_id',$radni_nalog_operacije_id)->first();
        }else{
            $iznos = 0.00;
            $naziv = '';
            $partner_id = $radni_nalog->partner_id;
            $partner_id = 1;

            $grupe = DB::select("SELECT DISTINCT operacija_grupa.operacija_grupa_id AS operacija_grupa_id, operacija_grupa.naziv FROM operacija_servis LEFT JOIN operacija ON operacija.operacija_id = operacija_servis.operacija_id LEFT JOIN operacija_grupa ON operacija_grupa.operacija_grupa_id = operacija.operacija_grupa_id WHERE partner_id=".$partner_id." ORDER BY operacija_grupa.naziv ASC");
            if(isset($grupe[0])){
                $operacije = DB::select("SELECT DISTINCT operacija.operacija_id AS operacija_id, operacija.naziv AS naziv FROM operacija_servis LEFT JOIN operacija ON operacija.operacija_id = operacija_servis.operacija_id WHERE partner_id=".$partner_id." AND operacija.operacija_grupa_id = ".$grupe[0]->operacija_grupa_id." ORDER BY operacija.naziv ASC");
                if(isset($operacije[0])){
                    $iznos = DB::table('operacija_servis')->where('operacija_id',$operacije[0]->operacija_id)->pluck('cena');
                    $naziv = $operacije[0]->naziv;
                }
            }

            $radni_nalog_operacije = (object) array('radni_nalog_operacije_id' => 0,'serviser_id' => null, 'operacija_servis_id' => null, 'opis_operacije' => $naziv, 'norma_sat' => 1.00, 'iznos' => $iznos, 'cena_sata' => 0.00);
        }


        $data = array(
            'radni_nalog'=>$radni_nalog,
            'radni_nalog_operacije'=>$radni_nalog_operacije,
        );
        return View::make('rma/partials/ajax/radni_nalog_operacija_modal_content',$data)->render();
    }

    public function radni_nalog_operacija_select(){
        $data = Input::get();
        $operacija_grupa_id = $data['operacija_grupa_id'];
        $partner_id = $data['partner_id'];
        $partner_id = 1;

        $operacijeSelect = RMA::operacijeSelect($partner_id,$operacija_grupa_id);
        $operacija_servis = DB::table('operacija_servis')->where(array('operacija_id'=>$operacijeSelect->first_operacija_id,'partner_id'=>$partner_id))->first();
        // $iznos = $operacija_servis->cena * (1 + DB::table('tarifna_grupa')->where('tarifna_grupa_id',$operacija_servis->tarifna_grupa_id)->pluck('porez') / 100);
        $iznos = $operacija_servis->cena;

        $response = array(
            'operacije_select' => $operacijeSelect->select_content,
            'operacija_naziv' => DB::table('operacija')->where('operacija_id',$operacijeSelect->first_operacija_id)->pluck('naziv'),
            'iznos' => $iznos
        );

        return Response::json($response);
    }

    public function radni_nalog_operacija_change(){
        $data = Input::get();
        $operacija_id = $data['operacija_id'];
        $partner_id = $data['partner_id'];
        $partner_id = 1;

        $operacija_servis = DB::table('operacija_servis')->where(array('operacija_id'=>$operacija_id,'partner_id'=>$partner_id))->first();
        // $iznos = $operacija_servis->cena * (1 + DB::table('tarifna_grupa')->where('tarifna_grupa_id',$operacija_servis->tarifna_grupa_id)->pluck('porez') / 100);
        $iznos = $operacija_servis->cena;

        return json_encode(array(
            'iznos' => $iznos
        ));
    }

    public function radni_nalog_operacija_save(){
        $data = Input::get();
        $data['partner_id'] = 1;

        $radni_nalog_operacije_id = $data['radni_nalog_operacije_id'];
        $operacija_servis = DB::table('operacija_servis')->where(array('operacija_id'=>$data['operacija_id'],'partner_id'=>$data['partner_id']))->first();
        
        $data['operacija_servis_id'] = $operacija_servis->operacija_servis_id;
        $data['norma_sat'] = $data['norma_sat'] == '' ? 0.00 : $data['norma_sat'];
        $data['opis_operacije'] = $data['opis_operacije'] == '' ? DB::table('operacija')->where(array('operacija_id'=>$data['operacija_id']))->pluck('naziv') : $data['opis_operacije'];
        $data['cena_sata'] = $operacija_servis->cena;

        unset($data['radni_nalog_operacije_id']);
        unset($data['operacija_id']);
        unset($data['partner_id']);

        if(DB::table('radni_nalog')->where('radni_nalog_id',$data['radni_nalog_id'])->pluck('status_id') == 4){
            return json_encode(array(
                'success' => false,
                'error_message' => 'Radni nalog je realizovan.'
            ));                  
        }
        if($radni_nalog_operacije_id > 0){
            DB::table('radni_nalog_operacije')->where('radni_nalog_operacije_id',$radni_nalog_operacije_id)->update($data);
        }else{
            DB::table('radni_nalog_operacije')->insert($data);
        }
        return json_encode(array(
            'success' => true
        )); 
    }

    public function radni_nalog_operacija_delete($radni_nalog_operacije_id){
        $radni_nalog_operacije = DB::table('radni_nalog_operacije')->where('radni_nalog_operacije_id',$radni_nalog_operacije_id)->first();
        if(DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_operacije->radni_nalog_id)->pluck('status_id') == 4){
            return Redirect::back()->with('error_message','Radni nalog je realizovan.');                    
        }
        DB::table('radni_nalog_operacije')->where('radni_nalog_operacije_id',$radni_nalog_operacije_id)->delete();
        return Redirect::back()->with('message','Uspešno ste obrisali operaciju.');
    }

    //trosak
    public function radni_nalog_trosak_modal_content(){
        $data = Input::get();
        $radni_nalog_trosak_id = $data['radni_nalog_trosak_id'];
        $radni_nalog_id = $data['radni_nalog_id'];

        $radni_nalog = RMA::getRadniNalog($radni_nalog_id);

        if($radni_nalog_trosak_id > 0){
            $radni_nalog_trosak = DB::table('radni_nalog_trosak')->where('radni_nalog_trosak_id',$radni_nalog_trosak_id)->first();
        }else{
            $radni_nalog_trosak = (object) array('radni_nalog_trosak_id' => 0,'partner_id' => null,'datum_racuna' => date('Y-m-d'),'vrednost_racuna' => 0.00,'broj_racuna' => null,'zaduzuje_kupca' => 0,'opis' => null);
        }

        $data = array(
            'radni_nalog'=>$radni_nalog,
            'radni_nalog_trosak'=>$radni_nalog_trosak,
        );
        return View::make('rma/partials/ajax/radni_nalog_trosak_modal_content',$data)->render();
    }

    public function radni_nalog_trosak_save(){
        $data = Input::get();
        if(DB::table('radni_nalog')->where('radni_nalog_id',$data['radni_nalog_id'])->pluck('status_id') == 4){
            $result = array(
                'success' => false,
                'errors' => array(),
                'error_message' => 'Radni nalog je realizovan.'
            );
            return Response::json($result);    
        }

        $radni_nalog_trosak_id = $data['radni_nalog_trosak_id'];
        unset($data['radni_nalog_trosak_id']);

        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'not_in' => 'Niste popunili polje!'
        );

        $validator_arr = array(
            'partner_id' => 'required|integer',
            'broj_racuna' => 'required|regex:'.RmaSupport::regex().'|max:20',
            'datum_racuna' => 'required',
            'vrednost_racuna' => 'required|numeric|digits_between:0,10',
            'opis' => 'required|regex:'.RmaSupport::regex().'|max:500'
        );

        $validator = Validator::make($data, $validator_arr, $messages);
        if ($validator->passes()) {
            if($radni_nalog_trosak_id > 0){
                DB::table('radni_nalog_trosak')->where('radni_nalog_trosak_id',$radni_nalog_trosak_id)->update($data);
            }else{
                DB::table('radni_nalog_trosak')->insert($data);
            }
            $result = array(
                'success' => true,
                'errors' => array()
            );

        }else{
            $result = array(
                'success' => false,
                'errors' => $validator->messages()
            );
        }        
        return Response::json($result);
    }

    public function radni_nalog_trosak_delete($radni_nalog_trosak_id){
        $radni_nalog_trosak = DB::table('radni_nalog_trosak')->where('radni_nalog_trosak_id',$radni_nalog_trosak_id)->first();
        if(DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_trosak->radni_nalog_id)->pluck('status_id') == 4){
            return Redirect::back()->with('error_message','Radni nalog je realizovan.');                    
        }

        DB::table('radni_nalog_trosak_dokument')->where('radni_nalog_trosak_id',$radni_nalog_trosak_id)->delete();
        DB::table('radni_nalog_trosak')->where('radni_nalog_trosak_id',$radni_nalog_trosak_id)->delete();
        return Redirect::back()->with('message','Uspešno ste obrisali stavku.');
    }

    public function radni_nalog_opreacija_uzrok_save(){
        $data = Input::get();
        $radni_nalog_id = $data['radni_nalog_id'];
        if(DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_id)->pluck('status_id') == 4){
            $result = array(
                'success' => false,
                'error_message' => 'Radni nalog je realizovan.'
            );
            return Response::json($result);    
        }
        if(strlen($data['uzrok_kvara']) > 500){
            $result = array(
                'success' => false,
                'error_message' => 'Dužina sadržaja je neodgovarajuća.'
            );
            return Response::json($result);    
        }

        $uzrok_kvara = $data['uzrok_kvara'];
        if($radni_nalog_id > 0){
            DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_id)->update(array('uzrok_kvara'=>$uzrok_kvara ));
            return Response::json(array(
                'success' => true
            ));
        }
    }
    public function radni_nalog_opreacija_napomena_save(){
        $data = Input::get();
        $radni_nalog_id = $data['radni_nalog_id'];
        if(DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_id)->pluck('status_id') == 4){
            $result = array(
                'success' => false,
                'error_message' => 'Radni nalog je realizovan.'
            );
            return Response::json($result);    
        }
        if(strlen($data['napomena_operacije']) > 500){
            $result = array(
                'success' => false,
                'error_message' => 'Dužina sadržaja je neodgovarajuća.'
            );
            return Response::json($result);    
        }
        
        $napomena_operacije = $data['napomena_operacije'];
        if($radni_nalog_id > 0){
            DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_id)->update(array('napomena_operacije'=>$napomena_operacije ));
            return Response::json(array(
                'success' => true
            ));
        }
    }
    //rezervni_deo
    public function radni_nalog_rezervni_deo_modal_content(){
        $data = Input::get();
        $radni_nalog_rezervni_deo_id = $data['radni_nalog_rezervni_deo_id'];
        $radni_nalog_id = $data['radni_nalog_id'];

        $radni_nalog = RMA::getRadniNalog($radni_nalog_id);

        if($radni_nalog_rezervni_deo_id > 0){
            $radni_nalog_rezervni_deo = DB::table('radni_nalog_rezervni_deo')->where('radni_nalog_rezervni_deo_id',$radni_nalog_rezervni_deo_id)->first();
        }else{
            $radni_nalog_rezervni_deo = (object) array('radni_nalog_rezervni_deo_id' => 0,'roba_id' => 0,'cena' => 0.00,'kolicina' => 1,'iznos' => 0.00);
        }

        $data = array(
            'radni_nalog'=>$radni_nalog,
            'radni_nalog_rezervni_deo'=>$radni_nalog_rezervni_deo,
        );
        return View::make('rma/partials/ajax/radni_nalog_rezervni_deo_modal_content',$data)->render();
    }

    public function radni_nalog_rezervni_deo_roba_search() {
        $rec = trim(Input::get('articles'));

        $skuFormatiran = "r.sifra_is ILIKE '%" . $rec . "%' ";
        $nazivFormatiran = "naziv_web ILIKE '%" . $rec . "%' ";
        $grupaFormatiran = "grupa ILIKE '%" . $rec . "%' ";
        $proizvodjacFormatiran = "p.naziv ILIKE '%" . $rec . "%' ";

        $articles = "SELECT r.roba_id as roba_id, r.sifra_is as sifra_is, naziv_web, grupa, racunska_cena_nc FROM roba r LEFT JOIN grupa_pr g ON r.grupa_pr_id = g.grupa_pr_id LEFT JOIN proizvodjac p ON r.proizvodjac_id = p.proizvodjac_id";

        if(Input::get('lager') == 'true'){
            $articles .= " INNER JOIN lager l ON r.roba_id = l.roba_id WHERE orgj_id=3 AND ";
        }else{
            $articles .= " WHERE ";
        }
        $articles .= "(" . $nazivFormatiran . " OR " . $skuFormatiran . " OR " . $grupaFormatiran . " OR " . $proizvodjacFormatiran . ") ORDER BY grupa ASC";


        header('Content-type: text/plain; charset=utf-8');          
        $list = "<ul class='articles_list'>";
        foreach (DB::select($articles) as $article) {
            $list .= "
                <li class='articles_list__item' data-roba_id='".$article->roba_id."' data-ncena='".$article->racunska_cena_nc."'>
                    <div class='articles_list__item__link'>"
                        ."<span class='articles_list__item__link__text'>" . $article->naziv_web . "</span>"
                        ."<span class='articles_list__item__link__small'>" . $article->sifra_is . "</span>"
                        ."<span class='articles_list__item__link__cat'>" . $article->grupa . "</span>
                    </div>
                </li>";
        }

        $list .= "</ul>";
        echo $list; 
    }

    public function radni_nalog_rezervni_deo_save(){
        $data = Input::get();
        if(DB::table('radni_nalog')->where('radni_nalog_id',$data['radni_nalog_id'])->pluck('status_id') == 4){
            $result = array(
                'success' => false,
                'errors' => array(),
                'error_message' => 'Radni nalog je realizovan.'
            );
            return Response::json($result);    
        }

        $radni_nalog_rezervni_deo_id = $data['radni_nalog_rezervni_deo_id'];
        unset($data['radni_nalog_rezervni_deo_id']);

        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'not_in' => 'Niste popunili polje!'
        );

        $validator_arr = array(
            'roba_id' => 'required|integer|not_in:0',
            'kolicina' => 'required|integer|digits_between:0,10',
            'cena' => 'required|numeric|digits_between:0,10',
            'iznos' => 'required|numeric|digits_between:0,10'
        );

        // $roba = DB::table('roba')->where('roba_id',$data['roba_id'])->first();
        // $cena = $roba->racunska_cena_nc * (1 + (DB::table('tarifna_grupa')->where('tarifna_grupa_id',$roba->tarifna_grupa_id)->pluck('porez') / 100));
        // $data['cena'] = $cena;
        // $data['iznos'] = $cena * $data['kolicina'];


        $validator = Validator::make($data, $validator_arr, $messages);
        if ($validator->passes()) {
            if($radni_nalog_rezervni_deo_id > 0){
                DB::table('radni_nalog_rezervni_deo')->where('radni_nalog_rezervni_deo_id',$radni_nalog_rezervni_deo_id)->update($data);
            }else{
                DB::table('radni_nalog_rezervni_deo')->insert($data);
            }
            $result = array(
                'success' => true,
                'errors' => array()
            );

        }else{
            $result = array(
                'success' => false,
                'errors' => $validator->messages()
            );
        }        
        return Response::json($result);
    }

    public function radni_nalog_rezervni_deo_delete($radni_nalog_rezervni_deo_id){
        $radni_nalog_rezervni_deo = DB::table('radni_nalog_rezervni_deo')->where('radni_nalog_rezervni_deo_id',$radni_nalog_rezervni_deo_id)->first();
        if(DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_rezervni_deo->radni_nalog_id)->pluck('status_id') == 4){
            return Redirect::back()->with('error_message','Radni nalog je realizovan.');                    
        }

        DB::table('radni_nalog_rezervni_deo')->where('radni_nalog_rezervni_deo_id',$radni_nalog_rezervni_deo_id)->delete();
        return Redirect::back()->with('message','Uspešno ste obrisali stavku.');
    }

    public function radni_nalog_trosak_dokumenti($radni_nalog_id,$radni_nalog_trosak_id){

        $data=array(
            "strana"=>'radni_nalog_dokumenti',
            "title"=> 'Radni Nalog Dokumenti',
        );

        $radni_nalog = DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_id)->first();
        if(is_null($radni_nalog)){
            return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-prijem/0');
        }
        $radni_nalog_troskovi = DB::table('radni_nalog_trosak')->where('radni_nalog_id',$radni_nalog_id)->orderBy('radni_nalog_trosak_id','asc')->get();
        if(count($radni_nalog_troskovi) == 0){
            return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-prijem/0');
        }
        $data["radni_nalog"] = $radni_nalog;
        $data["radni_nalog_troskovi"] = $radni_nalog_troskovi;

        $radni_nalog_trosak = DB::table('radni_nalog_trosak')->where('radni_nalog_trosak_id',$radni_nalog_trosak_id)->first();
        if(is_null($radni_nalog_trosak)){
            return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-prijem/0');
        }
        $data["partner_naziv"] = RMA::partner($radni_nalog_trosak->partner_id)->naziv;

        $radni_nalog_trosak_uput = DB::table('radni_nalog_trosak')->join('radni_nalog_trosak_dokument','radni_nalog_trosak_dokument.radni_nalog_trosak_id','=','radni_nalog_trosak.radni_nalog_trosak_id')->select('radni_nalog_trosak_dokument_id','radni_nalog_trosak.radni_nalog_trosak_id AS radni_nalog_trosak_id','radni_nalog_trosak_broj_dokumenta','uput','datum_dokumenta','serijski_broj','uredjaj','opis_kvara','napomena')->where(array('radni_nalog_trosak.radni_nalog_trosak_id'=>$radni_nalog_trosak_id, 'uput'=>1))->first();
        if(!is_null($radni_nalog_trosak_uput)){
            $data["radni_nalog_trosak_uput"] = $radni_nalog_trosak_uput;
        }else{
            $data["radni_nalog_trosak_uput"] = (object) array('radni_nalog_trosak_dokument_id'=>0,'radni_nalog_trosak_id'=>$radni_nalog_trosak_id,'radni_nalog_trosak_broj_dokumenta'=>'Novi uput','uput'=>1,'datum_dokumenta'=>date('Y-m-d'),'serijski_broj'=>$radni_nalog->serijski_broj,'uredjaj'=>$radni_nalog->uredjaj,'opis_kvara'=>$radni_nalog->opis_kvara,'napomena'=>$radni_nalog->napomena);
        }

        $radni_nalog_trosak_prijem = DB::table('radni_nalog_trosak')->join('radni_nalog_trosak_dokument','radni_nalog_trosak_dokument.radni_nalog_trosak_id','=','radni_nalog_trosak.radni_nalog_trosak_id')->select('radni_nalog_trosak_dokument_id','radni_nalog_trosak.radni_nalog_trosak_id AS radni_nalog_trosak_id','radni_nalog_trosak_broj_dokumenta','uput','datum_dokumenta','serijski_broj','uredjaj','napomena')->where(array('radni_nalog_trosak.radni_nalog_trosak_id'=>$radni_nalog_trosak_id, 'uput'=>0))->first();
        if(!is_null($radni_nalog_trosak_prijem)){
            $data["radni_nalog_trosak_prijem"] = $radni_nalog_trosak_prijem;
        }else{
            $data["radni_nalog_trosak_prijem"] = (object) array('radni_nalog_trosak_dokument_id'=>0, 'radni_nalog_trosak_broj_dokumenta' => '','radni_nalog_trosak_id'=>$radni_nalog_trosak_id,'radni_nalog_trosak_broj_dokumenta'=>'','uput'=>0,'datum_dokumenta'=>date('Y-m-d'),'serijski_broj'=>$radni_nalog->serijski_broj,'uredjaj'=>$radni_nalog->uredjaj,'napomena'=>'');
        }


        return View::make('rma/pages/radni_nalog_dokumenti', $data);
    }

    public function radni_nalog_trosak_dokumenti_save(){
        $data = Input::get();

        $radni_nalog_trosak_dokument_id = $data['radni_nalog_trosak_dokument_id'];
        unset($data['radni_nalog_trosak_dokument_id']);
        $radni_nalog_trosak = DB::table('radni_nalog_trosak')->where('radni_nalog_trosak_id',$data['radni_nalog_trosak_id'])->first();

        if(DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_trosak->radni_nalog_id)->pluck('status_id') == 4){
            return Redirect::back()->with('error_message','Radni nalog je realizovan.');                    
        }

        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'not_in' => 'Niste popunili polje!',
            'max' => 'Dužina sadržaja nije dozvoljena!',
            'integer' => 'Vrednost sadržaja nije dozvoljena!',
            'in' => 'Vrednost sadržaja nije dozvoljena!'
        );

        $validator_arr = array(
            'datum_dokumenta' => 'required',
            'uredjaj' => 'required|regex:'.RmaSupport::regex().'|max:500',
            'serijski_broj' => 'required|regex:'.RmaSupport::regex().'|max:255',
            'opis_kvara' => 'required|regex:'.RmaSupport::regex().'|max:1000',
            'napomena' => 'regex:'.RmaSupport::regex().'|max:500',
            'uput' => 'required|integer|in:0,1'
        );

        $validator = Validator::make($data, $validator_arr, $messages);
        if ($validator->passes()) {

            if($radni_nalog_trosak_dokument_id==0){
                $data['radni_nalog_trosak_broj_dokumenta'] = 'UPUT'.str_pad($radni_nalog_trosak->radni_nalog_trosak_id, 3, '0', STR_PAD_LEFT);
                DB::table('radni_nalog_trosak_dokument')->insert($data);
                DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_trosak->radni_nalog_id)->update(array('status_id'=>2));
            }else{
                DB::table('radni_nalog_trosak_dokument')->where('radni_nalog_trosak_dokument_id',$radni_nalog_trosak_dokument_id)->update($data);
            }
            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-trosak-dokumenti/'.$radni_nalog_trosak->radni_nalog_id.'/'.$radni_nalog_trosak->radni_nalog_trosak_id)->with('message',$message);
        }else{
            return Redirect::back()->withInput()->withErrors($validator);
        }
    }

    public function radni_nalog_trosak_dokumenti_prijem_save(){
        $data = Input::get();

        $radni_nalog_trosak_dokument_id = $data['prijem_radni_nalog_trosak_dokument_id'];
        unset($data['prijem_radni_nalog_trosak_dokument_id']);
        $radni_nalog_trosak = DB::table('radni_nalog_trosak')->where('radni_nalog_trosak_id',$data['prijem_radni_nalog_trosak_id'])->first();

        if(DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_trosak->radni_nalog_id)->pluck('status_id') == 4){
            return Redirect::back()->with('error_message','Radni nalog je realizovan.');                    
        }

        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'not_in' => 'Niste popunili polje!',
            'max' => 'Dužina sadržaja nije dozvoljena!',
            'integer' => 'Vrednost sadržaja nije dozvoljena!',
            'in' => 'Vrednost sadržaja nije dozvoljena!'
        );

        $validator_arr = array(
            'prijem_radni_nalog_trosak_broj_dokumenta' => 'required|regex:'.RmaSupport::regex().'|max:20',
            'prijem_datum_dokumenta' => 'required',
            'prijem_uredjaj' => 'required|regex:'.RmaSupport::regex().'|max:500',
            'prijem_serijski_broj' => 'required|regex:'.RmaSupport::regex().'|max:255',
            'prijem_napomena' => 'required|regex:'.RmaSupport::regex().'|max:500',
            'prijem_uput' => 'required|integer|in:0,1'
        );

        $validator = Validator::make($data, $validator_arr, $messages);
        if ($validator->passes()) {
            $db_data = array();
            foreach($data as $key => $value){
                $db_data[str_replace('prijem_','',$key)] = $value;
            }
            if($radni_nalog_trosak_dokument_id==0){
                DB::table('radni_nalog_trosak_dokument')->insert($db_data);
                DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_trosak->radni_nalog_id)->update(array('status_id'=>3));
            }else{
                DB::table('radni_nalog_trosak_dokument')->where('radni_nalog_trosak_dokument_id',$radni_nalog_trosak_dokument_id)->update($db_data);
            }

            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-trosak-dokumenti/'.$radni_nalog_trosak->radni_nalog_id.'/'.$radni_nalog_trosak->radni_nalog_trosak_id)->with('message',$message);
        }else{
            return Redirect::back()->withInput()->withErrors($validator);
        }
    }

    public function radni_nalog_trosak_dokument_delete($radni_nalog_trosak_dokument_id){
        $radni_nalog_trosak_dokument = DB::table('radni_nalog_trosak_dokument')->where('radni_nalog_trosak_dokument_id',$radni_nalog_trosak_dokument_id)->first();
        $radni_nalog_trosak = DB::table('radni_nalog_trosak')->where('radni_nalog_trosak_id',$radni_nalog_trosak_dokument->radni_nalog_trosak_id)->first();

        if(DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_trosak->radni_nalog_id)->pluck('status_id') == 4){
            return Redirect::back()->with('error_message','Radni nalog je realizovan.');                    
        }

        DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_trosak->radni_nalog_id)->update(array('status_id'=>($radni_nalog_trosak_dokument->uput == 1 ? 1 : 2)));

        $radni_nalog_trosak_dokument = DB::table('radni_nalog_trosak_dokument')->where('radni_nalog_trosak_dokument_id',$radni_nalog_trosak_dokument_id)->first();
        if($radni_nalog_trosak_dokument->uput == 1){
            DB::table('radni_nalog_trosak_dokument')->where('radni_nalog_trosak_id',$radni_nalog_trosak_dokument->radni_nalog_trosak_id)->delete();
        }else{
            DB::table('radni_nalog_trosak_dokument')->where('radni_nalog_trosak_dokument_id',$radni_nalog_trosak_dokument_id)->delete();
        }
        return Redirect::back()->with('message','Uspešno ste obrisali dokument.');
    }

    public function radni_nalog_predaja($id){

        $data=array(
            "strana"=>'radni_nalog_predaja',
            "title"=> 'Radni Nalog Predaja',
        );

        if($id <= 0){
            return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-prijem/0');
        }
        $radni_nalog = DB::table('radni_nalog')->where('radni_nalog_id',$id)->first();
        $partner = DB::table('partner')->where('partner_id',$radni_nalog->partner_id)->first();
        $data["radni_nalog"] = $radni_nalog;
        $data["radni_nalog_troskovi"] = DB::table('radni_nalog_trosak')->where('radni_nalog_id',$id)->orderBy('radni_nalog_trosak_id','asc')->get();

        $data["cene"] = RMA::radni_nalog_cene($id);

        return View::make('rma/pages/radni_nalog_predaja', $data);
    }

    public function radni_nalog_predaja_post(){
        $data = Input::get();

        $radni_nalog_id = $data['radni_nalog_id'];
        $radni_nalog = DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_id)->first();

        if(isset($data['check_predato'])){
            $data['status_id'] = 4;
        }else if($radni_nalog->status_id == 4){
            $radni_nalog_troskovi = DB::table('radni_nalog_trosak')->select('radni_nalog_trosak_id')->where('radni_nalog_id',$radni_nalog->radni_nalog_id)->get();
            $radni_nalog_troskovi_uput_check = DB::table('radni_nalog_trosak_dokument')->whereIn('radni_nalog_trosak_id',array_map('current',$radni_nalog_troskovi))->where('uput',1)->count() > 0;
            $radni_nalog_troskovi_vraceno_check = DB::table('radni_nalog_trosak_dokument')->whereIn('radni_nalog_trosak_id',array_map('current',$radni_nalog_troskovi))->where('uput',0)->count() > 0;
            $data['status_id'] = $radni_nalog_troskovi_vraceno_check ? 3 : ($radni_nalog_troskovi_uput_check ? 2 : 1);
        }
        unset($data['check_predato']);
        unset($data['radni_nalog_id']);
        // $radni_nalog_trosak = DB::table('radni_nalog_trosak')->where('radni_nalog_trosak_id',$data['prijem_radni_nalog_trosak_id'])->first();

        $messages = array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'not_in' => 'Niste popunili polje!',
            'max' => 'Dužina sadržaja nije dozvoljena!',
            'integer' => 'Vrednost sadržaja nije dozvoljena!',
            'in' => 'Vrednost sadržaja nije dozvoljena!'
        );

        $validator_arr = array(
            'uredjaj' => 'required|regex:'.RmaSupport::regex().'|max:500',
            'predat_uredjaj' => 'required|regex:'.RmaSupport::regex().'|max:500',
            'preuzeo_ime' => 'required|regex:'.RmaSupport::regex().'|max:50',
            'preuzeo_brojlk' => 'required|numeric|regex:'.RmaSupport::regex().'|digits_between:0,20',
            'predao_serviser_id' => 'required|integer|not_in:0',
            'datum_zavrsetka' => 'required|date'
        );

        $validator = Validator::make($data, $validator_arr, $messages);
        if ($validator->passes()) {
            DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_id)->update($data);

            return Redirect::back()->with('message','Uspešno ste sačuvali izmene.');
        }else{
            return Redirect::back()->withInput()->withErrors($validator);
        } 

    }

    public function radni_nalog_rekapitulacija($id,$trosak=0){
        if($id <= 0){
            return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-prijem/0');
        }

        $radni_nalog = DB::table('radni_nalog')->where('radni_nalog_id',$id)->first();
        $data = array();
        $data["radni_nalog"] = $radni_nalog;
        $data["kupac"] = DB::table('web_kupac')->where('web_kupac_id',$radni_nalog->kupac_id)->first();
        $data["radni_nalog_operacije"] = DB::table('radni_nalog_operacije')->leftJoin('operacija_servis','radni_nalog_operacije.operacija_servis_id','=','operacija_servis.operacija_servis_id')->leftJoin('tarifna_grupa','tarifna_grupa.tarifna_grupa_id','=','operacija_servis.tarifna_grupa_id')->where('radni_nalog_id',$id)->orderBy('radni_nalog_operacije_id','asc')->get();

        $data["radni_nalog_rezervni_delovi"] = DB::table('radni_nalog_rezervni_deo')->where('radni_nalog_id',$id)->orderBy('radni_nalog_rezervni_deo_id','asc')->get();

        $data["radni_nalog_troskovi"] = $trosak==1 ? DB::table('radni_nalog_trosak')->where('radni_nalog_id',$id)->orderBy('radni_nalog_trosak_id','asc')->get() : array();

        $data["cene"] = RMA::radni_nalog_cene($id,($trosak==1));

        $pdf = App::make('dompdf');
        $pdf->loadView('rma/pages/radni_nalog_rekapitulacija', $data);

        return $pdf->stream();
    }

    public function radni_nalog_usluzni_servis_uput($radni_nalog_trosak_id){
        if($radni_nalog_trosak_id <= 0){
            return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-prijem/0');
        }

        $radni_nalog_trosak = DB::table('radni_nalog_trosak')->where('radni_nalog_trosak_id',$radni_nalog_trosak_id)->first();
        if(is_null($radni_nalog_trosak)){
            return Redirect::to(RmaOptions::base_url().'rma/radni-nalog-prijem/0');
        }

        $radni_nalog = DB::table('radni_nalog')->where('radni_nalog_id',$radni_nalog_trosak->radni_nalog_id)->first();

        $data["usluzni_servis"] = RMA::partner($radni_nalog_trosak->partner_id);

        $radni_nalog_trosak_uput = DB::table('radni_nalog_trosak')->join('radni_nalog_trosak_dokument','radni_nalog_trosak_dokument.radni_nalog_trosak_id','=','radni_nalog_trosak.radni_nalog_trosak_id')->select('radni_nalog_trosak_dokument_id','radni_nalog_trosak.radni_nalog_trosak_id AS radni_nalog_trosak_id','radni_nalog_trosak_broj_dokumenta','uput','datum_dokumenta','serijski_broj','uredjaj','opis_kvara','napomena')->where(array('radni_nalog_trosak.radni_nalog_trosak_id'=>$radni_nalog_trosak_id, 'uput'=>1))->first();
        if(!is_null($radni_nalog_trosak_uput)){
            $data["radni_nalog_trosak_uput"] = $radni_nalog_trosak_uput;
        }else{
            $data["radni_nalog_trosak_uput"] = (object) array('radni_nalog_trosak_dokument_id'=>0,'radni_nalog_trosak_id'=>$radni_nalog_trosak_id,'radni_nalog_trosak_broj_dokumenta'=>'Novi uput','uput'=>1,'datum_dokumenta'=>date('Y-m-d'),'serijski_broj'=>$radni_nalog->serijski_broj,'uredjaj'=>$radni_nalog->uredjaj,'opis_kvara'=>$radni_nalog->opis_kvara,'napomena'=>$radni_nalog->napomena);
        }

        $radni_nalog_trosak_prijem = DB::table('radni_nalog_trosak')->join('radni_nalog_trosak_dokument','radni_nalog_trosak_dokument.radni_nalog_trosak_id','=','radni_nalog_trosak.radni_nalog_trosak_id')->select('radni_nalog_trosak_dokument_id','radni_nalog_trosak.radni_nalog_trosak_id AS radni_nalog_trosak_id','radni_nalog_trosak_broj_dokumenta','uput','datum_dokumenta','serijski_broj','uredjaj','napomena')->where(array('radni_nalog_trosak.radni_nalog_trosak_id'=>$radni_nalog_trosak_id, 'uput'=>0))->first();
        if(!is_null($radni_nalog_trosak_prijem)){
            $data["radni_nalog_trosak_prijem"] = $radni_nalog_trosak_prijem;
        }else{
            $data["radni_nalog_trosak_prijem"] = (object) array('radni_nalog_trosak_dokument_id'=>0, 'radni_nalog_trosak_broj_dokumenta' => '','radni_nalog_trosak_id'=>$radni_nalog_trosak_id,'radni_nalog_trosak_broj_dokumenta'=>'','uput'=>0,'datum_dokumenta'=>null,'serijski_broj'=>'','uredjaj'=>'','napomena'=>'');
        }

// return View::make('rma/pages/radni_nalog_usluzni_servis_uput', $data);
        $pdf = App::make('dompdf');
        $pdf->loadView('rma/pages/radni_nalog_usluzni_servis_uput', $data);

        return $pdf->stream();
    }

}