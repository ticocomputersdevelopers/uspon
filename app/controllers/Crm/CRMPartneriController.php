<?php
use Service\TranslatorService;

class CRMPartneriController extends Controller {

	public function crm_partneri(){

        // $partneri=DB::table('partner')->join('partner_je', function($join)
  //                {$join->on('partner.partner_id', '=', 'partner_je.partner_id');
  //                })->distinct()->get();
  //       $partneriCount=AdminKupci::getPartneriSearch(null,258,null,null,20,null,null);
		$limit=20;
         if(Input::get('page')){
            $page = Input::get('page');
        }else{
            $page = 1;
        }
        $offset = ($page-1)*$limit;
        $search=Input::get('search');

        $query = "SELECT * FROM partner p LEFT JOIN partner_je pj ON p.partner_id = pj.partner_id WHERE pj.partner_vrsta_id = 258 ";
        $where = false;

        
        
        if(!is_null($search) && $search != ''){
			$query .= " AND";
			$where = true;

			$search = urldecode($search);
			$query .= " (";
			foreach(explode(' ',$search) as $word){
				$query .= " p.naziv ILIKE '%".$word."%' OR p.telefon ILIKE '%".$word."%' OR p.adresa ILIKE '%".$word."%' OR p.kontakt_osoba ILIKE '%".$word."%' OR CAST(p.broj_maticni AS TEXT) ILIKE '%".$word."%' OR CAST(p.broj_maticni AS TEXT) ILIKE '%".$word."%' OR";
			}
			$query = substr($query,0,-3);
			$query .= ")";
			
		}
		
        // $pagination = " ORDER BY p.partner_id DESC LIMIT ".$limit." OFFSET ".$offset."";
        // $partneri = DB::select($query.(strlen($where)>0 ? " WHERE ":"").$where.$pagination);
        $partneri = DB::select($query);
		$count = count($partneri);
		$query .= " ORDER BY p.partner_id DESC LIMIT ".$limit." OFFSET ".$offset."";
        //All::dd($partneri);
        
        $svipartneri= DB::table('partner')->orderBy('partner_id','asc')->get();
        $data=array(
                "strana"=>'crm_partneri',
                "title"=> 'CRM partneri',
                "partneri"=>$partneri,
                "svipartneri"=>$svipartneri,               
                "count"=>$count,
                "limit"=>$limit
            );
            return View::make('crm/page', $data);
        
    }
    public function crm_partneri_edit($partner_id=null){
        $inputs = Input::get();
        $query_result=false;
        $validator_messages = array(
            'required'=>'Polje ne sme biti prazno!',
            'max'=>'Prekoračili ste maksimalnu vrednost polja!'
                               
                                );

        $validator_rules = array(
                'pib' =>  'required',
                'broj_maticni'=> 'required' 
            );

        
        $validator = Validator::make($inputs, $validator_rules, $validator_messages);

        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'crm/crm_partneri')->withInput()->withErrors($validator->messages());
        }
        else
        {  
        	if($partner_id==0 || $partner_id==null){

				$inputs['partner_id'] = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
				$inputs['datum_kreiranja'] = date('Y-m-d H:i:s');
				DB::table('partner')->insert($inputs);
			 
			 	DB::table('partner_je')->insert(['partner_id' => $inputs['partner_id'],'partner_vrsta_id' => 258]);
			 	$crm_tip=DB::table('crm_tip')->first();
             	$crm_tip=$crm_tip->crm_tip_id;
             	$crm_status=DB::table('crm_status')->first();  
             	$crm_status=$crm_status->crm_status_id;           
             	DB::table('crm')->insert(['partner_id' =>$inputs['partner_id'],'flag_zavrseno'=>0,'crm_status_id'=>$crm_status,'crm_tip_id'=>$crm_tip,'datum_kreiranja' => date('Y-m-d')]);
             return Redirect::to(AdminOptions::base_url().'crm/crm_partneri/')->with('message','Uspešno ste dodali partnera .');

        	}
        	else{
        		 DB::table('partner')->where('partner_id',$inputs['partner_id'])->update(array('naziv'=>$inputs['naziv'],'adresa'=>$inputs['adresa'],'mesto'=>$inputs['mesto'],'delatnost_naziv'=>$inputs['delatnost_naziv'],'mail'=>$inputs['mail'],'kontakt_osoba'=>$inputs['kontakt_osoba'],'telefon'=>$inputs['telefon'],'pib'=>$inputs['pib'],'broj_maticni'=>$inputs['broj_maticni']));
        	}
    
       
         return Redirect::to(AdminOptions::base_url().'crm/crm_partneri/')->with('message','Uspešno ste izmenili partnera .');
        }
        
        
    }

     public function dodaj_u_crm($partner_id){

         DB::table('partner_je')->where('partner_id',$partner_id)->update(array('partner_vrsta_id'=>258));
         DB::table('crm')->insert(['partner_id' => $partner_id,'flag_zavrseno'=>0,'crm_status_id'=>1,'crm_tip_id'=>1,'datum_kreiranja' => date('Y-m-d')]);
          return Redirect::to(AdminOptions::base_url().'crm/crm_partneri/')->with('message','Uspešno ste izmenili vrstu partnera .');
    }
    public function ukloni_partnera($partner_id){
    	 DB::table('partner_je')->where('partner_id',$partner_id)->delete();
    	 return Redirect::to(AdminOptions::base_url().'crm/crm_partneri')->with('message','Uspešno ste obrisali partnera .');
    }
   
   

   




}