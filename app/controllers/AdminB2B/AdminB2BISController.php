<?php
use ISCustom\AdminB2BISCustom;

class AdminB2BISController extends Controller {
    function is(){
        $data=array(
            "strana"=>'informacioni_sistem',
            "title"=>'Informacioni sistem',
        );
        return View::make('adminb2b/pages/informacioni-sistem', $data);     
    }

    function load(){
        $kind = Input::get('kind');

        if($kind == 'xlsx'){
            $messages = AdminB2BIS::xlsx_exe();
        }
        elseif($kind == 'xml'){
            $messages = AdminB2BIS::xml_exe();
        }
        elseif($kind == 'csv'){
            $messages = AdminB2BIS::csv_exe();
        }
        elseif($kind == 'sql'){
            $messages = AdminB2BIS::sql_exe();
        }

        if(count($messages)>0){
            return Redirect::to(AdminOptions::base_url().'admin/b2b/is')->with('error_messages',$messages);
        }else{
            return Redirect::to(AdminOptions::base_url().'admin/b2b/is')->with('message','Podaci iz informacionog sistema su uspešno učitani!');
        }    
    }

    function custom_load(){

        $messages = AdminB2BISCustom::xlsx_exe();

        if(count($messages)>0){
            return Redirect::to(AdminOptions::base_url().'admin/b2b/is')->with('error_messages',$messages);
        }else{
            return Redirect::to(AdminOptions::base_url().'admin/b2b/is')->with('message','Podaci iz informacionog sistema su uspešno učitani!');
        }    
    }

    function file_upload(){
        $file = Input::file('import_file');
        $table = Input::get('table');
        $kind = Input::get('kind');

        if(Input::hasFile('import_file') && $file->isValid() && $file->getClientOriginalExtension() == $kind){
            if($kind == 'xml'){
                $suffix = "xml";
            }
            elseif($kind == 'csv'){
                $suffix = "csv";
            }
            elseif($kind == 'dat'){
                $suffix = "dat";
            }else{
                $suffix = "excel";
            }            
            $file->move("files/IS/".$suffix,$table.".".$kind);

            return Redirect::to(AdminOptions::base_url().'admin/b2b/is')->with('message','Fajl je uspešno upload-ovan!');
        }else{
            return Redirect::to(AdminOptions::base_url().'admin/b2b/is')->with('error_message','Fajl nije upload-ovan! Proverite strukturu fajla!');
        }
        
    }

    function wings_post(){
        $result = AdminISWings::execute();
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    
    function calculus_post(){
        $result = AdminISCalculus::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function infograf_post(){
        $result = AdminIsInfograf::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function logik_post(){
        $result = AdminIsLogik::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function minimax_post(){
        $result = AdminIsMinimax::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function roaming_post(){
        $result = AdminIsRoaming::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function sbcs_post(){
        $result = AdminIsSbcs::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function skala_post(){
        $result = AdminIsSkala::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function gsm_post(){
        $result = AdminIsGSM::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function promobi_post(){
        $result = AdminIsPromobi::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function nssport_post(){
        $result = AdminIsNSSport::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function xml_post(){
        $result = AdminIsXml::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function softcom_post(){
        $result = AdminIsSoftCom::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }
    function panteon_post(){
        $result = AdminIsPanteon::execute();
        
        if($result->success){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('message','Podaci su uspešno preuzeti!');
        }else{
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/is')->with('error_message',$result->message);
        }
    }

    function auto_import_is($key){
        if(AdminB2BOptions::info_sys('excel')){

        }else if(AdminB2BOptions::info_sys('wings')){
            $result = AdminISWings::execute();
        }else if(AdminB2BOptions::info_sys('calculus')){
            $result = AdminISCalculus::execute();
        }else if(AdminB2BOptions::info_sys('infograf')){
            $result = AdminIsInfograf::execute();
        }else if(AdminB2BOptions::info_sys('logik')){
            $result = AdminIsLogik::execute();
        }else if(AdminB2BOptions::info_sys('minimax')){
            $result = AdminIsMinimax::execute();
        }else if(AdminB2BOptions::info_sys('roaming')){
            $ftp_host = '94.130.104.140';
            $ftp_user_name = 'roaming-update';
            $ftp_user_pass = 'qG7wX!Hbt!ma7#';
            $connection = ftp_connect($ftp_host);
            $login_result = ftp_login($connection,$ftp_user_name,$ftp_user_pass);
            ftp_pasv($connection, true);
            
            if(ftp_size($connection, "partneri.xml") != -1){
                ftp_get($connection,'files/IS/xml/partner.xml','partneri.xml', FTP_BINARY);
                ftp_delete($connection,'partneri.xml');
            }
            if(ftp_size($connection, "artikli.xml") != -1){
                ftp_get($connection,'files/IS/xml/roba.xml','artikli.xml', FTP_BINARY);
                ftp_delete($connection,'artikli.xml');
            }
            if(ftp_size($connection, "cenovnici.xml") != -1){
                ftp_get($connection,'files/IS/xml/roba_cene.xml','cenovnici.xml', FTP_BINARY);
                ftp_delete($connection,'cenovnici.xml');
            }
            if(ftp_size($connection, "kartice.xml") != -1){
                ftp_get($connection,'files/IS/xml/partner_kartica.xml','kartice.xml', FTP_BINARY);
                ftp_delete($connection,'kartice.xml');
            }
            if(ftp_size($connection, "kartice_stavke.xml") != -1){
                ftp_get($connection,'files/IS/xml/partner_kartica_item.xml','kartice_stavke.xml', FTP_BINARY);
                ftp_delete($connection,'kartice_stavke.xml');
            }
            ftp_close($connection);
            
            $result = AdminIsRoaming::execute();
        }else if(AdminB2BOptions::info_sys('sbcs')){
            $result = AdminIsSbcs::execute();
        }else if(AdminB2BOptions::info_sys('skala')){
            $result = AdminIsSkala::execute();
        }else if(AdminB2BOptions::info_sys('gsm')){
            $result = AdminIsGSM::execute();
        }else if(AdminB2BOptions::info_sys('promobi')){
            $ftp_host = '94.130.104.140';
            $ftp_user_name = 'manel';
            $ftp_user_pass = '6SyomQ_SWoTb';
            $connection = ftp_connect($ftp_host);
            $login_result = ftp_login($connection,$ftp_user_name,$ftp_user_pass);
            ftp_pasv($connection, true);
            
            if(ftp_size($connection, "partner.xml") != -1){
                ftp_get($connection,'files/IS/xml/partner.xml','partner.xml', FTP_BINARY);
                ftp_delete($connection,'partner.xml');
            }
            if(ftp_size($connection, "roba.xml") != -1){
                ftp_get($connection,'files/IS/xml/roba.xml','roba.xml', FTP_BINARY);
                ftp_delete($connection,'roba.xml');
            }

            $statusi = ftp_nlist($connection, "narudzbina_statusi/");
            foreach($statusi as $status){
                if(!in_array($status,array('.','..')) && ftp_size($connection, 'narudzbina_statusi/'.$status) != -1){
                    ftp_get($connection,'files/IS/xml/narudzbina_statusi/'.$status,'narudzbina_statusi/'.$status, FTP_BINARY);
                    ftp_delete($connection,'narudzbina_statusi/'.$status);
                }
            }

            ftp_close($connection);

            $result = AdminIsPromobi::execute();
        }else if(AdminB2BOptions::info_sys('nssport')){
            $result = AdminIsNSSport::execute();
        }else if(AdminB2BOptions::info_sys('xml')){
            $result = AdminIsXml::execute();
        }else if(AdminB2BOptions::info_sys('softcom')){
            $ftp_host = '94.130.104.140';
            $ftp_user_name = 'stasanet';
            $ftp_user_pass = 'kz2!PPUFSacn';
            $connection = ftp_connect($ftp_host);
            $login_result = ftp_login($connection,$ftp_user_name,$ftp_user_pass);
            ftp_pasv($connection, true);
            
            // if(ftp_size($connection, "partner.xml") != -1){
            //     ftp_get($connection,'files/IS/xml/partner.xml','partner.xml', FTP_BINARY);
            //     ftp_delete($connection,'partner.xml');
            // }
            if(ftp_size($connection, "StasaB2B") != -1){
                ftp_get($connection,'files/IS/dat/roba.dat','StasaB2B', FTP_BINARY);
                // ftp_delete($connection,'StasaB2B');
            }

            ftp_close($connection);

            $result = AdminIsSoftCom::execute();
        }else if(AdminB2BOptions::info_sys('panteon')){
            $result = AdminIsPanteon::execute();
        }else if(AdminB2BOptions::info_sys('timcode')){
            $result = AdminIsTimCode::execute();
        }else if(AdminB2BOptions::info_sys('navigator')){
            $result = AdminIsNavigator::execute();
        }else if(AdminB2BOptions::info_sys('spsoft')){
            $result = AdminIsSpSoft::execute();
        }

        if(!$result->success){
            echo $result->message;
        }       
    }

    function auto_import_is_partner_full($key){
        $result = (object) array('success'=>false, 'message' => '');

        if(AdminB2BOptions::info_sys('minimax')){
            $result = AdminIsMinimax::executePartnerFull();
        }

        if(!$result->success){
            echo $result->message;
        }       
    }
    function auto_import_is_partner_cart($key){
        $result = (object) array('success'=>false, 'message' => '');

        if(AdminB2BOptions::info_sys('logik')){
            $result = AdminIsLogik::updateCart();
        }

        if(!$result->success){
            echo $result->message;
        }       
    }
    function auto_import_is_price($key){
        $result = (object) array('success'=>false, 'message' => '');

        if(AdminB2BOptions::info_sys('logik')){
            $result = AdminIsLogik::execute_short_price();
        }

        if(!$result->success){
            echo $result->message;
        }       
    }
}