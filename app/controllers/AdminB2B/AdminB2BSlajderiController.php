<?php
 
class AdminB2BSlajderiController extends Controller {

    public function slider($b2b_slider_id,$b2b_slider_item_id=null,$lang_id=null){
        if(is_null($b2b_slider_item_id)){
            if(!is_null($b2b_slider_id) && $b2b_slider_id > 0){
                $b2b_slider_item_id = DB::table('b2b_slajder_stavka')->where('b2b_slajder_id',$b2b_slider_id)->orderBy('rbr','asc')->pluck('b2b_slajder_stavka_id');
            }else{
                $b2b_slider_item_id = 0;
            }
        }
        if(is_null($lang_id)){
            $lang_id = AdminLanguage::defaultLangObj()->jezik_id;
        }

        $selectedSlider = $b2b_slider_id > 0 ? DB::table('b2b_slajder')->where('b2b_slajder_id',$b2b_slider_id)->first() : (object) ['b2b_slajder_id' => 0, 'b2b_slajder_tip_id' =>  DB::table('b2b_slajder_tip')->orderBy('b2b_slajder_tip_id','asc')->pluck('b2b_slajder_tip_id'), 'naziv'=>'', 'flag_aktivan' => 1];
        $selectedSliderItem = $b2b_slider_item_id > 0 && !is_null($b2b_slider_item=AdminB2BSupport::sliderItem($b2b_slider_item_id)) ? $b2b_slider_item : (object) ['b2b_slajder_id' => $selectedSlider->b2b_slajder_id, 'b2b_slajder_stavka_id' => 0, 'flag_aktivan' => 1, 'image_path' => '', 'datum_od' => null, 'datum_do' => null];
        
        $data = array(
            'title' => 'B2B_Slajder',
            'strana' => 'b2b_slajder',
            'lang_id' => $lang_id,
            "jezici" => DB::table('jezik')->where('aktivan',1)->get(),
            'b2b_sliders' => DB::table('b2b_slajder')->orderBy('b2b_slajder_id','asc')->get(),
            'b2b_sliderItems' => $b2b_slider_id > 0 ? AdminB2BSupport::sliderItems($b2b_slider_id) : [],
            'selectedSlider' => $selectedSlider,
            'selectedSliderItem' => $selectedSliderItem,
            'sliderItemLang' => $b2b_slider_item_id > 0 && !is_null($b2b_slider_item_lang=AdminB2BSupport::sliderItemLang($b2b_slider_item_id,$lang_id)) ? $b2b_slider_item_lang : (object) ['b2b_slajder_stavka_id' => $selectedSliderItem->b2b_slajder_stavka_id, 'jezik_id' => $lang_id, 'naslov' => '', 'sadrzaj' => '', 'naslov_dugme' => '', 'link' => '', 'alt' => '']
        );

        return View::make('adminb2b/pages/b2b_slajder',$data);
    }

    public function slider_edit(){
        $data = Input::get();
        $b2b_slajder_id = $data['b2b_slajder_id'];
        $jezik_id = $data['jezik_id'];
        unset($data['b2b_slajder_id']);
        unset($data['jezik_id']);

        $validator = Validator::make($data,
        array(
            'b2b_slajder_tip_id' => 'required|integer',
            'naziv' => 'required|regex:'.AdminSupport::regex().'|max:255',
            'flag_aktivan' => 'required|in:0,1'
        ),
        array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'max' => 'Uneli ste neodgovarajući broj karaktera!',
            'integer' => 'Vrednost mora da bude broj',
            'in' => 'Unesite odgovarajuči karakter!'
        ));

        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
            if($b2b_slajder_id == 0){
                DB::table('b2b_slajder')->insert([
                    'b2b_slajder_tip_id' => $data['b2b_slajder_tip_id'],
                    'naziv' => $data['naziv'],
                    'flag_aktivan' => $data['flag_aktivan']
                ]);

                $b2b_slajder_id = DB::table('b2b_slajder')->max('b2b_slajder_id');

                $tipNaziv = DB::table('b2b_slajder_tip')->where('b2b_slajder_tip_id',$data['b2b_slajder_tip_id'])->pluck('naziv');
                if($tipNaziv == 'Slajder'){
                    AdminSupport::saveLog('SLAJDERI_DODAJ', array($b2b_slajder_id));  
                }else{
                    AdminSupport::saveLog('BANERI_DODAJ', array($b2b_slajder_id));
                }
            }else{
                DB::table('b2b_slajder')->where('b2b_slajder_id',$b2b_slajder_id)->update([
                    'b2b_slajder_tip_id' => $data['b2b_slajder_tip_id'],
                    'naziv' => $data['naziv'],
                    'flag_aktivan' => $data['flag_aktivan']
                ]);

                $tipNaziv = DB::table('b2b_slajder_tip')->where('b2b_slajder_tip_id',$data['b2b_slajder_tip_id'])->pluck('naziv');
                if($tipNaziv == 'Slajder'){
                    AdminSupport::saveLog('SLAJDERI_IZMENI', array($b2b_slajder_id));  
                }else{
                    AdminSupport::saveLog('BANERI_IZMENI', array($b2b_slajder_id));
                }
            }

            return Redirect::to('/admin/b2b/slider/'.$b2b_slajder_id)->with('message','Uspešno ste sačuvali podatke.');
        }    
    }

    public function slider_item_edit(){
        $data = Input::get();
        $b2b_slajder_stavka_id = $data['b2b_slajder_stavka_id'];
        $b2b_slajder_id = $data['b2b_slajder_id'];
        $jezik_id = $data['jezik_id'];
        unset($data['b2b_slajder_stavka_id']);
        unset($data['b2b_slajder_id']);
        unset($data['jezik_id']);
        $b2b_slajder = DB::table('b2b_slajder')->where('b2b_slajder_id',$b2b_slajder_id)->first();
        $b2b_sliderTip = DB::table('b2b_slajder_tip')->where('b2b_slajder_tip_id',$b2b_slajder->b2b_slajder_tip_id)->first();

        $validator = Validator::make($data,
        array(
            'flag_aktivan' => 'required|in:0,1',
            'link' => 'max:500',
            'alt' => 'max:255',
            'naslov' => 'max:100',
            'naslov_dugme' => 'max:100'
        ),
        array(
            'required' => 'Niste popunili polje!',
            'max' => 'Uneli ste neodgovarajući broj karaktera!',
            'in' => 'Unesite odgovarajuči karakter!'
        ));

        if($validator->fails()){
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }else{
            $max_image_size = 10000;
            $image = Input::file('image_path');

            $validator = Validator::make(
                array('image_path' => $image),
                array('image_path' => ($b2b_slajder_stavka_id == 0 ? 'required|' : '').'mimes:jpg,png,jpeg,gif,webp|max:'.strval($max_image_size)),
                array(
                    'required' => 'Niste izabrali fajl.',
                    'mimes' => 'Neodgovarajući format slike. Dozvoljeni formati su jpg, png, webp i jpeg.',
                    'max' => 'Maksimalna veličina slike je '.strval($max_image_size).'.',
                    )
                );
            if($validator->fails()){
                return Redirect::back()->withInput()->withErrors($validator->messages());
            }
            if($b2b_sliderTip->naziv == 'Slajder'){
                $path = 'images/slider/';
            }else{
                $path = 'images/banners/';
            }

            if($b2b_slajder_stavka_id == 0){
                $rbr = 1;
                if(count(DB::table('b2b_slajder_stavka')->where('b2b_slajder_id',$b2b_slajder_id)->get()) > 0){
                    $rbr = DB::table('b2b_slajder_stavka')->where('b2b_slajder_id',$b2b_slajder_id)->max('rbr') + 1;
                }
                DB::table('b2b_slajder_stavka')->insert([
                    'b2b_slajder_id' => $b2b_slajder_id,
                    'flag_aktivan' => $data['flag_aktivan'],
                    'rbr' => $rbr,
                    'datum_od' => !empty($data['datum_od']) ? $data['datum_od'] : null,
                    'datum_do' => !empty($data['datum_do']) ? $data['datum_do'] : null
                ]);

                $b2b_slajder_stavka_id = DB::table('b2b_slajder_stavka')->max('b2b_slajder_stavka_id');
            }else{
                DB::table('b2b_slajder_stavka')->where('b2b_slajder_stavka_id',$b2b_slajder_stavka_id)->update([
                    'flag_aktivan' => $data['flag_aktivan'],
                    'datum_od' => !empty($data['datum_od']) ? $data['datum_od'] : null,
                    'datum_do' => !empty($data['datum_do']) ? $data['datum_do'] : null
                ]);
            }
            if($image){
                $name = $b2b_slajder_stavka_id.'.'.$image->getClientOriginalExtension();
                $image->move($path, $name);
                DB::table('b2b_slajder_stavka')->where('b2b_slajder_stavka_id',$b2b_slajder_stavka_id)->update(array('image_path'=>$path.$name)); 
            }

            if(!empty($data['sadrzaj']) || !empty($data['link']) || !empty($data['alt']) || !empty($data['naslov']) || !empty($data['naslov_dugme'])){ 
                $langData = [
                        'sadrzaj' => !empty($data['sadrzaj']) ? $data['sadrzaj'] : null,
                        'link' => !empty($data['link']) ? $data['link'] : null,
                        'alt' => !empty($data['alt']) ? $data['alt'] : null,
                        'naslov' => !empty($data['naslov']) ? $data['naslov'] : null,
                        'naslov_dugme' => !empty($data['naslov_dugme']) ? $data['naslov_dugme'] : null
                    ];

                if(!is_null(DB::table('b2b_slajder_stavka_jezik')->where(['b2b_slajder_stavka_id'=>$b2b_slajder_stavka_id,'jezik_id'=>$jezik_id])->first())){

                    DB::table('b2b_slajder_stavka_jezik')->where(['b2b_slajder_stavka_id'=>$b2b_slajder_stavka_id,'jezik_id'=>$jezik_id])->update($langData);
                }else{
                    $langData['b2b_slajder_stavka_id'] = $b2b_slajder_stavka_id;
                    $langData['jezik_id'] = $jezik_id;

                    DB::table('b2b_slajder_stavka_jezik')->insert($langData);
                }
            }

            $tipNaziv = DB::table('b2b_slajder_tip')->where('b2b_slajder_tip_id',DB::table('b2b_slajder')->where('b2b_slajder_id',$b2b_slajder_id)->pluck('b2b_slajder_tip_id'))->pluck('naziv');
            if($tipNaziv == 'Slajder'){
                AdminSupport::saveLog('SLAJDERI_IZMENI', array($b2b_slajder_id));  
            }else{
                AdminSupport::saveLog('BANERI_IZMENI', array($b2b_slajder_id));
            }

            return Redirect::to('/admin/b2b/slider/'.$b2b_slajder_id.'/'.$b2b_slajder_stavka_id.'/'.$jezik_id)->with('message','Uspešno ste sačuvali podatke.');

        }    
    }

    public function slider_delete($slider_id){
        DB::table('b2b_sekcija_stranice')->where(['b2b_slajder_id'=>$slider_id])->update(['b2b_slajder_id'=>null]);
        DB::table('b2b_slajder_stavka')->where('b2b_slajder_id',$slider_id)->delete();
        DB::table('b2b_slajder')->where('b2b_slajder_id',$slider_id)->delete();

        $tipNaziv = DB::table('b2b_slajder_tip')->where('b2b_slajder_tip_id',DB::table('b2b_slajder')->where('b2b_slajder_id',$slider_id)->pluck('b2b_slajder_tip_id'))->pluck('naziv');
        if($tipNaziv == 'Slajder'){
            AdminSupport::saveLog('SLAJDERI_OBRISI', array($slider_id));  
        }else{
            AdminSupport::saveLog('BANERI_OBRISI', array($slider_id));
        }

        return Redirect::to('/admin/b2b/slider/0');
    }
    public function slider_item_delete($b2b_slider_item_id){
        $b2b_slider_id = DB::table('b2b_slajder_stavka')->where('b2b_slajder_stavka_id',$b2b_slider_item_id)->pluck('b2b_slajder_id');
        DB::table('b2b_slajder_stavka')->where('b2b_slajder_stavka_id',$b2b_slider_item_id)->delete();

        $tipNaziv = DB::table('b2b_slajder_tip')->where('b2b_slajder_tip_id',DB::table('b2b_slajder')->where('b2b_slajder_id',$b2b_slider_item_id)->pluck('b2b_slajder_tip_id'))->pluck('naziv');
        if($tipNaziv == 'Slajder'){
            AdminSupport::saveLog('SLAJDERI_IZMENI', array($b2b_slider_id));  
        }else{
            AdminSupport::saveLog('BANERI_IZMENI', array($b2b_slider_id));
        }

        return Redirect::to('/admin/b2b/slider/'.$b2b_slider_id.'/0');
    }

    public function slider_items_position(){
        $b2b_slajder_id = Input::get('b2b_slajder_id');
        $order_arr = Input::get('order');

        foreach($order_arr as $key => $val){
            DB::table('b2b_slajder_stavka')->where(['b2b_slajder_id' => $b2b_slajder_id, 'b2b_slajder_stavka_id'=>$val])->update(array('rbr'=>($key+1)));
        }
        
        $tip = DB::table('b2b_slajder_tip')->where('b2b_slajder_tip_id',DB::table('b2b_slajder')->where('b2b_slajder_id',$b2b_slajder_id)->pluck('b2b_slajder_tip_id'))->pluck('naziv');

        if($tip == 'Slajder'){
            AdminSupport::saveLog('SLAJDER_POZICIJA', array($b2b_slajder_id));
        }else{
            AdminSupport::saveLog('BANER_POZICIJA', array($b2b_slajder_id));
        }
    }

    public function bg_uload(){
        
        $slika_ime=$_FILES['bgImg']['name'];
        $slika_tmp_ime=$_FILES['bgImg']['tmp_name'];
        $naziv = Input::get('naziv');
        if( Input::get('link')){
            $link  = Input::get('link');
        }else{
             $link  = '#!';
        }
        if( Input::get('link2')){
            $link2  = Input::get('link2');
        }else{
             $link2  = '#!';
        }
        if ( !empty($slika_ime) ) {
            move_uploaded_file($slika_tmp_ime,"./images/upload/" . $slika_ime);

        $data = array(
            'naziv' => $naziv,
            'link' => $link,
            'link2' => $link2,
            'img' => "./images/upload/" . $slika_ime,
            'tip_prikaza' => 3
        );
        
        $log_ids_arr = DB::select("SELECT baneri_id from baneri where tip_prikaza = '3'");

        $count = DB::table('baneri')->where('tip_prikaza', 3)->count();

        if($count == 1){
            DB::table('baneri')->where('tip_prikaza', 3)->update($data);
        } else {
            DB::table('baneri')->where('tip_prikaza', 3)->insert($data);
        }          

        AdminSupport::saveLog('BANERI_SLIKA');
        return Redirect::back()->with('message','Uspešno ste dodali sliku.');
        }elseif(!empty($link) || empty($link)){
            $data = array(
                'naziv' => $naziv,
                'link' => $link,
                'link2' => $link2,
                'tip_prikaza' => 3
            );

            $count = DB::table('baneri')->where('tip_prikaza', 3)->count();

            if($count == 1){
                DB::table('baneri')->where('tip_prikaza', 3)->update($data);
            } else {
                DB::table('baneri')->where('tip_prikaza', 3)->insert($data);
            } 

            $log_ids_arr = DB::table('baneri')->max('baneri_id');

            AdminSupport::saveLog('BANERI_LINKOVI');

            return Redirect::back()->with('message','Uspešno ste dodali linkove.'); 
        }elseif(!empty($link2) || empty($link2)){
            $data = array(
                'naziv' => $naziv,
                'link' => $link,
                'link2' => $link2,
                'tip_prikaza' => 3
            );

            $count = DB::table('baneri')->where('tip_prikaza', 3)->count();

            if($count == 1){
                DB::table('baneri')->where('tip_prikaza', 3)->update($data);
            } else {
                DB::table('baneri')->where('tip_prikaza', 3)->insert($data);
            } 

            AdminSupport::saveLog('BANERI_LINKOVI', $log_ids_arr);

            return Redirect::back()->with('message','Uspešno ste dodali linkove.');
        }else{
            return Redirect::back()->with('message','Unesite podatke.');
        }
    }

    public function bgImgDelete() {
        AdminSupport::saveLog('BANERI_SLIKA_OBRISI');
        DB::table('baneri')->where('tip_prikaza', 3)->delete();
        return Redirect::back();
    }

}