<?php
use Service\TranslatorService;

class AdminB2BVestiController extends Controller {

	 public function vesti($active=null){

        $data=array(
            "strana"=>'b2b_vesti',
            "title"=> 'Vesti',
            "vesti" => AdminB2BVesti::getVesti($active),
            "count_aktivne" => DB::table('web_vest_b2b')->where('aktuelno', 1)->count(),
            "count_neaktivne" => DB::table('web_vest_b2b')->where('aktuelno', 0)->count(),
            "count_sve" => DB::table('web_vest_b2b')->count()

            );
        return View::make('adminb2b/pages/vesti', $data);
    }

    public function vest($id,$jezik_id=1){

        $vesti_jezik = null;  
        if($id != 0){
            $vesti_jezik=DB::table('web_vest_b2b_jezik')->where(array('web_vest_b2b_id'=>$id, 'jezik_id'=>$jezik_id))->first();
            $seo = AdminSeo::vest_b2b($id,$jezik_id);
        }
        $data=array(
            "strana" =>'vest',
            "title" => 'Vest',
            "web_vest_b2b_id" => $id,
            "naslov" => $vesti_jezik ? $vesti_jezik->naslov : '',
            "rbr" => $id ? AdminB2BVesti::find($id, 'rbr') : DB::table('web_vest_b2b')->max('rbr')+1,            
            "tekst" =>  $vesti_jezik ? $vesti_jezik->sadrzaj : '',
            "aktuelno" => $id ? AdminB2BVesti::find($id, 'aktuelno') : 1,          
            "slika" => $id ? AdminB2BVesti::find($id, 'slika') : null,
            "jezik_id"=>$jezik_id,
            "jezici" => DB::table('jezik')->where('aktivan',1)->get(),
            "seo_title" => $id != 0 ? $seo->title : '',
            "keywords" => $id != 0 ? $seo->keywords : '',
            "description" => $id != 0 ? $seo->description : ''
        );

        return View::make('adminb2b/pages/vest_single', $data);
    }

    public function saveNews(){
        $data = Input::get();
        $naslov = $data['naslov'];
        $rbr = $data['rbr'];
        $tekst = $data['tekst'];
        $id = $data['id'];
        $jezik_id = $data['jezik_id'];

        if(isset($data['aktuelno'])) {
            $aktuelno = 1;
        } else {
            $aktuelno = 0;
        }


        $rules = array(
            'naslov' => 'required|regex:'.AdminSupport::regex().'|max:159',
            'seo_title' => 'max:60',
            'description' => 'max:320',
            'keywords' => 'max:159',
            'rbr' => 'numeric|digits_between:1,10'
            );
        $validator = Validator::make($data, $rules);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/b2b/vest'.'/'.$id)->withInput()->withErrors($validator->messages());
        }else{
            if($rbr == ''){
                $rbr = null;
            }

            if($id == 0) {
                DB::table('web_vest_b2b')->insert(['rbr' => $rbr, 'datum' => date('Y-m-d'), 'aktuelno' => $aktuelno]);
                $id = DB::select("SELECT MAX(web_vest_b2b_id) FROM web_vest_b2b")[0]->max;

            } else {
                DB::table('web_vest_b2b')->where('web_vest_b2b_id', $id)->update(['rbr' => $rbr, 'aktuelno' => $aktuelno]);
            }
            
            if(Input::hasFile('news_img')){     
                $slika = Input::file('news_img');
                $extension = $slika->getClientOriginalExtension();
                $slika->move('images/vesti-b2b/',$id.'.'.$extension);
                DB::table('web_vest_b2b')->where('web_vest_b2b_id', $id)->update(['slika' => 'images/vesti-b2b/'.$id.'.'.$extension]);
            }

            $query = DB::table('web_vest_b2b_jezik')->where(array('web_vest_b2b_id'=>$id, 'jezik_id'=>$jezik_id));
            $jezik_data = array('naslov'=>$naslov,'sadrzaj'=>$tekst,'title'=>$data['seo_title'],'description'=>$data['description'],'keywords'=>$data['keywords']);

            if(!is_null($query->first())){
                $query->update($jezik_data);
                AdminSupport::saveLog('B2B_VESTI_IZMENI', array($id));
            }else{
                $jezik_data['web_vest_b2b_id'] = $id;
                $jezik_data['jezik_id'] = $jezik_id;
                DB::table('web_vest_b2b_jezik')->insert($jezik_data);
                if($jezik_id==1){
                    foreach(DB::table('jezik')->where('aktivan',1)->where('jezik_id','!=',1)->orderBy('izabrani','desc')->get() as $jezik){
                        $translator = new TranslatorService(DB::table('jezik')->where('jezik_id',1)->pluck('kod'),$jezik->kod);
                        $jezik_data = array('web_vest_b2b_id' => $id, 'jezik_id' => $jezik->jezik_id, 'naslov'=>$translator->translate($naslov),'sadrzaj'=>$translator->translate(html_entity_decode($tekst)),'title'=>$translator->translate($data['seo_title']),'description'=>$translator->translate($data['description']),'keywords'=>$translator->translate($data['keywords']));
                        DB::table('web_vest_b2b_jezik')->insert($jezik_data);                 
                    }
                }
                AdminSupport::saveLog('B2B_VESTI_DODAJ', array(DB::table('web_vest_b2b')->max('web_vest_b2b_id')));
            }
            return Redirect::to(AdminOptions::base_url().'admin/b2b/vest/' . $id.($jezik_id==1 ? '' : '/'.$jezik_id))->with('message', 'Uspešno ste sačuvali vest!');
        }
    }

    public function deleteNews($web_vest_b2b_id){
        AdminSupport::saveLog('B2B_VESTI_OBRISI', array($web_vest_b2b_id));
        DB::table('web_vest_b2b')->where('web_vest_b2b_id', $web_vest_b2b_id)->delete();
        Session::flash('message', 'Uspešno ste izbrisali vest!');
        
        return Redirect::to(AdminOptions::base_url().'admin/b2b/vesti');

    }

    public function filter($id) {
        if($id == 'aktivni') {
            $vesti = DB::table('web_vest_b2b')->where('aktuelno', 1)->paginate(20);
        } elseif($id == 'neaktivni') {
            $vesti = DB::table('web_vest_b2b')->where('aktuelno', 0)->orderBy('datum', 'DESC')->orderBy('naslov', 'ASC')->paginate(20);
        }
        

        $data=array(
            "strana"=>'vesti',
            "title"=> 'Vesti',
            "vesti" => $vesti,
            "count_aktivne" => DB::table('web_vest_b2b')->where('aktuelno', 1)->count(),
            "count_neaktivne" => DB::table('web_vest_b2b')->where('aktuelno', 0)->count(),
            "count_sve" => DB::table('web_vest_b2b')->count()

            );
        return View::make('adminb2b/pages/vesti', $data);
    }





	
}