<?php

class AdminB2BPagesController extends Controller {

   public function stranice($stranica_id,$jezik_id=1,$page_section_id=0,$page_section_type_id=null){
        if(is_null($jezik_id)){
            $jezik_id = DB::table('jezik')->where(array('aktivan'=>1,'izabrani'=>1))->pluck('jezik_id');
        }
        $query_list_pages=DB::table('web_b2b_seo')->orderBy('rb_strane','asc')->get();
        $query_stranica=DB::table('web_b2b_seo')->where('web_b2b_seo_id',$stranica_id)->first();
        $stranice=DB::table('web_b2b_seo')->where('web_b2b_seo_id',$stranica_id)->get();
        if(!is_null($query_stranica)){
            $stranica_jezik=DB::table('web_b2b_seo_jezik')->where(array('web_b2b_seo_id'=>$query_stranica->web_b2b_seo_id, 'jezik_id'=>$jezik_id))->first();
            $seo = Seo::b2b_page($query_stranica->naziv_stranice,$jezik_id);
        }
        $b2b_stranica_sekcije = $stranica_id > 0 ? AdminB2BSupport::pageSections($stranica_id) : [];
        $sekcije = count($b2b_stranica_sekcije) > 0 ?  DB::table('b2b_sekcija_stranice')->where('flag_aktivan',1)->whereNotIn('b2b_sekcija_stranice_id',array_map(function($item){ return $item->b2b_sekcija_stranice_id; },$b2b_stranica_sekcije))->get() : DB::table('b2b_sekcija_stranice')->where('flag_aktivan',1)->get();

        if(is_null($page_section_type_id)){
            if(!is_null($page_section_id) && $page_section_id > 0){
                $page_section_type_id = DB::table('b2b_sekcija_stranice')->where('b2b_sekcija_stranice_id',$page_section_id)->pluck('b2b_sekcija_stranice_tip_id');
            }else{
                $page_section_type_id = DB::table('b2b_sekcija_stranice_tip')->orderBy('rbr','asc')->pluck('b2b_sekcija_stranice_tip_id');
            }
        }

        $data=array(
        "strana"=>'b2b_stranice',
        "title"=>"Stranice",
        "query_list_pages"=>$query_list_pages,
        "stranice"=>$stranice,
        "stranica"=>!is_null($query_stranica) ? $query_stranica->naziv_stranice : 'nova',
        "jezik_id"=>$jezik_id,
        "jezici" => DB::table('jezik')->where('aktivan',1)->get(),
        'sekcije' => $sekcije,
        'b2b_stranica_sekcije' => $b2b_stranica_sekcije,
        'sections' => DB::table('b2b_sekcija_stranice')->orderBy('b2b_sekcija_stranice_id','asc')->get(),
        'page_section' => $page_section_id > 0 ? DB::table('b2b_sekcija_stranice')->where('b2b_sekcija_stranice_id',$page_section_id)->first() : (object) ['b2b_sekcija_stranice_id' => 0, 'b2b_sekcija_stranice_tip_id' => $page_section_type_id, 'naziv'=>'', 'flag_aktivan' => 1, 'boja_pozadine' => null, 'puna_sirina' => 0, 'b2b_slajder_id' => 0, 'tip_artikla_id' => null],
        'page_section_lang' => $page_section_id > 0 && !is_null($page_section_lang=AdminB2BSupport::sectionLang($page_section_id,$jezik_id)) ? $page_section_lang : (object) ['sadrzaj' => ''],
        'page_section_type' => AdminB2BSupport::getSectionType($page_section_type_id)
        );

        if($stranica_id==0 || is_null($query_stranica)){
            $data['jezik_id']=$jezik_id;
            $data['naziv_stranice']='';
            $data['naziv']='';
            $data['seo_title']='';
            $data['content']='';
            $data['keywords']='';
            // $data['tekst']='';
            $data['desription']='';
            $data['status']=1;
            $data['flag_page']=0;
            $data['web_b2b_seo_id']=0;
            $data['naziv_stranice']='';
            $data['flag_b2b'] = 0;
            $data['tip_artikla_id']=-1;
            $data['parent_id']=0;
            $data['grupa_pr_id']=-1;
            $data['redirect_web_b2b_seo_id']=null;
            // $data['anketa_id']=null;
            $data['disable']=0;
            $data['menu_top']=false;
            $data['header_menu']=false;
            $data['footer']=false;
            $data['b2b_header']=false;
            $data['b2b_footer']=false;
        } else {
            $data['jezik_id']=$jezik_id;
            $data['naziv_stranice']=$query_stranica->naziv_stranice;
            $data['naziv']= ($stranica_jezik && !is_null($stranica_jezik->naziv) && $stranica_jezik->naziv != '') ? $stranica_jezik->naziv : $query_stranica->title;
            // $data['tekst']=$query_stranica->tekst;
            $data['seo_title']= $seo->title;
            $data['content']= $stranica_jezik ? $stranica_jezik->sadrzaj : '';
            $data['keywords']= $seo->keywords;
            $data['desription'] = $seo->description;
            $data['status']=2;
            $data['flag_page']=$query_stranica->flag_page;
            $data['web_b2b_seo_id']=$query_stranica->web_b2b_seo_id;
            $data['naziv_stranice']=$query_stranica->naziv_stranice;
            $data['flag_b2b']=$query_stranica->flag_b2b_show;
            $data['tip_artikla_id']=$query_stranica->tip_artikla_id;
            $data['parent_id']=$query_stranica->parent_id;
            $data['grupa_pr_id']=$query_stranica->grupa_pr_id;
            $data['redirect_web_b2b_seo_id']=$query_stranica->redirect_web_b2b_seo_id;
            // $data['anketa_id']=$query_stranica->anketa_id;
            $data['disable']=$query_stranica->disable;
            $data['menu_top']=$query_stranica->menu_top ? true : false;
            $data['header_menu']=$query_stranica->header_menu ? true : false;
            $data['footer']=$query_stranica->footer ? true : false;
            $data['b2b_header']=$query_stranica->b2b_header ? true : false;
            $data['b2b_footer']=$query_stranica->b2b_footer ? true : false;

        }    
        return View::make('adminb2b/pages/b2b_stranice', $data);
    }

    public function store(){
     $web_b2b_seo_id=Input::get('web_b2b_seo_id');
        $disable = 0;
        if($web_b2b_seo_id != 0){
            $web_b2b_seo = DB::table('web_b2b_seo')->where('web_b2b_seo_id',$web_b2b_seo_id)->first();
            if($web_b2b_seo){
                $disable = $web_b2b_seo->disable;
            }else{
                return Redirect::to(AdminOptions::base_url().'admin/b2b/b2b_stranice/nova');
            }
        }
        $jezik_id=Input::get('jezik_id');
        $query_jezik = DB::table('web_b2b_seo_jezik')->where(array('web_b2b_seo_id'=>$web_b2b_seo_id, 'jezik_id'=>$jezik_id));

        $original_naslov = Input::get('naziv_stranice');
        $naslov= $disable==0 ? $original_naslov : ($jezik_id == 1 ? $web_b2b_seo->title : (!is_null($query_jezik->first()) ? $query_jezik->first()->naziv : $web_b2b_seo->title));

        $status=Input::get('status');
        $content= null;
        $seo_title=Input::get('seo_title');
        $keywords=Input::get('keywords');
        $description=Input::get('description');
        $header_menu=Input::get('header_menu');
        $footer_menu=Input::get('footer_menu');
        $top_menu=Input::get('top_menu');
        $b2b_header_menu=Input::get('b2b_header_menu');
        $b2b_footer_menu=Input::get('b2b_footer_menu');
        $parent_id= Input::get('parent_id');
        $naziv_stranice=AdminOptions::slugify($naslov);
        $tip_artikla_id= $disable==0 ? Input::get('tip_artikla_id') : -1;
        $grupa_pr_id= $disable==0 ? Input::get('grupa_pr_id') : -1;
        $redirect_web_b2b_seo_id= $disable==0 && is_numeric(Input::get('redirect_web_b2b_seo_id')) && Input::get('redirect_web_b2b_seo_id') > 0 ? Input::get('redirect_web_b2b_seo_id') : null;
           
        $data=array(
        'naziv_stranice'=>$naziv_stranice,
        'flag_page'=>0,
        'title'=>$naslov,
        'flag_b2b_show'=>0,
        'tip_artikla_id' => $tip_artikla_id,
        'grupa_pr_id' => $grupa_pr_id,
        'parent_id' => $parent_id,
        'menu_top' => isset($top_menu) ? 1 : 0,
        'header_menu' => isset($header_menu) ? 1 : 0,
        'footer' => isset($footer_menu) ? 1 : 0,
        'b2b_header' => isset($b2b_header_menu) ? 1 : 0,
        'b2b_footer' => isset($b2b_footer_menu) ? 1 : 0,
        'redirect_web_b2b_seo_id' => $redirect_web_b2b_seo_id,
        );
        $seo_data = array('naslov'=>$naslov,'seo_title'=>$seo_title,'description'=>$description,'keywords'=>$keywords);
        $validator_arr = array(
            'seo_title' => 'regex:'.AdminSupport::regex().'|max:60',
            'description' => 'regex:'.AdminSupport::regex().'|max:320',
            'keywords' => 'regex:'.AdminSupport::regex().'|max:159'
        );
        if($disable==0){
            $validator_arr['naslov'] = 'required|regex:'.AdminSupport::regex().'|max:255|unique:web_b2b_seo,title,'.$web_b2b_seo_id.',web_b2b_seo_id';
        }
        $validator = Validator::make($seo_data,$validator_arr);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/b2b/b2b_stranice/'.$web_b2b_seo_id.($jezik_id != 1 ? '/'.$jezik_id : ''))->withInput()->withErrors($validator->messages());
        }

        if(!($disable==0 AND AdminOptions::is_shop())){
            $data['tip_artikla_id'] = -1;
            $data['grupa_pr_id'] = -1;
        }
        if($web_b2b_seo_id==0){
            $old_value = DB::table('web_b2b_seo')->max('web_b2b_seo_id');
            DB::table('web_b2b_seo')->insert($data);
            $new_value = DB::table('web_b2b_seo')->max('web_b2b_seo_id');
            $web_b2b_seo_id = DB::table('web_b2b_seo')->where('naziv_stranice',$naziv_stranice)->pluck('web_b2b_seo_id');
            AdminSupport::saveLog('STRANICA_DODAJ', array(DB::table('web_b2b_seo')->max('web_b2b_seo_id')));
        }
        else {
            if($jezik_id==1 || DB::table('jezik')->where(array('aktivan'=>1))->count() == 1){
                DB::table('web_b2b_seo')->where('web_b2b_seo_id',$web_b2b_seo_id)->update($data);
            }

            AdminSupport::saveLog('STRANICA_IZMENI', array($web_b2b_seo_id));
        }

        //lang
        $jezik_data = array('naziv'=> $naslov, 'slug' => AdminOptions::slugify($naslov), 'sadrzaj'=>$content,'title'=>$seo_title,'description'=>$description,'keywords'=>$keywords);
        if(!is_null($query_jezik->first())){
            $query_jezik->update($jezik_data);

            if((!is_null($current_jezik = DB::table('jezik')->where(array('aktivan'=>1,'kod'=>'sr','jezik_id'=>$jezik_id))->first()) && !is_null($jezik = DB::table('jezik')->where(array('aktivan'=>1,'kod'=>'cir'))->where('jezik_id','!=',$jezik_id)->first()))
                || (!is_null($current_jezik = DB::table('jezik')->where(array('aktivan'=>1,'kod'=>'cir','jezik_id'=>$jezik_id))->first()) && !is_null($jezik = DB::table('jezik')->where(array('aktivan'=>1,'kod'=>'sr'))->where('jezik_id','!=',$jezik_id)->first()))
            ){
                $translator = new TranslatorService($current_jezik->kod,$jezik->kod);
                $naslov = $translator->translate($naslov);
                $jezik_data = array('naziv'=> $naslov, 'slug' => AdminOptions::slugify($naslov), 'sadrzaj'=>$translator->translate(html_entity_decode($content)),'title'=>$translator->translate($seo_title),'description'=>$translator->translate($description),'keywords'=>$translator->translate($keywords));
                DB::table('web_b2b_seo_jezik')->where(array('web_b2b_seo_id' => $web_b2b_seo_id, 'jezik_id' => $jezik->jezik_id))->update($jezik_data);
            }            
        }else{
            $jezik_data['web_b2b_seo_id'] = $web_b2b_seo_id;
            $jezik_data['jezik_id'] = $jezik_id;
            DB::table('web_b2b_seo_jezik')->insert($jezik_data);

            if($jezik_id==1){
                foreach(DB::table('jezik')->where('aktivan',1)->where('jezik_id','!=',$jezik_id)->orderBy('izabrani','desc')->get() as $jezik){
                    $translator = new TranslatorService(DB::table('jezik')->where('jezik_id',$jezik_id)->pluck('kod'),$jezik->kod);
                    $naslov = $translator->translate($naslov);
                    $jezik_data = array('web_b2b_seo_id' => $web_b2b_seo_id, 'jezik_id' => $jezik->jezik_id, 'naziv'=> $naslov, 'slug' => AdminOptions::slugify($naslov), 'sadrzaj'=>$translator->translate(html_entity_decode($content)),'title'=>$translator->translate($seo_title),'description'=>$translator->translate($description),'keywords'=>$translator->translate($keywords));
                    DB::table('web_b2b_seo_jezik')->insert($jezik_data);                 
                }
            }
        }

        return Redirect::to(AdminOptions::base_url().'admin/b2b/b2b_stranice/'.$web_b2b_seo_id.($jezik_id != 1 ? '/'.$jezik_id : ''))->with('message','Uspešno ste sačuvali podatke.');

}
     function page_section_position(){
        $web_b2b_seo_id = Input::get('web_b2b_seo_id');
        $order_arr = Input::get('order');

        foreach($order_arr as $rbr => $b2b_sekcija_stranice_id){
            DB::table('b2b_stranica_sekcija')->where(array('web_b2b_seo_id'=>$web_b2b_seo_id, 'b2b_sekcija_stranice_id' => $b2b_sekcija_stranice_id))->update(array('rbr'=>$rbr));
            $log_ids_arr[] = $b2b_sekcija_stranice_id;
        }

        AdminSupport::saveLog('SEKCIJA_STRANICE_POZICIJA', array($web_b2b_seo_id));
    }
    
    function page_section_add(){
        $web_b2b_seo_id = Input::get('web_b2b_seo_id');
        $b2b_sekcija_stranice_id = Input::get('b2b_sekcija_stranice_id');
        $log_ids_arr = array();

        $rbr = count(DB::table('b2b_stranica_sekcija')->where(array('web_b2b_seo_id'=>$web_b2b_seo_id))->get()) > 0 ? (DB::table('b2b_stranica_sekcija')->where(array('web_b2b_seo_id'=>$web_b2b_seo_id))->max('rbr')+1) : 1;
        DB::table('b2b_stranica_sekcija')->insert(array('web_b2b_seo_id'=>$web_b2b_seo_id, 'b2b_sekcija_stranice_id' => $b2b_sekcija_stranice_id, 'rbr'=>$rbr));

        AdminSupport::saveLog('SEKCIJA_STRANICE_DODAVANJE', array($web_b2b_seo_id));
    }
    
    function page_section_remove($page_id,$section_id){
        DB::table('b2b_stranica_sekcija')->where(array('web_b2b_seo_id'=>$page_id, 'b2b_sekcija_stranice_id' => $section_id))->delete();

        AdminSupport::saveLog('SEKCIJA_STRANICE_UKLANJANJE', array($page_id));

        return Redirect::to('/admin/b2b/b2b_stranice/'.$page_id);
    }

    function page_section_edit(){
        $data = Input::get();
        $b2b_sekcija_stranice_id = $data['b2b_sekcija_stranice_id'];
        $jezik_id = $data['jezik_id'];
        unset($data['b2b_sekcija_stranice_id']);
        unset($data['jezik_id']);
        $tipNaziv = DB::table('b2b_sekcija_stranice_tip')->where('b2b_sekcija_stranice_tip_id',$data['b2b_sekcija_stranice_tip_id'])->pluck('naziv');

        $validator = Validator::make($data,
        array(
            'b2b_sekcija_stranice_tip_id' => 'required|integer',
            'naziv' => 'required|regex:'.AdminSupport::regex().'|max:255',
            'flag_aktivan' => 'required|in:0,1',
            'b2b_slajder_id' => ($tipNaziv == 'Slajder' ? 'required|' : '').'integer',
            'tip_artikla_id' => ($tipNaziv == 'Lista artikala' ? 'required|' : '').'integer'
        ),
        array(
            'required' => 'Niste popunili polje!',
            'regex' => 'Unesite odgovarajuči karakter!',
            'max' => 'Uneli ste neodgovarajući broj karaktera!',
            'integer' => 'Vrednost mora da bude broj',
            'in' => 'Unesite odgovarajuči karakter!'
        ));

        if($validator->fails()){
            // return Redirect::back()->withInput()->withErrors($validator->messages());
            return Redirect::to(URL::previous() . "#page_id")->withInput()->withErrors($validator->messages());
        }else{
            if($b2b_sekcija_stranice_id == 0){
                DB::table('b2b_sekcija_stranice')->insert([
                    'b2b_sekcija_stranice_tip_id' => $data['b2b_sekcija_stranice_tip_id'],
                    'naziv' => $data['naziv'],
                    'flag_aktivan' => $data['flag_aktivan'],
                    'puna_sirina' => $data['puna_sirina'],
                    'boja_pozadine' => $data['boja_pozadine'] != '#000000' ? $data['boja_pozadine'] : null,
                    'b2b_slajder_id' => isset($data['b2b_slajder_id']) && !empty($data['b2b_slajder_id']) ? $data['b2b_slajder_id'] : null,
                    'tip_artikla_id' => isset($data['tip_artikla_id']) && !empty($data['tip_artikla_id']) ? $data['tip_artikla_id'] : null,
                ]);

                $b2b_sekcija_stranice_id = DB::table('b2b_sekcija_stranice')->max('b2b_sekcija_stranice_id');
            }else{
                DB::table('b2b_sekcija_stranice')->where('b2b_sekcija_stranice_id',$b2b_sekcija_stranice_id)->update([
                    'b2b_sekcija_stranice_tip_id' => $data['b2b_sekcija_stranice_tip_id'],
                    'naziv' => $data['naziv'],
                    'flag_aktivan' => $data['flag_aktivan'],
                    'puna_sirina' => $data['puna_sirina'],
                    'boja_pozadine' => $data['boja_pozadine'] != '#000000' ? $data['boja_pozadine'] : null,
                    'b2b_slajder_id' => isset($data['b2b_slajder_id']) ? $data['b2b_slajder_id'] : null,
                    'tip_artikla_id' => isset($data['tip_artikla_id']) ? $data['tip_artikla_id'] : null,
                ]);
            }

            if(isset($data['sadrzaj']) && !empty($data['sadrzaj'])){        
                if(!is_null($b2b_sekcija_stranice_jezik = DB::table('b2b_sekcija_stranice_jezik')->where(['b2b_sekcija_stranice_id'=>$b2b_sekcija_stranice_id,'jezik_id'=>$jezik_id])->first())){

                    DB::table('b2b_sekcija_stranice_jezik')->where('b2b_sekcija_stranice_jezik_id',$b2b_sekcija_stranice_jezik->b2b_sekcija_stranice_jezik_id)->update([
                        'sadrzaj' => $data['sadrzaj']
                    ]);
                }else{
                    DB::table('b2b_sekcija_stranice_jezik')->insert([
                        'b2b_sekcija_stranice_id' => $b2b_sekcija_stranice_id,
                        'jezik_id' => $jezik_id,
                        'sadrzaj' => $data['sadrzaj']
                    ]);
                }
            }

            // return Redirect::to('/admin/page-section/'.$b2b_sekcija_stranice_id.'/'.$data['sekcija_stranice_tip_id'].'/'.$jezik_id)->with('message','Uspešno ste sačuvali podatke.');
            return Redirect::to(URL::previous() . "#page_id")->with('message','Uspešno ste sačuvali podatke.');

        }    
    }

    function position(){
        $moved = Input::get('moved');
        $parent_id = Input::get('parent_id');
        $order_arr = Input::get('order');
        $log_ids_arr = array();

        foreach($order_arr as $key => $val){
            DB::table('web_b2b_seo')->where(array('parent_id'=>$parent_id, 'web_b2b_seo_id'=>$val))->update(array('rb_strane'=>$key));
            $log_ids_arr[] = $val;
        }

        AdminSupport::saveLog('POZICIJA_STRANICA', array($moved));
        
    }

    public function page_section_delete($page_section_id){
        DB::table('b2b_sekcija_stranice')->where('b2b_sekcija_stranice_id',$page_section_id)->delete();
        // return Redirect::to('/admin/page-section/0');
        return Redirect::to(URL::previous() . "#page_id")->with('message','Uspešno ste sačuvali podatke.');
    }

    public function delete_page(){

        $web_b2b_seo_id = Input::get('web_b2b_seo_id');
        if($web_b2b_seo_id != 0){
            AdminSupport::saveLog('B2B_PRODAVNICA_OBRISI', array($web_b2b_seo_id));
            DB::table('web_b2b_seo')->where(array('web_b2b_seo_id'=>$web_b2b_seo_id,'disable'=>0))->delete();            
        }
        return Redirect::to('admin/b2b/b2b_stranice/0')->with('message','Uspešno ste obrisali stranicu.');

    }
    public function delete_image(){
        $link=".".Input::get('url');
        if(File::exists($link)){
        File::delete($link);
        AdminSupport::saveLog('B2B_UPLOADOVANE_SLIKE_OBRISI');
        echo $link." Ok";
        }
        else {
        echo $link." Nije uredu";
        }
        
        
    }
    public function image_upload(){
            $slika_ime=$_FILES['img']['name'];
            $slika_tmp_ime=$_FILES['img']['tmp_name'];
            move_uploaded_file($slika_tmp_ime,"images/upload/$slika_ime");
            AdminSupport::saveLog('B2B_STRANICE_SLIKA_POSTAVI');
            return Redirect::back();
          // echo Admin_model::upload_directory();
    }   
}