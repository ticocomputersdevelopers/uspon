<?php

class AdminB2BPartnerController extends Controller {

    function proizvodjaci($partner_id=null){
        $proizvodjaci = DB::table('proizvodjac')->get();
        if ($partner_id!=-2) {
          // $rabati = AdminB2BProizvodjac::PartnerRabatGrupa();      
          // $proizvodjaci = !is_null($partner_id) ? AdminB2BProizvodjac::getManufacturers($partner_id) : array();
          foreach ($proizvodjaci as $proizvodjac) {
            $rabat = DB::table('partner_rabat_grupa')->where('grupa_pr_id', '=', -1)->where('partner_id', $partner_id)->where('proizvodjac_id', $proizvodjac->proizvodjac_id)->pluck('rabat');
             if($rabat != null) {
                    $proizvodjac->rabat = $rabat;                
            // var_dump($proizvodjac->rabat);
            // die();
            }else{
                $proizvodjac->rabat = 0;
            }
          }
        }
        $partneri = DB::table('partner')->orderBy('naziv', 'ASC')->get();

        if($partner_id == 'null'){
            $partner_id = -2;
        }

        $data=array(
            "strana" => 'proizvodjaci',
            "title" => 'Rabat partnera i proizvođača',
            "proizvodjaci" =>$proizvodjaci,
            "partneri" => $partneri,
            "partner_id" => $partner_id
        );
          return View::make('adminb2b/pages/partneri_proizvodjaci', $data);
    }

    function partneri(){

        $data=array(
            "strana"=>'partneri',
            "title"=> 'Rabat partnera',
            "partneri"=> DB::table('partner')->orderBy('naziv', 'ASC')->get()
        );
        return View::make('adminb2b/pages/partneri', $data);
    }

    function ajax() {
        $action = Input::get('action');

        if($action == 'rabat_proizvodjac_edit') {
            $proizvodjac_id = Input::get('proizvodjac_id');
            $rabat = Input::get('rabat');
            $partner_id = Input::get('partner_id');


            if ($partner_id == -2) {
                $query = DB::table('proizvodjac')->where('proizvodjac_id', $proizvodjac_id)->update(['rabat' => $rabat]);
            }else if($partner_id != -2){
                $provera = DB::table('partner_rabat_grupa')->where('proizvodjac_id', $proizvodjac_id)->where('partner_id', $partner_id)->where('grupa_pr_id', -1)->get();

                if($provera != null){
                    $query = DB::table('partner_rabat_grupa')->where('proizvodjac_id', $proizvodjac_id)->where('partner_id', $partner_id)->where('grupa_pr_id', -1)->update(['rabat' => $rabat]);
                }else{
                    $query = DB::table('partner_rabat_grupa')->insert(array('proizvodjac_id' => $proizvodjac_id, 'partner_id' => $partner_id, 'grupa_pr_id' => -1, 'rabat' => $rabat));

                }
            }
            if($query == true) {
                AdminSupport::saveLog('RABAT_PROIZVODJAC_IZMENI', array('proizvodjac' => $proizvodjac_id, 'partner' => $partner_id));
                return json_encode(1);
            } else {
                return json_encode(0);
            }
        }elseif ($action == 'edit_permision_manufacturer') {
            $proizvodjac_ids = Input::get('proizvodjac_ids');
            $checked = Input::get('checked') ==  1;
            $partner_id = Input::get('partner_id');

            foreach ($proizvodjac_ids as $proizvodjac_id ) {

                $provera = DB::table('nivo_pristupa_proizvodjac')->where('proizvodjac_id', $proizvodjac_id)->where('partner_id', $partner_id)->count()>0;

                if($checked){
                    $query = DB::table('nivo_pristupa_proizvodjac')->where('proizvodjac_id', $proizvodjac_id)->where('partner_id', $partner_id)->delete();
                }elseif(!$provera){
                    $query = DB::table('nivo_pristupa_proizvodjac')->insert(array('proizvodjac_id' => $proizvodjac_id, 'grupa_pr_id' => -1, 'partner_id' => $partner_id, 'unactive' => 1));
                }
            }

        }elseif($action == 'edit_permision_group') {
            $grupa_pr_ids = Input::get('grupa_pr_ids');
            $checked = Input::get('checked') ==  1;
            $partner_id = Input::get('partner_id');

            foreach ($grupa_pr_ids as $grupa_pr_id) {
                $provera = DB::table('nivo_pristupa_grupa')->where('grupa_pr_id',$grupa_pr_id)->where('partner_id', $partner_id)->count()>0;

                if($checked) {
                    $query = DB::table('nivo_pristupa_grupa')->where('grupa_pr_id', $grupa_pr_id)->where('partner_id', $partner_id)->delete();
                } elseif(!$provera){
                    $query = DB::table('nivo_pristupa_grupa')->insert(array('grupa_pr_id' => $grupa_pr_id,'partner_id' => $partner_id, 'unactive' => 1));
                }
            }
        }  elseif($action == 'edit_permision_article') {
            $roba_ids = Input::get('roba_ids');
            $checked = Input::get('checked') ==  1;
            $partner_id = Input::get('partner_id');
            foreach ($roba_ids as $roba_id) {
                $provera = DB::table('nivo_pristupa_artikal')->where('roba_id',$roba_id)->where('partner_id', $partner_id)->count()>0;
                if($checked) {
                    $query = DB::table('nivo_pristupa_artikal')->where('roba_id', $roba_id)->where('partner_id', $partner_id)->delete();
                } elseif(!$provera){
                    $query = DB::table('nivo_pristupa_artikal')->insert(array('roba_id' => $roba_id,'partner_id' => $partner_id, 'unactive' => 1));
                }
            }
        } elseif($action == 'edit_permision_article_all') {
            $roba_ids = DB::select("SELECT roba_id from roba where flag_aktivan=1 AND flag_cenovnik=1");
            $checked = Input::get('checked') ==  1;
            $partner_id = Input::get('partner_id');
            foreach ($roba_ids as $roba_id) {
                $provera = DB::table('nivo_pristupa_artikal')->where('roba_id',$roba_id->roba_id)->where('partner_id', $partner_id)->count()>0;
                if($checked) {
                    $query = DB::table('nivo_pristupa_artikal')->where('roba_id',$roba_id->roba_id)->where('partner_id', $partner_id)->delete();
                } elseif(!$provera){
                    $query = DB::table('nivo_pristupa_artikal')->insert(array('roba_id' => $roba_id->roba_id,'partner_id' => $partner_id, 'unactive' => 1));
                }
            }
        } elseif($action == 'edit_permision_page') {
            $web_b2b_seo_ids = Input::get('web_b2b_seo_ids');
            $checked = Input::get('checked') ==  1;
            $partner_id = Input::get('partner_id');

            foreach ($web_b2b_seo_ids as $web_b2b_seo_id) {
                $provera = DB::table('nivo_pristupa_strana')->where('web_b2b_seo_id',$web_b2b_seo_id)->where('partner_id', $partner_id)->count()>0;
                if($checked) {
                    $query = DB::table('nivo_pristupa_strana')->where('web_b2b_seo_id', $web_b2b_seo_id)->where('partner_id', $partner_id)->delete();
                } elseif(!$provera){
                    $query = DB::table('nivo_pristupa_strana')->insert(array('web_b2b_seo_id' => $web_b2b_seo_id,'partner_id' => $partner_id, 'unactive' => 1));
                }
            }
        } elseif($action == 'edit_permision_news') {
            $web_vest_b2b_ids = Input::get('web_vest_b2b_ids');
            $checked = Input::get('checked') ==  1;
            $partner_id = Input::get('partner_id');

            foreach ($web_vest_b2b_ids as $web_vest_b2b_id) {
                $provera = DB::table('nivo_pristupa_vest')->where('web_vest_b2b_id',$web_vest_b2b_id)->where('partner_id', $partner_id)->count()>0;
                if($checked) {
                    $query = DB::table('nivo_pristupa_vest')->where('web_vest_b2b_id', $web_vest_b2b_id)->where('partner_id', $partner_id)->delete();
                } elseif(!$provera){
                    $query = DB::table('nivo_pristupa_vest')->insert(array('web_vest_b2b_id' => $web_vest_b2b_id,'partner_id' => $partner_id, 'unactive' => 1));
                }
            }
        } elseif($action == 'edit_permision_warehouse') {
            $orgj_ids = Input::get('orgj_ids');
            $checked = Input::get('checked') ==  1;
            $partner_id = Input::get('partner_id');
            foreach ($orgj_ids as $orgj_id) {
                $provera = DB::table('nivo_pristupa_lager')->where('orgj_id',$orgj_id)->where('partner_id', $partner_id)->count()>0;
                if($checked) {
                    $query = DB::table('nivo_pristupa_lager')->where('orgj_id', $orgj_id)->where('partner_id', $partner_id)->delete();
                } elseif(!$provera){
                    $query = DB::table('nivo_pristupa_lager')->insert(array('orgj_id' => $orgj_id,'partner_id' => $partner_id, 'unactive' => 1));
                }
            }
        }


    }

    function rabat_partner_edit() {
            $partner_ids = Input::get('partner_ids');
            $rabat = Input::get('rabat');

            $response = array();

            foreach($partner_ids as $partner_id){
                DB::statement("UPDATE partner SET rabat = ". $rabat ." WHERE partner_id = ".$partner_id."");
                $response[$partner_id] = DB::table('partner')->where('partner_id',$partner_id)->pluck('rabat');
            }
            AdminSupport::saveLog('RABAT_PARTNER_IZMENI', $partner_ids);
            return json_encode($response);

    }

    function kategorija_partner_edit() {
            $partner_ids = Input::get('partner_ids');
            $kat = Input::get('kategorija');

            if($kat != 'Bez kategorije'){
            $kategorija = DB::table('partner_kategorija')->where('naziv',$kat)->pluck('id_kategorije');

            $response = array();
 
            foreach($partner_ids as $partner_id){

                DB::statement("UPDATE partner SET id_kategorije = ". $kategorija ." WHERE partner_id = ".$partner_id."");
                $response[$partner_id] = DB::table('partner_kategorija')->where('id_kategorije',$kategorija)->pluck('naziv');
            }

            AdminSupport::saveLog('KATEGORIJA_PARTNER_IZMENI', $partner_ids);

            return json_encode($response);
        }
        else if($kat == 'Bez kategorije'){

            $response = array();

            foreach($partner_ids as $partner_id){
                DB::statement("UPDATE partner SET id_kategorije = 0 WHERE partner_id = ".$partner_id."");
                $response[$partner_id] = "Bez kategorije";
            }

            AdminSupport::saveLog('KATEGORIJA_PARTNER_IZMENI', $partner_ids);

            return json_encode($response);
        }

    }


    public function partneri_search(){
        $word = trim(Input::get('search'));

        $data=array(
                    "strana"=>'partneri',
                    "title"=> 'Partneri',
                    "partneri" => AdminB2BSupport::searchPartneri($word),
                    "word" => $word
                );
            return View::make('adminb2b/pages/partneri', $data);
    }

    function rabat_kombinacije($search=null) {
        $page = Input::get('page') ? Input::get('page') : 1;
        $limit = 30;
        $offset = ($page-1)*$limit;

        $query = DB::table('partner_rabat_grupa')
            ->leftJoin('partner', 'partner_rabat_grupa.partner_id', '=', 'partner.partner_id')
            ->leftJoin('grupa_pr', 'partner_rabat_grupa.grupa_pr_id', '=', 'grupa_pr.grupa_pr_id')
            ->leftJoin('proizvodjac', 'partner_rabat_grupa.proizvodjac_id', '=', 'proizvodjac.proizvodjac_id')
            ->select('partner_rabat_grupa.*', 'partner.naziv as partner_naziv', 'grupa_pr.grupa', 'proizvodjac.naziv as proizvodjac_naziv');

        if(!empty($search)){
            $query = $query
                ->where('partner.partner_id', '!=', -1)
                ->where(function($q) use ($search){
                    $q->where('partner.naziv', 'ILIKE', "%".$search."%")->
                    orWhere('pib', 'ILIKE', "%".$search."%");
                });
        }
        $count_products = count($query->get());
        $kombinacije = $query->orderBy('id')
            ->limit($limit)->offset($offset)->get();

        $data=array(
            "strana" => 'rabat_kombinacije',
            "title" => 'Rabat kombinacije',
            "search" => !is_null($search) ? $search : '',
            "kombinacije" => $kombinacije,
            "count_products" => $count_products,
            "limit" => $limit,
            "proizvodjaci" => AdminB2BSupport::getProizvodjaci(),
            "partneri" => AdminB2BSupport::getDobavljaci()
            
        );
        return View::make('adminb2b/pages/rabat_kombinacije', $data);
    }

    function rabat_kombinacije_edit() {
        $input = Input::get();

        $query = DB::table('partner_rabat_grupa')->where('partner_id', $input['partner'])->where('grupa_pr_id', $input['grupa'])->where('proizvodjac_id', $input['proizvodjac'])->count();
        if($query == 0) {
            $insert = DB::table('partner_rabat_grupa')->insert(['partner_id' => $input['partner'], 'grupa_pr_id' => $input['grupa'], 'proizvodjac_id' => $input['proizvodjac'], 'rabat' => $input['rabat']]);
        } else {
            $update = DB::table('partner_rabat_grupa')->where('partner_id', $input['partner'])->where('grupa_pr_id', $input['grupa'])->where('proizvodjac_id', $input['proizvodjac'])->update(['rabat' => $input['rabat']]);
        }
        AdminSupport::saveLog('RABAT_KOMBINACIJA_DODAJ', array(DB::table('partner_rabat_grupa')->max('id')));

        return Redirect::to(AdminOptions::base_url().'admin/b2b/rabat_kombinacije');
    }

    function rabat_kombinacije_delete($id) {
        AdminSupport::saveLog('RABAT_KOMBINACIJE_OBRISI', array($id));
        $query = DB::table('partner_rabat_grupa')->where('id', $id)->delete();
        if($query == true) {
            return json_encode(1);
        } else {
            return json_encode(0);
        }
    }

     //partner_kategorije
    function partner_kategorija($id_kategorije=null){
        $data=array(
            "strana"=>'partner_kategorija',
            "title"=>$id_kategorije != null ? AdminB2BSupport::find_u_kategoriji($id_kategorije, 'naziv') : 'Nova kategorija',
            "id_kategorije"=>$id_kategorije,
            "naziv"=> $id_kategorije != null ? AdminB2BSupport::find_u_kategoriji($id_kategorije, 'naziv') : null,
            "rabat"=> $id_kategorije != null ? AdminB2BSupport::find_u_kategoriji($id_kategorije, 'rabat') : null,
            "active"=>$id_kategorije != null ? AdminB2BSupport::find_u_kategoriji($id_kategorije, 'active') : null 
        );
   
        return View::make('adminb2b/pages/partner_kategorija', $data);
    }

     //izmena kategorije
     function kategorije_edit()
    {   
        $inputs = Input::get();
        // die('test');
        $validator = Validator::make($inputs, array(
            'naziv' => 'required|max:100', 
            'rabat' => 'required|numeric' 
            ));
        if($validator->fails()){
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/partner_kategorija/'.$inputs['id_kategorije'])->withInput()->withErrors($validator->messages());
        }else{
            $general_data = $inputs;

            if($inputs['id_kategorije'] == 0){                
                $general_data['id_kategorije'] = DB::select("SELECT MAX(id_kategorije) AS max FROM partner_kategorija")[0]->max + 1;
            }

            if(isset($general_data['active'])){
                $general_data['active'] = 1;
            }else{
                $general_data['active'] = 0; 
            }

            if($inputs['id_kategorije'] != 0){
                $rabat = DB::table('partner_kategorija')->where('id_kategorije',$inputs['id_kategorije'])->pluck('rabat');
                DB::table('partner_kategorija')->where('id_kategorije',$inputs['id_kategorije'])->update($general_data);

                if ($rabat != DB::table('partner_kategorija')->where('id_kategorije',$inputs['id_kategorije'])->pluck('rabat')) {
                    AdminSupport::saveLog('B2B_KATEGORIJA_IZMENI_RAB', array($inputs['id_kategorije']));
                }else{
                    AdminSupport::saveLog('B2B_KATEGORIJA_IZMENI', array($inputs['id_kategorije']));
                }
            }else{
                DB::table('partner_kategorija')->insert($general_data);
                AdminSupport::saveLog('B2B_KATEGORIJA_DODAJ', array(DB::table('partner_kategorija')->max('id_kategorije')));
            }

            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/partner_kategorija/'.$general_data['id_kategorije'])->with('message',$message);
        }
    }

    //brisanje kategorije
    function kategorije_delete($id_kategorije)
    {   
        AdminSupport::saveLog('B2B_KATEGORIJA_OBRISI', array($id_kategorije));

        DB::table('partner_kategorija')->where('id_kategorije',$id_kategorije)->delete();
        

        return Redirect::to(AdminB2BOptions::base_url().'admin/b2b/partner_kategorija')->with('message','Uspešno ste obrisali sadržaj.');
    } 
    function proizvodjaci_partneri($partner_id=null){

        $proizvodjaci = DB::table('proizvodjac')->where('proizvodjac_id','>',0)->get();
        $partneri = DB::table('partner')->where('partner_id','>',0)->orderBy('naziv', 'ASC')->get();

        $data=array(
            "strana" => 'nivo_pristupa_proizvodjaci',
            "title" => 'Nivo pristupa',
            "proizvodjaci" =>$proizvodjaci,
            "partneri" => $partneri,
            "partner_id" => $partner_id
        );
          return View::make('adminb2b/pages/nivo_pristupa', $data);
    }
    function grupe_partneri($partner_id=null,$grupa_pr_id=null){

        $search=Input::get('search');
        if(is_null($search) || $search=='' || $search=='null'){
            $search=null;
        }

//         var_dump(trim($search)); die;

        $partneri = DB::table('partner')->where('partner_id','>',0)->orderBy('naziv', 'ASC')->get();

        if($grupa_pr_id > 0) {

            $roba_ids = DB::select("Select distinct t.roba_id from (
            select
            (select grupa_pr_id from grupa_pr where grupa_pr_id = (select parrent_grupa_pr_id from grupa_pr where grupa_pr_id = 
            (select parrent_grupa_pr_id from grupa_pr where grupa_pr_id = r.grupa_pr_id limit 1) limit 1)) as nadgrupa_nadgrupe_pr_id,
            (select grupa_pr_id from grupa_pr where grupa_pr_id = (select parrent_grupa_pr_id from grupa_pr where grupa_pr_id = r.grupa_pr_id limit 1)) nadgrupa_pr_id,
            (select grupa_pr_id from grupa_pr where grupa_pr_id = r.grupa_pr_id) as grupa_id,
            roba_id from roba r )  t where t.nadgrupa_nadgrupe_pr_id = ".$grupa_pr_id." or t.nadgrupa_pr_id = ".$grupa_pr_id." or t.grupa_id = ".$grupa_pr_id." ");

            $arr_roba_ids = array();
            foreach ($roba_ids as $roba) {
                $arr_roba_ids[] = $roba->roba_id;
            }

            $artikli = DB::table('roba')->select('roba_id','sifra_is', 'sifra_d' , 'sku', 'naziv_web')->where('flag_cenovnik','1')->whereIn('roba_id',$arr_roba_ids)->where('naziv_web','ilike','%'.trim($search).'%')->orWhere('sifra_is','ilike','%'.trim($search).'%')->orWhere('sifra_d','ilike','%'.trim($search).'%')->orWhere('sku','ilike','%'.trim($search).'%')->orWhere('roba_id',(int)trim($search))->orderBy('roba_id','asc')->paginate(100);
            $broj_artikala = DB::table('roba')->where('flag_cenovnik','1')->whereIn('roba_id',$arr_roba_ids)->where('naziv_web','ilike','%'.trim($search).'%')->orWhere('sifra_is','ilike','%'.trim($search).'%')->orWhere('sifra_d','ilike','%'.trim($search).'%')->orWhere('sku','ilike','%'.trim($search).'%')->orWhere('roba_id',(int)trim($search))->count();
        } else {
            $artikli = DB::table('roba')->select('roba_id','sifra_is', 'sifra_d' , 'sku', 'naziv_web')->where('flag_cenovnik','1')->where('naziv_web','ilike','%'.trim($search).'%')->orWhere('sifra_is','ilike','%'.trim($search).'%')->orWhere('sifra_d','ilike','%'.trim($search).'%')->orWhere('sku','ilike','%'.trim($search).'%')->orWhere('roba_id',(int)trim($search))->orderBy('roba_id','asc')->paginate(100);
            $broj_artikala = DB::table('roba')->where('flag_cenovnik','1')->where('naziv_web','ilike','%'.trim($search).'%')->orWhere('sifra_is','ilike','%'.trim($search).'%')->orWhere('sifra_d','ilike','%'.trim($search).'%')->orWhere('sku','ilike','%'.trim($search).'%')->orWhere('roba_id',(int)trim($search))->count();
        }


        $data=array(
            "strana" => 'nivo_pristupa_grupe',
            "title" => 'Nivo pristupa',
            "partneri" => $partneri,
            "partner_id" => $partner_id,
            "grupa_pr_id" => $grupa_pr_id,
            "artikli" => $artikli,
            "broj_artikala" => $broj_artikala,
            "search" => $search
        );
        return View::make('adminb2b/pages/nivo_pristupa', $data);
    }


    function lager_partneri($partner_id=null) {
        $lageri = DB::table('orgj')->where('b2b',1)->get();
        $partneri = DB::table('partner')->where('partner_id','>',0)->orderBy('naziv', 'ASC')->get();

        $data=array(
            "strana" => 'nivo_pristupa_lager',
            "title" => 'Nivo pristupa',
            "lageri" => $lageri,
            "partneri" => $partneri,
            "partner_id" => $partner_id
        );
        return View::make('adminb2b/pages/nivo_pristupa', $data);

    }
}