<?php

class AdminB2BSettingsController extends Controller {

	function index(){
        // if(!Admin_model::check_admin()){
        //     return Redirect::to(AdminB2BOptions::base_url().'admin/b2b');
        // }
		$data=array(
			"strana" => 'b2b_podesavanja',
			"title" => 'Podešavanja'
		);
		return View::make('adminb2b/pages/podesavanja', $data);
	}

    function settings(){
        $inputs = Input::all();
        if($inputs['action'] == 'magacin_enable'){
            DB::table('orgj')->where('orgj_id',$inputs['id'])->update(array('b2b'=>$inputs['aktivan']));
            if ($inputs['aktivan']) {
                AdminSupport::saveLog('B2B_MAGACIN_OMOGUCI', array($inputs['id']));
            }else{
                AdminSupport::saveLog('B2B_MAGACIN_ONEMOGUCI', array($inputs['id']));
            }
        }
        elseif($inputs['action'] == 'magacin_primary'){
            DB::table('imenik_magacin')->where('imenik_magacin_id',20)->update(array('orgj_id'=>$inputs['id']));
            AdminSupport::saveLog('B2B_MAGACIN_PRIMARNI', array($inputs['id']));

        }elseif($inputs['action'] == 'magacin_add'){
            $orgj_id = DB::table('orgj')->max('orgj_id')+1;
            $preduzece_id = 1;
            $sifra = '00'.$orgj_id;
            $level = 2;
            $tip_orgj_id = -1;
            $parent_orgj_id = 1;
            $b2b = 1;
            $naziv = $inputs['value'];

            DB::table('orgj')->insert(array(
                'orgj_id'=>$orgj_id,
                'preduzece_id'=> $preduzece_id,
                'sifra'=> $sifra,
                'level'=> $level,
                'tip_orgj_id'=> $tip_orgj_id,
                'parent_orgj_id'=> $parent_orgj_id,
                'naziv'=> $naziv,
                'b2b'=> $b2b
            ));
            AdminSupport::saveLog('B2B_MAGACIN_DODAJ', array(DB::table('orgj')->max('orgj_id')));

        }elseif($inputs['action'] == 'magacin_delete'){
            $orgj_id = $inputs['orgj_id'];
            AdminSupport::saveLog('B2B_MAGACIN_OBRISI', array($orgj_id));
            DB::table('imenik_magacin')->where('orgj_id',$orgj_id)->delete();
            DB::table('orgj')->where('orgj_id',$orgj_id)->delete();

        }elseif($inputs['action'] == 'switch_option'){
            DB::table('options')->where('options_id',$inputs['option_id'])->update(array('int_data'=>$inputs['value']));

            if ($inputs['value']) {
                AdminSupport::saveLog('B2B_OPSTA_PODESAVANJA_DA', array($inputs['option_id']));
            }else{
                AdminSupport::saveLog('B2B_OPSTA_PODESAVANJA_NE', array($inputs['option_id']));
            }

        }elseif($inputs['action'] == 'save_str_option'){
            DB::table('options')->where('options_id',$inputs['option_id'])->update(array('str_data'=>$inputs['value']));
            AdminSupport::saveLog('B2B_MAGACIN_PODESAVANJA', array($inputs['option_id']));
        }

    }

    function upload_cenovnik(){
        $file = Input::file('cenovnik');

        if(Input::hasFile('cenovnik') && $file->isValid() && $file->getClientOriginalExtension() == 'xls'){
         
            $file->move("files/","GEMBIRD_VP.xls");

            return Redirect::back()->with('message','Fajl je uspešno upload-ovan!');
        }else{
            return Redirect::back()->with('error_message','Fajl nije upload-ovan! Proverite strukturu fajla!');
        }
    }
    
}