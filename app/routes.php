<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// use Import\BCGroup;
// Route::get('/testBCGroup',function(){
//     BCGroup::executeShort(783112);
// });

// Route::get('/testDB',function(){
//     Artisan::call('update:db');
// });

// use IsLogik\DBData;
Route::get('/testiranje',function(){
    $id_iss = DB::table('roba')->WhereNotNull('id_is')->get();
    foreach ($id_iss as $id_is) {
        $roba_id = $id_is->roba_id;
        $sifra_id_is = $id_is->id_is;

        $lageri = DB::table('lager')->where('roba_id',$roba_id)->get();

        foreach ($lageri as $lager) {
            $orgj_id = $lager->orgj_id;
            $sifra=$orgj_id.'-'.$sifra_id_is;

            DB::table('lager')->where('orgj_id',$lager->orgj_id)->where('orgj_id','>',1)->WhereNull('sifra_is')->where('roba_id',$lager->roba_id)->update(array('sifra_is'=>$sifra));
        }

    }
});


Route::get('/linkovi',function(){

   // All::dd( DB::connection('logik')->select("SELECT * FROM product p WHERE (SELECT sum(amount-reservedamount+reversedamount) FROM productwarehouse pw WHERE ( warehouseid=1 OR warehouseid=2 OR warehouseid=3 OR warehouseid=4 OR warehouseid=5 OR warehouseid=7 OR warehouseid=19 ) AND pw.productid=p.productid) >0 AND b2bprice>0 AND active ='y'"));
    $products_file = "uspon_linkovi2.xls";
        
       
        

            $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
            $excelObj = $excelReader->load($products_file);
            $worksheet = $excelObj->getSheet(0);
            $lastRow = $worksheet->getHighestRow();
           

            for ($row = 2; $row <= $lastRow; $row++) {
               
                 $stari_link = $worksheet->getCell('A'.$row)->getValue();
                 $novi_link  = $worksheet->getCell('C'.$row)->getValue();

                 if(isset($stari_link))
                 {   

                $sPolja = '';
                $sVrednosti = '';

                $sPolja .= "link,";          $sVrednosti .= "'" . str_replace('https://www.uspon.rs/', '', $stari_link) . "',";
                $sPolja .= "novi_link";      $sVrednosti .= "'" . str_replace('https://www.uspon.rs/', '',$novi_link) . "'";
                
                DB::statement("INSERT INTO stari_linkovi (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
               
                 }
            }



});

Route::post('/category-ajax','ApiCategoryController@category_ajax');
Route::get('/export-xls/{kind}/{partner_slug?}', 'ExternController@export_xls');

Route::group(array('prefix'=>'api/v1','before'=>'api'),function (){
    // Route::get('/1s','ApiPageController@pages');
    Route::get('/categories','ApiCategoryController@categories');
    Route::get('/articles','ApiArticleController@articles');
    Route::get('/partners','ApiPartnerController@partners');
    Route::get('/b2b-orders','ApiOrderB2BController@b2bOrders');
    Route::get('/b2c-orders','ApiOrderB2CController@b2cOrders');

    Route::post('/categories-save','ApiCategoryController@categoriesSave');
    Route::post('/articles-save','ApiArticleController@articlesSave');
    Route::post('/partners-save','ApiPartnerController@partnersSave');
    Route::post('/partner-cards-save','ApiPartnerController@partnerCardsSave');
    Route::post('/b2b-orders-save','ApiOrderB2BController@b2bOrderSave');
    Route::post('/b2c-orders-save','ApiOrderB2CController@b2cOrderSave');

    Route::delete('/categories-delete','ApiCategoryController@categoriesDelete');
    Route::delete('/articles-delete','ApiArticleController@articlesDelete');
    Route::delete('/partners-delete','ApiPartnerController@partnersDelete');
    Route::delete('/partner-cards-delete','ApiPartnerController@partnerCardsDelete');
    Route::delete('/b2b-orders-delete','ApiOrderB2BController@b2bOrdersDelete');
    Route::delete('/b2b-order-items-delete','ApiOrderB2BController@b2bOrderItemsDelete');
    Route::delete('/b2c-orders-delete','ApiOrderB2CController@b2cOrdersDelete');
    Route::delete('/b2c-order-items-delete','ApiOrderB2CController@b2cOrderItemsDelete');
 

	Route::group(array('before'=>'parner_api'),function (){
		Route::get('/partner-export-xml','PartnerExportController@xml');
	});
});
/////////////////////////        ADMIN          ////////////////////////////////////
Route::get('/admin-login', 'Admin@admin_login');
Route::post('/admin-login-store', 'Admin@admin_login_store');
Route::get('/sso-login/{hash}/{shop?}', 'Admin@sso_login');
Route::get('/admin-logout/{confirm?}', 'Admin@logout');


//administracija za artikle (b2c admin na web-u)
Route::group(array('prefix'=>'admin','before'=>'b2c_admin'),function (){

Route::get('/podesavanje-naloga/{select_plan?}', 'Admin@account_settings');

// permisije
Route::get('/administratori/{id?}', 'AdminPermissionController@administratori');
Route::post('/administrator-edit', 'AdminPermissionController@administrator_edit');
Route::get('/administrator-delete/{id}', 'AdminPermissionController@administrator_delete');
Route::get('/grupa-modula/{id?}/{glavni_modul_id?}', 'AdminPermissionController@grupa_modula');
Route::post('/modul-grupa-edit', 'AdminPermissionController@modul_grupa_edit');
Route::get('/modul-grupa-delete/{id}', 'AdminPermissionController@modul_grupa_delete');
Route::get('/logovi/{DatumOd?}/{DatumDo?}/{korisnik_id?}/{akcija?}/{statistika?}', 'AdminPermissionController@logovi');
Route::get('/komercijalista-narudzbine/{imenik_id}', 'AdminPermissionController@komercijalista_narudzbine_export');

//stranice
Route::get('/stranice/{stranica_id}/{jezik_id?}/{page_section_id?}/{page_section_type_id?}', 'Admin@stranice');
Route::post('/pages', 'Admin@pages');
Route::post('/position', 'Admin@position');

// Route::get('/page-section/{page_section_id}/{page_section_type_id?}/{lang_id?}', 'AdminStraniceController@page_section');
Route::post('/page-section-edit', 'AdminStraniceController@page_section_edit');
Route::get('/page-section-delete/{page_section_id}', 'AdminStraniceController@page_section_delete');
Route::post('/page-section-position', 'Admin@page_section_position');
Route::post('/page-section-add', 'Admin@page_section_add');
Route::get('/page-section-remove/{page_id}/{section_id}', 'Admin@page_section_remove');


//preduzece
Route::get('/kontakt-podaci', 'AdminKontaktController@kontakt_podaci');
Route::post('/kontakt_update', 'AdminKontaktController@kontakt_update');
Route::post('/upload_logo', 'AdminKontaktController@upload_logo');
Route::post('/upload_potpis', 'AdminKontaktController@upload_potpis');
//baneri i slajderi
// Route::post('/banner_uload', 'Admin@banner_uload');
Route::post('/baneri_delete', 'Admin@baneri_delete');
Route::post('/css_edit', 'Admin@css_edit');
Route::post('/image_upload', 'Admin@image_upload');
Route::post('/image_upload_opis', 'Admin@image_upload_opis');
Route::post('/delete_image', 'Admin@delete_image');
Route::post('/delete_page', 'Admin@delete_page');


//podesavanja
Route::post('/setings_update', 'Admin@setings_update');
Route::post('/options_setings_update', 'Admin@options_setings_update');
Route::post('/options-change-str-data', 'Admin@options_change_str_data');
//email opcije
Route::post('/save_email_options', 'Admin@save_email_options');
Route::post('/save-google-analytics', 'Admin@save_google_analytics');
Route::post('/save-facebook-pixel', 'Admin@save_facebook_pixel');
Route::post('/save-loadbee', 'Admin@save_loadbee');
Route::post('/save-flixmedia', 'Admin@save_flixmedia');
Route::post('/save-pitchprint', 'Admin@save_pitchprint');
Route::post('/save-chat', 'Admin@save_chat');
Route::post('/save-color', 'Admin@save_color');
Route::post('/save-intesa-keys', 'Admin@save_intesa_keys');

Route::get('/', 'Admin@index');
Route::get('/futer/{id}/{jezik_id?}', 'AdminFooterController@footer');
Route::post('/futer-edit', 'AdminFooterController@footer_edit');
Route::get('/futer-delete/{id}', 'AdminFooterController@footer_delete');
Route::get('/futer-page-delete/{id}/{web_b2c_seo_id}', 'AdminFooterController@footer_page_delete');
Route::post('/position-footer-section', 'AdminFooterController@position_footer_section');

Route::get('/slider/{slider_id}/{slider_item_id?}/{lang_id?}', 'AdminSlajderiController@slider');
Route::post('/slider-edit', 'AdminSlajderiController@slider_edit');
Route::post('/slider-item-edit', 'AdminSlajderiController@slider_item_edit');
Route::get('/slider-delete/{slider_id}', 'AdminSlajderiController@slider_delete');
Route::get('/slider-item-delete/{slider_item_id}', 'AdminSlajderiController@slider_item_delete');
Route::post('/slider-items-position', 'AdminSlajderiController@slider_items_position');
Route::post('/bg-img-edit', 'AdminSlajderiController@bg_uload');
Route::get('/bg-img-delete', 'AdminSlajderiController@bgImgDelete');


Route::get('/podesavanja', 'Admin@podesavanja');
Route::get('/css', 'Admin@css');
Route::post('/css_save/{custom_css_id?}', 'Admin@css_save');
Route::get('/custom_css_delete/{id}', 'Admin@css_delete');
Route::get('/css_reset', 'Admin@css_reset');
Route::get('/css_submit', 'Admin@css_submit');
Route::get('/upload_images_pages', 'Admin@upload_images_pages');
Route::post('/aktivan-modul', 'Admin@aktivan_modul');


// artikli
Route::get('/artikli/{grupa_pr_id}/{proizvodjac?}/{dobavljac?}/{tip?}/{labela?}/{karakteristika?}/{magacin?}/{exporti?}/{katalog?}/{filteri?}/{flag?}/{search?}/{nabavna?}/{order?}', 'AdminArticlesController@artikli');
Route::get('/product/{id}/{clone_id?}', 'AdminArticlesController@product');
Route::post('/product-edit', 'AdminArticlesController@product_edit');
Route::get('/product-short/{id}/{clone_id?}', 'AdminArticlesController@product_short');
Route::post('/product-edit-short', 'AdminArticlesController@product_edit_short');
Route::get('/product-delete/{roba_id}/{check?}', 'AdminArticlesController@product_delete');
Route::get('/products-delete/{roba_id}/{check?}', 'AdminArticlesController@products_delete');
Route::post('/products-delete-post', 'AdminArticlesController@products_delete_post');
Route::post('/products-import', 'AdminArticlesController@products_import');
Route::post('/products-warranty-import', 'AdminArticlesController@products_warranty_import');

//slike
Route::get('/product_slike/{id}/{web_slika_id?}', 'AdminArticlesController@product_slike');
Route::post('/slika-edit', 'AdminArticlesController@slika_edit');
Route::post('/slika-delete', 'AdminArticlesController@slika_delete');
Route::post('/slike-more-delete', 'AdminArticlesController@slike_more_delete');
Route::get('/slika-delete/{id}/{web_slika_id}/{short?}', 'AdminArticlesController@slika_delete_get');

//opis
Route::get('/product_opis/{id}', 'AdminArticlesController@product_opis');
Route::post('/opis_edit', 'AdminArticlesController@opis_edit');

//seo
Route::get('/product_seo/{id}/{jezik_id?}', 'AdminArticlesController@product_seo');
Route::post('/seo_edit', 'AdminArticlesController@seo_edit');

// karakteristike
Route::get('/product_karakteristike/{id}', 'AdminArticlesController@product_karakteristike');
Route::post('/karakteristike_edit', 'AdminArticlesController@karakteristike_edit');
Route::get('/product_generisane/{id}', 'AdminArticlesController@product_generisane');
Route::get('/dobavljac_karakteristike/{id}', 'AdminArticlesController@product_od_dobavljaca');
Route::get('/vezani_artikli/{id}/{vezani?}', 'AdminArticlesController@vezani_artikli');
Route::get('/srodni_artikli/{id}/{srodni?}', 'AdminArticlesController@srodni_artikli');
Route::post('/grupa-karakteristike', 'AdminArticlesController@grupa_karakteristike');
Route::post('/position-naziv', 'AdminArticlesController@position_GrupeNaziv');

// Product Osobine
Route::get('/product-osobine/{roba_id}/{osobina_kombinacija_id?}', 'AdminOsobineController@product_osobine');
Route::post('/product-osobine/{roba_id}/{osobina_kombinacija_id}/save', 'AdminOsobineController@product_osobine_save');
Route::get('/product-osobine/{roba_id}/{osobina_kombinacija_id}/delete', 'AdminOsobineController@product_osobine_delete');
Route::post('/product-osobine-vrednost/{roba_id}/{osobina_kombinacija_id}/save', 'AdminOsobineController@product_osobine_vrednost_save');
Route::get('/product-osobine-vrednost/{roba_id}/{osobina_kombinacija_id}/{osobina_vrednost_id}/delete', 'AdminOsobineController@product_osobine_vrednost_delete');
Route::post('product-osobine-naziv-vrednosti', 'AdminOsobineController@product_osobine_naziv_vrednosti');

// Route::get('/product_osobine/{id}/{osobina_naziv_id}', 'AdminOsobineController@product_osobine_index');
// Route::post('/product_osobine/{id}', 'AdminOsobineController@product_osobine_add');
// Route::get('/product_osobine/{id}/{osobina_naziv_id}/all', 'AdminOsobineController@product_osobine_add_all');
// Route::post('/product_osobine/{id}/{osobina_naziv_id}', 'AdminOsobineController@product_osobine_edit');
// Route::get('/product_osobine/{roba_id}/{osobina_naziv_id}/{osobina_vrednost_id}/deletegroup', 'AdminOsobineController@product_osobine_grupa_delete');
// Route::get('/product_osobine/{roba_id}/{osobina_naziv_id}/{osobina_vrednost_id}/delete', 'AdminOsobineController@product_osobine_delete');
// Route::get('/product_osobine/{roba_id}/{osobina_naziv_id}/{osobina_vrednost_id}/deleteall', 'AdminOsobineController@product_osobine_delete_all');
Route::post('/position-osobine', 'AdminOsobineController@position_osobine');

// akcija
Route::get('/product_akcija/{id}', 'AdminArticlesController@product_akcija');
Route::get('/product_akcija_b2b/{id}', 'AdminArticlesController@product_akcija_b2b');
Route::post('/akcija_edit', 'AdminArticlesController@akcija_edit');
Route::post('/b2b_akcija_edit', 'AdminArticlesController@b2b_akcija_edit');

// dodatni fajlovi
Route::get('/dodatni_fajlovi/{id}/{extension_id?}', 'AdminArticlesController@dodatni_fajlovi');
Route::post('/dodatni_fajlovi_upload', 'AdminArticlesController@dodatni_fajlovi_upload');
Route::get('/dodatni_fajlovi_delete/{roba_id}/{web_file_id}', 'AdminArticlesController@dodatni_fajlovi_delete');

// ajax upload slika
Route::post('/upload-image-add', 'AdminGalleryController@uploadImageAdd');
Route::post('/energy-image-add', 'AdminGalleryController@energyImageAdd');
Route::post('/energy-class-remove', 'AdminGalleryController@energyRemove');
Route::get('/upload-image-delete/{image}', 'AdminGalleryController@uploadImageDelete');
Route::post('/ajax/gallery-content', 'AdminGalleryController@galleryContent');

// upload fajlova
Route::post('/upload-document-add', 'AdminFilesController@uploadDocumentAdd');
Route::get('/upload-document-delete/{file}', 'AdminFilesController@uploadDocumentDelete');
Route::post('/ajax/files-content', 'AdminFilesController@filesContent');


// AJAX
Route::post('/ajax/articles', 'AdminArticlesController@ajaxArticles');


// EXPORT XLS
Route::get('/export_all_xls', 'AdminArticlesController@export_all_xls');
Route::post('/export_xls', 'AdminArticlesController@export_xls');

// Vesti
Route::get('/vesti/{active?}', 'AdminVestiController@vesti');
Route::get('/vest/{id}/{jezik_id?}', 'AdminVestiController@vest');
Route::post('/vesti/{id}/save', 'AdminVestiController@saveNews');
Route::get('/vesti/{id}/delete', 'AdminVestiController@deleteNews');
Route::get('/vesti/f/{id}', 'AdminVestiController@filter');



// grupe
Route::get('/grupe/{id}/{grupa_pr_naziv_id?}/{jezik_id?}', 'AdminGroupsController@grupe');
Route::post('/grupa-edit', 'AdminGroupsController@grupa_edit');
Route::get('/grupa-delete/{grupa_pr_id}', 'AdminGroupsController@grupa_delete');
Route::post('/position-value', 'AdminGroupsController@position_value');
Route::post('/position-char', 'AdminGroupsController@position_char');



// ankete  
Route::get('/ankete/{anketa_id}/{anketa_pitanje_id?}/{anketa_odgovor_id?}', 'AdminAnketeController@ankete');
Route::post('/anketa-edit/{anketa_id?}', 'AdminAnketeController@anketa_edit');
Route::get('/anketa-delete/{anketa_id}', 'AdminAnketeController@anketa_delete');
Route::post('/ankete/pitanje-save/{anketa_pitanje_id?}', 'AdminAnketeController@anketa_pitanje');
Route::post('/ankete/pitanje-edit/{anketa_pitanje_id?}', 'AdminAnketeController@anketa_pitanje_edit');
Route::get('/pitanje-delete/{anketa_pitanje_id}', 'AdminAnketeController@pitanje_delete');
Route::post('/ankete/odgovor-save/{anketa_pitanje_id?}/{anketa_odgovor_id?}', 'AdminAnketeController@anketa_odgovor');
Route::post('/ankete/odgovor-edit/{anketa_pitanje_id?}/{anketa_odgovor_id?}', 'AdminAnketeController@anketa_odgovor_edit');
Route::get('/odgovor-delete/{anketa_pitanje_id}', 'AdminAnketeController@odgovor_delete');
Route::post('/poll-question-position', 'AdminAnketeController@poll_question_position');
Route::post('/poll-answer-position', 'AdminAnketeController@poll_answer_position');




// proizvodjac
Route::get('/proizvodjac/{proizvodjac_id}/{jezik_id?}', 'AdminSupportController@proizvodjac');
Route::post('/proizvodjac-edit', 'AdminSupportController@proizvodjac_edit');
Route::get('/proizvodjac-delete/{proizvodjac_id}', 'AdminSupportController@proizvodjac_delete');

// mesta (izbrisano je, ne postoji vise, nzm sto je jos u kodu)
Route::get('/mesto/{mesto_id?}', 'AdminSupportController@mesto');
Route::post('/mesto-edit', 'AdminSupportController@mesto_edit');
Route::get('/mesto-delete/{mesto_id}', 'AdminSupportController@mesto_delete');

// tipovi
Route::get('/tip/{tip_artikla_id}', 'AdminSupportController@tip_artikla');
Route::post('/tip-edit', 'AdminSupportController@tip_artikla_edit');
Route::get('/tip-delete/{tip_artikla_id}', 'AdminSupportController@tip_artikla_delete');

// kupovina_na_rate
Route::get('/kupovina_na_rate/{rata_id}', 'AdminSupportController@rata');
Route::post('/rata_edit', 'AdminSupportController@rata_edit');
Route::get('/rata_delete/{rata_id}', 'AdminSupportController@rata_delete');

// labele
Route::get('/labela/{labela_artikla_id}', 'AdminSupportController@labela_artikla');
Route::post('/labela-edit', 'AdminSupportController@labela_artikla_edit');
Route::get('/labela-delete/{labela_artikla_id}', 'AdminSupportController@labela_artikla_delete');

// jedinice mere
Route::get('/jedinica-mere/{jedinica_mere_id}', 'AdminSupportController@jedinica_mere');
Route::post('/jedinica-mere-edit', 'AdminSupportController@jedinica_mere_edit');
Route::get('/jedinica-mere-delete/{jedinica_mere_id}', 'AdminSupportController@jedinica_mere_delete');

// poreske stope
Route::get('/poreske-stope/{tarifna_grupa_id?}', 'AdminSupportController@poreske_stope');
Route::post('/poreske-stope-edit', 'AdminSupportController@poreske_stope_edit');
Route::get('/poreske-stope-delete/{tarifna_grupa_id}', 'AdminSupportController@poreske_stope_delete');


// stanje-artikla
Route::get('/stanje-artikla/{roba_flag_cene_id?}', 'AdminSupportController@stanje_artikla');
Route::post('/stanje-artikla-edit', 'AdminSupportController@stanje_artikla_edit');
Route::get('/stanje-artikla-delete/{roba_flag_cene_id}', 'AdminSupportController@stanje_artikla_delete');

// status-narudzbine
Route::get('/status_narudzbine/{narudzbina_status_id?}', 'AdminSupportController@status_narudzbine');
Route::post('/status_narudzbine_edit', 'AdminSupportController@status_narudzbine_edit');
Route::get('/status_narudzbine_delete/{narudzbina_status_id}', 'AdminSupportController@status_narudzbine_delete');

// kurirske-sluzbe
Route::get('/kurirska_sluzba/{narudzbina_status_id?}', 'AdminSupportController@kurirska_sluzba');
Route::post('/kurirska_sluzba_edit', 'AdminSupportController@kurirska_sluzba_edit');
Route::get('/kurirska_sluzba_delete/{narudzbina_status_id}', 'AdminSupportController@kurirska_sluzba_delete');



// konfigurator
Route::get('/konfigurator/{konfigurator_id?}', 'AdminSupportController@konfigurator');
Route::post('/konfigurator-edit', 'AdminSupportController@konfigurator_edit');
Route::get('/konfigurator-delete/{konfigurator_id}', 'AdminSupportController@konfigurator_delete');

// Komentari
Route::get('/komentari', 'AdminSupportController@komentari');
Route::get('/komentari/{solution}/{id}', 'AdminSupportController@komentar');
Route::post('/komentari/{solution}/{id}', 'AdminSupportController@updateComment');
Route::get('/komentari/{solution}/{id}/delete', 'AdminSupportController@deleteComment');

// Osobine
Route::get('/osobine/{id?}', 'AdminOsobineController@index');
Route::post('/osobine/{id?}', 'AdminOsobineController@editProperty');
Route::post('/osobine/{id}/{osobina_vrednost_id}', 'AdminOsobineController@editPropertyValue');
Route::get('/osobine/{id}/delete', 'AdminOsobineController@deleteProperty');
Route::get('/osobine/{id}/{vrednost_id}/delete', 'AdminOsobineController@deletePropertyValue');

//vauceri
Route::get('/vauceri', 'AdminVauceriController@index');
Route::post('/vauceri-save', 'AdminVauceriController@save');
Route::get('/vauceri-delete/{id}', 'AdminVauceriController@delete');
Route::post('/vauceri-import', 'AdminVauceriController@vauceri_import');

Route::post('/vauceri-podesavanja-save', 'AdminVauceriController@vauceri_save');
Route::get('/vauceri-podesavanja-delete/{id}', 'AdminVauceriController@vauceri_delete');


// Način plaćanja
Route::get('/nacin_placanja/{id}', 'AdminNacinPlacanjaController@index');
Route::post('/nacin_placanja/{id}', 'AdminNacinPlacanjaController@edit');
Route::post('/nacin_placanja/{id}/delete', 'AdminNacinPlacanjaController@delete');

// Način isporuke
Route::get('/nacin_isporuke/{id}', 'AdminNacinIsporukeController@index');
Route::post('/nacin_isporuke/{id}', 'AdminNacinIsporukeController@edit');
Route::post('/nacin_isporuke/{id}/delete', 'AdminNacinIsporukeController@delete');

// Troskovi isporuke
Route::get('/troskovi-isporuke', 'AdminTroskoviIsporukeController@index');
Route::post('/troskovi-isporuke-save', 'AdminTroskoviIsporukeController@save');
Route::get('/troskovi-isporuke-delete/{id}', 'AdminTroskoviIsporukeController@delete');

Route::get('/cena-isporuke', 'AdminTroskoviIsporukeController@cena');
Route::post('/cena-isporuke-save', 'AdminTroskoviIsporukeController@cena_save');
Route::get('/cena-isporuke-delete/{id}', 'AdminTroskoviIsporukeController@cena_delete');

// Definisane marze
Route::get('/definisane-marze', 'AdminDefinisaneMarzeController@index');
Route::post('/definisane-marze-save', 'AdminDefinisaneMarzeController@save');
Route::get('/definisane-marze-delete/{id}', 'AdminDefinisaneMarzeController@delete');

// Bodovi
Route::get('/bodovi', 'AdminBodoviController@index');
Route::post('/bodovi-popust-save', 'AdminBodoviController@save');
Route::get('/bodovi-popust-delete/{id}', 'AdminBodoviController@delete');

//Bodovi podesavanja
Route::post('/bodovi-podesavanja-save', 'AdminBodoviController@b_save');
Route::post('/bodovi-ostvareni-save', 'AdminBodoviController@o_save');
Route::get('/bodovi-ostvareni-delete/{id}', 'AdminBodoviController@o_delete');
// Unos kursa
Route::get('/kurs/{id}', 'AdminKursController@index');
Route::post('/kurs/edit', 'AdminKursController@edit');
Route::get('/kurs/{id}/nbs', 'AdminKursController@kurs_nbs');

// Garancije
Route::get('/garancije/{search}/{datum_od}/{datum_do}/{sort_column}/{sort_direction}', 'AdminGarancijaController@garancije');
Route::get('/garancija/{produzene_garancije_id}', 'AdminGarancijaController@garancija');
Route::post('/garancija/save', 'AdminGarancijaController@garancija_save');
Route::get('/garancija-delete/{produzene_garancije_id}', 'AdminGarancijaController@garancija_delete');
Route::get('/produzena-garancija-pdf/{id}', 'AdminGarancijaController@produzena_garancija_pdf');

// Aploadovanje fajlova
Route::get('/file-upload', 'AdminFilesController@index');
Route::post('/file-upload-save', 'AdminFilesController@save');

// Podaci za import
Route::get('/import_podaci_grupa/{partner_id}/{partner_grupa_id}/{search}','AdminImportController@import_podaci_grupa');
Route::post('/import_podaci_save','AdminImportController@import_podaci_save');
Route::get('import_podaci_delete/{partner_id}/{partner_grupa_id}/{search}','AdminImportController@import_podaci_delete');
Route::get('/import-podaci-proizvodjac/{partner_id}/{partner_proizvodjac_id}', 'AdminImportController@import_podaci_proizvodjac');
Route::post('/import-podaci-proizvodjac-save', 'AdminImportController@import_podaci_proizvodjac_save');
Route::get('import-podaci-proizvodjac-delete/{partner_id}/{partner_proizvodjac_id}', 'AdminImportController@import_podaci_proizvodjac_delete');

//narudzbine
Route::get('/porudzbine/{status}/{narudzbina_status_id}/{datum_od}/{datum_do}/{search}/{pickup_id}', 'AdminNarudzbineController@porudzbine');
Route::get('/porudzbina/delete/{web_b2c_narudzbina_id}', 'AdminNarudzbineController@porudzbinaDelete');
Route::post('/porudzbina/delete-more', 'AdminNarudzbineController@porudzbinaDeleteMore');
Route::get('/narudzbina/{web_b2c_narudzbina_id}/{roba_id?}', 'AdminNarudzbineController@narudzbina');
Route::post('/narudzbina', 'AdminNarudzbineController@updateNarudzbinu');
Route::get('/narudzbina-stavka/{narudzbina_stavka_id}', 'AdminNarudzbineController@narudzbina_stavka');
Route::post('/narudzbina-stavka', 'AdminNarudzbineController@narudzbina_stavka_update');
Route::get('/narudzbina-stavka-city/{web_b2c_narudzbina_id}/{id}', 'AdminNarudzbineController@narudzbina_stavka_city');
Route::get('/dodaj-novu-city/{web_b2c_narudzbina_id}/{id}', 'AdminNarudzbineController@dodaj_novu_city');
Route::get('/obrisi-city/{web_b2c_narudzbina_id}/{id}', 'AdminNarudzbineController@obrisi_city');
Route::get('/obrisi-stavku/{stavka_id}', 'AdminNarudzbineController@obrisi_stavku');
Route::post('/narudzbina-stavka-city-update', 'AdminNarudzbineController@narudzbina_stavka_city_update');
Route::get('/posta-posalji/{web_b2c_narudzbina_id}/{posta_slanje_id}', 'AdminNarudzbineController@posta_posalji');
Route::get('/posta-resetuj/{web_b2c_narudzbina_id}/{posta_slanje_id}', 'AdminNarudzbineController@posta_resetuj');
Route::get('/posta-posalji-city/{web_b2c_narudzbina_id}/{posta_zahtev_api_id}/{posta_slanje_id}', 'AdminNarudzbineController@posta_posalji_city');
Route::get('/posta-otkazi-city/{web_b2c_narudzbina_id}/{posta_zahtev_api_id}', 'AdminNarudzbineController@posta_cancel_city');
Route::get('/posta-nalepnice/{web_b2c_narudzbina_id}/{posta_zahtev_api_id}', 'AdminNarudzbineController@posta_nalepnice');
Route::get('/posta-dokument/{web_b2c_narudzbina_id}/{posta_zahtev_api_id}', 'AdminNarudzbineController@posta_dokument');
Route::get('/priprema-za-slanje/{web_b2c_narudzbina_id}/{posta_zahtev_api_id}', 'AdminNarudzbineController@priprema');
Route::get('/priprema-za-slanje-posalji', 'AdminNarudzbineController@priprema_posalji');
Route::get('/city-pdf/{pickup_id}', 'AdminNarudzbineController@city_pdf');

Route::post('/ajax/partner-data','AdminNarudzbineController@partner_data');
Route::post('/ajax/orders','AdminNarudzbineController@orders');

Route::get('/pdf/{web_b2c_narudzbina_ids}', 'AdminNarudzbineController@pdf');
Route::get('/pdf_racun/{web_b2c_narudzbina_ids}', 'AdminNarudzbineController@pdf_racun');
Route::get('/pdf_predracun/{web_b2c_narudzbina_ids}', 'AdminNarudzbineController@pdf_predracun');
Route::get('/pdf_ponuda/{web_b2c_narudzbina_ids}', 'AdminNarudzbineController@pdf_ponuda');


// analitika
Route::get('/analitika','AdminAnalysisController@analitikaGrupa');
// Route::get('/analitika', 'AdminAnalysisController@analitika');
// Route::get('/analitika/{datum_od?}/{datum_do?}/{id?}', 'AdminAnalysisController@analitika');

// Kupci i partneri
Route::get('/kupci_partneri', 'AdminKupciPartneriController@getIndex');

Route::get('/kupci_partneri/kupci/{id}', 'AdminKupciPartneriController@getKupac');
Route::get('/kupci_partneri/kupci/{id}/{confirm}/delete', 'AdminKupciPartneriController@deleteKupac');
Route::get('/kupci_partneri/kupci/{id}/snimi_kao_partner', 'AdminKupciPartneriController@snimiKaoPartner');
Route::post('/kupci_partneri/kupci/{id}/save', 'AdminKupciPartneriController@saveKupac');
Route::get('/kupci_partneri/kupci/{vrsta_kupca?}/{status_registracija?}/{search?}', 'AdminKupciPartneriController@getKupci');
Route::post('/kupci_partneri/kupci/{vrsta_kupca?}/{status_registracija?}', 'AdminKupciPartneriController@getKupciSearch');

Route::get('/kupci_partneri/partneri', 'AdminKupciPartneriController@getPartneri');
Route::post('/kupci_partneri/partneri-filter', 'AdminKupciPartneriController@partneriFilter');
Route::get('/kupci_partneri/partner/{id?}', 'AdminKupciPartneriController@getPartner');
Route::get('/kupci_partneri/partneri/{id}/{confirm}/delete', 'AdminKupciPartneriController@deletePartner');
Route::post('/kupci_partneri/partneri/{id}/save', 'AdminKupciPartneriController@savePartner');
Route::get('/kupci_partneri/export_kupac/{vrsta_fajla}/{vrsta_kupca}/{status_registracije}/{imena_kolona?}/{search?}', 'AdminKupciPartneriController@exportKupac');
Route::post('/partner-import', 'AdminKupciPartneriController@partner_import');
Route::get('/kupci_partneri/export_partner/{vrsta_fajla}/{imena_kolona?}/{search?}', 'AdminKupciPartneriController@exportMailsPartner');
Route::get('/kupci_partneri/export_newslette', 'AdminKupciPartneriController@export_newSletter');
Route::get('/exportpartner', 'AdminKupciPartneriController@exportPartner');

Route::post('/partner/card-refresh/{partner_id}', 'AdminKupciPartneriController@card_refresh');

Route::get('/partner/{partner_id}/korisnici','AdminKupciPartneriController@userKorisnici');
Route::get('/partner/{partner_id}/korisnik/{partner_korisnik_id}','AdminKupciPartneriController@userKorisnik');
Route::post('/partner/user_edit','AdminKupciPartneriController@userKorisnikEdit');
Route::get('/partner/{partner_id}/korisnik/{partner_korisnik_id}/delete','AdminKupciPartneriController@userKorisnikObrisi');
Route::get('/commercialist-b2b-login/{partner_id}','AdminKupciPartneriController@commercialist_b2b_login');

// katalog
Route::get('/katalog/{katalog_id}', 'AdminKatalogController@katalog');
Route::post('/katalog-edit', 'AdminKatalogController@katalog_edit');
Route::get('/katalog-delete/{katalog_id}', 'AdminKatalogController@katalog_delete');
Route::post('/katalog-polje-dodaj', 'AdminKatalogController@katalog_polje_dodaj');
Route::get('katalog-polje/{katalog_polja_id}/obrisi', 'AdminKatalogController@katalog_polje_obrisi');
Route::post('/katalog-polja-pozicija', 'AdminKatalogController@katalog_polja_pozicija');

Route::get('/update_db/{old?}', 'AdminDBUpdateVersion@update');
Route::get('/update_old_db', 'AdminDBUpdateVersion@update_old');
Route::get('/resolve', 'AdminDBUpdateVersion@resolve_encoding');

// admin import
Route::get('/web_import/{grupa_pr_id?}/{proizvodjac?}/{dobavljac?}/{filteri?}/{search?}/{order?}/{nabavna?}', 'AdminImportController@web_import');
Route::post('/ajax/dc_articles', 'AdminAjaxController@ajaxDcArticles');
Route::post('/upload_web_import', 'AdminImportController@upload_web_import');
Route::post('/web_import_ajax', 'AdminImportFileController@web_import_ajax');


Route::get('/export-external/{export_sifra}/{kind}','AdminExportController@export_external');

Route::get('/rma', 'Admin@rma');
Route::get('/dokumenti', 'Admin@dokumenti');

//ajax
Route::post('/ajax/groups', 'AdminGroupsController@ajaxGroups');
Route::post('/ajax/generisane', 'AdminAjaxController@ajaxGenerisane');
Route::post('/ajax/od_dobavljaca', 'AdminAjaxController@ajaxOdDobavljaca');
Route::post('/ajax/povezani', 'AdminAjaxController@ajaxPovezani');
Route::post('/ajax/articles_search', 'AdminAjaxController@articles_search');
Route::post('/ajax/srodni_search', 'AdminAjaxController@srodni_search');
Route::post('/ajax/narudzbine_search', 'AdminNarudzbineController@narudzbineSearch');
Route::post('/ajax/delete_stavka', 'AdminNarudzbineController@deleteStavka');
Route::post('/ajax/update_kolicina', 'AdminNarudzbineController@updateKolicina');
Route::post('/ajax/editCena', 'AdminNarudzbineController@editCena');
Route::post('/ajax/editTrosak', 'AdminNarudzbineController@editTrosak');

Route::post('/ajax/promena_statusa_narudzbine', 'AdminNarudzbineController@promena_statusa_narudzbine');
Route::post('/ajax/porudzbina_vise', 'AdminNarudzbineController@porudzbina_vise');
Route::post('/ajax/prihvati', 'AdminNarudzbineController@prihvati');
Route::post('/ajax/realizuj', 'AdminNarudzbineController@realizuj');
Route::post('/ajax/storniraj', 'AdminNarudzbineController@storniraj');
Route::post('/ajax/nestorniraj', 'AdminNarudzbineController@nestorniraj');
Route::post('/ajax/mail_send', 'AdminNarudzbineController@mail_send');
Route::post('/ajax/obrisi', 'AdminNarudzbineController@obrisi');
Route::post('/ajax/narudzbina-kupac', 'AdminNarudzbineController@narudzbina_kupac');

Route::post('/ajax/vrsta-partnera', 'AdminAjaxController@vrstaPartnera');

Route::post('/ajax/permissions', 'AdminAjaxController@permissions');
Route::post('/ajax/permissionsAll', 'AdminAjaxController@permissionsAll');

Route::post('/ajax/support', 'AdminAjaxController@support');
Route::post('/ajax/grupa-karakteristike-nazivi', 'AdminKatalogController@grupa_karakteristike_nazivi');
Route::post('/ajax/grupa-karakteristika-delete', 'AdminKatalogController@grupa_karakteristika_delete');

Route::get('/ajax/import/groups', 'AdminAjaxController@selectGroups');
Route::get('/ajax/import/brands', 'AdminAjaxController@renderBrands');
Route::get('/ajax/import/brands_mul', 'AdminAjaxController@renderBrandsMultiple');
Route::get('/ajax/import/tarifna', 'AdminAjaxController@renderTarifna');



//global ajax
Route::post('/ajax', 'AdminAjaxController@ajax');

Route::get('/sitemap-generate', 'AdminSeoController@sitemap_generate');

//ADMIN FRONT
Route::post('/front-admin-save', 'AdminFrontController@front_admin_save');
Route::post('/front-admin-product-list', 'AdminFrontProductsController@product_list');
Route::post('/front-admin-product-list-execute', 'AdminFrontProductsController@product_list_execute');
Route::post('/front-admin-product', 'AdminFrontProductsController@product');

});
Route::any('/posta-odgovor', 'AdminNarudzbineController@posta_odgovor');



//////  ==============    CRM    ========================== ///////
Route::group(array('prefix'=>'crm','before'=>'crm'),function (){

    Route::get('/crm_home/{crm_id?}', 'AdminCRMController@index');
    Route::get('/crm_home/{crm_id}/crm_delete', 'AdminCRMController@crm_delete');
    Route::post('/crm_lead/{crm_id}/lead_save', 'AdminCRMController@leadSave');
    Route::get('/crm_zavrseno/{crm_id}', 'AdminCRMController@zavrseno');
    Route::get('/crm_vrati/{crm_id}', 'AdminCRMController@vrati');
    Route::post('/crm_home/{crm_id}/edit', 'AdminCRMController@leadEdit');
    Route::get('/crm_home/{partner_kontakt_id}/delete', 'AdminCRMController@partner_kontakt_delete');
    Route::get('/crm_home/{crm_id}/preracunaj', 'AdminCRMController@preracunaj');
    Route::get('/crm_home/{crm_id}/dinari', 'AdminCRMController@dinari');
    //status
    Route::get('/crm_status/{crm_status_id?}','CRMSifrarniciController@crm_status');
    Route::post('/crm_status/{crm_status_id?}','CRMSifrarniciController@status_edit');
    Route::get('/crm_status/{crm_status_id}/status_delete','CRMSifrarniciController@status_delete');
    //tip
    Route::get('/crm_tip/{crm_tip_id?}','CRMSifrarniciController@crm_tip');
    Route::post('/crm_tip/{crm_tip_id?}','CRMSifrarniciController@tip_edit');
    Route::get('/crm_tip/{crm_tip_id}/tip_delete','CRMSifrarniciController@tip_delete');
    //crm sifrarnici
    Route::get('/crm_sifrarnik/{crm_akcija_tip_id?}','CRMSifrarniciController@crm_sifrarnik');
    Route::post('/crm_sifrarnik/{crm_akcija_tip_id?}','CRMSifrarniciController@sifrarnik_edit');
    Route::get('/crm_sifrarnik/crm_sifrarnik-delete/{crm_akcija_tip_id}','CRMSifrarniciController@akcija_tip_delete');
    //kontakt
    Route::get('/crm_kontakt_vrsta/{vrsta_kontakta_id?}','CRMSifrarniciController@vrsta_kontakta');
    Route::post('/crm_kontakt_vrsta/{vrsta_kontakta_id?}','CRMSifrarniciController@vrsta_kontakta_edit');
    Route::get('/crm_kontakt_vrsta/{vrsta_kontakta_id?}/vrsta_delete','CRMSifrarniciController@vrsta_delete');
    Route::post('/crm_dodatni_kontakti/{vrsta_kontakta_id?}','CRMSifrarniciController@dodatni_kontakti');
    //akcije crm
    Route::get('/crm_home/crm_akcija/{crm_id}', 'CRMAkcijeController@akcijaNew');
    Route::post('/crm_home/akcija_create/{crm_id}', 'CRMAkcijeController@akcijaCreate');
    Route::post('/crm_home/crm_akcija/akcija_edit/{crm_akcija_id}', 'CRMAkcijeController@akcijaEdit');
    Route::get('/crm_home/{crm_akcija_id}/akcija_delete', 'CRMAkcijeController@akcijaDelete');
    Route::get('/crm_akcija_vrati/{crm_akcija_id}', 'CRMAkcijeController@akcija_vrati');
    Route::get('/crm_akcija_zavrsena/{crm_akcija_id}', 'CRMAkcijeController@akcija_zavrsena');
    //crm fakturisanje
    Route::get('/crm_fakturisi/{partner_id}', 'CRMFaktureController@crm_fakturisi');
    Route::get('/crm_fakture', 'CRMFaktureController@fakturisanje');
    Route::post('/crm_fakture_save/{crm_fakture_id?}', 'CRMFaktureController@fakturisanje_save');
    Route::get('/crm_fakture_delete/{crm_fakture_id?}', 'CRMFaktureController@fakturisanje_delete');
    Route::get('/crm_fakture/{crm_fakture_id?}/preracunaj', 'CRMFaktureController@preracunaj_faktura');
    Route::get('/crm_fakture/{crm_fakture_id?}/dinari', 'CRMFaktureController@dinari_faktura');
    Route::post('/crm_fakture_dokumenta/{partner_id?}', 'CRMFaktureController@crm_fakture_dokumenta');
    Route::get('/faktura_poslata/{crm_fakture_id?}', 'CRMFaktureController@faktura_poslata');
    Route::get('/faktura_nije_poslata/{crm_fakture_id?}', 'CRMFaktureController@faktura_nije_poslata');
    Route::get('/faktura_cekiraj', 'CRMFaktureController@faktura_cekiraj');
    //crm partneri
    Route::get('/crm_partneri/{partner_id?}', 'CRMPartneriController@crm_partneri');
    Route::post('/crm_partneri_save/{partner_id?}', 'CRMPartneriController@crm_partneri_edit');
    Route::get('/crm_partneri_dodaj/{partner_id?}', 'CRMPartneriController@dodaj_u_crm');
    Route::post('/crm_partneri/partner_save', 'CRMPartneriController@partner_save');
    Route::get('/crm_partneri/{partner_id}/partner_obrisi', 'CRMPartneriController@ukloni_partnera');
     

    Route::post('/partner_details/{vrsta_kontakta_id?}','AdminCRMController@partnerDetails');
    Route::post('/crm_ispis/{crm_id?}','AdminCRMController@ispis');

    // crm partner
    Route::post('/crm_partner', 'AdminCRMController@crm_partner');

    //crm dokumenta
    Route::post('/partner_document/{crm_id?}','AdminCRMController@crmDocuments');
    Route::post('/partner_document_save', 'AdminCRMController@dokumenta_crm_save');
    Route::get('/partner_document_delete/{crm_dokumenta_id}/delete', 'AdminCRMController@dokumenta_crm_delete');
    Route::post('/ajax/search_articles', 'AdminCRMController@artikliCRMsearch');
    //crm analitika
    Route::get('/crm_analitika/{datum_od?}/{datum_do?}', 'AdminCRMController@analitika');
    //crm taskovi
    Route::get('/crm_taskovi', 'CRMTaskoviController@crm_taskovi');

     //crm logovi
    Route::get('/crm_logovi', 'AdminCRMController@crm_logovi');
     //crm logovi
    Route::get('/crm_sertifikati/{id?}', 'AdminCRMController@crm_sertifikati');
    Route::post('/crm_sertifikati_save/{crm_sertifikati_id}', 'AdminCRMController@crm_sertifikati_save');
    Route::get('/crm_sertifikati_delete/{crm_sertifikati_id}', 'AdminCRMController@crm_sertifikati_delete');
    
});



/////////////////////////////          ADMIN B2B          ///////////////////////////////////////////////

Route::group(array('prefix'=>'admin/b2b','before'=>'b2b_admin'),function (){
    Route::get('/', 'AdminB2BController@index');
    Route::get('/login-to-portal', 'AdminB2BController@login_to_portal');
    
    Route::get('/narudzbine/{status}/{narudzbina_status_id}/{datum_od}/{datum_do}/{search}/', 'AdminB2BNarudzbinaController@narudzbine');
 	Route::get('/narudzbina/{web_b2b_narudzbina_id}/{roba_id?}', 'AdminB2BNarudzbinaController@narudzbina');
 	Route::post('/narudzbina', 'AdminB2BNarudzbinaController@updateNarudzbinu');

    Route::post('/ajax/b2b_porudzbina_vise', 'AdminB2BNarudzbinaController@porudzbina_vise');
    Route::post('/ajax/b2b_promena_statusa_narudzbine', 'AdminB2BNarudzbinaController@b2b_promena_statusa_narudzbine');

    Route::post('/ajax/narudzbina-kupac', 'AdminB2BNarudzbinaController@narudzbina_kupac');

	Route::get('/analitika/partneri','AdminB2BAnalitikaController@analitikaPartneri');
	Route::get('/analitika/partner-logovi','AdminB2BAnalitikaController@analitikaPartnerLogovi');
    Route::get('/analitika', 'AdminB2BAnalitikaController@analitika');
    Route::get('/analitika/prihod/{odd?}/{dod?}', 'AdminB2BAnalitikaController@analitika');
	Route::get('/analitika/{datum_od?}/{datum_do?}/{id?}', 'AdminB2BAnalitikaController@analitika');

    Route::get('/article-list/{grupa_pr_id}/{proizvodjac?}/{dobavljac?}/{magacin?}/{filteri?}/{search?}/{nabavna?}/{order?}', 'AdminB2BArticlesController@article_list');

    Route::get('/proizvodjaci/{partner_id?}', 'AdminB2BPartnerController@proizvodjaci');
    Route::get('/partneri', 'AdminB2BPartnerController@partneri');

    Route::get('/access-list-manufacturer/{partner_id?}', 'AdminB2BPartnerController@proizvodjaci_partneri');
    Route::get('/access-list-group-article/{partner_id?}/{grupa_pr_id?}', 'AdminB2BPartnerController@grupe_partneri');
    Route::get('/access-list-warehouse/{partner_id?}', 'AdminB2BPartnerController@lager_partneri');

	Route::get('/kupci_partneri/partneri/{search?}', 'AdminKupciPartneriController@getPartneri');
	Route::post('/kupci_partneri/partneri/', 'AdminKupciPartneriController@partneriSearch');
	Route::get('/kupci_partneri/partner/{id?}', 'AdminKupciPartneriController@getPartner');
	Route::get('/kupci_partneri/partneri/{id}/{confirm}/delete', 'AdminKupciPartneriController@deletePartner');
	Route::post('/kupci_partneri/partneri/{id}/save', 'AdminKupciPartneriController@savePartner');
	Route::get('/kupci_partneri/export_kupac/{vrsta_fajla}/{vrsta_kupca}/{status_registracije}/{imena_kolona?}/{search?}', 'AdminKupciPartneriController@exportKupac');
	Route::post('/partner-import', 'AdminKupciPartneriController@partner_import');

	Route::get('/kupci_partneri/export_newslette', 'AdminKupciPartneriController@export_newSletter');

	Route::get('/exportpartner', 'AdminKupciPartneriController@exportPartner');

	Route::post('/partner/card-refresh/{partner_id}', 'AdminKupciPartneriController@card_refresh');

    Route::post('/partneri_search', 'AdminB2BPartnerController@partneri_search');
    Route::get('/podesavanja', 'AdminB2BSettingsController@index');
    Route::post('/settings', 'AdminB2BSettingsController@settings');
    Route::post('/upload-cenovnik', 'AdminB2BSettingsController@upload_cenovnik');

	Route::post('/ajax/prihvati', 'AdminB2BNarudzbinaController@prihvati');
	Route::post('/ajax/realizuj', 'AdminB2BNarudzbinaController@realizuj');
	Route::post('/ajax/storniraj', 'AdminB2BNarudzbinaController@storniraj');
	Route::post('/ajax/nestorniraj', 'AdminB2BNarudzbinaController@nestorniraj');
	Route::post('/ajax/obrisi', 'AdminB2BNarudzbinaController@obrisi');
	Route::post('/ajax/editCena', 'AdminB2BNarudzbinaController@editCena');
	Route::post('/ajax/delete_stavka', 'AdminB2BNarudzbinaController@deleteStavka');
	Route::post('/ajax/narudzbine_search', 'AdminB2BNarudzbinaController@narudzbineSearch');
	Route::post('/ajax/update_kolicina', 'AdminB2BNarudzbinaController@updateKolicina');
	Route::post('/ajax/edit-avans', 'AdminB2BNarudzbinaController@editAvans');

	// upload fajlova
    Route::post('/upload-document-add', 'AdminB2BFilesController@uploadDocumentAdd');
    Route::get('/upload-document-delete/{file}', 'AdminB2BFilesController@uploadDocumentDelete');
    Route::post('/ajax/files-content', 'AdminB2BFilesController@filesContent');
    

	Route::get('/pdf/{web_b2b_narudzbina_id}', 'AdminB2BNarudzbinaController@pdf');
	Route::get('/pdf_racun/{web_b2b_narudzbina_id}', 'AdminB2BNarudzbinaController@pdf_racun');
	Route::get('/pdf_predracun/{web_b2b_narudzbina_id}', 'AdminB2BNarudzbinaController@pdf_predracun');
	Route::get('/pdf_ponuda/{web_b2b_narudzbina_id}', 'AdminB2BNarudzbinaController@pdf_ponuda');
    //IS
    Route::get('/is', 'AdminB2BISController@is');
    Route::post('/is/load', 'AdminB2BISController@custom_load');
    Route::post('is/file-upload', 'AdminB2BISController@file_upload');

    //IS POST
    Route::group(array('prefix'=>'wings','before'=>'wings'),function (){
        Route::post('/post', 'AdminB2BISController@wings_post');
    });
    Route::group(array('prefix'=>'calculus','before'=>'calculus'),function (){
        Route::post('/post', 'AdminB2BISController@calculus_post');
    });
    Route::group(array('prefix'=>'infograf','before'=>'infograf'),function (){
        Route::post('/post', 'AdminB2BISController@infograf_post');
    });
    Route::group(array('prefix'=>'logik','before'=>'logik'),function (){
        Route::post('/post', 'AdminB2BISController@logik_post');
    });
    Route::group(array('prefix'=>'roaming','before'=>'roaming_is'),function (){
        Route::post('/post', 'AdminB2BISController@roaming_post');
    });
    Route::group(array('prefix'=>'sbcs','before'=>'sbcs_is'),function (){
        Route::post('/post', 'AdminB2BISController@sbcs_post');
    });
    Route::group(array('prefix'=>'skala','before'=>'skala_is'),function (){
        Route::post('/post', 'AdminB2BISController@skala_post');
    });
    Route::group(array('prefix'=>'gsm','before'=>'gsm_is'),function (){
        Route::post('/post', 'AdminB2BISController@gsm_post');
    });
    Route::group(array('prefix'=>'promobi','before'=>'promobi_is'),function (){
        Route::post('/post', 'AdminB2BISController@promobi_post');
    });
    Route::group(array('prefix'=>'nssport','before'=>'nssport_is'),function (){
        Route::post('/post', 'AdminB2BISController@nssport_post');
    });
    Route::group(array('prefix'=>'xml','before'=>'xml_is'),function (){
        Route::post('/post', 'AdminB2BISController@xml_post');
    });
    Route::group(array('prefix'=>'softcom','before'=>'softcom_is'),function (){
        Route::post('/post', 'AdminB2BISController@softcom_post');
    });
    Route::group(array('prefix'=>'panteon','before'=>'panteon_is'),function (){
        Route::post('/post', 'AdminB2BISController@panteon_post');
    });
    Route::group(array('prefix'=>'minimax','before'=>'minimax'),function (){
        Route::post('/post', 'AdminB2BISController@minimax_post');
    });

    // Rabat i kategorija
    Route::post('ajax/rabat_edit', 'AdminB2BArticlesController@ajax');
    Route::post('ajax/rabat_partner_edit', 'AdminB2BPartnerController@rabat_partner_edit');
    
    Route::post('ajax/kategorija_partner_edit', 'AdminB2BPartnerController@kategorija_partner_edit');
    Route::post('ajax/rabat_group_edit', 'AdminB2BArticlesController@ajax');
    
    Route::post('ajax/akc_rabat_group_edit', 'AdminB2BArticlesController@ajax');
    Route::post('ajax/rabat_proizvodjac_edit', 'AdminB2BPartnerController@ajax');
    Route::post('ajax/edit_permision_manufacturer', 'AdminB2BPartnerController@ajax');
    Route::post('ajax/edit_permision', 'AdminB2BPartnerController@ajax');
    Route::get('rabat_kombinacije/{search?}', 'AdminB2BPartnerController@rabat_kombinacije');
    Route::post('rabat_kombinacije', 'AdminB2BPartnerController@rabat_kombinacije_edit');
    Route::post('rabat_kombinacije/{id?}/delete', 'AdminB2BPartnerController@rabat_kombinacije_delete');

    
    // Stranice
    Route::get('/b2b_stranice/{stranica_id}/{jezik_id?}/{page_section_id?}/{page_section_type_id?}', 'AdminB2BPagesController@stranice');
    Route::post('/position', 'AdminB2BPagesController@position');

    Route::post('/page-section-edit', 'AdminB2BPagesController@page_section_edit');
    Route::get('/page-section-delete/{page_section_id}', 'AdminB2BPagesController@page_section_delete');
    Route::post('/page-section-position', 'AdminB2BPagesController@page_section_position');
    Route::post('/page-section-add', 'AdminB2BPagesController@page_section_add');
    Route::get('/page-section-remove/{page_id}/{section_id}', 'AdminB2BPagesController@page_section_remove');

    // slajderi
    Route::get('/slider/{slider_id}/{slider_item_id?}/{lang_id?}', 'AdminB2BSlajderiController@slider');
    Route::post('/slider-edit', 'AdminB2BSlajderiController@slider_edit');
    Route::post('/slider-item-edit', 'AdminB2BSlajderiController@slider_item_edit');
    Route::get('/slider-delete/{slider_id}', 'AdminB2BSlajderiController@slider_delete');
    Route::get('/slider-item-delete/{slider_item_id}', 'AdminB2BSlajderiController@slider_item_delete');
    Route::post('/slider-items-position', 'AdminB2BSlajderiController@slider_items_position');
    Route::post('/bg-img-edit', 'AdminB2BSlajderiController@bg_uload');
    Route::get('/bg-img-delete', 'AdminB2BSlajderiController@bgImgDelete');

    Route::post('/pages', 'AdminB2BPagesController@store');
    Route::post('/delete_page', 'AdminB2BPagesController@delete_page');
    Route::post('/image_upload', 'AdminB2BPagesController@image_upload');
    Route::post('/delete_image', 'AdminB2BPagesController@delete_image');
   
    Route::get('/baneri-slajdovi/{id}/{type}', 'AdminB2BBaneriController@banners_sliders');
	Route::post('/banner-edit', 'AdminB2BBaneriController@banner_edit');
	Route::get('/baneri-delete/{id}', 'AdminB2BBaneriController@baneri_delete');
	Route::post('/bg-img-edit', 'AdminB2BBaneriController@bg_uload');
	Route::get('/bg-img-delete', 'AdminB2BBaneriController@bgImgDelete');
	Route::post('/position', 'AdminB2BController@position');
	Route::post('/position-baner', 'AdminB2BBaneriController@position_baner');
	
	Route::get('/vesti/{active?}', 'AdminB2BVestiController@vesti');
	Route::get('/vest/{id}/{jezik_id?}', 'AdminB2BVestiController@vest');
	Route::post('/vesti/{id}/save', 'AdminB2BVestiController@saveNews');
	Route::get('/vesti/{id}/delete', 'AdminB2BVestiController@deleteNews');
	Route::get('/vesti/f/{id}', 'AdminB2BVestiController@filter');

    // partner kategorije
	Route::get('/partner_kategorija/{id_kategorije?}', 'AdminB2BPartnerController@partner_kategorija');
	Route::post('/partner_kategorija-edit', 'AdminB2BPartnerController@kategorije_edit');
	Route::get('/partner_kategorija-delete/{id_kategorije}', 'AdminB2BPartnerController@kategorije_delete');

});


//automatika za import i is
Route::group(array('before'=>'hash_protect'),function (){
    Route::get('/auto-full-import/{key}/{roba_insert?}','AdminImportFileController@auto_full_import');
    Route::get('/auto-short-import/{key}/{type?}','AdminImportFileController@auto_short_import');
    Route::get('/auto-import-3/{key}','AdminImportFileController@auto_import_3');

    //is
    Route::get('/auto-import-is/{key}','AdminB2BISController@auto_import_is');
    Route::get('/auto-import-is-price/{key}','AdminB2BISController@auto_import_is_price');
    Route::get('/auto-import-is-partner-full/{key}','AdminB2BISController@auto_import_is_partner_full');
    Route::get('/auto-import-is-partner-cart/{key}','AdminB2BISController@auto_import_is_partner_cart');
});

//export
Route::group(array('before'=>'hash_protect_export'),function (){
    
    Route::get('/direct-export/{export_sifra}/{kind}/{key}','AdminExportController@direct_export');
    Route::get('/export/{export_sifra}/{kind}/{key}','AdminExportController@export');
    Route::get('/export/export-grupe/{export_id}/{kind}/{key}','AdminExportController@export_grupe');
    Route::post('/export/grupe-povezi/{key}','AdminExportController@grupe_povezi');
    Route::post('/export/grupe-razvezi/{key}','AdminExportController@grupe_razvezi');
    Route::post('/export/grupa-file/{key}','AdminExportController@grupa_file');

    Route::get('/export/auto/{key}', 'AdminExportController@shopmania_auto_export');
    Route::post('/export/shopmania-config/{key}','AdminExportController@shopmania_config');
    Route::post('/export/magento-config/{key}','AdminExportController@magento_config');
    Route::post('/export/woocommerce-config/{key}','AdminExportController@woocommerce_config');
    Route::post('/export/shopify-config/{key}','AdminExportController@shopify_config');
    Route::post('/export/kupindo-config/{key}','AdminExportController@kupindo_config');
    Route::post('/export/ceners-config/{key}','AdminExportController@ceners_config');

});
Route::get('/backup/{backup_sifra}/{kind}/{key}','AdminBackupController@backup');


/////////////////////////////          B2B          ///////////////////////////////////////////////

Route::get('/b2b','B2bController@index');
//B2B Login
Route::get('/b2b/registration',array('as'=>'b2b.registration','uses'=>'B2bController@registration'));
Route::post('/b2b/registration',array('as'=>'b2b.registration.store','uses'=>'B2bController@registrationStore'));
Route::get('/b2b/zaboravljena-lozinka',array('as'=>'b2b.forgot_password','uses'=>'B2bController@forgotPassword'));
Route::post('/b2b/zaboravljena-lozinka',array('as'=>'b2b.forgot_password_post','uses'=>'B2bController@forgotPasswordPost'));
Route::get('/b2b/login','B2bController@login');
Route::post('/b2b/login/store',array('as'=>'b2b.login.store', 'uses'=>'B2bController@loginStore'));
//B2B
Route::group(array('prefix'=>'b2b','before'=>'b2b'),function (){
    Route::group(array('prefix'=>'user','before'=>'b2b_user_data'),function (){
	    Route::get('/edit',array('as'=>'b2b.user_edit','uses'=>'B2bUserController@userEdit'));
	    Route::post('/edit',array('as'=>'b2b.user_update','uses'=>'B2bUserController@userUpdate'));
	    Route::get('/card/{date_start?}/{date_end?}','B2bUserController@userCard');
	    Route::post('/card-refresh','B2bUserController@cardRefresh');
	    Route::get('/orders/{date_start?}/{date_end?}','B2bUserController@userOrders');
	    Route::post('/order','B2bUserController@userOrder');
	    Route::get('/users','B2bUserController@userKorisnici');
	    Route::get('/user/{partner_korisnik_id}','B2bUserController@userKorisnik');
	    Route::post('/user_edit',array('as'=>'b2b.user_user_edit','uses'=>'B2bUserController@userKorisnikEdit'));
	    Route::get('/user/{partner_korisnik_id}/delete','B2bUserController@userKorisnikObrisi');
	    Route::post('/partner-cart-items','B2bUserController@partnerCartItems');
	});
    Route::get('/B2Btagovi'.'/{tag}', 'B2bController@tagovi');
    Route::post('/low_price_mail', 'B2bController@low_price_mail');
	Route::get('/logout','B2bController@logout');
    Route::get('/ordering/{type}',array('as'=>'b2b.ordering','uses'=>'B2bController@ordering'));
    Route::get('/pretraga',array('as'=>'b2b.search','uses'=>'B2bController@search'));
    Route::get('/artikli/{grupa1}', array('as'=>'products_first','uses'=>'B2bController@products_first'));
    Route::get('/artikli/{grupa1}/{grupa2}', array('as'=>'products_second','uses'=>'B2bController@products_second'));
    Route::get('/artikli/{grupa1}/{grupa2}/{grupa3}', array('as'=>'products_third','uses'=>'B2bController@products_third'));
    Route::get('/artikli/{grupa1}/{grupa2}/{grupa3}/{grupa4}', array('as'=>'products_fourth','uses'=>'B2bController@products_fourth'));
    Route::get('/prikaz/{prikaz}', 'B2bController@prikaz');
    Route::get('/valuta/{valuta}', 'B2bController@b2b_valuta');
    Route::get('/limit/{limit}', 'B2bController@limit');
    Route::get('/artikal/{artikal}', array('as'=>'b2b.product','uses'=>'B2bController@product'));
    Route::post('/cart_add', array('as'=>'b2b.cart_add','uses'=>'B2bController@cartAdd'));
    Route::post('/cart_delete', array('as'=>'b2b.cart_delete','uses'=>'B2bController@cartDelete'));
    Route::post('/cart_delete_all', array('as'=>'b2b.cart_delete_all','uses'=>'B2bController@cartDeleteAll'));
    Route::get('/cart_content', array('as'=>'b2b.cart_content','uses'=>'B2bController@cartContent'));
    Route::post('/check_out', array('as'=>'b2b.check_out','uses'=>'B2bController@checkOut'));
    Route::get('/porudzbina/{bill}', array('as'=>'b2b.order','uses'=>'B2bController@order'));
    Route::get('/artikli-tip/{type}/{page}', array('as'=>'b2b.products.type','uses'=>'B2bController@productsType'));
    Route::get('/blog', 'VestiB2bController@getNews');
    Route::get('/blog/{id?}', 'VestiB2bController@oneNew');
    Route::get('/usluge-prijava-kvara', 'B2bUslugePrijavaKvaraController@usluge_prijava_kvara');
    Route::post('/prijava-kvara', 'B2bUslugePrijavaKvaraController@prijava_kvara');
    Route::get('/reklamacije', 'B2bTicketingController@reklamacije');
    Route::get('/tiket', 'B2bTicketingController@tiket');
    Route::post('/radni-nalog-prijem-post', 'B2bTicketingController@radni_nalog_prijem_post');
    Route::get('{page}', 'B2bController@page');
    Route::get('/brendovi', 'B2bController@brendovi');
    Route::get('/servis','B2bController@servis');
    Route::get('/rma','B2bController@servis');
    Route::post('/newsletter', 'B2bController@newsletter_add');
    Route::get('/katalog/{katalog_id?}', 'B2bKatalogController@katalog');
    Route::get('/kontakt','B2bController@kontakt');
    Route::post('/servis_send','B2bController@opisKvaraMail');
    Route::post('/price_diff','B2bController@price_diff');
    Route::post('/bestseller','B2bController@bestseller');
    Route::post('/comment-add-b2b','B2bController@comment_add_b2b');
    Route::get('/proizvodjac/{naziv}/{grupa?}', 'B2bBrendoviController@proizvodjac');
    Route::get('/tip/{naziv}/{grupa?}', 'B2bTipoviController@tip');

    Route::group(array('prefix'=>'dokumenti','before'=>'b2b_dokumenti'),function (){
	    Route::get('/ponude', 'B2bDokumentiController@ponude');
	    Route::get('/ponuda/{ponuda_id}', 'B2bDokumentiController@ponuda');
	    Route::post('/ponuda-post', 'B2bDokumentiController@ponuda_post');
	    Route::get('/ponuda-stavka/{ponuda_stavka_id}/delete', 'B2bDokumentiController@ponuda_stavka_delete');
	    Route::get('/ponuda/{ponuda_id}/delete', 'B2bDokumentiController@ponuda_delete');
	    Route::post('/ponuda-stavka-save', 'B2bDokumentiController@ponuda_stavka_save');
    });
});

/////////////////////////////         DOKUMENTI        ///////////////////////////////////////////////
Route::get('/dokumenti/login','DokumentiAuthController@login');
Route::post('/dokumenti/login/store', 'DokumentiAuthController@loginStore');
Route::group(array('prefix'=>'dokumenti','before'=>'dokumenti'),function (){
	Route::get('/','PonudaController@ponude');
	Route::get('/logout','DokumentiAuthController@logout');

	//nalog
	Route::get('/nalog','NalogController@nalog');
	Route::post('/nalog-save','NalogController@nalog_save');
	//ponude
	Route::get('/ponude','PonudaController@ponude');
	Route::get('/ponuda/{ponuda_id}','PonudaController@ponuda');
	Route::post('ponuda-post','PonudaController@ponuda_post');
	Route::get('/ponuda-stavka/{ponuda_stavka_id}/delete','PonudaController@ponuda_stavka_delete');
	Route::get('/ponuda/{ponuda_id}/delete','PonudaController@ponuda_delete');
	Route::get('/ponuda-pdf/{ponuda_id}','PonudaController@ponuda_pdf');
	Route::post('/ajax/ponuda-stavka-save','PonudaController@ponuda_stavka_save');
	Route::post('/ajax/ponuda-kolicina-narudzbine','PonudaController@ponuda_kolicina_narudzbine');
	Route::post('/ajax/ponuda-bez-pdv','PonudaController@ponuda_bez_pdv');
	
	//racuni
	Route::get('/racuni','RacunController@racuni');
	Route::get('/racun/{racun_id}','RacunController@racun');
	Route::post('racun-post','RacunController@racun_post');
	Route::get('/racun-stavka/{racun_stavka_id}/delete','RacunController@racun_stavka_delete');
	Route::get('/racun/{racun_id}/delete','RacunController@racun_delete');
	Route::get('/racun-pdf/{racun_id}','RacunController@racun_pdf');
	Route::post('/ajax/racun-stavka-save','RacunController@racun_stavka_save');
	//predracuni
	Route::get('/predracuni','PredracunController@predracuni');
	Route::get('/predracun/{predracun_id}','PredracunController@predracun');
	Route::post('predracun-post','PredracunController@predracun_post');
	Route::get('/predracun-stavka/{predracun_stavka_id}/delete','PredracunController@predracun_stavka_delete');
	Route::get('/predracun/{predracun_id}/delete','PredracunController@predracun_delete');
	Route::get('/predracun-pdf/{predracun_id}','PredracunController@predracun_pdf');
	Route::post('/ajax/predracun-stavka-save','PredracunController@predracun_stavka_save');
	//ajax
	Route::post('ajax/partner-pretraga','PonudaController@partner_pretraga');
	Route::post('ajax/partner-podaci','PonudaController@partner_podaci');
	Route::post('ajax/change-sablon','DokumentController@change_sablon');
	
	//podesavanja
	Route::get('/podesavanja','DokumentController@podesavanja');
	Route::post('/podesavanja-post','DokumentController@podesavanja_post');

	//partneri
	Route::get('/partneri','PartnerController@partneri');
	Route::post('/partneri/partneri-filter','PartnerController@partneriFilter');
	Route::get('/partner/{partner_id}','PartnerController@partner');
	Route::post('/partneri/{partner_id}/save','PartnerController@savePartner');
	Route::get('/partner/{partner_id}/{confirm}/delete','PartnerController@deletePartner');
	
});


Route::get('/rma/login','RmaLoginController@login');
Route::post('/rma/login/store', 'RmaLoginController@loginStore');
/////////////////////////////          RMA          ///////////////////////////////////////////////
Route::group(array('prefix'=>'rma','before'=>'rma'),function (){
	Route::get('/logout','RmaLoginController@logout');
	
	//operacije
	Route::get('/operacije/{grupa_id?}/{id?}', 'OperationsController@operacije');
	Route::post('/ajax/operacija-grupa-save', 'OperationsController@operacija_grupa_save');
	Route::post('/ajax/operacija-servis-save', 'OperationsController@operacija_servis_save');
	Route::post('/ajax/operacija-save', 'OperationsController@operacija_save');
	Route::get('/operacija-grupa/{grupa_id}/delete', 'OperationsController@operacija_grupa_delete');
	Route::get('/operacija/{operacija_id}/delete', 'OperationsController@operacija_delete');
	Route::get('/operacija-servis/{operacija_servis_id}/delete', 'OperationsController@operacija_sevis_delete');
	Route::get('/operacije-export', 'OperationsController@operacije_export');
	
	//serviseri
	Route::get('/serviseri/{servis_id?}/{serviser_id?}', 'ServiseriController@serviseri');
	Route::post('/serviseri-post', 'ServiseriController@serviseri_post');
	Route::get('/serviseri/{servis_id}/{serviser_id}/delete', 'ServiseriController@serviseri_delete');

	//RMA
	Route::get('/', 'RMAController@rma');
	Route::get('/izvestaj-serviseri', 'IzvestajiController@izvestaj_serviseri');
	Route::get('/izvestaj-brendovi', 'IzvestajiController@izvestaj_brendovi');
	Route::get('/izvestaj-grupe', 'IzvestajiController@izvestaj_grupe');
	Route::get('/izvestaj-modeli-grupe', 'IzvestajiController@izvestaj_modeli_grupe');
	Route::get('/izvestaj-rezervni-delovi', 'IzvestajiController@izvestaj_rezervni_delovi');
	Route::get('/izvestaj-operacije', 'IzvestajiController@izvestaj_operacije');
	Route::post('/ajax/radni-nalog-partner-podaci', 'RMAController@radni_nalog_partner_podaci');
	Route::post('/ajax/radni-nalog-kupac-podaci', 'RMAController@radni_nalog_kupac_podaci');
	Route::get('/radni-nalog-prijem/{id}', 'RMAController@radni_nalog_prijem');
	Route::get('/radni-nalog-prijem-dokument/{id}', 'RMAController@radni_nalog_prijem_dokument');
	Route::post('/radni-nalog-prijem-post', 'RMAController@radni_nalog_prijem_post');
	Route::post('/ajax/kupci-search', 'RMAController@kupci_search');
	Route::get('/radni-nalog-opis-rada/{id}', 'RMAController@radni_nalog_opis_rada');
	Route::get('/radni-nalog/{radni_nalog_id}/delete', 'RMAController@radni_nalog_delete');

	
	Route::post('/radni-nalog-opis-rada-post', 'RMAController@radni_nalog_opis_rada_post');
	Route::post('/ajax/radni-nalog-operacija-modal-content', 'RMAController@radni_nalog_operacija_modal_content');
	Route::post('/ajax/radni-nalog-operacija-select', 'RMAController@radni_nalog_operacija_select');
	Route::post('/ajax/radni-nalog-operacija-change', 'RMAController@radni_nalog_operacija_change');
	Route::post('/ajax/radni-nalog-operacija-save', 'RMAController@radni_nalog_operacija_save');
	Route::get('/radni-nalog-operacija/{radni_nalog_operacije_id}/delete', 'RMAController@radni_nalog_operacija_delete');

	Route::post('/ajax/radni-nalog-rezervni-deo-modal-content', 'RMAController@radni_nalog_rezervni_deo_modal_content');
	Route::post('/ajax/radni-nalog-rezervni-deo-roba-search', 'RMAController@radni_nalog_rezervni_deo_roba_search');
	Route::post('/ajax/radni-nalog-rezervni-deo-save', 'RMAController@radni_nalog_rezervni_deo_save');
	Route::get('/radni-nalog-rezervni-deo/{radni_nalog_rezervni_deo_id}/delete', 'RMAController@radni_nalog_rezervni_deo_delete');

	Route::post('/ajax/radni-nalog-trosak-modal-content', 'RMAController@radni_nalog_trosak_modal_content');
	Route::post('/ajax/radni-nalog-trosak-save', 'RMAController@radni_nalog_trosak_save');
	Route::get('/radni-nalog-trosak/{radni_nalog_trosak_id}/delete', 'RMAController@radni_nalog_trosak_delete');
	Route::post('/ajax/radni-nalog-opreacija-uzrok-save', 'RMAController@radni_nalog_opreacija_uzrok_save');
	Route::post('/ajax/radni-nalog-opreacija-napomena-save', 'RMAController@radni_nalog_opreacija_napomena_save');
	Route::get('/radni-nalog-trosak-dokumenti/{radni_nalog_id}/{radni_nalog_trosak_id}', 'RMAController@radni_nalog_trosak_dokumenti');
	Route::post('/radni-nalog-trosak-dokumenti-save', 'RMAController@radni_nalog_trosak_dokumenti_save');
	Route::post('/radni-nalog-trosak-dokumenti-prijem-save', 'RMAController@radni_nalog_trosak_dokumenti_prijem_save');
	Route::get('/radni-nalog-trosak-dokument/{radni_nalog_trosak_dokument_id}/delete', 'RMAController@radni_nalog_trosak_dokument_delete');
	Route::get('/radni-nalog-predaja/{id}', 'RMAController@radni_nalog_predaja');
	Route::post('/radni-nalog-predaja-post', 'RMAController@radni_nalog_predaja_post');

	Route::get('/radni-nalog-rekapitulacija/{id}/{trosak?}', 'RMAController@radni_nalog_rekapitulacija');
	Route::get('/radni-nalog-usluzni-servis-uput/{radni_nalog_trosak_id}', 'RMAController@radni_nalog_usluzni_servis_uput');
});


Route::get('/ticketing/login','TicketingLoginController@login');
Route::post('/ticketing/login/store', 'TicketingLoginController@loginStore');
/////////////////////////////          RMA          ///////////////////////////////////////////////
Route::group(array('prefix'=>'ticketing','before'=>'ticketing'),function (){
    Route::get('/logout','TicketingLoginController@logout');

    Route::get('/', 'TicketingController@index');
    Route::post('/radni-nalog-prijem-post', 'TicketingController@radni_nalog_prijem_post');
    Route::get('/reklamacije', 'TicketingController@reklamacije');
    Route::get('/promena-lozinke', 'TicketingLoginController@promena_lozinke');
    Route::post('/promena-lozinke-post', 'TicketingLoginController@promena_lozinke_post');
    
});
/////////////////////////////       SHOP         ///////////////////////////////////////////////

$shop_filter = Language::multi() ? array('prefix'=>'{lang}','before'=>'lang') : array('before'=>'lang');
Route::group($shop_filter,function (){

    if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
        
        return Redirect::to($new_link);
    }
    // Vesti
    Route::get('/'.Url_mod::slug_trans('blog'), 'VestiController@getNews');
    Route::get('/'.Url_mod::slug_trans('blog').'/{blog_slug?}', 'VestiController@oneNew');

    Route::get('/'.Url_mod::slug_trans('katalog').'/{katalog_id?}', 'KatalogController@katalog');

    Route::post('/login-post', 'LoginRegistration@user_login_post');
    Route::post('/login', 'LoginRegistration@user_login');
    Route::get('/logout', 'LoginRegistration@logout');
    Route::post('/zaboravljena-lozinka', 'LoginRegistration@forget_password');
    Route::post('/zaboravljena-lozinka-ajax', 'LoginRegistration@forget_password_ajax');
    Route::post('/registracija-post', 'LoginRegistration@user_registracija');
    Route::get('/'.Url_mod::slug_trans('potvrda-registracije').'/{kod}/{web_kupac_id}', 'LoginRegistration@confirm_user');
    Route::get('/'.Url_mod::slug_trans('korisnik').'/{username}', array('before'=>'user_filter', 'uses'=>'LoginRegistration@korisnik'));
    Route::get('/'.Url_mod::slug_trans('lista-zelja').'/{username}', array('before'=>'user_filter', 'uses'=>'LoginRegistration@lista_zelja'));
    Route::post('/korisnik-edit', 'LoginRegistration@korisnik_edit');

    Route::post('/newsletter', 'MainController@newsletter_add');
    Route::get('/'.Url_mod::slug_trans('newsletter-potvrda').'/{kod}', 'MainController@newsletter_confirm');
    Route::post('/meil_send', 'MailController@meil_send');
    Route::post('/contact-message-send', 'MailController@contact_message_send');
    Route::post('/enquiry-message-send', 'MailController@enquiry_message_send');
    Route::post('/comment-add','MainController@comment_add');
    Route::post('/poll-answer','MainController@poll_answer');
    Route::post('/poll-content','MainController@poll_content');

	Route::post('/quick-view-cart-add', 'CartController@quick_view_cart_add');
    Route::post('/list-cart-add', 'CartController@list_cart_add');
    Route::post('/wish-list-add', 'CartController@wish_list_add');
    Route::post('/wish-list-delete', 'CartController@wish_list_delete');
    Route::post('/add_project_id', 'CartController@project_id');
    Route::post('/product-cart-add', 'CartController@product_cart_add');
    Route::post('/konfigurator-cart-add', 'CartController@konfigurator_cart_add');
    Route::post('/vezani-cart-add', 'CartController@vezani_cart_add');
    Route::post('/cart-add-sub', 'CartController@cart_add_sub');
    Route::post('/cart-stavka-delete', 'CartController@cart_stavka_delete');
    Route::post('/cart-delete', 'CartController@cart_delete');
    Route::post('/vaucer-primeni', 'CartController@vaucer_primeni');
    Route::post('/vaucer-obrisi', 'CartController@vaucer_obrisi');


    Route::post('/select-narudzbina-mesto', 'OrderController@select_narudzbina_mesto');
    Route::post('/select-narudzbina-ulica', 'OrderController@select_narudzbina_ulica');
    Route::post('/order-create', 'OrderController@order_create');
    Route::post('/ajax/count-discount', 'OrderController@cena_vaucera');
    Route::get('/'.Url_mod::slug_trans('narudzbina').'/{web_b2c_narudzbina_id}', 'OrderController@narudzbina');
    Route::get('/intesa/{web_b2c_narudzbina_id}', 'IntesaBankingController@intesa');
    Route::post('/intesa-response', 'IntesaBankingController@intesa_response');
    Route::get('/'.Url_mod::slug_trans('intesa-odgovor').'/{web_b2c_narudzbina_id}','IntesaBankingController@intesa_failure');

    Route::get('/raiffeisen/{web_b2c_narudzbina_id}', 'RaiffeisenBankingController@raiffeisen');
    Route::get('/raiffeisen-response-status/{broj_dokumenta}/{kod_kupca}', 'RaiffeisenBankingController@raiffeisen_response');
    Route::get('/'.Url_mod::slug_trans('raiffeise-odgovor').'/{web_b2c_narudzbina_id}','RaiffeisenBankingController@raiffeisen_failure');
    Route::get('raiffeisen_response/{broj_dokumenta}/{status}/web-kredit', 'RaiffeisenBankingController@raiffeisen_response_servis');
    Route::get('/raiffeisen-web-kredit', 'RaiffeisenBankingController@representative_example');
    
    Route::get('/msu/{web_b2c_narudzbina_id}/{session}', 'MSUBankingController@msu');
    Route::any('/msu-response','MSUBankingController@msu_response');
    Route::get('/'.Url_mod::slug_trans('banka-greska').'/{web_b2c_narudzbina_id}','MSUBankingController@banka_greska');


    Route::post('/livesearch', 'SearchController@livesearch');
    Route::post('/proizvodjac-articles', 'AdminGarancijaController@proizvodjac_articles');

    Route::get('/'.Url_mod::slug_trans('prikaz').'/{prikaz}', 'ArticlesController@prikaz');
    Route::get('/'.Url_mod::slug_trans('limit').'/{limit}', 'ArticlesController@limit');
    Route::get('/'.Url_mod::slug_trans('valuta').'/{valuta}', 'ArticlesController@valuta');
    Route::get('/'.Url_mod::slug_trans('sortiranje').'/{sortiranje}', 'ArticlesController@sortiranje');
    Route::get('/search/{search}/{grupa_pr_id?}', 'SearchController@search');

    Route::post('/compare', 'CompareController@compare');
    Route::post('/compareajax', 'CompareController@compareajax');
    Route::post('/clearCompare', 'CompareController@clear_compare');
    Route::post('/tip-ajax', 'TipController@tip_ajax');

	Route::post('/quick-view', 'ArticleController@quick_view');
 	Route::get('/'.Url_mod::slug_trans('stampanje').'/{roba_id?}', 'ArticleController@stampanje');
    Route::get('/'.Url_mod::slug_trans('stampanje-narudzbenice').'/{web_b2c_narudzbina_id?}/{kod?}', 'OrderController@stampanje_narudzbenice');
    Route::get('/product-deklaracija/{id?}', 'ArticleController@product_deklaracija');

    Route::get('/'.Url_mod::slug_trans('konfigurator').'/{id}', 'ConfiguratorController@konfigurator');
    Route::get('/'.Url_mod::slug_trans('brendovi'), 'BrendoviController@brend_list');
    Route::get('/'.Url_mod::slug_trans('proizvodjac').'/{naziv}/{grupa?}', 'BrendoviController@proizvodjac');
    Route::get('/'.Url_mod::slug_trans('tip').'/{naziv}/{grupa?}', 'TipController@tip_products');
    Route::get('/'.Url_mod::slug_trans('akcija').'/{grupa?}', 'AkcijaController@akcija_products');
    Route::get('/'.Url_mod::slug_trans('usluge-prijava-kvara'), 'UslugePrijavaKvaraController@usluge_prijava_kvara');
    Route::post('/'.Url_mod::slug_trans('prijava-kvara'), 'UslugePrijavaKvaraController@prijava_kvara');

    // Tagovi
    Route::get('/'.Url_mod::slug_trans('tagovi').'/{tag}', 'TagsController@index');
	Route::get('/'.Url_mod::slug_trans('svi-artikli'), 'ArticlesController@artikli_all');
	Route::post('/link-artikla', 'ArticleController@article_link');
    Route::get('/'.Url_mod::slug_trans('artikal').'/{article}/{slika_id?}', 'ArticleController@article');
    Route::get('/{strana}', 'MainController@page');
    Route::get('/{grupa1}/{grupa2}/{grupa3}/{grupa4}', 'ArticlesController@artikli_forth');
    Route::get('/{grupa1}/{grupa2}/{grupa3}', 'ArticlesController@artikli_third');
    Route::get('/{grupa1}/{grupa2}', 'ArticlesController@artikli_second');
    // Route::get('/{grupa1}',array('as'=>'b2c.group1','uses'=>'ArticlesController@artikli_first'));

    //Provera garancije
    Route::post('/check-warranty', 'AdminArticlesController@check_warranty');
                                                                                                             
    Route::get('/', 'MainController@lang');
});

Route::get('/', 'MainController@index');


Route::get('{slug}',function(){
    if(Options::gnrl_options(3061) == 1 && !Session::has('b2c_admin'.Options::server())){
        return View::make('shop/site-unactive');
    }
    if(($new_link = Url_mod::checkOldLink(str_replace(Options::base_url(),'',Request::url()))) != null){
        return Redirect::to($new_link);
    }
    $data = array(
        "strana"=>'Not found',
        "org_strana"=>'Not found',
        "title"=>'Not found',
        "description"=>'Not found',
        "keywords"=>'Not found'
        );
	$content = View::make('shop/themes/'.Support::theme_path().'pages/not_found', $data)->render();
	return Response::make($content, 404);
})->where('slug', '^.*');