<?php

class Logik {

    public static function customer_cart($partner_id){
        return DB::connection('logik')->select("SELECT * FROM partnercard WHERE partnerid = ".strval($partner_id)."");
    }

    public static function customer_cart_body($items,$partner_id){
        $result_arr = array();
        $web_b2b_kartica_id = DB::select("SELECT nextval('web_b2b_kartica_web_b2b_kartica_id_seq')")[0]->nextval;

        foreach($items as $item) {

            $web_b2b_kartica_id++;
            $datum_dokumenta = isset($item->documentdate) ? $item->documentdate : '1970-00-00T00:00:00+02:00';
            $datum_dospeca = isset($item->valutedate) ? $item->valutedate : '1970-00-00T00:00:00+02:00';
            $vrsta_dokumenta = pg_escape_string(substr($item->identtype,0,300));
            $opis = isset($item->description) ? $item->description : pg_escape_string(substr($item->description,0,300));
            $duguje = isset($item->debit) ? $item->debit : 0.00;
            $potrazuje = isset($item->credit) ? $item->credit : 0.00;
            $saldo = (floatval($duguje)-floatval($potrazuje)) > 0 ? (floatval($duguje)-floatval($potrazuje)) : 0.00;

            $id_is = $partner_id.'-'.$web_b2b_kartica_id;

            $result_arr[] = "(".$web_b2b_kartica_id.",".strval($partner_id).",(NULL)::integer,'".$vrsta_dokumenta."',('".$datum_dokumenta."')::date,('".$datum_dospeca."')::date,'".$opis."',".strval($duguje).",".strval($potrazuje).",".strval($saldo).",(NULL)::integer,'".strval($id_is)."')";

        }

        return (object) array("body"=>implode(",",$result_arr));
    }

    public static function customer_cart_insert_update($table_temp_body,$partner_id,$upd_cols=array()) {

        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_b2b_kartica'"));
        $table_temp = "(VALUES ".$table_temp_body.") web_b2b_kartica_temp(".implode(',',$columns).")";

        // DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="web_b2b_kartica_id"){
                $updated_columns[] = "".$col." = web_b2b_kartica_temp.".$col."";
            }
        }
        // DB::statement("UPDATE web_b2b_kartica t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=web_b2b_kartica_temp.id_is");
        DB::statement("DELETE FROM web_b2b_kartica WHERE partner_id=".$partner_id."");
        //insert
        DB::statement("INSERT INTO web_b2b_kartica (SELECT * FROM ".$table_temp.")");
        // DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

        DB::statement("SELECT setval('web_b2b_kartica_web_b2b_kartica_id_seq', (SELECT MAX(web_b2b_kartica_id) FROM web_b2b_kartica), FALSE)");
    }


    public static function updateCart($partner_id=null){
        if(is_null($partner_id)){
            $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        }
        $partner = DB::table('partner')->where('partner_id',$partner_id)->first();

        $items = self::customer_cart(intval($partner->id_is));

        if(count($items) > 0){
            $resultCart = self::customer_cart_body($items,$partner->partner_id);
            if(isset($resultCart->body) && $resultCart->body != ''){
                self::customer_cart_insert_update($resultCart->body,$partner->partner_id);
            }
           // DB::connection('logik')->statement("UPDATE partnercard SET semaphore = 0 WHERE partnerid = ".strval($partner->id_is)."");
        }

    }

    public static function order($web_b2b_narudzbina,$partner,$napomena=''){

        $current_order_id = DB::connection('logik')->select("SELECT MAX(documentid) as current_order_id FROM document")[0]->current_order_id + 1;

        $sql = "INSERT INTO document (documentid,foreign_documentid,documentcurrency,documentdate,valutedate,description,foreign_exchangelistid,number,partnerid,documenttype,documentissuedate,foreign_partneraddressid,processed,processtype,comment) VALUES (".$current_order_id.",NULL,'RSD','".date('Y-m-d')."','".date('Y-m-d')."',NULL,NULL,'".$web_b2b_narudzbina->broj_dokumenta."','".strval($partner->id_is)."',NULL,'".date('Y-m-d')."',NULL,'y','B2B','".$napomena."')";
        DB::connection('logik')->statement($sql);

        return (object) array('id_is'=>$current_order_id,'web_b2b_narudzbina_id'=>$web_b2b_narudzbina->web_b2b_narudzbina_id);
    }
    public static function addOrderStavka($order_ids,$article,$cene,$item_number,$quantity,$kurs=1){
        $avans = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$order_ids->web_b2b_narudzbina_id)->pluck('avans');
        $avans = floatval($avans);
        $rabat = $cene->ukupan_rabat+$avans;
        
        $sql = "INSERT INTO documentitem (rebate,rebatetype,documentid,marginetype,price,itemvalue,productid,productname,quantity,sort,taxvalue) VALUES (".$rabat.",'P',".strval($order_ids->id_is).",'P',".$article->racunska_cena_end*$kurs.",".$article->racunska_cena_end*$kurs.",".strval($article->id_is).",'".$article->naziv_web."',".strval($quantity).",".$item_number.",".$cene->porez.")";
        DB::connection('logik')->statement($sql);
    }

    public static function createOrder($orderId){
        $order = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$orderId)->first();

        $cartItems=DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$orderId)->orderBy('broj_stavke','asc')->get();
        $delivery = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$order->web_nacin_isporuke_id)->pluck('naziv');
        $payment = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$order->web_nacin_placanja_id)->pluck('naziv');
        $note=substr('Način isporuke: '.$delivery.'. Način plaćanja: '.$payment.'. Napomena: '.$order->napomena,0,255);

        $success = false;
        $order_id = 0;

        $partner_id = $order->partner_id;
        $partner = DB::table('partner')->where('partner_id',$partner_id)->whereNotNull('id_is')->first();
        if(!is_null($partner)){
            $order_ids = self::order($order,$partner,$note);

            foreach($cartItems as $key => $stavka){
                $article = DB::table('roba')->where('roba_id',$stavka->roba_id)->first();
                $b2bRabatCene = B2bArticle::b2bRabatCene($stavka->roba_id);
                $kurs=1;
                self::addOrderStavka($order_ids,$article,$b2bRabatCene,($key+1),$stavka->kolicina,$kurs);
            }

            $success = true;
        }
        return (object) array('success'=>$success, 'order_id'=>$order_id);
    }

}
