<?php

class Infograf {

    public static function discounts($partner_id){
        $discounts = self::article_discounts($partner_id);
        $usable_discounts = array();
        foreach($discounts as $discount){
            $usable_discounts[$discount->IDArtikal] = $discount->rabat;
        }
        return $usable_discounts;
    }

    public static function article_discounts($partner_id){
        return DB::connection('infograf')->select("SELECT IDArtikal,getrabat(IDArtikal,".strval($partner_id).") as rabat FROM artikal WHERE cena > 0");
    }
    public static function customer_cart($partner_id){
        return DB::connection('infograf')->select("SELECT * FROM stanje_kupci WHERE Partner = ".strval($partner_id)."");
    }
    public static function order($partner_id,$napomena=''){
        $sql = "INSERT INTO narudzbina (partner_id,napomena) VALUES (".strval($partner_id).",'".strval($napomena)."')";
        DB::connection('infograf')->statement($sql);
        $last_order_id = DB::connection('infograf')->select("SELECT MAX(id) as last_order_id FROM narudzbina")[0]->last_order_id;
        return $last_order_id;
    }
    public static function addOrderStavka($web_order_id,$article_id,$quantity,$amount,$discount){
        $sql = "INSERT INTO narudzbina_stavka (narudzbina_id,artikal_id,kolicina,cena,rabat) VALUES (".strval($web_order_id).",".strval($article_id).",".strval($quantity).",".strval($amount).",".strval($discount).")";
        DB::connection('infograf')->statement($sql);
    }
    public static function orderConfirm($order_id){
        $sql = "CALL web_order(".strval($order_id).")";
        $result = DB::connection('infograf')->select($sql);
        if(isset($result[0])){
            return $result[0]->dok;
        }
        return 0;
    }

    public static function customer_cart_body($items,$partner_id){
        $result_arr = array();
        $web_b2b_kartica_id = DB::select("SELECT nextval('web_b2b_kartica_web_b2b_kartica_id_seq')")[0]->nextval;

        foreach($items as $item) {

            $web_b2b_kartica_id++;
            $partner_id = $partner_id;
            $datum_dokumenta = isset($item->datum) ? $item->datum : '1970-00-00T00:00:00+02:00';
            $vrsta_dokumenta = pg_escape_string(substr(str_replace(array('è','æ','ð','Æ','È'), array('č','ć','đ','Ć','Č'),(isset($item->faktura) ? $item->faktura : (isset($item->nalog) ? $item->nalog : ''))),0,300));
            $duguje = isset($item->duguje) ? $item->duguje : 0.00;
            $potrazuje = isset($item->potrazuje) ? $item->potrazuje : 0.00;
            $saldo = (floatval($duguje)-floatval($potrazuje)) > 0 ? (floatval($duguje)-floatval($potrazuje)) : 0.00;

            // $id_is = substr(($partner_id.'-'.substr($datum_dokumenta,0,10).B2bUrl::slug_trans($vrsta_dokumenta).$duguje.$potrazuje.(isset($item->BR) ? $item->BR : '').(isset($item->CB) ? $item->CB : '')),0,254);
            $id_is = $partner_id.'-'.$web_b2b_kartica_id;

            $result_arr[] = "(".$web_b2b_kartica_id.",".strval($partner_id).",(NULL)::integer,'".$vrsta_dokumenta."',('".$datum_dokumenta."')::date,('".$datum_dokumenta."')::date,'".$vrsta_dokumenta."',".strval($duguje).",".strval($potrazuje).",".strval($saldo).",(NULL)::integer,'".strval($id_is)."')";

        }

        return (object) array("body"=>implode(",",$result_arr));
    }

    public static function customer_cart_insert_update($table_temp_body,$partner_id,$upd_cols=array()) {

        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_b2b_kartica'"));
        $table_temp = "(VALUES ".$table_temp_body.") web_b2b_kartica_temp(".implode(',',$columns).")";

        // DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="web_b2b_kartica_id"){
                $updated_columns[] = "".$col." = web_b2b_kartica_temp.".$col."";
            }
        }
        // DB::statement("UPDATE web_b2b_kartica t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=web_b2b_kartica_temp.id_is");
        DB::statement("DELETE FROM web_b2b_kartica WHERE partner_id=".$partner_id."");
        //insert
        DB::statement("INSERT INTO web_b2b_kartica (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM web_b2b_kartica t WHERE t.id_is=web_b2b_kartica_temp.id_is))");
        // DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

        DB::statement("SELECT setval('web_b2b_kartica_web_b2b_kartica_id_seq', (SELECT MAX(web_b2b_kartica_id) FROM web_b2b_kartica), FALSE)");
    }


    public static function updateCart($partner_id=null){
        if(is_null($partner_id)){
            $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        }
        $partner = DB::table('partner')->where('partner_id',$partner_id)->first();

        $items = self::customer_cart(intval($partner->id_is));
        if(count($items) > 0){
            $resultCart = self::customer_cart_body($items,$partner->partner_id);
            if(isset($resultCart->body) && $resultCart->body != ''){
                self::customer_cart_insert_update($resultCart->body,$partner->partner_id);
            }
        }

    }

    public static function createOrder($cartItems,$note=''){
        $success = false;
        $order_id = 0;

        $kurs = DB::table('kursna_lista')->where('kursna_lista_id',DB::table('kursna_lista')->max('kursna_lista_id'))->pluck('ziralni');
        $partner_id = Session::get('b2b_user_'.B2bOptions::server());
        $partner = DB::table('partner')->where('partner_id',$partner_id)->whereNotNull('id_is')->first();
        if(!is_null($partner)){
            $order_id = self::order($partner->id_is,$note);

            foreach($cartItems as $stavka){
                $stavka_id = DB::table('roba')->where('roba_id',$stavka->roba_id)->pluck('id_is');
                $b2bRabatCene = B2bArticle::b2bRabatCene($stavka->roba_id);
                self::addOrderStavka($order_id,$stavka_id,$stavka->kolicina,($b2bRabatCene->ukupna_cena / $kurs),$b2bRabatCene->ukupan_rabat);
            }
            
            $order_id = self::orderConfirm($order_id);

            $success = true;
        }
        return (object) array('success'=>$success, 'order_id'=>$order_id);
    }

}