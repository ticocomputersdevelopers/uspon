<?php 

class B2bUrl {

	// public static function get_grupa_pr_id($parent_id,$grupe,$level){
	// 	$grupa_pr_id=0;

	// 	$grupa_prs = DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$parent_id,'prikaz'=>1,'web_b2b_prikazi'=>1))->get();
	// 	foreach($grupa_prs as $row){
	// 		if($grupe[$level] == self::slugify($row->grupa)){
	// 			$grupa_pr_id=$row->grupa_pr_id;
	// 		}
	// 	}
	// 	if(isset($grupe[$level+1])){
	// 		// unset($grupe[$level]);
	// 		return self::get_grupa_pr_id($grupa_pr_id,$grupe,$level+1);
	// 	}
	// 	return $grupa_pr_id;
	// }
	public static function get_grupa_pr_id($grupe){
		$grouped = array();
		for ($i=(count($grupe)-1); $i >= 0; $i--) {
			$grouped[] = array('grupa'=>$grupe[$i],'parent'=>(isset($grupe[$i-1]) ? $grupe[$i-1] : null));
		}

		$grupa_pr_id = 0;
		for ($i=(count($grouped)-1); $i >= 0; $i--) {
			$grupa_pr_details = self::get_grupa_pr_details($grouped[$i]['grupa'],$grouped[$i]['parent']);
			foreach($grupa_pr_details as $grupa_pr_detail){
				if($grupa_pr_id == $grupa_pr_detail->parrent_grupa_pr_id){
					$grupa_pr_id = $grupa_pr_detail->grupa_pr_id;
				}
			}
		}
		return $grupa_pr_id;
	}

	public static function get_grupa_pr_details($grupa,$parent_grupa=null){
		$grupa_pr_ids=array();
		$parrent_grupa_pr_ids=array();
		$grupa_prs = DB::table('grupa_pr')->where(array('prikaz'=>1,'web_b2b_prikazi'=>1))->get();
		foreach($grupa_prs as $row){
			if($grupa == trim(self::slugify($row->grupa)) && $row->grupa_pr_id > 0){
				$grupa_pr_ids[] = $row->grupa_pr_id;
			}
			if(!is_null($parent_grupa) && $parent_grupa == trim(self::slugify($row->grupa)) && $row->grupa_pr_id > -1){
				$parrent_grupa_pr_ids[] = $row->grupa_pr_id;
			}
		}

		$result = array();
		if(count($grupa_pr_ids) > 0){
			if(is_null($parent_grupa)){
				$result = DB::table('grupa_pr')->select('grupa_pr_id','parrent_grupa_pr_id')->whereIn('grupa_pr_id',$grupa_pr_ids)->get();
			}else if(count($parrent_grupa_pr_ids) > 0){
				$result = DB::table('grupa_pr')->select('grupa_pr_id','parrent_grupa_pr_id')->whereIn('grupa_pr_id',$grupa_pr_ids)->whereIn('parrent_grupa_pr_id',$parrent_grupa_pr_ids)->get();
			}
		}
		return $result;
	}
	
	public static function grupa_pr_id_bc($grupa){
		$grupa_pr_id=0;
		$grupa_prs = DB::table('grupa_pr')->where(array('prikaz'=>1,'web_b2b_prikazi'=>1))->get();
		foreach($grupa_prs as $row){
			if($grupa == self::slugify($row->grupa)){
				$grupa_pr_id=$row->grupa_pr_id;
			}
		}
		return $grupa_pr_id;
	}

	public static function search_conver($str){
		 $string_new=strtolower($str); 
		$urlKey = strtr($string_new, array("&frasl;"=>"/"," "=>"%"));
	
		return $urlKey;
	}
	//pretvara reci u link
	 public static function slugify($str,$lang=null){
	    $table = array(
	            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
	            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
	            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
	            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
	            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
	            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
	            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
	            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '-', ' ' => '-'," )"=> ""
	    );
	    $string = strtolower(strtr($str, $table));
	    $string = str_replace(array('А','Б','В','Г','Д','Ђ','Е','Ж','З','И','Ј','К','Л','Љ','М','Н','Њ','О','П','Р','С','Т','Ћ','У','Ф','Х','Ц','Ч','Џ','Ш','а','б','в','г','д','ђ','е','ж','з','и','ј','к','л','љ','м','н','њ','о','п','р','с','т','ћ','у','ф','х','ц','ч','џ','ш'),array('a','b','v','g','d','dj','e','z','z','i','j','k','l','lj','m','n','nj','o','p','r','s','t','c','u','f','h','c','c','dz','s','a','b','v','g','d','dj','e','z','z','i','j','k','l','lj','m','n','nj','o','p','r','s','t','c','u','f','h','c','c','dz','s'), $string);
	    $urlKey = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
		return $urlKey;
	}
	
	 public static function filters_convert($str){
		 $string_new=strtolower($str);
		$urlKey = strtr($string_new, array("š"=>"s","č"=>"c","ć"=>"c","đ"=>"dj","ž"=>"z","Š"=>"s","Č"=>"c","Ć"=>"c","Đ"=>"dj","Ž"=>"z"," "=>"_","-"=>"",','=>"","."=>"","*"=>"","?"=> "","!"=> "","/"=> "","("=> "",")"=> "",'"'=> "",'%'=> "",'#'=> "",'$'=> "",'+'=> "",'-'=> "",'`'=> "",'@'=> "",'^'=> "",'&'=> "",'{'=> "",'}'=> "",'['=> "",']'=> "",';' => "",'|'=> "" ,':'=> "" ,'<'=> "",'>'=> "",'_'=> "" ));
	
		return $urlKey;
    
    	
	}
	public static function text_convert($string){
		$urlKey = strtr($string, array("?"=>"&scaron;","?"=>"&#269;","?"=>"&#263;","?"=>"&#273;","?"=>"&#382;","?"=>"&Scaron;","?"=>"&#268;","?"=>"&#262;","?"=>"&#272;","?"=>"&#381;",'"'=>""));
    	return $urlKey;
	}
	public static function b2bBreadcrumbs(){
		$url=$_SERVER['REQUEST_URI'];
		$url_arr = explode('/',$url);
		
		$string = "<li><a href='".B2bOptions::base_url()."b2b'>".B2bCommon::get_title_page_start()."</a></li>";
		$end = count($url_arr)-1;
		for($i=3;$i<=$end;$i++){

			$gr_arr = array();
			foreach($url_arr as $key => $grp){
				if($key <= $i && $key >= 3){
					$gr_arr[] = $grp;
				}
			}

			if($i < $end){
				$string .= "<li><a href='".B2bOptions::base_url()."b2b/artikli/".implode('/',$gr_arr)."'>".B2bCommon::getGrupa(self::grupa_pr_id_bc($url_arr[$i]))."</a></li>";
			}else{
				$url_arr[$i] = explode('?',$url_arr[$i])[0];
				$string .= "<li>".B2bCommon::getGrupa(self::grupa_pr_id_bc($url_arr[$i]))."</li>";
			}
		}
		echo $string;
	}

}