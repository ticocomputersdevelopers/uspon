<?php 

class B2bBasket {

    public static function cartItems(){
        if(Session::has('b2b_cart')){
            return DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->orderBy('broj_stavke','asc')->get();
        }
        return [];
    }

    public static function addCartB2b($roba_id,$quantity, $status, $partner_id){
        if(Session::has('b2b_cart') && !is_null(DB::table('web_b2b_korpa')->where(array('web_b2b_korpa_id'=>Session::get('b2b_cart'),'naruceno'=>0))->first())){
            $product = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',Session::get('b2b_cart'))->where('roba_id',$roba_id)->first();
            //update
            if($product){
                $data = array();
                if($status == 1){
                    $data = ['kolicina'=>$quantity];
                }
                else if($status==2){
                    $data = ['kolicina'=>$product->kolicina+$quantity];
                }
                else if($status==3) {
                    $data = ['kolicina'=>$product->kolicina-$quantity];
                }
                DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',Session::get('b2b_cart'))->where('roba_id',$roba_id)->update($data);
            }
            //insert
            else{
                $roba = DB::table('roba')->where('roba_id',$roba_id)->first();
                $rabatCene = B2bArticle::b2bRabatCene($roba_id);
                $data = [
                  'web_b2b_korpa_id'=>Session::get('b2b_cart'),
                  'broj_stavke'=>self::getNextB2bItem(Session::get('b2b_cart')),
                  'roba_id'=>$roba_id,
                  'kolicina'=>$quantity,
                  'jm_cena'=>$rabatCene->cena_sa_rabatom,
                  'tarifna_grupa_id'=>$roba->tarifna_grupa_id,
                  'racunska_cena_nc'=>$roba->racunska_cena_nc,
                  'racunska_cena_end'=>B2bOptions::gnrl_options(3028) == 1 ? $roba->web_cena/(1+(DB::table('tarifna_grupa')->where('tarifna_grupa_id',$roba->tarifna_grupa_id)->pluck('porez'))/100) : $roba->racunska_cena_end
                ];
                DB::table('web_b2b_korpa_stavka')->insert($data);
            }
        }
        else{

            $data = DB::table('web_b2b_korpa')->insert(['partner_id'=>$partner_id,'datum'=>date('Y-m-d H:i:s')]);
            $cart = DB::table('web_b2b_korpa')->where('partner_id',$partner_id)->where('naruceno',0)->orderBy('web_b2b_korpa_id','desc')->first();
            Session::put('b2b_cart',$cart->web_b2b_korpa_id);

            $roba = DB::table('roba')->where('roba_id',$roba_id)->first();
            $rabatCene = B2bArticle::b2bRabatCene($roba_id);

            $data = [
                'web_b2b_korpa_id'=>Session::get('b2b_cart'),
                'broj_stavke'=>self::getNextB2bItem(Session::get('b2b_cart')),
                'roba_id'=>$roba_id,
                'kolicina'=>$quantity,
                'jm_cena'=>$rabatCene->cena_sa_rabatom,
                'tarifna_grupa_id'=>$roba->tarifna_grupa_id,
                'racunska_cena_nc'=>$roba->racunska_cena_nc,
                  'racunska_cena_end'=>B2bOptions::gnrl_options(3028) == 1 ? $roba->web_cena/(1+(DB::table('tarifna_grupa')->where('tarifna_grupa_id',$roba->tarifna_grupa_id)->pluck('porez'))/100) : $roba->racunska_cena_end
            ];
            DB::table('web_b2b_korpa_stavka')->insert($data);
        }
    }
    public static function getB2bQuantityItem($roba_id){
        
        if(Session::has('b2b_cart')){
            $cart = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',Session::get('b2b_cart'))->where('roba_id',$roba_id)->first();
            if($cart){
                return $cart->kolicina;
            }
            else {
                return 0;
            }
        }
        else{
            return 0;
        }
    }
    public static function b2bCountItems(){
        if(Session::has('b2b_cart')){
            $cart = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',Session::get('b2b_cart'));
            return $cart->count();
        }
        else {
            return 0;
        }
    }
	public static function cena($cena,$valuta_show = true){
        $decimale = Options::web_options(209);

        $valuta = null;
        if(Session::has('b2b_valuta') && is_numeric(Session::get('b2b_valuta'))){
            $valuta = DB::table('valuta')->where('valuta_id',Session::get('b2b_valuta'))->first();
        }else{
            $valuta = DB::table('valuta')->where('izabran',1)->first();
            Session::put('b2b_valuta',$valuta->valuta_id);
        }

        $kurs=B2bOptions::kurs($valuta->valuta_id);
        $valutaLabel = ' <span> '.strtolower($valuta->valuta_sl).'.</span>';
        if($cena!="0"){
            $iznos = number_format(round(floatval($cena/$kurs),$decimale), 2, ',', '.');
        }else{
            $iznos = '0.00';
        }

        if($valuta_show == true){
            return $iznos.$valutaLabel;
        }else{
            return $iznos;
        }
    }
    public static function  getNextB2bItem($web_b2b_korpa_id){
        $b2bCart = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',$web_b2b_korpa_id)->count();

        return $b2bCart+1;

    }
   public static function total($avans=0.00){
       $sumOsnovnaCena = 0;
       $sumPdvCena = 0;
       $sumAvansCena = 0;
       $sumUkupnaCena = 0;

       $avans_popust = 1-($avans/100);
       $cart = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->get();
       foreach($cart as $row){
            $porez = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$row->tarifna_grupa_id)->pluck('porez');
            $rabatCene = B2bArticle::b2bRabatCene($row->roba_id);
            $sumOsnovnaCena += $rabatCene->cena_sa_rabatom*$row->kolicina;
            $sumPdvCena += $rabatCene->cena_sa_rabatom*($porez/100)*$row->kolicina;
            $sumAvansCena += $rabatCene->cena_sa_rabatom*(1+$porez/100)*($avans/100)*$row->kolicina;
            $sumUkupnaCena += $rabatCene->cena_sa_rabatom*(1+$porez/100)*$avans_popust*$row->kolicina;
       }
        return (object) array(
            'porez_cena' => $sumPdvCena,
            'cena_sa_rabatom' => $sumOsnovnaCena,
            'avans' => $sumAvansCena,
            'ukupna_cena' => $sumUkupnaCena
            );
   }

   public static function orderTotal($web_b2b_narudzbina_id){
       $sumOsnovnaCena = 0;
       $sumPdvCena = 0;
       $sumAvansCena = 0;
       $sumUkupnaCena = 0;

       $order = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', $web_b2b_narudzbina_id)->first();
       $avans_popust = 1-($order->avans/100);
       $order_stavke = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $web_b2b_narudzbina_id)->get();
       foreach($order_stavke as $row){
            $porez = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$row->tarifna_grupa_id)->pluck('porez');
            $rabatCene = B2bArticle::b2bRabatCene($row->roba_id);

            $sumOsnovnaCena += $rabatCene->cena_sa_rabatom*$row->kolicina;
            $sumPdvCena += $rabatCene->cena_sa_rabatom*($porez/100)*$row->kolicina;
            $sumAvansCena += $rabatCene->cena_sa_rabatom*(1+$porez/100)*($order->avans/100)*$row->kolicina;
            $sumUkupnaCena += $rabatCene->cena_sa_rabatom*(1+$porez/100)*$avans_popust*$row->kolicina;
       }

        return (object) array(
            'porez_cena' => $sumPdvCena,
            'cena_sa_rabatom' => $sumOsnovnaCena,
            'avans' => $sumAvansCena,
            'ukupna_cena' => $sumUkupnaCena
            );
   }
   public static function b2bCartStavkaCene($web_b2b_korpa_stavka_id){
        $stavka = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_stavka_id',$web_b2b_korpa_stavka_id)->first();
        $porez = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$stavka->tarifna_grupa_id)->pluck('porez');

        return (object) array(
            'osnovna_cena' => $stavka->racunska_cena_end,
            'ukupan_rabat' => $stavka->racunska_cena_end>0 ? ($stavka->racunska_cena_end-$stavka->jm_cena)*100/$stavka->racunska_cena_end : 0,
            'cena_sa_rabatom' => $stavka->jm_cena,
            'porez' => $porez,
            'ukupna_cena' => $stavka->jm_cena*(1+$porez/100)
            );
   }
   public static function b2bOrderStavkaCene($web_b2b_narudzbina_stavka_id){
        $stavka = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_stavka_id',$web_b2b_narudzbina_stavka_id)->first();
        $porez = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$stavka->tarifna_grupa_id)->pluck('porez');

        return (object) array(
            'osnovna_cena' => $stavka->racunska_cena_end,
            'ukupan_rabat' => $stavka->racunska_cena_end > 0 ? ($stavka->racunska_cena_end-$stavka->jm_cena)*100/$stavka->racunska_cena_end : 0,
            'cena_sa_rabatom' => $stavka->jm_cena,
            'porez' => $porez,
            'ukupna_cena' => $stavka->jm_cena*(1+$porez/100)
            );
   }
    public static function nacin_isporuke($nacin_isporuke_id){
        
        foreach(DB::table('web_nacin_isporuke')->where('selected',1)->orderBy('web_nacin_isporuke_id','asc')->get() as $row){
            
            echo '<option id="'.$row->web_nacin_isporuke_id.'" value="'.$row->web_nacin_isporuke_id.'" '.($row->web_nacin_isporuke_id==$nacin_isporuke_id ? 'selected' : '').'>'.$row->naziv.'</option> ';
        }
        
    }
   public static function b2bTaxPrice(){
       $sum = 0;
       $cart = DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id', Session::get('b2b_cart'))->get();

       foreach($cart as $row){
           $roba = DB::table('roba')->where('roba_id',$row->roba_id)->first();
           $rab_p = B2bArticle::b2bPartnerGroupRabat($roba->grupa_pr_id, Session::get('b2b_user_'.B2bOptions::server()));
           $priceDiscount = B2bArticle::procenat_m($row->jm_cena,$rab_p);
           $tax = B2bArticle::b2bTax($roba->tarifna_grupa_id);
           $sum += B2bArticle::procenat($priceDiscount, $tax)*$row->kolicina;
       }
       return $sum;

   }
    public static function nacin_placanja(){
        
         foreach(DB::table('web_nacin_placanja')->where('selected',1)->orderBy('b2b_default','desc')->get() as $row){
            
            echo '<option id="'.$row->web_nacin_placanja_id.'" value="'.$row->web_nacin_placanja_id.'">'.$row->naziv.'</option> ';
        }
    }
    public static function b2bItemPrice($roba_id, $basic_price){

        $roba = DB::table('roba')->where('roba_id',$roba_id)->first();
        $cartItem = DB::table('web_b2b_korpa_stavka')->where('roba_id',$roba_id)->where('web_b2b_korpa_id',Session::get('b2b_cart'))->first();
        $rab_p = B2bArticle::b2bPartnerGroupRabat($roba->grupa_pr_id, Session::get('b2b_user_'.B2bOptions::server()));
        $priceDiscount = B2bArticle::procenat_m($basic_price,$rab_p);
        $tax = B2bArticle::b2bTax($roba->tarifna_grupa_id);
        return B2bArticle::procenat_p($priceDiscount, $tax)*$cartItem->kolicina;


    }
    public static function getNameNacinIsporuke($web_nacin_isporuke_id){
        $ni = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$web_nacin_isporuke_id)->first();
        return $ni->naziv;
    }
    public static function getNameNacinPlacanja($web_nacin_placanja_id){
        $np = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$web_nacin_placanja_id)->first();
        return $np->naziv;
    }
    public static function mesto($mesto_id){
        return DB::table('mesto')->where('mesto_id',$mesto_id)->pluck('mesto');
    }
    public static function b2bOrderBasicPrice($order_id){
        $sum = 0;
        $cart = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $order_id)->get();
        foreach($cart as $row){
            $porez = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$row->tarifna_grupa_id)->pluck('porez');
            $cena_sa_rabatom = $row->jm_cena / (1+$porez/100);

            $sum += $cena_sa_rabatom*$row->kolicina;
        }
        return $sum;
    }
    public static function b2bOrderTaxPrice($order_id){
        $sum_ukupna_cena = 0;
        $sum_bez_poreza = 0;
        $cart = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $order_id)->get();

        foreach($cart as $row){
            $porez = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$row->tarifna_grupa_id)->pluck('porez');
            $cena_sa_rabatom = $row->jm_cena / (1+$porez/100);

            $sum_ukupna_cena += $row->jm_cena*$row->kolicina;
            $sum_bez_poreza += $cena_sa_rabatom*$row->kolicina;
        }
        return ($sum_ukupna_cena - $sum_bez_poreza);

    }

    public static function rezervacija($cartItems){
        if(B2bOptions::web_options(131)==1){
            foreach($cartItems as $item){
                $lager = DB::table('lager')->where('roba_id',$item['roba_id'])->where('orgj_id',function($q){
                    $q->from('imenik_magacin')->where('imenik_magacin_id',20)->limit(1)->pluck('orgj_id');
                })->where('poslovna_godina_id',function($q){
                    $q->from('poslovna_godina')->where('status',0)->limit(1)->pluck('poslovna_godina_id');
                })->first();
                
                if(!is_null($lager)){
                    $rezervisano = $lager->rezervisano + $item['kolicina'];

                    DB::table('lager')->where('roba_id',$item['roba_id'])->where('orgj_id',function($q){
                        $q->from('imenik_magacin')->where('imenik_magacin_id',20)->limit(1)->pluck('orgj_id');
                    })->where('poslovna_godina_id',function($q){
                        $q->from('poslovna_godina')->where('status',0)->limit(1)->pluck('poslovna_godina_id');
                    })->update(array('rezervisano'=>$rezervisano));
                }          
            }
        }
    }

    public static function troskovi($web_b2b_narudzbina_id=null,$roba_id=null){
        $tezinaSum=0;
        $cenaSum=0;
        if(is_null($roba_id)){
            if(!is_null($web_b2b_narudzbina_id)){
                foreach(DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row){
                    $tezinaSum+=DB::table('roba')->where('roba_id',$row->roba_id)->pluck('tezinski_faktor')*$row->kolicina;
                    $cenaSum+= B2bArticle::b2bRabatCene($row->roba_id)->cena_sa_rabatom*$row->kolicina;
                }
            }else if(Session::has('b2b_cart')){
                foreach(DB::table('web_b2b_korpa_stavka')->where('web_b2b_korpa_id',Session::get('b2b_cart'))->get() as $row){
                    $tezinaSum+=DB::table('roba')->where('roba_id',$row->roba_id)->pluck('tezinski_faktor')*$row->kolicina;
                    $cenaSum+= B2bArticle::b2bRabatCene($row->roba_id)->cena_sa_rabatom*$row->kolicina;
                }
            }
        }else{
            $tezinaSum+=DB::table('roba')->where('roba_id',$roba_id)->pluck('tezinski_faktor');
            $cenaSum+=B2bArticle::b2bRabatCene($row->roba_id)->cena_sa_rabatom;
        }

        $troskoviSum = 0;
        $troskoviTezinaObj = (object) array('cena'=>0,'tezina_gr'=>0,'cena_do'=>0);
        $troskoviCenaObj = (object) array('cena'=>0,'tezina_gr'=>0,'cena_do'=>0);

        foreach(DB::table('web_troskovi_isporuke')->orderBy('tezina_gr','desc')->get() as $row) {
            if($tezinaSum >= $row->tezina_gr){
                $troskoviTezinaObj = $row;
                break;
            }
        }

        foreach(DB::table('web_troskovi_isporuke')->orderBy('cena_do','desc')->get() as $row) {
            if($cenaSum < $row->cena_do){
                $troskoviCenaObj = $row;
            }
        }
        
        //samo tezina
        if(AdminOptions::web_options(133)==1 && AdminOptions::web_options(150)==0){
            $troskoviSum = $troskoviTezinaObj->cena;
        //samo cena
        }else if(AdminOptions::web_options(150)==1 && AdminOptions::web_options(133)==0){
            $troskoviSum = $troskoviCenaObj->cena;
        //kombinacija cene i tezine
        }else if(AdminOptions::web_options(150)==1 && AdminOptions::web_options(133)==1){
            if($troskoviTezinaObj->cena == $troskoviCenaObj->cena){
                $troskoviSum = $troskoviTezinaObj->cena;
            }else{
                $troskoviSum = max(array($troskoviTezinaObj->cena,$troskoviCenaObj->cena));
                // if($troskoviTezinaObj->tezina_gr < $troskoviCenaObj->tezina_gr || $troskoviCenaObj->cena_do < $troskoviTezinaObj->cena_do){
                //     $troskoviSum = min(array($troskoviTezinaObj->cena,$troskoviCenaObj->cena));
                // }else{
                //     $troskoviSum = max(array($troskoviTezinaObj->cena,$troskoviCenaObj->cena));
                // }
            }
        }

        return $troskoviSum;
    }

    public static function cena_dostave(){
        $cena_isporuke = DB::table('cena_isporuke')->first();
        return !is_null($cena_isporuke) ? $cena_isporuke->cena : 0;
    }
    public static function cena_do(){
        $cena_isporuke = DB::table('cena_isporuke')->first();
        return !is_null($cena_isporuke) ? $cena_isporuke->cena_do : 0;
    }

    public static function getNew() {
            $ukupno = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('realizovano', 0)->where('prihvaceno', 0)->where('stornirano', '!=', 1)->count();
        return $ukupno;
    }

}