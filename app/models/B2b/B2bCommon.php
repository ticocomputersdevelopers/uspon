<?php 
use Service\Mailer;

class B2bCommon {

	public static function get_page_start(){
		
		return 'pocetna';
	}
	public static function getGrupa($grupa_pr_id){
		$grupa = DB::table('grupa_pr')->where('grupa_pr_id', $grupa_pr_id)->pluck('grupa');
		return $grupa;
	}
	public static function seo_title($strana){
		return DB::table('web_b2b_seo')->where('naziv_stranice',$strana)->pluck('title');
	}
	public static function seo_description($strana){
		return DB::table('web_b2b_seo')->where('naziv_stranice',$strana)->pluck('description');
	}
	public static function seo_keywords($strana){
		return DB::table('web_b2b_seo')->where('naziv_stranice',$strana)->pluck('keywords');
	}
	public static function get_korpa(){
		return 'korpa';
	}

	public static function allGroups(&$niz,$grupa_pr_id)
	{		
		$niz[]=$grupa_pr_id;
		$check_parent=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$grupa_pr_id, 'web_b2b_prikazi'=>1))->get();
		if (count($check_parent)>0)
		{
			foreach ($check_parent as $row)
			{
				Groups::allGroups($niz,$row->grupa_pr_id);
			}				
		}
	}

    public static function selectGroups($grupa_pr_id=null){
        $render = '
        <option value="0" '.(($grupa_pr_id == 0 || is_null($grupa_pr_id) || $grupa_pr_id == 'null') ? 'selected' : '').'>Izaberite grupu</option>';
        foreach(self::getLevelGroups(0,1) as $row_gr){
            if($row_gr->grupa_pr_id == $grupa_pr_id){
                $render .= '<option value="'.$row_gr->grupa_pr_id.'" selected>'.$row_gr->grupa.'</option>';
            }else{
                $render .= '<option value="'.$row_gr->grupa_pr_id.'">'.$row_gr->grupa.'</option>';
            }
            foreach(self::getLevelGroups($row_gr->grupa_pr_id,1) as $row_gr1){
                if($row_gr1->grupa_pr_id == $grupa_pr_id){
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" selected >&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.'</option>';
                }else{
                    $render .= '<option value="'.$row_gr1->grupa_pr_id.'" >&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr1->grupa.'</option>';
                }
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id,1) as $row_gr2){
                    if($row_gr2->grupa_pr_id == $grupa_pr_id){
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" selected >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.'</option>';
                    }else{
                        $render .= '<option value="'.$row_gr2->grupa_pr_id.'" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr2->grupa.'</option>';
                    }
                    foreach(self::getLevelGroups($row_gr2->grupa_pr_id,1) as $row_gr3){
                        if($row_gr3->grupa_pr_id == $grupa_pr_id){
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'" selected>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.'</option>';
                        }else{
                            $render .= '<option value="'.$row_gr3->grupa_pr_id.'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$row_gr3->grupa.'</option>';
                        }
                    }
                }
            }
        }
        return $render;
    }

    public static function getLevelGroups($grupa_pr_id,$web_b2b_prikazi=null){
        $query = DB::table('grupa_pr')->select('grupa_pr_id', 'grupa', 'web_b2b_prikazi', 'parrent_grupa_pr_id')->where('parrent_grupa_pr_id',$grupa_pr_id);
    	if(!is_null($web_b2b_prikazi)){
    		$query = $query->where('web_b2b_prikazi',$web_b2b_prikazi);
    	}
    	return $query->orderBy('redni_broj', 'asc')->get();
    }
	public static function allB2bGroups(&$niz,$grupa_pr_id)
	{		
		$niz[]=$grupa_pr_id;
		$check_parent=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$grupa_pr_id, 'web_b2b_prikazi'=>1))->get();
		if (count($check_parent)>0)
		{
			foreach ($check_parent as $row)
			{
				self::allB2bGroups($niz,$row->grupa_pr_id);
			}				
		}
	}

	public static function broj_cerki($grupa_pr_id){
		
		return DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$grupa_pr_id,'web_b2b_prikazi'=>1))->count();
			
	}
	public static function cerke($grupa_pr_id){

			$niz2=array();
				$niz="";
				$br=0;
				$query_roba=DB::table('grupa_pr')->where(array('web_b2c_prikazi'=>1,'parrent_grupa_pr_id'=>$grupa_pr_id));
				
				foreach ($query_roba->get() as $key) {
							
							$grupa_pr_id_c=$key->grupa_pr_id;
							
								 if($br==0){
									$niz.=$grupa_pr_id_c; 
								 }	
								 else {
									 $niz.=",".$grupa_pr_id_c;
								 }
								 $br+=1;
								}
									
							$niz2=explode(',',$niz);	
					return $niz2;	
						}	

	public static function cerke2($grupa_pr_id){

		
				$niz="";
				
				$query_roba=DB::table('grupa_pr')->where(array('web_b2c_prikazi'=>1,'parrent_grupa_pr_id'=>$grupa_pr_id));
				
				foreach ($query_roba->get() as $key) {
							
							$grupa_pr_id_c=$key->grupa_pr_id;							
								 
									 $niz.=$grupa_pr_id_c.',';								 
								 
								}
								$niz.=0;	
							$niz2=explode(',',$niz);	
					return $niz;	
						
      }

	public static function grupa_title($grupa_pr_id){
		
		return DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->pluck('grupa');
		
	}
	public static function grupa_description($grupa_pr_id){
			$opis = DB::table('grupa_pr')->where('grupa_pr_id', $grupa_pr_id)->pluck('opis');
			if(isset($opis)){
				return $opis;
			}
			return self::grupa_title($grupa_pr_id);			
	}
	public static function grupa_keywods($grupa_pr_id){
		 $keywords = DB::table('grupa_pr')->where('grupa_pr_id', $grupa_pr_id)->pluck('keywords');
		 if(isset($keywords)){
				return $keywords;
			}
		return self::grupa_title($grupa_pr_id);	
		 
	}

 public static function ip_adress(){
		$ip;
		if(getenv("HTTP_CLIENT_IP")){
			$ip = getenv("HTTP_CLIENT_IP");
		}
		else if(getenv("HTTP_X_FORWARDED_FOR")){
			$ip = getenv("HTTP_X_FORWARDED_FOR");
		}
		else if(getenv("REMOTE_ADDR")){
			$ip = getenv("REMOTE_ADDR");
		}
		else{
			$ip = "UNKNOWN";
		}
		return $ip;
	}

      public static function send_email_to_client($body,$email,$subject){
        
		if(B2bOptions::gnrl_options(3003)){        

             Mail::send('email.order', array('body'=>$body), function($message) use ($email, $subject)
            {
                $message->from(EmailOption::mail_from(), EmailOption::name());

                $message->to($email)->subject($subject);
            });
        }else{

			Mailer::send(B2bOptions::company_email(),$email, $subject, $body);    
		}

      }
       public static function send_email_to_admin($body,$email,$name,$subject){
        
        if(B2bOptions::gnrl_options(3003)){
         
             Mail::send('email.order', array('body'=>$body), function($message) use ($email, $name, $subject)
            {
                $message->from($email, $name);

                $message->to(EmailOption::mail_from())->subject($subject);
            });

        }else{
			Mailer::send(B2bOptions::company_email(),B2bOptions::company_email(), $subject, $body);
		}
      }
	public static function check_image_grupa($putanja){
		$check = false;
		if($putanja != null && $putanja != ""){
			if(file_exists($putanja)){
				$check = true;
			}
		}
		return $check;	
	}

	public static function brojiArtikleRekurzivno($grupa_pr_id)
	{
		$result = '';
		if(B2bOptions::gnrl_options(1306)){
			$result = self::artikli_count($grupa_pr_id);
		}
		return $result;

	}

	public static function artikli_count($grupa_pr_id){

		$gr=self::vratiSveGrupe($grupa_pr_id);
		$queryOsnovni=DB::table('roba')
                ->select([DB::RAW('DISTINCT(roba.roba_id)')])
               	->leftJoin('roba_grupe', 'roba_grupe.roba_id','=','roba.roba_id');

		if(self::checkImage() != ''){
			$queryOsnovni->leftJoin('web_slika', 'roba.roba_id','=','web_slika.roba_id');
		} 
     
        $queryOsnovni->where(function($queryOsnovni) use ($gr){
                	$queryOsnovni->whereIn('roba.grupa_pr_id', $gr)
                	      ->orWhereIn('roba_grupe.grupa_pr_id', $gr);
                })
				->where(array('roba.flag_aktivan'=>1, 'roba.flag_cenovnik'=>1));

		if(self::checkPrice() != ''){
			$queryOsnovni->where('web_cena','>',0);
		}
		if(self::checkImage() != ''){
			$queryOsnovni->whereNotNull('web_slika.roba_id');
		}
		if(self::checkDescription() != ''){
			$queryOsnovni->whereNotNull('web_opis');
		}

		return count($queryOsnovni->get());					
	}
	public static function vratiSveGrupe($grupa_pr_id)
	{
		$grupe=self::sveGrupe($grupa_pr_id);		
		$grupe=trim($grupe, ',');
		$grupe=explode(',',$grupe);
		$grupe=array_filter($grupe);
		return $grupe;	

	}
	public static function sveGrupe($grupa_pr_id)
	{		
		$niz=$grupa_pr_id.',';
		$svaDeca=self::deca($grupa_pr_id);
		if (count($svaDeca)>0)
		{
			foreach ($svaDeca as $row)
			{
				$niz.=self::sveGrupe($row->grupa_pr_id);
			}				

		}
		return $niz;
	}
	public static function deca($grupa_pr_id){
		
		return DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$grupa_pr_id,'web_b2c_prikazi'=>1))->get();
			
	}
	public static function get_kontakt(){
		
		foreach(DB::table('web_b2c_seo')->where('web_b2c_seo_id',3)->get() as $row){
			return $row->naziv_stranice;
			
		}
		
	}
	public static function menu_footer(){
		return DB::table('web_b2b_seo')->where('b2b_footer',1)->orderBy('rb_strane','asc')->get();
	}
	public static function akcija($grupa_pr_id=null,$limit=null,$page=null,$sort_column='b2b_akcija_redni_broj',$sort_direct='ASC'){
		$datum = date('Y-m-d');
		$partner_id=Session::get('b2b_user_'.B2bOptions::server());
		$query = "SELECT DISTINCT r.roba_id, akcija_redni_broj, naziv_web, b2b_akcija_redni_broj FROM roba r".AdminArticles::checkImage('join').AdminArticles::checkCharacteristics('join')." WHERE r.proizvodjac_id NOT IN (SELECT proizvodjac_id FROM nivo_pristupa_proizvodjac WHERE partner_id = ".$partner_id." ) AND r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND r.b2b_akcija_flag_primeni = 1 AND CASE WHEN r.b2b_datum_akcije_od IS NOT NULL AND r.b2b_datum_akcije_do IS NOT NULL THEN r.b2b_datum_akcije_od <= '".$datum."' AND r.b2b_datum_akcije_do >= '".$datum."' 
		WHEN r.b2b_datum_akcije_od IS NOT NULL AND r.b2b_datum_akcije_do IS NULL THEN r.b2b_datum_akcije_od <= '".$datum."' 
		WHEN datum_akcije_od IS NULL AND r.b2b_datum_akcije_do IS NOT NULL THEN r.b2b_datum_akcije_do >= '".$datum."' 
		ELSE 1 = 1 END ".AdminArticles::checkImage().AdminArticles::checkPrice().AdminArticles::checkDescription().AdminArticles::checkCharacteristics()."";

		if(isset($grupa_pr_id)){
			$grupe = self::vratiSveGrupe($grupa_pr_id);
			$query .= " AND grupa_pr_id IN (".implode(',',$grupe).")";
		}

		$query .= " ORDER BY ".$sort_column." ".$sort_direct;		
		if(isset($page) && isset($limit)){
			$page = ($page-1)*$limit;
			$query .= " LIMIT ".$limit." OFFSET ".$page."";
		}

		return $array_tip = DB::select($query);
	}	

	public static function provera_akcija($roba_id){
		$datum = date('Y-m-d');

		$query_akcija=DB::select("SELECT DISTINCT r.roba_id FROM roba r".AdminArticles::checkImage('join').AdminArticles::checkCharacteristics('join')." WHERE r.roba_id = ".$roba_id." AND r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND r.b2b_akcija_flag_primeni = 1 and (b2b_datum_akcije_od is null or b2b_datum_akcije_od <= '".$datum."') AND (b2b_datum_akcije_do is null or b2b_datum_akcije_do >= '".$datum."') ".AdminArticles::checkImage().AdminArticles::checkPrice().AdminArticles::checkDescription().AdminArticles::checkCharacteristics()."");

			if(count($query_akcija) > 0){
				return 1;
			}else{
				return 0;
			}
	}

	public static function akcija_count($grupa_pr_id=null,$limit=null,$offset=null){
		return count(self::akcija($grupa_pr_id,$limit,$offset));
	}

	public static function menu_top_items(){
		return DB::table('web_b2b_seo')->where('b2b_header',1)->orderBy('rb_strane','asc')->get();
	}
	public static function articleViewB2B($roba_id){
		return DB::table('roba')->where('roba_id', $roba_id)->increment('pregledan_puta_b2b');
	}
	public static function mostPopularArticlesB2B($limit=4){
		$partner_id=Session::get('b2b_user_'.B2bOptions::server());
        $popular=DB::select("SELECT DISTINCT r.roba_id, r.pregledan_puta_b2b FROM roba r".AdminOptions::checkImage('join').AdminOptions::checkCharacteristics('join')." WHERE r.proizvodjac_id NOT IN (SELECT proizvodjac_id FROM nivo_pristupa_proizvodjac WHERE partner_id = ".$partner_id." ) AND r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND r.roba_id <> -1 AND ((SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) > 0)".AdminOptions::checkImage().AdminOptions::checkPrice().AdminOptions::checkDescription().AdminOptions::checkCharacteristics()." ORDER BY r.pregledan_puta_b2b DESC LIMIT ".$limit." ");

        return $popular;
    }
    public static function bestSellerB2B($limit=4){
    	$partner_id=Session::get('b2b_user_'.B2bOptions::server());
		$roba_ids_string = '';
		$roba_ids = DB::select("SELECT DISTINCT r.roba_id FROM roba r".Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.proizvodjac_id NOT IN (SELECT proizvodjac_id FROM nivo_pristupa_proizvodjac WHERE partner_id = ".$partner_id." ) AND r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND r.roba_id <> -1".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."");
		if(count($roba_ids) > 0){
			$roba_ids_string = 'WHERE roba_id IN ('.implode(',',array_map('current',$roba_ids)).')';
		}
		if($roba_ids_string == ''){
			return (object) array();
		}else{
			return DB::select('
				SELECT roba_id, SUM(kolicina) as count 
				FROM web_b2b_narudzbina_stavka ns LEFT JOIN web_b2b_narudzbina n 
				ON n.web_b2b_narudzbina_id = ns.web_b2b_narudzbina_id 
				'.$roba_ids_string.' AND realizovano = 1 AND stornirano != 1
				GROUP BY roba_id 
				ORDER BY count 
				DESC LIMIT '.$limit.'');
		}
	}
	public static function latestAdded($limit=4){
		$partner_id=Session::get('b2b_user_'.B2bOptions::server());
		// DB::table('roba')->where('flag_aktivan', 1)->where('flag_cenovnik', 1)->orderBy('roba_id', 'DSC')->limit(5)->get();
		$latest = DB::select("SELECT DISTINCT r.roba_id FROM roba r".Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.proizvodjac_id NOT IN (SELECT proizvodjac_id FROM nivo_pristupa_proizvodjac WHERE partner_id = ".$partner_id." ) AND r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND r.roba_id <> -1".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()." ORDER BY r.roba_id DESC LIMIT ".$limit."");
		return $latest;
	}
	public static function artikli_b2b_tip($tip,$limit=null,$offset=null){
		$partner_id=Session::get('b2b_user_'.B2bOptions::server());
		$query = "SELECT DISTINCT r.roba_id FROM roba r".Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.proizvodjac_id NOT IN (SELECT proizvodjac_id FROM nivo_pristupa_proizvodjac WHERE partner_id = ".$partner_id." ) AND r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND tip_cene = ".$tip." ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()." ORDER BY r.roba_id ASC"; // ORDER BY r.roba_id ASC

		if(isset($offset) && isset($limit)){
			$offset = ($offset-1)*$limit;
			$query .= " LIMIT ".$limit." OFFSET ".$offset."";
		}elseif(isset($limit)){
			$query .= " LIMIT ".$limit."";
		}

        return $array_tip = DB::select($query);
	}
	public static function get_title_page_start(){
		return 'Početna';
	}
    public static function get_valuta(){
		if(Session::has('valuta') && is_numeric(Session::get('valuta'))){
			return DB::table('valuta')->where('valuta_id',Session::get('valuta'))->pluck('valuta_sl');
		}else{
			return DB::table('valuta')->where('izabran',1)->pluck('valuta_sl');
		}
    }
        public static function get_sort(){
        	if(Session::has('order')){
        		$sessija=Session::get('order');
        		if($sessija=='price_asc'){
        			return "Cena - Rastuće";
        		}
        		else if($sessija=='price_desc'){
        			return "Cena - Opadajuće";
        		}
        		else if($sessija=='news'){
        			return "Najnovije";
        		}
        		else if($sessija=='name'){
        			return "Prema nazivu";
        		}
        	}
        	else {
        		return B2bOptions::web_options(207) == 0 ? "Cena - Rastuće" : "Cena - Opadajuće";
        	}
        }    

	public static function checkImage($join=null){
		if(B2bOptions::gnrl_options(1308)){
			return '';
		}else{
			$result = 'AND ws.roba_id IS NOT NULL ';
			if($join == 'join'){
				$result = ' LEFT JOIN web_slika ws ON r.roba_id = ws.roba_id';
			}
			return $result;
		}
	}
	public static function checkPrice(){
		if(B2bOptions::gnrl_options(1307)){
			return '';
		}else{
			return 'AND r.web_cena > 0 ';
		}
	}
	public static function checkDescription(){
		if(B2bOptions::gnrl_options(1305)){
			return '';
		}else{
			return 'AND r.web_opis IS NOT NULL ';
		}
	}
	public static function checkCharacteristics($join=null){
		if(B2bOptions::gnrl_options(1306)){
			return '';
		}else{
			$result = 'AND (r.web_karakteristike IS NOT NULL OR wrk.roba_id IS NOT NULL OR dck.roba_id IS NOT NULL) ';
			if($join == 'join'){
				$result = ' LEFT JOIN web_roba_karakteristike wrk ON r.roba_id = wrk.roba_id LEFT JOIN dobavljac_cenovnik_karakteristike dck ON r.roba_id = dck.roba_id';
			}
			return $result;
		}
	}

	public static function grupa_level($grupa_pr_id){
		$query_grupa_level1=DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->pluck('parrent_grupa_pr_id');

		if($query_grupa_level1==0){
			return 1;
		}
		else {	
				$query_grupa_level2=DB::table('grupa_pr')->where('grupa_pr_id',$query_grupa_level1)->pluck('parrent_grupa_pr_id');

				if($query_grupa_level2==0)
					{ return 2;}
				else {
					return 3;
				}


		}
	}

	public static function check_ewe($roba_id){
	
		$get_ewe_id=DB::table('partner')->where('naziv', 'ILIKE', "%ewe%")->pluck('partner_id');
		$query_dc=DB::table('dobavljac_cenovnik')->where('partner_id',$get_ewe_id)->where('roba_id',$roba_id);
		if($query_dc->count()==1){
			return 1;
		}
		else {
			return 0;
		}
		
	}
	public static function user_password($length = 10){
    	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$randomString = '';
    	for ($i = 0; $i < $length; $i++) {
        	$randomString .= $characters[rand(0, strlen($characters) - 1)];
    	}
    	return $randomString;
	}

	public static function get_filter_array($string){
		$str_arr = explode('-',$string);
		$key = array_search('', $str_arr);
		if($key !== false){
			$str_arr[$key+1] = '-'.$str_arr[$key+1];
			unset($str_arr[$key]);
		}
		$str_arr = array_values($str_arr);
		return $str_arr;
	}

    public static function get_manofacture_name($proizvodjac_id){
    	return DB::table("proizvodjac")->where('proizvodjac_id',$proizvodjac_id)->pluck('naziv');
    }

    public static function manufacturer_categories($proizvodjac_id, $check=null)
    {
        $query=DB::select("SELECT r.grupa_pr_id, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id) as grupa FROM roba r WHERE r.proizvodjac_id = ".$proizvodjac_id." AND r.flag_aktivan = 1 AND r.flag_cenovnik = 1 ORDER BY grupa_pr_id ASC");

        // $query = DB::table('roba')->select('grupa_pr.grupa_pr_id','grupa_pr.grupa')->join('grupa_pr', 'roba.grupa_pr_id', '=', 'grupa_pr.grupa_pr_id')->where('roba.proizvodjac_id', $proizvodjac_id)->where(array('flag_aktivan' => 1,'flag_cenovnik' => 1))->orderBy('grupa_pr.grupa_pr_id', 'asc')->get();
        $categories=array();

        if($check == 1){
            foreach($query as $row){
                $categories[]=$row->grupa_pr_id;
            }
            return array_unique($categories);           
        }
        
        foreach($query as $row){
            $categories[]=$row->grupa;
        }
        return array_count_values($categories);

    }

    public static function type_categories($tip_id, $check=null)
    {
        $query=DB::select("SELECT r.grupa_pr_id, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id) as grupa FROM roba r WHERE r.tip_cene = ".$tip_id." AND r.flag_aktivan = 1 AND r.flag_cenovnik = 1 ORDER BY grupa_pr_id ASC");

        // $query = DB::table('roba')->select('grupa_pr.grupa_pr_id','grupa_pr.grupa')->join('grupa_pr', 'roba.grupa_pr_id', '=', 'grupa_pr.grupa_pr_id')->where('roba.proizvodjac_id', $proizvodjac_id)->where(array('flag_aktivan' => 1,'flag_cenovnik' => 1))->orderBy('grupa_pr.grupa_pr_id', 'asc')->get();
        $categories=array();

        if($check == 1){
            foreach($query as $row){
                $categories[]=$row->grupa_pr_id;
            }
            return array_unique($categories);           
        }
        
        foreach($query as $row){
            $categories[]=$row->grupa;
        }
        return array_count_values($categories);

    }

	public static function orderStatus($order){
		if($order->stornirano == 1){
			return 'Stornirana';
		}
		if($order->realizovano == 1){
			return 'Realizovana';
		}
		if($order->prihvaceno == 1){
			return 'Prihvaćena';
		}
		return 'Nova';
	}

	public static function xml_node($xml,$name,$value,$parent){
		$xmltag = $xml->createElement($name);
	    $xmltagText = $xml->createTextNode($value);
	    $xmltag->appendChild($xmltagText);
	    $parent->appendChild($xmltag);
	}
	public static function menu_top_pages(){
		$query = DB::table('web_b2b_seo')->where('menu_top',1);
			$query = $query->where('naziv_stranice','!=','prijava');
		return $query->orderBy('rb_strane','asc')->get();
	}
	public static function header_menu_pages($parrent_id=0){
		return DB::table('web_b2b_seo')->where(array('parent_id'=>$parrent_id,'header_menu'=>1))->orderBy('rb_strane','asc')->get();
	}
	public static function stranice_count(){
		return DB::table('web_b2b_seo')->where('menu_top',1)->count();
	}
	public static function count_childs_pages($web_b2b_seo_id){
		return DB::table('web_b2b_seo')->where(array('parent_id'=>$web_b2b_seo_id))->count();
	}

    public static function getLoggedAdminUser($role=null){
        if(!Session::has('b2c_admin'.AdminOptions::server())){
            return null;
        }

        if(is_null($role)){
            return DB::table('imenik')->where(array('flag_aktivan'=>1, 'imenik_id'=>Session::get('b2c_admin'.AdminOptions::server())))->first();
        }

        if($role=='ADMIN'){
            return DB::table('imenik')->where(array('kvota'=>0.00,'flag_aktivan'=>1, 'imenik_id'=>Session::get('b2c_admin'.AdminOptions::server())))->first();
        }else if($role=='KOMERCIJALISTA'){
            return DB::table('imenik')->where(array('kvota'=>32.00,'flag_aktivan'=>1, 'imenik_id'=>Session::get('b2c_admin'.AdminOptions::server())))->first();
        }
        return null;
    }
    // SECTIONS AND SLIDERS
    public static function pageSectionContent($page_section_id,$column='sadrzaj'){
        return DB::table('b2b_sekcija_stranice_jezik')->where(['b2b_sekcija_stranice_id'=>$page_section_id,'jezik_id'=>Language::lang_id()])->pluck($column);
    }
    public static function getShortListNews($ignore_id=0) {
		$news = DB::table('web_vest_b2b')->join('web_vest_b2b_jezik','web_vest_b2b_jezik.web_vest_b2b_id','=','web_vest_b2b.web_vest_b2b_id')->select('*','web_vest_b2b_jezik.sadrzaj as tekst')->where(array('aktuelno'=>1,'jezik_id'=>Language::lang_id()))->where('web_vest_b2b.web_vest_b2b_id','<>',$ignore_id)->orderBy('rbr','asc')->limit(6)->get();
		return $news;
	}
	public static function slajder($b2b_slajder_id){
        return DB::table('b2b_slajder')->select('b2b_slajder.*','b2b_slajder_tip.naziv as b2b_slajder_tip_naziv')->join('b2b_slajder_tip','b2b_slajder.b2b_slajder_tip_id','=','b2b_slajder_tip.b2b_slajder_tip_id')->where(['b2b_slajder.flag_aktivan'=>1, 'b2b_slajder_id'=>$b2b_slajder_id])->first();
    }

    public static function slajderStavke($b2b_slajder_id){
        $now = date('Y-m-d');
        return DB::table('b2b_slajder_stavka')->leftJoin('b2b_slajder_stavka_jezik','b2b_slajder_stavka.b2b_slajder_stavka_id','=','b2b_slajder_stavka_jezik.b2b_slajder_stavka_id')
        ->where(['flag_aktivan'=>1, 'b2b_slajder_id'=>$b2b_slajder_id])->where(function($q){ $q->where(['jezik_id'=>Language::lang_id()])->orWhereNull('jezik_id'); })
        ->where(function($q) use ($now){ $q->where('datum_od','<',$now)->orWhereNull('datum_od'); })
        ->where(function($q) use ($now){ $q->where('datum_do','>=',$now)->orWhereNull('datum_do'); })
        ->orderBy('rbr','asc')->get();
    }

    public static function getFirstSlider($tip='Slajder'){
        return DB::table('b2b_slajder')->where(['flag_aktivan'=>1, 'b2b_slajder_tip_id'=>DB::table('b2b_slajder_tip')->where('naziv',$tip)->pluck('b2b_slajder_tip_id')])->first();
    }
    public static function fileExtension($image_path){
        $extension = 'jpg';
        $slash_parts = explode('/',$image_path);
        $old_name = $slash_parts[count($slash_parts)-1];
        $old_name_arr = explode('.',$old_name);
        $extension = $old_name_arr[count($old_name_arr)-1];
        return $extension;
    }

    public static function getPartnerID(){
		$partner_id=Session::get('b2b_user_'.B2bOptions::server());
    }
    public static function getPartnerLocation0(){
		$partner_id=Session::get('b2b_user_'.B2bOptions::server());
    	$adresa = DB::table('partner')->where('partner_id', $partner_id)->pluck('adresa');
    	$mesto = DB::table('partner')->where('partner_id', $partner_id)->pluck('mesto');
    	$location = $adresa.' - '.$mesto;
    	return $location;
    }
	public static function getPartnerLocation($selected = null){
		
		$string = '<option class="JSdefaultPick" value="'.self::getPartnerLocation0().'" selected>'.self::getPartnerLocation0().'</option>';
		foreach(DB::table('partner_lokacija')->where('partner_id',Session::get('b2b_user_'.B2bOptions::server()))->orderBy('adresa','asc')->get() as $row){ 				
			if($selected == $row->adresa){
				$string .= '<option value="'.pg_escape_string($row->adresa).'" selected>'.$row->adresa.'</option>';
			}else{
				$string .= '<option value="'.pg_escape_string($row->adresa).'">'.$row->adresa.'</option>';
			}
		}
		echo $string;
	}
}