<?php

class AdminGarancije {

	public static function getGarancije($limit,$page=1,$search=null,$datum_od=null,$datum_do=null,$sort_column=null,$sort_direction=null){

		$query = " FROM produzene_garancije pg LEFT JOIN roba r ON pg.roba_id = r.roba_id LEFT JOIN grupa_pr g ON r.grupa_pr_id = g.grupa_pr_id LEFT JOIN proizvodjac p ON pg.proizvodjac_id = p.proizvodjac_id";

		$is_where = false;
		if(count(DB::select("SELECT DISTINCT parent_id FROM produzene_garancije WHERE parent_id IS NOT NULL")) > 0){
			$query .= " WHERE produzene_garancije_id NOT IN (SELECT DISTINCT parent_id FROM produzene_garancije WHERE parent_id IS NOT NULL)";
			$is_where = true;
		}

		if(!is_null($search)){
			if(!$is_where){
				$query .= " WHERE";
			}
			foreach(explode(' ',$search) as $search){
				$query .= " AND (";
				if(is_numeric($search)){
					$query .= "produzene_garancije_id = ".$search." OR ";
				}
				$query .= "ime ILIKE '%".$search."%'
					OR prezime ILIKE '%".$search."%'
					OR adresa ILIKE '%".$search."%'
					OR grad ILIKE '%".$search."%'
					OR maloprodaja ILIKE '%".$search."%'
					OR p.naziv ILIKE '%".$search."%'
					OR grupa ILIKE '%".$search."%'
					OR naziv_web ILIKE '%".$search."%'
					OR serijski_broj ILIKE '%".$search."%'
					OR fiskalni ILIKE '%".$search."%')";
			}
		}
		if(!is_null($datum_od)){
			if(!$is_where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " DATE(pg.datum_kreiranja) > '".$datum_od."'";
		}
		if(!is_null($datum_do)){
			if(!$is_where){
				$query .= " WHERE";
				$where = true;
			}else{
				$query .= " AND";
			}
			$query .= " DATE(pg.datum_kreiranja) < '".$datum_do."'";
		}

		$count = DB::select("SELECT COUNT(*) as count".$query)[0]->count;

		if(!is_null($sort_column) && !is_null($sort_direction)){
			$query .= " ORDER BY ".strtolower($sort_column)." ".strtoupper($sort_direction)."";
		}else{
			$query .= " ORDER BY final_parent_created DESC";
		}

        $query = "SELECT DISTINCT pg.*, g.grupa, r.naziv_web, ROW_NUMBER() OVER(ORDER BY produzene_garancije_id DESC) AS row, p.naziv AS proizvodjac, CASE WHEN parent_id IS NULL THEN datum_kreiranja ELSE get_final_parent_date_created(parent_id) END  as final_parent_created".$query;
        $query_limit = $query." LIMIT ".$limit." OFFSET ".strval(($page-1)*$limit)."";


		return (object) array('items'=>DB::select($query_limit), 'query_all'=>$query, 'count' => $count);
	}

	public static function getFinalParent($parent_id){
		$parent = DB::table('produzene_garancije')->where('produzene_garancije_id',$parent_id)->first();
		if(!is_null($parent->parent_id)){
			$finalParent = self::getFinalParent($parent->parent_id);
		}else{
			$finalParent = $parent;
		}
		return $finalParent;
	}

	public static function getGeneralAndChildsIds($produzene_garancije_id,&$branch){
		$garancija = DB::table('produzene_garancije')->where('produzene_garancije_id',$produzene_garancije_id)->first();
		array_push($branch, $garancija->produzene_garancije_id);
		if(!is_null($garancija->parent_id)){
			self::getGeneralAndChildsIds($garancija->parent_id,$branch);
		}
	}

	public static function sortDirection($column,$current_column='final_parent_created',$current_direction='DESC'){
		if($column == $current_column){
			return ($current_direction == 'DESC') ? 'ASC' : 'DESC';
		}
		return 'ASC';
	}

	public static function getProizvodjacArticles($proizvodjac_id){
        $garancija_proizvodjac = DB::table('proizvodjac')->where('proizvodjac_id',$proizvodjac_id)->where('produzena_garancija',1)->orderBy('proizvodjac_id','asc')->first();
        $articles = DB::select("SELECT roba_id, sku, naziv_web, grupa FROM roba r LEFT JOIN grupa_pr g ON r.grupa_pr_id = g.grupa_pr_id LEFT JOIN proizvodjac p ON r.proizvodjac_id = p.proizvodjac_id WHERE p.proizvodjac_id > 0".(!is_null($garancija_proizvodjac) ? " AND p.proizvodjac_id = ".$garancija_proizvodjac->proizvodjac_id : "")." AND r.produzena_garancija = 1 AND p.produzena_garancija = 1 ORDER BY grupa ASC");
        return $articles;
	}

}