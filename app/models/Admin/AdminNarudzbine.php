<?php
use Service\Mailer;
use Service\Drip;

class AdminNarudzbine {

	public static function narudzbina_iznos_ukupno($web_b2c_narudzbina_id){
		$ukupno=0;
		$vaucer_popust = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('vaucer_popust');
		foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
			$ukupno+=$row->jm_cena*$row->kolicina;
		}
		return $ukupno-$vaucer_popust;
	}
	public static function bodovi_popust($web_b2c_narudzbina_id){
      return DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('popust');

    }
	public static function nazivweb($web_b2c_narudzbina_stavka_id) { 
		$id= DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id', $web_b2c_narudzbina_stavka_id)->pluck('roba_id');

		return DB::table('roba')->where('roba_id', $id)->pluck('naziv_web');
	}
	public static function narudzbina_stavka_posta($posta_zahtev_api_id){
		return DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->where('posta_zahtev_api_id', $posta_zahtev_api_id)->orderBy('web_b2c_narudzbina_stavka_posta_zahtev_id','asc')->get();
	}

	// public static function narudzbina_stavka_kolicina($stavka_id){
	// 	$stavka   = DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->where('stavka_id', $stavka_id)->first();
	// 	$kolicina_posta = $stavka->kolicina;
	// 	$kolicina = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id',$stavka->stavka_id)->pluck('kolicina');
	// 	return $kolicina - $kolicina_posta;
	// }

	public static function city_odgovor_id($web_b2c_narudzbina_id,$posta_zahtev_api_id){
		return DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->where('posta_zahtev_api_id', $posta_zahtev_api_id)->pluck('posta_odgovor_id');
	}
	// public static function narudzbina_iznos($web_b2c_narudzbina_id){

	// 	$id = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_b2c_narudzbina_id');
	// 	$id2 = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$id)->pluck('web_b2c_narudzbina_stavka_id');
	// 	$id3 = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id',$id2)->pluck('jm_cena');

	// 	return $id3;
	// }

	public static function narudzbina_status_active($web_b2c_narudzbina_id){
		foreach (DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
			if($row->prihvaceno == 0 and $row->realizovano == 0 and $row->stornirano == 0){
				return "Nova";
			}
			else  if($row->prihvaceno != 0 and $row->realizovano == 0 and $row->stornirano == 0){
				return "Prih.";
			}
			else  if($row->realizovano != 0 and $row->stornirano == 0){
				return "Real.";
			}
			else  if($row->stornirano != 0){
				return "Stor.";
			}
		}
	}

	public static function narudzbina_status_css($web_b2c_narudzbina_id){
		foreach (DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
			if($row->prihvaceno == 0 and $row->realizovano == 0 and $row->stornirano == 0){
				return "nova";
			}
			else  if($row->prihvaceno != 0 and $row->realizovano == 0 and $row->stornirano == 0){
				return "prihvacena";
			}
			else  if($row->realizovano != 0 and $row->stornirano == 0){
				return "realizovana";
			}
			else  if($row->stornirano != 0){
				return "stornirana";
			}
		}
	}

	// public static function narudzbina_status_posible($aktivna,$web_b2c_nrudzbina_id){
	// 	switch($aktivna){
	// 		case "Nova":
	// 			echo '
	// 				<option value="1" class="status-porudzbine" data-status-target="1" data-narudzbina-id="'.$web_b2c_nrudzbina_id.'">Prihvati</option>'.
	// 				'<option value="1" class="status-porudzbine" data-status-target="3" data-narudzbina-id="'.$web_b2c_nrudzbina_id.'">Storniraj</option>';
	// 		break;
	// 		case "Prih.":
	// 			echo ' 
	// 				<option value="2" class="status-porudzbine" data-status-target="2" data-narudzbina-id="'.$web_b2c_nrudzbina_id.'">Realizuj</option>
	// 				<option value="3" class="status-porudzbine" data-status-target="3" data-narudzbina-id="'.$web_b2c_nrudzbina_id.'">Storniraj</option>';
	// 		break;
	// 		case "Real.":
	// 			echo '
	// 				<option value="3" class="status-porudzbine" data-status-target="3" data-narudzbina-id="'.$web_b2c_nrudzbina_id.'">Storniraj</option>';
	// 		break;
	// 		case "Stor.":
	// 			echo '';
	// 		break;
	// 	}
		
	// }

	public static function find($narudzbina_id, $column){
		return DB::table('web_b2c_narudzbina')->select($column)->where('web_b2c_narudzbina_id', $narudzbina_id)->pluck($column);
	}

	public static function save($data){
		if(isset($data['posta_slanje_poslato'])){	
			$data['posta_slanje_poslato'] = 1;
		} else {
			$data['posta_slanje_poslato'] = 0;
		}

		if(isset($data['posta_slanje_id'])){
			
		} else {
			$data['posta_slanje_id'] = 1;
		}
		$save_data = [
				'web_kupac_id' => $data['web_kupac_id'],
				'broj_dokumenta' => $data['broj_dokumenta'],
				'datum_dokumenta' => $data['datum_dokumenta'],
				'web_nacin_placanja_id' => $data['web_nacin_placanja_id'],
				'web_nacin_isporuke_id' => $data['web_nacin_isporuke_id'],
				'ip_adresa' => $data['ip_adresa'],
				'napomena' => $data['napomena'],
				'komentar' => $data['komentar'],
				'posta_slanje_broj_posiljke' => $data['posta_slanje_broj_posiljke'],
				'posta_slanje_poslato' => $data['posta_slanje_poslato'],
				'posta_slanje_id' => $data['posta_slanje_id'],
				'poslednja_izmena'=>date('Y-m-d H:i:s')
				];

		if($data['web_b2c_narudzbina_id'] == 0) {
			$save_data['datum_dokumenta'] = date('Y-m-d H:i:s');
			DB::table('web_b2c_narudzbina')->insert($save_data);

		    //drip
		    if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1){
		        $dbOrder = DB::table('web_b2c_narudzbina')->where('broj_dokumenta',$save_data['broj_dokumenta'])->first();
		        $drip = new Drip();
		        $drip->addOrUpdateOrder($dbOrder,"placed");   
		    }
			AdminSupport::saveLog('NARUDZBINA_DODAJ', array(DB::table('web_b2c_narudzbina')->max('web_b2c_narudzbina_id')));
		} else {
			DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $data['web_b2c_narudzbina_id'])->update($save_data);

		    //drip
		    if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1){
		        $dbOrder = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $data['web_b2c_narudzbina_id'])->first();
		        $drip = new Drip();
		        $drip->addOrUpdateOrder($dbOrder,"updated");   
		    }
			AdminSupport::saveLog('NARUDZBINA_IZMENI', array($data['web_b2c_narudzbina_id']));
		}



	}


	public static function ukupnaCena($narudzbina_id) {

		 $stavke = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $narudzbina_id)->get();
			$ukupnaCena = 0;
			foreach ($stavke as $stavka) {
				$ukupnaCena += $stavka->jm_cena * $stavka->kolicina;
			}
			return $ukupnaCena . '.00';
	}

	public static function brojNarudzbine() {
        $br_nar = DB::select("SELECT (last_value + 1) as web_b2c_narudzbina_id FROM web_b2c_narudzbina_web_b2c_narudzbina_id_seq")[0]->web_b2c_narudzbina_id;
        return DB::table('vrsta_dokumenta')->where('vrsta_dokumenta_id',501)->pluck('sifra_vd').str_pad($br_nar, 5, '0', STR_PAD_LEFT);
	}

	public static function kolicina($web_b2c_narudzbina_id,$roba_id){
		return intval(DB::table('web_b2c_narudzbina_stavka')->where(array('web_b2c_narudzbina_id'=>$web_b2c_narudzbina_id,'roba_id'=>$roba_id))->pluck('kolicina'));
	}

	public static function rezervacija($roba_id,$kolicina,$checkLager=true,$rezervacija=true){
        $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
        $orgj_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
        $lager = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->first();

        if(!is_null($lager)){
			if($checkLager && AdminOptions::vodjenje_lagera() == 1){
				DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->update(array('kolicina'=>($lager->kolicina-$kolicina)));
			}
			if($rezervacija && AdminOptions::web_options(131) == 1){
				DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->update(array('rezervisano'=>($lager->rezervisano-$kolicina)));
			}
		}
	}

	public static function datum_narudzbine($web_b2c_narudzbina_id){
		
		
		foreach(DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
			$datum=$row->datum_dokumenta;
			$dana2 = date('d', strtotime($datum));
			$meseci2 = date('m', strtotime($datum));
			$godina2 = date('Y', strtotime($datum));
		
		}
		
		return $dana2.".".$meseci2.".".$godina2;
	}

	// public static function datum($web_b2c_narudzbina_id){
		
	// 	return DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('datum_dokumenta');

	// }

   public static function narudzbina_ni($web_b2c_narudzbina_id){
	   $ni=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_nacin_isporuke_id'); 
	   return DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$ni)->pluck('naziv');
	}

	public static function narudzbina_np($web_b2c_narudzbina_id){
	   $np=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_nacin_placanja_id'); 
	   return DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$np)->pluck('naziv');
	} 

	public static function artikal_narudzbina($web_b2c_narudzbina_stavka_id) {
		
		$roba_id=DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id',$web_b2c_narudzbina_stavka_id)->pluck('roba_id');
		return AdminCommon::short_title($roba_id);
		
	}

	 public static function artikal_cena_naruzbina($web_b2c_narudzbina_stavka_id) {
		
		$jm_cena=DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id',$web_b2c_narudzbina_stavka_id)->pluck('jm_cena');
		return AdminCommon::cena($jm_cena);
		
	}
	public static function artikal_cena_stavka($web_b2c_narudzbina_stavka_id) {
		
		$jm_cena=DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id',$web_b2c_narudzbina_stavka_id)->pluck('jm_cena');
		$kolicina=DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id',$web_b2c_narudzbina_stavka_id)->pluck('kolicina');
		$ukupno=$jm_cena*$kolicina;
		return AdminCommon::cena($ukupno);
		
	} 
	public static function narudzbine($kupac_id) { 
		return DB::table('web_b2c_narudzbina')->where('web_kupac_id', $kupac_id)->orderBy('web_b2c_narudzbina_id', 'asc')->get();
	}
	public static function narudzbina_stavke($web_b2c_narudzbina_id) { 
		return DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->orderBy('web_b2c_narudzbina_stavka_id', 'asc')->get();
	}

	// public static function find_status_narudzbine($narudzbina_status_id)
 //    {
 //        return DB::table('narudzbina_status')->where('narudzbina_status_id', $narudzbina_status_id)->pluck('naziv');
 //    }

    public static function status_narudzbine($web_b2c_narudzbina_id) { 
        $nazivi = [];
        foreach(DB::select("select (select naziv from narudzbina_status where narudzbina_status_id = nsn.narudzbina_status_id) as naziv from narudzbina_status_narudzbina nsn where web_b2c_narudzbina_id = ".$web_b2c_narudzbina_id) as $row){
            $nazivi[] = $row->naziv;
        }
        return implode(', ', $nazivi);
	}

	// public static function artikli_narudzbine($web_b2c_narudzbina_id) { 
	// 	$stavke= DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->orderBy('web_b2c_narudzbina_stavka_id', 'asc')->get();
	// 	$artikli=array();
	// 	foreach ($stavke as $stavka) {
	// 		$artikal = DB::table('roba')->where('roba_id',$stavka->roba_id)->first();
	// 		if($artikal){
	// 			$artikli[] = (object) array(

	// 				'naziv' => $artikal->naziv_web,
	// 				'ukupna_cena' => $stavka->jm_cena * $stavka->kolicina
					
	// 			);	
	// 		}
	// 	}
	// 	return $artikli;
	// }

	public static function dodatni_statusi($all=false) { 
		$query = DB::table('narudzbina_status');
		if(!$all){
			$query = $query->where('selected','>','0');
		}
		return $query->get();
	}

	// public static function cityexpres_statusi() { 
	// 	return DB::table('posta_zahtev_api')->select('pickup_id')->where('posta_odgovor_id','!=','')->where('pickup_id','!=','')->get();
	// }
	
	// public static function artikli_cena($web_b2c_narudzbina_stavka_id) { 
	// 	$stavke= DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_stavka_id', $web_b2c_narudzbina_stavka_id)->orderBy('web_b2c_narudzbina_stavka_id', 'asc')->get();
	// 	$jmcena=array();
	// 	foreach ($stavke as $jmcena) {
			
	// 			$jmcena[] = (object) array(
	// 				'jm_cena'=> $artikal->jm_cena

	// 			);	
			
	// 	}
	// 	return $jmcena;
	// }

	public static function kupacAdresa($web_kupac_id) { 
		return DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('adresa');
	}
	public static function kupacMesto($web_kupac_id) { 
		return DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->pluck('mesto');
	}

	public static function kupacTelefoni($web_kupac_id) { 
		$kupac = DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->first();
		$telephones = '';
		if(isset($kupac->telefon)){
			$telephones .= $kupac->telefon;
		}        
		if(isset($kupac->telefon_mobilni)){
			$telephones .= ' / '.$kupac->telefon_mobilni;
		}
		return $telephones;
	}

	public static function createPdf($web_b2c_narudzbina_id) { 
		$stavke = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get();

		$web_kupac_id=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id');
		$web_kupac = DB::table('web_kupac')->where('web_kupac_id',$web_kupac_id)->first();
		if($web_kupac->flag_vrsta_kupca == 0){
			$subnaziv = AdminSupport::encodeForPdf('Ime i Prezime:');
			$naziv = AdminSupport::encodeForPdf($web_kupac->ime.' '.$web_kupac->prezime);
		}else{
			$subnaziv = AdminSupport::encodeForPdf('Ime i Prezime:');
			$naziv = AdminSupport::encodeForPdf($web_kupac->naziv.' '.$web_kupac->pib);
		}
		
		$fpdf = new MCPdf();
		$fpdf->AddPage();
		//image
		$fpdf->Image('http://www.tico.rs/./images/logo.png',95,5,24);
		$fpdf->Ln(10);

		// title
		$fpdf->SetFont('Courier','B',18);
		$fpdf->Cell(40,10,AdminSupport::encodeForPdf('Narudžbina'));
		$fpdf->Ln(10);

		// subtitle
		$fpdf->SetFont('Courier','B',13);
		$fpdf->Cell(40,10,AdminSupport::encodeForPdf('Informacije o narudžbini'));
		$fpdf->Ln(9);

		$fpdf->SetFont('Courier',null,10);
		$fpdf->SetDrawColor(0,0,0);
		$fpdf->SetFillColor(256,256,256);
		$fpdf->SetTextColor(0,0,0);
		$fpdf->SetLineWidth(0.1);

		$fpdf->Cell(80,6,AdminSupport::encodeForPdf('Broj porudžbine:'),1,0,'A',true);
		$fpdf->Cell(110,6,AdminNarudzbine::find($web_b2c_narudzbina_id,'broj_dokumenta'),1,0,'A',true);
		$fpdf->Ln(6);

		$fpdf->Cell(80,6,AdminSupport::encodeForPdf('Datum porudžbine:'),1,0,'A',true);
		$fpdf->Cell(110,6,AdminNarudzbine::find($web_b2c_narudzbina_id,'datum_dokumenta'),1,0,'A',true);
		$fpdf->Ln(6);

		$fpdf->Cell(80,6,AdminSupport::encodeForPdf('Način isporuke:'),1,0,'A',true);
		$fpdf->Cell(110,6,AdminSupport::encodeForPdf( AdminCommon::n_i($web_b2c_narudzbina_id)),1,0,'A',true);
		$fpdf->Ln(6);

		$fpdf->Cell(80,6,AdminSupport::encodeForPdf('Način plaćanja:'),1,0,'A',true);
		$fpdf->Cell(110,6,AdminSupport::encodeForPdf(AdminCommon::n_p($web_b2c_narudzbina_id)),1,0,'A',true);
		$fpdf->Ln(6);

		$fpdf->Cell(80,6,'Napomena:',1,0,'A',true);
		$fpdf->Cell(110,6,AdminSupport::encodeForPdf(AdminNarudzbine::find($web_b2c_narudzbina_id,'napomena')),1,0,'A',true);
		$fpdf->Ln(6);

		// subtitle
		$fpdf->SetFont('Courier','B',13);
		$fpdf->Cell(40,10,AdminSupport::encodeForPdf('Informacije o kupcu'));
		$fpdf->Ln(9);

		$fpdf->SetFont('Courier',null,10);
		$fpdf->SetDrawColor(0,0,0);
		$fpdf->SetFillColor(256,256,256);
		$fpdf->SetTextColor(0,0,0);
		$fpdf->SetLineWidth(0.1);

		$fpdf->Cell(80,6,$subnaziv,1,0,'A',true);
		$fpdf->Cell(110,6,$naziv,1,0,'A',true);
		$fpdf->Ln(6);

		$fpdf->Cell(80,6,'Adresa:',1,0,'A',true);
		$fpdf->Cell(110,6,AdminSupport::encodeForPdf($web_kupac->adresa),1,0,'A',true);
		$fpdf->Ln(6);

		$fpdf->Cell(80,6,'Mesto:',1,0,'A',true);
		$fpdf->Cell(110,6,AdminSupport::encodeForPdf(AdminSupport::find_mesto($web_kupac->mesto_id,'mesto')),1,0,'A',true);
		$fpdf->Ln(6);;

		$fpdf->Cell(80,6,'Telefon:',1,0,'A',true);
		$fpdf->Cell(110,6,AdminSupport::encodeForPdf($web_kupac->telefon),1,0,'A',true);
		$fpdf->Ln(6);

		$fpdf->Cell(80,6,'E-mail:',1,0,'A',true);
		$fpdf->Cell(110,6,AdminSupport::encodeForPdf($web_kupac->email),1,0,'A',true);
		$fpdf->Ln(15);

		$fpdf->SetFont('Courier','B',10);
		$fpdf->SetWidths(array(107,22,21,40));
		$fpdf->Row(array('Naziv proizvoda','Cena',AdminSupport::encodeForPdf('Količina'),'Ukupna cena'));
		$fpdf->SetFont('Courier',null,10);
		foreach($stavke as $stavka){
			$fpdf->Row(array(AdminSupport::encodeForPdf(AdminCommon::short_title($stavka->roba_id)),round($stavka->jm_cena).'.00',round($stavka->kolicina),$stavka->kolicina*$stavka->jm_cena.'.00'));
		}
		$fpdf->SetFont('Courier','B',10);
		$fpdf->Row(array('','','',AdminCommon::narudzbina_ukupno($web_b2c_narudzbina_id).'.00 rsd.'));

		$fpdf->Ln(25);
		$fpdf->SetFont('Courier',null,12);
		$fpdf->Cell(190,10,'_____________________                             ________________________');

		$fpdf->Output('','',true);
		exit;
	} 


	public static function pdv($id) {
		return DB::table('tarifna_grupa')->where('tarifna_grupa_id', $id)->pluck('porez');
	}

	public static function tarifnaGrupaPdv($id) {
		
		$procenat = DB::table('tarifna_grupa')->where('tarifna_grupa_id', $id)->pluck('porez');

		return $procenat / 100 + 1;
	}

	public static function formatDate($date) {
		$arr = explode("-", $date);
		return $arr[2] . "-" . $arr[1] . "-" . $arr[0];
	}

    public static function api_posta($posta_slanje_id){
        $posta_slanje = DB::table('posta_slanje')->where(array('posta_slanje_id'=>$posta_slanje_id, 'api_aktivna' => 1))->first();
        if(is_null($posta_slanje)){
            return false;
        }else{
            return true;
        }
    }

    public static function opstine(){
        return DB::table('narudzbina_opstina')->orderBy('naziv','asc')->get();
    }
    public static function posta_slanje_narudzbine($web_b2c_narudzbina_id){
        return DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get();
    }
    public static function mesta($opstina_old_id){
        return DB::table('narudzbina_mesto')->where('narudzbina_opstina_code',DB::table('narudzbina_opstina')->where('narudzbina_opstina_id',($opstina_old_id ? $opstina_old_id : DB::table('narudzbina_opstina')->orderBy('naziv','asc')->first()->narudzbina_opstina_id))->orderBy('naziv','asc')->first()->code)->orderBy('naziv','asc')->get();
    }
    public static function mesta_cityexpress(){
        return DB::table('narudzbina_mesto')->where('posta_slanje_id',2)->orderBy('naziv','asc')->get();
    }
    public static function ulice($mesto_old_id){
        return DB::table('narudzbina_ulica')->where('narudzbina_mesto_code',DB::table('narudzbina_mesto')->where('narudzbina_mesto_id',($mesto_old_id ? $mesto_old_id : DB::table('narudzbina_mesto')->where('narudzbina_opstina_code',DB::table('narudzbina_opstina')->where('narudzbina_opstina_id',DB::table('narudzbina_opstina')->orderBy('naziv','asc')->first()->narudzbina_opstina_id)->orderBy('naziv','asc')->first()->code)->orderBy('naziv','asc')->first()->narudzbina_mesto_id))->orderBy('naziv','asc')->first()->code)->orderBy('naziv','asc')->get();
    }
    
    public static function mesto($ulica_id){
        $ulica = self::ulica($ulica_id);
        if(!is_null($ulica)){
            $mesto = DB::table('narudzbina_mesto')->where('code',$ulica->narudzbina_mesto_code)->first();
            if(!is_null($mesto)){
                return $mesto;
            }
        }
        return null;
    }
    public static function opstina($ulica_id){
        $mesto = self::mesto($ulica_id);
        if(!is_null($mesto)){
            $opstina = DB::table('narudzbina_opstina')->where('code',$mesto->narudzbina_opstina_code)->first();
            if(!is_null($opstina)){
                return $opstina;
            }
        }
        return null;
    }
    public static function ulica($ulica_id){
        return DB::table('narudzbina_ulica')->where('narudzbina_ulica_id',$ulica_id)->first();
    }
    public static function mesto_id($ulica_id){
        $mesto = self::mesto($ulica_id);
        if(!is_null($mesto)){
            return $mesto->narudzbina_mesto_id;
        }
        return 0;
    }
    public static function opstina_id($ulica_id){
        $opstina = self::opstina($ulica_id);
        if(!is_null($opstina)){
            return $opstina->narudzbina_opstina_id;
        }
        return 0;
    }

    public static function grouped_by_posiljalac($web_b2c_narudzbina_id,$posta_slanje_id,$posta_zahtev=false){
    	$narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->first();
        $narudzbina_stavke_query = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id);
        if($narudzbina->posta_slanje_id == $posta_slanje_id){
            $narudzbina_stavke = $narudzbina_stavke_query->where(function($query) use ($posta_slanje_id) {
                $query->where('posta_slanje_id', $posta_slanje_id)
                      ->orWhere('posta_slanje_id', null);
            })->get();
        }else{
            $narudzbina_stavke = $narudzbina_stavke_query->where('posta_slanje_id',$posta_slanje_id)->get();
        }

        $grouped_by_posiljalac = array();
        foreach($narudzbina_stavke as $stavka){
        	if(!$posta_zahtev){
	        	if($stavka->posta_zahtev == 0){
		            $grouped_by_posiljalac[$stavka->posiljalac_id][] = $stavka;
		        }
		    }else{
		    	$grouped_by_posiljalac[$stavka->posiljalac_id][] = $stavka;
		    }
        }
        return $grouped_by_posiljalac;
	}

    public static function narudzbina_poste_slanja($narudzbina_id){
    	$narudzbina = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$narudzbina_id)->first();
    	if(is_null($narudzbina)){
    		return array();
    	}
    	$narudzbina_stavke = DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$narudzbina_id)->get();
    	if(count($narudzbina_stavke) == 0){
    		return array();
    	}
    	$narudzbina_poste_slanja = array();
    	foreach($narudzbina_stavke as $stavka){
    		$narudzbina_poste_slanja[] = ($stavka->posta_slanje_id ? $stavka->posta_slanje_id : $narudzbina->posta_slanje_id);
    	}
    	return DB::table('posta_slanje')->where('api_aktivna',1)->whereIn('posta_slanje_id',array_unique($narudzbina_poste_slanja))->get();
    }

    public static function broj_paketa($stavke){
    	$broj_paketa = 0;
    	foreach($stavke as $stavka){
    		if(!is_null($stavka->broj_paketa) && $stavka->broj_paketa != ''){
    			$broj_paketa += $stavka->broj_paketa;
    		}
    	}
    	if($broj_paketa > 0){
	    	return round($broj_paketa);
	    }
	    return 1;
    }

    public static function ulica_broj($stavke){
    	$ulica_id = 0;
    	$broj = null;
    	foreach($stavke as $stavka){
    		if(!is_null($stavka->ulica_id) && $stavka->ulica_id != '' && !is_null($stavka->broj) && $stavka->broj != ''){
    			$ulica_id = $stavka->ulica_id;
    			$broj = $stavka->broj;
    			break;
    		}
    	}
    	return (object) array(
    		'ulica_id' => $ulica_id,
    		'broj' => $broj
    		);
    }

    public static function posta_info($stavke){
    	$web_nacin_placanja_id = 0;
    	$transport_placa = 0;
    	$cena_otkupa = 0;
    	$racun_otkupa = '';
    	$otkup_prima = -1;
    	$odgovor = 0;
    	$napomena = '';
    	$opis = '';

    	foreach($stavke as $stavka){
    		$racun_otkupa = $stavka->racun_otkupa;
    		$otkup_prima = $stavka->otkup_prima;
    		$odgovor = $stavka->odgovor;
    		$napomena = $stavka->napomena;
    		$opis = $stavka->opis;
    		if(!is_null($stavka->web_nacin_placanja_id) && !is_null($stavka->transport_placa) && !is_null($stavka->cena_otkupa)){
    			$web_nacin_placanja_id = $stavka->web_nacin_placanja_id;
    			$transport_placa = $stavka->transport_placa;
    			$cena_otkupa = $stavka->cena_otkupa;
    			break;
    		}
    	}

    	return (object) array(
    		'web_nacin_placanja_id' => $web_nacin_placanja_id,
    		'transport_placa' => $transport_placa,
    		'cena_otkupa' => $cena_otkupa,
    		'racun_otkupa' => !is_null($racun_otkupa) ? $racun_otkupa : '',
    		'otkup_prima' => $otkup_prima,
    		'odgovor' => $odgovor,
    		'napomena' => $napomena,
    		'opis' => $opis
    		);
    }

    public static function dobavljac($roba_id){
    	$partner= DB::table('roba')->where('roba_id',$roba_id)->first();
    	if(is_null($partner)){
    		return null;
    	}
    	return	DB::table('partner')->where('partner_id',$partner->dobavljac_id)->first();
    }

    public static function mailOrderNotificationToClient($narudzbina_id,$status){

        $web_kupac=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->pluck('web_kupac_id');
        $email=DB::table('web_kupac')->where('web_kupac_id', $web_kupac)->pluck('email');

        $narudzbina=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $narudzbina_id)->pluck('broj_dokumenta');

        $subject='Informacije o narudžbini';

        if($status=='prihvacena')
        { 
        	$body='Vaša narudžbina ' . $narudzbina . ' je prihvaćena';

        	$filePath = 'files/pdf_predracun_'.$narudzbina.'.pdf';
	        $pdf = App::make('dompdf');
	        $pdf->loadView('admin.pdf_predracun',['web_b2c_narudzbina_ids' => array($narudzbina_id)])->save($filePath);
    	}
        else if($status=='realizovana')
        {
	        $body='Vaša narudžbina ' . $narudzbina . ' je realizovana';
	        $filePath=null;
      	}
      
    	Mailer::send(Options::company_email(),$email,$subject,$body,$filePath); 

       	if (!is_null($filePath)) {
	    	if(File::exists($filePath)){
	          	File::delete($filePath);
	        }	
    	}
    }

    // public static function posta_odgovor($stavka_id){
    //     $stavka= DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->where('stavka_id',$stavka_id)->first();
    //     $narudzbina_id = DB::table('web_b2c_narudzbina_stavka')->where('stavka_id', $stavka->stavka_id)->pluck('web_b2c_narudzbina_id');
    //     return DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$narudzbina_id)->pluck('posta_odgovor_id');
    // }
    
    public function posta_dokument_postoji($web_b2c_narudzbina_id,$posta_zahtev_api_id){

       return DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_zahtev_api_id',$posta_zahtev_api_id)->pluck('dokument');        
    }

	public static function saveMoreStatuses($web_b2c_narudzbina_id,$narudzbina_status_ids){
		$old_statuses = array_map('current',DB::table('narudzbina_status_narudzbina')->select('narudzbina_status_id')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get());
		$for_delete = array_diff($old_statuses,$narudzbina_status_ids);
		$for_insert = array_diff($narudzbina_status_ids,$old_statuses);

		if(count($for_delete) > 0){
			DB::table('narudzbina_status_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->whereIn('narudzbina_status_id',$for_delete)->delete();
		}
		if(count($for_insert) > 0){
			foreach($for_insert as $narudzbina_status_id){

				DB::table('narudzbina_status_narudzbina')->insert(array('web_b2c_narudzbina_id'=>$web_b2c_narudzbina_id,'narudzbina_status_id'=>$narudzbina_status_id));
			}


		}
	}

}
