<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Kama {

public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/Kama/Kama_excel/Kama.xlsx";
						$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}


       		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        //$loadedSheetName = $excelObj->getSheetNames();
		
	        for ($row = 1; $row <= $lastRow; $row++) {
	        	
  				$sifra    = $worksheet->getCell('A'.$row)->getValue();
	            $naziv    = $worksheet->getCell('C'.$row)->getValue();
	            $grupa    = $worksheet->getCell('E'.$row)->getValue();
	            $model    = $worksheet->getCell('G'.$row)->getValue();
				$cena_nc  = $worksheet->getCell('I'.$row)->getValue();
				$slika1	  =	$worksheet->getCell('M'.$row)->getValue();	
				$slika2	  =	$worksheet->getCell('N'.$row)->getValue();	
				$slika3	  =	$worksheet->getCell('O'.$row)->getValue();	
				$slika4	  =	$worksheet->getCell('P'.$row)->getValue();		
				
				$sezona	  =	$worksheet->getCell('L'.$row)->getValue();
				$dimenzi  = $worksheet->getCell('Q'.$row)->getValue();
				$karakter = $worksheet->getCell('R'.$row)->getValue();
				$tezina	  =	$worksheet->getCell('U'.$row)->getValue();
				$efikasn  =	$worksheet->getCell('V'.$row)->getValue();
				$nosivos  = $worksheet->getCell('T'.$row)->getValue();
				$brzina   = $worksheet->getCell('S'.$row)->getValue();				
				$prijanj  = $worksheet->getCell('W'.$row)->getValue();
				$buka     = $worksheet->getCell('X'.$row)->getValue();				

			
				
			if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc)){					
				

				$proizvodjac = "Kama";
				$s = "Sezona: " . $sezona . "";				
				$d = "Dimenzije: ". $dimenzi ." ";
				$k = "Tip: ". $karakter ." ";
				$n = "Index Nosivosti: ". $nosivos ."";
				$t = "Težina: ". $tezina ." kg";
				$br = "Index Brzine: ". $brzina ."";				
				$e = "Stepen iskorišćenja goriva: ". $efikasn ." ";
				$pr = "Prianjanje na mokroj podlozi: ". $prijanj ." ";			
				$b = "Spoljašnja buka pri kotrljanju: ". $buka ." db";
				
				
				if(isset($buka)){
				$opis = "<ul> 
						<li> $s </li> 
						<li> $d </li> 
						<li> $k </li> 
						<li> $n </li>
						<li> $t </li>
						<li> $br </li>
						<li> $e </li>  
						<li> $pr </li>
						<li> $b </li> 
						</ul>";
				}else{
				$opis = "<ul> 
						<li> $s </li> 
						<li> $d </li> 
						<li> $k </li> 
						<li> $n </li>
						<li> $t </li>
						<li> $br </li>
						</ul>";	
				}

			if ($slika1 !='' && isset($slika1)) {
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $sifra . "',";
				$sPolja .= "akcija,";					$sVrednosti .= "" . 1 . ",";
				$sPolja .= "putanja";				    $sVrednosti .= "'" . $slika1  . "'";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				
			}

			if ($slika2 !='' && isset($slika2)) {
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $sifra . "',";
				$sPolja .= "akcija,";					$sVrednosti .= "" . 0 . ",";
				$sPolja .= "putanja";				    $sVrednosti .= "'" . $slika2 . "'";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				
			}

			if ($slika3 !='' && isset($slika3)) {
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $sifra . "',";
				$sPolja .= "akcija,";					$sVrednosti .= "" . 0 . ",";
				$sPolja .= "putanja";				    $sVrednosti .= "'" . $slika3 . "'";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				
			}
			if ($slika4 !='' && isset($slika4)) {
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $sifra . "',";
				$sPolja .= "akcija,";					$sVrednosti .= "" . 0 . ",";
				$sPolja .= "putanja";				    $sVrednosti .= "'" . $slika4 . "'";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				
			}

				$sPolja = '';
				$sVrednosti = '';

				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" . $sifra . "',";
				$sPolja .= " naziv,";					$sVrednosti .= "'" . Support::encodeTo1250($naziv) . "',";
				$sPolja .= " grupa,";					$sVrednosti .= "'" . Support::encodeTo1250($grupa) . "',";
				$sPolja .= " model,";					$sVrednosti .= "'" . Support::encodeTo1250($model) . "',";
				$sPolja .= " opis,";					$sVrednosti .= "'" . Support::encodeTo1250($opis) . "',";
				$sPolja .= " proizvodjac,";				$sVrednosti .= "'" . $proizvodjac . "',";
				$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
				$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . "";
					

				
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			}		
		
}	
       		
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}

		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/pirot/pirot_excel/pirot.xlsx');
			$products_file = "files/pirot/pirot_excel/pirot.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	       	
	       	$i = 0;
   			while ($excelObj->setActiveSheetIndex($i)){

        	$worksheet = $excelObj->getActiveSheet();
			$lastRow = $worksheet->getHighestRow();
			
       		$i++;
       		if($i == 22){
       			break;
       		}


	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra   = $worksheet->getCell('A'.$row)->getValue();
	            $naziv   = $worksheet->getCell('B'.$row)->getValue();
				$cena_nc = $worksheet->getCell('C'.$row)->getValue();
				$slika   = $worksheet->getCell('E'.$row)->getValue();
				$opis    = $worksheet->getCell('D'.$row)->getValue();
				$proizvodjac = 'pirot Toys';
					
					$naziv=strtolower($naziv);
					$cena_nc=$cena_nc*1.2;
																
					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" . addslashes($sifra) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($naziv)) . "  " .  addslashes(Support::encodeTo1250($opis)) . "  ( " .  addslashes(Support::encodeTo1250($sifra)) . " )',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
					$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . $proizvodjac . "',";
					$sPolja .= " flag_slika_postoji,";		$sVrednosti .= " 1,";
					$sPolja .= " mpcena,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= " pmp_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= " web_cena";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($cena_nc),1,$kurs,$valuta_id_nc),2, '.', '') . "";


					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	
					if(!empty($slika)){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",'".$sifra."','".$slika."',1 )");	
				}
			}
		}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}