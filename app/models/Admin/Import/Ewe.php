<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use File;

class Ewe {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		Support::initQueryExecute();
		//Citamo vrstu cene za nabavnu cenu
		$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null){
				Support::autoDownload(Support::autoLink($dobavljac_id),'files/ewe/ewe_xml/ewe.xml');
			}
			$file_name = Support::file_name("files/ewe/ewe_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/ewe/ewe_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}

		if($kurs==null){
			$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
		}

		foreach ($products as $product):
			
			$opis = $product->xpath('specifications/attribute_group');
		    // $flag_opis_postoji = "0";
		    $redni_br_grupe=0;
			foreach($opis as $opis_line){
				$redni_br_grupe++;
				// $parent = $opis_line->xpath("parent::*");
				$parent_group=$opis_line->attributes()->name;
				foreach ($opis_line->children() as $karakteristika){

				    $novaVrednost="";
					foreach ($karakteristika->children() as $deca) {
						$novaVrednost.=$deca;
					}
					
					DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp(partner_id,sifra_kod_dobavljaca,karakteristika_naziv,karakteristika_vrednost,karakteristika_grupa,redni_br_grupe)"
					."VALUES(".$dobavljac_id."," . Support::quotedStr($product->id) . ",".Support::quotedStr(Support::encodeTo1250($karakteristika['name'])).","
					." ".Support::quotedStr(Support::encodeTo1250($novaVrednost)) .",".Support::quotedStr(Support::encodeTo1250($parent_group)).",".$redni_br_grupe.")");
					// $flag_opis_postoji = "1";

				}
			}


			$images = $product->xpath('images/image');
			$flag_slika_postoji = "0";
			$i=0;
			foreach ($images as $slika):
				if($i==0){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->id).",'".Support::encodeTo1250($slika)."',1 )");
				}else{
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->id).",'".Support::encodeTo1250($slika)."',0 )");
				}
				$flag_slika_postoji = "1";
				$i++;
			endforeach;	

			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($product->id) . ",";
			$sPolja .= "proizvodjac,";				$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->manufacturer)) . ",";
			$sPolja .= "barkod,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->ean)) . ",";
			$sPolja .= "naziv,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->manufacturer) ." ".Support::encodeTo1250($product->name)). ",";
			$sPolja .= "grupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->category)) . ",";
			$sPolja .= "podgrupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->subcategory)) . ",";
			$sPolja .= "pdv,";						$sVrednosti .= "" . Support::quotedStr($product->vat) . ",";
			// if(!empty($product->recommended_retail_price)){
			// 	$sPolja .= "pmp_cena,";				$sVrednosti .= "" . Support::replace_empty_numeric($product->recommended_retail_price,1,$kurs,$valuta_id_nc) . ",";
			// }							
			$sPolja .= "kolicina,";					$sVrednosti .= " 50,";
			$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 2,";
			$sPolja .= "flag_slika_postoji,";		$sVrednosti .= "" . $flag_slika_postoji . ",";
			//$sPolja .= "cena_nc";					$sVrednosti .= "" . Support::replace_empty_numeric($product->price,2,$kurs,$valuta_id_nc) . "";	
			if(!empty($product->price_rebate)){
				$sPolja .= "cena_nc";					$sVrednosti .= "" . Support::replace_empty_numeric($product->price_rebate,1,$kurs,$valuta_id_nc) . "";
			}else{
				$sPolja .= "cena_nc";					$sVrednosti .= "" . Support::replace_empty_numeric($product->price,1,$kurs,$valuta_id_nc) . "";	
			}	
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

		endforeach;
		
		Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
		
		//Brisemo fajl
		$file_name = Support::file_name("files/ewe/ewe_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/ewe/ewe_xml/".$file_name);
		}
	
	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		Support::initQueryExecute();
		//Citamo vrstu cene za nabavnu cenu
		$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null){
				Support::autoDownload(Support::autoLink($dobavljac_id),'files/ewe/ewe_xml/ewe.xml');
			}
			$file_name = Support::file_name("files/ewe/ewe_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/ewe/ewe_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file);			
		}

		if($kurs==null){
			$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
		}

		foreach ($products as $product):

			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($product->id) . ",";
			$sPolja .= "pdv,";						$sVrednosti .= "" . Support::quotedStr($product->vat) . ",";
			// if(!empty($product->recommended_retail_price)){
			// 	$sPolja .= "pmp_cena,";				$sVrednosti .= "" . Support::replace_empty_numeric($product->recommended_retail_price,1,$kurs,$valuta_id_nc) . ",";
			// }							
			$sPolja .= "kolicina,";					$sVrednosti .= " 50,";
			
			if(!empty($product->price_rebate)){
				$sPolja .= "cena_nc";					$sVrednosti .= "" . Support::replace_empty_numeric($product->price_rebate,1,$kurs,$valuta_id_nc) . "";
			}else{
				$sPolja .= "cena_nc";					$sVrednosti .= "" . Support::replace_empty_numeric($product->price,1,$kurs,$valuta_id_nc) . "";	
			}	
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

		endforeach;
		
		//Support::queryShortExecute($dobavljac_id);
		
		//Brisemo fajl
		$file_name = Support::file_name("files/ewe/ewe_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/ewe/ewe_xml/".$file_name);
		}
	
	}

}