<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Xplorer {

	public static function execute($dobavljac_id,$extension=null){
		if($extension==null){
			
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/xplorer/xplorer_xml/xplorer.xml');
			$products_file = "files/xplorer/xplorer_xml/xplorer.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();
	        
			$products = simplexml_load_file($products_file);
			foreach ($products as $product):

				$flag_slika_postoji = 0;
				if($product->Slika1!=null && $product->Slika1!=''){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$product->Sifra."','".addslashes(Support::encodeTo1250($product->Slika1))."',1 )");
					$flag_slika_postoji = 1;				
		
					if($product->Slika2!=null && $product->Slika2!=''){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$product->Sifra."','".addslashes(Support::encodeTo1250($product->Slika2))."',0 )");			
					}
					if($product->Slika3!=null && $product->Slika3!=''){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$product->Sifra."','".addslashes(Support::encodeTo1250($product->Slika3))."',0 )");				
					}
					if($product->Slika4!=null && $product->Slika4!=''){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$product->Sifra."','".addslashes(Support::encodeTo1250($product->Slika4))."',0 )");				
					}
				}

				if($product->AkcijskaCijena > 0){
					$mpcena = $product->AkcijskaCijena;
				}else{
					$mpcena = $product->OriginalnaCijena;
				}
				$cena_nc = $mpcena / 1.15;

				$sPolja = '';
				$sVrednosti = '';

				
				$opis = addslashes(Support::encodeTo1250($product->Opis));
				$opis = pg_escape_string($opis);

				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->Sifra)) . "',";
				$sPolja .= "naziv,";					$sVrednosti .= "'" . Support::encodeTo1250($product->Naziv) ."',";
				$sPolja .= "proizvodjac,";				$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->brand)) ."',";
				$sPolja .= "barkod,";					$sVrednosti .= "'" . addslashes(str_replace('EAN code: ','',$product->Oznaka1)) ."',";
				$sPolja .= "grupa,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->kategorija)) . "',";
				$sPolja .= "opis,";						$sVrednosti .= "'" . $opis . "',";
				$sPolja .= "kolicina,";					$sVrednosti .= "". $product->Kolicina .",";
				$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 0,";
				$sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 1,";
				$sPolja .= "flag_slika_postoji,";		$sVrednosti .= "". $flag_slika_postoji .",";
				$sPolja .= "pmp_cena,";					$sVrednosti .= "" . $mpcena . ",";
				$sPolja .= "mpcena,";					$sVrednosti .= "" . $mpcena . ",";
				$sPolja .= "web_cena,";					$sVrednosti .= "" . $mpcena . ",";				
				$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena_nc . "";
		
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/xplorer/xplorer_xml/xplorer.xml');
			$products_file = "files/xplorer/xplorer_xml/xplorer.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();
	        
			$products = simplexml_load_file($products_file);
			foreach ($products as $product):

				if($product->AkcijskaCijena > 0){
					$mpcena = $product->AkcijskaCijena;
				}else{
					$mpcena = $product->OriginalnaCijena;
				}
				$cena_nc = $mpcena / 1.15;

				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->Sifra)) . "',";
				$sPolja .= "kolicina,";					$sVrednosti .= "". $product->Kolicina .",";	
				$sPolja .= "pmp_cena,";					$sVrednosti .= "" . $mpcena . ",";						
				$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena_nc . "";
		
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}