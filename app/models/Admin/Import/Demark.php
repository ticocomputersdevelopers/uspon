<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Demark {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/demark/demark_xml/demark.xml');
			$products_file = "files/demark/demark_xml/demark.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file, 'SimpleXMLElement', LIBXML_NOCDATA);	

				foreach ($products as $product) {
					$sifra = (string) $product->sifra;
					$barkod = (string) $product->barkod;
					$naziv = (string) $product->naziv;
					$grupa = (string) $product->grupa;
					$podgrupa = (string) $product->podgrupa;
					$proizvodjac = (string) $product->proizvodjac;
					$opis = (string) $product->opis;
					$cena_nc = (float) $product->cena;
					$pmp_cena = (float) $product->mpcena;
					$kolicina = (int) $product->kolicina;
					$pdv = (int) $product->pdv;
					
					$images = $product->xpath('slike/slika');
					$flag_slika_postoji = "0";

					if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc) && $cena_nc > 0) {
						foreach ($images as $slika):
							$i=0;
							if($i==0){
								DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".Support::encodeTo1250($slika)."',1 )");
							}else{
								DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".Support::encodeTo1250($slika)."',0 )");
							}
							$flag_slika_postoji = "1";
							$i++;
						endforeach;
					
					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . Support::encodeTo1250($sifra) . "',";
					$sPolja .= "barkod,";					$sVrednosti .= "'" . pg_escape_string(addslashes(Support::encodeTo1250($barkod))) . "',";
					if($proizvodjac == 'Prc'){

					$sPolja .= "naziv,";				$sVrednosti .= " '" . pg_escape_string(addslashes(Support::encodeTo1250($naziv))) .  " ( ".addslashes(Support::encodeTo1250($sifra)) . " ) ',";
					} else {
					$sPolja .= "naziv,";				$sVrednosti .= " '" . pg_escape_string(addslashes(Support::encodeTo1250($proizvodjac))) .   " " . pg_escape_string(addslashes(Support::encodeTo1250($naziv))) .  " ( ".addslashes(Support::encodeTo1250($sifra)) . " ) ',";
					}
					$sPolja .= "grupa,";					$sVrednosti .= "'" . pg_escape_string(addslashes(Support::encodeTo1250($grupa))) . "',";
					$sPolja .= "podgrupa,";					$sVrednosti .= "'" . pg_escape_string(addslashes(Support::encodeTo1250($podgrupa))) . "',";
					$sPolja .= "proizvodjac,";				$sVrednosti .= "'" . pg_escape_string(addslashes(Support::encodeTo1250($proizvodjac))) . "',";
					$sPolja .= " flag_slika_postoji,";		$sVrednosti .= " '" . $flag_slika_postoji . "',";
					if(!empty($opis)){
					$sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 1,";
					}else{
					$sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 0,";
					}
					$sPolja .= "pdv,";						$sVrednosti .= "" . $pdv . ",";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . $kolicina . ",";
					$sPolja .= " opis,";					$sVrednosti .= " '" . pg_escape_string(addslashes(Support::encodeTo1250($product->opis))) . "',";
					$sPolja .= " pmp_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($pmp_cena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					}
				}
				
			Support::queryExecute($dobavljac_id,array('i','u'),array('i','u'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/demark/demark_xml/demark.xml');
			$products_file = "files/demark/demark_xml/demark.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
		
		$products = simplexml_load_file($products_file);	
				
				foreach ($products as $product) {

					$sPolja = '';
					$sVrednosti = '';

					$sifra = (string) $product->sifra;
					$cena_nc = (float) $product->cena;
					$pmp_cena = (float) $product->mpcena;
					$kolicina = (int) $product->kolicina;

					if (isset($sifra) && isset($cena_nc) && is_numeric($cena_nc) && $cena_nc > 0) {
						$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
						$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . Support::encodeTo1250($sifra) . "',";
						$sPolja .= "kolicina,";					$sVrednosti .= "" . $kolicina . ",";
						$sPolja .= " pmp_cena,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric(floatval($pmp_cena),1,$kurs,$valuta_id_nc),2, '.', '') . ",";
						$sPolja .= "cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). "";
							
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
					}
				}

			Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}
	
}