<?php
namespace Import;
use Import\Support;
use DB;
use File;
use AdminOptions;

class Uspon {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			self::download();
			$products_file = "files/uspon/uspon.json";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$string = file_get_contents($products_file);
			$products = json_decode($string, true);
	        
	        foreach($products as $product_arr) {
	        	$product = (object) $product_arr;
	        	$naziv_art = $product->title;
	        	$sifra = $product->code;
	        	$kolicina = $product->stock_amount;
	        	$cena_nc = $product->b2b_price;
	        	$slika = $product->image;
	        	$proizvodjac = $product->manufname;
	        	$barkod = $product->barcode;
	        	if(isset($product->category[0])){
	        	$grupa = $product->category[0];
	        	}
	        	if(isset($product->category[1])){
	        	$podgrupa = $product->category[1];
       			}

       			$opis = $product->description;
       			foreach ($opis as $desc) {
       				//self::dd($desc['data']);
       					foreach($desc['data'] as $opis){
       						$vrednost=$opis['value'];
       						$naziv = $opis['title'];
       						DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp(partner_id,sifra_kod_dobavljaca,karakteristika_naziv,karakteristika_vrednost)"
				."VALUES(".$dobavljac_id."," . Support::quotedStr($sifra) . ",".Support::quotedStr(Support::encodeTo1250($naziv)).","
				." ".Support::quotedStr(Support::encodeTo1250($vrednost)).") ");
       					}

}
				if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc)){					

					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";
					$sPolja .= " naziv,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($naziv_art)) . "',";					
					$sPolja .= " grupa,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($grupa)) . "',";			
					$sPolja .= " podgrupa,";				$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($podgrupa)) . "',";				
					$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($proizvodjac)) . "',";
					if(isset($slika)){
					$sPolja .= " flag_slika_postoji,";		$sVrednosti .= " 1,";
					}					
					$sPolja .= " barkod,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($barkod)) . "',";
					$sPolja .= " web_flag_karakteristike,";	$sVrednosti .= " 2,";
					$sPolja .= " pdv,";						$sVrednosti .= " " . number_format(20.00,2,'.','') . ",";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(floatval($kolicina), 2, '.', '') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'" . addslashes(Support::encodeTo1250($sifra)) . "','".Support::encodeTo1250($slika)."',1 )");

				}
			}
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i'));
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			self::download();
			$products_file = "files/uspon/uspon.json";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$string = file_get_contents($products_file);
			$products = json_decode($string, true);
	        
	        foreach($products as $product_arr) {
	        	$product = (object) $product_arr;	        	
	        	$sifra = $product->code;
	        	$kolicina = $product->stock_amount;
	        	$cena_nc = $product->b2b_price;

				if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc)){					

					$sPolja = '';
					$sVrednosti = '';

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250($sifra)) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(floatval($kolicina), 2, '.', '') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}
			}

			//Support::queryShortExecute($dobavljac_id, $type);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}
	
	public function download(){
		$json_url = 'https://uspon.rs/ajax/jsonforallproducts';
		$file='files/uspon/uspon.json';
		$username = 'uspon';  // authentication
		$password = 'j5{n+Ump6?@P';  // authentication

		// jSON String for request
		$json_string = '[username]';

		// Initializing curl
		$ch = curl_init( $json_url );

		// Configuring curl options
		$options = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_USERPWD => $username . ':' . $password,  // authentication
		CURLOPT_HTTPHEADER => array('Content-type: application/json') ,
		CURLOPT_FILE => fopen($file, "w"),
		CURLOPT_POSTFIELDS => $json_string
		);

		// Setting curl options
		curl_setopt_array( $ch, $options );

		// Getting results
		$result = curl_exec($ch); // Getting jSON result string
	}	
		public static function dd($arr){	
		echo '<pre>';
		print_r($arr);
		echo '</pre>';
		die();
	}	
}