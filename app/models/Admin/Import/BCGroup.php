<?php
namespace Import;
use Import\Support;
use DB;
use File;

class BCgroup{

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			self::autoDownload(Support::autoLink($dobavljac_id),'files/bcgroup/bcgroup_xml/bcgroup.xml');
			$products_file = "files/bcgroup/bcgroup_xml/bcgroup.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		if($continue){
			Support::initQueryExecute();
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
			$products = simplexml_load_file($products_file, 'SimpleXMLElement', LIBXML_NOCDATA);
				foreach ($products as $product) {
					$sifra 	  	 = (string) $product->id;
					$barkod   	 = (string) $product->ean;
					$grupa 	  	 = (string) $product->category;
					$proizvodjac = (string) $product->brand;
					$naziv 	 	 = (string) $product->name;
					$opis 		 = (string) $product->description;
					$slika		 = (string) $product->image;
					$kolicina	 = (int)    $product->stock;
					$cena_nc 	 = (float)  $product->dealer_price / 1.2;

					if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc) && $cena_nc > 0) {
							
						$sPolja = '';
						$sVrednosti = '';
						$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
						$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" . Support::encodeTo1250($sifra) . "',";
						if($barkod != ""){
							$sPolja .= " barkod,";				$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($barkod)). "',";
						}
						$sPolja .= "grupa,";                    $sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($grupa)) . "',";
						$sPolja .= "proizvodjac,";				$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($proizvodjac)). "',";
						$sPolja .= "naziv,";					$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($naziv)). "',";
						$sPolja .= "opis,";						$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($opis)) . "',";
						if($slika != null){
							$sPolja .= "flag_slika_postoji,";		$sVrednosti .= " 1,";	
							}else{
							$sPolja .= "flag_slika_postoji,";		$sVrednosti .= " 0,";	
							}
						if(!empty($opis)){
							$sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 1,";	
							}else{
							$sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 0,";	
							}
						$sPolja .= "kolicina,";					$sVrednosti .= " '" .$kolicina. "',";	
						$sPolja .= "cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). "";
							
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$sifra."','".addslashes(Support::encodeTo1250($slika))."',1 )");

					}
				}
			Support::queryExecute($dobavljac_id,array('i','u'),array('i','u'),array());
						

			
			//Brisemo fajl
			// if($extension!=null){
			// 	File::delete('files/import.'.$extension);
			// }else{
            //     if(File::exists($products_file)){
            //         File::delete($products_file);
            //     }				
			// }
		}
	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		
		if($extension==null){
			self::autoDownload(Support::autoLink($dobavljac_id),'files/bcgroup/bcgroup_xml/bcgroup.xml');
			$products_file = "files/bcgroup/bcgroup_xml/bcgroup.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		if($continue){
			Support::initQueryExecute();
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}
		
		$products = simplexml_load_file($products_file);
				foreach ($products as $product) {
					$sifra 	  	 = (string) $product->id;
					$kolicina	 = (int)    $product->stock;
					$cena_nc 	 = (float)  $product->dealer_price / 1.2;

					if(isset($sifra) && isset($cena_nc) && is_numeric($cena_nc) && $cena_nc > 0) {
							
						$sPolja = '';
						$sVrednosti = '';
						$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
						$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= "'" . Support::encodeTo1250($sifra) . "',";
						$sPolja .= " kolicina,";				$sVrednosti .= " '" .$kolicina. "',";	
						$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). "";
							
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					}
				}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

	public static function autoDownload($link,$file) {
            file_put_contents($file, file_get_contents($link,false, stream_context_create(array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
            )))));
    }

}
