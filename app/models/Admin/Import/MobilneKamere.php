<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class MobilneKamere {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/mobilnekamere/mobilnekamere_excel/mobilnekamere.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
	        for ($row = 1; $row <= $lastRow; $row++) {

	        	$proizvodjac ="LTL ACORN";
	        	$grupa= "LTL ACORN MOBILNE KAMERE";

	            $sifra = $worksheet->getCell('B'.$row)->getValue();
	            $naziv = $worksheet->getCell('A'.$row)->getValue();
	          	$cena_nc=floatval($worksheet->getCell('I'.$row)->getValue());
	           	$opis = $worksheet->getCell('E'.$row)->getValue();	            
					


	           	if (isset($naziv) && isset($opis) && isset($cena_nc) ) {

	           		
					$sPolja = '';
					$sVrednosti = '';					

					$cena_nc = ($cena_nc * $kurs)/1.2;

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . Support::encodeTo1250($sifra) . "',";	
					$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv). " ',";
					$sPolja .= " opis,";					$sVrednosti .= " '" . Support::encodeTo1250($opis). "',";	
					$sPolja .= " grupa,";					$sVrednosti .= " '" . Support::encodeTo1250($grupa). "',";
					$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . Support::encodeTo1250($proizvodjac). "',";	
					$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00,2,'.','') . ",";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";
					
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}
			}

			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
	
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
		

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/mobilnekamere/mobilnekamere_excel/mobilnekamere.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('B'.$row)->getValue();
	            $naziv = $worksheet->getCell('A'.$row)->getValue();
	          	$cena_nc=floatval($worksheet->getCell('I'.$row)->getValue());
	           	
	           	if (isset($naziv) && isset($opis) && isset($cena_nc)) {

					$sPolja = '';
					$sVrednosti = '';

					$cena_nc = ($cena_nc * $kurs)/1.2;

					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . Support::encodeTo1250($sifra) . "',";	
					$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv). " ',";
					$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";
					
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}
			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}