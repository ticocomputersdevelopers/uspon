<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class AtomPartner {

	public static function ftpConnection() {
		$ftp_server = 'atompartner.rs';
		$ftp_username = 'klikklak@atompartner.rs';
		$ftp_userpass = 'klik@klak16';

		$local_file = 'files/import.xml';
		$server_file = 'klikklak.xml';

		$ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
		$login = ftp_login($ftp_conn, $ftp_username, $ftp_userpass);
		$down = ftp_get($ftp_conn, $local_file, $server_file, FTP_BINARY);
	}

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		
		$extension = 'xml';
		if($extension==null){
			$products_file = "files/atompartner/atompartner_xml/atompartner.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			self::ftpConnection();
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	        
			foreach ($products as $product):
				if(!empty($product->id)){

					$sPolja = '';
					$sVrednosti = '';
				
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->id)) . "',";
					$sPolja .= "naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->name) . " ( " . Support::encodeTo1250($product->part_no) . " )") . "',";
					$sPolja .= "grupa,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->category)) . "',";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . str_replace('>','',$product->stock) . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '', $product->price),1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/atompartner/atompartner_xml/atompartner.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	        
			foreach ($products as $product):
				if(!empty($product->id)){

					$sPolja = '';
					$sVrednosti = '';
				
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->id)) . "',";
					$sPolja .= "kolicina,";					$sVrednosti .= "" . str_replace('>','',$product->stock) . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric(str_replace(',', '', $product->price),1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

}