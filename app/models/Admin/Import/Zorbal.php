<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class Zorbal {

	public static function execute($dobavljac_id,$extension=null){
		if($extension==null){
			$products_file = "files/zorbal/zorbal_excel/zorbal.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
		        
	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
				$cena = $worksheet->getCell('C'.$row)->getValue();
				$rabat = $worksheet->getCell('D'.$row)->getValue();

				if(isset($sifra) && isset($naziv) && isset($cena) && is_numeric($cena)){
					
					if(isset($rabat) && is_numeric($rabat)){
						$cena_nc = round($cena*(1-$rabat),2);
					}else{
						$cena_nc = round($cena,2);
					}

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($sifra)) ."',";
					$sPolja .= "naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($naziv)) ." ( ".addslashes(Support::encodeTo1250($sifra))." )',";
					$sPolja .= "kolicina,";					$sVrednosti .= "1,";			
					$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena_nc . "";
			
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

				}


			}
			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){

		if($extension==null){
			$products_file = "files/zorbal/zorbal_excel/zorbal.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

	        
	        for ($row = 1; $row <= $lastRow; $row++) {
	            $sifra = $worksheet->getCell('A'.$row)->getValue();
	            $naziv = $worksheet->getCell('B'.$row)->getValue();
				$cena = $worksheet->getCell('C'.$row)->getValue();
				$rabat = $worksheet->getCell('D'.$row)->getValue();

				if(isset($sifra) && isset($naziv) && isset($cena) && is_numeric($cena)){
					
					if(isset($rabat) && is_numeric($rabat)){
						$cena_nc = round($cena*(1-$rabat),2);
					}else{
						$cena_nc = round($cena,2);
					}

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($sifra)) ."',";
					$sPolja .= "kolicina,";					$sVrednosti .= "1,";			
					$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena_nc . "";
			
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

				}


			}
			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}