<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use SOAPClient;

class CT {

    public static function execute($dobavljac_id,$kurs=null){
        Support::initIniSetup();
        Support::initQueryExecute();
        if($kurs==null){
            $kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
        }

        $functionGroups = "GetCTProductGroups"; 
        $functionArticles = "GetCTProducts"; 
        $functionGroupsWithAttributes = "GetCTProductGroups_WithAttributes";

        $user_data =array("username" => Support::serviceUsername($dobavljac_id), "password" => Support::servicePassword($dobavljac_id));
        
            $client = new SOAPClient( 
            'http://www.ct4partners.com/ws/ctproductsinstock_v2.asmx?WSDL', 
            array( 
                'location' => 'http://www.ct4partners.com/ws/ctproductsinstock_v2.asmx?WSDL', 
                'trace' => 1, 
                'style' => SOAP_RPC, 
                'use' => SOAP_ENCODED, 
            ) 
            );

            $groups = $client->__soapCall($functionGroups, array("parameters"=>$user_data));
            $groups_with_attributes = $client->__soapCall($functionGroupsWithAttributes, array("parameters"=>$user_data));

            foreach ($groups as $group) {
                $myfile = fopen("files/ct/ct_xml/groups.xml", "w");
                fwrite($myfile, $group);
                fclose($myfile);
            }

            $str=file_get_contents('files/ct/ct_xml/groups.xml');

            //replace something in the file string - this is a VERY simple example
            $str=preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $str);

            //write the entire string
            file_put_contents('files/ct/ct_xml/groups.xml', $str);


            //Citam fajlove
            //$products = simplexml_load_file("articles.xml");
            $groups = simplexml_load_file("files/ct/ct_xml/groups.xml");

            $eng_srb_attr = array();
                foreach ($groups_with_attributes as $groupattr) {

                foreach ($groupattr as $gr) {
                
                    foreach ($gr as $object) {
                        if(isset($object->Attributes) ){                                                
                            
                            $prod=$object->Attributes;
                            if(isset($prod->ProductAttribute))  {                       
                            $prod_attr=$prod->ProductAttribute;
                            foreach ($prod_attr as $attr_g) {
                                if(is_object($attr_g)){
                                $eng_srb_attr['"'.$attr_g->AttributeCode.'"'] = $attr_g->AttributeName;
                            }
                            }   //var_dump($prod->ProductAttribute);die;
                            }
                        }
                    }
                    
                }
                
            }   


            //Citamo vrstu cene za nabavnu cenu
            $valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;

            //read the entire string
            $str=file_get_contents('files/ct/ct_xml/groups.xml');

            //replace something in the file string - this is a VERY simple example
            $str=preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $str);

            //write the entire string
            file_put_contents('files/ct/ct_xml/groups.xml', $str);


            //Citam fajlove
            //$products = simplexml_load_file("articles.xml");
            $groups = simplexml_load_file("files/ct/ct_xml/groups.xml");


            $obradjeni=array();
            $obradjeniSlike=array();

            $associativeArrayGroups = array();
            foreach ($groups as $group) {
                $associativeArrayGroups['"'.$group->Code.'"'] = $group->GroupDescription;
            }

            $functionArticlesAttributes = "GetCTProducts_WithAttributes"; 

            
            foreach ($groups as $group) {
                
                $user_data2 = $user_data;
                $user_data2['productGroupCode'] = $group->Code;

                $products_with_attributes = $client->__soapCall($functionArticlesAttributes, array("parameters"=>$user_data2));
                
                if($products_with_attributes->GetCTProducts_WithAttributesResult == ''){
                    continue;
                }               
                foreach ($products_with_attributes as $a) {
                    $myfile = fopen("files/ct/ct_xml/prod_with_attributes.xml", "w");
                    fwrite($myfile, $a);
                    fclose($myfile);
                }
                
                //read the entire string
                $str=file_get_contents('files/ct/ct_xml/prod_with_attributes.xml');

                //replace something in the file string - this is a VERY simple example
                $str=preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $str);
                

                //write the entire string
                file_put_contents('files/ct/ct_xml/prod_with_attributes.xml', $str);

                $products = simplexml_load_file("files/ct/ct_xml/prod_with_attributes.xml");


                foreach ($products as $product):

                    $karakteristike = array();
                    foreach ($product->DICTIONARY->children() as $item) {
                        if($item->key=='CODE'){
                            $sifra_kod_dobavljaca=$item->value;
                        }
                        if($item->key=='Url'){
                            $key_slika=$item->value;
                        }
                        if($item->key=='MANUFACTURER'){
                            $proizvodjac=$item->value;
                        }
                        if($item->key=='RETAILPRICE'){
                            $pmp_cena=str_replace(',', '.', $item->value);
                        }
                        if($item->key=='PRODUCTGROUPCODE'){
                            $grupa_key=$item->value;
                        }
                        if($item->key=='ManufacturerTSID'){
                            $ManufacturerTSID=$item->value;
                        }
                        if($item->key=='ProductTSID'){
                            $ProductTSID=$item->value;
                        }
                        if($item->key=='TYPE_AVO'){
                            $TYPE_AVO=$item->value;
                        }
                        if($item->key=='DESCRIPT_SIMCARD'){
                            $DESCRIPT_SIMCARD=$item->value;
                        }
                        if($item->key=='NAME'){
                            $naziv=$item->value;
                        }
                        if($item->key=='QTTYINSTOCK'){
                            $kol=str_replace(array('>','&gt;'),array('',''),$item->value);

                            if($kol==0){
                                $kolicina=0;
                            }else{
                                $kolicina=intval($kol);
                            }
                        } 
                        if($item->key=='SHORT_DESCRIPTION'){
                            $opis_dobavljac=nl2br($item->value);
                        }
                        if($item->key=='BARCODE'){
                            $barcode=$item->value;
                        }
                        if($item->key=='USB_DICT'){
                            $usb_dict=$item->value;
                        }
                        if($item->key=='HEATING_TYP'){
                            $HEATING_TYP=$item->value;
                        }
                        if($item->key=='TYP_HEATING'){
                            $TYP_HEATING=$item->value;
                        }
                        if($item->key=='Bar Code'){
                            $barcode2=$item->value;
                        }
                        if($item->key=='AIRPURIFOP_AIRO'){
                            $airo=$item->value;
                        }
                        if($item->key=='TAX'){
                            $pdv=str_replace(',', '.', $item->value);
                        }
                        if($item->key=='PRICE'){
                            $ncena=str_replace(',', '.', $item->value);
                        }
                        if($item->key=='IMAGE_URL'){
                            $images = $item->value;
                        }
                        if($item->key=='ACS_TIPE'){
                            $acs=$item->value;
                        }
                        if($item->key=='OTHER_BEAUTYCARE'){
                            $other=$item->value;
                        }
                        if($item->key=='ADD_VACUMCLACC'){
                            $vacum=$item->value;
                        }
                        if($item->key=='EUR_ExchangeRate'){
                            $kurs_eura = number_format((double)str_replace(',', '.', $item->value),2,'.','');
                        }
 
                        if($item->key!='CODE' && $item->key!='MANUFACTURER' && $item->key!='TYP_HEATING' && $item->key!='HEATING_TYP' && $item->key!='Url' && $item->key!='ManufacturerTSID' && $item->key!='USB_DICT' && $item->key!='ProductTSID' && $item->key!='OTHER_BEAUTYCARE' && $item->key!='RETAILPRICE' && $item->key!='AIRPURIFOP_AIRO' && $item->key!='ACS_TIPE' && $item->key!='ADD_VACUMCLACC' && $item->key!='PRODUCTGROUPCODE' && $item->key!='NAME' && $item->key!='DESCRIPT_SIMCARD' && $item->key!='QTTYINSTOCK' && $item->key!='SHORT_DESCRIPTION' && $item->key!='Bar Code' && $item->key!='BARCODE' && $item->key!='TAX' && $item->key!='PRICE' && $item->key!='IMAGE_URL' && $item->key!='MANUFACTURERCODE' && $item->key!='EUR_ExchangeRate' && $item->key!='ItemDescription' && $item->key!='UnitVolume' && $item->key!='GrossWeight' && $item->key!='NetWeight' && $item->key!='Length' && $item->key!='Width' && $item->key!='Height' && $item->key!='WARRANTY'){

                            if(!empty($item->value))
                                $karakteristike['"'.$item->key.'"']=$item->value;
                        }

                    }
                    
                    if(!empty($sifra_kod_dobavljaca)){

                        $flag_slika_postoji = "0";
                        $i = 0;
                        if($images!=''){

                            foreach ($images as $slika):

                                if(!in_array(Support::quotedStr($sifra_kod_dobavljaca),$obradjeniSlike,false)){
                                    $sqlSlikaInsert = "INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra_kod_dobavljaca).",'".$slika."',1 )";
                                }else{
                                    $sqlSlikaInsert = "INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra_kod_dobavljaca).",'".$slika."',0 )";
                                }
                                DB::statement($sqlSlikaInsert);
                                $i++;
                                $flag_slika_postoji = "1";
                            endforeach; 
                            $obradjeniSlike[]=Support::quotedStr($sifra_kod_dobavljaca);

                        }
                    
                        if(!in_array(Support::quotedStr($sifra_kod_dobavljaca),$obradjeni,false)){
                            $obradjeni[]=Support::quotedStr($sifra_kod_dobavljaca);

                            $flag_opis_postoji = "0";
                            foreach ($karakteristike as $key => $value){
                            
                            
                                DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp(partner_id,sifra_kod_dobavljaca,karakteristika_naziv,karakteristika_vrednost)"
                                    ."VALUES(".$dobavljac_id."," . Support::quotedStr($sifra_kod_dobavljaca) . ",".Support::quotedStr(addslashes(Support::encodeTo1250(str_replace('"', '', $eng_srb_attr[$key])))).","
                                    ." ".Support::quotedStr(addslashes(Support::encodeTo1250($value))).") ");
                                $flag_opis_postoji = "1";
                            }


                            $sPolja = '';
                            $sVrednosti = '';

                            $sPolja .= "partner_id,";               $sVrednosti .= "" . $dobavljac_id . ",";
                            $sPolja .= "sifra_kod_dobavljaca,";     $sVrednosti .= "" . Support::quotedStr($sifra_kod_dobavljaca) . ",";
                            if($proizvodjac!=''){
                                $sPolja .= "proizvodjac,";              $sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($proizvodjac)) . ",";
                            }
                            if($naziv!=''){
                                $sPolja .= "naziv,";                    $sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($naziv)) . "' ( '".Support::quotedStr($sifra_kod_dobavljaca)."' ) '" . ",";    
                            }
                            if($associativeArrayGroups['"'.$grupa_key.'"']!=''){
                                $sPolja .= "grupa,";                    $sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($associativeArrayGroups['"'.$grupa_key.'"'])) . ",";
                            }
                            if($opis_dobavljac!=''){
                                //$flag_opis_postoji = "1";
                                //$sPolja .= "opis,";                   $sVrednosti .= "" . Support::quotedStr(addslashes(Support::encodeTo1250($opis_dobavljac))) . ",";
                            }else{
                                //$flag_opis_postoji = "0";
                            }
                            
                            if($barcode!=''){
                                $sPolja .= "barkod,";                   $sVrednosti .= "" . Support::quotedStr($barcode) . ",";
                            }
                            if($pdv!=''){
                                $sPolja .= "pdv,";                      $sVrednosti .= "" . $pdv . ",";
                            }
                            $sPolja .= "kolicina,";                 $sVrednosti .= "" . $kolicina . ",";
                            $sPolja .= "web_flag_karakteristike,";  $sVrednosti .= " 2,";
                            $sPolja .= "flag_slika_postoji,";       $sVrednosti .= "" . $flag_slika_postoji . ",";
                            $sPolja .= "pmp_cena,";                 $sVrednosti .= "" . number_format((double) Support::replace_empty_numeric($pmp_cena,1,$kurs,$valuta_id_nc),2, '.', '') . ",";
                            $sPolja .= "cena_nc";                   $sVrednosti .= "" . number_format((double) Support::replace_empty_numeric($ncena,2,$kurs_eura,$valuta_id_nc),2, '.', '') . "";  
                            
                            DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
 
                        }
                    }
                endforeach;
                
            }

            Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
    }

    public static function executeShort($dobavljac_id,$kurs=null){

        Support::initIniSetup();
        Support::initQueryExecute();

        if($kurs==null){
            $kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
        }
        $valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;

        $user_data =array("username" => Support::serviceUsername($dobavljac_id), "password" => Support::servicePassword($dobavljac_id));
        
        $client = new SOAPClient( 
            'http://www.ct4partners.com/ws/ctproductsinstock_v2.asmx?WSDL', 
            array( 
                'location' => 'http://www.ct4partners.com/ws/ctproductsinstock_v2.asmx?WSDL', 
                'trace' => 1, 
                'style' => SOAP_RPC, 
                'use' => SOAP_ENCODED, 
            ) 
        );

        $products = $client->__soapCall("GetCTProducts", array("parameters"=>$user_data));

        if($products->GetCTProductsResult != ''){

            foreach ($products as $product) {

                    $myfile = fopen("files/ct/ct_xml/product_price.xml", "w");
                    fwrite($myfile, $product);
                    fclose($myfile);
            }

            $str=file_get_contents('files/ct/ct_xml/product_price.xml');
            $str=preg_replace('/(<\?xml[^?]+?)utf-16/i', '$1utf-8', $str);                          
            file_put_contents('files/ct/ct_xml/product_price.xml', $str);
            $products = simplexml_load_file("files/ct/ct_xml/product_price.xml");

            $obradjeni=array();
            foreach ($products as $product) {



                foreach ($product->DICTIONARY->children() as $item) {
                    if($item->key=='CODE'){
                        $sifra_kod_dobavljaca=$item->value;
                    }
                    
                    if($item->key=='RETAILPRICE'){
                        $pmp_cena=str_replace(',', '.', $item->value);
                    }                                   
                    
                    if($item->key=='QTTYINSTOCK'){
                        $kol=str_replace(array('>','&gt;'),array('',''),$item->value);

                        if($kol==0){
                            $kolicina=0;
                        }else{
                            $kolicina=intval($kol);
                        }
                    }
                    
                    if($item->key=='TAX'){
                        $pdv=str_replace(',', '.', $item->value);
                    }
                    if($item->key=='PRICE'){
                        $ncena=str_replace(',', '.', $item->value);
                    }
                    
                    if($item->key=='EUR_ExchangeRate'){
                        $kurs_eura = number_format((double)str_replace(',', '.', $item->value),2,'.','');
                    }                                   

                }
                
                if(!empty($sifra_kod_dobavljaca)){

                    
                    if(!in_array(Support::quotedStr($sifra_kod_dobavljaca),$obradjeni,false)){
                        $obradjeni[]=Support::quotedStr($sifra_kod_dobavljaca);

                        $sPolja = '';
                        $sVrednosti = '';

                        $sPolja .= "partner_id,";               $sVrednosti .= "" . $dobavljac_id . ",";
                        $sPolja .= "sifra_kod_dobavljaca,";     $sVrednosti .= "" . Support::quotedStr($sifra_kod_dobavljaca) . ",";
                        if($pdv!=''){
                            $sPolja .= "pdv,";                  $sVrednosti .= "" . $pdv . ",";
                        }
                        $sPolja .= "kolicina,";                 $sVrednosti .= "" . $kolicina . ",";
                        
                        $sPolja .= "pmp_cena,";                 $sVrednosti .= "" . number_format((double) Support::replace_empty_numeric($pmp_cena,1,$kurs,$valuta_id_nc),2, '.', '') . ",";   
                        $sPolja .= "cena_nc";                   $sVrednosti .= "" . number_format((double) Support::replace_empty_numeric($ncena,2,$kurs_eura,$valuta_id_nc),2, '.', '') . "";  
                        
                        DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

                    }
                }
                
            }
            return true;
        }else{
            return false;
        }

        //Support::queryShortExecute($dobavljac_id);
    }

}

?>