<?php
namespace Import;
use Import\Support;
use DB;
use File;

class MaliKucniAparati {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/malikucni/malikucni_json/malikucni.json";
			$token = self::autorize();
			$artikli = self::artikli($token,$products_file);
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}
		if($continue){
			Support::initQueryExecute();
			$string = file_get_contents($products_file);
			$products = json_decode($string, true);
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			foreach ($products['data'] as $product) {
				$sifra = $product['id'];
				$barkod = $product['attributes']['barkod'];
				$naziv = $product['attributes']['naziv'];
				$proizvodjac = $product['attributes']['proizvodjac'];
				$kolicina =(int) $product['attributes']['kolicina'];
				$grupa = $product['attributes']['vrsta'];
				$cena_nc = (float) $product['attributes']['cenarabat'];
				$pmp_cena = (float) $product['attributes']['cenamp'];

				if(isset($sifra) && !empty($sifra) && isset($naziv) && isset($cena_nc) && is_numeric($cena_nc)){

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($sifra)) . "',";
					if($barkod != ""){
							$sPolja .= " barkod,";			$sVrednosti .= " '" . Support::encodeTo1250(pg_escape_string($barkod)). "',";
						}
					$sPolja .= " naziv,";					$sVrednosti .= " '" . pg_escape_string(addslashes(Support::encodeTo1250($naziv))) . "',";
					$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . pg_escape_string(addslashes(Support::encodeTo1250($proizvodjac))) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . $kolicina  . ",";
					$sPolja .= " grupa,";					$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($grupa)) . "',";
					
					$sPolja .= " pmp_cena,";				$sVrednosti .= " 0,";
					$sPolja .= "cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). "";
					

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					}
				

			}

			}
			Support::queryExecute($dobavljac_id,array('i','u'),array('i','u'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/malikucni/malikucni_json/malikucni.json";
			$token = self::autorize();
			$artikli = self::artikli($token,$products_file);
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();
			$string = file_get_contents($products_file);
			$products = json_decode($string, true);
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        foreach ($products['data'] as $product) {
				$sifra = $product['id'];
				$kolicina =(int) $product['attributes']['kolicina'];
				$cena_nc = (float) $product['attributes']['cenarabat'];
				$pmp_cena = (float) $product['attributes']['cenamp'];

				if(isset($sifra) && !empty($sifra)  && isset($cena_nc) && is_numeric($cena_nc)){

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
					$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . pg_escape_string(Support::encodeTo1250($sifra)) . "',";
					$sPolja .= " kolicina,";				$sVrednosti .= " " . $kolicina  . ",";
					$sPolja .= " pmp_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($pmp_cena,1,$kurs,$valuta_id_nc),2, '.', '') . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', ''). "";
					

					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

					}
				

			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

	public static function autorize(){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://portal.wings.rs/api/v1/malikucniaparati/system.user.log',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>'{
		    "aUn": "usPon88",
		    "aUp": "ca9uS"
		}',
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/json',
		    'Cookie: PHPSESSID=rvnat0s1iv1mtsi87cq9gl2sj0'
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	      $result = json_decode($response);
	      return $result->data[0]->attributes->token;
	    }	
			
	}

		public static function artikli($access_token,$file){

		$url = "https://portal.wings.rs/api/v1/malikucniaparati/local.cache.artikal?dLength=1&partnerId=14504";
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_TIMEOUT => 300000,
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
	    	$total = json_decode($response)->meta->total;
	    }

		$url = 'https://portal.wings.rs/api/v1/malikucniaparati/local.cache.artikal?dLength='.$total.'&partnerId=14504';
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_TIMEOUT => 300000,
		  CURLOPT_HTTPHEADER => array(
		    "Cookie: PHPSESSID=".$access_token.""
		  ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return false;
	    } else {
		    $fp = fopen($file, 'w');
		    fwrite($fp, $response);
		    fclose($fp);
	    }		
	}

}