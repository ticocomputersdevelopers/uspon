<?php
namespace Import;
use Import\Support;
use DB;
use File; 
use PHPExcel; 
use PHPExcel_IOFactory;

class Element {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			self::download();
			self::download_categories();
			$products_file = "files/element/element_xml/element.xml";
			$products_file_categories = "files/element/element_xml/element_categories.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}


		$products_2 = simplexml_load_file($products_file);
		$products_2 = $products_2->xpath('//product');

		$categories = simplexml_load_file($products_file_categories);
		$categories = $categories->xpath('//Kat');
		
		$mappedCategories = array();
		foreach ($categories as $key => $category) {
			$mappedCategories[(int)$category->attributes()->KatID] = (string)$category->attributes()->Naziv;
		}

		foreach($products_2 as $product):

			$products = (object) $product;
			$sifra= $products->textId;
			$grupa_id =(int) $products->parent;
			if (array_key_exists($grupa_id, $mappedCategories)) {
				$grupa = $mappedCategories[$grupa_id];
			}else{
				$grupa = '';
			}
			$naziv_artikla= $products->naziv.' ('.$sifra.')';			
			$cena = $products->netoCena;
			$cena = str_replace(".00.00", ".00", $cena);
			$kolicina= $products->lagerVp;
			$ean= $products->barkod;
			$proiz= $products->proizvodjac;
			$opis = $products->specifications;
			$opis_html = $products->opis;
			$opis_html = str_replace("http:www", "http://www", $opis_html);
			$cena_akc = $products->akcijskaCena;
			$pmpcena = $products->cena;
			$min_kol =  $products->paket;
			
			$slika= $products->slika;			
			$slika2= $products->slika2;
			$slika3= $products->slika3;
			$slika4= $products->slika4;
			
			$value = $opis->value;
			$total = count($value);


		   	for ($i=0; $i < $total; $i++) { 
				foreach($opis as $atribute){

				$naziv = $atribute->attribute[$i]->attributes()->name;

					foreach ($opis as $atribute1) {	
						$value = $atribute1->value[$i];					
					
					DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp(partner_id,sifra_kod_dobavljaca,karakteristika_naziv,karakteristika_vrednost)"
					."VALUES(".$dobavljac_id."," . Support::quotedStr($sifra) . ",".Support::quotedStr(Support::encodeTo1250($naziv)).","
					." ".Support::quotedStr(Support::encodeTo1250($value)).") ");
							}
				}
			}
			$sPolja = '';
			$sVrednosti = '';

			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($sifra) . ",";
			$sPolja .= "barkod,";					$sVrednosti .= "" . Support::quotedStr($ean) . ",";
			$sPolja .= "proizvodjac,";				$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250(pg_escape_string($proiz))) . ",";
			if(isset($opis_html) AND !empty($opis_html)){
			$sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 1,";	
			$sPolja .= "opis,";						$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250(pg_escape_string($opis_html))) . ",";
			}
			$sPolja .= "kolicina,";					$sVrednosti .= "" . Support::quotedStr($kolicina) . ",";
			$sPolja .= "naziv,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250(pg_escape_string($naziv_artikla))) . ",";
			$sPolja .= "grupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250(pg_escape_string($grupa))) . ",";
			if(isset($slika) AND !empty($slika)){
			$sPolja .= "flag_slika_postoji,";		$sVrednosti .= " 1,";	
			}else{
			$sPolja .= "flag_slika_postoji,";		$sVrednosti .= " 0,";	
			}
			$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 2,";
			$sPolja .= "komentar,";					$sVrednosti .= " '" . $min_kol . "',";
			$sPolja .= "pmp_cena,";					$sVrednosti .= "" . $pmpcena . ",";
			if($cena_akc > 0 AND !empty($cena_akc)){
			$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena_akc . "";
			}else{
			$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena . "";	
			}

			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");			
			
			DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".$slika."',1 )");
			if(isset($slika2) AND !empty($slika2)){
			DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".$slika2."',0 )");
			}
			if(isset($slika3) AND !empty($slika3)){
			DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".$slika3."',0 )");
			}	
			if(isset($slika4) AND !empty($slika4)){	
			DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".$slika4."',0 )");	
			}
			endforeach;
			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
	
		$file_name = Support::file_name("files/element/element_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/element/element_xml/".$file_name);
		}
	}
}


	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
				
		if($extension==null){
			self::download();
			$products_file = "files/element/element_xml/element.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}


		$products_2 = simplexml_load_file($products_file);
		$products_2 = $products_2->xpath('//product');

		foreach($products_2 as $product):
			$products = (object) $product;

			$sifra= $products->textId;				
			$cena = $products->netoCena;
			$cena = str_replace(".00.00", ".00", $cena);
			$kolicina= $products->lagerVp;			
			$pmpcena = $products->cena;
			$cena_akc = $products->akcijskaCena;

			
			$sPolja = '';
			$sVrednosti = '';

			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr($sifra) . ",";
			$sPolja .= "kolicina,";					$sVrednosti .= "" . Support::quotedStr($kolicina) . ",";
			$sPolja .= "pmp_cena,";					$sVrednosti .= "" . $pmpcena . ",";
			if($cena_akc > 0 AND !empty($cena_akc)){
			$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena_akc . "";
			}else{
			$sPolja .= "cena_nc";					$sVrednosti .= "" . $cena . "";	
			}
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	
		
			endforeach;
	
				$file_name = Support::file_name("files/element/element_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/element/element_xml/".$file_name);
		}
	}
}
public static function download(){
    $userAgent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36';
    $cookieFile = 'cookie.txt';
	$file='files/element/element_xml/element.xml';
	
	$curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://www.elementa.rs/index/product-list-xml-dkwidlvehmf');
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
    curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_FILE, fopen($file, "w"));
    curl_exec($curl);
    curl_close($curl);
}

public static function download_categories(){
    $userAgent = 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.2309.372 Safari/537.36';
    $cookieFile = 'cookie.txt';
	$file='files/element/element_xml/element_categories.xml';
	
	$curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://www.elementa.rs/index/category-list-xml');
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($curl, CURLOPT_COOKIEJAR, $cookieFile);
    curl_setopt($curl, CURLOPT_USERAGENT, $userAgent);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_FILE, fopen($file, "w"));
    curl_exec($curl);
    curl_close($curl);
}
}