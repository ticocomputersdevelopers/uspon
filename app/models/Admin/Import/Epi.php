<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Epi {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/epi/epi_xml/epi.xml');
			$products_file = "files/epi/epi_xml/epi.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	
	        
			foreach ($products as $product):
				$sPolja = '';
				$sVrednosti = '';

				$naziv=$product->ARTIKAL;		
				$grupa=$product->KATEGORIJA;		
				$podgrupa = $product->PODKATEGORIJA;		
				$kolicina=$product->KOLICINA;	
				$sifra=$product->SIFRA;
				$cena = $product->CENA;

				$cena= $cena*$kurs;

				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";	
				$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv). "',";
				$sPolja .= " grupa,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($grupa)) . "',";
				$sPolja .= " podgrupa,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($podgrupa)) . "',";
				$sPolja .= " cena_nc,";					$sVrednosti .= " " . Support::replace_empty_numeric(number_format((float)$cena, 2, '.', ''),1,'',''). ",";
				$sPolja .= " kolicina";					$sVrednosti .= " " . $kolicina. "";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/epi/epi_xml/epi.xml');
			$products_file = "files/epi/epi_xml/epi.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			}
		$products = simplexml_load_file($products_file);	
			foreach ($products as $product):

				$sPolja = '';
				$sVrednosti = '';

				$naziv=$product->ARTIKAL;		
				$grupa=$product->KATEGORIJA;		
				$podgrupa = $product->PODKATEGORIJA;		
				$kolicina=$product->KOLICINA;	
				$sifra=$product->SIFRA;
				$cena = $product->CENA;

				$cena= $cena*$kurs;

				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $sifra . "',";
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . $cena . ",";
				$sPolja .= "kolicina";					$sVrednosti .= "" .$kolicina. "";
						
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	


}