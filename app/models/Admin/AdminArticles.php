<?php
use Service\Drip;

class AdminArticles {

	public static function fetchAll($criteria, $pagination=null, $sort=null)
	{	
		$level2= array();
		if($criteria['grupa_pr_id'] == 0){
			$level2[] = -1;
		}
		AdminCommon::allGroups($level2,$criteria['grupa_pr_id']);
		if(count($level2) <= 0){
			$level2 = array(0,-1);
		}
		
		$select="SELECT DISTINCT r.roba_id, r.datum_akcije_do, r.flag_cenovnik,r.sifra_is,labela, r.sifra_d, naziv, r.sku, proizvodjac_id, dobavljac_id, (SELECT naziv FROM partner WHERE partner_id = r.dobavljac_id) as dobavljac, flag_aktivan, flag_prikazi_u_cenovniku, flag_zakljucan, akcija_flag_primeni, b2b_akcija_flag_primeni, tarifna_grupa_id, garancija, produzena_garancija, web_opis, web_flag_karakteristike, tip_cene, web_cena, mpcena,tezinski_faktor,tags, CASE WHEN (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) is null then 0 ELSE (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) END as kolicina, racunska_cena_nc, racunska_cena_end, end_marza, web_marza, mp_marza, r.grupa_pr_id, (SELECT ROUND(SUM(kolicina)) FROM dobavljac_cenovnik where dobavljac_cenovnik.roba_id = r.roba_id) as dobavljac_kolicina, (SELECT COUNT(web_slika_id) FROM web_slika ws where ws.roba_id = r.roba_id) as br_slika, r.barkod, CASE WHEN r.barkod IS NULL THEN 0 ELSE 1 END AS sort_expression_barkod
		FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id";

		if(isset($criteria['magacin']) && $criteria['magacin'] != '0'){
			$select .= " LEFT JOIN lager l ON l.roba_id = r.roba_id";
		}

		$where = " WHERE r.roba_id <> -1 AND ( r.grupa_pr_id IN(".implode(",",$level2).") OR rg.grupa_pr_id IN(".implode(",",$level2)."))";	


		if(isset($criteria['proizvodjac']) && $criteria['proizvodjac'] != '0'){
			$proizvodjaci = str_replace("+", ", ", $criteria['proizvodjac']);
			$where .= " AND proizvodjac_id IN (". $proizvodjaci .")";
		}
		if(isset($criteria['dobavljac']) && $criteria['dobavljac'] != '0'){
			$dobavljaci = str_replace("-", ", ", $criteria['dobavljac']);
			$where .= " AND dobavljac_id IN (". $dobavljaci .")";
		}
		if(isset($criteria['tip']) && $criteria['tip'] != '0'){
			$tipovi = str_replace("-", ", ", $criteria['tip']);
			$where .= " AND tip_cene IN (". $tipovi .")";
		}
		if(isset($criteria['labela']) && $criteria['labela'] != '0'){
			$labela = str_replace("-", ", ", $criteria['labela']);
			$where .= " AND labela IN (". $labela .")";
		}


		if(isset($criteria['karakteristika']) && $criteria['karakteristika'] != '0'){
			
			if(strpos($criteria['karakteristika'],'-') !== false ){
			$where .= " AND NOT EXISTS (SELECT roba_id FROM web_roba_karakteristike WHERE roba_id = r.roba_id AND grupa_pr_naziv_id = ".str_replace('-','',$criteria['karakteristika']).")";
			}else{			
			$where .= " AND EXISTS (SELECT roba_id FROM web_roba_karakteristike WHERE roba_id = r.roba_id AND grupa_pr_naziv_id = ".$criteria['karakteristika'].")";				
			}
		}

		if(isset($criteria['magacin']) && $criteria['magacin'] != '0'){
			$where .= " AND l.poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND l.orgj_id IN (". $criteria['magacin'] .")";
		}
		// else{
		// 	$where .= " AND ((l.poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND (l.orgj_id IN (SELECT orgj_id FROM imenik_magacin WHERE izabrani=1) OR l.orgj_id IS NULL)) OR l.roba_id IS NULL)";
		// }
		// else{
		// 	$where .= " AND (l.poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) OR l.orgj_id IS NULL)";
		// }

		if(isset($criteria['exporti']) && $criteria['exporti'] != '0'){
			if(strpos($criteria['exporti'],'-') !== false && (strpos($criteria['exporti'],'0') === false || strpos($criteria['exporti'],'0') > 1)) {
				$where .= " AND r.roba_id NOT IN (SELECT roba_id FROM roba_export WHERE export_id = ". str_replace('-','',$criteria['exporti']) .")";
			}
			elseif(strpos($criteria['exporti'],'-') === false){
				$where .= " AND r.roba_id IN (SELECT roba_id FROM roba_export WHERE export_id = ". $criteria['exporti'] .")";
			}
		}
		if(isset($criteria['katalog']) && $criteria['katalog'] != '0'){
			if(strpos($criteria['katalog'],'-') !== false && strpos($criteria['katalog'],'0') === false){
				$where .= " AND r.roba_id NOT IN (SELECT roba_id FROM katalog_roba WHERE katalog_id = ". str_replace('-','',$criteria['katalog']) .")";
			}
			elseif(strpos($criteria['katalog'],'-') === false){
				$where .= " AND r.roba_id IN (SELECT roba_id FROM katalog_roba WHERE katalog_id = ". $criteria['katalog'] .")";
			}
		}

		if(isset($criteria['filteri'])){
			$filteri = explode("-", $criteria['filteri']);
			if($filteri[0] == "0"){
				$where .= " AND (web_opis IS NULL OR web_opis = '')";
			}
			if($filteri[0] == "1"){
				$where .= " AND web_opis IS NOT NULL AND web_opis <> ''";
			}
			if($filteri[1] == "0"){
				if($criteria['flag'] == 1){
					$where .= " AND web_flag_karakteristike <> 0";
				}else{
					$where .= " AND (web_karakteristike = '' OR web_karakteristike IS NULL)";
				}
			}
			if($filteri[1] == "1"){
				if($criteria['flag'] == 1){
					$where .= " AND web_flag_karakteristike = 0";
				}else{
					$where .= " AND web_karakteristike IS NOT NULL AND web_karakteristike <> ''";
				}
			}
			if($filteri[2] == "0"){
				if($criteria['flag'] == 1){
					$where .= " AND web_flag_karakteristike <> 1";
				}else{
					$where .= " AND r.roba_id NOT IN (SELECT DISTINCT roba_id FROM web_roba_karakteristike)";
				}
			}
			if($filteri[2] == "1"){
				if($criteria['flag'] == 1){
					$where .= " AND web_flag_karakteristike = 1";
				}else{
					$where .= " AND r.roba_id IN (SELECT DISTINCT roba_id FROM web_roba_karakteristike)";
				}
			}
			if($filteri[3] == "0"){
				if($criteria['flag'] == 1){
					$where .= " AND web_flag_karakteristike <> 2";
				}else{
					$where .= " AND r.roba_id NOT IN (SELECT DISTINCT roba_id FROM dobavljac_cenovnik_karakteristike WHERE roba_id <> -1)";
				}
			}
			if($filteri[3] == "1"){
				if($criteria['flag'] == 1){
					$where .= " AND web_flag_karakteristike = 2";
				}else{
					$where .= " AND r.roba_id IN (SELECT DISTINCT roba_id FROM dobavljac_cenovnik_karakteristike WHERE roba_id <> -1)";
				}
			}
			if($filteri[4] == "0"){
				$where .= " AND (SELECT COUNT(web_slika_id) FROM web_slika ws where ws.roba_id = r.roba_id) = 0";
			}
			if($filteri[4] == "1"){
				$where .= " AND (SELECT COUNT(web_slika_id) FROM web_slika ws where ws.roba_id = r.roba_id) > 0";
			}
			if($filteri[5] == "0"){
				if(isset($criteria['magacin']) && $criteria['magacin'] != '0'){
					$where .= " AND ((l.poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND kolicina = 0) OR kolicina IS NULL AND orgj_id != 1)";
				}else{
					$where .= " AND ((SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) = 0 OR (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id) IS NULL)";
				}
			}
			if($filteri[5] == "1"){
				if(isset($criteria['magacin']) && $criteria['magacin'] != '0'){
					$where .= " AND l.poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND kolicina > 0 AND orgj_id != 1";
				}else{
					$where .= " AND (SELECT ROUND(SUM(kolicina)) FROM lager where roba_id = r.roba_id AND orgj_id != 1) > 0";
				}
			}
			if($filteri[6] == "0"){
				$where .= " AND akcija_flag_primeni = 0";
			}
			if($filteri[6] == "1"){
				$where .= " AND akcija_flag_primeni = 1";
			}
			if($filteri[7] == "0"){
				$where .= " AND flag_zakljucan = false";
			}
			if($filteri[7] == "1"){
				$where .= " AND flag_zakljucan = true";
			}
			if($filteri[8] == "0"){
				$where .= " AND flag_aktivan <> 1";
			}
			if($filteri[8] == "1"){
				$where .= " AND flag_aktivan = 1";
			}
			if($filteri[9] == "0"){
				$where .= " AND flag_prikazi_u_cenovniku <> 1";
			}
			if($filteri[9] == "1"){
				$where .= " AND flag_prikazi_u_cenovniku = 1";
			}
			if($filteri[10] == "0"){
				$where .= " AND (SELECT SUM(kolicina) FROM dobavljac_cenovnik where dobavljac_cenovnik.roba_id = r.roba_id) = 0";
			}
			if($filteri[10] == "1"){
				$where .= " AND (SELECT SUM(kolicina) FROM dobavljac_cenovnik where dobavljac_cenovnik.roba_id = r.roba_id) > 0";
			}
			if($filteri[11] == "0"){
				$where .= " AND flag_cenovnik <> 1";
			}
			if($filteri[11] == "1"){
				$where .= " AND flag_cenovnik = 1";
			}
			if($filteri[12] == "0"){
				$where .= " AND tezinski_faktor = 0";
			}
			if($filteri[12] == "1"){
				$where .= " AND tezinski_faktor > 0";
			}
			if($filteri[13] == "0"){
				$where .= " AND (deklaracija IS NULL OR trim(deklaracija) = '')";
			}
			if($filteri[13] == "1"){
				$where .= " AND deklaracija IS NOT NULL AND trim(deklaracija) <> ''";
			}
			if($criteria['flag'] == 0){
				if($filteri[14] == "0"){
					$where .= " AND garancija = 0";
				}
				if($filteri[14] == "1"){
					$where .= " AND garancija > 0";
				}
			}elseif ($criteria['flag'] == 1) {
				if($filteri[14] == "0"){
					$where .= " AND produzena_garancija = 0";
				}
				if($filteri[14] == "1"){
					$where .= " AND produzena_garancija > 0";
				}
			}
			if($filteri[15] == "0"){
				$where .= " AND b2b_akcija_flag_primeni = 0";
			}
			if($filteri[15] == "1"){
				$where .= " AND b2b_akcija_flag_primeni = 1";
			}
			if($filteri[16] == "0"){
				$where .= " AND (visina is null or visina = 0) ";
			}
			if($filteri[16] == "1"){
				$where .= " AND (visina > 0)";
			}
			if($filteri[17] == "0"){
				$where .= " AND (sirina is null or sirina = 0)";
			}
			if($filteri[17] == "1"){
				$where .= " AND (sirina > 0)";
			}
			if($filteri[18] == "0"){
				$where .= " AND (duzina is null or duzina = 0)";
			}
			if($filteri[18] == "1"){
				$where .= " AND (duzina > 0)";
			}
			if($filteri[19] == "0"){
				$where .= " AND (bruto_masa is null or bruto_masa = 0)";
			}
			if($filteri[19] == "1"){
				$where .= " AND (bruto_masa > 0)";
			}
			if($filteri[20] == "0"){
				$where .= " AND jm_mase = -1";
			}
			if($filteri[20] == "1"){
				$where .= " AND jm_mase <> -1";
			}

		}

		if(isset($sort)){
			$sort = explode("-", $sort);
			if($sort[0] == 'r.barkod') {
				$order = " ORDER BY sort_expression_barkod ".$sort[1].", r.barkod ".$sort[1];
			} else {
				$order = " ORDER BY ".$sort[0]." ".$sort[1]."";
			}
		}else{
			$order = " ORDER BY kolicina DESC";
		}
		
		if(isset($criteria['search']) && $criteria['search'] != '0'){
			$search_string = $criteria['search'];
			
				$input_arr = explode('+',$search_string);
				$search_arr = $input_arr;
				
				foreach($input_arr as $word){
					$search_arr[] = strtoupper($word);
				}
				$search_arr = array_unique($search_arr);
				$where .= " AND (";
				$where .= AdminSupport::searchQueryString($search_arr,'r.roba_id::varchar');
                $where .= " OR ";
				$where .= AdminSupport::searchQueryString($search_arr,'naziv');
				$where .= " OR ";
				$where .= AdminSupport::searchQueryString($search_arr,'sku');
				$where .= " OR ";
				$where .= AdminSupport::searchQueryString($search_arr,'r.sifra_is');
				$where .= " OR ";
				$where .= AdminSupport::searchQueryString($search_arr,'r.id_is');
				$where .= " OR ";
				$where .= AdminSupport::searchQueryString($search_arr,'sifra_d');
				$where .= " OR ";
				$where .= AdminSupport::searchQueryString($search_arr,'r.barkod');
				$where .= ")";
			
		}

        if(isset($criteria['nabavna'])){
            $nabavna_arr = explode('-',$criteria['nabavna']);
            if(is_numeric($nabavna_arr[0]) && is_numeric($nabavna_arr[1])){
                $where .= " AND racunska_cena_nc BETWEEN ".$nabavna_arr[0]." AND ".$nabavna_arr[1]."";
            }
        }

		if(is_array($pagination)){
			$pagination = " LIMIT ".$pagination['limit']." OFFSET ".$pagination['offset']."";
		}else{
			$pagination = "";
		}

		return DB::select($select.$where.$order.$pagination);
	}

	public static function find($roba_id,$column,$magacin_id=null){
		
		if($column == 'kolicina' || $column == 'orgj_id' || $column == 'poslovna_godina_id'){
			$godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
			$default_magacin_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
			if($magacin_id==0){
				if($column == 'kolicina'){
					$result = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$godina_id))->sum('kolicina');
				}elseif($column == 'orgj_id'){
					$result = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$godina_id))->pluck('orgj_id');
				}elseif($column == 'poslovna_godina_id'){
					$result = $godina_id;
				}

			}elseif($magacin_id!=null){
				$default_magacin_id = $magacin_id;
				$result = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$godina_id,'orgj_id'=>$default_magacin_id))->pluck($column);
			}
			if(is_null($result)){
				if($column=='kolicina'){
					$result = 0;
				}
				if($column=='orgj_id'){
					$result = $default_magacin_id;
				}
				if($column=='poslovna_godina_id'){
					$result = $godina_id;
				}
			}
		}else{
			$result = DB::table('roba')->select($column)->where('roba.roba_id',$roba_id)->pluck($column);
		}
		return $result;
		// if($result){
		// 	return $result;
		// }
		// else{
		// 	return '';
		// }
	}

	public static function save($data){
		$data['naziv_web'] = trim($data['naziv_web']);
        $data['naziv'] = $data['naziv_web'];
        $data['naziv_displej'] = '';
		if($data['roba_id'] == 0){

            foreach($data as $key => $val){
                if(!$val && $val!='0'){
                    unset($data[$key]);
                }
            }
            $data['klasa_pr_id'] = -1;
            $data['flag_razlomljeno'] = 0;
            $data['cenovnik_jm_id'] = 9;
            $data['promena'] = 1;
            $data['tarifna_grupa_id'] = 0;
            $data['jedinica_mere_id'] = 1;

            $kolicina = null;
			if(isset($data['kolicina'])){
				$kolicina = $data['kolicina'];
        	}
			if(isset($data['orgj_id'])){
	        	$orgj_id = $data['orgj_id'];
        	}else{
        		$orgj_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
        	}
    		unset($data['kolicina']);
        	unset($data['orgj_id']);

        	$data['roba_id'] = DB::select("SELECT nextval('roba_roba_id_seq')")[0]->nextval;
        	$data['sifra_k'] = DB::table('roba')->max('sifra_k')+1;

			DB::table('roba')->insert($data);

			$new_roba_id = DB::select("SELECT currval('roba_roba_id_seq')")[0]->currval;

			if(!is_null($kolicina)){
				$data_lager = array(
					'roba_id' => $new_roba_id,
					'orgj_id' => $orgj_id,
					'kolicina' => $kolicina,
					'nabavna_po' => isset($data['racunska_cena_nc']) ? $data['racunska_cena_nc'] : 0,
					'valuta_id' => 1,
					'poslovna_godina_id' => DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id'),
					);
				DB::table('lager')->insert($data_lager);

			}

		    //drip
		    if(Config::get('app.livemode') && AdminOptions::gnrl_options(3060) == 1){
				$dbProduct = DB::table('roba')->where('roba_id',$data['roba_id'])->first();
				$drip = new Drip();
				$drip->addOrUpdateProduct($dbProduct,'created');  
		    }

			AdminSupport::saveLog('ARTIKLI_DODAJ', array(DB::table('roba')->max('roba_id')));
			return $new_roba_id;
		}else{
            foreach($data as $key => $val){
                if(!$val && $val!='0'){
                    $data[$key]=NULL;
                }
            }
            
            if(!isset($data['garancija'])){
            	$data['garancija']=0;
            }

        	$roba_id = $data['roba_id'];
        	unset($data['roba_id']);

        	if(isset($data['orgj_id'])){
        		$orgj_id = $data['orgj_id'];
        	}else{
        		$orgj_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
        	}
			$godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
        	if(DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$godina_id,'orgj_id'=>$orgj_id))->first()){
        		DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$godina_id,'orgj_id'=>$orgj_id))->update(array('kolicina'=>$data['kolicina']));
        	}
        	elseif(isset($data['kolicina'])){
				$data_lager = array(
					'roba_id' => $roba_id,
					'orgj_id' => $orgj_id,
					'kolicina' => $data['kolicina'],
					'nabavna_po' => isset($data['racunska_cena_nc']) ? $data['racunska_cena_nc'] : 0,
					'valuta_id' => 1,
					'poslovna_godina_id' => $godina_id,
					);
				DB::table('lager')->insert($data_lager);	
			}
			  	
        	unset($data['kolicina']);
        	unset($data['orgj_id']);
 
			DB::table('roba')->where('roba_id', $roba_id)->update($data);
		    //drip
		    if(Config::get('app.livemode') && AdminOptions::gnrl_options(3060) == 1){
				$dbProduct = DB::table('roba')->where('roba_id',$roba_id)->first();
				$drip = new Drip();
				$drip->addOrUpdateProduct($dbProduct,'updated');
		    }

			$data_dobavljac = array(
				'flag_prikazi_u_cenovniku' => $data['flag_prikazi_u_cenovniku'],
				'grupa_pr_id' => $data['grupa_pr_id']
				);
			if(isset($data['flag_zakljucan'])){
				$data_dobavljac['flag_zakljucan'] = $data['flag_zakljucan'] == 'true' ? 1 : 0;
			}
			if(isset($data['flag_aktivan'])){
				$data_dobavljac['flag_aktivan'] = $data['flag_aktivan'];
			}
			if(isset($data['proizvodjac_id'])){
				$data_dobavljac['proizvodjac_id'] = $data['proizvodjac_id'];
			}
			if(isset($data['tarifna_grupa_id'])){
				$data_dobavljac['tarifna_grupa_id'] = $data['tarifna_grupa_id'];
			}
			DB::table('dobavljac_cenovnik')->where('roba_id', $roba_id)->update($data_dobavljac);
			AdminSupport::saveLog('ARTIKLI_IZMENI', array($roba_id));

			return $roba_id;
		}
	}

	public static function delete($roba_id, $check=null)
	{
        $redirect = AdminOptions::base_url().'/admin';
        $continue = true;
        $message = '';
        $check != null && $check == 1 ? $check = true : $check = false;
        $zakljucan = DB::table('roba')->where('roba_id', $roba_id)->pluck('flag_zakljucan');
        if ($zakljucan == 1 && !$check) {
            $redirect = AdminOptions::base_url().'admin/product/'.$roba_id;
            $continue = false;
            $message = 'Roba ID: ' . $roba_id . ' - nije obrisan jer je zaključan. ';
        }
        
        if($continue){
            $stavke_b2c = DB::table('web_b2c_narudzbina_stavka')->where('roba_id',$roba_id)->count() + DB::table('web_b2c_korpa_stavka')->where('roba_id',$roba_id)->count();
            $stavke_b2b = DB::table('web_b2b_narudzbina_stavka')->where('roba_id',$roba_id)->count() + DB::table('web_b2b_korpa_stavka')->where('roba_id',$roba_id)->count();
            $stavke = $stavke_b2c + $stavke_b2b;
            
            if($stavke > 0 && !$check){
                $redirect = AdminOptions::base_url().'admin/product/'.$roba_id;
                $continue = false;
            	$message = 'Roba ID: ' . $roba_id . ' - nije obrisan zbog istorije. ';
            }

            if($continue){
                DB::table('vezani_artikli')->where('roba_id',$roba_id)->delete();
                DB::table('vezani_artikli')->where('vezani_roba_id',$roba_id)->delete();
                DB::table('roba_export')->where('roba_id',$roba_id)->delete();
                DB::table('web_b2c_korpa_stavka')->where('roba_id',$roba_id)->delete();
                DB::table('web_b2b_korpa_stavka')->where('roba_id',$roba_id)->delete();
                DB::table('web_b2c_narudzbina_stavka')->where('roba_id',$roba_id)->delete();
                DB::table('web_b2b_narudzbina_stavka')->where('roba_id',$roba_id)->delete();
                DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->delete();
                DB::table('osobina_roba')->where('roba_id',$roba_id)->delete();
                DB::table('srodni_artikli')->where('roba_id',$roba_id)->orWhere('srodni_roba_id',$roba_id)->delete();

                //drip
		        if(Config::get('app.livemode') && AdminOptions::gnrl_options(3060) == 1){
		            $dbProduct = DB::table('roba')->where('roba_id',$roba_id)->first();
		            $drip = new Drip();
		            $drip->addOrUpdateProduct($dbProduct,'deleted');
		        }

                $slike_query = DB::table('web_slika')->where('roba_id',$roba_id)->get();
                if(count($slike_query)>0){
                    foreach($slike_query as $row){
                        if(file_exists(trim($row->putanja))==true){
                            unlink(trim($row->putanja));
                        }
                    }
                }
                DB::table('web_slika')->where('roba_id',$roba_id)->delete();
                DB::table('lager')->where('roba_id',$roba_id)->delete();

                $redirect = AdminOptions::base_url().'admin/artikli/'.AdminArticles::find($roba_id, 'grupa_pr_id');
                DB::table('dobavljac_cenovnik')->where('roba_id',$roba_id)->update(array('povezan'=>0,'roba_id'=>-1));
                DB::table('roba')->where('roba_id',$roba_id)->delete();

            }
        }
        return (object) array(
        	'success' => $continue,
        	'redirect' => $redirect,
        	'message' => $message
        	);
	}

	public static function kolicina($provera_lagera,$roba_id,$sum,$magacin_id)
	{
		$kolicina = '';
		if(isset($provera_lagera)){		
			if($magacin_id ==0 ){
				$kolicina = round(AdminArticles::find($roba_id,'kolicina',0));
			}
			else{
				$kolicina = round(AdminArticles::find($roba_id,'kolicina',$magacin_id));
			}
		}
		return $kolicina;
	}

	public static function findGrupe($grupa_pr_id, $column)
	{
		$result = DB::table('grupa_pr')->select($column)->where('grupa_pr_id', $grupa_pr_id)->pluck($column);

		if($result){
			return $result;
		} else {
			return '';
		}

	}

	

	public static function getSlike($roba_id,$web_slika_id=0)
	{
		$slike_query = DB::table('web_slika')->select('roba_id','web_slika_id','akcija','flag_prikazi','putanja','alt','parent_id')->where('roba_id', $roba_id);
		if($web_slika_id>0){
			$slike_query->where('parent_id', $web_slika_id);
		}else{
			$slike_query->whereNull('parent_id');
		}
		return $slike_query->orderBy('akcija','DESC')->orderBy('web_slika_id','ASC')->get();

		// if($result){
		// 	return $result;
		// } else {
		// 	return '';
		// }

	}
	
	public static function getDodatneSlike($parent_id){
		return DB::table('web_slika')->where('parent_id', $parent_id)->get();
	}

	// public static function findSlika($web_slika_id, $column, $akcija=null)
	// {

	// 	return DB::table('web_slika')->select($column)->where('web_slika_id', $web_slika_id)->pluck($column);

	// }

	public static function saveImage($data)
	{
		$slika = $data['slika'];
        $originalName = $slika->getClientOriginalName();
        $extension = $slika->getClientOriginalExtension();

        $akcija = 1;
        $count = DB::table('web_slika')->where('roba_id',$data['roba_id'])->count();
        if($count > 0){
        	$akcija = 0;
        }

        $dobavljac_id = DB::table('dobavljac_cenovnik')->where('roba_id',$data['roba_id'])->pluck('partner_id');
        if ($dobavljac_id < 1) {
        	$dobavljac_id = '-1';
        }

        $sifra = DB::table('dobavljac_cenovnik')->where('roba_id',$data['roba_id'])->pluck('sifra_kod_dobavljaca');
        if ($sifra < 1) {
        	$sifra = '';
        }

		if($data['web_slika_id'] == '0'){
	        $next_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
	        //$name = $next_id.'.'.$extension;
	        $name_convert = $next_id.'.'.$extension;
	        $name = $next_id.'.webp';
			$insert_data = array(
				'web_slika_id'=>$next_id,
				'roba_id'=>$data['roba_id'],
				'akcija'=>$akcija, 
				'dobavljac_id'=>$dobavljac_id, 
				'sifra_kod_dobavljaca'=>$sifra,
				'flag_prikazi'=>1, 
				'putanja'=>'images/products/big/'.$name
				);
			if(isset($data['parent_id']) && $data['parent_id'] != '0'){
				$insert_data['parent_id'] = $data['parent_id'];
			}			
	        AdminArticles::saveImageFile($slika, $name_convert);
	        DB::table('web_slika')->insert($insert_data); 

		}else{

			AdminArticles::deleteImageFile($data['web_slika_id']);

	        $name = $data['web_slika_id'].'.'.$extension;
	        AdminArticles::saveImageFile($slika, $name);
	        
	        DB::table('web_slika')->where('web_slika_id', $data['web_slika_id'])->update(array('putanja'=>'images/products/big/'.$name));
		}

	    //drip
	    if(Config::get('app.livemode') && AdminOptions::gnrl_options(3060) == 1){
			$dbProduct = DB::table('roba')->where('roba_id',$data['roba_id'])->first();
			$drip = new Drip();
			$drip->addOrUpdateProduct($dbProduct,'updated');
	    }
		// AdminSupport::saveLog('ARTIKLI_DODAJ_SLIKE', array($data['roba_id']));
	}

	public static function saveImageFile($slikaObj, $name)
	{
        $sirina_big = DB::table('options')->where('options_id',1331)->pluck('int_data');
        $sirina_small = DB::table('options')->where('options_id',1333)->pluck('int_data');
	
			
       	try {   		
			$slikaObj->move('images/products/big/', $name);
			$name_old=$name;
			
			self::imagesToWebp('images/products/big/'.$name);			
 
			$width=Image::make('images/products/big/'.$name)->getWidth();
  			if($width>$sirina_big){
			Image::make('images/products/big/'.$name)->resize($sirina_big, null, function ($constraint) {
				$constraint->aspectRatio(); })->save();
			}
			$slika_old='images/products/big/'.$name_old;
			
			$extension=explode('.',$name_old);
			
			if($extension[1] !='webp'){
				File::delete($slika_old);
			}

       	} catch (Exception $e) {
       		
       	}

		// Image::make('images/products/big/'.$name)->resize($sirina_small, null, function ($constraint) {
		// 	$constraint->aspectRatio(); })->save('images/products/small/'.$name);
	}

	public static function deleteImageFile($web_slika_id)
	{
		$putanja = DB::table('web_slika')->where('web_slika_id',$web_slika_id)->pluck('putanja');

		if(in_array(explode('/',$putanja)[3],scandir('images/products/big'))){
			unlink($putanja);
		}
		// if(in_array(explode('/',$putanja)[3],scandir('images/products/small'))){
		// 	unlink('images/products/small/'.explode('/',$putanja)[3]);
		// }
	}

	public static function karakteristike_save($data){

		$data['web_karakteristike'] = '<p>'.str_replace("\n", "<br>",$data['web_karakteristike']).'</p>';
		DB::table('roba')->where('roba_id', $data['roba_id'])->update(array('web_karakteristike'=>$data['web_karakteristike']));
	}

	public static function vrsteCena($naziv){
		if(DB::table('vrsta_cena')->where(array('selected'=>1,'naziv'=>$naziv))->count() == 0){
			return false;
		}else{
			return true;
		}
	}

    public static function saveSelectImage($roba_id,$selectedImage,$alt=null)
    {
        try {
        	$check_images = DB::table('web_slika')->where('roba_id',$roba_id)->count();
            $slika_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
            $name = $slika_id.'.jpg';
            $sirina_big = DB::table('options')->where('options_id',1331)->pluck('int_data');

            $width=Image::make('images/upload_image/'.$selectedImage)->getWidth();

	        copy('images/upload_image/'.$selectedImage,'images/products/big/'.$name);

            $insert_data = array(
                'web_slika_id'=>intval($slika_id),
                'roba_id'=>intval($roba_id),
                'akcija'=> $check_images == 0 ? 1 : 0, 
                'dobavljac_id'=>-1, 
                'flag_prikazi'=>1,
                'alt' => $alt,
                'putanja'=>'images/products/big/'.$name
                );
			if($width>$sirina_big){
           		Image::make('images/products/big/'.$name)->resize(DB::table('options')->where('options_id',1331)->pluck('int_data'), null, function ($constraint){ $constraint->aspectRatio(); })->save();
           	}
        //    Image::make('images/products/big/'.$name)->resize(DB::table('options')->where('options_id',1333)->pluck('int_data'), null, function ($constraint){ $constraint->aspectRatio(); })->save('images/products/small/'.$name);

            DB::table('web_slika')->insert($insert_data);

        }
        catch (Exception $e) {}

    }
	public static function cloneImages($clone_id,$roba_id){
		$images = DB::table('web_slika')->where('roba_id',$clone_id)->orderBy('web_slika_id','ASC')->get();
		foreach($images as $image){

			$next_id = DB::select("SELECT nextval('web_slika_web_slika_id_seq')")[0]->nextval;
			$name = $next_id.'.'.explode('.',explode('/',$image->putanja)[3])[1];
			$new_putanja = 'images/products/big/'.$name;

			try {
				copy($image->putanja,$new_putanja);
			}
			catch (Exception $e){}

			$insert_data = array(
				'web_slika_id'=>$next_id,
				'roba_id'=>$roba_id,
				'akcija'=>$image->akcija, 
				'dobavljac_id'=>-1, 
				'flag_prikazi'=>1, 
				'putanja'=>$new_putanja
				);
			DB::table('web_slika')->insert($insert_data);
		}
	}
	public static function cloneKarak($clone_id,$roba_id){

		$clone_art = DB::table('roba')->select('web_karakteristike','web_flag_karakteristike','description','keywords','web_opis')->where('roba_id',$clone_id)->first();
		DB::table('roba')->where('roba_id',$roba_id)->update(array('web_karakteristike'=>$clone_art->web_karakteristike,'web_flag_karakteristike'=>$clone_art->web_flag_karakteristike,'web_opis'=>$clone_art->web_opis,'description'=>$clone_art->description,'keywords'=>$clone_art->keywords));

		DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike (partner_id,sifra_kod_dobavljaca,roba_id,karakteristika_naziv,karakteristika_vrednost,karakteristika_grupa,redni_br_grupe) 
			SELECT partner_id,sifra_kod_dobavljaca,".$roba_id.",karakteristika_naziv,karakteristika_vrednost,karakteristika_grupa,redni_br_grupe FROM dobavljac_cenovnik_karakteristike WHERE roba_id=".$clone_id." ORDER BY dobavljac_cenovnik_karakteristike_id ASC");

		DB::statement("INSERT INTO web_roba_karakteristike (roba_id,rbr,grupa_pr_naziv_id,vrednost,vrednost_css,grupa_pr_vrednost_id) 
			SELECT ".$roba_id.",rbr,grupa_pr_naziv_id,vrednost,vrednost_css,grupa_pr_vrednost_id FROM web_roba_karakteristike WHERE roba_id=".$clone_id."");
	}

	public static function saveMoreGroups($roba_id,$groups){
		$roba_grupe = DB::table('roba_grupe')->select('grupa_pr_id')->where('roba_id',$roba_id)->get();
		$old_groups = array_map('current',$roba_grupe);
		$for_delete = array_diff($old_groups,$groups);
		$for_insert = array_diff($groups,$old_groups);

		if(count($for_delete) > 0){
			DB::table('roba_grupe')->where('roba_id',$roba_id)->whereIn('grupa_pr_id',$for_delete)->delete();
		}
		if(count($for_insert) > 0){
			foreach($for_insert as $gr){
				DB::table('roba_grupe')->insert(array('roba_id'=>$roba_id,'grupa_pr_id'=>$gr));
			}
		}
	}

	public static function article_link($roba_id,$lang=null){
		$link = AdminOptions::base_url();
		if(AdminLanguage::multi() && AdminOptions::web_options(130) != 2){
			if(is_null($lang)){
				$link .= AdminLanguage::lang().'/';
			}else{
				$link .= $lang.'/';
			}
		}

		if(AdminOptions::web_options(130) == 2){
			Session::put('b2b_user_'.Options::server(),1);
			$link .= 'b2b/artikal';
		}else{
			$link .= AdminOptions::slug_trans('artikal',$lang);
		}
		return $link .= '/'.AdminOptions::slugify(AdminCommon::seo_title($roba_id));
	}
	public static function checkImage($join=null){
		if(Options::gnrl_options(1308)){
			return '';
		}else{
			$result = 'AND ws.roba_id IS NOT NULL ';
			if($join == 'join'){
				$result = ' LEFT JOIN web_slika ws ON r.roba_id = ws.roba_id';
			}
			return $result;
		}
	}
	public static function checkCharacteristics($join=null){
		if(Options::gnrl_options(1306)){
			return '';
		}else{
			$result = 'AND (r.web_karakteristike IS NOT NULL OR wrk.roba_id IS NOT NULL OR dck.roba_id IS NOT NULL) ';
			if($join == 'join'){
				$result = ' LEFT JOIN web_roba_karakteristike wrk ON r.roba_id = wrk.roba_id LEFT JOIN dobavljac_cenovnik_karakteristike dck ON r.roba_id = dck.roba_id';
			}
			return $result;
		}
	}
	public static function checkPrice(){
		if(Options::gnrl_options(1307)){
			return '';
		}else{
			return 'AND r.'.Options::checkCena().' > 0 ';
		}
	}
	public static function checkDescription(){
		if(Options::gnrl_options(1305)){
			return '';
		}else{
			return 'AND r.web_opis IS NOT NULL ';
		}
	}
	public static function provera_akcija($roba_id,$portal='b2c'){
		$datum = date('Y-m-d');
		$flag_prikazi = $portal=='b2c' ? "r.flag_prikazi_u_cenovniku" : "r.flag_cenovnik";
		$flag_akcija = $portal=='b2c' ? "r.akcija_flag_primeni" : "r.b2b_akcija_flag_primeni";
		$datum_od = $portal=='b2c' ? "r.datum_akcije_od" : "r.b2b_datum_akcije_od";
		$datum_do = $portal=='b2c' ? "r.datum_akcije_do" : "r.b2b_datum_akcije_do";

		$query_akcija=DB::select("SELECT DISTINCT r.roba_id FROM roba r".AdminArticles::checkImage('join').AdminArticles::checkCharacteristics('join')." WHERE r.roba_id = ".$roba_id." AND r.flag_aktivan = 1 AND ".$flag_prikazi." = 1 AND ".$flag_akcija." = 1 AND CASE WHEN ".$datum_od." IS NOT NULL AND ".$datum_do." IS NOT NULL THEN ".$datum_od." <= '".$datum."' AND ".$datum_do." >= '".$datum."' 
		WHEN ".$datum_od." IS NOT NULL AND ".$datum_do." IS NULL THEN ".$datum_od." <= '".$datum."' 
		WHEN datum_akcije_od IS NULL AND ".$datum_do." IS NOT NULL THEN ".$datum_do." >= '".$datum."' 
		ELSE 1 = 1 END ".AdminArticles::checkImage().AdminArticles::checkPrice().AdminArticles::checkDescription().AdminArticles::checkCharacteristics()."");

			if(count($query_akcija) > 0){
				return 1;
			}else{
				return 0;
			}
	}
	public static function jedinica_mere($roba_id){
		$jedinica_mere_id = DB::table('roba')->where('roba_id',$roba_id)->pluck('jedinica_mere_id');
		return DB::table('jedinica_mere')->where('jedinica_mere_id',$jedinica_mere_id)->first();
	}
	public static function imagesToWebp($file){

    $compression_quality = 40;
    if (!file_exists($file)) {
        return false;
    }
    $file_type = exif_imagetype($file);
    //https://www.php.net/manual/en/function.exif-imagetype.php
    //exif_imagetype($file);
    // 1    IMAGETYPE_GIF
    // 2    IMAGETYPE_JPEG
    // 3    IMAGETYPE_PNG
    // 6    IMAGETYPE_BMP
    // 15   IMAGETYPE_WBMP
    // 16   IMAGETYPE_XBM
    $output_file =  str_replace(array('.png','.jpg','.jepg'), array('','',''), $file) . '.webp';
    if (file_exists($output_file)) {
        return $output_file;
    }
    if (function_exists('imagewebp')) {
        switch ($file_type) {
            case '1': //IMAGETYPE_GIF
                $image = imagecreatefromgif($file);
                break;
            case '2': //IMAGETYPE_JPEG
                $image = imagecreatefromjpeg($file);
                break;
            case '3': //IMAGETYPE_PNG
                    $image = imagecreatefrompng($file);
                    imagepalettetotruecolor($image);
                    imagealphablending($image, true);
                    imagesavealpha($image, true);
                    break;
            case '6': // IMAGETYPE_BMP
                $image = imagecreatefrombmp($file);
                break;
            case '15': //IMAGETYPE_Webp
               return false;
                break;
            case '16': //IMAGETYPE_XBM
                $image = imagecreatefromxbm($file);
                break;
            default:
                return false;
        }
        // Save the image
        $result = imagewebp($image, $output_file, $compression_quality);
        imagedestroy($image);
        if (false === $result) {
            return false;
        }

        // Free up memory
        imagedestroy($image);
        return $output_file;
    } elseif (class_exists('Imagick')) {
        $image = new Imagick();
        $image->readImage($file);
        if ($file_type === "3") {
            $image->setImageFormat('webp');
            $image->setImageCompressionQuality($compression_quality);
            $image->setOption('webp:lossless', 'true');
        }
        $image->writeImage($output_file);
        
    }
    return false;
    }
}
