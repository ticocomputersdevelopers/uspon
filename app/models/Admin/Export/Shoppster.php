<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use DOMDocument;
use Response;
use View;
use B2bOptions;
use Options;
use PHPExcel; 
use PHPExcel_IOFactory;
use B2bArticle;

class Shoppster {

	public static function execute($export_id,$kind,$short=false){

		$export_products = DB::select("SELECT roba_id, sifra_is,web_cena, mpcena,racunska_cena_nc, naziv_web,grupa_pr_id, web_flag_karakteristike, duzina, sirina, visina, web_karakteristike, web_opis, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, model, (SELECT ROUND(sum(kolicina)) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0)) AS kolicina, akcija_flag_primeni, b2b_akcija_flag_primeni,akcijska_cena, barkod, napomena, garancija,datum_akcije_do,datum_akcije_od,tezinski_faktor, stari_konto, tarifna_grupa_id FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND proizvodjac_id <> -1 AND flag_aktivan = 1 AND web_cena > 0");

		if($kind=='xml'){
			return self::xml_exe($export_products);
		} else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function xml_exe($products){
		$serialized_array = self::serialized_array();

		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("products");
		$xml->appendChild($root);
		foreach($products as $article){
		$grupa=explode('-',self::grupa_link($article->grupa_pr_id));
		
			$product = $xml->createElement("product");

		    Support::xml_node($xml,"product_category",isset($grupa[0]) ? $grupa[0] :'',$product);
		    Support::xml_node($xml,"product_subcategory",isset($grupa[1]) ? $grupa[1] :'',$product);
		    Support::xml_node($xml,"product_group",isset($grupa[2]) ? $grupa[2] :'',$product);
		    Support::xml_node($xml,"brand",$article->proizvodjac,$product);
		    Support::xml_node($xml,"product_id",$article->roba_id,$product);
		    Support::xml_node($xml,"identifier",$article->naziv_web,$product);
			Support::xml_node($xml,"description",$article->web_opis."\n".Support::characteristics($article->roba_id,$article->web_flag_karakteristike,$article->web_karakteristike),$product);
		    Support::xml_node($xml,"color",'',$product);
			Support::xml_node($xml,"size",$article->stari_konto>0?$article->stari_konto:'',$product);
			Support::xml_node($xml,"origin",'',$product);
			Support::xml_node($xml,"free_delivery",'False',$product);
			Support::xml_node($xml,"ean_tyoe",'EAN13',$product);
			Support::xml_node($xml,"ean",$article->barkod,$product);


		    Support::xml_node($xml,"height",$article->visina,$product);
			Support::xml_node($xml,"width",$article->sirina,$product);
			Support::xml_node($xml,"lenght",$article->duzina,$product);
			Support::xml_node($xml,"weight",$article->tezinski_faktor>0?($article->tezinski_faktor/1000).'kg':'',$product);
			Support::xml_node($xml,"bulky",'',$product);

			// Support::xml_node($xml,"price",$article->racunska_cena_nc,$product);
			Support::xml_node($xml,"price",B2bArticle::b2bRabatCene($article->roba_id,1)->ukupna_cena*(1+self::procenat_grupe($serialized_array,$article->grupa_pr_id)),$product);
			Support::xml_node($xml,"msrp",$article->mpcena*1.05,$product);



			Support::xml_node($xml,"discounted_price",'',$product);
			Support::xml_node($xml,"discounted_from",'',$product);
			Support::xml_node($xml,"discounted_to",'',$product);
			Support::xml_node($xml,"discount_code",'',$product);


			Support::xml_node($xml,"vat",'rs-'.DB::table('tarifna_grupa')->where('tarifna_grupa_id',$article->tarifna_grupa_id)->pluck('porez'),$product);
			Support::xml_node($xml,"uom",'piece',$product);
		    Support::xml_node($xml,"stock",$article->kolicina,$product);
			Support::xml_node($xml,"group_id",$article->grupa_pr_id,$product);
			Support::xml_node($xml,"delivery_time",'3',$product);
			Support::xml_node($xml,"distributor",Options::company_name(),$product);
		    Support::xml_node($xml,"prim_image",Support::major_image($article->roba_id),$product);
		    $images = $xml->createElement("images");
				
			    foreach (Support::slike($article->roba_id) as $key => $image ) {
			    	
			    	Support::xml_node($xml,"image".($key+1),AdminOptions::base_url().$image,$images);
			    }
			$product->appendChild($images);	

			$root->appendChild($product);
		}

		$xml->formatOutput = true;
		$store_path = 'files/exporti/shoppster/shoppster.xml';
		$xml->save($store_path) or die("Error");

		return $store_path;
	}
	public static function grupa_link($grupa_pr_id,$link=''){
        $first = DB::table('grupa_pr')->where('grupa_pr_id','>',0)->where(array('grupa_pr_id'=>$grupa_pr_id))->first();
        if(is_null($first)){
        	return '/';
        }
        if($link!=''){
        	$link = $first->grupa.'-'.$link;
        }else{
        	$link = $first->grupa;
        }
        if($first->parrent_grupa_pr_id != 0){
        	$link = self::grupa_link($first->parrent_grupa_pr_id,$link);
        }
        return $link;
	}

	public static function all_subbgroups($grupa_pr_id) {
		return array_map('current', DB::select('select grupa_pr_id from grupa_pr gp where grupa_pr_id in (WITH RECURSIVE ActiveGroupHierarchy AS (
                      SELECT grupa_pr_id, parrent_grupa_pr_id
                      FROM grupa_pr
                      WHERE grupa_pr_id > 0 and grupa_pr_id = '.$grupa_pr_id.'
                      AND grupa_pr_id != parrent_grupa_pr_id
                      UNION
                      SELECT g.grupa_pr_id, g.parrent_grupa_pr_id
                      FROM grupa_pr g
                      JOIN ActiveGroupHierarchy ag ON g.parrent_grupa_pr_id = ag.grupa_pr_id
                      WHERE g.grupa_pr_id != g.parrent_grupa_pr_id
                    )
                    SELECT grupa_pr_id
                    FROM ActiveGroupHierarchy) '));
	}

	public static function serialized_array() {
		$products_file = 'files/shoppster/Shoppster.xlsx';
			
		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
        $excelObj = $excelReader->load($products_file);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();

        $niz = [];
       	for ($row = 2; $row <= $lastRow; $row++) {
            $grupa_pr_id = (int) $worksheet->getCell('A'.$row)->getValue();
            $procenat = $worksheet->getCell('C'.$row)->getValue();
            $niz[serialize(self::all_subbgroups($grupa_pr_id))] = $procenat;
            
        }
        return $niz;
	}

	public static function procenat_grupe($serialized_array,$grupa_pr_id) {
			foreach ($serialized_array as $key => $value) {
			    $unserializedKey = unserialize($key);
			    if (is_array($unserializedKey) && in_array($grupa_pr_id, $unserializedKey)) {
			        return $value;
			    }
			}
			return 0;
	}
}