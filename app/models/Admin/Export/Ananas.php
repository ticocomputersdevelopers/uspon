<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use DOMDocument;
use Response;
use View;
use Input;
use AdminArticles;
use All;
use Service\Mailer;

class Ananas {

	public static function execute($export_id,$kind,$short=false){
		$export_products = "SELECT roba_id, web_cena, mpcena,naziv_web, web_flag_karakteristike, web_karakteristike, web_opis, web_karakteristike, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, model,
			-- (SELECT ROUND(kolicina) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani=1)) AS kolicina
			akcija_flag_primeni, akcijska_cena, barkod, sku, napomena, garancija, tezinski_faktor, jedinica_mere_id, tarifna_grupa_id, datum_akcije_do, datum_akcije_od,grupa_pr_id FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND barkod <> '' AND barkod is not null AND flag_aktivan = 1 AND flag_prikazi_u_cenovniku = 1 AND web_cena > 0";
		
		if($kind=='json'){
			return self::json_exe($export_products,$export_id);
		}  else {
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function json_exe($products,$export_id) {

		try {
		// $barkodovi = array_column(json_decode(file_get_contents('files/exporti/ananas/unexist_products.json')),'ean');
		// $upit = DB::select(" select count(*) from roba where barkod is not null and barkod <> '' and barkod in ('".implode("','",$barkodovi)."') and roba_id not in (select roba_id from roba_export where export_id = 11) ");

		// var_dump($upit); die;



		$mp_magacini = DB::select("select orgj_id from orgj where naziv ilike '%Maloprodaja%'");

		// povlacenje ucitanih artikala

		$size = 2000;
		$page = 0;
		$loaded_products = [];

		while($size > 0 && $page < 1000) {

			$token = json_decode(self::getToken($export_id));
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://api.ananas.rs/product/api/v1/merchant-integration/products?page='.$page.'&size='.$size.'',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_HTTPHEADER => array(
			    'Authorization: '.$token->token_type.' '.$token->access_token,
			    'Content-Type: application/json'
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);

			// $size = sizeof(json_decode($response));
			$page = $page + 1;

			if($size > 0 && is_array(json_decode($response)) && !empty(json_decode($response))) {
				$loaded_products = array_merge($loaded_products,json_decode($response));
			} else {
				$size = 0;
			}



		}

		file_put_contents('files/exporti/ananas/loaded_products.json', json_encode($loaded_products));



		// insert new products
		
		$loaded_products = json_decode(file_get_contents('files/exporti/ananas/loaded_products.json'));
		$loaded_barcodes = array_column($loaded_products,'ean');
		$products_insert = DB::select($products." and barkod not in ('".implode("','",$loaded_barcodes)."')");


		$articles = array();
		foreach ($products_insert as $key => $article) {
			$slike = array();
			foreach(DB::table('web_slika')->where(array('roba_id'=>$article->roba_id, 'flag_prikazi'=>1))->get() as $slika){
				$slike[] = AdminOptions::base_url().$slika->putanja;
			}
			$articles[$key] = array(
										'name' => $article->naziv_web,
										'description' => !empty($article->web_karakteristike) ? preg_replace('/<[^>]*>/', ' ',$article->web_karakteristike) : preg_replace('/<[^>]*>/', ' ',$article->web_opis),
										'coverImage' => Support::major_image($article->roba_id),
										'ean' => $article->barkod,
										'brand' => $article->proizvodjac,
										'gallery' => $slike,
										// 'packageWeightValue' => number_format($article->tezinski_faktor/1000, 2, '.', ''),
										// "packageWeightUnit" => "kg",
										"basePrice" => $article->web_cena*1.05,
										// 'basePrice' => ($article->akcija_flag_primeni==1 && (is_null($article->datum_akcije_od) || $article->datum_akcije_od <= date('Y-m-d')) && (is_null($article->datum_akcije_do) || $article->datum_akcije_do >= date('Y-m-d'))) ? (float) $article->akcijska_cena: (float) $article->web_cena,
										'vat' => (float) number_format(DB::table('tarifna_grupa')->where('tarifna_grupa_id',$article->tarifna_grupa_id)->pluck('porez'), 2, '.', ''),
										'stockLevel' => (int) DB::table('lager')->where(array('roba_id'=>$article->roba_id))->sum('kolicina'),
										'sku' => $article->sku,
										'category' => $article->grupa
									);
			if($article->tezinski_faktor > 0) {
				$articles[$key]['packageWeightValue'] = number_format($article->tezinski_faktor/1000, 2, '.', '');
				$articles[$key]['packageWeightUnit'] = "kg";
			}

			if(empty(trim($articles[$key]['description']))) {
				$articles[$key]['description'] = '-';
			}

			// Za ove grupe se ne gledaju maloprodajni magacini, samo vp
			if(in_array($article->grupa_pr_id,array(2830,3258,2808,2728,2824,3320,2750,3260,2752,3056,2748,2806,3050,2810,3058))) {
				if($articles[$key]['stockLevel'] > 0) {
					foreach ($mp_magacini as $mp_magacin) {
						$articles[$key]['stockLevel'] = $articles[$key]['stockLevel'] - (int) AdminArticles::find($article->roba_id,'kolicina',$mp_magacin->orgj_id);
					}
				}	
			}
			if($articles[$key]['stockLevel'] < 0) {
				$articles[$key]['stockLevel'] = 0;
			}
		}
		
		$store_path = 'files/exporti/ananas/ananas.json';
		file_put_contents($store_path, json_encode($articles));

		$token = json_decode(self::getToken($export_id));
		$result = self::send_data($token,$articles); 
		if(isset(json_decode($result)->id)) {
			$filename = "files/exporti/ananas/status.txt";        
	        $myfile = fopen($filename, "a");
	        $line = date("d-m-Y H:i:s")." - OK - ".json_decode($result)->id." \r\n";
	        fwrite($myfile, $line);
	        fclose($myfile);
		} else {
			$filename = "files/exporti/ananas/status.txt";        
	        $myfile = fopen($filename, "a");
	        $line = date("d-m-Y H:i:s")." - FALSE \r\n";
	        fwrite($myfile, $line);
	        fclose($myfile);		
		}


		// edit inserted

		$mapped_loaded_products = array_column($loaded_products,'id','ean');
		$products_edit = DB::select($products." and barkod in ('".implode("','",$loaded_barcodes)."')");
		$articles = array();
		foreach($products_edit as $key => $article) {
				$articles[$key] = array(
										'id' => $mapped_loaded_products[$article->barkod],
										'ean' => $article->barkod,
										"basePrice" => $article->web_cena*1.05,
										'vat' => (float) number_format(DB::table('tarifna_grupa')->where('tarifna_grupa_id',$article->tarifna_grupa_id)->pluck('porez'), 2, '.', ''),
										'stockLevel' => (int) DB::table('lager')->where(array('roba_id'=>$article->roba_id))->sum('kolicina'),
										'sku' => $article->sku
									);

			// Za ove grupe se ne gledaju maloprodajni magacini, samo vp
			if(in_array($article->grupa_pr_id,array(2830,3258,2808,2728,2824,3320,2750,3260,2752,3056,2748,2806,3050,2810,3058))) {
				if($articles[$key]['stockLevel'] > 0) {
					foreach ($mp_magacini as $mp_magacin) {
						$articles[$key]['stockLevel'] = $articles[$key]['stockLevel'] - (int) AdminArticles::find($article->roba_id,'kolicina',$mp_magacin->orgj_id);
					}
				}	
			}
			if($articles[$key]['stockLevel'] < 0) {
				$articles[$key]['stockLevel'] = 0;
			}
		}

		file_put_contents('files/exporti/ananas/ananas_edit.json', json_encode($articles));

		$token = json_decode(self::getToken($export_id));
		$result = self::edit_data($token,$articles);
		file_put_contents('files/exporti/ananas/edit_products.json', $result);


		// edit stock unexist

		$ids = array_column($loaded_products,'id');
		$eans = array_column($loaded_products,'ean');
		$barcodes = array_merge(array_column($products_insert,'barkod'),array_column($products_edit,'barkod'));
		$articles = array();
		foreach($ids as $key => $id) {
			if(!in_array($eans[$key],$barcodes)) {
				$articles[] = array(
										'id' => $id,
										'ean' => $eans[$key],
										'stockLevel' => (int) 0
								   ); 
			}
		}

		file_put_contents('files/exporti/ananas/ananas_unexist.json', json_encode($articles));
		
		$token = json_decode(self::getToken($export_id));
		$result = self::edit_data($token,$articles);
		file_put_contents('files/exporti/ananas/unexist_products.json', $result);

			$filename = "files/exporti/ananas/log.txt";        
	        $myfile = fopen($filename, "a");
	        $line = date("d-m-Y H:i:s")." - OK \r\n";
	        fwrite($myfile, $line);
	        fclose($myfile);

	        die;
	    }catch (Exception $e){
			Mailer::send('automatika@tico.rs','automatika@tico.rs','Ananas export Uspon','Greška prilikom exporta!');
		}

	}



	public static function getClientId($export_id) {
		return DB::table('export')->where('export_id',$export_id)->pluck('clientid');
	}

	public static function getClientSecret($export_id) {
		return DB::table('export')->where('export_id',$export_id)->pluck('clientsecret');
	}

	public static function getToken($export_id) {
		$postData = array(
							"grantType" => "CLIENT_CREDENTIALS",
							"clientId" => self::getClientId($export_id),
							"clientSecret" => self::getClientSecret($export_id),
							"scope" => "public_api/full_access"
						);
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://api.ananas.rs/iam/api/v1/auth/token',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => json_encode($postData),
		  CURLOPT_HTTPHEADER => array(
		    'Content-Type: application/json'
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		return $response;
	}

	public static function send_data($token,$articles) {
		$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://api.ananas.rs/product/api/v1/merchant-integration/import',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'POST',
			  CURLOPT_POSTFIELDS => json_encode($articles),
			  CURLOPT_HTTPHEADER => array(
			    'Authorization: '.$token->token_type.' '.$token->access_token,
			    'Content-Type: application/json'
			  ),
			));

			$response = curl_exec($curl);
			curl_close($curl);
			return $response;

	}

	public static function edit_data($token,$articles) {

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://api.ananas.rs/product/api/v1/merchant-integration/product/bulk',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'PUT',
		  CURLOPT_POSTFIELDS => json_encode($articles),
		  CURLOPT_HTTPHEADER => array(
		    'Authorization: '.$token->token_type.' '.$token->access_token,
		    'Content-Type: application/json'
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		return $response;
	}

	public static function search_product($export_id,$ean) {

			$token = json_decode(self::getToken($export_id));
			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => 'https://api.ananas.rs/product/api/v1/merchant-integration/products?ean='.$ean.'',
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => '',
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => 'GET',
			  CURLOPT_HTTPHEADER => array(
			    'Authorization: '.$token->token_type.' '.$token->access_token,
			    'Content-Type: application/json'
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			return $response;
	}

}