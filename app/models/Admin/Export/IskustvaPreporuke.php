<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use DOMDocument;
use Response;
use View;
use Product;

class IskustvaPreporuke {

	public static function execute($export_id,$kind,$short=false){

		$export_products = DB::select("SELECT roba_id, web_cena, naziv_web, web_flag_karakteristike, web_karakteristike, web_opis, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, model, (SELECT ROUND(sum(kolicina)) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0)) AS kolicina, akcija_flag_primeni, akcijska_cena, barkod, napomena, garancija, tezinski_faktor FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND flag_aktivan = 1 AND flag_prikazi_u_cenovniku = 1 AND web_cena > 0");

		if($kind=='xml'){
			return self::xml_exe($export_products);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function xml_exe($products){
		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("articles");
		$xml->appendChild($root);

		foreach($products as $article){

			$product   = $xml->createElement("item");

		    Support::xml_node($xml,"EAN",$article->barkod,$product);
		    Support::xml_node($xml,"Price",Product::get_price($article->roba_id),$product);
		    Support::xml_node($xml,"ProductUrl",Support::product_link($article->roba_id),$product);
		    if($article->kolicina > 0) {
		    	Support::xml_node($xml,"Available",'true',$product);
		    } else {
		    	Support::xml_node($xml,"Available",'false',$product);
		    }

			$root->appendChild($product);
		}

		$xml->formatOutput = true;
		$store_path = 'files/exporti/iskustva_preporuke/iskustva_preporuke.xml';
		$xml->save($store_path) or die("Error");

		// header('Content-type: text/xml');
		// //header('Content-Disposition: attachment; filename="CeneRS.xml"');
		// echo file_get_contents($store_path); die;	
		return $store_path;
	}

}