<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use DOMDocument;
use View;
use Product;

class Tehnoteka {

	public static function execute($export_id,$kind,$short=false){

		$export_products = DB::select("SELECT roba_id, web_cena, naziv_web, (SELECT sum(kolicina) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0)) AS kolicina, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) as proizvodjac, akcija_flag_primeni, akcijska_cena, datum_akcije_od, datum_akcije_do, barkod FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND flag_aktivan = 1 AND flag_prikazi_u_cenovniku = 1 AND web_cena > 0");
		if($kind=='xml'){
			return self::xml_exe($export_products);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function xml_exe($products){
		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("proizvodi");
		$xml->appendChild($root);

		foreach($products as $article){

			$product   = $xml->createElement("proizvod");

		    Support::xml_node($xml,"Barkod",$article->barkod,$product);
		    Support::xml_node($xml,"SKU",$article->roba_id,$product);
		    Support::xml_node($xml,"Naziv_proizvoda",$article->naziv_web,$product);
		    Support::xml_node($xml,"Brend",$article->proizvodjac,$product);
		    Support::xml_node($xml,"Trenutna_ili_promo_cena",Product::get_price($article->roba_id),$product);
		    Support::xml_node($xml,"Stara_cena",($article->akcija_flag_primeni==1 && (is_null($article->datum_akcije_od) || $article->datum_akcije_od <= date('Y-m-d')) && (is_null($article->datum_akcije_do) || $article->datum_akcije_do >= date('Y-m-d')) ?$article->web_cena:''),$product);
		    Support::xml_node($xml,"Datum_od_kada_traje_promocija",($article->akcija_flag_primeni==1 && (is_null($article->datum_akcije_od) || $article->datum_akcije_od <= date('Y-m-d')) && (is_null($article->datum_akcije_do) || $article->datum_akcije_do >= date('Y-m-d')) ?$article->datum_akcije_od:''),$product);
		    Support::xml_node($xml,"Datum_do_kada_traje_promocija",($article->akcija_flag_primeni==1 && (is_null($article->datum_akcije_od) || $article->datum_akcije_od <= date('Y-m-d')) && (is_null($article->datum_akcije_do) || $article->datum_akcije_do >= date('Y-m-d')) ?$article->datum_akcije_do:''),$product);
		    if($article->kolicina > 0) {
		    	Support::xml_node($xml,"Stanje",'true',$product);
		    } else {
		    	Support::xml_node($xml,"Stanje",'false',$product);
		    }
		    Support::xml_node($xml,"URL_ka_stranici_proizvoda",Support::product_link($article->roba_id),$product);

			$root->appendChild($product);
		}

		$xml->formatOutput = true;
		$store_path = 'files/exporti/tehnoteka/tehnoteka.xml';
		$xml->save($store_path) or die("Error");

		// header('Content-type: text/xml');
		// //header('Content-Disposition: attachment; filename="CeneRS.xml"');
		// echo file_get_contents($store_path); die;	
		return $store_path;
	}

}