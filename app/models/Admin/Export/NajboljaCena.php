<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminCommon;
use DOMDocument;
use All;
use Response;
use View;
use Input;
use AdminArticles;

class NajboljaCena {
	public static function execute($export_id,$kind,$short=false){
		$export_products = DB::select("SELECT roba_id, web_cena, deklaracija, mpcena,naziv_web, racunska_cena_end, web_flag_karakteristike, web_karakteristike, web_opis, web_karakteristike, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, model,
			akcija_flag_primeni, akcijska_cena, barkod, sku, napomena, garancija, tezinski_faktor, jedinica_mere_id, tarifna_grupa_id, datum_akcije_do, datum_akcije_od,grupa_pr_id FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND barkod <> '' AND barkod is not null AND ((web_opis <> '' AND web_opis is not null) OR (web_karakteristike <> '' AND web_karakteristike is not null))  AND flag_aktivan = 1 AND flag_cenovnik = 1 AND web_cena > 0");
		
		if($kind=='json'){
			self::json_exe($export_id,$export_products);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function json_exe($export_id,$products) {
		$articles = array();
		foreach ($products as $key => $article) {
			$articles[$key] = array(
								'name'  => $article->naziv_web,
								'ean'   => $article->barkod,
								'model' => $article->model,
								'category' => $article->grupa,
								'brand' => $article->proizvodjac,
								"price" => $article->akcija_flag_primeni==1?$article->akcijska_cena:$article->web_cena,
								'link'  => Support::product_link($article->roba_id),
								'image' => Support::major_image($article->roba_id),
								'description' => pg_escape_string($article->web_opis),
								'specification' => pg_escape_string($article->web_karakteristike),
								'image' => Support::major_image($article->roba_id),
								'stock' => AdminArticles::find($article->roba_id,'kolicina',0) > 0 ? 'Da':'Ne'
								);
		}
		$store_path = 'files/exporti/najboljacena/najboljacena.json';
		file_put_contents($store_path, json_encode($articles));
		return $store_path;
	}
}