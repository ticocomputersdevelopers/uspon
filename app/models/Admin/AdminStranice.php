<?php

class AdminStranice {

	public static function sectionTypes(){

		if(!AdminOptions::is_shop()){
			$query = DB::table('sekcija_stranice_tip')->where('flag_aktivan',1)->where('naziv','!=','Lista artikala');
		}else{
			$query = DB::table('sekcija_stranice_tip')->where('flag_aktivan',1);
		}

		return $query->orderBy('rbr','asc')->get();
	}

	public static function getSectionType($type_id){
		return DB::table('sekcija_stranice_tip')->where('sekcija_stranice_tip_id',$type_id)->first();
	}

	public static function sliders(){
		return DB::table('slajder')->where(['flag_aktivan'=>1])->whereIn('slajder_tip_id',array_map('current',DB::table('slajder_tip')->whereNotIn('naziv',['Popup Baner'])->get()))->orderBy('slajder_id','asc')->get();
	}

	public static function articleTypes(){
		return DB::table('tip_artikla')->where('active',1)->orderBy('rbr','asc')->get();
	}

	public static function sectionLang($page_section_id,$lang_id){
		return DB::table('sekcija_stranice_jezik')->where(['sekcija_stranice_id'=>$page_section_id,'jezik_id'=>$lang_id])->first();
	}

	public static function pageSections($page_id){
		return DB::table('stranica_sekcija')->select('sekcija_stranice.*','stranica_sekcija.rbr')->where('web_b2c_seo_id',$page_id)->join('sekcija_stranice','sekcija_stranice.sekcija_stranice_id','=','stranica_sekcija.sekcija_stranice_id')->orderBy('rbr','asc')->get();
	}
}