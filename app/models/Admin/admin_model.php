<?php

class Admin_model {

    // public static function pages($parent_id=0){
    //   if($parent_id==0){
    //     return DB::table('web_b2c_seo')->where('parrent_id',0)->orWhereNull('parrent_id')->orderBy('rb_strane','asc')->get();
    //   }else{
    //     return DB::table('web_b2c_seo')->where('parrent_id',$parent_id)->orderBy('rb_strane','asc')->get();
    //   }
    // }

    // public static function parent_pages($parent_id=0,$page_id){
    //     return DB::table('web_b2c_seo')->where('parrent_id',$parent_id)->where('web_b2c_seo_id','!=',$page_id)->orderBy('rb_strane','asc')->get();
    // }

    // public static function page_level($page_id,$level=1){
    //   $page = DB::table('web_b2c_seo')->where('web_b2c_seo_id',$page_id)->first();
    //   if($page->parrent_id == 0){
    //     return $level;
    //   }else{
    //     return self::page_level($page->parrent_id,($level+1));
    //   }
    // }

    // public static function page_childs_depth($pages_ids,$count=0){
    //   $pages = DB::table('web_b2c_seo')->select('web_b2c_seo_id')->whereIn('parrent_id',$pages_ids)->get();
    //   if(count($pages) == 0){
    //     return $count;
    //   }else{
    //       return self::page_childs_depth(array_map('current',$pages),($count+1));
    //   }
    // }

    public static function check_admin($sifra=null){
      // return true;
        $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
        if($imenik_id == 0){
            return true;
        }
        
        if($sifra == null){
            return false;
        }
        if(!is_array($sifra)){
          $sifra = array($sifra);
        }
        $ac_group_id = intval(DB::table('imenik')->where('imenik_id',$imenik_id)->pluck('kvota'));
        $ac_module_ids = array_map('current',DB::table('ac_module')->select('ac_module_id')->whereIn('sifra', $sifra)->get());
        $count = DB::table('ac_group_module')->where(array('ac_group_id'=>intval($ac_group_id),'alow'=>1))->whereIn('ac_module_id', $ac_module_ids)->count();

        if($count > 0){
            return true;
        }else{
            return false;
        }
    }
    public static function get_admin(){
        $user = DB::table('imenik')->where('imenik_id',Session::get('b2c_admin'.AdminOptions::server()))->first();
        return $user->ime.' '.$user->prezime;
    }
    public static function get_komercijalista($imenik_id){
        $user = DB::table('imenik')->where('imenik_id',$imenik_id)->first();
        return $user->ime.' '.$user->prezime;
    }
 
    public static function narudzbina_kupac($web_b2c_narudzbina_id){
        
        // Dodao sam $row->naziv ukoliko je firma da ispise ime firme
        $kupac_id=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id');
        foreach(DB::table('web_kupac')->where('web_kupac_id',$kupac_id)->get() as $row){
            return $row->ime." ".$row->prezime." ".$row->naziv;
        }
        
        
    }
    public static function city_pickup_id($web_b2c_narudzbina_id){
        
       $id= DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('pickup_id');
       if($id == 1){
        return "<p class='pulsate' style='color: red;font-size:12px'>Priprema za slanje</p>";
       }elseif($id > 1){
        return "<p style='color: green;font-size:12px'>Poslato <i class='fa fa-check' style='font-size:12px;color:green'></i></p>";
       }else{
        return "";
       }        
    }
    public static function city_pickup_exist(){
        
       $id= DB::table('posta_zahtev_api')->where('pickup_id','=',1)->pluck('pickup_id');
       if($id == 1){
        return 1;
       }       
    }

    // public static function css_prev(){
    //     $css="<style>";
    //     foreach(DB::table('css_class')->get() as $row){
    //       $css.=$row->naziv."{";
    //       foreach(DB::table('css_class_atribute')->where('css_class_id',$row->css_class_id)->get() as $row2){
    //         $css.=$row2->naziv_promenjive.":".$row2->vrednost_active.";";
    //       }
    //       $css.="}";
    //     }
    //     $css.="</style>";
        
    //     return $css;
    // }
    
    // public static function css_value($css_class_atribute_id){
    //     return DB::table('css_class_atribute')->where('css_class_atribute_id',$css_class_atribute_id)->pluck('vrednost_active');
    // }

    // public static function css_logo($css_class_atribute_id){
    //        $link=0;
        
    //        $niz=array();
    //         $vrednost = DB::table('css_class_atribute')->where('css_class_atribute_id',$css_class_atribute_id)->pluck('vrednost_active');

    //        $string = strrpos($vrednost, 'px');
          
    //         $link=substr($vrednost,0,$string);
    //         return  $link;

    // }
    
    public static function logo(){
        $logo=DB::table('preduzece')->where('preduzece_id',1)->pluck('logo');
        if($logo==null or $logo==''){
            
            return AdminOptions::base_url()."images/logo.png";
            
        }
        else {
            return AdminOptions::base_url()."".$logo;
        }
    }

    // public static function potpis(){
    //     $potpis=DB::table('preduzece')->where('preduzece_id',1)->pluck('potpis');
    //     if($potpis !=null or $potpis !=''){
            
    //        return AdminOptions::base_url()."".$potpis;
            
    //     }
     
    // }
    
    public static function upload_directory(){
        $dir    = "./images/upload";

        //return $dir;
 
        $files = scandir($dir);
            $slike="<ul>";
        $br=0;
       foreach ($files as $row) {
       if($br>1 && $row != 'b2b'){
       
           $slike.="<li><img src='".AdminOptions::base_url()."images/upload/".$row."'  class='images-upload-list' /><a href='javascript:void(0)' title='Obriši' data-url='/images/upload/".$row."' class='images-delete'><i class='fa fa-times' aria-hidden='true'></i></a></li>";
           }
           
        
            $br+=1;
        }
        $slike.="</ul>";
        return $slike;
     
    }

    // public static function shopmania_files(){
        
    //      $dir    = "./cenovnici/shopmania";
    //     //return $dir;
    //     $files = scandir($dir);
    //         $cenovnici="<ul>";
    //     $br=0;
    //    foreach ($files as $row) {
    //    if($br>1){
       
    //        $cenovnici.="<li><a class='uploaded-link' href='".AdminOptions::base_url()."cenovnici/shopmania/".$row."'>".AdminOptions::base_url()."cenovnici/shopmania/".$row."</a>";
    //        }
           
        
    //         $br+=1;
    //     }
    //     $cenovnici.="</ul>";
    //     return $cenovnici;
        
    // }
    
    // public static function eponuda_files(){
        
    //      $dir    = "./cenovnici/eponuda";
    //     //return $dir;
    //     $files = scandir($dir);
    //         $cenovnici="<ul>";
    //     $br=0;
    //    foreach ($files as $row) {
    //    if($br>1){
       
    //        $cenovnici.="<li><a class='uploaded-link' href='".AdminOptions::base_url()."cenovnici/eponuda/".$row."'>".AdminOptions::base_url()."cenovnici/eponuda/".$row."</a>";
    //        }
           
        
    //         $br+=1;
    //     }
    //     $cenovnici.="</ul>";
    //     return $cenovnici;
        
    // }

    // public static function pages_menu_show($stranica){
    //     $flag_page=DB::table('web_b2c_seo')->where('naziv_stranice',$stranica)->pluck('flag_page');
        
    //     switch($flag_page){
    //           case"0":
    //                 echo '
    //                     <label class="checkbox-label medium-6 columns">         
    //           <input type="checkbox" id="header_menu" name="header_menu" >
    //                         <span class="label-text">Header meniju</span>
    //         </label>
    //         <label class="checkbox-label medium-6 columns"> 
    //           <input type="checkbox" id="footer_menu" name="footer_menu">
    //                         <span class="label-text">Footer meniju</span>
    //         </label>';
    //         break;
    //         case"1":
    //                 echo '
    //                     <label class="checkbox-label medium-6 columns">         
    //           <input type="checkbox" id="header_menu" name="header_menu" checked="" >
    //                         <span class="label-text">Header meniju</span>
    //         </label>
    //         <label class="checkbox-label medium-6 columns"> 
    //           <input type="checkbox" id="footer_menu" name="footer_menu">
    //                         <span class="label-text">Footer meniju</span>
    //         </label>';
    //         break;
            
    //         case"2":
    //                 echo '
    //                     <label class="checkbox-label medium-6 columns">         
    //           <input type="checkbox" id="header_menu" name="header_menu"  >
    //                         <span class="label-text">Header meniju</span>
    //         </label>
    //         <label class="checkbox-label medium-6 columns"> 
    //           <input type="checkbox" id="footer_menu" name="footer_menu" checked="">
    //                         <span class="label-text">Footer meniju</span>
    //         </label>';
    //         break;
    //          case"3":
    //                 echo '
    //                     <label class="checkbox-label medium-6 columns">         
    //           <input type="checkbox" id="header_menu" name="header_menu" checked="" >
    //                         <span class="label-text">Header meniju</span>
    //         </label>
    //         <label class="checkbox-label medium-6 columns"> 
    //           <input type="checkbox" id="footer_menu" name="footer_menu" checked="">
    //                         <span class="label-text">Footer meniju</span>
    //         </label>';
    //         break;
    //         default:
    //       echo '
    //                     <label class="checkbox-label medium-6 columns">         
    //           <input type="checkbox" id="header_menu" name="header_menu" >
    //                         <span class="label-text">Header meniju</span>
    //         </label>
    //         <label class="checkbox-label medium-6 columns"> 
    //           <input type="checkbox" id="footer_menu" name="footer_menu">
    //                         <span class="label-text">Footer meniju</span>
    //         </label>';
    //   break;
    //     }
        
    // }

    // public static function pages_menu_b2b_show($flag_b2b_show){

    //     switch($flag_b2b_show){
    //         case"0":
    //             echo '  
    //                 <label class="checkbox-label medium-6 columns">
    //         <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" > 
    //                     <span class="label-text">Header meniju</span>
    //       </label>

    //       <label class="checkbox-label medium-6 columns">
    //         <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu">
    //                     <span class="label-text">Footer meniju</span>
    //       </label>';
    //             break;
    //         case"1":
    //             echo '  
    //                 <label class="checkbox-label medium-6 columns">
    //         <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" checked="" >
    //                     <span class="label-text">Header meniju</span>
    //       </label>
    //       <label class="checkbox-label medium-6 columns">
    //         <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu">
    //                     <span class="label-text">Footer meniju</span>
    //       </label>';
    //             break;

    //         case"2":
    //             echo '
    //                 <label class="checkbox-label medium-6 columns">
    //         <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu"  >
    //                     <span class="label-text">Header meniju</span>
    //       </label>
    //       <label class="checkbox-label medium-6 columns">
    //         <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu" checked="">
    //                     <span class="label-text">Footer meniju</span>
    //       </label>';
    //             break;
    //         case"3":
    //             echo '
    //                 <label class="checkbox-label medium-6 columns">
    //         <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" checked="" >
    //                     <span class="label-text">Header meniju</span>
    //       </label>
    //       <label class="checkbox-label medium-6 columns">
    //         <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu" checked="">
    //                     <span class="label-text">Footer meniju</span>
    //       </label>';
    //             break;
    //         default:
    //             echo '
    //                 <label class="checkbox-label medium-6 columns">
    //         <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" >
    //                     <span class="label-text">Header meniju</span>
    //       </label>
    //       <label class="checkbox-label medium-6 columns">
    //         <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu">
    //                     <span class="label-text">Footer meniju</span>
    //       </label>';
    //             break;
    //     }

    // }
  
}