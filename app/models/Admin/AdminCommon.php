<?php

class AdminCommon {

    // public static function get_page_start(){
        
    //     return DB::table('web_b2c_seo')->where('web_b2c_seo_id',1)->pluck('naziv_stranice');
    // }

    public static function allGroups(&$niz,$grupa_pr_id)
    {       
        $niz[]=$grupa_pr_id;
        $check_parent=DB::table('grupa_pr')->where('parrent_grupa_pr_id',$grupa_pr_id)->get();
        if (count($check_parent)>0)
        {
            foreach ($check_parent as $row)
            {
                AdminCommon::allGroups($niz,$row->grupa_pr_id);
            }               

        }
    }

    public static function lat_long(){
        $mapa=DB::table('preduzece')->where('preduzece_id',1)->pluck('mapa');
        $lat_long = array();
        $mapa = explode(';',$mapa);
        $lat_long[0] = isset($mapa[0]) ? $mapa[0] : '';
        $lat_long[1] = isset($mapa[1]) ? $mapa[1] : '';
        return $lat_long;
    }

    public static function mostPopularArticles(){
        // $popular = DB::table('roba')->where('flag_aktivan', 1)->where('flag_prikazi_u_cenovniku', 1)->orderBy('pregledan_puta', 'DSC')->limit(5)->get();
        $popular=DB::select("SELECT DISTINCT r.roba_id, r.pregledan_puta FROM roba r".AdminOptions::checkImage('join').AdminOptions::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND r.roba_id <> -1".AdminOptions::checkImage().AdminOptions::checkPrice().AdminOptions::checkDescription().AdminOptions::checkCharacteristics()." ORDER BY r.pregledan_puta DESC LIMIT 4");

        return $popular;
    }


    public static function cena($cena,$valuta_show = false){
        $decimale = 2;

        $valuta = Cache::remember('admin_valuta',20,function(){ return DB::table('valuta')->where('izabran',1)->first(); });

        $kurs=AdminOptions::kurs($valuta->valuta_id);
        $valutaLabel = ' <span> '.strtolower($valuta->valuta_sl).'.</span>';
        if($cena!="0"){            
            $iznos = number_format(round(floatval($cena/$kurs),$decimale), 2, ',', '.');
        }else{
            $iznos = '0.00';
        }

        if($valuta_show == true){
            return $iznos.$valutaLabel;
        }else{
            return $iznos;
        }
    }

    public static function n_i($web_b2c_narudzbina_id){
         $nacin_i=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_nacin_isporuke_id');
         
         return DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$nacin_i)->pluck('naziv');
    }
    public static function n_i_b2b($web_b2b_narudzbina_id){
         $nacin_i=DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->pluck('web_nacin_isporuke_id');
         
         return DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$nacin_i)->pluck('naziv');
    }
    public static function n_p($web_b2c_narudzbina_id){
         $nacin_i=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_nacin_placanja_id');
         
         return DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$nacin_i)->pluck('naziv');
    }
    public static function n_p_b2b($web_b2b_narudzbina_id){
         $nacin_i=DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->pluck('web_nacin_placanja_id');
         
         return DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$nacin_i)->pluck('naziv');
    }
    public static function narudzbina_ukupno($web_b2c_narudzbina_id){
        $ukupno=0;
        foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
            $ukupno+=$row->kolicina*$row->jm_cena;
        }
        return $ukupno;
    }
    public static function narudzbina_ukupno_b2b($web_b2b_narudzbina_id){
        $ukupno=0;
        foreach(DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row){
            $ukupno+=$row->kolicina*$row->jm_cena;
        }
        return $ukupno;
    }
    
    public static function troskovi_isporuke($web_b2c_narudzbina_id){
            $tezinaSum=0;
            $cenaSum=0;
            $troskoviPoKategoriji = 0;
            foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
                $tezinaSum+=DB::table('roba')->where('roba_id',$row->roba_id)->pluck('tezinski_faktor')*$row->kolicina;
                $cenaSum+=$row->jm_cena*$row->kolicina;

                if(DB::table('roba')->where('roba_id',$row->roba_id)->pluck('grupa_pr_id') == 3528 && $row->jm_cena > 10000) {
                    $troskoviPoKategoriji += $row->kolicina*1000;
                }
            }

            $troskoviSum = 0;
            $troskoviTezinaObj = (object) array('cena'=>0,'tezina_gr'=>0,'cena_do'=>0);
            $troskoviCenaObj = (object) array('cena'=>0,'tezina_gr'=>0,'cena_do'=>0);

            foreach(DB::table('web_troskovi_isporuke')->orderBy('tezina_gr','desc')->get() as $row) {
                if($tezinaSum > 0 && $tezinaSum >= $row->tezina_gr){
                    $troskoviTezinaObj = $row;
                    break;
                }
            }

            foreach(DB::table('web_troskovi_isporuke')->orderBy('cena_do','desc')->get() as $row) {
                if($cenaSum < $row->cena_do){
                    $troskoviCenaObj = $row;
                }
            }

            //samo tezina
            if(AdminOptions::web_options(133)==1 && AdminOptions::web_options(150)==0){
                return $troskoviSum;
            //samo cena
            }else if(AdminOptions::web_options(150)==1 && AdminOptions::web_options(133)==0){
                $troskoviSum = $troskoviCenaObj->cena + $troskoviPoKategoriji;
            //kombinacija cene i tezine
            }else if(AdminOptions::web_options(150)==1 && AdminOptions::web_options(133)==1){
                if($troskoviTezinaObj->cena == $troskoviCenaObj->cena){
                    $troskoviSum = $troskoviTezinaObj->cena;
                }else{
                    $troskoviSum = max(array($troskoviTezinaObj->cena,$troskoviCenaObj->cena));
                    // if($troskoviTezinaObj->tezina_gr < $troskoviCenaObj->tezina_gr || $troskoviCenaObj->cena_do < $troskoviTezinaObj->cena_do){
                    //     $troskoviSum = min(array($troskoviTezinaObj->cena,$troskoviCenaObj->cena));
                    // }else{
                    //     $troskoviSum = max(array($troskoviTezinaObj->cena,$troskoviCenaObj->cena));
                    // }
                }
            }else if(AdminOptions::web_options(150)==0 && AdminOptions::web_options(133)==0){
                return 0;
            }

            return $troskoviSum;
    }
    public static function cena_dostave_custom($web_b2c_narudzbina_id){
        if(AdminOptions::web_options(133)==1 OR AdminOptions::web_options(150)==1){
            return !is_null($trosak_isporuke = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('trosak_isporuke')) ? $trosak_isporuke : 0;
        }
        return 0;
    }
    
    public static function cena_dostave($web_b2c_narudzbina_id){
        if(AdminNarudzbine::find($web_b2c_narudzbina_id,'web_nacin_isporuke_id') != 2) {
            return ($cena_dostave_custom = self::cena_dostave_custom($web_b2c_narudzbina_id) > 0) ? $cena_dostave_custom : self::troskovi_isporuke($web_b2c_narudzbina_id);
        } else {
            return 0;
        } 
    }

    // public static function cena_do(){

    //     foreach(DB::table('cena_isporuke')->get() as $row){
    //         $cena_do = $row->cena_do;
    //     }
    //     return $cena_do;

    // }

    public static function racun_ukupno_bez_pdv($web_b2c_narudzbina_id){
        $ukupno = 0;
        foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
            $ukupno += $row->kolicina * ($row->jm_cena / 1.2);
        }
        return $ukupno;
    }
    
    // public static function narudbina_stavka_pdf($web_b2c_narudzbina_id){
    // // <th colspan="5">Informacije o narucenim proizvodima:</th>
    // echo
    // '<table>
    //     <tr>
    //         <td class="cell-product-name">Naziv proizvoda:</td>
    //         <td class="cell">Cena :</td>
    //         <td class="cell">Količina</td>
    //         <td class="cell">Ukupna cena:</td>
    //     </tr>';
    //     foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
    //         $ukupno=$row->kolicina*$row->jm_cena;
    //         echo 
    //         '<tr>
    //             <td class="cell-product-name">'.AdminCommon::short_title($row->roba_id).'</td>
    //             <td class="cell">'.self::cena($row->jm_cena).'</td>
    //             <td class="cell">'.(int)$row->kolicina.'</td>
    //             <td class="cell">'.self::cena($ukupno).'</td>
    //         </tr>';
    //     }
    // echo
    //     '</table>
    //     <table>
    //         <tr class="text-right">
    //             <td class="summary text-right">Ukupno:'.self::cena(self::narudzbina_ukupno($web_b2c_narudzbina_id)).'</td>
    //         </tr>
    //     </table>';
    // }

    // public static function narudzbina_kupac($web_b2c_narudzbina_id){
    //     $web_kupac=DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('web_kupac_id');

    //     foreach(DB::table('web_kupac')->where('web_kupac_id',$web_kupac)->get() as $row){

    //         if($row->flag_vrsta_kupca == 0){
    //             $name = '<td>Ime i Prezime:</td>
    //                     <td>'.$row->ime.' '.$row->prezime.'</td>';
    //         }else{
    //             $name = '<td>Firma i PIB:</td>
    //                     <td>'.$row->naziv.' '.$row->pib.'</td>';
    //         }

    //         echo '  <tbody>
    //                                 <th colspan="2">Informacije o kupcu:</th>
    //                                 <tr>
    //                                     '.$name.'
    //                                 </tr>
    //                                 <tr>
    //                                     <td>Adresa:</td>
    //                                     <td>'.$row->adresa.'</td>
    //                                 </tr>
    //                                 <tr>
    //                                     <td>Mesto:</td>
    //                                     <td>'.AdminSupport::find_mesto($row->mesto_id,'mesto').'</td>
    //                                 </tr>
    //                                 <tr>
    //                                     <td>Telefon:</td>
    //                                     <td>'.$row->telefon.'</td>
    //                                 </tr>
    //                                      <tr>
    //                                     <td>Email:</td>
    //                                     <td>'.$row->email.'</td>
    //                                 </tr>
    //                             </tbody>';
    //     }
        
    // }

    // public static function narudbina_stavka($web_b2c_narudzbina_id){
    //     echo '<table>
    //                             <tbody>
    //                                 <th colspan="5">Informacije o narucenim proizvodima:</th>
    //                                 <tr>
    //                                     <td class="cell-product-name">Naziv proizvoda:</td>
    //                                     <td class="cell">Cena :</td>
    //                                     <td class="cell">Količina</td>
    //                                     <td class="cell">Ukupna cena:</td>
    //                                 </tr>';
    //     foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
    //         $ukupno=$row->kolicina*$row->jm_cena;
    //         echo  '<tr>
    //                                     <td class="cell-product-name">'.AdminCommon::short_title($row->roba_id).'</td>
    //                                     <td class="cell">'.self::cena($row->jm_cena).'</td>
    //                                     <td class="cell">'.(int)$row->kolicina.'</td>
    //                                     <td class="cell">'.self::cena($ukupno).'</td>
    //                                 </tr>';
    //     }
    //     echo '</tbody>
    //                         </table>
    //                         <table>
    //                             <tbody>
    //                                 <tr>
    //                                     <td class="summary">Ukupno:'.self::cena(self::narudzbina_ukupno($web_b2c_narudzbina_id)).'</td>
    //                                 </tr>
    //                             </tbody>
    //                         </table>';
    // }

    public static function get_price($roba_id){

        $row = DB::table('roba')->where('roba_id',$roba_id)->first();
        $cena = AdminOptions::web_options(132)==1?$row->web_cena:$row->mpcena;
        if($row->akcija_flag_primeni == 1){         
            $cena = $row->akcijska_cena;
            if(!isset($cena) || $cena == 0){
                $cena = AdminOptions::web_options(132)==1?$row->web_cena:$row->mpcena;
            }
        }
        return round($cena);            
    }

    public static function short_title($roba_id){
        $query_title=DB::table('roba')->where('roba_id',$roba_id)->get();
        foreach ($query_title as $row){
            if(strlen($row->naziv_web)>70){
            return substr($row->naziv_web,0,67)."...";
            }
            else {
                return $row->naziv_web;
            }
        }
    
    }
    public static function seo_title($roba_id){
        $roba=DB::table('roba')->where('roba_id',$roba_id)->first();
        if($roba){
            return $roba->naziv_web;
        }
        return null;
    
    }

    public static function analitika_title($roba_id){
        $query_title=DB::table('roba')->where('roba_id',$roba_id)->get();

        foreach ($query_title as $row){
            if(strlen($row->naziv_web)>70){
            return str_replace('"','inch',substr($row->naziv_web,0,67))."...";
            }
            else {
                return str_replace('"','inch',$row->naziv_web);
            }
        }
    
    } 

}
