<?php

class PartnerCard extends Eloquent
{
    protected $table = 'web_b2b_kartica';
    
    protected $primaryKey = 'web_b2b_kartica_id';
    
    public $timestamps = false;

	protected $hidden = array(
        'web_b2b_kartica_id',
        'web_b2b_narudzbina_id',
        'vrsta_dokumenta',
        'datum_dokumenta',
        'datum_dospeca',
        'opis',
        'duguje',
        'potrazuje',
        'saldo',
        'foreigned',
        'id_is'
      	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['web_b2b_kartica_id']) ? $this->attributes['web_b2b_kartica_id'] : null;
	}
    public function getPartnerIdAttribute()
    {
        return isset($this->attributes['partner_id']) ? $this->attributes['partner_id'] : null;
    }
    public function getDocumentKindAttribute()
    {
        return isset($this->attributes['vrsta_dokumenta']) ? $this->attributes['vrsta_dokumenta'] : null;
    }
    public function getDocumentDateAttribute()
    {
        return isset($this->attributes['datum_dokumenta']) ? $this->attributes['datum_dokumenta'] : null;
    }
    public function getIncomingDateAttribute()
    {
        return isset($this->attributes['datum_dospeca']) ? $this->attributes['datum_dospeca'] : null;
    }
    public function getDescriptionAttribute()
    {
        return isset($this->attributes['opis']) ? $this->attributes['opis'] : null;
    }
    public function getDebitAttribute()
    {
        return isset($this->attributes['duguje']) ? floatval($this->attributes['duguje']) : 0.00;
    }
    public function getCreditAttribute()
    {
        return isset($this->attributes['potrazuje']) ? floatval($this->attributes['potrazuje']) : 0.00;
    }
    public function getExternalCodeAttribute()
    {
        return isset($this->attributes['id_is']) && !empty($this->attributes['id_is']) ? $this->attributes['id_is'] : null;
    }

	protected $appends = array(
        'id',
        'partner_id',
        'document_kind',
        'document_date',
        'incoming_date',
        'description',
        'debit',
        'credit',
    	'external_code'
    	);

    public function partner(){
        return $this->belongsTo('PartnerApi','partner_id');
    }
}