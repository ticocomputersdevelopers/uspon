<?php 
namespace Service;

use Language;
use AdminOptions;


class TranslatorService {
	const CIRILIC = 'cir';
	private $requestUrl = "https://translate.googleapis.com/translate_a/single?client=gtx";
	private $enableGoogle;
	private $translator;
	private $fromLang;
	private $toLang;

	public function __construct($fromLang,$toLang){
		$this->fromLang = $fromLang;
		$this->toLang = $toLang;
		$this->enableGoogle = $fromLang != self::CIRILIC && $toLang != self::CIRILIC && AdminOptions::gnrl_options(3038) == 1;

		if($this->enableGoogle){
			$this->requestUrl .= "&sl=".$this->fromLang."&tl=".$this->toLang."&dt=t";
		}
	}

	public function translate($string){
		if(!is_null($string) && $string != ''){
			if($this->enableGoogle){
				$this->requestUrl .= "&q=".urlencode($string);

				try {
					$content = @file_get_contents($this->requestUrl);
					$result = json_decode($content);
					if(isset($result[0]) && isset($result[0][0]) && isset($result[0][0][0])){
						$string = html_entity_decode($result[0][0][0]);

						if($this->toLang == 'sr'){
							$string = Language::cir_to_latin($string);
						}
					}

				} catch (Exception $e) {
					
				}

			}else{
				if($this->fromLang == 'sr' && $this->toLang == self::CIRILIC){

					$string = Language::latin_to_cir(strip_tags($string));
				}elseif($this->fromLang == self::CIRILIC && $this->toLang == 'sr'){
					$string = Language::cir_to_latin(strip_tags($string));
				}
			}
		}
		return $string;

	}

}