<?php
namespace Service;

use DB;
use AdminPartneri;
use AdminNarudzbine;
use AdminOptions;
use All;

class CityExpress {

	public static function createShipment($web_b2c_narudzbina_id,$posta_zahtev_api_id){
       $stavke=DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_zahtev_api_id',$posta_zahtev_api_id)->first();
       $broj_dokumenta = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('broj_dokumenta');
       $api = DB::table('posta_slanje')->where('posta_slanje_id',$stavke->posta_slanje_id)->where('api_aktivna',1)->pluck('auth_code');


       $data=  array(
       			"ShipmentWeight"=>$stavke->masa,
				"Collies"=> array(
				    array(		      
				      "Ref1"=> $broj_dokumenta.'-'.$posta_zahtev_api_id,
				      "Ref2"=> "",
				      "Ref3"=> "",
				      "RemarkGoods"=> $stavke->opis,
				      "ColliDocumentData"=> null,
				      "ColliDocumentExtension"=> null
				  )),
				"ConsigneeName"=> AdminPartneri::kupac_stavka($stavke->primalac_id)->ime.' '.AdminPartneri::kupac_stavka($stavke->primalac_id)->prezime,
				"ConsigneePostalCode"=> AdminPartneri::cityNaziv($stavke->primalac_ptt)->ptt,
				"ConsigneeStreet"=> AdminPartneri::kupac_stavka($stavke->primalac_id)->adresa,
				"ConsigneeCity"=> AdminPartneri::cityNaziv($stavke->primalac_ptt)->naziv,
				"ConsigneeCountryPrefix"=> "RS",
				"ConsigneeTelephoneNumber"=> AdminPartneri::kupac_stavka($stavke->primalac_id)->telefon,
				"ConsigneeEmailAddress"=> AdminPartneri::kupac_stavka($stavke->primalac_id)->email,
				"SenderName"=> AdminPartneri::partner($stavke->posiljalac_id)->naziv,
				"SenderPostalCode"=> AdminPartneri::cityNaziv($stavke->posiljalac_ptt)->ptt,
				"SenderStreet"=> AdminPartneri::partner($stavke->posiljalac_id)->adresa,
				"SenderCity"=> AdminPartneri::cityNaziv($stavke->posiljalac_ptt)->naziv,
				"SenderCountryPrefix"=> "RS",
				"SenderTelephoneNumber"=> AdminPartneri::partner($stavke->posiljalac_id)->telefon,
				"ExpressType"=> 100,
				"ExWorksType"=> 4,
				"NotificationType"=> $stavke->tip_obavestenja_id,
				"CodAmount"=> $stavke->cena_otkupa,
				"AllowSaturdayDelivery"=> $stavke->dostava_subotom,
				"RemarkDelivery"=> $stavke->napomena,
				"RemarkPickup"=> "",
				"IsCargo"=> false,
				"ReturnDocument"=> false,
				"ApiKey"=> $api
        );

    // echo json_encode($data);die;
	    $curl = curl_init();
	    curl_setopt_array($curl, array(
	      CURLOPT_URL => "http://webapi.cityexpress.rs/api/data/CreateShipment",
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_CUSTOMREQUEST => "POST",
	      CURLOPT_POSTFIELDS => json_encode($data),
	      CURLOPT_HTTPHEADER => array(
	        "Content-Type: application/json"
	      ),	      
	    ));

	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return (object) array(
	      	'success'=>false,
	      	'results'=>null,
	      	'message'=>$error
	      );
	    } 
	    else {
	      $result = json_decode($response);
	     //All::dd($result);
	      $success = $result->IsValid == 1 ? true : false;
	      return (object) array(
	      	'success'=>$success,
	      	'results'=>$result,
	      	'message'=>!empty($result->ValidationErrors) && is_array($result->ValidationErrors) ? implode("\n", $result->ValidationErrors) : ''
	      );
	    }		
	}
	public static function createShipmentPlain($web_b2c_narudzbina_id,$posta_zahtev_api_id){
       $stavke=DB::table('posta_zahtev_api')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->where('posta_zahtev_api_id',$posta_zahtev_api_id)->first();
       $broj_dokumenta = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->pluck('broj_dokumenta');
       $broj_stavki = DB::table('web_b2c_narudzbina_stavka_posta_zahtev')->where('posta_zahtev_api_id',$posta_zahtev_api_id)->sum('kolicina');
       $broj_stavki = str_replace(".0000", "", $broj_stavki);
       $api = DB::table('posta_slanje')->where('posta_slanje_id',$stavke->posta_slanje_id)->where('api_aktivna',1)->pluck('auth_code');

       $data=  array(
			  "NumberOfCollies"=> $broj_stavki,
			  "Weight"=> $stavke->masa,
			  "OverseasPrintsShippingLabels"=> true,
			  "Ref1"=> $broj_dokumenta.'--'.$posta_zahtev_api_id,
			  "Ref2"=> "",
			  "Ref3"=> "",
			  "RemarkGoods"=> $stavke->opis,
			  "ConsigneeName"=> isset(AdminPartneri::kupac_stavka($stavke->primalac_id)->ime) ? AdminPartneri::kupac_stavka($stavke->primalac_id)->ime.' '.AdminPartneri::kupac_stavka($stavke->primalac_id)->prezime : AdminPartneri::kupac_stavka($stavke->primalac_id)->naziv,
			  "ConsigneePostalCode"=> AdminPartneri::cityNaziv($stavke->primalac_ptt)->ptt,
			  "ConsigneeStreet"=> AdminPartneri::kupac_stavka($stavke->primalac_id)->adresa,
			  "ConsigneeCity"=> AdminPartneri::cityNaziv($stavke->primalac_ptt)->naziv,
			  "ConsigneeCountryPrefix"=> "RS",
			  "ConsigneeTelephoneNumber"=> AdminPartneri::kupac_stavka($stavke->primalac_id)->telefon,
			  "ConsigneeEmailAddress"=> AdminPartneri::kupac_stavka($stavke->primalac_id)->email,
			  "SenderName"=> AdminPartneri::partner($stavke->posiljalac_id)->naziv,
			  "SenderPostalCode"=> AdminPartneri::cityNaziv($stavke->posiljalac_ptt)->ptt,
			  "SenderStreet"=> AdminPartneri::partner($stavke->posiljalac_id)->adresa,
			  "SenderCity"=> AdminPartneri::cityNaziv($stavke->posiljalac_ptt)->naziv,
			  "SenderCountryPrefix"=> "RS",
			  "SenderTelephoneNumber"=>AdminPartneri::partner($stavke->posiljalac_id)->telefon,
			  "ExpressType"=> $stavke->vreme_dostave,
			  "ExWorksType"=> 4,
			  "NotificationType"=> $stavke->tip_obavestenja_id,
			  "AllowSaturdayDelivery"=> $stavke->dostava_subotom,
			  "RemarkDelivery"=> $stavke->napomena,
			  "RemarkPickup"=> "",
			  "IsCargo"=> false,
			  "ReturnDocument"=> false,
			  "ApiKey"=>$api
			);

     //echo json_encode($data);die;
	    $curl = curl_init();
	    curl_setopt_array($curl, array(
	      CURLOPT_URL => "http://webapi.cityexpress.rs/api/data/CreateShipmentPlain",
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_CUSTOMREQUEST => "POST",
	      CURLOPT_POSTFIELDS => json_encode($data),
	      CURLOPT_HTTPHEADER => array(
	        "Content-Type: application/json"
	      ),	      
	    ));

	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      return (object) array(
	      	'success'=>false,
	      	'results'=>null,
	      	'message'=>$error
	      );
	    } 
	    else {
	      $result = json_decode($response);
	      //All::dd($result);
	      $success = $result->IsValid == 1 ? true : false;
	      return (object) array(
	      	'success'=>$success,
	      	'results'=>$result,
	      	'message'=>!empty($result->ValidationErrors) && is_array($result->ValidationErrors) ? implode("\n", $result->ValidationErrors) : ''
	      );
	    }		
	}
	public static function shippingLabel($shipment_id){
		$posta=DB::table('posta_zahtev_api')->where('posta_odgovor_id',$shipment_id)->pluck('posta_slanje_id');
        $api = DB::table('posta_slanje')->where('posta_slanje_id',$posta)->where('api_aktivna',1)->pluck('auth_code');

		$data=  array(
			"ShipmentId"=> $shipment_id,
		 	"ApiKey"=> $api
        );
         $curl = curl_init();
	    curl_setopt_array($curl, array(
	      CURLOPT_URL => "http://webapi.cityexpress.rs/api/data/GetShippingLabelsForSingleShipment",
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_CUSTOMREQUEST => "POST",
	      CURLOPT_POSTFIELDS => json_encode($data),
	      CURLOPT_HTTPHEADER => array(
	        "Content-Type: application/json"
	      ),	      
	    ));

	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	   	if ($err) {
	      $error = "cURL Error #:" . $err;
	      return (object) array(
	      	'success'=>false,
	      	'results'=>null,
	      	'message'=>$error
	      );
	    } 
	    else {
	      $result = json_decode($response);
	      //All::dd($result);
	      $success = $result->IsValid == 1 ? true : false;
	      return (object) array(
	      	'success'=>$success,
	      	'results'=>$result,
	      	'message'=>!empty($result->ValidationErrors) && is_array($result->ValidationErrors) ? implode("\n", $result->ValidationErrors) : ''
	      );
	    }		
	}
	public static function requestPickup($shipment_id){
		$posta=DB::table('posta_zahtev_api')->where('posta_odgovor_id',$shipment_id)->pluck('posta_slanje_id');
        $api = DB::table('posta_slanje')->where('posta_slanje_id',$posta)->where('api_aktivna',1)->pluck('auth_code');
		
		$data=  array(
			"ShipmentId"=> $shipment_id,
		 	"ApiKey"=> $api
        );
         $curl = curl_init();
	      curl_setopt_array($curl, array(
	      CURLOPT_URL => "http://webapi.cityexpress.rs/api/data/RequestPickup",
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_CUSTOMREQUEST => "POST",
	      CURLOPT_POSTFIELDS => json_encode($data),
	      CURLOPT_HTTPHEADER => array(
	        "Content-Type: application/json"
	      ),	      
	    ));

	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	   	if ($err) {
	      $error = "cURL Error #:" . $err;
	      return (object) array(
	      	'success'=>false,
	      	'results'=>null,
	      	'message'=>$error
	      );
	    } 
	    else {
	      $result = json_decode($response);
	       //All::dd($result);
	      $success = $result->IsValid == 1 ? true : false;
	      return (object) array(
	      	'success'=>$success,
	      	'results'=>$result,
	      	'message'=>!empty($result->ValidationErrors) && is_array($result->ValidationErrors) ? implode("\n", $result->ValidationErrors) : ''
	      );
	    }	
	}
	public static function requestPickupForShipments($shipment_ids){
        $api = DB::table('posta_slanje')->where('api_aktivna',1)->pluck('auth_code');
		
		$data=  array(
			"ShipmentIds"=>$shipment_ids,
		 	"ApiKey"=> $api
        );
       
         $curl = curl_init();
          curl_setopt_array($curl, array(
          CURLOPT_URL => "http://webapi.cityexpress.rs/api/data/RequestPickupForShipments",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),          
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          $error = "cURL Error #:" . $err;
          return (object) array(
            'success'=>false,
            'results'=>null,
            'message'=>$error
          );
        } 
        else {
          $result = json_decode($response);
          //All::dd($result);
          $success = $result->IsValid == 1 ? true : false;
          return (object) array(
            'success'=>$success,
            'results'=>$result,
            'message'=>!empty($result->ValidationErrors) && is_array($result->ValidationErrors) ? implode("\n", $result->ValidationErrors) : ''
          );
        }	
	}
	public static function requestLabelsForShipments($shipment_ids){
        $api = DB::table('posta_slanje')->where('api_aktivna',1)->pluck('auth_code');
		
		$data=  array(
			"ShipmentIds"=>$shipment_ids,
		 	"ApiKey"=> $api
        );
       
         $curl = curl_init();
          curl_setopt_array($curl, array(
          CURLOPT_URL => "http://webapi.cityexpress.rs/api/data/GetShippingLabelsForShipments",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => json_encode($data),
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),          
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          $error = "cURL Error #:" . $err;
          return (object) array(
            'success'=>false,
            'results'=>null,
            'message'=>$error
          );
        } 
        else {
          $result = json_decode($response);
           //All::dd($result);
          $success = $result->IsValid == 1 ? true : false;
          return (object) array(
            'success'=>$success,
            'results'=>$result,
            'message'=>!empty($result->ValidationErrors) && is_array($result->ValidationErrors) ? implode("\n", $result->ValidationErrors) : ''
          );
        }	
	}
	public static function CancelShipment($shipmentId){

		$posta=DB::table('posta_zahtev_api')->where('posta_odgovor_id',$shipmentId)->pluck('posta_slanje_id');
        $api = DB::table('posta_slanje')->where('posta_slanje_id',$posta)->where('api_aktivna',1)->pluck('auth_code');
		
		$data=  array(
			"ShipmentId"=> $shipmentId,
		 	"ApiKey"=> $api
        );
         $curl = curl_init();
	      curl_setopt_array($curl, array(
	      CURLOPT_URL => "http://webapi.cityexpress.rs/api/data/CancelShipment",
	      CURLOPT_RETURNTRANSFER => true,
	      CURLOPT_CUSTOMREQUEST => "POST",
	      CURLOPT_POSTFIELDS => json_encode($data),
	      CURLOPT_HTTPHEADER => array(
	        "Content-Type: application/json"
	      ),	      
	    ));

	    $response = curl_exec($curl);
	    $err = curl_error($curl);
	    curl_close($curl);

	    if ($err) {
	      $error = "cURL Error #:" . $err;
	      echo $error;
	    } else {
	      $result = json_decode($response);
	      
	    }
	    DB::table('posta_zahtev_api')->where('posta_odgovor_id',$shipmentId)->update(array('posta_odgovor_id'=>''));		
	}
}