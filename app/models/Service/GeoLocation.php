<?php
namespace Service;
use All;

class GeoLocation
{
	
	public static function ipVisitorCurrencyCode()
	{
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];
	    $currency  = null;

	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, "http://www.geoplugin.net/json.gp?ip=".$ip);
	    curl_setopt($ch, CURLOPT_HEADER, 0);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    try {    	
		    $ip_data_in = curl_exec($ch);
		    curl_close($ch);
	    } catch (Exception $e) {
	    	return $currency;
	    }

	    $ip_data = json_decode($ip_data_in,true);
	    $ip_data = str_replace('&quot;', '"', $ip_data);

	    if($ip_data && isset($ip_data['geoplugin_currencyCode']) && !is_null($ip_data['geoplugin_currencyCode'])) {
	        $currency = $ip_data['geoplugin_currencyCode'];
	    }

	    return $currency;
	}
}