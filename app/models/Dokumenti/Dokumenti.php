<?php

class Dokumenti
{
	public static function getPartner($partner_id, $column=null){
		if(is_null($column)){
			return DB::table('partner')->where('partner_id',$partner_id)->first();
		}
		return DB::table('partner')->where('partner_id',$partner_id)->pluck($column);
	}

	public static function partnerSelect($partner_id=null){
		if(!is_null($partner=DokumentiOptions::user('saradnik'))){
			$partneri = DB::table('partner')->select('partner_id','naziv')->where('parent_id',$partner->id)->where('partner_id','>',0)->orderBy('naziv','asc')->get();
		}else{
			$partneri = DB::table('partner')->select('partner_id','naziv')->whereNull('parent_id')->where('partner_id','>',0)->orderBy('naziv','asc')->get();
		}
		$render = '';
        foreach($partneri as $row){
            if($row->partner_id == $partner_id){
                $render .= '<option value="'.$row->partner_id.'" selected>'.$row->naziv.'</option>';
            }else{
                $render .= '<option value="'.$row->partner_id.'">'.$row->naziv.'</option>';
            }
        }
        return $render;
	}

	public static function partnerSimpleDetails($partner_id){
		$render = '';
		if($partner_id > 0){
			$partner = DB::table('partner')->where('partner_id',$partner_id)->first();
			$render .= '<table>
							<tr>
								<td>Ulica i broj</td><td>' .$partner->adresa.'</td>
							</tr>
							<tr>
								<td>Mesto</td><td>' .$partner->mesto.'</td>
							</tr>
							<tr>
								<td>PIB</td><td>' .$partner->pib.'</td>
							</tr>
							<tr>
								<td>Marični broj</td><td>' .$partner->broj_maticni.'</td>
							</tr>
						</table>';
		}
        return $render;
	}

	public static function partnerContactDetails($partner_id){
		$render = '';
		if($partner_id > 0){
			$partner = DB::table('partner')->where('partner_id',$partner_id)->first();
			$render .= '<table>
							<tr>
								<td>Kontakt osoba</td><td>' .$partner->kontakt_osoba.'</td>
							</tr>
							<tr>
								<td>E-mail</td><td>' .$partner->mail.'</td>
							</tr>
							<tr>
								<td>Telefon</td><td>' .$partner->telefon.'</td>
							</tr>
							<tr>
								<td></td><td></td>
							</tr>
						</table>';
		}
        return $render;
	}

	public static function dateInversion($date,$delimeter='-',$originalDelimeter='-'){
		$dateArray = explode($originalDelimeter,$date);
		return $dateArray[2].$delimeter.$dateArray[1].$delimeter.$dateArray[0];
	}

	public static function partnerOtherDetails($partner_id){
		$render = '';
		if($partner_id > 0){
			$cart = DB::select("select sum(duguje) as duguje, sum(potrazuje) as potrazuje, sum(saldo) as saldo from web_b2b_kartica where partner_id=".$partner_id."");
			if(count($cart) > 0){
				if(is_null($cart[0]->duguje) || is_null($cart[0]->potrazuje) || is_null($cart[0]->saldo)){
					$cart = array((object) array('duguje'=>0,'potrazuje'=>0,'saldo'=>0));
				}
			}else{
				$cart = array((object) array('duguje'=>0,'potrazuje'=>0,'saldo'=>0));
			}

			$render .= '<table>
							<tr>
								<td>Klijent duguje</td><td>' .number_format($cart[0]->duguje,2,",",".").' din.</td>
							</tr>
							<tr>
								<td>Klijent potražuje</td><td>' .number_format($cart[0]->potrazuje,2,",",".").' din.</td>
							</tr>
							<tr>
								<td>Saldo</td><td>' .number_format($cart[0]->saldo,2,",",".").' din.</td>
							</tr>
							<tr>
								<td></td><td></td>
							</tr>
						</table>';
		}
        return $render;
	}

	public static function artikliSelect($roba_id,$roba_ids=array()){
		unset($roba_ids[array_search($roba_id,$roba_ids)]);
		$data = DB::table('roba')->select('roba_id','naziv_web','racunska_cena_end as cena')->where('flag_aktivan',1)->where('flag_cenovnik',1)->whereNotIn('roba_id',$roba_ids)
		// ->whereIn('roba_id',array_map('current',DB::select("select roba_id from lager where orgj_id=4 and poslovna_godina_id=2014 and (kolicina".(B2bOptions::web_options(131)==1 ? " - rezervisano" : "").") > 0")))
		->orderBy('naziv_web','asc')->get();

		$render = '';
		if($roba_id == 0){
			$render .= '<option value="0" data-nab_cena="0" ></option>';
		}
        foreach($data as $row){
            $render .= '<option value="'.$row->roba_id.'" data-nab_cena="'.$row->cena.'" '.($row->roba_id == $roba_id ? 'selected' : '').'>'.$row->naziv_web.'</option>';
        }
        return $render;
	}
	public static function statusSelect($dokumenti_status_id='0'){
		$statusi = DB::table('dokumenti_status')->select('dokumenti_status_id','naziv')->where('aktivan',1)->orderBy('naziv','asc')->get();
		$render = '<option value="0" '.($dokumenti_status_id == '0' ? 'selected' : '').'>Bez statusa</option>';
        foreach($statusi as $row){
            if($row->dokumenti_status_id == $dokumenti_status_id){
                $render .= '<option value="'.$row->dokumenti_status_id.'" selected>'.$row->naziv.'</option>';
            }else{
                $render .= '<option value="'.$row->dokumenti_status_id.'">'.$row->naziv.'</option>';
            }
        }
        return $render;
	}
	public static function dokumentStavke($roba_ids){
		$stavke = array();
		foreach($roba_ids as $key => $roba_id){
			$stavke[$key] = (object) array('roba_id'=>$roba_id,'kolicina'=>0,'nab_cena'=>0,'pdv'=>0);
		}
		return $stavke;
	}

	public static function podesavanja(){
		$user = DokumentiOptions::user('saradnik');
		$podesavanja = null;
		if(!is_null($user)){
			$podesavanja = DB::table('dokument_podesavanja')->where('partner_id',$user->id)->first();
		}else{
			$podesavanja = DB::table('dokument_podesavanja')->where('admin',1)->first();
			$podesavanja = (object) array('logo'=>DokumentiOptions::company_logo(),'sablon'=> !is_null($podesavanja) ? $podesavanja->sablon : null,'footer'=> !is_null($podesavanja) ? $podesavanja->footer : null);
		}
		if(is_null($podesavanja)){
			$podesavanja = (object) array('logo'=>null,'sablon'=>null,'footer'=>null);
		}
		return $podesavanja;
	}
	public static function getTarifnaGrupaId($pdv){
		$tg = DB::table('tarifna_grupa')->where(array('porez'=>$pdv))->first();

		if(is_null($tg)){
			$next_id = DB::table('tarifna_grupa')->max('tarifna_grupa_id')+1;
			DB::table('tarifna_grupa')->insert(array(
				'tarifna_grupa_id' => $next_id,
				'sifra' => $next_id,
				'naziv' => substr($pdv,0,99),
				'porez' => $pdv,
				'active' => 1,
				'default_tarifna_grupa' => 0,
				'tip' => substr($pdv,0,19)
				));
			$tg = DB::table('tarifna_grupa')->where('porez',$pdv)->first();
		}
		return $tg->tarifna_grupa_id;		
	}

	public static function makeOrder($ponuda_id){
		$user = DokumentiOptions::user();
		$ponuda = DB::table('ponuda')->where('ponuda_id',$ponuda_id)->first();
		$web_nacin_placanja_id = 1;
		$web_nacin_isporuke_id = 1;
        date_default_timezone_set('Europe/Belgrade');

        $br_nar = !is_null($br_doc = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id','!=','-1')->orderBy('web_b2b_narudzbina_id','desc')->pluck('broj_dokumenta')) ? intval(str_replace( 'WNB','',$br_doc)) + 1 : 1;
        $num_nar = str_pad($br_nar, 4, '0', STR_PAD_LEFT);

        $data=array(
            'partner_id'=>$ponuda->partner_id,
            'orgj_id'=>-1,
            'poslovna_godina_id'=>B2bOptions::poslovna_godina(),
            'vrsta_dokumenta_id'=>500,
            'broj_dokumenta'=>"WNB".$num_nar,
            'datum_dokumenta'=>date("Y-m-d"),
            'valuta_id'=>1,
            'kurs'=>B2bOptions::kurs(),
            'web_nacin_placanja_id'=>$web_nacin_placanja_id,
            'web_nacin_isporuke_id'=>$web_nacin_isporuke_id,
            'iznos'=>0,
            'prihvaceno'=>0,
            'stornirano'=>0,
            'realizovano'=>0,
            'napomena'=>"Narudžbinu kreirao ".(isset($user->naziv) ? $user->naziv : $user->ime." ".$user->prezime)." (".$user->id.")",
            'ip_adresa'=>B2bCommon::ip_adress(),
            'posta_slanje_id'=>-1,
            'posta_slanje_poslato'=>0,
            'promena'=>1,
            'partner_korisnik_id'=> null,
            'sifra_connect'=>0,
            'avans'=> 0.00
        );

        $cart = DB::table('ponuda_stavka')->where('ponuda_id',$ponuda->ponuda_id)->get();
        
        $success = true;
        //LOGIK
        // if(B2bOptions::info_sys('logik')){
        //     $nacin_isporuke = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$web_nacin_isporuke_id)->pluck('naziv');
        //     $nacin_placanja = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$web_nacin_placanja_id)->pluck('naziv');
            
        //     $response = Logik::createOrder($cart,$nacin_isporuke,$nacin_placanja,'');
        //     if($response->success){
        //         $data['sifra_is'] = $response->order_id;
        //     }else{
        //         $success = false;
        //     }            
        // }
        
        if($success){
            DB::table('web_b2b_narudzbina')->insert($data);

            $order_id = DB::table('web_b2b_narudzbina')->where('partner_id',$ponuda->partner_id)->where('broj_dokumenta',"WNB".$num_nar)->pluck('web_b2b_narudzbina_id');

            $cartItems = array();
            foreach($cart as $row){
                $roba = DB::table('roba')->where('roba_id',$row->roba_id)->first();
                if(isset($roba->roba_id) && $roba->roba_id != null){
                    $cartItems[]=[
                      'web_b2b_narudzbina_id'=>$order_id,
                      'broj_stavke'=>$row->broj_stavke,
                      'roba_id'=>$row->roba_id,
                      'kolicina'=>$row->kolicina_narudzbine,
                      'jm_cena'=>$row->pcena,
                      'tarifna_grupa_id'=>self::getTarifnaGrupaId($row->pdv),
                      'racunska_cena_nc'=>$row->nab_cena,
                    ];
                }
            }
            if(count($cartItems) > 0){
                // B2bBasket::rezervacija($cartItems);
                DB::table('web_b2b_narudzbina_stavka')->insert($cartItems);
            }

            $order = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$order_id)->first();
            $partner = DB::table('partner')->where('partner_id',$ponuda->partner_id)->first();
            $partnerKorisnik = null;
            $orderItems = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$order->web_b2b_narudzbina_id)->orderBy('broj_stavke','asc')->get();

            $body = View::make('b2b.emails.order',compact('order','partner','partnerKorisnik','orderItems'))->render();

            B2bCommon::send_email_to_client($body,$partner->mail,"Porudžbina WNB".$num_nar);
            B2bCommon::send_email_to_admin($body, $partner->mail, $partner->naziv, "Porudžbina WNB".$num_nar);

       }
	}

	public static function toCart($ponuda_id){
		$ponuda_stavke = DB::table('ponuda_stavka')->where('ponuda_id',$ponuda_id)->get();
		foreach($ponuda_stavke as $ponuda_stavka){
			if(!is_null($ponuda_stavka->roba_id)){
				B2bBasket::addCartB2b($ponuda_stavka->roba_id,$ponuda_stavka->kolicina,1, Session::get('b2b_user_'.B2bOptions::server()));
			}
		}
	}

	public static function clone($ponuda_id){
		$ponuda = (array) DB::table('ponuda')->where('ponuda_id',$ponuda_id)->first();
		$ponuda['ponuda_id'] = DB::select("SELECT nextval('ponuda_ponuda_id_seq') as next_id")[0]->next_id;
        $ponuda['broj_dokumenta'] = "PON".str_pad($ponuda['ponuda_id'],5,"0", STR_PAD_LEFT);
        $ponuda['vrsta_dokumenta_id'] = 40;
        $ponuda['dokumenti_status_id'] = null;
        $ponuda['datum_ponude'] = date('Y-m-d');
        $ponuda['vazi_do'] = date('Y-m-d');
        $ponuda['rok_isporuke'] = date('Y-m-d');
        unset($ponuda['u_korpu']);
		DB::table('ponuda')->insert($ponuda);


		$ponuda_stavke = DB::table('ponuda_stavka')->where('ponuda_id',$ponuda_id)->orderBy('broj_stavke','asc')->get();
		foreach($ponuda_stavke as $ponuda_stavka){
			$ponuda_stavka_arr = (array) $ponuda_stavka;
			$ponuda_stavka_arr['ponuda_id'] = $ponuda['ponuda_id'];
			$ponuda_stavka_arr['ponuda_stavka_id'] = DB::select("SELECT nextval('ponuda_stavka_ponuda_stavka_id_seq') as next_id")[0]->next_id;
			DB::table('ponuda_stavka')->insert($ponuda_stavka_arr);
		}
		return $ponuda['ponuda_id'];
	}

	public static function getStavkaOpis($roba_id){
		
		$opis = DB::table('roba')->where('roba_id',$roba_id)->pluck('web_opis');
		return strlen($opis) > 300 ? substr(strip_tags($opis), 0,300).'...' : $opis;
	}
	
}
