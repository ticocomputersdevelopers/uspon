<?php 

class DokumentiOptions {
	public static function base_url(){
		return DB::table('options')->where('options_id',1316)->pluck('str_data');
	}

	public static function user($kind=null){
		$user = null;
		$org_user = null;

		if(Session::has('dokumenti_user_'.Options::server())){
			$userKind = Session::get('dokumenti_user_kind_'.Options::server());
			$userId = Session::get('dokumenti_user_'.Options::server());

			if($userKind == 'admin'){
				$org_user = DB::table('imenik')->where('imenik_id',$userId)->first();
				$user = (object) array('kind'=>'admin', 'id'=>$org_user->imenik_id);
			}else if($userKind == 'saradnik'){
				$org_user = DB::table('partner')->where('partner_id',$userId)->first();
				$user = (object) array('kind'=>'saradnik', 'id'=>$org_user->partner_id);
			}

		}

		if(!is_null($user) && !is_null($kind)){
			if($kind=='admin' && $user->kind != 'admin'){
				return null;
			}else if($kind=='saradnik' && $user->kind != 'saradnik'){
				return null;
			}
		}

		if(!is_null($org_user)){
			return (object) array_merge((array) $user,(array) $org_user);
		}
		return null;
	}

	public static function company_name(){
		return DB::table('preduzece')->pluck('naziv');
	}

	public static function company_adress(){
		return DB::table('preduzece')->pluck('adresa');
	}
	public static function company_mesto(){
		return DB::table('preduzece')->pluck('mesto');
	}
	public static function company_phone(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->telefon;
			}
	}
	public static function company_fax(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->fax;
			}
			
	}

	public static function company_ziro(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->ziro;
			}
	}
	public static function company_pib(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->pib;
			}
	}
	public static function company_email(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->email;
			}
	}
	public static function company_delatnost_sifra(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->delatnost_sifra;
			}
	}
	public static function company_logo(){
		$cn_query=DB::table('preduzece')->get();
			foreach($cn_query as $row){
				return $row->logo;
			}
	}
	public static function gnrl_options($options_id,$kind='int_data'){
		return DB::table('options')->where('options_id',$options_id)->pluck($kind);
	}

}
