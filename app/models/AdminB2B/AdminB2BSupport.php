<?php

class AdminB2BSupport {

    public static function getProizvodjaci()
    {
        return DB::table('proizvodjac')->select('proizvodjac_id', 'naziv')->where('proizvodjac_id', '<>', -1)->where('proizvodjac_id', '<>', 0)->orderBy('proizvodjac_id', 'ASC')->get();
    }

    public static function getDobavljaci()
    {
        return DB::table('partner')->select('partner_id', 'naziv')->where('partner_id', '<>', -1)->orderBy('naziv', 'ASC')->get();
    }
    public static function find_tarifna_grupa($tarifna_grupa_id, $column)
    {
        return DB::table('tarifna_grupa')->select($column)->where('tarifna_grupa_id', $tarifna_grupa_id)->pluck($column);
    }
    public static function dobavljac($id) 
    {
      return DB::table('partner')->where('partner_id', $id)->pluck('naziv');
    }
    public static function lagerDob($roba_id){
        $kol_fr = DB::table('dobavljac_cenovnik')->select('kolicina')->where('roba_id',$roba_id)->orderBy('kolicina','desc')->orderBy('web_cena','asc')->first();
        if($kol_fr != null){
            return intval($kol_fr->kolicina);
        }else{
            return '';
        }
    }
    public static function barkod($roba_id){
        return DB::table('roba')->where('roba_id',$roba_id)->pluck('barkod');
    }
    //funkcija za vracanje kategorije
    public static function getKategorije()
    {
        return DB::table('partner_kategorija')->where('id_kategorije','!=',-1)->get();
    }
    
  //izvuci iz kategorije
    public static function find_u_kategoriji($id_kategorije, $column)
    {
        return DB::table('partner_kategorija')->select($column)->where('id_kategorije', $id_kategorije)->pluck($column);
    }
    public static function getB2BPartner($id, $column){
        $partner = DB::table('web_b2b_kartica')->where('partner_id', $id)->pluck($column);
        return $partner;
    }
    public static function getVesti($active=null){
        if($active==null){
            $vesti = DB::table('web_vest_b2b')->orderBy('rbr', 'asc')->orderBy('datum', 'dsc')->paginate(20);
        }elseif($active==1 || $active==0){
            $vesti = DB::table('web_vest_b2b')->where('aktuelno',$active)->orderBy('rbr', 'asc')->orderBy('datum', 'dsc')->paginate(20);
        }
        return $vesti;
    }
    public static function find($web_vest_b2b_id, $column) {
        $info = DB::table('web_vest_b2b')->where('web_vest_b2b_id', $web_vest_b2b_id)->pluck($column);
        return $info;
    }
    public static function pages_menu_b2b_show($flag_b2b_show){

        switch($flag_b2b_show){
            case"0":
                echo '  
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" > 
                        <span class="label-text">Header meniju</span>
                    </label>

                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu">
                        <span class="label-text">Footer meniju</span>
                    </label>';
                break;
            case"1":
                echo '  
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" checked="" >
                        <span class="label-text">Header meniju</span>
                    </label>
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu">
                        <span class="label-text">Footer meniju</span>
                    </label>';
                break;

            case"2":
                echo '
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu"  >
                        <span class="label-text">Header meniju</span>
                    </label>
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu" checked="">
                        <span class="label-text">Footer meniju</span>
                    </label>';
                break;
            case"3":
                echo '
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" checked="" >
                        <span class="label-text">Header meniju</span>
                    </label>
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu" checked="">
                        <span class="label-text">Footer meniju</span>
                    </label>';
                break;
            default:
                echo '
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_header_menu" name="b2b_header_menu" >
                        <span class="label-text">Header meniju</span>
                    </label>
                    <label class="checkbox-label medium-6 columns">
                        <input type="checkbox" id="b2b_footer_menu" name="b2b_footer_menu">
                        <span class="label-text">Footer meniju</span>
                    </label>';
                break;
        }

    }
     public static function upload_directory(){
        $dir    = "./images/upload";
        //return $dir;
        $files = scandir($dir);
            $slike="<ul>";
        $br=0;
       foreach ($files as $row) {
       if($br>1){
       
            if (Admin_model::check_admin(array('B2B_STRANICE_AZURIRANJE'))) {
                $slike.="<li><img src='".AdminOptions::base_url()."images/upload/".$row."'  class='images-upload-list' /><a href='javascript:void(0)' title='Obriši' data-url='/images/upload/".$row."' class='images-delete'><i class='fa fa-times' aria-hidden='true'></i></a></li>";
            }else{
                $slike.="<li><img src='".AdminOptions::base_url()."images/upload/".$row."'  class='images-upload-list' /></li>";
           }
        }
           
        
            $br+=1;
        }
        $slike.="</ul>";
        return $slike;
    }
    public static function query_gr_level($grupa_pr_id)
    {
    return DB::select("SELECT grupa_pr_id, parrent_grupa_pr_id, grupa, redni_broj, rabat FROM grupa_pr WHERE parrent_grupa_pr_id = ".$grupa_pr_id." and web_b2b_prikazi=1 ORDER BY redni_broj ASC");
    }

    public static function searchPartneri($word){
        $search = DB::table('partner')->
            where('partner_id', '!=', -1)->
            where('naziv', 'ILIKE', "%$word%")->
            orWhere('pib', 'ILIKE', "%$word%")->
            orderBy('partner_id', 'ASC')->
            paginate(20);
        return $search;
    }

    public static function quotedStr($string){
        $string = str_replace('\'', '', $string);
        $string = str_replace("'", "", $string);
        $string = str_replace(chr(047), "", $string);
        // $string = str_replace(chr(057), "", $string);
        $string = str_replace(chr(134), "", $string);
        $string = chr(047).$string.chr(047);
        return $string;
    }
    public static function getB2BPartneri(){
        $partneri = DB::table('partner')->where('partner_id', '!=', -1)->orderBy('partner_id', 'ASC')->paginate(20);
        return $partneri;
    }
    public static function getB2Bkartica($id){
        $kartica = DB::table('web_b2b_kartica')->where('partner_id',$id)->get();
       
        return $kartica;
    }
    public static function getDuguje($id){
        $duguje = DB::table('web_b2b_kartica')->where('partner_id',$id)->sum('duguje');
       
        return $duguje;
    }
    public static function getPotrazuje($id){
        $potrazuje = DB::table('web_b2b_kartica')->where('partner_id',$id)->sum('potrazuje');
       
        return $potrazuje;
    }
    public static function getSum($id){
        $duguje = DB::table('web_b2b_kartica')->where('partner_id',$id)->sum('duguje');
        $potrazuje = DB::table('web_b2b_kartica')->where('partner_id',$id)->sum('potrazuje');

        $saldo = $potrazuje - $duguje;
        return $saldo;
    }
    public static function narudzbina_kupac_pdf($web_b2b_narudzbina_id){
        $partner=DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->pluck('partner_id');

        foreach(DB::table('partner')->where('partner_id',$partner)->get() as $row){

            $name = '<td>Firma i PIB:</td>
            <td>'.$row->naziv.' '.$row->pib.'</td>';
        

        echo ' 
            <table>
            <tr>
            '.$name.'
            </tr>
            <tr>
            <td>Adresa:</td>
            <td>'.$row->adresa.'</td>
            </tr>
            <tr>
            <td>Mesto:</td>
            <td>'.$row->mesto.'</td>
            </tr>
            <tr>
            <td>Telefon:</td>
            <td>'.$row->telefon.'</td>
            </tr>
            
            </table>';
        }
    }
    
    public static function page_link_b2b($naziv_stranice){
        Session::put('b2b_user_'.Options::server(),1);
        return AdminB2BOptions::base_url().'b2b/'.$naziv_stranice;
    }

    public static function lager($roba_id){
        $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
        $orgj_id = DB::table('imenik_magacin')->where('imenik_magacin_id',20)->limit(1)->pluck('orgj_id');
        $lager = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->first();
        $kolicina = 0;
        if($lager){
            $kolicina += $lager->kolicina;
            if(AdminB2BOptions::web_options(131) == 1){
                $kolicina -= $lager->rezervisano;
            }
        }
        return round($kolicina);
    }

    public static function getB2BLogAkcijaId($sifra_akcije){
        $log_b2b_akcija = DB::table('log_b2b_akcija')->where('sifra', $sifra_akcije)->pluck('log_b2b_akcija_id');
        if(is_null($log_b2b_akcija)){
            $naziv= str_replace('_',' ',strtolower($sifra_akcije));
            DB::table('log_b2b_akcija')->insert(['sifra' => $sifra_akcije, 'log_b2b_akcija_id'=> DB::table('log_b2b_akcija')->max('log_b2b_akcija_id')+1,'naziv'=>$naziv]);
            DB::statement("SELECT setval('log_b2b_akcija_log_b2b_akcija_id_seq', (SELECT MAX(log_b2b_akcija_id) FROM log_b2b_akcija), FALSE)");
            $log_b2b_akcija = DB::table('log_b2b_akcija')->where('sifra', $sifra_akcije)->pluck('log_b2b_akcija_id');
            return $log_b2b_akcija;
        }else{
            return $log_b2b_akcija;
        }
    }
    public static function saveB2BLog($sifra_akcije,array $ids=array()){
       if(Session::has('b2b_user_'.AdminB2BOptions::server())){
            
            $nazivi = self::logB2BItemsNamesJson($sifra_akcije, $ids);
           
            $log_b2b_akcija_id = self::getB2BLogAkcijaId($sifra_akcije);
            DB::table('log_b2b_partner')->insert(array('partner_id'=>Session::get('b2b_user_'.AdminB2BOptions::server()),'log_b2b_akcija_id'=>$log_b2b_akcija_id, 'ip' => B2b::ip_adress(),'datum'=>date('Y-m-d H:i:s'), 'ids'=>implode(',', $ids), 'naziv_proizvoda'=>$nazivi));
        
        }
    }

    public static function logB2BItemsNames($data){
        $rows = json_decode($data);
        if(count($rows) > 0){
            return $rows;
        }
        return array();
    }

    public static function logB2BItemsNamesJson($logCode, array $ids = array()){
        if(count($ids)==0){
            return json_encode([]);
        }

        $results = array();
        $ids = array_map('intval',$ids);

        if (in_array($logCode, 
            array(
                'PARTNER_B2B_LOGIN',
                'PARTNER_B2B_LOGOUT'                          
            )
        )) {
            $results = array_map(function($item){
                return (object) array('id'=>$item->partner_id, 'naziv'=>$item->naziv, 'datum' => date('Y-m-d H:i:s'));
            },DB::table('partner')->select('partner_id','naziv')->whereIn('partner_id',$ids)->get());


        }else if (in_array($logCode, 
            array(
                'PARTNER_B2B_GROUP_VIEW'                        
            )
        )) {
            $results = array_map(function($item){
                return (object) array('id'=>$item->grupa_pr_id, 'naziv'=>$item->grupa, 'datum' => date('Y-m-d H:i:s'));
            },DB::table('grupa_pr')->select('grupa_pr_id','grupa')->whereIn('grupa_pr_id',$ids)->get());


        }else if (in_array($logCode, 
            array(
                'PARTNER_B2B_PRODUCT_VIEW'                        
            )
        )) {
            $results = array_map(function($item){
                return (object) array('id'=>$item->roba_id, 'naziv'=>$item->naziv_web, 'datum' => date('Y-m-d H:i:s'));
            },DB::table('roba')->select('roba_id','naziv_web')->whereIn('roba_id',$ids)->get());
        }

        return json_encode($results);
    }

    public static function unique_multidim_array($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();
       
        foreach($array as $val) {
            if (!in_array($val->{$key}, $key_array)) {
                $key_array[$i] = $val->{$key};
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    } 

    public static function partner_b2b_logs_details($proizvodi) {
        $proizvodi = json_decode($proizvodi);
        if(is_null($proizvodi) || !is_array($proizvodi)){
            return array();
        }
        $array_count_values = array_count_values(array_column($proizvodi,'id')); 
        $uniqueProizvodi = self::unique_multidim_array($proizvodi,'id');

        $mappedProizvodi = array_map(function($item) use ($array_count_values){
            $itemArr = (array) $item;
            $itemArr['count'] = $array_count_values[$item->id];
            return (object) $itemArr;
        },$uniqueProizvodi);

        usort($mappedProizvodi, function ($item1, $item2) {
            return $item1->count < $item2->count;
        });
        return $mappedProizvodi;
    }

    public static function getTipoviB2B($active=null)
    {
        if($active){
            return DB::table('tip_artikla')->select('tip_artikla_id','naziv','rbr')->where('prikaz','<>',0)->where('tip_artikla_id','>',0)->orderBy('rbr','asc')->get();
        }else{
            return DB::table('tip_artikla')->select('tip_artikla_id','naziv')->where('prikaz','<>',0)->where('active',1)->where('tip_artikla_id','>',0)->orderBy('rbr','asc')->get();
        }  
    }

     public static function pages($parent_id=0){
      if($parent_id==0){
        return DB::table('web_b2b_seo')->where('parent_id',0)->orWhereNull('parent_id')->orderBy('rb_strane','asc')->get();
      }else{
        return DB::table('web_b2b_seo')->where('parent_id',$parent_id)->orderBy('rb_strane','asc')->get();
      }
    }
    public static function parent_pages($parent_id=0,$page_id){
        return DB::table('web_b2b_seo')->where('parent_id',$parent_id)->where('web_b2b_seo_id','!=',$page_id)->orderBy('rb_strane','asc')->get();
    }
    // public static function page_level($page_id,$level=1){
    //   $page = DB::table('web_b2b_seo')->where('web_b2b_seo_id',$page_id)->first();
    //   if($page->parent_id == 0){
    //     return $level;
    //   }else{
    //     return self::page_level($page->parent_id,($level+1));
    //   }
    // }

    public static function sectionTypes(){

        if(!AdminOptions::is_shop()){
            $query = DB::table('b2b_sekcija_stranice_tip')->where('flag_aktivan',1)->where('naziv','!=','Lista artikala');
        }else{
            $query = DB::table('b2b_sekcija_stranice_tip')->where('flag_aktivan',1);
        }

        return $query->orderBy('rbr','asc')->get();
    }

    public static function getSectionType($type_id){
        return DB::table('b2b_sekcija_stranice_tip')->where('b2b_sekcija_stranice_tip_id',$type_id)->first();
    }


    public static function sectionLang($page_section_id,$lang_id){
        return DB::table('b2b_sekcija_stranice_jezik')->where(['b2b_sekcija_stranice_id'=>$page_section_id,'jezik_id'=>$lang_id])->first();
    }

    public static function pageSections($page_id){
        return DB::table('b2b_stranica_sekcija')->select('b2b_sekcija_stranice.*','b2b_stranica_sekcija.rbr')->where('web_b2b_seo_id',$page_id)->join('b2b_sekcija_stranice','b2b_sekcija_stranice.b2b_sekcija_stranice_id','=','b2b_stranica_sekcija.b2b_sekcija_stranice_id')->orderBy('rbr','asc')->get();
    }

    public static function sliders(){
        return DB::table('b2b_slajder')->where(['flag_aktivan'=>1])->whereIn('b2b_slajder_tip_id',array_map('current',DB::table('b2b_slajder_tip')->whereNotIn('naziv',['Popup Baner'])->get()))->orderBy('b2b_slajder_id','asc')->get();
    }

    public static function articleTypes(){
        return DB::table('tip_artikla')->where('active',1)->orderBy('rbr','asc')->get();
    }
    public static function sliderTypes(){
        return DB::table('b2b_slajder_tip')->where('flag_aktivan',1)->orderBy('rbr','asc')->get();
    }

    // public static function getSliderType($type_id){
    //  return DB::table('slajder_tip')->where('slajder_tip_id',$type_id)->first();
    // }

    public static function getSliderTypeBySliderId($slider_id){
        return DB::table('b2b_slajder_tip')->where('b2b_slajder_tip_id',DB::table('b2b_slajder')->where('b2b_slajder_id',$slider_id)->pluck('b2b_slajder_tip_id'))->first();
    }

    // public static function sliders(){
    //  return DB::table('slajder')->where('flag_aktivan',1)->orderBy('rbr','asc')->get();
    // }

    public static function sliderItems($slider_id){
        return DB::table('b2b_slajder_stavka')->where(['b2b_slajder_id'=>$slider_id])->orderBy('rbr','asc')->get();
    }

    public static function sliderItem($slider_item_id){
        return DB::table('b2b_slajder_stavka')->where(['b2b_slajder_stavka_id'=>$slider_item_id])->first();
    }

    public static function sliderItemLang($slider_item_id,$lang_id){
        return DB::table('b2b_slajder_stavka_jezik')->where(['b2b_slajder_stavka_id'=>$slider_item_id,'jezik_id'=>$lang_id])->first();
    }
    public static function page_slug($naziv_stranice,$lang_id=null){
        if($lang_id==null){
            $lang_id = 1;
        }
        $jezik_kod = AdminLanguage::multi() ? DB::table('jezik')->where(array('aktivan'=>1, 'jezik_id'=>$lang_id))->pluck('kod').'/' : '';
        $web_b2b_seo = DB::table('web_b2b_seo')->where('naziv_stranice',$naziv_stranice)->first();
        if(!is_null($web_b2b_seo)){
            $web_b2b_seo_jezik = DB::table('web_b2b_seo_jezik')->where(array('web_b2b_seo_id'=>$web_b2b_seo->web_b2b_seo_id,'jezik_id'=>$lang_id))->first();
            if(!is_null($web_b2b_seo_jezik) && !is_null($web_b2b_seo_jezik->slug) && !is_null($web_b2b_seo_jezik->naziv) && $web_b2b_seo_jezik->slug != '' && $web_b2b_seo_jezik->naziv != ''){
                return (object) array('slug'=>$jezik_kod.$web_b2b_seo_jezik->slug,'naziv'=>$web_b2b_seo_jezik->naziv);
            }
            return (object) array('slug'=>$jezik_kod.$naziv_stranice,'naziv'=>$web_b2b_seo->title);
        }
        return (object) array('slug'=>$jezik_kod.$naziv_stranice,'naziv'=>'');
    }
    public static function page_childs_depth($pages_ids,$count=0){
      $pages = DB::table('web_b2b_seo')->select('web_b2b_seo_id')->whereIn('parent_id',$pages_ids)->get();
      if(count($pages) == 0){
        return $count;
      }else{
          return self::page_childs_depth(array_map('current',$pages),($count+1));
      }
    }
    // B2B ARTICLES LISTING
    public static function mostPopularArticles(){
        // $popular = DB::table('roba')->where('flag_aktivan', 1)->where('flag_cenovnik', 1)->orderBy('pregledan_puta', 'DSC')->limit(5)->get();
        $popular=DB::select("SELECT DISTINCT r.roba_id, r.pregledan_puta FROM roba r".B2b::checkLagerB2b('join').AdminOptions::checkImage('join').AdminOptions::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND r.roba_id <> -1".B2b::checkLagerB2b().AdminOptions::checkImage().AdminOptions::checkPrice().AdminOptions::checkDescription().AdminOptions::checkCharacteristics()." ORDER BY r.pregledan_puta DESC LIMIT 4");

        return $popular;
    }
    public static function latestAdded($limit=4){
        // DB::table('roba')->where('flag_aktivan', 1)->where('flag_cenovnik', 1)->orderBy('roba_id', 'DSC')->limit(5)->get();
        $latest = DB::select("SELECT DISTINCT r.roba_id FROM roba r".B2b::checkLagerB2b('join').Product::checkActiveGroup('join').Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND r.roba_id <> -1".B2b::checkLagerB2b().Product::checkActiveGroup().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()." ORDER BY r.roba_id DESC LIMIT ".$limit."");
        return $latest;
    }
    public static function bestSeller($limit=4){
        $roba_ids_string = '';
        $roba_ids = DB::select("SELECT DISTINCT r.roba_id FROM roba r".B2b::checkLagerB2b('join').Product::checkActiveGroup('join').Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND r.roba_id <> -1".B2b::checkLagerB2b().Product::checkActiveGroup().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."");
        if(count($roba_ids) > 0){
            $roba_ids_string = 'WHERE roba_id IN ('.implode(',',array_map('current',$roba_ids)).')';
        }
        if($roba_ids_string == ''){
            return (object) array();
        }else{
            return DB::select('
                SELECT roba_id, SUM(kolicina) as count 
                FROM web_b2c_narudzbina_stavka ns LEFT JOIN web_b2c_narudzbina n 
                ON n.web_b2c_narudzbina_id = ns.web_b2c_narudzbina_id 
                '.$roba_ids_string.' AND realizovano = 1 AND stornirano != 1
                GROUP BY roba_id 
                ORDER BY count 
                DESC LIMIT '.$limit.'');
        }
    }

    public static function artikli_tip($tip,$limit=null,$offset=null){
        $query = "SELECT DISTINCT r.roba_id FROM roba r".B2b::checkLagerB2b('join').Product::checkImage('join').Product::checkActiveGroup('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND tip_cene = ".$tip." ".B2b::checkLagerB2b().Product::checkImage().Product::checkActiveGroup().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()." ORDER BY r.roba_id ASC"; // ORDER BY r.roba_id ASC

        if(isset($offset) && isset($limit)){
            $offset = ($offset-1)*$limit;
            $query .= " LIMIT ".$limit." OFFSET ".$offset."";
        }elseif(isset($limit)){
            $query .= " LIMIT ".$limit."";
        }

        return $array_tip = DB::select($query);
    }
    public static function akcija($grupa_pr_id=null,$limit=null,$offset=null){
        $datum = date('Y-m-d');
        $query = "SELECT DISTINCT r.roba_id, akcija_redni_broj FROM roba r".B2b::checkLagerB2b('join').Product::checkActiveGroup('join').Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_cenovnik = 1 AND akcija_flag_primeni = 1 AND CASE WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_od <= '".$datum."' AND datum_akcije_do >= '".$datum."' 
        WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NULL THEN datum_akcije_od <= '".$datum."' 
        WHEN datum_akcije_od IS NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_do >= '".$datum."' 
        ELSE 1 = 1 END ".B2b::checkLagerB2b().Product::checkActiveGroup().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."";

        if(isset($grupa_pr_id)){
            $grupe = Groups::vratiSveGrupe($grupa_pr_id);
            $query .= " AND grupa_pr_id IN (".implode(',',$grupe).")";
        }

        $query .= " ORDER BY akcija_redni_broj, r.roba_id ASC";     
        if(isset($offset) && isset($limit)){
            $offset = ($offset-1)*$limit;
            $query .= " LIMIT ".$limit." OFFSET ".$offset."";
        }elseif(isset($limit)){
            $query .= " LIMIT ".$limit."";
        }

        return $array_tip = DB::select($query);
    }

}
