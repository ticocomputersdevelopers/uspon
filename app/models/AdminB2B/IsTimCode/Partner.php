<?php
namespace IsTimCode;
use AdminB2BIS;
use DB;

class Partner {

	public static function table_body($partners){

		$result_arr = array();

		$partner_id = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
		foreach($partners as $partner) {
	    	$partner_id++;

            $id_is=$partner->id;
            $sifra=isset($partner->attributes->sifra) ? pg_escape_string($partner->attributes->sifra) : null;
            $naziv=isset($partner->attributes->naziv) ? pg_escape_string($partner->attributes->naziv) : null;
            $mesto=isset($partner->attributes->adresa2) ? pg_escape_string($partner->attributes->adresa2) : null;
            $adresa=isset($partner->attributes->adresa1) ? pg_escape_string($partner->attributes->adresa1) : null;
            $telefon=isset($partner->attributes->telefon) ? pg_escape_string($partner->attributes->telefon) : null;
            $pib=isset($partner->attributes->pib) ? $partner->attributes->pib : null;
            $mail=isset($partner->attributes->mail) ? $partner->attributes->mail : null;
            $rabat=isset($partner->attributes->rabat) ? $partner->attributes->rabat : 0;
            $status=isset($partner->attributes->status) ? $partner->attributes->status : "K";
            $broj_maticni="";

	        $result_arr[] = "(".strval($partner_id).",'".$sifra."','".trim($naziv)."','".trim($adresa)."','".trim($mesto)."',0,'".trim($telefon)."',NULL,'".strval($pib)."','".strval($broj_maticni)."',NULL,NULL,NULL,NULL,".strval($rabat).",0,0,'".trim($naziv)."',0,0,1,1,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'".trim($mail)."',0,1,0,0,NULL,1,'".$id_is."',(NULL)::integer,NULL,NULL,(NULL)::integer)";
		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="partner_id" && $col!="id_is" && $col!="rabat" && $col!="login" && $col!="password"){
		    	$updated_columns[] = "".$col." = partner_temp.".$col."";
			}
		}
		DB::statement("UPDATE partner t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=partner_temp.id_is::varchar");

		//insert
		DB::statement("INSERT INTO partner (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner t WHERE t.id_is=partner_temp.id_is::varchar))");

		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		DB::statement("SELECT setval('partner_partner_id_seq', (SELECT MAX(partner_id) FROM partner) + 1, FALSE)");
	}
}