<?php
namespace IsTimCode;
use SoapClient;
use AdminB2BOptions;
use All;

class TimCodeAPI {

	public static function groups($auth){
		$data['auth'] = $auth;

		$data['arrFilter'] = array();
		$data['i_mDate'] = date("Y-m-d",strtotime("0000-00-00"));
		$client = new SoapClient('https://tim-erp.com/ERPX_WEB/awws/ErpX.awws?wsdl');
		$response = $client->wsGetGrpRoba($data);

		$result = json_decode($response);
		if($result && is_object($result)){
			return $result->grpRoba;
		}
		return [];
	}

	public static function manufacturers($auth){
		$data['auth'] = $auth;

		$data['arrFilter'] = array();
		$data['i_mDate'] = date("Y-m-d",strtotime("0000-00-00"));
		$client = new SoapClient('https://tim-erp.com/ERPX_WEB/awws/ErpX.awws?wsdl');
		$response = $client->wsGetManuf($data);

		$result = json_decode($response);
		if($result && is_object($result)){
			return $result->manuf;
		}
		return [];
	}

	public static function articles($auth,array $magacini){
		$data['auth'] = $auth;

		$data['arrMagacinID'] = $magacini;
		$data['arrFilter'] = array();
		$data['i_mDate'] = date("Y-m-d",strtotime("0000-00-00"));
		$client = new SoapClient('https://tim-erp.com/ERPX_WEB/awws/ErpX.awws?wsdl');
		$response = $client->wsGetRoba($data);

		$result = json_decode($response);

		if($result && is_object($result)){
			return $result->roba;
		}
		return [];
	}

	public static function magacini($auth){
		$data['auth'] = $auth;

		$data['arrFilter'] = array();
		$data['i_mDate'] = date("Y-m-d",strtotime("0000-00-00"));
		$client = new SoapClient('https://tim-erp.com/ERPX_WEB/awws/ErpX.awws?wsdl');
		$response = $client->wsGetMagacin($data);

		$result = json_decode($response);
		if($result && is_object($result)){
			return $result;
		}
		return [];
	}

	public static function pricelist($auth,$komintentId,$magacinId,$datum=null){
		if(is_null($datum)){
			$datum = date('Y-m-d');
		}
		$data['auth'] = $auth;
		$data['komitID'] = $komintentId;
		$data['magacinID'] = $magacinId;
		$data['datum'] = $datum;
		$data['arrFilter'] = array();
		$data['i_mDate'] = date("Y-m-d",strtotime("0000-00-00"));
		$client = new SoapClient('https://tim-erp.com/ERPX_WEB/awws/ErpX.awws?wsdl');
		$response = $client->wsGetAkcijskeCene($data);

		$result = json_decode($response);
All::dd($result );
		if($result && is_object($result)){
			return $result->roba;
		}
		return [];
	}

	public static function partneri($auth){
		$data = array(
			'auth' => $auth,
			'arrFilter' => array()
		);

		$client = new SoapClient('https://tim-erp.com/ERPX_WEB/awws/ErpX.awws?wsdl');
		$response = $client->wsGetKomit($data);

		$result = json_decode($response);
		if($result && is_object($result)){
			return $result->komit;
		}
		return [];
	}

}