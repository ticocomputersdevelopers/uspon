<?php
namespace ISTables;
use AdminB2BIS;
use PHPExcel;
use PHPExcel_IOFactory;

class ArtCatExcel {

	public static function table_body(){

		$products_file = "files/IS/excel/roba_grupe.xlsx";
		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
		$excelObj = $excelReader->load($products_file);
		$worksheet = $excelObj->getSheet(0);
		$lastRow = $worksheet->getHighestRow();

		$result_arr = array();
		$category_ids = array();
		for ($row = 1; $row <= $lastRow; $row++) {
		    $B = $worksheet->getCell('B'.$row)->getValue();
		    $C = $worksheet->getCell('C'.$row)->getValue();
		    $D = $worksheet->getCell('D'.$row)->getValue();

		    if(is_numeric($B) && is_numeric($D)){
		        $result_arr[] = "(".intval($B).",'".addslashes(AdminB2BIS::encodeTo1250($C))."',NULL,".intval($D).",".intval($B).",1,1,1,0,NULL,NULL,NULL,NULL,0,0,NULL,NULL)";
		        $category_ids[] = intval($B);
		    }

		}
		return (object) array("body"=>implode(",",$result_arr),"ids"=>$category_ids);
	}

	public static function query_execute($table_temp_body) {

		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba_grupe'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_grupe_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		//insert
		DB::statement("INSERT INTO roba_grupe (SELECT * FROM ".$table_temp.")");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}
}