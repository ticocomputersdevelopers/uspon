<?php
namespace IsXml;
use DB;

class Article {

	public static function table_body($articles){
		$result_arr = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;

		foreach($articles as $article) {
			if(isset($article->ID)){
				$id_is = $article->ID;
				$roba_id++;
				$sifra_k++;

				$sifra_is = isset($article->sifra) ? pg_escape_string($article->sifra) : '';
				$naziv = isset($article->naziv) ? Support::convert(substr($article->naziv,0,300)) : '';
				$web_opis = isset($article->opis) ? Support::convert($article->opis) : '';
				$jedinica_mere_id = isset($article->jedinica_mere) && $article->jedinica_mere != '' ? Support::getJedinicaMereId($article->jedinica_mere) : 1;
				$proizvodjac_id = isset($article->proizvodjac) && trim($article->proizvodjac) != '' ? Support::getProizvodjacId(Support::convert($article->proizvodjac)) : -1;
				$vrednost_tarifne_grupe = isset($article->pdv) && is_numeric($article->pdv) ? $article->pdv : 20;
				$tarifna_grupa_id = isset($article->tarifna_grupa) ? Support::getTarifnaGrupaId($article->tarifna_grupa,$vrednost_tarifne_grupe) : 0;
				$grupa_pr_id = isset($article->grupa) && $article->grupa != '' ? Support::getGrupaId($article->grupa) : -1;
				// $grupa_pr_id = Support::getGrupaId('NOVI ARTIKLI');
				$racunska_cena_nc = isset($article->cena) && is_numeric(floatval($article->cena)) ? floatval($article->cena) : 0;
				$mpcena = isset($article->mpcena) && is_numeric(floatval($article->mpcena)) ? floatval($article->mpcena) : 0;
				$web_cena = isset($article->web_cena) && is_numeric(floatval($article->web_cena)) ? floatval($article->web_cena) : 0;

				$marza = isset($article->marza) && is_numeric(floatval($article->marza)) ? floatval($article->marza) : 0;
				$flag_aktivan = $racunska_cena_nc > 0 ? '1' : '0';
				// $flag_prikazi_u_cenovniku = isset($article->prikaz) && is_numeric(intval($article->prikaz)) && in_array(intval($article->prikaz),array(0,1)) ? strval($article->prikaz) : '0';
				$flag_prikazi_u_cenovniku = $web_cena > 0 ? '1' : '0';

				$result_arr[] = "(".strval($roba_id).",NULL,'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".substr($naziv,0,20)."',0,-1,0,0,0,0,9,0,0,0,0,1,".$flag_prikazi_u_cenovniku.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_nc).",0,".strval($racunska_cena_nc).",0,NULL,".strval($mpcena).",false,".strval($marza).",(NULL)::integer,'".$naziv."',1,NULL,NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,-1,".strval($web_cena).",1,0,'".$web_opis."',NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,1,0,NULL,0,0,1,1,-1,NULL,NULL,NULL,NULL,0,0.00,0.00,0.00,0,NULL,(NULL)::date,(NULL)::date,(NULL)::integer,NULL,'".strval($sifra_is)."','".strval($id_is)."',0,0)";
			}
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="sifra_d" && $col!="sifra_is" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> ''");	
		//insert
		DB::statement("INSERT INTO roba(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp." WHERE roba_temp.id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");


		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.id_is NOT IN (SELECT id_is FROM ".$table_temp.")");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}