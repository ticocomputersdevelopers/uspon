<?php
namespace IsXml;

use DB;


class DBData {

	public static function articles(){
		return DB::connection('dsc')->select("SELECT art_id as ID, art_naziv as naziv, art_pdv as pdv, (select kat_naziv from kategorije where a.kat_id = kat_id) as nadgrupa, (select podkat_naziv from podkategorije where a.podkat_id = podkat_id) as grupa, (select brand_naziv from brand where a.brand_id = brand_id) as proizvodjac, (art_kol - art_kol_rezervisano) as kolicina, art_cena*120 as cena, art_cena*120*1.1*1.2 as mpcena, art_cena*120*1.1*1.2 as web_cena, 10 as marza, art_opis as opis FROM artikli a WHERE art_aktivan = 'Y'");
	}

	public static function partners(){
		$partners = array();
		return $partners;
	}


}