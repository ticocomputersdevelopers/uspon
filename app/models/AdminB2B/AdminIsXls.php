<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsXls\ReadFile;
use IsXls\Article;
use IsXls\Group;
use IsXls\Lager;
use IsXls\Osobine;
use IsXls\Ftp;
use IsXls\Images;
use IsXls\Support;

class AdminIsXls {

    // http://planetsport.selltico.com/auto-import-is/9f0dadfba1fbfd5634ff3c3f5dbda743
    public static function xls_execute(){
        $result = ReadFile::articles();
        return self::execute($result->articles);
    }

    public static function xlsx_execute(){
        $result = ReadFile::articles(true); 
        return self::execute($result->articles);
    }
    
    public static function execute($articles){

        try {
            // //groups        
            Ftp::ftpConnection();
            $groups=ReadFile::groups();
            //All::dd($groups);
            $dodatne_groupe1=ReadFile::dodatne_groupe1();
            $dodatne_groupe2=ReadFile::dodatne_groupe2();

            // All::dd($dodatne_groupe2);
            $resultGroup = Group::table_body($groups);
            Group::query_insert_update($resultGroup->body);

            $resultDodatne_groupe1 = Group::table_body($dodatne_groupe1);
            Group::query_insert_update($resultDodatne_groupe1->body);

            $resultDodatne_groupe2 = Group::table_body($dodatne_groupe2);
            Group::query_insert_update($resultDodatne_groupe2->body);

            Support::updateGroupsParent();       

            //articles
            //$articles=ReadFile::articles();
            $resultArticle = Article::table_body($articles);
            if($resultArticle->body!=''){
                Article::query_insert_update($resultArticle->body,array('web_cena','flag_prikazi_u_cenovniku','grupa_pr_id','flag_aktivan'));
                Article::query_update_unexists($resultArticle->body);
            }
            
            $doatneGrupeUpdate = Group::dodatneGrupeUpdate($articles);

            //osobine
            $resultOsobine = Osobine::table_body($articles);
            if($resultOsobine->osobina_kombinacija !=''){
                Osobine::query_insert_update($resultOsobine);
                Osobine::update_osobina_kombinacija_vrednost($articles);
            }
            // lager
            $mappedArticles = Support::getMappedArticles();
            $resultStock = lager::table_body($articles,$mappedArticles);
            lager::query_insert_update($resultStock->body);

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }

}