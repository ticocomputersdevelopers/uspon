<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsLogik\DBData;
 
use IsLogik\Support;
use IsLogik\Group;
use IsLogik\ArticleGroup;
use IsLogik\Article;
use IsLogik\Stock;
use IsLogik\Komercijalisti;
use IsLogik\Partner;
use IsLogik\PartnerGroup;
use IsLogik\PartnerSellers;
use IsLogik\PartnerAddress;
use IsLogik\PartnerCard;
use Service\ElasticSearchService;


class AdminIsLogik {
    // /auto-import-is/13b80ca7f6c9af1087512c0427179fd5
    public static function execute(){

        try {

            //DB::statement("UPDATE roba r set flag_cenovnik=0 WHERE sifra_is is null OR racunska_cena_end < 1 OR (select sum(kolicina) from lager l where orgj_id!=1 and l.roba_id = r.roba_id)<1 OR r.roba_id NOT IN (SELECT roba_id from lager where orgj_id!=1) ");

            
            //articles
            $articles = DBData::articlesKol();
            $resultArticle = Article::table_body($articles);
            $id_iss = Article::query_insert_update($resultArticle->body,array('naziv','naziv_web','sifra_is','flag_aktivan','flag_prikazi_u_cenovniku','flag_cenovnik','proizvodjac_id','naziv_displej','jedinica_mere_id','tarifna_grupa_id','racunska_cena_end','web_cena','mpcena','barkod'));
            Article::query_update_unexists($resultArticle->body);
          
            $mappedArticles = Support::getMappedArticles();
            DB::statement("UPDATE roba r set flag_cenovnik=0 WHERE ( sifra_is is null OR racunska_cena_end < 1 OR (select sum(kolicina) from lager l where orgj_id!=1 and l.roba_id = r.roba_id)<1 ) AND flag_zakljucan='false'");

            //DB::statement("update lager set sifra_is = CONCAT_WS('-',orgj_id, roba.id_is) from roba where roba.roba_id=lager.roba_id and orgj_id !=1 and lager.sifra_is is null");
            
            //stocks
            Support::update_sifra_is_lager();
            $stock = DBData::stock_short();
            $mappedStockrooms = Support::getMappedStockrooms();
            $resultStock = Stock::table_body($stock,$mappedArticles,$mappedStockrooms,array('kolicina'));
            Stock::query_insert_update($resultStock->body);      
            Stock::query_update_unexists($resultStock->body);           
            //DB::statement("UPDATE roba r set flag_cenovnik=0 WHERE ( sifra_is is null OR racunska_cena_end < 1 OR (select sum(kolicina) from lager l where orgj_id!=1 and l.roba_id = r.roba_id)<1 ) AND flag_zakljucan='false'");
          
            Support::veleprodaja1_magacin();
            
            Support::convertCharsGrupa();

            Support::linkedWidthDC();
            Support::deleteDouble();
           //die('test');
            
            //partner
            $partners = DBData::partners();
            $resultPartner = Partner::table_body($partners);
            Partner::query_insert_update($resultPartner->body,array('naziv','adresa','mesto','telefon','fax','limit_p','dani_valute','naziv_puni','mail','komercijalista','imenik_id'));
            
            $mappedPartners = Support::getMappedPartners();
            
            //partner_address
            $partner_address = DBData::partner_adress();
            $resultPartnerAddress = PartnerAddress::table_body($partner_address,$mappedPartners);
            PartnerAddress::query_insert_update($resultPartnerAddress->body);
            
            //komercijalisti
            $komercijalisti = DBData::partners();            
            $resultKomercijalisti = Komercijalisti::table_body($komercijalisti);
            Komercijalisti::query_insert_update($resultKomercijalisti->body);
            
            $mappedSellers = Support::getMappedSellers();
            //partner seller
            $resultPartnerSeller = PartnerSellers::table_body($komercijalisti,$mappedSellers,$mappedPartners);
            PartnerSellers::query_insert_update($resultPartnerSeller->body);
            
            Support::updateStatusNarudzbina();
            DB::statement("UPDATE roba r set flag_cenovnik=0 WHERE ( sifra_is is null OR racunska_cena_end < 1 OR (select sum(kolicina) from lager l where orgj_id!=1 and l.roba_id = r.roba_id)<1 ) AND flag_zakljucan='false'");
            
            DB::statement("UPDATE roba r set flag_prikazi_u_cenovniku=0 WHERE web_cena =0 ");

            //skidanje artikala sa akcije
            DB::statement("UPDATE roba set akcija_flag_primeni = 0 where flag_aktivan = 1 and flag_zakljucan = false and akcija_flag_primeni = 1 and datum_akcije_do is not null and datum_akcije_do < '".date('Y-m-d')."' ");
          
            // AdminB2BIS::synchronization(false);
            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            echo $e->getMessage();
            AdminB2BIS::synchronization(false);
            AdminB2BIS::saveISLog('false');
            AdminB2BIS::saveISLogError($e->getMessage());
            AdminB2BIS::sendNotification(array(9,12,15,18),10,0);
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }
    
    // /auto-import-is-cart/13b80ca7f6c9af1087512c0427179fd5
    public static function updateCart(){

        try {
            $mappedPartners = Support::getMappedPartners();

            //partner card
            $partnersCards = DBData::partners_cards_all();
            $resultPartnersCards = PartnerCard::table_body($partnersCards,$mappedPartners);
            PartnerCard::query_insert_update($resultPartnersCards->body);

            

            AdminB2BIS::synchronization(false);
            AdminB2BIS::saveCardISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::synchronization(false);
            AdminB2BIS::saveCardISLog('false');
            AdminB2BIS::saveISLogError($e->getMessage());
            AdminB2BIS::sendNotification(array(1),50,15);
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }
    public static function execute_short_price(){
        
        try{
            
            $articles = DBData::articlesKol();
            $resultArticle = Article::table_body($articles);
            $id_iss = Article::query_insert_update($resultArticle->body,array('racunska_cena_end','web_cena','mpcena'));
            //DB::statement("UPDATE roba r set flag_cenovnik=0 WHERE sifra_is is null OR racunska_cena_end < 1 OR (select sum(kolicina) from lager l where orgj_id!=1 and l.roba_id = r.roba_id)<1 OR r.roba_id NOT IN (SELECT roba_id from lager where orgj_id!=1) ");


            Support::linkedWidthDC();
            Support::deleteDouble();
            DB::statement("UPDATE roba r set flag_cenovnik=0 WHERE (sifra_is is null OR racunska_cena_end < 1 OR (select sum(kolicina) from lager l where orgj_id!=1 and l.roba_id = r.roba_id)<1 OR r.roba_id NOT IN (SELECT roba_id from lager where orgj_id!=1)) AND flag_zakljucan='false' ");

            AdminB2BIS::saveISLogPrice('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            echo $e->getMessage();
            AdminB2BIS::synchronization(false);
            AdminB2BIS::saveISLogPrice('false');
            AdminB2BIS::saveISLogPriceError($e->getMessage());
            AdminB2BIS::sendNotification(array(9,12,15,18),10,0);
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }

    public static function execute_short_quantity(){
        
        try{

            $mappedArticles = Support::getMappedArticles();
            //stocks
            $stock = DBData::stock_short();
            $mappedStockrooms = Support::getMappedStockrooms();
            $resultStock = Stock::table_body($stock,$mappedArticles,$mappedStockrooms,array('kolicina'));
            Stock::query_insert_update($resultStock->body);      
            Stock::query_update_unexists($resultStock->body);
            
            DB::statement("UPDATE roba r set flag_cenovnik=0 WHERE (sifra_is is null OR racunska_cena_end < 1 OR (select sum(kolicina) from lager l where orgj_id!=1 and l.roba_id = r.roba_id)<1 OR r.roba_id NOT IN (SELECT roba_id from lager where orgj_id!=1)) AND flag_zakljucan='false'");
                       
            Support::veleprodaja1_magacin();
            
            DB::statement("UPDATE roba r set flag_cenovnik=0 WHERE (sifra_is is null OR racunska_cena_end < 1 OR (select sum(kolicina) from lager l where orgj_id!=1 and l.roba_id = r.roba_id)<1 OR r.roba_id NOT IN (SELECT roba_id from lager where orgj_id!=1)) AND flag_zakljucan='false'");
            AdminB2BIS::saveISLogQuantity('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            echo $e->getMessage();
            AdminB2BIS::synchronization(false);
            AdminB2BIS::saveISLogQuantity('false');
            AdminB2BIS::saveISLogQuantityError($e->getMessage());
            AdminB2BIS::sendNotification(array(9,12,15,18),10,0);
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }


}