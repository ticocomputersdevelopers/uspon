<?php
namespace IsSpSoft;

use DB;
use Groups;


class Support {


	public static function getPartnerId($sifra_kupca_logik){
		$partner = DB::table('partner')->where('id_is',$sifra_kupca_logik)->first();
		if(!is_null($partner)){
			return $partner->partner_id;
		}
		return null;
	}

	public static function getTarifnaGrupaId($naziv_tarifne_grupe,$vrednost_tarifne_grupe=20){
		$tg = DB::table('tarifna_grupa')->where(array('porez'=>$vrednost_tarifne_grupe))->first();

		if(is_null($tg)){
			$next_id = DB::table('tarifna_grupa')->max('tarifna_grupa_id')+1;
			DB::table('tarifna_grupa')->insert(array(
				'tarifna_grupa_id' => $next_id,
				'sifra' => $next_id,
				'naziv' => substr($naziv_tarifne_grupe,0,99),
				'porez' => $vrednost_tarifne_grupe,
				'active' => 1,
				'default_tarifna_grupa' => 0,
				'tip' => substr($vrednost_tarifne_grupe,0,19)
				));
			$tg = DB::table('tarifna_grupa')->where('porez',$vrednost_tarifne_grupe)->first();
		}
		return $tg->tarifna_grupa_id;
	}

	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();

		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'naziv' => $proizvodjac,
				'sifra_connect' => 0
				));
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}


	public static function getNewGrupaId($group_name){
		$group = DB::table('grupa_pr')->where('grupa',$group_name)->first();

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => $group_name,
				'parrent_grupa_pr_id' => 0,
				'sifra' => $grupa_pr_id
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
		}
		return $grupa_pr_id;
	}

	public static function saveSingleGroup($mapp,$group_code,$group_parent_code=null,$update_parent=false){
		$group_code = trim($group_code);
		$group = DB::table('grupa_pr')->where('sifra_is',$group_code)->whereNotNull('sifra_is')->where('sifra_is','!=','')->first();

		$group_parent_id = self::getGrupaId('KATEGORIJE');
		if(!is_null($group_parent_code) && trim($group_parent_code) != ''){
			$group_parent_id = self::getGrupaId(trim($group_parent_code));
		}

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			if(is_null($grupa_pr_id)){
				$grupa_pr_id = 1;
			}
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => isset($mapp[$group_code]) ? $mapp[$group_code] : $group_code,
				'parrent_grupa_pr_id' => $group_parent_id,
				'sifra' => $grupa_pr_id,
				'sifra_is' => $group_code
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
			if($update_parent){
				DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->update(array('parrent_grupa_pr_id' => $group_parent_id));				
			}
		}
		return $grupa_pr_id;
	}

	public static function filteredGroups($articles){
		$groups = array();
		$mapp = array();
		foreach($articles as $article) {
			if(isset($article->acClassif2) && $article->acClassif2 != ''){
				$groups[mb_convert_encoding($article->acClassif2,mb_detect_encoding($article->acClassif2),"UTF-8")] = mb_convert_encoding($article->acClassif,mb_detect_encoding($article->acClassif),"UTF-8");

				$mapp[mb_convert_encoding($article->acClassif2,mb_detect_encoding($article->acClassif2),"UTF-8")] = Support::convert($article->acClassif2Name);
			}
			if(isset($article->acClassif) && $article->acClassif != ''){
				$mapp[mb_convert_encoding($article->acClassif,mb_detect_encoding($article->acClassif),"UTF-8")] = Support::convert($article->acClassifName);
			}
		}
		return (object) ['tree' => $groups, 'mapp' => $mapp];
	}

	public static function parentGroups($allgroups,$group,&$tree){
		if(!is_null($group) && $group != ''){
			$tree[] = $group;
			if(isset($allgroups[$group]) && $allgroups[$group] != ''){
				self::parentGroups($allgroups,$allgroups[$group],$tree);
			}
		}
	}

	public static function saveGroups($groups){
		foreach($groups->tree as $grupa => $nadgrupa) {
			if($grupa != $nadgrupa){
				$tree = array($grupa);
				self::parentGroups($groups->tree,$nadgrupa,$tree);
				for ($i=(count($tree)-1);$i>=0;$i--) {
					self::saveSingleGroup($groups->mapp,$tree[$i],(isset($tree[$i+1]) ? $tree[$i+1] : null),true);
				}
			}
		}
	}

	public static function getPartnerCategoryId($category_name){
		$category_name = trim($category_name);
		$category = DB::table('partner_kategorija')->where('naziv',$category_name)->first();

		if(is_null($category)){
			$id_kategorije = DB::select("SELECT MAX(id_kategorije) + 1 AS max FROM partner_kategorija")[0]->max;
			if(is_null($id_kategorije)){
				$id_kategorije = 1;
			}
			DB::table('partner_kategorija')->insert(array(
				'id_kategorije' => $id_kategorije,
				'naziv' => $category_name,
				'rabat' => 0,
				'active' => 1
				));
		}else{
			$id_kategorije = $category->id_kategorije;
		}
		return $id_kategorije;
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[$article->id_is] = $article->roba_id;
		}
		return $mapped;
	}

	public static function getMappedPartners(){
		$mapped = array();

		$partners = DB::table('partner')->select('sifra','partner_id')->whereNotNull('sifra')->where('sifra','!=','')->get();
		foreach($partners as $partner){
			$mapped[trim($partner->sifra)] = $partner->partner_id;
		}

		return $mapped;
	}

	public static function getMappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->whereNotNull('sifra_is')->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();
		foreach($groups as $group){
			$mapped[$group->sifra_is] = $group->grupa_pr_id;
		}
		return $mapped;
	}

	public static function convert($string){
		$string = str_replace(array('è','ð','æ','Æ'),array('č','đ','ć','Ć'),$string);
		
        /* Only do the slow convert if there are 8-bit characters */
        if ( !preg_match("/[\200-\237]/", $string) && !preg_match("/[\241-\377]/", $string) )
               return $string;

        // decode three byte unicode characters
          $string = preg_replace_callback("/([\340-\357])([\200-\277])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-224)*4096+(ord($matches[2])-128)*64+(ord($matches[3])-128)).\';\';'),
                    $string);

        // decode two byte unicode characters
          $string = preg_replace_callback("/([\300-\337])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-192)*64+(ord($matches[2])-128)).\';\';'),
                    $string);

        // $string = str_replace(array("\n","&#352;","&#353;","&#268;","&#269;","&#272;","&#273;","&#381;","&#382;","&#262;","&#263;"),array("<br>","Š","š","Č","č","Đ","đ","Ž","ž","Ć","ć"),$string);
		$string = str_replace(array("\n"),array("<br>"),$string);

        return $string;
	}

	public static function entityDecode(array $id_iss=[]){
		if(count($id_iss) > 0){
			DB::statement("update roba set naziv = replace(naziv, '&#353;', 'š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#273;', 'đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#269;', 'č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#263;', 'ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#382;', 'ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#352;', 'Š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#272;', 'Đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#268;', 'Č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#262;', 'Ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#381;', 'Ž') where id_is in (".implode(",",$id_iss).")");

			DB::statement("update roba set naziv_web = replace(naziv_web, '&#353;', 'š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#273;', 'đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#269;', 'č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#263;', 'ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#382;', 'ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#352;', 'Š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#272;', 'Đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#268;', 'Č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#262;', 'Ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#381;', 'Ž') where id_is in (".implode(",",$id_iss).")");

			DB::statement("update roba set web_opis = replace(web_opis, '&#353;', 'š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#273;', 'đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#269;', 'č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#263;', 'ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#382;', 'ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#352;', 'Š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#272;', 'Đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#268;', 'Č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#262;', 'Ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#381;', 'Ž') where id_is in (".implode(",",$id_iss).")");

		}else{
			DB::statement("update roba set naziv = replace(naziv, '&#353;', 'š')");
			DB::statement("update roba set naziv = replace(naziv, '&#273;', 'đ')");
			DB::statement("update roba set naziv = replace(naziv, '&#269;', 'č')");
			DB::statement("update roba set naziv = replace(naziv, '&#263;', 'ć')");
			DB::statement("update roba set naziv = replace(naziv, '&#382;', 'ž')");
			DB::statement("update roba set naziv = replace(naziv, '&#352;', 'Š')");
			DB::statement("update roba set naziv = replace(naziv, '&#272;', 'Đ')");
			DB::statement("update roba set naziv = replace(naziv, '&#268;', 'Č')");
			DB::statement("update roba set naziv = replace(naziv, '&#262;', 'Ć')");
			DB::statement("update roba set naziv = replace(naziv, '&#381;', 'Ž')");

			DB::statement("update roba set naziv_web = replace(naziv_web, '&#353;', 'š')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#273;', 'đ')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#269;', 'č')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#263;', 'ć')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#382;', 'ž')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#352;', 'Š')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#272;', 'Đ')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#268;', 'Č')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#262;', 'Ć')");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#381;', 'Ž')");

			DB::statement("update roba set web_opis = replace(web_opis, '&#353;', 'š')");
			DB::statement("update roba set web_opis = replace(web_opis, '&#273;', 'đ')");
			DB::statement("update roba set web_opis = replace(web_opis, '&#269;', 'č')");
			DB::statement("update roba set web_opis = replace(web_opis, '&#263;', 'ć')");
			DB::statement("update roba set web_opis = replace(web_opis, '&#382;', 'ž')");
			DB::statement("update roba set web_opis = replace(web_opis, '&#352;', 'Š')");
			DB::statement("update roba set web_opis = replace(web_opis, '&#272;', 'Đ')");
			DB::statement("update roba set web_opis = replace(web_opis, '&#268;', 'Č')");
			DB::statement("update roba set web_opis = replace(web_opis, '&#262;', 'Ć')");
			DB::statement("update roba set web_opis = replace(web_opis, '&#381;', 'Ž')");

		}
			
    }
    
	public static function postUpdate(){

        DB::statement("update roba r set flag_prikazi_u_cenovniku = 1 where (select kolicina from lager where roba_id = r.roba_id and orgj_id = 4) > 0 and r.flag_zakljucan='false'");
        DB::statement("update roba r set flag_prikazi_u_cenovniku = 0 where (select kolicina from lager where roba_id = r.roba_id and orgj_id = 4) = 0 or (select kolicina from lager where roba_id = r.roba_id and orgj_id = 4) is null and r.flag_zakljucan='false'");

        DB::statement("update roba r set flag_cenovnik = 1 where (select (kolicina+rezervisano) as ukupno from lager where roba_id = r.roba_id and orgj_id = 4) > 0 and r.flag_zakljucan='false'");
        DB::statement("update roba r set flag_cenovnik = 0 where (select (kolicina+rezervisano) as ukupno from lager where roba_id = r.roba_id and orgj_id = 4) = 0 or (select kolicina from lager where roba_id = r.roba_id and orgj_id = 4) is null and r.flag_zakljucan='false'");

		foreach(DB::table('grupa_pr')->where('grupa','!=','BEZ PANTEON SIFRE')->where('grupa_pr_id','>',0)->where(function($q){ return $q->whereNull('sifra_is')->orWhere('sifra_is',''); })->get() as $grupa){
			$ids=array();
		    Groups::allGroups($ids,$grupa->grupa_pr_id);
		    if(count(DB::table('roba')->where('flag_aktivan',1)->where(array('flag_aktivan'=>1,'flag_cenovnik'=>1))->whereIn('grupa_pr_id',$ids)->get()) > 0){
		    	DB::table('grupa_pr')->whereIn('grupa_pr_id',$ids)->update(array('web_b2b_prikazi'=>1,'web_b2c_prikazi'=>1));
		    }else{
		    	DB::table('grupa_pr')->whereIn('grupa_pr_id',$ids)->update(array('web_b2b_prikazi'=>0,'web_b2c_prikazi'=>0));
		    }
		}

		DB::statement("DELETE FROM log_b2b_partner WHERE (CURRENT_TIMESTAMP - datum) > '30 days'");
	}

	public static function roba_cene($id_is){
		return DB::table('roba')->select('racunska_cena_end','stara_cena')->where('id_is',$id_is)->first();
	}

	public static function lager_magacin($roba_id,$orgj_id){
		return DB::table('lager')->select('kolicina','poc_kolicina')->where(array('roba_id'=>$roba_id,'orgj_id'=>$orgj_id))->first();
	}

	public static function groupedGroups($articles){
		$codes = array();
		$subcodes = array();
		$grouped = array();
		foreach($articles as $article) {
			if(isset($article->idGrupa) && $article->idGrupa != ''){
				if(!in_array($article->idGrupa, $codes)){
					$grouped[] = (object) ['code' => $article->idGrupa, 'name' => $article->grupa, 'parent' => '0'];

					$codes[] = $article->idGrupa;
				}

				if(isset($article->idPodGrupa) && $article->idPodGrupa != ''){
					if(!in_array($article->idPodGrupa, $subcodes)){
						$grouped[] = (object) ['code' => $article->idPodGrupa, 'name' => $article->podgrupa, 'parent' => $article->idGrupa];

						$subcodes[] = $article->idPodGrupa;
					}
				}
			}
		}
		return $grouped;
	}
	public static function updateGroupsParent($groups,$mainParentId){
		foreach($groups as $group){
			$group_id = $group->code;

			if($group->parent != '0'){
				$parrent_grupa_pr = DB::table('grupa_pr')->where('sifra_is',$group->parent)->first();
				if(!is_null($parrent_grupa_pr)){
					$parrent_grupa_pr_id = $parrent_grupa_pr->grupa_pr_id;
					DB::table('grupa_pr')->where(array('sifra_is'=>$group->code))->update(array('parrent_grupa_pr_id'=>$parrent_grupa_pr_id));
				}else{
					// DB::table('grupa_pr')->where('id_is',$group->parent)->delete();
				}
			}
		}

		DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0))->whereNotNull('sifra_is')->update(array('parrent_grupa_pr_id'=>$mainParentId));
	}


}