<?php
namespace IsGSM;

use DB;


class Support {


	public static function getPartnerId($sifra_kupca_logik){
		$partner = DB::table('partner')->where('id_is',$sifra_kupca_logik)->first();
		if(!is_null($partner)){
			return $partner->partner_id;
		}
		return null;
	}

	public static function getTarifnaGrupaId($naziv_tarifne_grupe,$vrednost_tarifne_grupe=20){
		$tg = DB::table('tarifna_grupa')->where(array('porez'=>$vrednost_tarifne_grupe))->first();

		if(is_null($tg)){
			$next_id = DB::table('tarifna_grupa')->max('tarifna_grupa_id')+1;
			DB::table('tarifna_grupa')->insert(array(
				'tarifna_grupa_id' => $next_id,
				'sifra' => $next_id,
				'naziv' => substr($naziv_tarifne_grupe,0,99),
				'porez' => $vrednost_tarifne_grupe,
				'active' => 1,
				'default_tarifna_grupa' => 0,
				'tip' => substr($vrednost_tarifne_grupe,0,19)
				));
			$tg = DB::table('tarifna_grupa')->where('porez',$vrednost_tarifne_grupe)->first();
		}
		return $tg->tarifna_grupa_id;
	}

	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();

		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'naziv' => $proizvodjac,
				'sifra_connect' => 0
				));
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}

	public static function getGrupaId($group_name){
		$group_name = trim($group_name);
		$group = DB::table('grupa_pr')->where('grupa',$group_name)->first();

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => $group_name,
				'parrent_grupa_pr_id' => 0,
				'sifra' => $grupa_pr_id
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
		}
		return $grupa_pr_id;
	}

	public static function getPartnerCategoryId($category_name){
		$category_name = trim($category_name);
		$category = DB::table('partner_kategorija')->where('naziv',$category_name)->first();

		if(is_null($category)){
			$id_kategorije = DB::select("SELECT MAX(id_kategorije) + 1 AS max FROM partner_kategorija")[0]->max;
			if(is_null($id_kategorije)){
				$id_kategorije = 1;
			}
			DB::table('partner_kategorija')->insert(array(
				'id_kategorije' => $id_kategorije,
				'naziv' => $category_name,
				'rabat' => 0,
				'active' => 1
				));
		}else{
			$id_kategorije = $category->id_kategorije;
		}
		return $id_kategorije;
	}

	public static function getSellers($partners){
		$sellers = array();
		foreach($partners as $partner){
			$sellers[] = $partner->komercijalista;
		}
		return array_unique($sellers);
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[trim(strval($article->id_is))] = $article->roba_id;
		}
		return $mapped;
	}
	public static function linkedWidthDC(){
		DB::table("UPDATE dobavljac_cenovnik dc SET roba_id = r.roba_id, povezan = 1 FROM roba r WHERE dc.sifra_kod_dobavljaca = r.sifra_d AND povezan <> 1 AND dc.roba_id = -1 AND r.sifra_d IS NOT NULL AND r.sifra_d <> ''");
		DB::table("UPDATE roba r SET dobavljac_id = dc.partner_id FROM dobavljac_cenovnik dc WHERE dc.roba_id = r.roba_id");

		DB::table("UPDATE dobavljac_cenovnik dc SET roba_id = -1, povezan = 0 WHERE roba_id <> -1 AND povezan = 1 AND roba_id NOT IN (SELECT roba_id FROM roba)");
	}

	public static function getMappedPartners(){
		$mapped = array();
		$partners = DB::table('partner')->select('id_is','partner_id')->whereNotNull('id_is')->get();
		foreach($partners as $partner){
			$mapped[$partner->id_is] = $partner->partner_id;
		}
		return $mapped;
	}
	public static function getMappedBills(){
		$mapped = array();
		$bills = DB::table('racun')->select('broj_dokumenta','racun_id')->whereNotNull('broj_dokumenta')->get();
		foreach($bills as $bill){
			$mapped[strval($bill->broj_dokumenta)] = $bill->racun_id;
		}
		return $mapped;
	}
	public static function getMappedSellers(){
		$mapped = array();
		$sellers = DB::table('imenik')->select('imenik_id','ime','prezime')->whereNotNull('ime')->whereNotNull('prezime')->where('kvota',32.00)->get();
		foreach($sellers as $seller){
			$mapped[strval(($seller->ime.' '.$seller->prezime))] = $seller->imenik_id;
		}
		return $mapped;
	}

	public static function convert($text){
	    $text = preg_replace("/[áàâãªä]/u","a",$text);
	    $text = preg_replace("/[ÁÀÂÃÄ]/u","A",$text);
	    $text = preg_replace("/[ÍÌÎÏ]/u","I",$text);
	    $text = preg_replace("/[íìîï]/u","i",$text);
	    $text = preg_replace("/[éèêë]/u","e",$text);
	    $text = preg_replace("/[ÉÈÊË]/u","E",$text);
	    $text = preg_replace("/[óòôõºö]/u","o",$text);
	    $text = preg_replace("/[ÓÒÔÕÖ]/u","O",$text);
	    $text = preg_replace("/[úùûü]/u","u",$text);
	    $text = preg_replace("/[ÚÙÛÜ]/u","U",$text);
	    $text = preg_replace("/[’‘‹›‚]/u","'",$text);
	    $text = preg_replace("/[“”«»„]/u",'"',$text);
	    $text = str_replace("–","-",$text);
	    $text = str_replace(" "," ",$text);
	    $text = str_replace("ç","c",$text);
	    $text = str_replace("Ç","C",$text);
	    $text = str_replace("ñ","n",$text);
	    $text = str_replace("Ñ","N",$text);
	 
	    //2) Translation CP1252. &ndash; => -
	    $trans = get_html_translation_table(HTML_ENTITIES); 
	    $trans[chr(130)] = '&sbquo;';    // Single Low-9 Quotation Mark 
	    $trans[chr(131)] = '&fnof;';    // Latin Small Letter F With Hook 
	    $trans[chr(132)] = '&bdquo;';    // Double Low-9 Quotation Mark 
	    $trans[chr(133)] = '&hellip;';    // Horizontal Ellipsis 
	    $trans[chr(134)] = '&dagger;';    // Dagger 
	    $trans[chr(135)] = '&Dagger;';    // Double Dagger 
	    $trans[chr(136)] = '&circ;';    // Modifier Letter Circumflex Accent 
	    $trans[chr(137)] = '&permil;';    // Per Mille Sign 
	    $trans[chr(138)] = '&Scaron;';    // Latin Capital Letter S With Caron 
	    $trans[chr(139)] = '&lsaquo;';    // Single Left-Pointing Angle Quotation Mark 
	    $trans[chr(140)] = '&OElig;';    // Latin Capital Ligature OE 
	    $trans[chr(145)] = '&lsquo;';    // Left Single Quotation Mark 
	    $trans[chr(146)] = '&rsquo;';    // Right Single Quotation Mark 
	    $trans[chr(147)] = '&ldquo;';    // Left Double Quotation Mark 
	    $trans[chr(148)] = '&rdquo;';    // Right Double Quotation Mark 
	    $trans[chr(149)] = '&bull;';    // Bullet 
	    $trans[chr(150)] = '&ndash;';    // En Dash 
	    $trans[chr(151)] = '&mdash;';    // Em Dash 
	    $trans[chr(152)] = '&tilde;';    // Small Tilde 
	    $trans[chr(153)] = '&trade;';    // Trade Mark Sign 
	    $trans[chr(154)] = '&scaron;';    // Latin Small Letter S With Caron 
	    $trans[chr(155)] = '&rsaquo;';    // Single Right-Pointing Angle Quotation Mark 
	    $trans[chr(156)] = '&oelig;';    // Latin Small Ligature OE 
	    $trans[chr(159)] = '&Yuml;';    // Latin Capital Letter Y With Diaeresis 
	    $trans['euro'] = '&euro;';    // euro currency symbol 
	    ksort($trans); 
	     
	    foreach ($trans as $k => $v) {
	        $text = str_replace($v, $k, $text);
	    }
	 
	    // 3) remove <p>, <br/> ...
	    $text = strip_tags($text); 
	     
	    // 4) &amp; => & &quot; => '
	    $text = html_entity_decode($text);
	     
	    // 5) remove Windows-1252 symbols like "TradeMark", "Euro"...
	    $text = preg_replace('/[^(\x20-\x7F)]*/','', $text); 
	     
	    $targets=array("\r\n","\n","\r","\t","''");
	    $results=array(" "," "," ","","'");
	    $text = str_replace($targets,$results,$text);
	     
		return ($text);
	}

	public static function filteredGroups($articles){
		$groups = array();
		foreach($articles as $article) {
			if(isset($article->PODGRUPA) && $article->PODGRUPA != ''){
				$groups[$article->PODGRUPA] = $article->GRUPA;
			}
		}
		return $groups;
	}
	public static function saveGroups($groups){
		foreach($groups as $grupa => $nadgrupa) {
			if($grupa != $nadgrupa){
				$tree = array($grupa);
				self::parentGroups($groups,$nadgrupa,$tree);
				for ($i=(count($tree)-1);$i>=0;$i--) {
					self::saveSingleGroup($tree[$i],(isset($tree[$i+1]) ? $tree[$i+1] : null),true);
				}
			}
		}
	}
	public static function parentGroups($allgroups,$group,&$tree){
		if(!is_null($group) && $group != ''){
			$tree[] = $group;
			if(isset($allgroups[$group]) && $allgroups[$group] != ''){
				self::parentGroups($allgroups,$allgroups[$group],$tree);
			}
		}
	}
	public static function saveSingleGroup($group_name,$group_parent=null,$update_parent=false){
		$group_name = trim($group_name);
		$group = DB::table('grupa_pr')->where('grupa',$group_name)->first();

		$group_parent_id = 0;
		if(!is_null($group_parent) && $group_parent != ''){
			$group_parent_id = self::getGrupaId($group_parent);
		}

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			if(is_null($grupa_pr_id)){
				$grupa_pr_id = 1;
			}
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => $group_name,
				'parrent_grupa_pr_id' => $group_parent_id,
				'sifra' => $grupa_pr_id
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
			if($update_parent){
				DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->update(array('parrent_grupa_pr_id' => $group_parent_id));				
			}
		}
		return $grupa_pr_id;
	}
		

}