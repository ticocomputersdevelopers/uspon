<?php
namespace IsGSM;

use DB;

class PartnerBill {

    public static function table_body($partners_cards,$mappedPartners){
        $result_arr = array();
        $racun_id = DB::select("SELECT nextval('racun_racun_id_seq1')")[0]->nextval;

        foreach($partners_cards as $item) {
            $partner_id = isset($mappedPartners[strval($item->id)]) ? $mappedPartners[strval($item->id)] : null;
            $broj_dokumenta = isset($item->opis) ? pg_escape_string(substr($item->opis,0,300)) : null;

            if(!is_null($partner_id) && !is_null($broj_dokumenta) && substr($broj_dokumenta,0,1) == 'R'){
                $racun_id++;
                $datum_racuna = isset($item->datum) ? $item->datum : '1970-00-00T00:00:00+02:00';
                $duguje = isset($item->duguje) ? $item->duguje : 0.00;

                // $id_is = $partner_id.'-'.$racun_id;

                $result_arr[] = "(".$racun_id.",'".$broj_dokumenta."',('".$datum_racuna."')::date,".strval($partner_id).",100,".$duguje.")";

            }
        }

        return (object) array("body"=>implode(",",$result_arr));
    }

    public static function query_insert_update($table_temp_body,$upd_cols=array()) {
        if($table_temp_body == ''){
            return false;
        }
        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='racun'"));
        $table_temp = "(VALUES ".$table_temp_body.") racun_temp(".implode(',',$columns).")";

        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="racun_id"){
                $updated_columns[] = "".$col." = racun_temp.".$col."";
            }
        }

        //update
        // DB::statement("UPDATE racun SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE (racun_temp.partner_id,racun_temp.vrsta_dokumenta,racun_temp.datum_dokumenta,racun_temp.opis,racun_temp.duguje,racun_temp.potrazuje) IN (SELECT partner_id,vrsta_dokumenta,datum_dokumenta,opis,duguje,potrazuje FROM racun)");

        //insert
        DB::statement("INSERT INTO racun (SELECT * FROM ".$table_temp." WHERE (partner_id,broj_dokumenta,datum_racuna,iznos) NOT IN (SELECT partner_id,broj_dokumenta,datum_racuna,iznos FROM racun))");

        DB::statement("SELECT setval('racun_racun_id_seq1', (SELECT MAX(racun_id) FROM racun), FALSE)");
    }

    public static function query_delete_unexists($table_temp_body) {
        //
    }
}
