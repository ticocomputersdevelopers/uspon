<?php
namespace IsLogik;
 
use DB;

class PartnerCard {

    public static function table_body($partners_cards,$mappedPartners){
        $result_arr = array();
        // $web_b2b_kartica_id = DB::select("SELECT nextval('web_b2b_kartica_web_b2b_kartica_id_seq')")[0]->nextval;
        $web_b2b_kartica_id = 0;

        foreach($partners_cards as $item) {
            $partner_id = isset($mappedPartners[strval($item->partnerid)]) ? $mappedPartners[strval($item->partnerid)] : null;

            if(!is_null($partner_id)){
                $web_b2b_kartica_id++;
                $datum_dokumenta = isset($item->documentdate) ? $item->documentdate : '1970-00-00T00:00:00+02:00';
                $datum_dospeca = isset($item->valutedate) ? $item->valutedate : '1970-00-00T00:00:00+02:00';
                $vrsta_dokumenta = pg_escape_string(substr(Support::convert($item->identtype),0,300));
                $opis = isset($item->description) ? $item->description : pg_escape_string(substr(Support::convert($item->description),0,300));
                $duguje = isset($item->debit) ? $item->debit : 0.00;
                $potrazuje = isset($item->credit) ? $item->credit : 0.00;
                $saldo = floatval($duguje)-floatval($potrazuje);
                $foreignid = $item->identid;
                $id_is = $partner_id.'-'.$foreignid;

                $result_arr[] = "(".$web_b2b_kartica_id.",".strval($partner_id).",(NULL)::integer,'".$vrsta_dokumenta."',('".$datum_dokumenta."')::date,('".$datum_dospeca."')::date,'".$opis."',".strval($duguje).",".strval($potrazuje).",".strval($saldo).",".strval($foreignid).",'".strval($id_is)."')";

            }
        }

        return (object) array("body"=>implode(",",$result_arr));
    }

    public static function query_insert_update($table_temp_body,$upd_cols=array()) {
        if($table_temp_body == ''){
            return false;
        }
        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_b2b_kartica'"));
        $table_temp = "(VALUES ".$table_temp_body.") web_b2b_kartica_temp(".implode(',',$columns).")";

        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="web_b2b_kartica_id" && $col!="id_is"){
                $updated_columns[] = "".$col." = web_b2b_kartica_temp.".$col."";
            }
        }

        //update
        // DB::statement("UPDATE web_b2b_kartica SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE (web_b2b_kartica_temp.partner_id,web_b2b_kartica_temp.vrsta_dokumenta,web_b2b_kartica_temp.datum_dokumenta,web_b2b_kartica_temp.opis,web_b2b_kartica_temp.duguje,web_b2b_kartica_temp.potrazuje) IN (SELECT partner_id,vrsta_dokumenta,datum_dokumenta,opis,duguje,potrazuje FROM web_b2b_kartica)");

        // //delete
        // DB::statement("DELETE FROM web_b2b_kartica WHERE (partner_id,vrsta_dokumenta,datum_dokumenta,datum_dospeca,opis,duguje,potrazuje,foreigned) NOT IN (SELECT partner_id,vrsta_dokumenta,datum_dokumenta,datum_dospeca,opis,duguje,potrazuje,foreigned FROM ".$table_temp.")");
        DB::statement("DELETE FROM web_b2b_kartica");
        DB::statement("SELECT setval('web_b2b_kartica_web_b2b_kartica_id_seq', 1, FALSE)");

        // //insert
        // DB::statement("INSERT INTO web_b2b_kartica (SELECT * FROM ".$table_temp." WHERE (partner_id,vrsta_dokumenta,datum_dokumenta,datum_dospeca,opis,duguje,potrazuje,foreigned) NOT IN (SELECT partner_id,vrsta_dokumenta,datum_dokumenta,datum_dospeca,opis,duguje,potrazuje,foreigned FROM web_b2b_kartica))");
        DB::statement("INSERT INTO web_b2b_kartica (SELECT * FROM ".$table_temp.")");

        DB::statement("SELECT setval('web_b2b_kartica_web_b2b_kartica_id_seq', (SELECT MAX(web_b2b_kartica_id) FROM web_b2b_kartica), FALSE)");
    }

    public static function query_delete_unexists($table_temp_body) {
        //
    }
}
