<?php
namespace IsLogik;
 
use mysqli;
use DB;

class DBData {

	public static function groups(){
		return DB::connection('logik')->select("SELECT * FROM categorypr ORDER BY categoryprid ASC");
	}
	public static function article_groups($article_id){
		return DB::connection('logik')->select("SELECT * FROM artikli_kategorije WHERE sifra_artikla_logik = ".strval($article_id)." AND semaphore = 2");
	}
	public static function articles_groups(){
		return DB::connection('logik')->select("SELECT * FROM productcategorypr");
	}
 
	public static function articles(){
		return DB::connection('logik')->select("SELECT * FROM product");
	}
	public static function articlesKol(){
		return DB::connection('logik')->select("SELECT * FROM product p WHERE (SELECT sum(amount-(reservedamount+reversedamount)) FROM productwarehouse pw WHERE ( warehouseid=1 OR warehouseid=2 OR warehouseid=3 OR warehouseid=4 OR warehouseid=5 OR warehouseid=7 OR warehouseid=19 OR warehouseid=23 OR warehouseid=15 OR warehouseid=27 OR warehouseid=26 OR warehouseid=24) AND pw.productid=p.productid) > 0 AND (b2bprice>0 OR b2b = 'n') AND active ='y'");
	}
	public static function articlesAll(){
		return DB::connection('logik')->select("SELECT DISTINCT * FROM artikli");
	}

	public static function stockrooms(){
		return DB::connection('logik')->select("SELECT * FROM warehouse");
	}

	public static function stock(){
		return DB::connection('logik')->select("SELECT productid,warehouseid,amount,reservedamount,reversedamount FROM productwarehouse WHERE ( warehouseid=1 OR warehouseid=2 OR warehouseid=3 OR warehouseid=4 OR warehouseid=5 OR warehouseid=7 OR warehouseid=19 OR warehouseid=33 OR warehouseid=23 OR warehouseid=15  OR warehouseid=27 OR warehouseid=26 OR warehouseid=24) AND productid IN ( SELECT productid FROM product WHERE active ='y')");
	}
	public static function stock_short(){
		return DB::connection('logik')->select("SELECT productid,warehouseid,amount,reservedamount,reversedamount FROM productwarehouse WHERE ( warehouseid=1 OR warehouseid=2 OR warehouseid=3 OR warehouseid=4 OR warehouseid=5 OR warehouseid=7 OR warehouseid=19 OR warehouseid=33 OR warehouseid=23 OR warehouseid=15  OR warehouseid=27 OR warehouseid=26 OR warehouseid=24) AND (amount-(reservedamount+reversedamount))>0 AND productid IN ( SELECT productid FROM product WHERE active ='y')");
	}
	public static function partners(){
		return DB::connection('logik')->select("SELECT * FROM partner where active = 'y' and partnerid is not null");
	}
	public static function partner_adress(){
		return DB::connection('logik')->select("SELECT * FROM partneraddress");
	}
	public static function partners_groups_rabat(){
		return DB::connection('logik')->select("SELECT DISTINCT * FROM partnercategorypr");
	}
	public static function partners_cards(){
		return DB::connection('logik')->select("SELECT * FROM kartica_kupca WHERE semaphore = 2");
	}
	public static function partners_cards_all(){
		return DB::connection('logik')->select("SELECT * FROM partnercard");
	}



	public static function update_groups(){
		DB::connection('logik')->statement("UPDATE kategorije SET semaphore = 0 WHERE semaphore = 2");
	}
	public static function update_articles(array $ids_is){
		if(count($ids_is) > 0){
			$articles = DB::table('roba')->select('id_is','roba_id')->whereNotNull('id_is')->whereIn('id_is',$ids_is)->get();

			foreach($articles as $article){
				DB::connection('logik')->statement("UPDATE artikli SET sifra_artikla_tico = '".strval($article->roba_id)."', semaphore = 0 WHERE sifra_artikla_logik = ".$article->id_is."");
			}
		}
	}

	public static function not_updated_stock(){
		return DB::connection('logik')->select("SELECT * FROM proizvodiumagacinu WHERE ticosifraproizvoda IS NULL OR ticosiframagacina IS NULL");
	}
	public static function update_stock(array $mapped_stocks,array $mapped_articles){
		$mapped_stocks_change = array();
		$all_stocks = array();
		foreach($mapped_stocks as $mapped_stock_key => $mapped_stock_val){
			foreach($mapped_stock_val as $msv){
				$mapped_stocks_change[$msv] = $mapped_stock_key;
				$all_stocks[] = $msv;
			}
		}

		$not_updated_stock = DB::connection('logik')->select("SELECT * FROM proizvodiumagacinu WHERE logiksiframagacina IN (".implode(',',$all_stocks).") AND (ticosifraproizvoda IS NULL OR ticosiframagacina IS NULL OR  ticosifraproizvoda = '' OR ticosiframagacina = '')");
		foreach($not_updated_stock as $nustock){
			$ticosifraproizvoda = isset($mapped_articles[$nustock->logiksifraproizvoda]) ? $mapped_articles[$nustock->logiksifraproizvoda] : null;
			$ticosiframagacina = isset($mapped_stocks_change[$nustock->logiksiframagacina]) ? $mapped_stocks_change[$nustock->logiksiframagacina] : null;

			DB::connection('logik')->statement("UPDATE proizvodiumagacinu SET ticosifraproizvoda = '".strval($ticosifraproizvoda)."', ticosiframagacina = '".strval($ticosiframagacina)."' WHERE logiksifraproizvoda = ".$nustock->logiksifraproizvoda." AND logiksiframagacina = ".$nustock->logiksiframagacina." AND (ticosifraproizvoda IS NULL OR ticosiframagacina IS NULL OR ticosifraproizvoda = '' OR ticosiframagacina = '')");
		}
	}
	public static function update_articles_groups(array $mapped_articles){
		if(count($mapped_articles) > 0){
			$articles_groups = DB::connection('logik')->select("SELECT sifra_artikla_logik FROM artikli_kategorije WHERE semaphore = 2");
			foreach($articles_groups as $article_group) {
				if(isset($mapped_articles[$article_group->sifra_artikla_logik])){
					DB::connection('logik')->statement("UPDATE artikli_kategorije SET sifra_artikla_tico = '".strval($mapped_articles[$article_group->sifra_artikla_logik])."', semaphore = 0 WHERE sifra_artikla_logik = ".$article_group->sifra_artikla_logik." AND semaphore = 2");
				}
			}
		}

	}
	public static function update_partners(array $ids_is){
		if(count($ids_is) > 0){
			$partners = DB::table('partner')->select('id_is','partner_id')->whereNotNull('id_is')->whereIn('id_is',$ids_is)->get();
			foreach($partners as $partner){
				DB::connection('logik')->statement("UPDATE kupci SET sifra_kupca_tico = '".strval($partner->partner_id)."', semaphore = 0 WHERE sifra_kupca_logik = ".$partner->id_is." AND vrsta_kupca = 2 AND semaphore = 2");
			}
		}
	}
	public static function update_partners_groups(array $mapped_result){

		if(count($mapped_result) > 0){
			foreach($mapped_result as $row){
				DB::connection('logik')->statement("UPDATE kupci_kategorije SET sifra_kupca_tico = '".$row->partner_id."', sifra_kategorije_tico = '".$row->grupa_pr_id."', semaphore = 0 WHERE sifra_kupca_logik = ".strval($row->sifra_kupca_logik)." AND sifra_kategorije_logik = ".strval($row->sifra_kategorije_logik)." AND semaphore = 2");
			}
		}
	}

	public static function update_partners_cards(){
        $partner_id_is = array_map('current',DB::connection('logik')->select("SELECT DISTINCT sifra_kupca_logik FROM kartica_kupca WHERE semaphore = 2"));
        $mapped_result = Support::getMappedPartners($partner_id_is);

		if(count($mapped_result) > 0){
			foreach($mapped_result as $sifra_kupca_logik => $partner_id){
				DB::connection('logik')->statement("UPDATE kartica_kupca SET sifra_kupca_tico = '".$partner_id."', semaphore = 0 WHERE sifra_kupca_logik = ".strval(intval($sifra_kupca_logik))." AND (semaphore = 2 OR sifra_kupca_tico IS NULL)");
			}
		}
	}

}