<?php
namespace IsLogik;

use DB;
use All;

class Komercijalisti {

	public static function table_body($komercijalisti){

		$result_arr = array(); 

		$komercijalista_id = DB::select("SELECT nextval('imenik_magacin_imenik_magacin_id_seq')")[0]->nextval;
		foreach($komercijalisti as $komercijalista) {
			if(!empty($komercijalista->userid)){
			
				
		    	$komercijalista_id++;
	            $id_is=$komercijalista->userid;
	            $naziv = explode(' ', $komercijalista->responsableperson);
	            $ime =$naziv[1];
	            $prezime = !empty($naziv[0]) ? $naziv[0] : NULL;
	            $mail=isset($komercijalista->responsablepersonmail) ? substr($komercijalista->responsablepersonmail,0,100) : null;
	            $mobilni= null;

		        $result_arr[] = "(".strval($komercijalista_id).",NULL,'".trim($ime)."',NULL,'".trim(str_replace("æ", "ć", $prezime))."',NULL,NULL,NULL,NULL,NULL,NULL,'".trim($mobilni)."',(NULL)::date,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,1,0,0,0,0,NULL,-1,NULL,'".$mail."',-1,(NULL)::date,(NULL)::bytea,NULL,NULL,'',0,-1,32,0,0,0,0,0,0,1,1,1,1,0,(NULL)::integer,(NULL)::date,0,'".$id_is."')";
		    }
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='imenik'"));
        $table_temp = "(VALUES ".$table_temp_body.") imenik_temp(".implode(',',$columns).")";

       // DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="imenik_id" ){
                $updated_columns[] = "".$col." = imenik_temp.".$col."";
            }
        }


        //insert
        DB::statement("INSERT INTO imenik (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM imenik t WHERE t.id_is=imenik_temp.id_is))");
        //update
        //DB::statement("UPDATE imenik i SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE i.id_is=imenik_temp.id_is::varchar");
        DB::statement("DELETE FROM imenik WHERE imenik_id NOT IN ( SELECT MIN(imenik_id) FROM imenik GROUP BY id_is) and imenik_id >2 AND id_is is not null ");

		DB::statement("SELECT setval('imenik_magacin_imenik_magacin_id_seq', (SELECT MAX(imenik_id) FROM imenik) + 1, FALSE)");
	}

	public static function query_delete_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		DB::statement("DELETE FROM partner WHERE id_is NOT IN (SELECT id_is FROM ".$table_temp.")");
		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}