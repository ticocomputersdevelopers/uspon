<?php
namespace IsLogik;
use DB;
 
class ArticleGroup {

	public static function table_body($articles_groups,$mapped_articles,$mapped_groups){

		$result_arr = array();

		foreach($articles_groups as $article_group) {

			if(isset($mapped_groups[strval($article_group->categoryprid)]) && isset($mapped_articles[strval($article_group->productid)])){

				$result_arr[] = "(".strval($mapped_articles[strval($article_group->productid)]).",".strval($mapped_groups[strval($article_group->categoryprid)]).")";
			}

		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_delete($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$table_temp = "(VALUES ".$table_temp_body.") roba_grupe_temp(roba_id, grupa_pr_id)";


		DB::statement("update roba r set grupa_pr_id = t.grupa_pr_id
		FROM (SELECT roba_id, grupa_pr_id,
			     ROW_NUMBER() OVER (partition BY roba_id ORDER BY grupa_pr_id asc) AS rnum
		     FROM ".$table_temp.") t
		where t.rnum = 1 and r.roba_id = t.roba_id");

		// DB::statement("INSERT INTO roba_grupe (SELECT roba_id, grupa_pr_id FROM 
		// 	(SELECT roba_id, grupa_pr_id,
		// 	     ROW_NUMBER() OVER (partition BY roba_id ORDER BY grupa_pr_id asc) AS rnum
		//      FROM ".$table_temp.") t
		// where t.rnum > 1 and (t.roba_id, t.grupa_pr_id) not in (select roba_id, grupa_pr_id from roba_grupe))");

		// DB::statement("DELETE FROM roba_grupe WHERE (roba_id, grupa_pr_id) NOT IN (SELECT roba_id, grupa_pr_id FROM 
		// 	(SELECT roba_id, grupa_pr_id,
		// 	     ROW_NUMBER() OVER (partition BY roba_id ORDER BY grupa_pr_id asc) AS rnum
		//      FROM ".$table_temp.") t
		// where t.rnum > 1)");
	}

}