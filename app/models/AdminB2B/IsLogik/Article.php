<?php
namespace IsLogik;
use DB;
use All;
 
class Article {

	public static function table_body($articles){
		$articleManufacturers = array_unique(array_map(function($article){
					return $article->manufname;
				},$articles));
		foreach($articleManufacturers as $articleManufacturer){
			if(!empty($articleManufacturer)){
				Support::getProizvodjacId($articleManufacturer);
			}
		}
		$mappedManufacturers = Support::getMappedManufacturers();

		$articleVats = array_unique(array_map(function($article){
					return $article->taxvalue;
				},$articles));
		foreach($articleVats as $articleVat){
			if(!empty($articleVat)){
				Support::getTarifnaGrupaId($articleVat);
			}
		}
		$mappedVats = Support::getMappedVats();

		$articleUnits = array_unique(array_map(function($article){
			return $article->unitname;
		},$articles));
		foreach($articleUnits as $articleUnit){
			Support::getJedinicaMereId($articleUnit);
		}
		$mappedUnits = Support::getMappedUnits();


		$result_arr = array();
		$ids_is = array();

		$roba_id =  DB::table('roba')->max('roba_id')+1;
		$sifra_k = DB::table('roba')->max('sifra_k')+1;
		foreach($articles as $article) {
			if(isset($article->code) && !empty($article->code)) {

			$id_is = $article->productid;
			$ids_is[] = $id_is;

			$roba_id++;
			$sifra_k++;
			$sifra_is = !empty($article->code) ? "'".$article->code."'" : "NULL";
			$sifra_d = "NULL";

			$naziv = pg_escape_string(Support::convert(substr($article->name,0,300)));
			$web_opis = null;
			$web_karakteristike = null;
			// $jedinica_mere_id = Support::getJedinicaMereId($article->unitname);
			// $proizvodjac_id = !is_null($article->manufname) && trim($article->manufname) != '' ? Support::getProizvodjacId(Support::convert($article->manufname)) : -1;
			// $tarifna_grupa_id = Support::getTarifnaGrupaId($article->taxvalue,$article->taxvalue);
			$tarifna_grupa_id = isset($mappedVats[strval($article->taxvalue)]) ? $mappedVats[strval($article->taxvalue)] : 0;
			$jedinica_mere_id = isset($mappedUnits[strval($article->unitname)]) ? $mappedUnits[strval($article->unitname)] : -1;
			$proizvodjac_id = !empty(Support::getProizvodjacId($article->manufname)) ? Support::getProizvodjacId($article->manufname) : -1;
			
			$flag_aktivan = $article->active == 'y' ? '1' : '0';
			$grupa_pr_id = 3527;
			$stara_grupa_id = 'NULL';
			$barkod = $article->barcode;
			$tezinski_faktor = !empty($article->mass) ? floatval($article->mass)*1000 : 0.00;
			$model = pg_escape_string(Support::convert($article->model));
			$b2b_max_rabat = isset($article->maxrebate) && !is_null($article->maxrebate) ? $article->maxrebate : 0;
			$b2b_akcijski_rabat = isset($article->maxretailrebate) && !is_null($article->maxretailrebate) ? $article->maxretailrebate : 0;
			$akcija_flag_primeni = $b2b_akcijski_rabat > 0 ? '1' : '0';
			$racunska_cena_nc =0; 
			$racunska_cena_end = $article->b2bprice;
			$mpcena = $article->recommendedprice*1.2;
			$web_cena = $article->webprice*1.2;
			// $garancija = $article->warranty > 0 ? $article->warranty : 0;
			// $garancija =str_replace("m", "", $garancija);
			// $garancija =explode(" ",$garancija)[0];
			// $garancija = !empty($garancija) ? $garancija : 0;
			$garancija = 0;
			//$web_cena = $mpcena;

			if($article->active == 'y' && $article->web == 'y'){
				$flag_prikazi_u_cenovniku = 1;	
			}else{
				$flag_prikazi_u_cenovniku = 0;
			}
			// $flag_cenovnik = $article->b2b == 'y' ? '1' : '0';
			
			if($article->active == 'y' && $article->b2b == 'y'){
				$flag_cenovnik = 1;
			}else{
				$flag_cenovnik = 0;
			}

			$result_arr[] = "(".strval($roba_id).",".$sifra_d.",'".$naziv."',NULL,NULL,NULL,".$grupa_pr_id.",".strval($tarifna_grupa_id).",".strval($jedinica_mere_id).",".strval($proizvodjac_id).",-1,".strval($sifra_k).",NULL,NULL,'".$naziv."',0,-1,0,0,0,".strval($tezinski_faktor).",9,0,0,0,".$garancija.",1,".$flag_prikazi_u_cenovniku.",0,NULL,".$flag_aktivan.",".strval($racunska_cena_nc).",".strval($racunska_cena_end).",".strval($racunska_cena_end).",0,NULL,".strval($mpcena).",false,0,(NULL)::integer,'".$naziv."',".$flag_cenovnik.",".$stara_grupa_id.",NULL,(NULL)::integer,(NULL)::integer,0,0,0,-1,1,".strval($web_cena).",1,0,'".$web_opis."','".$web_karakteristike."',NULL,0,".$akcija_flag_primeni.",0,NULL,NULL,NULL,NULL,1,0,'".$barkod."',0,0,1,1,-1,'".$model."',NULL,NULL,NULL,0,".$b2b_max_rabat.",".$b2b_akcijski_rabat.",0.00,0,".$sifra_is.",(NULL)::date,(NULL)::date,(NULL)::integer,NULL,".$sifra_is.",'".strval($id_is)."',0,(NULL)::integer,0,(NULL)::date,(NULL)::date,0.00,0.00,0,NULL,NULL,NULL,NULL,NULL,0,0,0,0,0,0,-1)";
			}

		}
		return (object) array("body"=>implode(",",$result_arr),"ids_is"=>$ids_is);
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}

		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		//DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		//DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		
		// update
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="roba_id" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = roba_temp.".$col."";
			}
		}

		DB::statement("UPDATE roba t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar AND t.id_is IS NOT NULL AND t.id_is <> '' ");	

		$id_iss = DB::select("SELECT id_is FROM ".$table_temp." WHERE id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");
 
		//insert
		DB::statement("INSERT INTO roba(".implode(',',$columns_without_id).") SELECT ".implode(',',$columns_without_id)." FROM ".$table_temp." WHERE roba_temp.id_is NOT IN (SELECT id_is FROM roba WHERE id_is IS NOT NULL AND id_is <> '')");


		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		DB::statement("SELECT setval('roba_roba_id_seq', (SELECT MAX(roba_id) FROM roba) + 1, FALSE)");

		//$id_iss = array_map(function($row){ return "'".$row->id_is."'"; },$id_iss);
		DB::statement("UPDATE roba set flag_cenovnik=0 WHERE sifra_is is null and roba.flag_zakljucan='false'");

		//Support::entityDecode($id_iss);

		return array_map('current',$id_iss);
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='roba'"));
		$table_temp = "(VALUES ".$table_temp_body.") roba_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.id_is IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar)");
		DB::statement("UPDATE roba t SET flag_cenovnik = 0 WHERE t.id_is::varchar NOT IN (SELECT id_is::varchar FROM ".$table_temp.") and t.flag_zakljucan='false' and t.id_is is not null and t.id_is <> '' ");
		// DB::statement("DELETE FROM roba WHERE flag_aktivan = 0 ");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}