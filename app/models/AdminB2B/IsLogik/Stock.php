<?php
namespace IsLogik;
use DB; 

use All;

class Stock {

	public static function table_body($articles,$mapped_articles,$mapped_stockrooms){

		$result_arr = array();
		foreach($articles as $article) {
			$sifra = $article->productid;
			$stockCode = $article->warehouseid;
			$roba_id = isset($mapped_articles[strval($sifra)]) ? $mapped_articles[strval($sifra)] : null;
			$orgj_id = isset($mapped_stockrooms[strval($stockCode)]) ? $mapped_stockrooms[strval($stockCode)] : null;

			if(!is_null($roba_id) && !is_null($orgj_id)){
				$kolicina = 0;
				if(!empty($article->amount) && is_numeric($article->amount) && $article->amount > 0){
					$reserve =abs($article->reservedamount+$article->reversedamount);
					$kolicina = $article->amount-$reserve;
				}
				if($kolicina < 0){
					$kolicina=0;
				}

				$result_arr[] = "(".strval($roba_id).",".strval($orgj_id).",0,0,0,".strval($kolicina).",0,0,0,0,0,0,0,0,(NULL)::integer,0,0,0,0,0,2014,-1,0,0,'".(strval($stockCode).'-'.strval($sifra))."')";
			}

		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='lager'"));
		$table_temp = "(VALUES ".$table_temp_body.") lager_temp(".implode(',',$columns).")";

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="sifra_is"){
		    	$updated_columns[] = "".$col." = lager_temp.".$col."";
			}
		}

		DB::statement("UPDATE lager t SET kolicina = lager_temp_grouped.kolicina FROM (SELECT MAX(orgj_id) AS orgj_id, SUM(kolicina) AS kolicina, MAX(roba_id) AS roba_id FROM ".$table_temp." GROUP BY roba_id, orgj_id, poslovna_godina_id) AS lager_temp_grouped WHERE t.roba_id=lager_temp_grouped.roba_id AND t.orgj_id=lager_temp_grouped.orgj_id and t.roba_id not in (select roba_id from roba where flag_zakljucan = true)");

		$where = "";
		if(DB::table('lager')->count() > 0){
			$where .= " WHERE (roba_id, orgj_id, poslovna_godina_id) NOT IN (SELECT roba_id, orgj_id, poslovna_godina_id FROM lager)";
		}

		//insert
		DB::statement("INSERT INTO lager (roba_id, orgj_id, kolicina, poslovna_godina_id, sifra_is) SELECT MAX(roba_id) AS roba_id, MAX(orgj_id) AS orgj_id, SUM(kolicina) AS kolicina, 2014, sifra_is FROM ".$table_temp.$where." GROUP BY roba_id, orgj_id, poslovna_godina_id, sifra_is");

	}
	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='lager'"));
		$table_temp = "(VALUES ".$table_temp_body.") lager_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// DB::statement("UPDATE roba t SET flag_aktivan = 0 WHERE t.id_is IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.id_is=roba_temp.id_is::varchar)");
		//DB::statement("UPDATE lager l SET kolicina = 0 WHERE l.roba_id::varchar NOT IN (SELECT roba_id::varchar FROM ".$table_temp." WHERE l.orgj_id = lager_temp.orgj_id) AND l.roba_id IN (SELECT roba_id FROM roba r WHERE r.roba_id = l.roba_id AND r.sifra_is IS NOT NULL AND flag_zakljucan='false')");
		DB::statement("UPDATE lager l SET kolicina = 0 WHERE l.sifra_is IS NOT NULL AND NOT EXISTS(SELECT * FROM ".$table_temp." WHERE l.roba_id=lager_temp.roba_id AND l.orgj_id=lager_temp.orgj_id) AND orgj_id != 1 AND l.roba_id not in (select roba_id from roba where flag_zakljucan = true)");

		//DB::statement("DELETE FROM roba WHERE flag_aktivan = 0 ");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

}
