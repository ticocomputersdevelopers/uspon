<?php
namespace IsLogik;

use DB;
 

class Support {
	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	} 

	public static function getTarifnaGrupaId($tarifna_grupa){
		$tg = DB::table('tarifna_grupa')->where('porez',$tarifna_grupa)->first();

		if(is_null($tg)){
			DB::table('tarifna_grupa')->insert(array(
				'tarifna_grupa_id' => DB::table('tarifna_grupa')->max('tarifna_grupa_id')+1,
				'sifra' => DB::table('tarifna_grupa')->max('tarifna_grupa_id')+1,
				'naziv' => $tarifna_grupa,
				'porez' => $tarifna_grupa
				));
			$tg = DB::table('tarifna_grupa')->where('naziv',$tarifna_grupa)->first();
		}
		return $tg->tarifna_grupa_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where(DB::raw('LOWER(TRIM(naziv))'), strtolower(trim($proizvodjac)))->first();

		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'naziv' => $proizvodjac,
				'sifra_connect' => 0,
				'id_is' => strval($proizvodjac)
				));
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}

	public static function getOrgjId($orgj_name){
		$orgj_name = trim($orgj_name);
		$orgj = DB::table('orgj')->where('sifra',$orgj_name)->whereNotNull('sifra')->where('sifra','!=','')->first();

		if(is_null($orgj)){
			$orgj_id = DB::select("SELECT MAX(orgj_id) + 1 AS max FROM orgj")[0]->max;
			DB::table('orgj')->insert(array(
				'orgj_id' => $orgj_id,
				'naziv' => $orgj_name,
				'sifra' => $orgj_name,
				'parent_orgj_id' => 1,
				'preduzece_id' => 1,
				'level' => 2,
				'tip_orgj_id' => -1,
				'tip_orgj_id' => -1
				));
		}else{
			$orgj_id = $orgj->orgj_id;
		}
		return $orgj_id;
	}

	public static function getMappedManufacturers(){
		$mapped = array();
		$manufacturers = DB::table('proizvodjac')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($manufacturers as $manufacturer){
			$mapped[strval($manufacturer->id_is)] = $manufacturer->proizvodjac_id;
		}
		return $mapped;
	}

	public static function getMappedVats(){
		$mapped = array();
		$vats = DB::table('tarifna_grupa')->whereNotNull('porez')->get();
		foreach($vats as $group){
			$mapped[strval($group->porez)] = $group->tarifna_grupa_id;
		}
		return $mapped;
	}
	public static function getMappedUnits(){
		$mapped = array();
		$units = DB::table('jedinica_mere')->whereNotNull('naziv')->get();
		foreach($units as $unit){
			$mapped[strval($unit->naziv)] = $unit->jedinica_mere_id;
		}
		return $mapped;
	}

	public static function updateGroupsParent($groups){
		foreach($groups as $group){
			$group_id = $group->categoryprid;
			$parent_id = isset($group->parentid) && $group->parentid != 0 ? intval($group->parentid) : '0';
			if($parent_id != 0){
				$parrent_grupa_pr = DB::table('grupa_pr')->where('id_is',$parent_id)->first();
				if(!is_null($parrent_grupa_pr)){
					$parrent_grupa_pr_id = $parrent_grupa_pr->grupa_pr_id;
					DB::table('grupa_pr')->where(array('id_is'=>$group_id))->update(array('parrent_grupa_pr_id'=>$parrent_grupa_pr_id));
				}
				// else{
				// 	DB::table('grupa_pr')->where('parrent_grupa_pr_id',$parent_id)->delete();
				// }
			}
		}

        DB::statement("update grupa_pr set parrent_grupa_pr_id = (select grupa_pr_id from grupa_pr where grupa ilike '%Logik grupe%' limit 1) where parrent_grupa_pr_id = 0 and id_is is not null and id_is <> ''");
	}

	public static function getNewGrupaId($group_name){
		$group = DB::table('grupa_pr')->where('grupa',$group_name)->first();

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => $group_name,
				'parrent_grupa_pr_id' => 0,
				'sifra' => $grupa_pr_id
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
		}
		return $grupa_pr_id;
	}

	public static function getPartnerId($sifra_kupca_logik){
		$partner = DB::table('partner')->where('id_is',$sifra_kupca_logik)->first();
		if(!is_null($partner)){
			return $partner->partner_id;
		}
		return null;
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[strval($article->id_is)] = $article->roba_id;
		}
		return $mapped;
	}
	public static function getMappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($groups as $group){
			$mapped[$group->id_is] = $group->grupa_pr_id;
		}
		return $mapped;
	}
	public static function getMappedPartners(array $id_is = array()){
		$mapped = array();
		if(count($id_is) > 0){
			$partners = DB::table('partner')->select('id_is','partner_id')->whereNotNull('id_is')->whereIn('id_is',$id_is)->get();
		}else{
			$partners = DB::table('partner')->select('id_is','partner_id')->whereNotNull('id_is')->get();
		}
		foreach($partners as $partner){
			$mapped[$partner->id_is] = $partner->partner_id;
		}
		return $mapped;
	}

	public static function getMappedSellers(){
		$mapped = array();
		$sellers = DB::table('imenik')->select('imenik_id','id_is')->whereNotNull('id_is')->where('kvota',32.00)->get();
		foreach($sellers as $seller){
			$mapped[strval(trim($seller->id_is))] = $seller->imenik_id;
		}
		return $mapped;
	}
	public static function getMappedStockrooms(){
		$mapped = array();

		$orgjs = DB::table('orgj')->select('sifra','orgj_id')->whereNotNull('sifra')->where('sifra','!=','')->get();
		foreach($orgjs as $orgj){
			$mapped[strval($orgj->sifra)] = $orgj->orgj_id;
		}

		return $mapped;
	}

	public static function getGrupaId($is_grupa_id){
		if(!is_null($is_grupa_id) && $is_grupa_id != ''){
			$grupa_pr = DB::table('grupa_pr')->where('id_is',intval($is_grupa_id))->first();
			if(!is_null($grupa_pr)){
				return $grupa_pr->grupa_pr_id;
			}
		}
		return -1;
	}
	public static function linkedWidthDC(){

		DB::statement("UPDATE dobavljac_cenovnik dc SET roba_id = r.roba_id, povezan = 1 FROM roba r WHERE dc.barkod = r.barkod AND povezan <> 1 AND dc.web_marza > 0 AND dc.roba_id = -1 AND r.barkod IS NOT NULL AND r.barkod <> ''"); 
		DB::statement("UPDATE roba r SET dobavljac_id = dc.partner_id,sifra_d =dc.sifra_kod_dobavljaca FROM dobavljac_cenovnik dc WHERE dc.roba_id = r.roba_id");

		// DB::table("UPDATE dobavljac_cenovnik dc SET roba_id = -1, povezan = 0 WHERE roba_id <> -1 AND povezan = 1 AND roba_id NOT IN (SELECT roba_id FROM roba)");
	}

	public static function deleteDouble(){

		DB::statement("UPDATE dobavljac_cenovnik set roba_id = -1,povezan = 0 where roba_id in (select  roba_id from roba where (sifra_is is null or sifra_is = '') and barkod in (select barkod from roba where sifra_is is not null and sifra_is <> '' and barkod in (select barkod from roba where barkod is not null and barkod <> '' group by barkod having count(barkod) > 1)) ) "); 
		
		DB::statement("DELETE from roba where (sifra_is is null or sifra_is = '') and barkod in (select barkod from roba where sifra_is is not null and sifra_is <> '' and barkod in (select barkod from roba where barkod is not null and barkod <> '' group by barkod having count(barkod) > 1)) and roba_id not in (SELECT distinct roba_id from web_b2c_narudzbina_stavka) and roba_id not in (SELECT distinct roba_id from web_b2b_narudzbina_stavka)");
		DB::statement("UPDATE roba set flag_cenovnik=0 WHERE sifra_is is null AND flag_zakljucan='false' ");
	}
	public static function getManufacturerId($manufacturer_id){
		$proizvodjac = DB::table('proizvodjac')->where('id_is',$manufacturer_id)->first();
		if(!is_null($proizvodjac)){
			return $proizvodjac->proizvodjac_id;
		}
		return -1;
	}

	public static function groupedStocks($stock){
		$stock = array_map(function($item){
			return $item->warehouseid;
		},$stock);

		return array_values(array_unique($stock));
	}

	public static function article_update_partner(){
		DB::statement("UPDATE roba r SET sifra_d = dc.sifra_kod_dobavljaca FROM dobavljac_cenovnik dc WHERE dc.roba_id = r.roba_id AND dc.roba_id > 0 AND dc.partner_id = 1 AND dc.povezan = 1");
		// DB::statement("UPDATE roba r SET flag_aktivan = 0 WHERE dobavljac_id = -1 OR dobavljac_id IS NULL");
	}
	public static function postUpdate(){
        DB::statement("update roba r set flag_cenovnik = 1 where (select sum(kolicina) as ukupno from lager where roba_id = r.roba_id ) > 0 and r.flag_zakljucan='false'");
        DB::statement("update roba r set flag_cenovnik = 0 where ( (select sum(kolicina) as ukupno from lager where roba_id = r.roba_id ) = 0 or (select sum(kolicina) from lager where roba_id = r.roba_id) is null ) and r.flag_zakljucan='false'");
	}
	public static function veleprodaja1_magacin(){

       // DB::statement("update roba r set flag_cenovnik = 1 where (select kolicina from lager where roba_id = r.roba_id and orgj_id = 11) > 0 and r.roba_id in (SELECT roba_id from roba where grupa_pr_id in (select grupa_pr_id from grupa_pr where parrent_grupa_pr_id = 2364)) and r.flag_zakljucan='false'");
       DB::statement("update roba r set flag_cenovnik = 0 where (select kolicina from lager where roba_id = r.roba_id and orgj_id = 11) = 0 and (select kolicina from lager where roba_id = r.roba_id and orgj_id = 26) = 0 and r.roba_id in (SELECT roba_id from roba where grupa_pr_id in (select grupa_pr_id from grupa_pr where parrent_grupa_pr_id = 2364)) and r.flag_zakljucan='false'");


		// DB::statement("update roba r set flag_cenovnik = 1 where (select kolicina from lager where roba_id = r.roba_id and orgj_id = 11) > 0 and r.roba_id in (SELECT roba_id from roba where grupa_pr_id in (select grupa_pr_id from grupa_pr where parrent_grupa_pr_id = 2360)) and r.flag_zakljucan='false'");
        DB::statement("update roba r set flag_cenovnik = 0 where (select kolicina from lager where roba_id = r.roba_id and orgj_id = 11) = 0 and (select kolicina from lager where roba_id = r.roba_id and orgj_id = 26) = 0 and roba_id in (SELECT roba_id from roba where grupa_pr_id in (select grupa_pr_id from grupa_pr where parrent_grupa_pr_id = 2360)) and r.flag_zakljucan='false'");
	}
	public static function updateStatusNarudzbina(){
        DB::statement("UPDATE web_b2b_narudzbina SET realizovano=1");
        
	}
	public static function convert($text){
		// return $text;

		$text = str_replace(array('È','è','ð','æ','Æ'),array('Č','č','đ','ć','Ć'),$text);
		// $text = str_replace("'",'"',$text);
		// $text = preg_replace('/[^a-zA-Z0-9\ \!\%\&\(\)\=\*\/\,\.\+\-\_\@\?\:\;\<\>\"\č\ć\ž\š\đ\Č\Ć\Ž\Š\Đ]/', '', $text);
		// $text = iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$text);

		return $text;
	 //    $text = preg_replace("/[áàâãªä]/u","a",$text);
	 //    $text = preg_replace("/[ÁÀÂÃÄ]/u","A",$text);
	 //    $text = preg_replace("/[ÍÌÎÏ]/u","I",$text);
	 //    $text = preg_replace("/[íìîï]/u","i",$text);
	 //    $text = preg_replace("/[éèêë]/u","e",$text);
	 //    $text = preg_replace("/[ÉÈÊË]/u","E",$text);
	 //    $text = preg_replace("/[óòôõºö]/u","o",$text);
	 //    $text = preg_replace("/[ÓÒÔÕÖ]/u","O",$text);
	 //    $text = preg_replace("/[úùûü]/u","u",$text);
	 //    $text = preg_replace("/[ÚÙÛÜ]/u","U",$text);
	 //    $text = preg_replace("/[’‘‹›‚]/u","'",$text);
	 //    $text = preg_replace("/[“”«»„]/u",'"',$text);
	 //    $text = str_replace("–","-",$text);
	 //    $text = str_replace(" "," ",$text);
	 //    $text = str_replace("ç","c",$text);
	 //    $text = str_replace("Ç","C",$text);
	 //    $text = str_replace("ñ","n",$text);
	 //    $text = str_replace("Ñ","N",$text);
	 
	 //    //2) Translation CP1252. &ndash; => -
	 //    $trans = get_html_translation_table(HTML_ENTITIES); 
	 //    $trans[chr(130)] = '&sbquo;';    // Single Low-9 Quotation Mark 
	 //    $trans[chr(131)] = '&fnof;';    // Latin Small Letter F With Hook 
	 //    $trans[chr(132)] = '&bdquo;';    // Double Low-9 Quotation Mark 
	 //    $trans[chr(133)] = '&hellip;';    // Horizontal Ellipsis 
	 //    $trans[chr(134)] = '&dagger;';    // Dagger 
	 //    $trans[chr(135)] = '&Dagger;';    // Double Dagger 
	 //    $trans[chr(136)] = '&circ;';    // Modifier Letter Circumflex Accent 
	 //    $trans[chr(137)] = '&permil;';    // Per Mille Sign 
	 //    $trans[chr(138)] = '&Scaron;';    // Latin Capital Letter S With Caron 
	 //    $trans[chr(139)] = '&lsaquo;';    // Single Left-Pointing Angle Quotation Mark 
	 //    $trans[chr(140)] = '&OElig;';    // Latin Capital Ligature OE 
	 //    $trans[chr(145)] = '&lsquo;';    // Left Single Quotation Mark 
	 //    $trans[chr(146)] = '&rsquo;';    // Right Single Quotation Mark 
	 //    $trans[chr(147)] = '&ldquo;';    // Left Double Quotation Mark 
	 //    $trans[chr(148)] = '&rdquo;';    // Right Double Quotation Mark 
	 //    $trans[chr(149)] = '&bull;';    // Bullet 
	 //    $trans[chr(150)] = '&ndash;';    // En Dash 
	 //    $trans[chr(151)] = '&mdash;';    // Em Dash 
	 //    $trans[chr(152)] = '&tilde;';    // Small Tilde 
	 //    $trans[chr(153)] = '&trade;';    // Trade Mark Sign 
	 //    $trans[chr(154)] = '&scaron;';    // Latin Small Letter S With Caron 
	 //    $trans[chr(155)] = '&rsaquo;';    // Single Right-Pointing Angle Quotation Mark 
	 //    $trans[chr(156)] = '&oelig;';    // Latin Small Ligature OE 
	 //    $trans[chr(159)] = '&Yuml;';    // Latin Capital Letter Y With Diaeresis 
	 //    $trans['euro'] = '&euro;';    // euro currency symbol 
	 //    ksort($trans); 
	     
	 //    foreach ($trans as $k => $v) {
	 //        $text = str_replace($v, $k, $text);
	 //    }
	 
	 //    // 3) remove <p>, <br/> ...
	 //    $text = strip_tags($text); 
	     
	 //    // 4) &amp; => & &quot; => '
	 //    $text = html_entity_decode($text);
	     
	 //    // 5) remove Windows-1252 symbols like "TradeMark", "Euro"...
	 //    $text = preg_replace('/[^(\x20-\x7F)]*/','', $text); 
	     
	 //    $targets=array('\r\n','\n','\r','\t');
	 //    $results=array(" "," "," ","");
	 //    $text = str_replace($targets,$results,$text);
	     
		// return ($text);
	}

	public static function convertChars($text){
		return str_replace(array('è','ð','æ','Æ'),array('č','đ','ć','Ć'),$text);
	}
	public static function convertCharsGrupa(){
		DB::statement("update grupa_pr set grupa = replace(grupa, 'æ', 'ć')");
		DB::statement("update grupa_pr set grupa = replace(grupa, 'è', 'č')");
		DB::statement("update grupa_pr set grupa = replace(grupa, 'ð', 'đ')");
		DB::statement("update grupa_pr set grupa = replace(grupa, 'Æ', 'Ć')");
		DB::statement("update grupa_pr set grupa = replace(grupa, 'È', 'Č')");
	}
	public static function entityDecode(array $id_iss=[]){
		if(count($id_iss) > 0){
			DB::statement("update roba set naziv = replace(naziv, '&#353;', 'š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#273;', 'đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#269;', 'č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#263;', 'ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#382;', 'ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#352;', 'Š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#272;', 'Đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#268;', 'Č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#262;', 'Ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#381;', 'Ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#176;', '°') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv = replace(naziv, '&#380;', 'ž') where id_is in (".implode(",",$id_iss).")");

			DB::statement("update roba set naziv_web = replace(naziv_web, '&#353;', 'š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#273;', 'đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#269;', 'č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#263;', 'ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#382;', 'ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#352;', 'Š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#272;', 'Đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#268;', 'Č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#262;', 'Ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#381;', 'Ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#176;', '°') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set naziv_web = replace(naziv_web, '&#380;', 'ž') where id_is in (".implode(",",$id_iss).")");

			DB::statement("update roba set web_opis = replace(web_opis, '&#353;', 'š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#273;', 'đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#269;', 'č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#263;', 'ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#382;', 'ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#352;', 'Š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#272;', 'Đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#268;', 'Č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#262;', 'Ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#381;', 'Ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#176;', '°') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_opis = replace(web_opis, '&#380;', 'ž') where id_is in (".implode(",",$id_iss).")");

			DB::statement("update roba set web_karakteristike = replace(web_karakteristike, '&#353;', 'š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_karakteristike = replace(web_karakteristike, '&#273;', 'đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_karakteristike = replace(web_karakteristike, '&#269;', 'č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_karakteristike = replace(web_karakteristike, '&#263;', 'ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_karakteristike = replace(web_karakteristike, '&#382;', 'ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_karakteristike = replace(web_karakteristike, '&#352;', 'Š') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_karakteristike = replace(web_karakteristike, '&#272;', 'Đ') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_karakteristike = replace(web_karakteristike, '&#268;', 'Č') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_karakteristike = replace(web_karakteristike, '&#262;', 'Ć') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_karakteristike = replace(web_karakteristike, '&#381;', 'Ž') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_karakteristike = replace(web_karakteristike, '&#176;', '°') where id_is in (".implode(",",$id_iss).")");
			DB::statement("update roba set web_karakteristike = replace(web_karakteristike, '&#380;', 'ž') where id_is in (".implode(",",$id_iss).")");
		}else{
			// DB::statement("update roba set naziv = replace(naziv, '&#353;', 'š')");
			// DB::statement("update roba set naziv = replace(naziv, '&#273;', 'đ')");
			// DB::statement("update roba set naziv = replace(naziv, '&#269;', 'č')");
			// DB::statement("update roba set naziv = replace(naziv, '&#263;', 'ć')");
			// DB::statement("update roba set naziv = replace(naziv, '&#382;', 'ž')");
			// DB::statement("update roba set naziv = replace(naziv, '&#352;', 'Š')");
			// DB::statement("update roba set naziv = replace(naziv, '&#272;', 'Đ')");
			// DB::statement("update roba set naziv = replace(naziv, '&#268;', 'Č')");
			// DB::statement("update roba set naziv = replace(naziv, '&#262;', 'Ć')");
			// DB::statement("update roba set naziv = replace(naziv, '&#381;', 'Ž')");

			// DB::statement("update roba set naziv_web = replace(naziv_web, '&#353;', 'š')");
			// DB::statement("update roba set naziv_web = replace(naziv_web, '&#273;', 'đ')");
			// DB::statement("update roba set naziv_web = replace(naziv_web, '&#269;', 'č')");
			// DB::statement("update roba set naziv_web = replace(naziv_web, '&#263;', 'ć')");
			// DB::statement("update roba set naziv_web = replace(naziv_web, '&#382;', 'ž')");
			// DB::statement("update roba set naziv_web = replace(naziv_web, '&#352;', 'Š')");
			// DB::statement("update roba set naziv_web = replace(naziv_web, '&#272;', 'Đ')");
			// DB::statement("update roba set naziv_web = replace(naziv_web, '&#268;', 'Č')");
			// DB::statement("update roba set naziv_web = replace(naziv_web, '&#262;', 'Ć')");
			// DB::statement("update roba set naziv_web = replace(naziv_web, '&#381;', 'Ž')");

			// DB::statement("update roba set web_opis = replace(web_opis, '&#353;', 'š')");
			// DB::statement("update roba set web_opis = replace(web_opis, '&#273;', 'đ')");
			// DB::statement("update roba set web_opis = replace(web_opis, '&#269;', 'č')");
			// DB::statement("update roba set web_opis = replace(web_opis, '&#263;', 'ć')");
			// DB::statement("update roba set web_opis = replace(web_opis, '&#382;', 'ž')");
			// DB::statement("update roba set web_opis = replace(web_opis, '&#352;', 'Š')");
			// DB::statement("update roba set web_opis = replace(web_opis, '&#272;', 'Đ')");
			// DB::statement("update roba set web_opis = replace(web_opis, '&#268;', 'Č')");
			// DB::statement("update roba set web_opis = replace(web_opis, '&#262;', 'Ć')");
			// DB::statement("update roba set web_opis = replace(web_opis, '&#381;', 'Ž')");

		}	
    }

	public static function convertEntity($string){
		$string = str_replace(array('è','ð','æ','Æ'),array('č','đ','ć','Ć'),$string);
		
        /* Only do the slow convert if there are 8-bit characters */
        if ( !preg_match("/[\200-\237]/", $string) && !preg_match("/[\241-\377]/", $string) )
               return $string;

        // decode three byte unicode characters
          $string = preg_replace_callback("/([\340-\357])([\200-\277])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-224)*4096+(ord($matches[2])-128)*64+(ord($matches[3])-128)).\';\';'),
                    $string);

        // decode two byte unicode characters
          $string = preg_replace_callback("/([\300-\337])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-192)*64+(ord($matches[2])-128)).\';\';'),
                    $string);

        // $string = str_replace(array("\n","&#352;","&#353;","&#268;","&#269;","&#272;","&#273;","&#381;","&#382;","&#262;","&#263;"),array("<br>","Š","š","Č","č","Đ","đ","Ž","ž","Ć","ć"),$string);
		$string = str_replace(array("\n"),array("<br>"),$string);

		// $string = iconv("UTF-8", "WINDOWS-1250//TRANSLIT//IGNORE",$string);
		
        return $string;
    }
    public static function customer_cart($partner_id){
        return DB::connection('logik')->select("SELECT * FROM partnercard WHERE partnerid = ".strval($partner_id)."");
    }

    public static function customer_cart_body($items,$partner_id){
        $result_arr = array();
        $web_b2b_kartica_id = DB::select("SELECT nextval('web_b2b_kartica_web_b2b_kartica_id_seq')")[0]->nextval;

        foreach($items as $item) {

            $web_b2b_kartica_id++;
            $datum_dokumenta = isset($item->documentdate) ? $item->documentdate : '1970-00-00T00:00:00+02:00';
            $vrsta_dokumenta = pg_escape_string(substr($item->identtype,0,300));
            $opis = isset($item->description) ? $item->description : pg_escape_string(substr($item->description,0,300));
            $duguje = isset($item->debit) ? $item->debit : 0.00;
            $potrazuje = isset($item->credit) ? $item->credit : 0.00;
            $saldo = (floatval($duguje)-floatval($potrazuje)) > 0 ? (floatval($duguje)-floatval($potrazuje)) : 0.00;

            $id_is = $partner_id.'-'.$web_b2b_kartica_id;

            $result_arr[] = "(".$web_b2b_kartica_id.",".strval($partner_id).",(NULL)::integer,'".$vrsta_dokumenta."',('".$datum_dokumenta."')::date,('".$datum_dokumenta."')::date,'".$opis."',".strval($duguje).",".strval($potrazuje).",".strval($saldo).",(NULL)::integer,'".strval($id_is)."')";

        }

        return (object) array("body"=>implode(",",$result_arr));
    }

    public static function customer_cart_insert_update($table_temp_body,$partner_id,$upd_cols=array()) {

        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_b2b_kartica'"));
        $table_temp = "(VALUES ".$table_temp_body.") web_b2b_kartica_temp(".implode(',',$columns).")";

        // DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="web_b2b_kartica_id"){
                $updated_columns[] = "".$col." = web_b2b_kartica_temp.".$col."";
            }
        }
        // DB::statement("UPDATE web_b2b_kartica t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=web_b2b_kartica_temp.id_is");
        DB::statement("DELETE FROM web_b2b_kartica WHERE partner_id=".$partner_id."");
        //insert
        DB::statement("INSERT INTO web_b2b_kartica (SELECT * FROM ".$table_temp.")");
        // DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

        DB::statement("SELECT setval('web_b2b_kartica_web_b2b_kartica_id_seq', (SELECT MAX(web_b2b_kartica_id) FROM web_b2b_kartica), TRUE)");
    }


    public static function refreshCart(){

    	$partner_ids=DB::table('partner')->whereNotNull('id_is')->get();
    	
    	foreach ($partner_ids as $partner_id) {
    	    	
	        if(is_null($partner_id->partner_id)){
	            die;
	        }
	        $partner = DB::table('partner')->where('partner_id',$partner_id->partner_id)->first();

	        $items = self::customer_cart(intval($partner->id_is));

	        if(count($items) > 0){
	            $resultCart = self::customer_cart_body($items,$partner->partner_id);
	            if(isset($resultCart->body) && $resultCart->body != ''){
	                self::customer_cart_insert_update($resultCart->body,$partner->partner_id);
	            }
	           // DB::connection('logik')->statement("UPDATE partnercard SET semaphore = 0 WHERE partnerid = ".strval($partner->id_is)."");
	        }
		}
    }

    public static function update_sifra_is_lager() {
	   	$id_iss = DB::table('roba')->WhereNotNull('id_is')->get();
	    foreach ($id_iss as $id_is) {
	        $roba_id = $id_is->roba_id;
	        $sifra_id_is = $id_is->id_is;

	        $lageri = DB::table('lager')->where('roba_id',$roba_id)->get();

	        foreach ($lageri as $lager) {
	            $orgj_id = $lager->orgj_id;
	            $sifra=$orgj_id.'-'.$sifra_id_is;

	            DB::table('lager')->where('orgj_id',$lager->orgj_id)->where('orgj_id','>',1)->WhereNull('sifra_is')->where('roba_id',$lager->roba_id)->update(array('sifra_is'=>$sifra));
	        }

	    }
    }
}