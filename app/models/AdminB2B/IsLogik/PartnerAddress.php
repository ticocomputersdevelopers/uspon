<?php
namespace IsLogik;

use DB;

class PartnerAddress {

    public static function table_body($partner_address,$mappedPartners){
        $result_arr = array();        
        $partner_lokacija_id = DB::select("SELECT MAX(partner_lokacija_id) + 1 AS max FROM partner_lokacija")[0]->max;
        foreach($partner_address as $partner) {
            
            if(isset($partner->partnerid)){            
                           
                $partner_id = isset($mappedPartners[$partner->partnerid]) ? $mappedPartners[$partner->partnerid] : null;
               
                    if(!is_null($partner_id)){                       
                    	$partner_lokacija_id++;
                        $result_arr[] = "(".strval($partner_lokacija_id).",".$partner_id.",(NULL)::integer,NULL,'".pg_escape_string(str_replace(array('è','ð','æ','Æ'),array('č','đ','ć','Ć'),$partner->address))."',NULL,NULL,0,NULL,(NULL)::date,0)";

                    }
            } 
        }

        return (object) array("body"=>implode(",",$result_arr));
    }

    public static function query_insert_update($table_temp_body,$upd_cols=array()) {
        if($table_temp_body == ''){
            return false;
        }
        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner_lokacija'"));
        //var_dump($columns);die;
        $table_temp = "(VALUES ".$table_temp_body.") partner_lokacija_temp(".implode(',',$columns).")";

        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="partner_lokacija_id"){
                $updated_columns[] = "".$col." = partner_lokacija_temp.".$col."";
            }
        }

        //delete
        DB::statement("DELETE FROM partner_lokacija ");

        //insert
        DB::statement("INSERT INTO partner_lokacija (SELECT * FROM ".$table_temp." WHERE (partner_id) NOT IN (SELECT partner_id FROM partner_lokacija))");
    }

    public static function query_delete_unexists($table_temp_body) {
        //
    }
}