<?php
namespace IsLogik;
 
use DB;

class Group {

	public static function table_body($groups){

		$result_arr = array();

		$grupa_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;

		foreach($groups as $key => $group) {
			$id = $group->categoryprid;
			$naziv = pg_escape_string(substr($group->name,0,200));
			if(!is_null($id) && !empty($naziv)){
		    	$grupa_id++;
				$parent_id = isset($group->parentid) && $group->parentid != 0 ? intval($group->parentid) : '0';

				$result_arr[] = "(".strval($grupa_id).",'".$naziv."',NULL,0,".strval($grupa_id).",0,1,1,0,NULL,NULL,NULL,NULL,0,".strval($key).",NULL,NULL,(NULL)::integer,'".$parent_id."','".$id."',NULL,0)";
			}

		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$all_columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='grupa_pr'"));
		$columns = $all_columns;
		$columns_without_id = $all_columns;
		unset($columns_without_id[0]);
		unset($columns_without_id[array_search('sifra',$columns_without_id)]);
		
		$table_temp = "(VALUES ".$table_temp_body.") grupa_pr_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="grupa_pr_id" && $col!="sifra" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = grupa_pr_temp.".$col."";
			}
		}
		DB::statement("UPDATE grupa_pr t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=grupa_pr_temp.id_is");

		DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		//insert
		DB::statement("INSERT INTO grupa_pr(grupa_pr_id,sifra,".implode(',',$columns_without_id).") SELECT nextval('grupa_pr_grupa_pr_id_seq'),nextval('grupa_pr_grupa_pr_id_seq'),".implode(',',$columns_without_id)." FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM grupa_pr t WHERE t.id_is=grupa_pr_temp.id_is)");

		DB::statement("UPDATE grupa_pr gp SET parrent_grupa_pr_id = gpj.grupa_pr_id from grupa_pr gpj where gp.sifra_is is not null and gpj.id_is is not null and gp.sifra_is <> '' and gpj.id_is <> '' and gp.sifra_is=gpj.id_is");

		DB::statement("UPDATE grupa_pr SET parrent_grupa_pr_id = ".Support::getNewGrupaId('Logik grupe')." where sifra_is = '0'");

		DB::statement("SELECT setval('grupa_pr_grupa_pr_id_seq', (SELECT MAX(grupa_pr_id) FROM grupa_pr) + 1, FALSE)");
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='grupa_pr'"));
		$table_temp = "(VALUES ".$table_temp_body.") grupa_pr_temp(".implode(',',$columns).")";

		DB::statement("UPDATE roba SET grupa_pr_id = -1 WHERE grupa_pr_id IN (SELECT grupa_pr_id FROM grupa_pr WHERE id_is NOT IN (SELECT id_is FROM ".$table_temp."))");
		DB::statement("DELETE FROM grupa_pr t WHERE id_is NOT IN (SELECT id_is FROM ".$table_temp.")");
	}

}