<?php
namespace IsLogik;
 
use DB;

class Partner {

	public static function table_body($partners){

		$result_arr = array();
		$ids_is = array();

		$partner_id = DB::select("SELECT nextval('partner_partner_id_seq')")[0]->nextval;
		foreach($partners as $partner) {
	    	$partner_id++;
 
            $id_is=$partner->partnerid;
            $sifra= isset($partner->partnerid) ? $partner->partnerid : null;
            $naziv=!empty($partner->name) ? pg_escape_string(substr($partner->name,0,250)) : null;
            $mesto=isset($partner->city) ? pg_escape_string(substr($partner->city,0,250)) : null;
            $postanski_broj=isset($partner->zip) ? pg_escape_string(substr($partner->zip,0,100)) : '';
            $adresa=isset($partner->address) ? substr(pg_escape_string($partner->address),0,250).' '.$postanski_broj : null;
            $telefon=isset($partner->phone) ? pg_escape_string(substr($partner->phone,0,250)) : null;
            $pib=isset($partner->code) ? pg_escape_string(substr($partner->code,0,100)) : null;
            $mail=isset($partner->email) ? pg_escape_string(substr($partner->email,0,100)) : null;
            $komercijalista=isset($partner->responsableperson) ? pg_escape_string(substr($partner->responsableperson,0,100)) : null;
            $rabat= isset($partner->rebatepercent) ? $partner->rebatepercent : 0;
            $kontakt_osoba=isset($partner->contactperson) ? pg_escape_string(substr($partner->contactperson,0,100)) : null;
            $limit=0;
            $valuta_placanja= isset($partner->valutedays) ? $partner->valutedays : 0;
            $fax=isset($partner->fax) ? pg_escape_string(substr($partner->fax,0,100)) : '';

            //$login=isset($partner->username) ? pg_escape_string(substr(Support::convert($partner->username),0,250)) : null;
            //$password=isset($partner->password) ? pg_escape_string(substr($partner->password,0,250)) : null;

	        $result_arr[] = "(".strval($partner_id).",'".$sifra."','".trim($naziv)."','".trim($adresa)."','".trim($mesto)."',0,'".trim($telefon)."','".trim($fax)."','".strval($pib)."',NULL,NULL,NULL,NULL,NULL,".strval($rabat).",".$limit.",".$valuta_placanja.",'".trim($naziv)."',0,0,1,1,0,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,'".trim($mail)."',0,1,0,".$partner->userid.",'".str_replace("æ", "ć", $komercijalista)."',1,'".$id_is."',(NULL)::integer,NULL,'".$kontakt_osoba."',(NULL)::integer,NULL,NULL,0,(NULL)::integer,NULL,(NULL)::timestamp)";

	        $ids_is[] = $id_is;
		}
		return (object) array("body"=>implode(",",$result_arr),"ids_is" => $ids_is);
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='partner'"));
		$table_temp = "(VALUES ".$table_temp_body.") partner_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="partner_id" && $col!="id_is" && $col!="login" && $col!="password"){
		    	$updated_columns[] = "".$col." = partner_temp.".$col."";
			}
		}
		DB::statement("UPDATE partner t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=partner_temp.id_is AND t.id_is is not null");

		//insert
		DB::statement("INSERT INTO partner (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM partner t WHERE t.id_is=partner_temp.id_is))");

		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
		DB::statement("SELECT setval('partner_partner_id_seq', (SELECT MAX(partner_id) FROM partner) + 1, FALSE)");


		DB::statement("update partner set naziv = replace(naziv, '&#353;', 'š')");
		DB::statement("update partner set naziv = replace(naziv, '&#273;', 'đ')");
		DB::statement("update partner set naziv = replace(naziv, '&#269;', 'č')");
		DB::statement("update partner set naziv = replace(naziv, '&#263;', 'ć')");
		DB::statement("update partner set naziv = replace(naziv, '&#382;', 'ž')");
		DB::statement("update partner set naziv = replace(naziv, '&#352;', 'Š')");
		DB::statement("update partner set naziv = replace(naziv, '&#272;', 'Đ')");
		DB::statement("update partner set naziv = replace(naziv, '&#268;', 'Č')");
		DB::statement("update partner set naziv = replace(naziv, '&#262;', 'Ć')");
		DB::statement("update partner set naziv = replace(naziv, '&#381;', 'Ž')");
		DB::statement("update partner set naziv = replace(naziv, 'È', 'Č')");
		DB::statement("update partner set naziv = replace(naziv, 'Æ', 'Ć')");
		DB::statement("update partner set naziv = replace(naziv, 'æ', 'ć')");
		DB::statement("update partner set naziv = replace(naziv, 'è', 'č')");
		DB::statement("update partner set naziv = replace(naziv, 'ð', 'đ')");

		DB::statement("update partner set naziv_puni = replace(naziv_puni, '&#353;', 'š')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, '&#273;', 'đ')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, '&#269;', 'č')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, '&#263;', 'ć')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, '&#382;', 'ž')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, '&#352;', 'Š')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, '&#272;', 'Đ')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, '&#268;', 'Č')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, '&#262;', 'Ć')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, '&#381;', 'Ž')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, 'È', 'Č')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, 'Æ', 'Ć')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, 'æ', 'ć')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, 'è', 'č')");
		DB::statement("update partner set naziv_puni = replace(naziv_puni, 'ð', 'đ')");

		DB::statement("update partner set mesto = replace(mesto, '&#353;', 'š')");
		DB::statement("update partner set mesto = replace(mesto, '&#273;', 'đ')");
		DB::statement("update partner set mesto = replace(mesto, '&#269;', 'č')");
		DB::statement("update partner set mesto = replace(mesto, '&#263;', 'ć')");
		DB::statement("update partner set mesto = replace(mesto, '&#382;', 'ž')");
		DB::statement("update partner set mesto = replace(mesto, '&#352;', 'Š')");
		DB::statement("update partner set mesto = replace(mesto, '&#272;', 'Đ')");
		DB::statement("update partner set mesto = replace(mesto, '&#268;', 'Č')");
		DB::statement("update partner set mesto = replace(mesto, '&#262;', 'Ć')");
		DB::statement("update partner set mesto = replace(mesto, '&#381;', 'Ž')");
		DB::statement("update partner set mesto = replace(mesto, 'È', 'Č')");
		DB::statement("update partner set mesto = replace(mesto, 'Æ', 'Ć')");
		DB::statement("update partner set mesto = replace(mesto, 'æ', 'ć')");
		DB::statement("update partner set mesto = replace(mesto, 'è', 'č')");
		DB::statement("update partner set mesto = replace(mesto, 'ð', 'đ')");

		DB::statement("update partner set adresa = replace(adresa, '&#353;', 'š')");
		DB::statement("update partner set adresa = replace(adresa, '&#273;', 'đ')");
		DB::statement("update partner set adresa = replace(adresa, '&#269;', 'č')");
		DB::statement("update partner set adresa = replace(adresa, '&#263;', 'ć')");
		DB::statement("update partner set adresa = replace(adresa, '&#382;', 'ž')");
		DB::statement("update partner set adresa = replace(adresa, '&#352;', 'Š')");
		DB::statement("update partner set adresa = replace(adresa, '&#272;', 'Đ')");
		DB::statement("update partner set adresa = replace(adresa, '&#268;', 'Č')");
		DB::statement("update partner set adresa = replace(adresa, '&#262;', 'Ć')");
		DB::statement("update partner set adresa = replace(adresa, '&#381;', 'Ž')");
		DB::statement("update partner set adresa = replace(adresa, 'È', 'Č')");
		DB::statement("update partner set adresa = replace(adresa, 'Æ', 'Ć')");
		DB::statement("update partner set adresa = replace(adresa, 'æ', 'ć')");
		DB::statement("update partner set adresa = replace(adresa, 'ð', 'đ')");
		DB::statement("update partner set adresa = replace(adresa, 'è', 'č')");


	}
}