<?php
namespace IsSkala;

use DB;
use Image;
use finfo;
use All;

class Support {

	public static function updateGroupsParent($groups){

		foreach($groups as $group){
			$group_id = $group->sifra;
			$parent_id = isset($group->parent) && $group->parent != 0 ? $group->parent : 0;
			if($parent_id != 0){
				$parrent_grupa_pr = DB::table('grupa_pr')->where('id_is',$parent_id)->first();
				if(!is_null($parrent_grupa_pr)){
					$parrent_grupa_pr_id = $parrent_grupa_pr->grupa_pr_id;
					// $parrent_grupa_pr_id = DB::table('grupa_pr')->where('grupa_pr_id',$parrent_grupa_pr_id)->pluck('parrent_grupa_pr_id');
					DB::table('grupa_pr')->where(array('id_is'=>$group_id))->update(array('parrent_grupa_pr_id'=>$parrent_grupa_pr_id));
				}else{
					DB::table('grupa_pr')->where('parrent_grupa_pr_id',$parent_id)->delete();
				}
			}
		}

	}

	public static function getGrupaId($is_grupa_id){
		$grupa_pr = DB::table('grupa_pr')->where('id_is',intval($is_grupa_id))->first();
		if(!is_null($grupa_pr)){
			return $grupa_pr->grupa_pr_id;
		}
		return -1;
	}

	public static function getManufacturerId($manufacturer_id){
		$proizvodjac = DB::table('proizvodjac')->where('id_is',$manufacturer_id)->first();
		if(!is_null($proizvodjac)){
			return $proizvodjac->proizvodjac_id;
		}
		return -1;
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[$article->id_is] = $article->roba_id;
		}
		return $mapped;
	}
	public static function getMappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->where('parrent_grupa_pr_id','!=',0)->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($groups as $group){
			$group_exp = explode('-',$group->id_is);
			if(isset($group_exp[1])){
				$mapped[$group_exp[1]] = ($group->grupa_pr_id);
			}
		}
		return $mapped;
	}

	public static function updateAricleMainGroups($mainArticlesGroups,$mappedArticles,$mappedGroups){
		foreach($mainArticlesGroups as $mainArticlesGroup){
			if(isset($mappedArticles[$mainArticlesGroup->id_proizvoda]) && isset($mappedGroups[$mainArticlesGroup->id_podkategorije])){
				DB::statement("UPDATE roba SET grupa_pr_id = ".$mappedGroups[$mainArticlesGroup->id_podkategorije]." WHERE roba_id = ".$mappedArticles[$mainArticlesGroup->id_proizvoda]."");
			}
		}
	}

	public static function image_type($name){
		$name_arr = explode('.',$name);
		for ($i=(count($name_arr)-1); $i>0; $i--) {
			if(strtolower($name_arr[$i]) =='png'){
				return 'png';
			}elseif(strtolower($name_arr[$i]) =='jpg'){
				return 'jpg';
			}elseif(strtolower($name_arr[$i]) =='jpeg'){
				return 'jpeg';
			}
		}
		return null;
	}

	public static function saveImageFile($putanja,$putanjaName,$sirina_big=600){
		try {
			$content = @file_get_contents(str_replace(' ','%20',"http://skala.rs/images/proizvodi/thumbs/lister-".$putanjaName));
            file_put_contents($putanja,$content);

			// if(filesize($putanja) <= 1000000){
			// 	Image::make($putanja)->resize($sirina_big, null, function ($constraint) { $constraint->aspectRatio(); })->save();
			// }
			return true;
		} catch (Exception $e) {
			File::delete($putanja);
			return false;
		}
	}
	public static function roba_id_from_sifra($sifra){
		return DB::table('roba')->where('sifra_is',$sifra)->pluck('roba_id');
	}
	public static function save_image_files($images,$new_images){
		$sirina_big = DB::table('options')->where('options_id',1331)->pluck('int_data');
		foreach($images as $image){
			if(isset($new_images[$image->id_slike])){
				self::saveImageFile($new_images[$image->id_slike],$image->putanja,$sirina_big);
			}
		}
	}
	public static function convert($string){
        /* Only do the slow convert if there are 8-bit characters */
        if ( !preg_match("/[\200-\237]/", $string) && !preg_match("/[\241-\377]/", $string) )
               return $string;

        // decode three byte unicode characters
          $string = preg_replace_callback("/([\340-\357])([\200-\277])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-224)*4096+(ord($matches[2])-128)*64+(ord($matches[3])-128)).\';\';'),
                    $string);

        // decode two byte unicode characters
          $string = preg_replace_callback("/([\300-\337])([\200-\277])/",
                    create_function ('$matches', 'return \'&#\'.((ord($matches[1])-192)*64+(ord($matches[2])-128)).\';\';'),
                    $string);

          return $string;
	}
}