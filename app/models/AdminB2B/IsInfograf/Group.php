<?php
namespace IsInfograf;

use DB;

class Group {

	public static function table_body($groups){

		$result_arr = array();

		$grupa_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
		foreach($groups as $group) {
		    $grupa_id++;

			$id = intval($group->id);
			$naziv = pg_escape_string(Support::convert($group->naziv));
			$parent_id = isset($group->parent_id) && $group->parent_id != 0 ? intval($group->parent_id) : '0';

			$result_arr[] = "(".strval($grupa_id).",'".$naziv."',NULL,".strval($parent_id).",".strval($grupa_id).",1,1,1,0,NULL,NULL,NULL,NULL,0,0,NULL,NULL,(NULL)::integer,NULL,'".$id."',NULL)";

		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='grupa_pr'"));
		$table_temp = "(VALUES ".$table_temp_body.") grupa_pr_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");
		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="grupa_pr_id" && $col!="sifra" && $col!="sifra_is" && $col!="id_is"){
		    	$updated_columns[] = "".$col." = grupa_pr_temp.".$col."";
			}
		}
		DB::statement("UPDATE grupa_pr t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=grupa_pr_temp.id_is");

		//insert
		DB::statement("INSERT INTO grupa_pr (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM grupa_pr t WHERE t.id_is=grupa_pr_temp.id_is))");

		DB::statement("SELECT setval('grupa_pr_grupa_pr_id_seq', (SELECT MAX(grupa_pr_id) FROM grupa_pr) + 1, FALSE)");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

	public static function query_update_unexists($table_temp_body) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='grupa_pr'"));
		$table_temp = "(VALUES ".$table_temp_body.") grupa_pr_temp(".implode(',',$columns).")";

		DB::statement("UPDATE grupa_pr t SET prikaz = 0, web_b2b_prikazi = 0, web_b2c_prikazi = 0 WHERE NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.id_is=grupa_pr_temp.id_is)");
	}

}