<?php
namespace IsInfograf;

use DB;

class Manufacturer {

	public static function table_body($manufacturers){

		$result_arr = array();

		$manufacturer_id = DB::select("SELECT MAX(proizvodjac_id) + 1 AS max FROM proizvodjac")[0]->max;
		foreach($manufacturers as $manufacturer) {
		    $manufacturer_id++;

			$id = $manufacturer->id_pro;
			$name = pg_escape_string(Support::convert($manufacturer->proizvodjac));

			$result_arr[] = "(".$manufacturer_id.",'".$name."',NULL,(NULL)::integer,(NULL)::integer,0,NULL,NULL,NULL,0,0,(NULL)::integer,'".$id."',0,NULL,NULL)";
  
		}
		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='proizvodjac'"));
		$table_temp = "(VALUES ".$table_temp_body.") proizvodjac_temp(".implode(',',$columns).")";

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="proizvodjac_id" && $col!="id_is" && $col=="naziv"){
		    	$updated_columns[] = "".$col." = proizvodjac_temp.".$col."";
			}
		}
		DB::statement("UPDATE proizvodjac t SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE t.id_is=proizvodjac_temp.id_is");

		//insert
		DB::statement("INSERT INTO proizvodjac (SELECT * FROM ".$table_temp." WHERE NOT EXISTS(SELECT * FROM proizvodjac t WHERE t.id_is=proizvodjac_temp.id_is))");

		DB::statement("SELECT setval('proizvodjac_proizvodjac_id_seq', (SELECT MAX(proizvodjac_id) FROM proizvodjac) + 1, FALSE)");
		// DB::statement("SET CLIENT_ENCODING TO 'UTF8'");
	}

	// public static function query_update_unexists($table_temp_body) {
	// 	if($table_temp_body == ''){
	// 		return false;
	// 	}
	// 	$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='proizvodjac'"));
	// 	$table_temp = "(VALUES ".$table_temp_body.") proizvodjac_temp(".implode(',',$columns).")";

	// 	DB::statement("UPDATE proizvodjac t SET prikaz = 0, web_b2b_prikazi = 0, web_b2c_prikazi = 0 WHERE NOT EXISTS(SELECT * FROM ".$table_temp." WHERE t.id_is=proizvodjac_temp.id_is)");
	// }

}