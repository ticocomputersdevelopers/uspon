<?php

class AdminB2BAnalitika {

	public static function getObradjene() {
		$realizovane = DB::table('web_b2b_narudzbina')->where('realizovano', 1)->where('web_b2b_narudzbina_id', '!=', '-1')->where('stornirano', '!=', 1)->count();
		return $realizovane;

	}

	public static function getPrihvacene() {
		$ukupno = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('prihvaceno', 1)->where('realizovano','!=', 1)->where('stornirano', '!=', 1)->count();
		return $ukupno;
	}

	public static function getRealizovane() {
		$ukupno = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('realizovano', 1)->where('stornirano', '!=', 1)->count();
		return $ukupno;
	}

	public static function getStornirane() {
		$ukupno = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('stornirano', '=', 1)->count();
		return $ukupno;
	}

	public static function getUkupnoPorudzbina() {
		$ukupno = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->count();
		return $ukupno;

	}

	public static function getNaCekanju() {
		$ukupno = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('realizovano', 0)->where('stornirano', '!=', 1)->count();
		return $ukupno;

	}
		public static function getNew() {
		$ukupno = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', -1)->where('realizovano', 0)->where('prihvaceno', 0)->where('stornirano', '!=', 1)->count();
		return $ukupno;
	}

	public static function ukupnoArtikala() {
		$ukupno = DB::table('roba')->where('roba_id', '!=', -1)->where('flag_aktivan', 1)->count();
		return $ukupno;
	}

	public static function ukupnoKorisnika() {
		$korisnici = DB::table('partner')->where('partner_id', '!=', -1)->count();
		return $korisnici;
	}

	public static function ukupanPrihod() {
		
		$query = DB::table('web_b2b_narudzbina')->leftJoin('web_b2b_narudzbina_stavka', 'web_b2b_narudzbina.web_b2b_narudzbina_id', '=', 'web_b2b_narudzbina_stavka.web_b2b_narudzbina_id')->where('web_b2b_narudzbina.realizovano', 1)->get();

		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->jm_cena * $q->kolicina;
		}
		return $ukupno;
	}

	public static function ukupanRUC() {
		
		// $query = DB::table('web_b2b_narudzbina')->leftJoin('web_b2b_narudzbina_stavka', 'web_b2b_narudzbina.web_b2b_narudzbina_id', '=', 'web_b2b_narudzbina_stavka.web_b2b_narudzbina_id')->where('realizovano', '=', 1)->where('racunska_cena_nc', '>', 0)->get();

		// $ukupno = 0;
		// foreach($query as $q){
		// 	$ukupno += $q->kolicina * ($q->jm_cena) - $q->racunska_cena_nc*$q->kolicina;
		// }
		// return $ukupno;

		return DB::select("select sum((wbns.jm_cena - wbns.racunska_cena_nc)*wbns.kolicina) as ukupno from web_b2b_narudzbina wbn left join web_b2b_narudzbina_stavka wbns on wbn.web_b2b_narudzbina_id = wbns.web_b2b_narudzbina_id where realizovano = 1")[0]->ukupno;
	}


	public static function jucerasnjiPrihod() {
		$juce = date('Y-m-d', strtotime(date('Y-m-d')." -1 day"));
		
		$query = DB::table('web_b2b_narudzbina')->leftJoin('web_b2b_narudzbina_stavka', 'web_b2b_narudzbina.web_b2b_narudzbina_id', '=', 'web_b2b_narudzbina_stavka.web_b2b_narudzbina_id')->where('web_b2b_narudzbina.realizovano', 1)->whereBetween('web_b2b_narudzbina.datum_dokumenta', array($juce, date('Y-m-d')))->get();

		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->jm_cena * $q->kolicina;
		}
		return $ukupno;
	}

	public static function mesecniPrihod() {
		$mesec = date('Y-m-d', strtotime(date('Y-m-d') . " -1 month"));
		
		
		$query = DB::table('web_b2b_narudzbina')->leftJoin('web_b2b_narudzbina_stavka', 'web_b2b_narudzbina.web_b2b_narudzbina_id', '=', 'web_b2b_narudzbina_stavka.web_b2b_narudzbina_id')->where('web_b2b_narudzbina.realizovano', 1)->whereBetween('web_b2b_narudzbina.datum_dokumenta', array($mesec, date('Y-m-d')))->select('web_b2b_narudzbina_stavka.jm_cena', 'web_b2b_narudzbina_stavka.kolicina')->get();

		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->jm_cena * $q->kolicina;
		}
		return $ukupno;

	}

	public static function mesecnaAnalitika($m) {
	
		$ukupno = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', '!=', '-1')->whereBetween('datum_dokumenta', array(date('Y-'.$m.'-01'), date( "Y-m-d", strtotime( date("Y") . "-" . $m . "-01 +1 month" ))))->count();

		return $ukupno;
	}

	public static function mesecnaAnalitika2($m) {
	
		$query = DB::table('web_b2b_narudzbina')->leftJoin('web_b2b_narudzbina_stavka', 'web_b2b_narudzbina.web_b2b_narudzbina_id', '=', 'web_b2b_narudzbina_stavka.web_b2b_narudzbina_id')->where('web_b2b_narudzbina.realizovano', 1)->whereBetween('web_b2b_narudzbina.datum_dokumenta', array(date('Y-'.$m.'-01'), date( "Y-m-d", strtotime( date("Y") . "-" . $m . "-01 +1 month" ))))->select('web_b2b_narudzbina_stavka.jm_cena', 'web_b2b_narudzbina_stavka.kolicina')->get();
		
		$ukupno = 0;
		foreach($query as $q){
			$ukupno += $q->jm_cena * $q->kolicina;
		}
		return $ukupno;

	}

	public static function bestSeller($datum_od=null,$datum_do=null){
    	if(isset($datum_od) && isset($datum_do)){
			$bettwen = "AND datum_dokumenta BETWEEN '".$datum_od."' AND '".$datum_do."' ";
			$limit = "";
    	}else{
			$bettwen = "";
			$limit = " LIMIT 10";
    	}
		return DB::select("
			SELECT roba_id, SUM(kolicina) as count 
			FROM web_b2b_narudzbina_stavka ns LEFT JOIN web_b2b_narudzbina n 
			ON n.web_b2b_narudzbina_id = ns.web_b2b_narudzbina_id 
			WHERE realizovano = 1 AND stornirano != 1 
			".$bettwen."
			GROUP BY roba_id 
			ORDER BY count DESC".$limit."");
	}

	public static function mostPopularArticles(){
		$popular = DB::table('roba')->where('flag_aktivan', 1)->where('flag_prikazi_u_cenovniku', 1)->orderBy('pregledan_puta_b2b', 'DSC')->limit(5)->get();
		return $popular;
	}

public static function prikazGrupa($id) {
		return DB::select("select(select naziv from roba where roba_id = wbns.roba_id and grupa_pr_id=$id),
                sum(kolicina), sum(kolicina*jm_cena) AS ukupno, sum((kolicina*jm_cena/1.2)-(racunska_cena_nc*kolicina)) AS razlika, racunska_cena_nc
                FROM web_b2b_narudzbina_stavka wbns WHERE  web_b2b_narudzbina_id IN 
                (SELECT web_b2b_narudzbina_id FROM web_b2b_narudzbina WHERE realizovano = 1 and stornirano=0)
                GROUP BY naziv, racunska_cena_nc
                order by sum desc");
	}
}