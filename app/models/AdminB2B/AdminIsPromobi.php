<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsPromobi\FileData;
use IsPromobi\Article;
use IsPromobi\Partner;
use IsPromobi\Stock;
use IsPromobi\PartnerCard;
use IsPromobi\Support;


class AdminIsPromobi {

    public static function execute(){

        try {
            // FileData::readOrderStatuses();

            // $partners = FileData::partners();
            $articleDetails = FileData::articles();
            // // $partnersCards = FileData::partners_cards();

            // //partner
            // $resultPartner = Partner::table_body($partners);
            // Partner::query_insert_update($resultPartner->body,array('naziv'));
            // // $mappedPartners = Support::getMappedPartners();

            $groups = Support::filteredGroups($articleDetails->articles);
            Support::saveGroups($groups);
            
            //articles
            $resultArticle = Article::table_body($articleDetails->articles);
            Article::query_insert_update($resultArticle->body,array('flag_aktivan','flag_prikazi_u_cenovniku','sifra_is','grupa_pr_id','racunska_cena_nc','racunska_cena_end','mpcena','web_cena'));
            // Article::query_update_unexists($resultArticle->body);
            $mappedArticles = Support::getMappedArticles();


            //b2b stock
            if($b2bMagacin = AdminB2BOptions::info_sys('promobi')->b2b_magacin){
                $resultStock = Stock::table_body($articleDetails->articles,$mappedArticles,$b2bMagacin);
                Stock::query_insert_update($resultStock->body);
            }
            //b2c stock
            if($b2cMagacin = AdminB2BOptions::info_sys('promobi')->b2c_magacin){
                $resultStock = Stock::table_body($articleDetails->articles,$mappedArticles,$b2cMagacin);
                Stock::query_insert_update($resultStock->body);
            }

            // //partner card
            // $resultPartnersCards = PartnerCard::table_body($partnersCards,$mappedPartners);
            // PartnerCard::query_insert_update($resultPartnersCards->body);


            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}