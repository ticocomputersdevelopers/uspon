<?php
namespace IsPromobi;

use DB;
use File;
use Service\Mailer;
use B2bOptions;

class FileData {

	public static function articles(){
		$file_path = "files/IS/xml/roba.xml";
		$products = array();
		// $partner_categories = array();

		if(!File::exists($file_path)){
			return (object) array('articles' => $products);
		}

		$xml_products = simplexml_load_file($file_path);
		// foreach($xml_products as $xml_product){
		// 	$xml_product_arr = (array) $xml_product;
		// 	$xml_product_arr['cena_nc'] = strval($xml_product->cena);
		// 	$products[] = (object) $xml_product_arr;


			// $partner_categories[] = (object) array('sifra_artikla' => $xml_product->sifra_artikla, 'kategorija_partnera' => 
			// 	array(
			// 		(object) array('naziv' => 'KA', 'cena' => $xml_product->KA),
			// 		(object) array('naziv' => 'LKA', 'cena' => $xml_product->LKA),
			// 		(object) array('naziv' => 'TT', 'cena' => $xml_product->TT),
			// 		(object) array('naziv' => 'RT', 'cena' => $xml_product->RT)
			// 	)
			// );
			
		// }
		$products = $xml_products;

		self::make_backup($file_path);
		return (object) array('articles' => $products);
	}

	public static function customers(){
		$file_path = "files/IS/xml/partner.xml";

		$customers = array();
		if(!File::exists($file_path)){
			return $customers;
		}

		$customers = simplexml_load_file($file_path);

		self::make_backup($file_path);
		return $customers;
	}

	public static function partners_cards(){
		$file_path = "files/IS/xml/partner_kartica.xml";

		$partners_cards = array();
		if(!File::exists($file_path)){
			return $partners_cards;
		}

		$partners_cards = simplexml_load_file($file_path);

		self::make_backup($file_path);
		return $partners_cards;
	}


    public static function readOrderStatuses(){
    	$statuses = File::files('files/IS/xml/narudzbina_statusi/');
    	foreach($statuses as $status){
    		$statusData = simplexml_load_file($status);
    		if(isset($statusData->narudzbina_sifra) && !is_null($statusData->narudzbina_sifra) && trim($statusData->narudzbina_sifra) != ''){
    			$order = DB::table('web_b2b_narudzbina')->where('broj_dokumenta',trim($statusData->narudzbina_sifra))->first();
    			if(!is_null($order)){
    				$updateData = array();
    				if($statusData->status == 'PRIHVACENO'){
    					$updateData = array('prihvaceno' => 1);
    				}else if($statusData->status == 'PONISTENO'){
    					$updateData = array('stornirano' => 1);
    				}else if($statusData->status == 'REALIZOVANO'){
    					$updateData = array('realizovano' => 1);
    				}
    				if(count($updateData) > 0){
	    				DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$order->web_b2b_narudzbina_id)->update($updateData);
	    			}

    				$partner = DB::table('partner')->where('partner_id',$order->partner_id)->first();
    				if(!is_null($partner)){
    					if($statusData->slanje_maila == 1 && !is_null($partner->mail) && trim($partner->mail) != ''){
	    					$mailFrom = B2bOptions::company_email();
	    					if(!is_null($mailFrom) && trim($mailFrom) != ''){
	    						$body = '<h2>Status narudžbine '.$statusData->narudzbina_sifra.' je promenjen u '.$statusData->status.'.</h2>';
		    					Mailer::send($mailFrom,trim($partner->mail),'Promena statusa narudžbine '.$statusData->narudzbina_sifra,$body);
		    				}
		    			}
	    			}
    			}
    		}

    		File::delete($status);
    	}
    }

	public static function make_backup($filePath){
		if(File::exists($filePath)){
			// $dirPath = 'files/IS/backup/'.date('Y-m-d');
			// File::makeDirectory($dirPath,0777,true,true);

			// $fileArr = explode('/',$filePath);
			// $fileName = $fileArr[count($fileArr)-1];

			// File::copy($filePath,$dirPath.'/'.date('H:i:s').'-'.$fileName);
			// File::delete($filePath);
		}
	}

}