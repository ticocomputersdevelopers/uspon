<?php
namespace IsPromobi;

use DB;

class Seller {

	public static function table_body($sellers){

		$result_arr = array();

		$imenik_id = DB::select("SELECT MAX(imenik_id) + 1 AS max FROM imenik")[0]->max;

		foreach($sellers as $seller) {
			if($seller != ''){
		    	$sellerArr = explode(' ',$seller);
		    	$ime = isset($sellerArr[0]) ? $sellerArr[0] : '';
		    	$prezime = isset($sellerArr[1]) ? $sellerArr[1] : '';

		    	if($ime != ''){
			    	$imenik_id++;

			        $result_arr[] = "(".$imenik_id.",'','".pg_escape_string($ime)."',NULL,'".pg_escape_string($prezime)."',NULL,NULL,NULL,'/',NULL,NULL,NULL,(NULL)::date,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,'m',0,'0','0','0',NULL,-1,NULL,NULL,-1,(NULL)::date,(NULL)::bytea,NULL,NULL,NULL,0,-1,32.00,0.00,0.00,0.00,0.00,0.00,0.00,1,1,1,1,0,(NULL)::integer,(NULL)::timestamp,0)";
			    }
		    }
		}

		return (object) array("body"=>implode(",",$result_arr));
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='imenik'"));
		$table_temp = "(VALUES ".$table_temp_body.") imenik_temp(".implode(',',$columns).")";

		// DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="imenik_id" && $col!="ime" && $col!="prezime"){
		    	$updated_columns[] = "".$col." = imenik_temp.".$col."";
			}
		}

		//insert
		DB::statement("INSERT INTO imenik (SELECT * FROM ".$table_temp." WHERE (ime, prezime) NOT IN (SELECT ime, prezime FROM imenik))");

	}
}