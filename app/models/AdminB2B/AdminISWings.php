<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use ISWings\WingsAPI;
use ISWings\Group;
use ISWings\Article;
use ISWings\Lager;
use ISWings\Partner;
use ISWings\Support;


class AdminISWings {

    public static function execute(){
        try {
            $wings_data = AdminB2BOptions::info_sys('wings');
            $username = $wings_data->username;
            $password = $wings_data->password;
            $token = WingsAPI::autorize($username,$password);

            if($token){
                $grupe = WingsAPI::grupe($token);
                $internalGroupMapped = Support::internalGroupMapped($grupe);
                $resultGroup = Group::table_body($grupe,$internalGroupMapped);
                Group::query_insert_update($resultGroup->body,array('grupa','parrent_grupa_pr_id'));
                Group::query_update_unexists($resultGroup->body);
                Support::updateGroupsParent($grupe,$internalGroupMapped);

                $magacini = WingsAPI::magacini($token);
                $articles = array();
                foreach(range(1,100) as $page){
                    $articles = array_merge($articles,WingsAPI::lager($token,$magacini[2]->id,$page));
                }

                //articles
                $resultArticle = Article::table_body($articles,$internalGroupMapped);
                $id_iss = Article::query_insert_update($resultArticle->body,array('grupa_pr_id','naziv','jedinica_mere_id','proizvodjac_id','naziv_displej','racunska_cena_nc','racunska_cena_end','web_cena','mpcena','naziv_web','web_opis','akcija_flag_primeni','barkod'));
                Article::query_update_unexists($resultArticle->body);
                $mappedArticles = Support::getMappedArticles();

                //lager
                $resultLager = Lager::table_body($articles,$mappedArticles,1);
                Lager::query_insert_update($resultLager->body);

                // // $id_iss = array_map('current', DB::table('roba')->select('id_is')->get());
                // Support::saveImages($token,$id_iss,$mappedArticles);

                
                // //partner
                // $partners = WingsAPI::parneri_svi($token);
                // $resultPartner = Partner::table_body($partners);
                // Partner::query_insert_update($resultPartner->body);

                
                AdminB2BIS::saveISLog('true');
                return (object) array('success'=>true);
            }
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>'Podaci za pristup su neispravni.');
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}