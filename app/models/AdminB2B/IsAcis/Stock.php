<?php
namespace IsAcis;
use DB;

class Stock {
	public static function table_body($articles,$mapped_articles,$magacin=1){

		$result_arr = array();
		foreach($articles as $article) {
			

				$sifra = $article->Sifra_Artikla;

				$roba_id = isset($mapped_articles[strval($sifra)]) ? $mapped_articles[strval($sifra)] : null;

				if(!is_null($roba_id)){
					$kolicina = 0;
					if(!empty($article->{'Koli?ina'}) && is_numeric($article->{'Koli?ina'}) && $article->{'Koli?ina'} > 0){
						$kolicina = $article->{'Koli?ina'};
					}

					$result_arr[] = "(".strval($roba_id).",".intval($magacin).",0,0,0,".strval($kolicina).",0,0,0,0,0,0,0,0,(NULL)::integer,0,0,0,0,0,2014,-1,0,0,'".strval($sifra)."')";
				}
			}
		return (object) array("body"=>implode(",",$result_arr));
		
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
		$columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='lager'"));
		$table_temp = "(VALUES ".$table_temp_body.") lager_temp(".implode(',',$columns).")";

		// update
		$updated_columns=array();
		if(count($upd_cols)>0){
			$columns = $upd_cols;
		}
		$updated_columns = array();
		foreach($columns as $col){
			if($col!="sifra_is"){
		    	$updated_columns[] = "".$col." = lager_temp.".$col."";
			}
		}

		DB::statement("UPDATE lager t SET kolicina = lager_temp.kolicina, rezervisano = lager_temp.rezervisano, poc_kolicina = lager_temp.poc_kolicina FROM ".$table_temp." WHERE t.roba_id=lager_temp.roba_id AND t.orgj_id=lager_temp.orgj_id AND t.poslovna_godina_id=lager_temp.poslovna_godina_id");

		$where = "";
		if(DB::table('lager')->count() > 0){
			$where .= " WHERE (roba_id, orgj_id, poslovna_godina_id) NOT IN (SELECT roba_id, orgj_id, poslovna_godina_id FROM lager)";
		}

		//insert
		DB::statement("INSERT INTO lager (roba_id, orgj_id, kolicina, poslovna_godina_id) SELECT roba_id, orgj_id, SUM(kolicina), poslovna_godina_id FROM ".$table_temp.$where." GROUP BY roba_id, orgj_id, poslovna_godina_id ");

	}

}