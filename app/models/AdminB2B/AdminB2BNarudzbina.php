<?php

class AdminB2BNarudzbina {

	public static function getOrders()
	{
		$orders = DB::table('web_b2b_narudzbina')->orderBy('web_b2b_narudzbina_id','desc')->paginate(20);
		return $orders;
	}

	public static function narudzbina_partner($web_b2b_narudzbina_id){
        
        $partner_id = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->pluck('partner_id');
        foreach(DB::table('partner')->where('partner_id',$partner_id)->get() as $row){
            return $row->naziv;
        }
          
    }
    public static function tarifnaGrupaPdv($id) {
        
        $procenat = DB::table('tarifna_grupa')->where('tarifna_grupa_id', $id)->pluck('porez');

        return $procenat / 100 + 1;
    }
    public static function pdv($id) {
        return DB::table('tarifna_grupa')->where('tarifna_grupa_id', $id)->pluck('porez');
    }
    public static function formatDate($date) {
        $arr = explode("-", $date);
        return $arr[2] . "-" . $arr[1] . "-" . $arr[0];
    }
    public static function kolicina($web_b2b_narudzbina_id,$roba_id){
        return intval(DB::table('web_b2b_narudzbina_stavka')->where(array('web_b2b_narudzbina_id'=>$web_b2b_narudzbina_id,'roba_id'=>$roba_id))->pluck('kolicina'));
    }

    public static function partner_telefon($web_b2b_narudzbina_id){
        
        $partner_id = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->pluck('partner_id');
        
        return DB::table('partner')->where('partner_id',$partner_id)->pluck('telefon');
    }

    public static function partner_mesto($web_b2b_narudzbina_id){
        
        $partner_id = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->pluck('partner_id');
        
        return DB::table('partner')->where('partner_id',$partner_id)->pluck('mesto');
    }

    public static function ukupnaCena($narudzbina_id) {

		 $stavke = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $narudzbina_id)->get();
			$ukupnaCena = 0;
			foreach ($stavke as $stavka) {
				$ukupnaCena += $stavka->jm_cena * $stavka->kolicina;
			}
			return $ukupnaCena . '.00';
	}

   public static function orderTotal($web_b2b_narudzbina_id){
       $sumOsnovnaCena = 0;
       $sumPdvCena = 0;
       $sumAvansCena = 0;
       $sumUkupnaCena = 0;

       $order = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', $web_b2b_narudzbina_id)->first();
       $avans_popust = 1-($order->avans/100);
       $order_stavke = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $web_b2b_narudzbina_id)->get();
       foreach($order_stavke as $row){
            $porez = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$row->tarifna_grupa_id)->pluck('porez');

            $sumOsnovnaCena += $row->jm_cena*$row->kolicina;
            $sumPdvCena += $row->jm_cena*($porez/100)*$row->kolicina;
            $sumAvansCena += $row->jm_cena*(1+$porez/100)*($order->avans/100)*$row->kolicina;
            $sumUkupnaCena += $row->jm_cena*(1+$porez/100)*$avans_popust*$row->kolicina;
       }

        return (object) array(
            'porez_cena' => $sumPdvCena,
            'cena_sa_rabatom' => $sumOsnovnaCena,
            'avans' => $sumAvansCena,
            'ukupna_cena' => $sumUkupnaCena
            );
   }

    public static function troskovi($web_b2b_narudzbina_id,$roba_id=null){
        $tezinaSum=0;
        $cenaSum=0;
        if(is_null($roba_id)){
            foreach(DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row){
                $tezinaSum+=DB::table('roba')->where('roba_id',$row->roba_id)->pluck('tezinski_faktor')*$row->kolicina;
                $cenaSum+= B2bArticle::b2bRabatCene($row->roba_id)->cena_sa_rabatom*$row->kolicina;
            }
        }else{
            $tezinaSum+=DB::table('roba')->where('roba_id',$roba_id)->pluck('tezinski_faktor');
            $cenaSum+=B2bArticle::b2bRabatCene($row->roba_id)->cena_sa_rabatom;
        }

        $troskoviSum = 0;
        $troskoviTezinaObj = (object) array('cena'=>0,'tezina_gr'=>0,'cena_do'=>0);
        $troskoviCenaObj = (object) array('cena'=>0,'tezina_gr'=>0,'cena_do'=>0);

        foreach(DB::table('web_troskovi_isporuke')->orderBy('tezina_gr','desc')->get() as $row) {
            if($tezinaSum >= $row->tezina_gr){
                $troskoviTezinaObj = $row;
                break;
            }
        }

        foreach(DB::table('web_troskovi_isporuke')->orderBy('cena_do','desc')->get() as $row) {
            if($cenaSum < $row->cena_do){
                $troskoviCenaObj = $row;
            }
        }
        
        //samo tezina
        if(AdminOptions::web_options(133)==1 && AdminOptions::web_options(150)==0){
            $troskoviSum = $troskoviTezinaObj->cena;
        //samo cena
        }else if(AdminOptions::web_options(150)==1 && AdminOptions::web_options(133)==0){
            $troskoviSum = $troskoviCenaObj->cena;
        //kombinacija cene i tezine
        }else if(AdminOptions::web_options(150)==1 && AdminOptions::web_options(133)==1){
            if($troskoviTezinaObj->cena == $troskoviCenaObj->cena){
                $troskoviSum = $troskoviTezinaObj->cena;
            }else{
                $troskoviSum = max(array($troskoviTezinaObj->cena,$troskoviCenaObj->cena));
                // if($troskoviTezinaObj->tezina_gr < $troskoviCenaObj->tezina_gr || $troskoviCenaObj->cena_do < $troskoviTezinaObj->cena_do){
                //     $troskoviSum = min(array($troskoviTezinaObj->cena,$troskoviCenaObj->cena));
                // }else{
                //     $troskoviSum = max(array($troskoviTezinaObj->cena,$troskoviCenaObj->cena));
                // }
            }
        }

        return $troskoviSum;
    }

	public static function findProperty($osobina_naziv_id, $column){
		return DB::table('osobina_naziv')->where('osobina_naziv_id', $osobina_naziv_id)->pluck($column);
	}

	public static function findPropertyValue($osobina_vrednost_id, $column){

		return DB::table('osobina_vrednost')->where('osobina_vrednost_id', $osobina_vrednost_id)->pluck($column);
	}
	public static function getOsobineStr($roba_id,$osobina_vrednost_ids){
		$str = '';
        if($osobina_vrednost_ids != null){
        	$vrednosti_ids = array();
        	foreach(DB::table('osobina_vrednost')->whereIn('osobina_vrednost_id',explode('-',$osobina_vrednost_ids))->whereIn('osobina_naziv_id',array_map('current',DB::table('osobina_naziv')->select('osobina_naziv_id')->get()))->get() as $row){
        		$vrednosti_ids[] = $row->osobina_vrednost_id;
			}
			if(count($vrednosti_ids)>0){			
	    		$str .= '(';
	            foreach($vrednosti_ids as $osobina_vrednost_id){
	    			$str .= self::findProperty(self::findPropertyValue($osobina_vrednost_id,'osobina_naziv_id'),'naziv').': '. self::findPropertyValue($osobina_vrednost_id,'vrednost').', ';
	            }
	            $str = substr($str,0,-2);
				$str .= ')';
			}
        }
        return $str;
	}
    public static function check_osobine($roba_id){
        $check = false;
        if(DB::table('roba')->where('roba_id',$roba_id)->pluck('osobine')==1){
            if(DB::table('osobina_kombinacija')->where(array('roba_id'=>$roba_id,'aktivna'=>1))->count() > 0){
                $check = true;
            }
        }
        return $check;
    }
    
    public static function datum_narudzbine($web_b2b_narudzbina_id){
        
        foreach(DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row){
            $datum=$row->datum_dokumenta;
            $dana2 = date('d', strtotime($datum));
            $meseci2 = date('m', strtotime($datum));
            $godina2 = date('Y', strtotime($datum));
        }
        return $dana2.".".$meseci2.".".$godina2;
    }

     public static function narudzbina_iznos_ukupno($web_b2b_narudzbina_id){
        $ukupno=0;
        foreach(DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row){
            $ukupno += $row->jm_cena * $row->kolicina;
        }
        return $ukupno;
    }

    public static function find($narudzbina_id, $column){
		return DB::table('web_b2b_narudzbina')->select($column)->where('web_b2b_narudzbina_id', $narudzbina_id)->pluck($column);
	}

    public static function brojNarudzbine() {
			$narudzbine=DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id','!=','-1');
			$num = array(1);
			foreach($narudzbine->get() as $row){
				$num[] = intval(str_replace( 'WNC', '', $row->broj_dokumenta));
			}
			$br_nar = max($num) + 1;
			return "WNC".str_pad($br_nar, 5, '0', STR_PAD_LEFT);
	}
    public static function narudzbina_np($web_b2b_narudzbina_id){
       $np = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->pluck('web_nacin_placanja_id'); 
       return DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$np)->pluck('naziv');
    }

    public static function narudzbina_napomena($web_b2b_narudzbina_id){
        return DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->pluck('napomena'); 
    } 

    public static function narudzbina_ni($web_b2b_narudzbina_id){
       $ni = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->pluck('web_nacin_isporuke_id'); 
       return DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$ni)->pluck('naziv');
    }

    public static function artikli($web_b2b_narudzbina_id){
       $artikal_id = DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->pluck('roba_id'); 
       return DB::table('roba')->where('roba_id',$artikal_id)->pluck('naziv');      
    }

	public static function dodatni_statusi() { 
		return DB::table('narudzbina_status')->where('selected','>','0')->get();
	}

	public static function find_status_narudzbine($narudzbina_status_id)
    {
        return DB::table('narudzbina_status')->where('narudzbina_status_id', $narudzbina_status_id)->pluck('naziv');
    }

    public static function status_narudzbine($web_b2b_narudzbina_id) { 
		$id= DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', $web_b2b_narudzbina_id)->pluck('narudzbina_status_id');
		$naziv = DB::table('narudzbina_status')->where('narudzbina_status_id', $id)->pluck('naziv');
		return $naziv;
	}

    public static function narudzbina_status_active($web_b2b_narudzbina_id){
        foreach (DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row){
            if($row->prihvaceno == 0 and $row->realizovano == 0 and $row->stornirano == 0){
                return "Nova";
            }
            else  if($row->prihvaceno != 0 and $row->realizovano == 0 and $row->stornirano == 0){
                return "Prih.";
            }
            else  if($row->realizovano != 0 and $row->stornirano == 0){
                return "Real.";
            }
            else  if($row->stornirano != 0){
                return "Stor.";
            }
        }
    }

    public static function narudzbina_status_posible($aktivna,$web_b2b_nrudzbina_id){
        switch($aktivna){
            case "Nova":
                echo '
                    <option value="1" class="status-porudzbine" data-status-target="1" data-narudzbina-id="'.$web_b2b_nrudzbina_id.'">Prihvati</option>';
            break;
            case "Prih.":
                echo ' 
                    <option value="2" class="status-porudzbine" data-status-target="2" data-narudzbina-id="'.$web_b2b_nrudzbina_id.'">Realizuj</option>
					<option value="3" class="status-porudzbine" data-status-target="3" data-narudzbina-id="'.$web_b2b_nrudzbina_id.'">Storniraj</option>';
            break;
            case "Real.":
                echo '
                    <option value="3" class="status-porudzbine" data-status-target="3" data-narudzbina-id="'.$web_b2b_nrudzbina_id.'">Storniraj</option>';
            break;
            case "Stor.":
                echo '';
            break;
        }
        
    }

    public static function artikal_narudzbina($web_b2b_narudzbina_stavka_id) {
        
        $roba_id=DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_stavka_id',$web_b2b_narudzbina_stavka_id)->pluck('roba_id');
        return AdminB2BArticles::short_title($roba_id);
        
    }

    public static function naziv($web_b2b_narudzbina_stavka_id) {
        
        $roba_id=DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_stavka_id',$web_b2b_narudzbina_stavka_id)->pluck('roba_id');
       
        return DB::table('roba')->where('roba_id',$roba_id)->pluck('naziv');;
        
    }

    public static function narudzbina_stavke($web_b2b_narudzbina_id) { 
        return DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id', $web_b2b_narudzbina_id)->orderBy('web_b2b_narudzbina_stavka_id', 'asc')->get();
    }

    public static function narudzbina_status_css($web_b2b_narudzbina_id){
        foreach (DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$web_b2b_narudzbina_id)->get() as $row){
            if($row->prihvaceno == 0 and $row->realizovano == 0 and $row->stornirano == 0){
                return "nova";
            }
            else  if($row->prihvaceno != 0 and $row->realizovano == 0 and $row->stornirano == 0){
                return "prihvacena";
            }
            else  if($row->realizovano != 0 and $row->stornirano == 0){
                return "realizovana";
            }
            else  if($row->stornirano != 0){
                return "stornirana";
            }
        }
    }

    public static function artikal_cena_naruzbina($web_b2b_narudzbina_stavka_id) {
        
        $jm_cena=DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_stavka_id',$web_b2b_narudzbina_stavka_id)->pluck('jm_cena');
        return AdminB2BArticles::cena($jm_cena);
        
    }

    public static function artikal_cena_stavka($web_b2b_narudzbina_stavka_id) {
        
        $jm_cena=DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_stavka_id',$web_b2b_narudzbina_stavka_id)->pluck('jm_cena');
        $kolicina=DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_stavka_id',$web_b2b_narudzbina_stavka_id)->pluck('kolicina');
        $ukupno=$jm_cena*$kolicina;
        return AdminB2BArticles::cena($ukupno);
        
    }
    public static function rezervacija($roba_id,$kolicina,$stornirano=false){
        $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
        $orgj_id = DB::table('imenik_magacin')->where('imenik_magacin_id',20)->limit(1)->pluck('orgj_id');
        $lager = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->first();

        if(!is_null($lager)){
            if(($lager->kolicina-$kolicina) >= 0 && !$stornirano){
                DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->update(array('kolicina'=>($lager->kolicina-$kolicina)));
            }

            if(AdminB2BOptions::web_options(131) == 1 && ($lager->rezervisano-$kolicina) >= 0){
                DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id))->update(array('rezervisano'=>($lager->rezervisano-$kolicina)));
            }
        }
    }
    public static function save($data){

	if(isset($data['posta_slanje_poslato'])){	
		$data['posta_slanje_poslato'] = 1;
	} else {
		$data['posta_slanje_poslato'] = 0;
	}

	if(isset($data['posta_slanje_id'])){
		
	} else {
		$data['posta_slanje_id'] = 1;
	}
	$save_data = [
			'partner_id' => $data['partner_id'],
			'broj_dokumenta' => $data['broj_dokumenta'],
			'datum_dokumenta' => $data['datum_dokumenta'],
			'web_nacin_placanja_id' => $data['web_nacin_placanja_id'],
			'web_nacin_isporuke_id' => $data['web_nacin_isporuke_id'],
			'ip_adresa' => $data['ip_adresa'],
			'napomena' => $data['napomena'],
            'komentar' => $data['komentar'],
			'posta_slanje_broj_posiljke' => $data['posta_slanje_broj_posiljke'],
			'posta_slanje_poslato' => $data['posta_slanje_poslato'],
			'narudzbina_status_id' => $data['narudzbina_status_id'],
			'posta_slanje_id' => $data['posta_slanje_id']
			];

	if($data['web_b2b_narudzbina_id'] == 0) {
        $save_data['datum_dokumenta'] = date('Y-m-d H:i:s');
		DB::table('web_b2b_narudzbina')->insert($save_data);
        AdminSupport::saveLog('B2B_NARUDZBINA_DODAJ', array(DB::table('web_b2b_narudzbina')->max('web_b2b_narudzbina_id')));

	} else {
		DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id', $data['web_b2b_narudzbina_id'])->update($save_data);
        AdminSupport::saveLog('B2B_NARUDZBINA_IZMENI', array($data['web_b2b_narudzbina_id']));
	}

	}

}