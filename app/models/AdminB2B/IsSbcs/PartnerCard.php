<?php
namespace IsSbcs;

use DB;


class PartnerCard {

	public static function table_body($partners){
		$partner_id_is = array_map('current',DB::select("SELECT DISTINCT id_is FROM partner"));
		$mappedPartners = Support::getMappedPartners($partner_id_is);
        $result_arr = array();
        $web_b2b_kartica_id = DB::select("SELECT nextval('web_b2b_kartica_web_b2b_kartica_id_seq')")[0]->nextval;

        foreach($partners as $partner) {
            $partner_sifra = intval($partner->sifra);
            foreach($partner->kartica_kupca->stavka_kartice as $item) {
                $partner_id = isset($mappedPartners[$partner_sifra]) ? $mappedPartners[$partner_sifra] : null;

                if(!is_null($partner_id)){
    	            $web_b2b_kartica_id++;
    	            $datum = isset($item->datum) ? $item->datum : '1980-00-00';
    	            $naziv_dokumenta = pg_escape_string(substr($item->naziv_dokumenta,0,300));
    	            $opis = isset($item->opis) ? $item->opis : pg_escape_string(substr($item->opis,0,300));
    	            $duguje = isset($item->duguje) ? $item->duguje : 0.00;
    	            $potrazuje = isset($item->potrazuje) ? $item->potrazuje : 0.00;
    	            $saldo = (floatval($duguje)-floatval($potrazuje)) > 0 ? (floatval($duguje)-floatval($potrazuje)) : 0.00;

    	            $id_is = $partner_id.'-'.$web_b2b_kartica_id;

    	            $result_arr[] = "(".$web_b2b_kartica_id.",".strval($partner_id).",(NULL)::integer,'".$naziv_dokumenta."',('".$datum."')::date,('".$datum."')::date,'".$opis."',".strval($duguje).",".strval($potrazuje).",".strval($saldo).",(NULL)::integer,'".strval($id_is)."')";

    	        }
            }
        }

        return (object) array("body"=>implode(",",$result_arr), "mapped_result"=>$mappedPartners);
	}

	public static function query_insert_update($table_temp_body,$upd_cols=array()) {
		if($table_temp_body == ''){
			return false;
		}
        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='web_b2b_kartica'"));
        $table_temp = "(VALUES ".$table_temp_body.") web_b2b_kartica_temp(".implode(',',$columns).")";

        DB::statement("SET CLIENT_ENCODING TO 'WIN1250'");

        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="web_b2b_kartica_id" && $col!="id_is"){
                $updated_columns[] = "".$col." = web_b2b_kartica_temp.".$col."";
            }
        }

        //update
        // DB::statement("UPDATE web_b2b_kartica SET ".implode(',',$updated_columns)." FROM ".$table_temp." WHERE (web_b2b_kartica_temp.partner_id,web_b2b_kartica_temp.vrsta_dokumenta,web_b2b_kartica_temp.datum_dokumenta,web_b2b_kartica_temp.opis,web_b2b_kartica_temp.duguje,web_b2b_kartica_temp.potrazuje) IN (SELECT partner_id,vrsta_dokumenta,datum_dokumenta,opis,duguje,potrazuje FROM web_b2b_kartica)");

        //insert
        DB::statement("INSERT INTO web_b2b_kartica (SELECT * FROM ".$table_temp." WHERE (partner_id,vrsta_dokumenta,datum_dokumenta,opis,duguje,potrazuje) NOT IN (SELECT partner_id,vrsta_dokumenta,datum_dokumenta,opis,duguje,potrazuje FROM web_b2b_kartica))");
        DB::statement("SET CLIENT_ENCODING TO 'UTF8'");

        DB::statement("SELECT setval('web_b2b_kartica_web_b2b_kartica_id_seq', (SELECT MAX(web_b2b_kartica_id) FROM web_b2b_kartica), FALSE)");
	}

	public static function query_delete_unexists($table_temp_body) {
		//
	}
}
