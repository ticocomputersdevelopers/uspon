<?php
use Service\Mailer;

class All {
	public static function dd($arr){	
		echo '<pre>';
		print_r($arr);
		echo '</pre>';
		die();
	}	
	
	public static function menu_top($column, $addon = ''){

		foreach (DB::table('web_b2c_seo')->where($column,1)->orWhere($column,3)->orderBy('rb_strane','asc')->get() as $row){
			
			echo "<li><a href='".Options::base_url().$addon.Url_mod::page_slug($row->naziv_stranice)->slug."'>".Url_mod::page_slug($row->naziv_stranice)->naziv."</a></li>";
			
		}
	}

	public static function menu_top_pages(){
		$query = DB::table('web_b2c_seo')->where('menu_top',1);
		if(Session::has('b2c_kupac')){
			$query = $query->where('naziv_stranice','!=','prijava');
		}
		return $query->orderBy('rb_strane','asc')->get();
	}
	public static function header_menu_pages($parrent_id=0){
		return DB::table('web_b2c_seo')->where(array('parrent_id'=>$parrent_id,'header_menu'=>1))->orderBy('rb_strane','asc')->get();
	}

	public static function broj_cerki($web_b2c_seo_id){
		
		return DB::table('web_b2c_seo')->where(array('parrent_id'=>$web_b2c_seo_id))->count();
	}
	// public static function header_pages(){
	// 	return DB::table('web_b2c_seo')->where(array('parrent_id'=>0,'header_menu'=>1))->orderBy('rb_strane','asc')->get();
	// }

	// public static function footer_pages(){
	// 	return DB::table('web_b2c_seo')->where('footer',1)->orderBy('rb_strane','asc')->get();
	// }

	// public static function menu_top_items($column){
	// 	return DB::table('web_b2c_seo')->where($column,1)->orWhere($column,3)->orderBy('rb_strane','asc')->get();
	// }

	// public static function menu_footer($column, $addon = ''){

	// 	foreach (DB::table('web_b2c_seo')->where($column,2)->orWhere($column,3)->orderBy('rb_strane','asc')->get() as $row){
			
	// 		echo "<li><a href='".Options::base_url().$addon.Url_mod::page_slug($row->naziv_stranice)->slug."'>".Url_mod::page_slug($row->naziv_stranice)->naziv."</a></li>";
			
	// 	}
	// }

	// public static function get_top_menu_items(){
	// 	return DB::table('web_b2c_seo')->where('flag_page', 4)->orderBy('rb_strane','asc')->get();
	// }
	
	public static function get_page_start(){		
		return 'pocetna';
	}

	public static function get_title_page_start(){
		return self::get_page('pocetna')->naziv;
	}

	public static function get_page($slug){
		$page = DB::table('web_b2c_seo')->where('naziv_stranice',$slug)->first();
		$page_lang = DB::table('web_b2c_seo_jezik')->where(array('web_b2c_seo_id'=>$page->web_b2c_seo_id, 'jezik_id'=>Language::lang_id()))->first();

		$page_data = array('naziv'=>$page->title, 'slug'=>$page->naziv_stranice);
		if(!is_null($page_lang)){
			$page_data['naziv'] = $page_lang->naziv;
			$page_data['slug'] = $page_lang->slug;
		}
		return (object) $page_data;
	}
	
	public static function user_password($length = 10){
    	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$randomString = '';
    	for ($i = 0; $i < $length; $i++) {
        	$randomString .= $characters[rand(0, strlen($characters) - 1)];
    	}
    	return $randomString;
	}
	
	
	// public static function user_password2($length){
 //    	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 //    	$randomString = '';
 //    	for ($i = 0; $i < $length; $i++) {
 //        	$randomString .= $characters[rand(0, strlen($characters) - 1)];
 //    	}
 //    	return $randomString;
	// }
	
	public static function ip_adress(){
		$ip;
		if(getenv("HTTP_CLIENT_IP")){
			$ip = getenv("HTTP_CLIENT_IP");
		}
		else if(getenv("HTTP_X_FORWARDED_FOR")){
			$ip = getenv("HTTP_X_FORWARDED_FOR");
		}
		else if(getenv("REMOTE_ADDR")){
			$ip = getenv("REMOTE_ADDR");
		}
		else{
			$ip = "UNKNOWN";
		}
		return $ip;
	}
	
    public static function procenat_m($num1,$num2){
    	$result=0;
    	$result=($num1-($num1*$num2)/100);
    	return $result;
    }
    public static function procenat_p($num1,$num2){
    	$result=0;
    	$result=($num1+($num1*$num2)/100);
    	return $result;
    }
  	public static	function procenat($num1,$num2){
    	$result=0;
    	$result=($num1*$num2)/100;
    	return $result;
    }
	
	public static function check_ewe($roba_id){
	
		$get_ewe_id=DB::table('partner')->where('naziv', 'ILIKE', "%ewe%")->pluck('partner_id');
		$query_dc=DB::table('dobavljac_cenovnik')->where('partner_id',$get_ewe_id)->where('roba_id',$roba_id);
		if($query_dc->count()==1){
			return 1;
		}
		else {
			return 0;
		}
		
	}
	// public static function encrypt($string){
	// 	/*$key = 'opIaod3392iiu';
	// 	return base64_encode(mcrypt_cbc(MCRYPT_RIJNDAEL_128,$key,$string,MCRYPT_ENCRYPT,''));*/
 //       $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_RAND);
	// 	$key = 'opIaod3392iiu';
	// 	return base64_encode(mcrypt_cbc(MCRYPT_RIJNDAEL_128,$key,$string,MCRYPT_MODE_CBC,$iv));
	// }

	// public static function decrypt($string){
	   
	// 	$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND);
	// 	$key = 'opIaod3392iiu';
	// 	$string = base64_decode($string);
	// 	return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128,$key, $string, MCRYPT_MODE_ECB, ''));
 	      
			
	// }

	public static function lat_long(){
		$mapa=DB::table('preduzece')->where('preduzece_id',1)->pluck('mapa');
		$lat_long = array();
		$mapa = explode(';',$mapa);
        $lat_long[0] = isset($mapa[0]) ? $mapa[0] : '';
        $lat_long[1] = isset($mapa[1]) ? $mapa[1] : '';
        return $lat_long;
	}

	// public static function change_opis($str){
	// $urlKey = strtr($str, array("\n"=>"<br />"));
   
 //    	return $urlKey;
	// }

	public static function userCodeGenerate(){
        //Random
        $strKey = md5(microtime());
        //Proveri da vec ne postoji taj
		$kod_user=DB::table('web_kupac')->where('kod',$strKey);
            
        if($kod_user->count() > 0){
            //Kod vec postoji
            return userCodeGenerate();
        }else{
            //Kod je ok
            return $strKey;
        }
    }
    
    public static function send_email_to_admin($body,$email,$name,$subject){
        
        if(Options::gnrl_options(3003)){
         
             Mail::send('shop.email.order', array('body'=>$body), function($message) use ($email, $name, $subject)
            {
                $message->from($email, $name);

                $message->to(EmailOption::mail_from())->subject($subject);
            });

        }else{
			Mailer::send(Options::company_email(),Options::company_email(), $subject, $body);
		}
    }

    public static function get_manofacture_name($proizvodjac_id){
    	return DB::table("proizvodjac")->where('proizvodjac_id',$proizvodjac_id)->pluck('naziv');
    }
	
	public static function get_manofacture($proizvodjac){
	
		foreach(DB::table('proizvodjac')->get() as $row){

			if($proizvodjac==Url_mod::filters_convert($row->naziv)){
				return $row->proizvodjac_id;
			}
		}
	}
	public static function get_fitures($vrednost,$grupa_pr_id){
		$niz=array();
		$grupa_pr_naziv=DB::table('grupa_pr_naziv')->where('grupa_pr_id',$grupa_pr_id);
		foreach($grupa_pr_naziv->get() as $row){
			$niz[]=$row->grupa_pr_naziv_id;
		}
		
		foreach(DB::table('grupa_pr_vrednost')->whereIn('grupa_pr_naziv_id',$niz)->get() as $row){
		
			if($vrednost==Url_mod::filters_convert($row->naziv)){
				return $row->grupa_pr_vrednost_id;
			}
		}
	}

	// public static function get_karkteristike($grupa_pr_ids = array(),$vrednost){
	// 	$query=DB::table('grupa_pr_naziv')->select('grupa_pr_vrednost.grupa_pr_vrednost_id', 'grupa_pr_vrednost.naziv')->distinct()
	// 	->leftJoin('grupa_pr_vrednost', 'grupa_pr_naziv.grupa_pr_naziv_id','=','grupa_pr_vrednost.grupa_pr_naziv_id')
	// 	->whereIn('grupa_pr_naziv.grupa_pr_id', $grupa_pr_ids)->get();

	// 	foreach($query as $row){
	// 		if($vrednost==Url_mod::filters_convert($row->naziv)){
	// 			return $row->grupa_pr_vrednost_id;
	// 		}
	// 	}
	// }

	// public static function get_vrednosti_count($grupa_pr_id = array(), $grupa_pr_naziv_id){
	// 	$query=DB::table('grupa_pr_naziv')->select('grupa_pr_vrednost.grupa_pr_vrednost_id')
	// 	->leftJoin('grupa_pr_vrednost', 'grupa_pr_naziv.grupa_pr_naziv_id','=','grupa_pr_vrednost.grupa_pr_naziv_id')->distinct()
	// 	->whereIn('grupa_pr_naziv.grupa_pr_id', $grupa_pr_id)
	// 	->where('grupa_pr_vrednost.grupa_pr_naziv_id', $grupa_pr_naziv_id)->count();

	// 	return $query;
	// }

	public static function get_fitures_name($grupa_pr_vrednost_id){
			return DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id',$grupa_pr_vrednost_id)->pluck('naziv');
	}

	public static function getCompare($ids)
	{
		if(!is_array($ids)){
			if(DB::table('roba')->where('roba_id', $ids)->whereNotNull('proizvodjac_id')->where('proizvodjac_id','>',0)->count() > 0){
		 		return DB::table('roba')->join('proizvodjac','roba.proizvodjac_id','=','proizvodjac.proizvodjac_id')
		 		->where('roba.roba_id', $ids)
		 		->select('roba.roba_id','roba.naziv_web',Options::checkCena()=='web_cena'?'roba.web_cena':'roba.mpcena','roba.akcija_flag_primeni','roba.akcijska_cena','proizvodjac.naziv as proizvodjac')->get();
		 	}else{
		 		return DB::select("SELECT naziv_web,web_cena,mpcena,akcija_flag_primeni,akcijska_cena, 'Nedefinisan' as proizvodjac FROM roba WHERE roba_id = ".$ids."");
		 	}
		}else{
	 		return DB::table('roba')
	 		->join('web_roba_karakteristike','roba.roba_id','=','web_roba_karakteristike.roba_id')
	 		->join('grupa_pr_naziv','web_roba_karakteristike.grupa_pr_naziv_id','=','grupa_pr_naziv.grupa_pr_naziv_id')
	 		->whereIn('roba.roba_id', $ids)
	 		->where('grupa_pr_naziv.active', 1)
	 		->select('grupa_pr_naziv.naziv','grupa_pr_naziv.rbr')
	 		->distinct()
	 		->orderBy('grupa_pr_naziv.rbr')	 		
	 		->get();

		}
	}
	public static function getVrednost($id,$grupa_pr_naziv)
	{
	 		return DB::table('web_roba_karakteristike')
	 		->join('grupa_pr_naziv','web_roba_karakteristike.grupa_pr_naziv_id','=','grupa_pr_naziv.grupa_pr_naziv_id')
	 		->where('roba_id', $id)
	 		->where('grupa_pr_naziv.naziv', $grupa_pr_naziv)
	 		->pluck('vrednost');
	}

	public static function getKarakteristike($queryIds)
	{

		$query="SELECT gn.naziv, gv.grupa_pr_vrednost_id, gv.naziv as vrednost FROM web_roba_karakteristike wrk INNER JOIN grupa_pr_vrednost gv ON gv.grupa_pr_vrednost_id = wrk.grupa_pr_vrednost_id INNER JOIN grupa_pr_naziv gn ON gn.grupa_pr_naziv_id = gv.grupa_pr_naziv_id WHERE roba_id IN (".$queryIds.") AND gn.active = 1 AND gv.active = 1 ORDER BY gn.rbr ASC, gv.rbr ASC";

 		$karak=array();
 		foreach(DB::select($query) as $row){
 			$karak[$row->naziv][]= $row->grupa_pr_vrednost_id.'<=>'.$row->vrednost;
 		}
 		$karakteristike=array();
		foreach($karak as $key => $value ){
			$karakteristike[$key] = array_count_values($value);
		}

		$groupedCharac=array();
		foreach($karakteristike as $key => $value){
			foreach($value as $k => $val){
				$valArr = explode('<=>',$k);

				if(!isset($groupedCharac[$key][$valArr[1]])){
					$groupedCharac[$key][$valArr[1]] = ['ids' => $valArr[0], 'count' => $val];
				}else{
					$groupedCharac[$key][$valArr[1]] = ['ids' => $groupedCharac[$key][$valArr[1]]['ids'].'-'.$valArr[0], 'count' => $groupedCharac[$key][$valArr[1]]['count']+$val];
				}

			}
		}

		return $groupedCharac;		
	}

	public static function getGroupedCharac($characIds)
	{
		if(count($characIds) == 0){
			return [];
		}
		$characs = DB::select("SELECT gpv.naziv, gpv.grupa_pr_vrednost_id FROM grupa_pr_vrednost gpv WHERE grupa_pr_vrednost_id IN (".implode(",",$characIds).")");
		$grouped = [];
		foreach($characs as $charac){
			if(!isset($grouped[$charac->naziv])){
				$grouped[$charac->naziv] = $charac->grupa_pr_vrednost_id;
			}else{
				$grouped[$charac->naziv] = $grouped[$charac->naziv].'-'.$charac->grupa_pr_vrednost_id;
			}
		}
		return $grouped;
	}

	public static function getManufacturersShortList()
	{
 		return DB::table('proizvodjac')->where('brend_prikazi',1)->where('proizvodjac_id', '>', 0)->orderBy('rbr','asc')->orderBy('proizvodjac_id','asc')->limit(10)->get();
	}

	public static function getProizvodjaci($queryIds)
	{
 		$proiz = array_map('current',DB::select("SELECT proizvodjac_id, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = r.proizvodjac_id ) as pro_naziv FROM roba r WHERE proizvodjac_id <> -1 AND roba_id IN (".$queryIds.") ORDER BY pro_naziv ASC"));
		return array_count_values($proiz);
	}
	
	public static function check_compare($roba_id){
		if(Session::has('compare_ids') && in_array($roba_id, Session::get('compare_ids'))){
			return true;
		}else{
			return false;
		}
	}

	public static function getNarudzbine($web_kupac_id) {
		$nar = DB::table('web_b2c_narudzbina')->where('web_kupac_id', $web_kupac_id)->get();
		return $nar;
	} 
	public static function getSlika($roba_id) {
		$slika = DB::table('web_slika')->where('roba_id', $roba_id)->where('akcija',1)->pluck('putanja');
		return $slika;
	} 

	public static function getWishIds($web_kupac_id) {
		//return DB::table('lista_zelja')->where('web_kupac_id', $web_kupac_id)->get();	
		return DB::table('lista_zelja')->join('roba','roba.roba_id','=','lista_zelja.roba_id')->where(array('web_kupac_id'=> $web_kupac_id, 'flag_aktivan'=>1, 'flag_prikazi_u_cenovniku'=>1))->get(); 
	} 

	// public static function web_slika_wishList($web_kupac_id){
	// 	$nar = DB::table('lista_zelja')->where('web_kupac_id', $web_kupac_id)->pluck('roba_id');
	// 	$web_slika=DB::select("SELECT putanja,regexp_replace(putanja, '.+/', '') AS slika FROM web_slika where roba_id=".$nar." ORDER BY akcija DESC");

	// 	if($web_slika){
	// 		$putanja = 'images/products/big/'.$web_slika[0]->slika;;
	// 	} else {
	// 		$putanja="images/no-image.jpg";
	// 	}
	     
	// 	return $putanja;
	// }
	
	public static function getNews() {
		$news = DB::table('web_vest_b2c')->join('web_vest_b2c_jezik','web_vest_b2c_jezik.web_vest_b2c_id','=','web_vest_b2c.web_vest_b2c_id')->select('*','web_vest_b2c_jezik.sadrzaj as tekst')->where(array('aktuelno'=>1,'jezik_id'=>Language::lang_id()))->orderBy('rbr','desc')->paginate(30);
		return $news;
	}
	public static function getKatalog()
    {

		return DB::table('katalog')->select('katalog_id','naziv','putanja_slika', 'putanja_pdf', 'aktivan')->where(array('b2c_aktivno'=>1))->where('vazi_od','<=',date('Y-m-d'))->where('katalog_id','<>',-1)->whereRaw("(select count(katalog_polja_id) from katalog_polja where katalog_id=katalog.katalog_id) > 0")->where('katalog_id','<>',0)->orderBy('vazi_od','desc')->get();
        // return DB::table('katalog')->select('katalog_id','naziv')->where(array('aktivan'=>1,'b2c_aktivno'=>1))->where('katalog_id','<>',-1)->whereRaw("(select count(katalog_polja_id) from katalog_polja where katalog_id=katalog.katalog_id) > 0")->where('katalog_id','<>',0)->orderBy('naziv','asc')->get();
          
    }
	public static function getShortListNews($ignore_id=0) {
		$news = DB::table('web_vest_b2c')->join('web_vest_b2c_jezik','web_vest_b2c_jezik.web_vest_b2c_id','=','web_vest_b2c.web_vest_b2c_id')->select('*','web_vest_b2c_jezik.sadrzaj as tekst')->where(array('aktuelno'=>1,'jezik_id'=>Language::lang_id()))->where('web_vest_b2c.web_vest_b2c_id','<>',$ignore_id)->orderBy('rbr','asc')->limit(6)->get();
		return $news;
	}
	public static function shortNewDesc($desc, $length=200) {
		if(strlen($desc) > $length){
	        $content = strip_tags($desc);
	        $sentences = explode('.', $content);
	        $export_content = '';
	        $snt_length = 0;
	        foreach ($sentences as $sentence) {
	            $snt_length += strlen($sentence);
	            if($snt_length < $length){
	                $export_content .= $sentence.'.';
	            }else{
	                break;
	            }
	        }
	        return $export_content.'..';
		}else{
			return $desc;
		}

	}

	public static function articleView($roba_id){
		return DB::table('roba')->where('roba_id', $roba_id)->increment('pregledan_puta');
	}

	// public static function articleViewB2B($roba_id){
	// 	return DB::table('roba')->where('roba_id', $roba_id)->increment('pregledan_puta_b2b');
	// }

	// public static function lager($roba_id){
	// 	$lager = DB::table('lager')->where('roba_id', $roba_id)->where('roba_id', '!=', -1)->pluck('kolicina');
	// 	return $lager;
	// }

	public static function getKonfiguratorArticles($grupa_pr_id){
			$lager_join = "";
			$lager_string = "";
			if(Options::vodjenje_lagera() == 1){
				$lager_join = "RIGHT JOIN lager l ON l.roba_id = r.roba_id ";
				$lager_string = "AND l.kolicina > 0 ";
			}

		    $level2=Groups::vratiSveGrupe($grupa_pr_id);  
		    $select="SELECT DISTINCT r.roba_id, r.naziv_web, r.".Options::checkCena()."";
		    $roba=" FROM roba r ".$lager_join."LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkImage('join').Product::checkCharacteristics('join')."";
			$where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".$lager_string.Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND ( r.grupa_pr_id IN(".implode(",",$level2).") OR rg.grupa_pr_id IN(".implode(",",$level2).")) ORDER BY r.".Options::checkCena()." ".(Options::web_options(207) == 0 ? "ASC" : "DESC")."";

			return DB::select($select.$roba.$where);	
	}
	public static function getKonfiguratos(){
		return DB::select("SELECT * FROM konfigurator WHERE dozvoljen = 1 ORDER BY konfigurator_id ASC");
	}

	public static function provera_tipa($tip_id,$number=null){
		$query_tip=DB::select("SELECT DISTINCT r.roba_id FROM roba r".Product::checkImage('join').Product::checkActiveGroup('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND tip_cene = ".$tip_id." ".Product::checkImage().Product::checkActiveGroup().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."");

		if($number==1){
			return count($query_tip);
		}else{
			if(count($query_tip) > 0){
				return true;
			}else{
				return false; 
			}			
		}
	}
	public static function broj_akcije($grupa_pr_id=null){
		$datum = date('Y-m-d');
		$query_akcija=DB::select("SELECT DISTINCT r.roba_id FROM roba r".Product::checkImage('join').Product::checkActiveGroup('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND akcija_flag_primeni = 1 AND CASE WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_od <= '".$datum."' AND datum_akcije_do >= '".$datum."' 
		WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NULL THEN datum_akcije_od <= '".$datum."' 
		WHEN datum_akcije_od IS NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_do >= '".$datum."' 
		ELSE 1 = 1 END ".Product::checkImage().Product::checkActiveGroup().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."");
		if(isset($grupa_pr_id)){
			$grupa_pr_ids = Groups::vratiSveGrupe($grupa_pr_id);
			$query_akcija=" AND grupa_pr_id IN (".implode(',',$grupa_pr_ids).")";
		}
		return count($query_akcija);
	}
	
	public static function provera_akcija($roba_id){
		$datum = date('Y-m-d');
		$query_akcija=DB::select("SELECT DISTINCT r.roba_id FROM roba r".Product::checkImage('join').Product::checkActiveGroup('join').Product::checkCharacteristics('join')." WHERE r.roba_id = ".$roba_id." AND r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND akcija_flag_primeni = 1 AND 
			CASE WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_od <= '".$datum."' AND datum_akcije_do >= '".$datum."' 
			WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NULL THEN datum_akcije_od <= '".$datum."' 
			WHEN datum_akcije_od IS NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_do >= '".$datum."' 
			ELSE 1 = 1 END ".Product::checkImage().Product::checkActiveGroup().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."");

			if(count($query_akcija) > 0){
				return 1;
			}else{
				return 0;
			}
	}

	public static function provera_datuma_i_tajmera($roba_id){
		$datum = DB::table('roba')->where('roba_id',$roba_id)->pluck('datum_akcije_do');
		$tajmer = DB::table('web_options')->where('naziv','Akcija tajmer')->pluck('int_data');
		if ($datum && $tajmer) {
			return 1;
		}else{
			return 0;
		}
	}

	public static function action_start($roba_id)
	{
		$datum = DB::table('roba')->where('roba_id', $roba_id)->pluck('datum_akcije_od');
		if(isset($datum)) {
			return date('d.m.Y', strtotime($datum));
		}
	}

	public static function action_deadline_article($roba_id)
	{
		$datum = DB::table('roba')->where('roba_id',$roba_id)->pluck('datum_akcije_do');
		if(isset($datum)) {
			return date('d.m.Y', strtotime($datum));
		}
	}

	public static function action_deadline($roba_id)
	{
		return DB::table('roba')->where('roba_id',$roba_id)->pluck('datum_akcije_do');
	}

    public static function check_category($grupa_pr_id){
        if(Session::has('compare_category') && Session::get('compare_category') != 0){
        	if($grupa_pr_id != Session::get('compare_category')){
            	Session::forget('compare_ids');
            	Session::put('compare_category',$grupa_pr_id);          
        	}
        }else{
        	Session::put('compare_category',$grupa_pr_id); 
        }
    }

    public static function footer_sections(){
    	return DB::select("SELECT * FROM futer_sekcija fs LEFT JOIN futer_sekcija_tip fst ON fs.futer_sekcija_tip_id = fst.futer_sekcija_tip_id LEFT JOIN futer_sekcija_jezik fsj ON fs.futer_sekcija_id = fsj.futer_sekcija_id WHERE fst.aktivan = 1 AND fsj.jezik_id = ".Language::lang_id()." AND fs.aktivan = 1 ORDER BY fs.rbr ASC");
    }

    public static function footer_section_pages($futer_sekcija_id){
    	$where = " ";
		if(Session::has('b2c_kupac')){
			$where .= "AND naziv_stranice <> 'prijava'";
		}
    	return DB::select("SELECT * FROM futer_sekcija_strana fss LEFT JOIN web_b2c_seo wbs ON fss.web_b2c_seo_id = wbs.web_b2c_seo_id WHERE futer_sekcija_id = ".$futer_sekcija_id."".$where." ORDER BY rb_strane ASC");
    }

    // public static function tekst_pocetna() 
    // {
    //   return DB::table('web_b2c_seo')->where('naziv_stranice', 'pocetna')->pluck('tekst');
    // }

    public static function newslatter_description(){
    	$newslatter_description = DB::table('web_b2c_newslatter_description')->where(array('jezik_id'=>Language::lang_id()))->first();
    	if(!is_null($newslatter_description)){
    		return $newslatter_description;
    	}
    	return (object) array('naslov'=>'','sadrzaj'=>'');
    }
	public static function getGrupe($queryIds)
	{
 		$gruoups = array_map('current',DB::select("SELECT grupa_pr_id, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id ) as gr_naziv FROM roba r WHERE grupa_pr_id <> -1 AND grupa_pr_id IN (".$queryIds.") ORDER BY gr_naziv ASC"));
		return array_count_values($gruoups);
	}
	
	public static function getSliderDeviceByDeviceId($slider_id){
		return DB::table('slajder')->where('slajder_id',$slider_id)->pluck('slajder_device_id');
	}
}
	 