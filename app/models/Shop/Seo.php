<?php 
class Seo {
	public static function get_page_id($slug){
		$strana = DB::table('web_b2c_seo')->where('naziv_stranice',$slug)->first();
		return !is_null($strana) ? $strana->web_b2c_seo_id : 0;
	}

	public static function get_b2b_page_id($slug){
		$strana = DB::table('web_b2b_seo')->where('naziv_stranice',$slug)->first();
		return !is_null($strana) ? $strana->web_b2b_seo_id : 0;
	}

	public static function make_keywords($string){
		return str_replace(array(' ','  ','   '),array(', ',', ',', '),trim(preg_replace('/[^a-z-šđčćžćабвгдђежзијклљмнњопрстћуфхцчџш ]+/', '', strtolower(str_replace(array('Š','Đ','Č','Ć','Ž','А','Б','В','Г','Д','Ђ','Е','Ж','З','И','Ј','К','Л','Љ','М','Н','Њ','О','П','Р','С','Т','Ћ','У','Ф','Х','Ц','Ч','Џ','Ш'),array('š','đ','č','ć','ž','а','б','в','г','д','ђ','е','ж','з','и','ј','к','л','љ','м','н','њ','о','п','р','с','т','ћ','у','ф','х','ц','ч','џ','ш'), $string)))));
	}

	public static function cleaning($string){
		return preg_replace("/[^a-zA-Z0-9-šđčćžćŠĐČĆŽабвгдђежзијклљмнњопрстћуфхцчџшАБВГДЂЕЖЗИЈКЛЉМНЊОПРСТЋУФХЦЧЏШ|!%&()=*,.+-_@?:;<> ]/", "",$string);
	}

	public static function make_description($string){
		$string = trim(strip_tags($string));
		$sum_len = 0;
		$str_arr = explode(' ',$string);
		$description = '';
		foreach($str_arr as $str){
			$sum_len += strlen($str);
			if($sum_len < 163){
				$description .= ' '.$str;
			}else{
				break; 
			}
		}
		return trim(self::cleaning($description));
	}
 
	public static function page_autogenerate($page_id){
		$strana = DB::table('web_b2c_seo')->where('web_b2c_seo_id',$page_id)->first();
		$title = self::cleaning(Language::trans($strana->title));
        return (object) array(
        	'title' => $title.' - '.Options::company_name(),
        	'description' => $title,
        	'keywords' => self::make_keywords($title)
        	);
	}
	public static function b2b_page_autogenerate($page_id){
		$strana = DB::table('web_b2b_seo')->where('web_b2b_seo_id',$page_id)->first();
		$title = self::cleaning(Language::trans($strana->title));
        return (object) array(
        	'title' => $title.' - '.Options::company_name(),
        	'description' => $title,
        	'keywords' => self::make_keywords($title)
        	);
	}

	public static function grupa_autogenerate($grupa_pr_id){
		$grupa = DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->first();
		$title = self::cleaning(Language::trans($grupa->grupa));
        return (object) array(
        	'title' => $title.' - '.Options::company_name(),
        	'description' => $title,
        	'keywords' => self::make_keywords($title)
        	);
	}

	public static function article_autogenerate($roba_id){
		$roba = DB::table('roba')->select('naziv','web_opis')->where('roba_id',$roba_id)->first();
		$title = self::cleaning(Language::trans($roba->naziv));
		$description = Language::trans($roba->web_opis);
        return (object) array(
        	'title' => $title.' - '.Options::company_name(),
        	'description' => !is_null($description) && strip_tags($description) != '' ? self::make_description($description) : $title,
        	'keywords' => self::make_keywords($title)
        	);
	}

	public static function proizvodjac_autogenerate($proizvodjac_id){
		$proizvodjac = DB::table('proizvodjac')->select('naziv')->where('proizvodjac_id',$proizvodjac_id)->first();
		$title = self::cleaning(Language::trans($proizvodjac->naziv));
        return (object) array(
        	'title' => $title.' - '.Options::company_name(),
        	'description' => $title,
        	'keywords' => $title.', '.self::company_tag(Options::company_name())
        	);
	}

	public static function vest_autogenerate($web_vest_b2c_id){
		$web_vest_b2c = DB::table('web_vest_b2c_jezik')->select('naslov','sadrzaj')->where(array('web_vest_b2c_id'=>$web_vest_b2c_id))->orderBy('jezik_id','asc')->first();
		$title = self::cleaning(Language::trans($web_vest_b2c->naslov));
		$description = Language::trans($web_vest_b2c->sadrzaj);
        return (object) array(
        	'title' => $title.' - '.Options::company_name(),
        	'description' => !is_null($description) && strip_tags($description) != '' ? self::make_description($description) : $title,
        	'keywords' => self::make_keywords($title)
        	);
	}

	public static function page($slug){
		$page_id = self::get_page_id($slug);
        $stranica_jezik=DB::table('web_b2c_seo_jezik')->where(array('web_b2c_seo_id'=>$page_id, 'jezik_id'=>Language::lang_id()))->first();
        $autogenerate = self::page_autogenerate($page_id);
        return (object) array(
        	'title' => (!is_null($stranica_jezik) && isset($stranica_jezik->title) && $stranica_jezik->title != '' ? self::cleaning($stranica_jezik->title) : $autogenerate->title),
        	'description' => !is_null($stranica_jezik) && isset($stranica_jezik->description) && $stranica_jezik->description != '' ? self::cleaning($stranica_jezik->description) : $autogenerate->description,
        	'keywords' => !is_null($stranica_jezik) && isset($stranica_jezik->keywords) && $stranica_jezik->keywords != '' ? $stranica_jezik->keywords : $autogenerate->keywords
        	);
	}
	public static function b2b_page($slug){
		$page_id = self::get_b2b_page_id($slug);
        $stranica_jezik=DB::table('web_b2b_seo_jezik')->where(array('web_b2b_seo_id'=>$page_id, 'jezik_id'=>Language::lang_id()))->first();
        $autogenerate = self::b2b_page_autogenerate($page_id);
        return (object) array(
        	'title' => (!is_null($stranica_jezik) && isset($stranica_jezik->title) && $stranica_jezik->title != '' ? self::cleaning($stranica_jezik->title) : $autogenerate->title),
        	'description' => !is_null($stranica_jezik) && isset($stranica_jezik->description) && $stranica_jezik->description != '' ? self::cleaning($stranica_jezik->description) : $autogenerate->description,
        	'keywords' => !is_null($stranica_jezik) && isset($stranica_jezik->keywords) && $stranica_jezik->keywords != '' ? $stranica_jezik->keywords : $autogenerate->keywords
        	);
	}

	public static function grupa($grupa_pr_id){
        $grupa_jezik=DB::table('grupa_pr_jezik')->where(array('grupa_pr_id'=>$grupa_pr_id, 'jezik_id'=>Language::lang_id()))->first();
        $autogenerate = self::grupa_autogenerate($grupa_pr_id);
        return (object) array(
        	'title' => (!is_null($grupa_jezik) && isset($grupa_jezik->title) && $grupa_jezik->title != '' ? self::cleaning($grupa_jezik->title) : $autogenerate->title),
        	'description' => !is_null($grupa_jezik) && isset($grupa_jezik->description) && $grupa_jezik->description != '' ? self::cleaning($grupa_jezik->description) : $autogenerate->description,
        	'keywords' => !is_null($grupa_jezik) && isset($grupa_jezik->keywords) && $grupa_jezik->keywords != '' ? $grupa_jezik->keywords : $autogenerate->keywords
        	);
	}

	public static function article($roba_id){
        $roba_jezik=DB::table('roba_jezik')->where(array('roba_id'=>$roba_id, 'jezik_id'=>Language::lang_id()))->first();
        $autogenerate = self::article_autogenerate($roba_id);
        return (object) array(
        	'title' => (!is_null($roba_jezik) && isset($roba_jezik->title) && $roba_jezik->title != '' ? self::cleaning($roba_jezik->title) : $autogenerate->title),
        	'description' => !is_null($roba_jezik) && isset($roba_jezik->description) && $roba_jezik->description != '' ? self::cleaning($roba_jezik->description) : $autogenerate->description,
        	'keywords' => !is_null($roba_jezik) && isset($roba_jezik->keywords) && $roba_jezik->keywords != '' ? $roba_jezik->keywords : $autogenerate->keywords
        	);
	}

	public static function proizvodjac($proizvodjac_id){
        $proizvodjac_jezik=DB::table('proizvodjac_jezik')->where(array('proizvodjac_id'=>$proizvodjac_id, 'jezik_id'=>Language::lang_id()))->first();
        $autogenerate = self::proizvodjac_autogenerate($proizvodjac_id);
        return (object) array(
        	'title' => (!is_null($proizvodjac_jezik) && isset($proizvodjac_jezik->title) && $proizvodjac_jezik->title != '' ? self::cleaning($proizvodjac_jezik->title) : $autogenerate->title),
        	'description' => !is_null($proizvodjac_jezik) && isset($proizvodjac_jezik->description) && $proizvodjac_jezik->description != '' ? self::cleaning($proizvodjac_jezik->description) : $autogenerate->description,
        	'keywords' => !is_null($proizvodjac_jezik) && isset($proizvodjac_jezik->keywords) && $proizvodjac_jezik->keywords != '' ? $proizvodjac_jezik->keywords : $autogenerate->keywords
        	);
	}

	public static function vest($web_vest_b2c_id){
        $web_vest_b2c_jezik=DB::table('web_vest_b2c_jezik')->where(array('web_vest_b2c_id'=>$web_vest_b2c_id, 'jezik_id'=>Language::lang_id()))->first();
        $autogenerate = self::vest_autogenerate($web_vest_b2c_id);
        return (object) array(
        	'title' => (!is_null($web_vest_b2c_jezik) && isset($web_vest_b2c_jezik->title) && $web_vest_b2c_jezik->title != '' ? self::cleaning($web_vest_b2c_jezik->title) : $autogenerate->title),
        	'description' => !is_null($web_vest_b2c_jezik) && isset($web_vest_b2c_jezik->description) && $web_vest_b2c_jezik->description != '' ? self::cleaning($web_vest_b2c_jezik->description) : $autogenerate->description,
        	'keywords' => !is_null($web_vest_b2c_jezik) && isset($web_vest_b2c_jezik->keywords) && $web_vest_b2c_jezik->keywords != '' ? $web_vest_b2c_jezik->keywords : $autogenerate->keywords
        	);
	}

	public static function company_tag($company_name){
		return trim(preg_replace('/[^a-z-šđčćžćабвгдђежзијклљмнњопрстћуфхцчџш ]+/', '', strtolower(str_replace(array('Š','Đ','Č','Ć','Ž','А','Б','В','Г','Д','Ђ','Е','Ж','З','И','Ј','К','Л','Љ','М','Н','Њ','О','П','Р','С','Т','Ћ','У','Ф','Х','Ц','Ч','Џ','Ш'),array('š','đ','č','ć','ž','а','б','в','г','д','ђ','е','ж','з','и','ј','к','л','љ','м','н','њ','о','п','р','с','т','ћ','у','ф','х','ц','ч','џ','ш'), $company_name))));
	}

	// public static function seo_title($strana){
	// 	return DB::table('web_b2c_seo')->where('naziv_stranice',$strana)->pluck('title');	
	// }

	// public static function seo_keywords($strana){
	// 	foreach(DB::table('web_b2c_seo')->where('naziv_stranice',$strana)->get() as $row){
				
	// 			return $row->keywords;
	// 		}
		
	// }

	// public static function seo_description($strana){
	// 	foreach(DB::table('web_b2c_seo')->where('naziv_stranice',$strana)->get() as $row){
				
	// 			return $row->description;
	// 		}
		
	// }

	// public static function b2b_seo_title($strana){
	// 	return DB::table('web_b2b_seo')->where('naziv_stranice',$strana)->pluck('title');
	// }
	// public static function b2b_seo_description($strana){
	// 	return DB::table('web_b2b_seo')->where('naziv_stranice',$strana)->pluck('description');
	// }
	// public static function b2b_seo_keywords($strana){
	// 	return DB::table('web_b2b_seo')->where('naziv_stranice',$strana)->pluck('keywords');
	// }

	public static function grupa_description($grupa_pr_id){
			$opis = DB::table('grupa_pr')->where('grupa_pr_id', $grupa_pr_id)->pluck('opis');
			if(isset($opis)){
				return $opis;
			}
			return Seo::grupa_title($grupa_pr_id);			
	}

	public static function grupa_title($grupa_pr_id){
		
		return Language::trans(DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->pluck('grupa'));
		
	}

	public static function grupa_keywods($grupa_pr_id){
		 $keywords = DB::table('grupa_pr')->where('grupa_pr_id', $grupa_pr_id)->pluck('keywords');
		 if(isset($keywords)){
				return $keywords;
			}
		return Seo::grupa_title($grupa_pr_id);	
		 
	}

	public static function get_kontakt(){
		return 'kontakt';
		
	}

	// public static function get_onama(){
	// 	return 'o-nama';
	// }

	public static function get_korpa(){
		return 'korpa';
	}





}