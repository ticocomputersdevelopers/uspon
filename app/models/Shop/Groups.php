<?php 
class Groups {

	public static function broj_cerki($grupa_pr_id){
		
		return DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$grupa_pr_id,'web_b2c_prikazi'=>1))->count();
			
	}

	public static function check_image_grupa($putanja){
		$check = false;
		if($putanja != null && $putanja != ""){
			if(file_exists($putanja)){
				$check = true;
			}
		}
		return $check;	
	}	

	public static function get_grupa_title($grupa_slug)
	{
		foreach(DB::table('grupa_pr')->orderBy('grupa_pr_id','asc')->get() as $grupa){
			if(Url_mod::slug_trans($grupa->grupa) == $grupa_slug){
				return $grupa->grupa;
				break;
			}
		}
		return $grupa_slug;
	}

	// public static function get_parrent_gr_name($grupa_pr_id){
 //        $parrent_grupa=DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->pluck('parrent_grupa_pr_id');
 //        if($parrent_grupa!=0){
 //            $naziv=DB::table('grupa_pr')->where('grupa_pr_id',$parrent_grupa)->pluck('grupa');
 //            return "/".Url_mod::slug_trans($naziv);
 //        }
 //        else {
 //            $naziv="";
 //            return $naziv;
 //        }   
 //    }

	public static function grupa_level($grupa_pr_id){
		$query_grupa_level1=DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->pluck('parrent_grupa_pr_id');

		if($query_grupa_level1==0){
			return 1;
		}
		else {	
				$query_grupa_level2=DB::table('grupa_pr')->where('grupa_pr_id',$query_grupa_level1)->pluck('parrent_grupa_pr_id');

				if($query_grupa_level2==0)
					{ return 2;}
				else {
					return 3;
				}
		}
	}

	// public static function cerke($grupa_pr_id){

	// 	$niz2=array();
	// 			$niz="";
	// 			$br=0;
	// 			$query_roba=DB::table('grupa_pr')->where(array('web_b2c_prikazi'=>1,'parrent_grupa_pr_id'=>$grupa_pr_id));
				
	// 			foreach ($query_roba->get() as $key) {
							
	// 						$grupa_pr_id_c=$key->grupa_pr_id;
							
	// 							 if($br==0){
	// 								$niz.=$grupa_pr_id_c; 
	// 							 }	
	// 							 else {
	// 								 $niz.=",".$grupa_pr_id_c;
	// 							 }
	// 							 $br+=1;
	// 							}
									
	// 						$niz2=explode(',',$niz);	
	// 	return $niz2;	
	// }	
	// 
	

	// public static function cerke2($grupa_pr_id){

	// 	$niz="";
		
	// 	$query_roba=DB::table('grupa_pr')->where(array('web_b2c_prikazi'=>1,'parrent_grupa_pr_id'=>$grupa_pr_id));
		
	// 	foreach ($query_roba->get() as $key) {
					
	// 				$grupa_pr_id_c=$key->grupa_pr_id;							
						 
	// 						 $niz.=$grupa_pr_id_c.',';								 
						 
	// 					}
	// 					$niz.=0;	
	// 				$niz2=explode(',',$niz);	
	// 		return $niz;	
						
 //      }


	public static function deca($grupa_pr_id){
		return DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$grupa_pr_id,'web_b2c_prikazi'=>1))->get();
	}

	public static function sveGrupe($grupa_pr_id)
	{		
		$niz=$grupa_pr_id.',';
		$svaDeca=Groups::deca($grupa_pr_id);
		if (count($svaDeca)>0)
		{
			foreach ($svaDeca as $row)
			{
				$niz.=Groups::sveGrupe($row->grupa_pr_id);
			}				

		}
		return $niz;			
	}
	
	

	public static function vratiSveGrupe($grupa_pr_id)
	{
		$grupe=Groups::sveGrupe($grupa_pr_id);		
		$grupe=trim($grupe, ',');
		$grupe=explode(',',$grupe);
		$grupe=array_filter($grupe);
		return $grupe;	
	}

	public static function allGroups(&$niz,$grupa_pr_id)
	{		
		$niz[]=$grupa_pr_id;
		$check_parent=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$grupa_pr_id, 'web_b2c_prikazi'=>1))->get();
		if (count($check_parent)>0)
		{
			foreach ($check_parent as $row)
			{
				Groups::allGroups($niz,$row->grupa_pr_id);
			}				
		}
	}

	public static function getGrupa($grupa_pr_id){
		$grupa = DB::table('grupa_pr')->where('grupa_pr_id', $grupa_pr_id)->pluck('grupa');
		return Language::trans($grupa);
	}

	public static function getSearchResults() {
        $offset = Language::segment_offset();
        $org_search = Request::segment(2+$offset);
		$search = pg_escape_string(strtolower(urldecode($org_search)));
		return $search;
	}

	public static function firstGropusSelect($type=''){
		$select = '<select class="JSSearchGroup'.$type.'">';
		$query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get();
		$select .= '<option value=""></option>';
		foreach($query_category_first as $group){
			$select .= '<option value="'.$group->grupa_pr_id.'">'.Language::trans($group->grupa).'</option>';
		}

		$select .= '</select>';
		return $select;
	}
	// public static function artikli_count($grupa_pr_id){

	// 	$gr=self::vratiSveGrupe($grupa_pr_id);
	// 	$queryOsnovni=DB::table('roba')
 //                ->select([DB::RAW('DISTINCT(roba.roba_id)')])
 //               	->leftJoin('roba_grupe', 'roba_grupe.roba_id','=','roba.roba_id');

		     
 //        $queryOsnovni->where(function($queryOsnovni) use ($gr){
 //                	$queryOsnovni->whereIn('roba.grupa_pr_id', $gr)
 //                	      ->orWhereIn('roba_grupe.grupa_pr_id', $gr);
 //                })
	// 			->where(array('roba.flag_aktivan'=>1, 'roba.flag_prikazi_u_cenovniku'=>1));

	// 	return count($queryOsnovni->get());					
	// }

    public static function listGroups(){
        $render = '<ul id="JSListaGrupe" class="fast-add-group" hidden="hidden">';
        foreach(self::getLevelGroups(0) as $row_gr){                
            $render .= '<li class="JSListaGrupa" data-grupa_pr_id="'.$row_gr->grupa_pr_id.'" data-grupa="'.Language::trans($row_gr->grupa).'">'.Language::trans($row_gr->grupa).' - '.$row_gr->web_b2c_prikazi.'</li>';
            foreach(self::getLevelGroups($row_gr->grupa_pr_id) as $row_gr1){
                $render .= '<li class="JSListaGrupa" data-grupa_pr_id="'.$row_gr1->grupa_pr_id.'" data-grupa="'.Language::trans($row_gr1->grupa).'" >|&nbsp;&nbsp;&nbsp;&nbsp;'.Language::trans($row_gr1->grupa).' - '.$row_gr1->web_b2c_prikazi.'</li>';
                foreach(self::getLevelGroups($row_gr1->grupa_pr_id) as $row_gr2){
                    $render .= '<li class="JSListaGrupa" data-grupa_pr_id="'.$row_gr2->grupa_pr_id.'" data-grupa="'.Language::trans($row_gr2->grupa).'" >|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.Language::trans($row_gr2->grupa).' - '.$row_gr2->web_b2c_prikazi.'</li>';
                    foreach(self::getLevelGroups($row_gr2->grupa_pr_id) as $row_gr3){
                        $render .= '<li class="JSListaGrupa" data-grupa_pr_id="'.$row_gr3->grupa_pr_id.'" data-grupa="'.Language::trans($row_gr3->grupa).'">|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;'.Language::trans($row_gr3->grupa).' - '.$row_gr3->web_b2c_prikazi.'</li>';
                    }
                }
            }
        }
        return $render.'<ul>';
    }
    public static function getLevelGroups($grupa_pr_id){
        return DB::table('grupa_pr')->select('grupa_pr_id', 'grupa', 'web_b2c_prikazi', 'parrent_grupa_pr_id')->where('parrent_grupa_pr_id',$grupa_pr_id)->orderBy('redni_broj', 'asc')->get();
    }

    public static function category_slug($grupa_pr_id,$slug=''){
        $grupa = DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->first();
        if(!is_null($grupa)){
        	$slug = Url_mod::slug_trans($grupa->grupa).'/'.$slug;
        	if($grupa->parrent_grupa_pr_id > 0){
        		$slug = self::category_slug($grupa->parrent_grupa_pr_id,$slug);
        	}
        }
        return $slug;
    }

    public static function category_link($grupa_pr_id){
    	return Options::base_url() . self::category_slug($grupa_pr_id);
    }
    public static function group_link($group_id,$lang=null)
	{	$slug = null;
		$grupa = DB::table('grupa_pr')->where('grupa_pr_id','>',0)->where(array('grupa_pr_id'=>$group_id,'web_b2c_prikazi'=>1))->first();
		if(!is_null($grupa)){
			$slug = AdminOptions::slug_trans($grupa->grupa,$lang);
			if($grupa->parrent_grupa_pr_id > 0 && $grupa->grupa_pr_id != $grupa->parrent_grupa_pr_id){
				$subSlug = self::group_link($grupa->parrent_grupa_pr_id,$lang);
				if(is_null($subSlug)){
					return null;
				}
				$slug = $subSlug.'/'.$slug;
			}
		}
		return $slug;
	}
}