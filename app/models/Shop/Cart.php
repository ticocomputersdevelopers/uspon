<?php 
use Service\Drip;

    class Cart {
    
        public static function kupac_id(){
            $kupac_id = 0;
            if(Session::has('b2c_kupac')){
                $kupac_id = Session::get('b2c_kupac');
            }
            return $kupac_id; 
        }
        public static function korpa_id(){
            if(!Session::has('b2c_korpa')){
                if(self::kupac_id() != 0){
                    $query = DB::table('web_b2c_korpa')->where(array('web_kupac_id'=>self::kupac_id(),'naruceno'=>0))->orderBy('web_b2c_korpa_id','desc')->first();
                    if($query){
                        $korpa_id = $query->web_b2c_korpa_id;
                    }else{
                        $korpa_id = DB::select("SELECT nextval('web_b2c_korpa_web_b2c_korpa_id_seq') AS korpa_id")[0]->korpa_id;
                    }
                }else{
                    $korpa_id = DB::select("SELECT nextval('web_b2c_korpa_web_b2c_korpa_id_seq') AS korpa_id")[0]->korpa_id;
                }
                Session::put('b2c_korpa',$korpa_id);
            }else{
                DB::table('web_b2c_korpa')->where(array('web_b2c_korpa_id'=>Session::get('b2c_korpa')))->update(array('web_kupac_id'=>self::kupac_id()));
            }
            return Session::get('b2c_korpa');
        }

        public static function kolicina_lager($roba_id){
            $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
            //$orgj_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
            $query = DB::table('lager')->where(array('roba_id'=>$roba_id))->whereNotIn('orgj_id',[25])->sum('kolicina');
            $lager = 0;
            if($query){
                $lager = $query;
            }
            if($query && Options::vodjenje_lagera()==1 && Options::web_options(131)==1){
                $lager -= $query;
            }
            return $lager > 0 ? $lager : 0;
        }

        public static function kolicina_korpa($roba_id,$korpa_id){
            $kolicina = 0;
            $query_korpa = DB::select("SELECT SUM(kolicina) as ukupno FROM web_b2c_korpa_stavka WHERE roba_id=".$roba_id." AND web_b2c_korpa_id=".$korpa_id." GROUP BY roba_id");
            if(count($query_korpa) > 0){
                $kolicina = $query_korpa[0]->ukupno;
            }
            return $kolicina;
        }

        public static function check_avaliable($roba_id){
            if(Options::vodjenje_lagera()==0){ 
                return 1000;
            }
            $korpa = 0;
            if(Session::has('b2c_korpa')){
                $korpa =self::kolicina_korpa($roba_id,self::korpa_id());
            }

            return self::kolicina_lager($roba_id) - $korpa;
        }
        public static function add_to_cart($kupac_id,$korpa_id,$roba_id,$kolicina,array $osobine,$parent_vezani_id=null,$kamata=0,$project_id=null){
            $exist = DB::table('web_b2c_korpa')->where('web_b2c_korpa_id',$korpa_id)->count() > 0;
            if(!$exist){
                DB::table('web_b2c_korpa')->insert(array('web_b2c_korpa_id'=>$korpa_id,'web_kupac_id'=>$kupac_id,'datum'=>date("Y-m-d")));
            }
            self::add_stavka($korpa_id,$roba_id,$kolicina,$osobine,$parent_vezani_id,$kamata,$project_id);

            if(Config::get('app.livemode') && Options::gnrl_options(3060) == 1 && $kupac_id > 0){
                $drip = new Drip();
                $dbcart = DB::table('web_b2c_korpa')->where('web_b2c_korpa_id',$korpa_id)->first();
                if($exist){
                    $drip->addOrUpdateCart($dbcart,"updated");
                }else{
                    $drip->addOrUpdateCart($dbcart,"created");
                }
            }
        }

        // public static function cart_ukupno_korpa(){
        //     $valuta=' rsd';
        //     $ukupno=0;
        //     if(Session::has('b2c_korpa')){
        //          foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',self::korpa_id())->get() as $row){
        //            $ukupno+=$row->jm_cena*$row->kolicina;
        //          }
        //     }
        //     $ukupno= number_format($ukupno, 2, ',' , '.');
        //     return $ukupno.$valuta;
        // }

        public static function br_rata($kamata){
            return DB::table('kupovina_na_rate')->where('kamata',$kamata)->pluck('br_meseca');
        }

        public static function add_stavka($korpa_id,$roba_id,$kolicina,array $osobine,$parent_vezani_id=null,$kamata=0,$project_id=null){
            $query = DB::table('web_b2c_korpa_stavka')->where(array('roba_id'=>$roba_id,'web_b2c_korpa_id'=>$korpa_id));

            $first = DB::table('roba')->where('roba_id',$roba_id)->first(); 
            $jm_cena = is_null($parent_vezani_id) ? Product::get_price($roba_id,true,true) : Product::get_price_vezani($parent_vezani_id,$roba_id);
            $jm_cena = $jm_cena * (1+($kamata/100));
            
            $rata = self::br_rata($kamata);

            $data=array(
            'web_b2c_korpa_id'=>$korpa_id,
            'roba_id'=>$roba_id,
            'broj_stavke'=>1,
            'kolicina'=>$kolicina,
            'jm_cena'=> $jm_cena,
            'br_rata'=> $rata,
            'project_id'=> $project_id,
            'tarifna_grupa_id'=>$first->tarifna_grupa_id,
            'racunska_cena_nc'=>$first->racunska_cena_nc,
            'parent_vezani_roba_id'=> !is_null($parent_vezani_id) ? intval($parent_vezani_id) : null
            );

            if(!Product::check_osobine($roba_id)){
                if($query->count() == 0){
                    DB::table('web_b2c_korpa_stavka')->insert($data);
                }else{
                    $first = $query->first();
                    $stavka_id = $first->web_b2c_korpa_stavka_id;
                    if($first->project_id != $project_id){
                        DB::table('web_b2c_korpa_stavka')->insert($data);
                    }else{                    
                        if(Options::web_options(320) == 0){
                            $new_kol = $first->kolicina + intval($kolicina);
                        }else{
                            $new_kol = $first->kolicina + $kolicina;
                        }
                        DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->update(array('kolicina'=>$new_kol,'parent_vezani_roba_id'=>$data['parent_vezani_roba_id'],'jm_cena'=>$data['jm_cena']));
                    }
                }
            }else{
                if($query->count() == 0){
                    if(count($osobine) > 0){
                        $data['osobina_vrednost_ids'] = implode('-',$osobine);
                    }
                    DB::table('web_b2c_korpa_stavka')->insert($data);
                }else{
                    if(count($osobine) <= 0){
                        $last_savka = $query->orderBy('web_b2c_korpa_stavka_id','desc')->first();
                        $stavka_id = $last_savka->web_b2c_korpa_stavka_id;
                        if($last_savka->project_id != $project_id){
                            DB::table('web_b2c_korpa_stavka')->insert($data);
                        }else{  
                            if(Options::web_options(320) == 0){
                                $kolicina = intval($kolicina);
                            }
                            $new_kol = $first->kolicina + $kolicina;

                            DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->update(array('kolicina'=>$new_kol,'parent_vezani_roba_id'=>$data['parent_vezani_roba_id'],'jm_cena'=>$data['jm_cena']));
                        }
                    }else{

                        $stavka_id = null;
                        $stavka = null;
                        foreach($query->get() as $row){
                            if($row->osobina_vrednost_ids != null && $row->osobina_vrednost_ids != ''){
                                $vrednosti = explode('-',$row->osobina_vrednost_ids);
                                if(count(array_diff($vrednosti,$osobine))==0 && count(array_diff($osobine,$vrednosti))==0){
                                    $stavka_id = $row->web_b2c_korpa_stavka_id;
                                    $stavka = $row;
                                    break;
                                }
                            }
                        }

                        if($stavka_id == null){
                            $data['osobina_vrednost_ids'] = implode('-',$osobine);
                            DB::table('web_b2c_korpa_stavka')->insert($data);
                        }else{
                            if($stavka->project_id != $project_id){
                                DB::table('web_b2c_korpa_stavka')->insert($data);
                            }else{ 
                                if(Options::web_options(320) == 0){
                                    $kolicina = intval($kolicina);
                                }
                                $new_kol = DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->pluck('kolicina') + $kolicina;

                                DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$stavka_id)->update(array('kolicina'=>$new_kol,'parent_vezani_roba_id'=>$data['parent_vezani_roba_id'],'jm_cena'=>$data['jm_cena']));
                            }                     
                        }
                    }
                }
            }

            // self::rezervacija($roba_id,$kolicina);
        }

        // public static function rezervacija($roba_id,$kolicina){
        //     if(Options::vodjenje_lagera()==1 && Options::web_options(131)==1){
        //         $poslovna_godina_id = DB::table('poslovna_godina')->where('status',0)->pluck('poslovna_godina_id');
        //         $orgj_id = DB::table('imenik_magacin')->where('izabrani',1)->pluck('orgj_id');
        //         $query = DB::table('lager')->where(array('roba_id'=>$roba_id,'poslovna_godina_id'=>$poslovna_godina_id,'orgj_id'=>$orgj_id));
        //         $new_kolicina = ($query->first()->kolicina - $kolicina) >= 0 ? ($query->first()->kolicina - $kolicina) : 0;
        //         $new_rezervisano = ($query->first()->rezervisano + $kolicina) >= 0 ? ($query->first()->rezervisano + $kolicina) : 0;
        //         $query->update(array('kolicina'=>$new_kolicina,'rezervisano'=>$new_rezervisano));
        //     }   
        // }

        public static function check_avaliable_vezani($kolicina,$roba_id,$parent_vezani_roba_id=null){
            if(!is_null($parent_vezani_roba_id)){
                $korpa_id = self::korpa_id();
                $kolicina_vezani = DB::table('web_b2c_korpa_stavka')->where(array('parent_vezani_roba_id'=>$parent_vezani_roba_id,'roba_id'=>$roba_id,'web_b2c_korpa_id'=>$korpa_id))->pluck('kolicina');
                $kolicina_vezani = !is_null($kolicina_vezani) ? $kolicina_vezani : 0;
                $kolicina_glavni = DB::table('web_b2c_korpa_stavka')->where(array('roba_id'=>$parent_vezani_roba_id,'web_b2c_korpa_id'=>$korpa_id))->pluck('kolicina');
                $kolicina_glavni = !is_null($kolicina_glavni) ? $kolicina_glavni : 0;
                return ($kolicina_vezani + $kolicina) <= $kolicina_glavni;
            }
            return true;
        }

        public static function broj_cart(){
            $ukupno=0;
            if(Options::web_options(320) == 0){
                if(Session::has('b2c_korpa')){
                    foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',self::korpa_id())->get() as $row){
                        $ukupno+=$row->kolicina;
                    }
                }
                return $ukupno;
            }else{
                if(Session::has('b2c_korpa')){
                    $ukupno=DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',self::korpa_id())->count(); 
                }
                return $ukupno;
            }
        }    

        public static function cena($cena,$valuta_show = true){
            $decimale = Options::web_options(209);

            $valuta = null;

            // if(Session::has('valuta') && is_numeric(Session::get('valuta'))){
            //     $valuta = DB::table('valuta')->where('valuta_id',Session::get('valuta'))->first();
            // }else{
            //     $valuta = DB::table('valuta')->where('izabran',1)->first();
            //     Session::put('valuta',$valuta->valuta_id);
            // }

            // only RSD
            $valuta = DB::table('valuta')->where('izabran',1)->first();
            Session::put('valuta',$valuta->valuta_id);

            $kurs=Options::kurs($valuta->valuta_id);
            $valutaLabel = ' <span class="global-currency"> '.strtolower($valuta->valuta_sl).'.</span>';
            if($cena!="0"){            
                $iznos = number_format(round(floatval($cena/$kurs),$decimale), 2, ',', '.');
            }else{
                $iznos = '0.00';
            }

            if($valuta_show == true){
                return $iznos.$valutaLabel;
            }else{
                return $iznos;
            }
        }

        // public static function cart_ukupno(){
        //     $ukupno=0;
        //     $active_coupons = DB::table('vaucer_cart')->select('vaucer_id')->where('web_b2c_korpa_id', self::korpa_id())->get();
        //     // var_dump($active_coupons); die;
        //     if(Session::has('b2c_korpa')){
        //         foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',self::korpa_id())->get() as $row){
        //             $ukupno+=$row->jm_cena*$row->kolicina;
        //             if($active_coupons) {
        //                 foreach(self::getActiveCoupons(self::korpa_id()) as $row1){
        //                     foreach(self::getCoupons($row1->vaucer_id) as $coupon){
        //                         $group = DB::table('roba')->where('roba_id', $row->roba_id)->pluck('grupa_pr_id');
        //                         if($group == $coupon->grupa_pr_id) {
        //                             $iznos = $coupon->iznos;
        //                             // Akcija
        //                             if($coupon->vaucer_tip_id == 1) {
        //                                 $ukupno -= $iznos*$row->kolicina;
        //                             }
        //                             // Popust
        //                             if($coupon->vaucer_tip_id == 2) {
        //                                 $ukupno -= ($ukupno * $iznos * 0.01)*$row->kolicina;
        //                             }
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //         if($active_coupons) {
        //             foreach(self::getActiveCoupons(self::korpa_id()) as $row){
        //                 foreach(self::getCoupons($row->vaucer_id) as $coupon){
        //                     if(!$coupon->grupa_pr_id) {
        //                         $iznos = $coupon->iznos;
        //                         // Akcija
        //                         if($coupon->vaucer_tip_id == 1) {
        //                             $ukupno -= $iznos;
        //                         }
        //                         // Popust
        //                         if($coupon->vaucer_tip_id == 2) {
        //                             $ukupno -= ($ukupno * $iznos * 0.01);
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        //     if(AdminOptions::web_options(152)==1){
        //         return ceil( $ukupno/10 )*10;
        //     }else{
        //         return $ukupno;   
        //     }
        // }

        public static function cart_ukupno(){
            $ukupno=0;
            $active_coupons = DB::table('vaucer_cart')->select('vaucer_id')->where('web_b2c_korpa_id', self::korpa_id())->get();
            // var_dump($active_coupons); die;
            if(Session::has('b2c_korpa')){
                foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',self::korpa_id())->get() as $row){
                    $cena = Product::get_price($row->roba_id);
                    $ukupno+=$cena*$row->kolicina;
                }
                if($active_coupons) {
                    foreach(self::getActiveCoupons(self::korpa_id()) as $row){
                        foreach(self::getCoupons($row->vaucer_id) as $coupon){
                            $iznos = $coupon->iznos;
                            // Akcija
                            if($coupon->vaucer_tip_id == 1) {
                                $ukupno -= $iznos;
                            }
                            // Popust
                            if($coupon->vaucer_tip_id == 2) {
                                $ukupno -= ($ukupno * $iznos * 0.01);
                            }
                        }
                    }
                }
            }
            if(AdminOptions::web_options(152)==1){
                return ceil( $ukupno/10 )*10;
            }else{
                return $ukupno;   
            }
        }

        // public static function get_vaucer_popust(){
        //     $ukupno=0;
        //     $ukupan_popust=0;
        //     $active_coupons = DB::table('vaucer_cart')->select('vaucer_id')->where('web_b2c_korpa_id', self::korpa_id())->get();
        //     // var_dump($active_coupons); die;
        //     if(Session::has('b2c_korpa')){
        //         foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',self::korpa_id())->get() as $row){
        //             $ukupno+=$row->jm_cena*$row->kolicina;
        //         }
        //         if($active_coupons) {
        //             foreach(self::getActiveCoupons(self::korpa_id()) as $row){
        //                 foreach(self::getCoupons($row->vaucer_id) as $coupon){
        //                     $iznos = $coupon->iznos;
        //                     // Akcija
        //                     if($coupon->vaucer_tip_id == 1) {
        //                         $ukupan_popust += $iznos;
        //                     }
        //                     // Popust
        //                     if($coupon->vaucer_tip_id == 2) {
        //                         $ukupan_popust += ($ukupno * $iznos * 0.01);
        //                     }
        //                 }
        //             }
        //         }
        //     }
        //     return $ukupan_popust;
        // }

        public static function cart_ukupno_bez_vaucera(){
            $ukupno=0;
            if(Session::has('b2c_korpa')){
                  foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',self::korpa_id())->get() as $row){
                    $ukupno+=$row->jm_cena*$row->kolicina;
                  }
            }
            if(AdminOptions::web_options(152)==1){
                return ceil( $ukupno/10 )*10;
            }else{
                return $ukupno;   
            }
        }

        public static function get_vaucer_popust(){
            $ukupno = self::cart_ukupno_bez_vaucera();
            $popust = self::cart_ukupno();
            $ukupan_popust = $ukupno - $popust;
            return $ukupan_popust;
        }

        public static function osobina_kombinacija_check_available($roba_id,$osobine_arr,$korpa_id=null,$stavka_id=null){
            if(Options::vodjenje_lagera()==0){ 
                return 1000;
            }            
            $osobina_kombinacija = null;
            foreach(DB::table('osobina_kombinacija')->where(array('roba_id'=>$roba_id,'aktivna'=>1))->get() as $kombinacija){
                $osobina_kombinacija_ids = array_map('current',DB::table('osobina_kombinacija_vrednost')->select('osobina_vrednost_id')->where(array('osobina_kombinacija_id'=>$kombinacija->osobina_kombinacija_id))->get());
                if(count(array_diff($osobina_kombinacija_ids,$osobine_arr)) == 0 && count(array_diff($osobine_arr,$osobina_kombinacija_ids)) == 0){
                    $osobina_kombinacija = $kombinacija;
                    break;
                }
            }

            $kolicina_korpa = 0;
            $query_uslov = array('roba_id'=>$roba_id);
            if(!is_null($korpa_id)){
                $query_uslov['web_b2c_korpa_id'] = $korpa_id;
            }
            if(!is_null($stavka_id)){
                $query_uslov['web_b2c_korpa_stavka_id'] = $stavka_id;
            }
            foreach(DB::table('web_b2c_korpa_stavka')->where($query_uslov)->get() as $stavka){
                $stavka_vrednosti_ids = explode('-',$stavka->osobina_vrednost_ids);
                if(count(array_diff($stavka_vrednosti_ids,$osobine_arr)) == 0 && count(array_diff($osobine_arr,$stavka_vrednosti_ids)) == 0){
                    $kolicina_korpa = $stavka->kolicina;
                    break;
                }                       
            }

            return !is_null($osobina_kombinacija) ? ($osobina_kombinacija->kolicina - $kolicina_korpa) : 0;
        }

        public static function troskovi($web_b2c_narudzbina_id=null,$roba_id=null){
            $tezinaSum=0;
            $cenaSum=0;
            $troskoviPoKategoriji = 0;
            if(is_null($roba_id)){
                if(!is_null($web_b2c_narudzbina_id)){
                    foreach(DB::table('web_b2c_narudzbina_stavka')->where('web_b2c_narudzbina_id',$web_b2c_narudzbina_id)->get() as $row){
                        $tezinaSum+=DB::table('roba')->where('roba_id',$row->roba_id)->pluck('tezinski_faktor')*$row->kolicina;
                        $cenaSum+=$row->jm_cena*$row->kolicina;

                        if(DB::table('roba')->where('roba_id',$row->roba_id)->pluck('grupa_pr_id') == 3528 && $row->jm_cena > 10000) {
                            $troskoviPoKategoriji += $row->kolicina*1000;
                        }
                    }
                }else if(Session::has('b2c_korpa')){
                    foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',self::korpa_id())->get() as $row){
                        $tezinaSum+=DB::table('roba')->where('roba_id',$row->roba_id)->pluck('tezinski_faktor')*$row->kolicina;
                        $cenaSum+=$row->jm_cena*$row->kolicina;

                        if(DB::table('roba')->where('roba_id',$row->roba_id)->pluck('grupa_pr_id') == 3528 && $row->jm_cena > 10000) {
                            $troskoviPoKategoriji += $row->kolicina*1000;
                        }
                    }
                }
            }else{
                $tezinaSum+=DB::table('roba')->where('roba_id',$roba_id)->pluck('tezinski_faktor');
                $cenaSum+=DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena');

                if(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id') == 3528 && DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena') > 10000) {
                    $troskoviPoKategoriji += 1000;
                }
            }

            $troskoviSum = 0;
            $troskoviTezinaObj = (object) array('cena'=>0,'tezina_gr'=>0,'cena_do'=>0);
            $troskoviCenaObj = (object) array('cena'=>0,'tezina_gr'=>0,'cena_do'=>0);

            foreach(DB::table('web_troskovi_isporuke')->orderBy('tezina_gr','desc')->get() as $row) {
                if($tezinaSum > 0 && $tezinaSum >= $row->tezina_gr){
                    $troskoviTezinaObj = $row;
                    break;
                }
            }

            foreach(DB::table('web_troskovi_isporuke')->orderBy('cena_do','desc')->get() as $row) {
                if($cenaSum < $row->cena_do){
                    $troskoviCenaObj = $row;
                }
            }
            
            //samo tezina
            if(AdminOptions::web_options(133)==1 && AdminOptions::web_options(150)==0){
                $troskoviSum = $troskoviTezinaObj->cena;
            //samo cena
            }else if(AdminOptions::web_options(150)==1 && AdminOptions::web_options(133)==0){
                $troskoviSum = $troskoviCenaObj->cena + $troskoviPoKategoriji;
            //kombinacija cene i tezine
            }else if(AdminOptions::web_options(150)==1 && AdminOptions::web_options(133)==1){
                if($troskoviTezinaObj->cena == $troskoviCenaObj->cena){
                    $troskoviSum = $troskoviTezinaObj->cena;
                }else{
                    $troskoviSum = max(array($troskoviTezinaObj->cena,$troskoviCenaObj->cena));
                    // if($troskoviTezinaObj->tezina_gr < $troskoviCenaObj->tezina_gr || $troskoviCenaObj->cena_do < $troskoviTezinaObj->cena_do){
                    //     $troskoviSum = min(array($troskoviTezinaObj->cena,$troskoviCenaObj->cena));
                    // }else{
                    //     $troskoviSum = max(array($troskoviTezinaObj->cena,$troskoviCenaObj->cena));
                    // }
                }
            }

            if(!is_null($web_b2c_narudzbina_id)){
                $besplatna_dostava = DB::table('web_b2c_narudzbina')->where('web_b2c_narudzbina_id', $web_b2c_narudzbina_id)->pluck('besplatna_dostava');
                if($besplatna_dostava) {
                    return 0;
                } else {
                    return $troskoviSum;
                }
            }else if(Session::has('b2c_korpa')) {
                $besplatna_dostava = DB::table('web_b2c_korpa')->where('web_b2c_korpa_id', self::korpa_id())->pluck('besplatna_dostava');
                if($besplatna_dostava) {
                    return 0;
                } else {
                    return $troskoviSum;
                }
            }
            
            // return $troskoviSum;
        }

        // public static function troskovi_isporuke(){
        //     $ukupno=0;
        //     if(Session::has('b2c_korpa')){
        //           foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',self::korpa_id())->get() as $row){
        //             $ukupno+=DB::table('roba')->where('roba_id',$row->roba_id)->pluck('tezinski_faktor')*$row->kolicina;
        //           }
        //     }
        //     $cena = 0;
        //     $obj=DB::table('web_troskovi_isporuke')->orderBy('web_troskovi_isporuke_id','desc')->first();
        //     if(isset($obj)){
        //         $cena = $obj->cena;
        //     }
        //     foreach (DB::table('web_troskovi_isporuke')->orderBy('web_troskovi_isporuke_id','desc')->get() as $row) {
        //         if(round($ukupno) <= $row->tezina_gr){
        //             $cena = $row->cena;
        //         }
        //     }
        //     return $cena;
        // } 

        public static function cena_dostave(){
            $cena_isporuke = DB::table('cena_isporuke')->first();
            return !is_null($cena_isporuke) ? $cena_isporuke->cena : 0;
        }
        public static function cena_do(){
            $cena_isporuke = DB::table('cena_isporuke')->first();
            return !is_null($cena_isporuke) ? $cena_isporuke->cena_do : 0;
        }
        public static function add_to_wish($kupac_id,$roba_id){
            $item = DB::table('lista_zelja')->where(array('roba_id'=>$roba_id,'web_kupac_id'=>$kupac_id))->first();
            if(is_null($item)){
                DB::table('lista_zelja')->insert(array('roba_id'=>$roba_id,'web_kupac_id'=>$kupac_id));
                return true;
            }
            return false;
        }
        public static function delete_wish($roba_id){
            DB::table('lista_zelja')->where('roba_id',$roba_id)->delete();        
        }
        public static function broj_wish(){
            if(Session::has('b2c_kupac')){
                return DB::table('lista_zelja')->where('web_kupac_id',Session::get('b2c_kupac'))->count();
            }
            else {
                return 0;
            }
        }   

        // public static function api_posta($posta_slanje_id){
        //     $posta_slanje = DB::table('posta_slanje')->where(array('posta_slanje_id'=>$posta_slanje_id, 'api_aktivna' => 1))->first();
        //     if(is_null($posta_slanje)){
        //         return false;
        //     }else{
        //         return true;
        //     }
        // }

        // public static function opstine(){
        //     return DB::table('narudzbina_opstina')->orderBy('naziv','asc')->get();
        // }

        // public static function mesta($opstina_old_id){
        //     return DB::table('narudzbina_mesto')->where('narudzbina_opstina_code',DB::table('narudzbina_opstina')->where('narudzbina_opstina_id',($opstina_old_id ? $opstina_old_id : DB::table('narudzbina_opstina')->orderBy('naziv','asc')->first()->narudzbina_opstina_id))->orderBy('naziv','asc')->first()->code)->orderBy('naziv','asc')->get();
        // }

        // public static function ulice($mesto_old_id){
        //     return DB::table('narudzbina_ulica')->where('narudzbina_mesto_code',DB::table('narudzbina_mesto')->where('narudzbina_mesto_id',($mesto_old_id ? $mesto_old_id : DB::table('narudzbina_mesto')->where('narudzbina_opstina_code',DB::table('narudzbina_opstina')->where('narudzbina_opstina_id',DB::table('narudzbina_opstina')->orderBy('naziv','asc')->first()->narudzbina_opstina_id)->orderBy('naziv','asc')->first()->code)->orderBy('naziv','asc')->first()->narudzbina_mesto_id))->orderBy('naziv','asc')->first()->code)->orderBy('naziv','asc')->get();
        // }
        
        public static function mesto($ulica_id){
            $ulica = self::ulica($ulica_id);
            if(!is_null($ulica)){
                $mesto = DB::table('narudzbina_mesto')->where('code',$ulica->narudzbina_mesto_code)->first();
                if(!is_null($mesto)){
                    return $mesto;
                }
            }
            return null;
        }
        public static function opstina($ulica_id){
            $mesto = self::mesto($ulica_id);
            if(!is_null($mesto)){
                $opstina = DB::table('narudzbina_opstina')->where('code',$mesto->narudzbina_opstina_code)->first();
                if(!is_null($opstina)){
                    return $opstina;
                }
            }
            return null;
        }
        public static function ulica($ulica_id){
            return DB::table('narudzbina_ulica')->where('narudzbina_ulica_id',$ulica_id)->first();
        }

        // public static function mesto_id($ulica_id){
        //     $mesto = self::mesto($ulica_id);
        //     if(!is_null($mesto)){
        //         return $mesto->narudzbina_mesto_id;
        //     }
        //     return 0;
        // }

        // public static function opstina_id($ulica_id){
        //     $opstina = self::opstina($ulica_id);
        //     if(!is_null($opstina)){
        //         return $opstina->narudzbina_opstina_id;
        //     }
        //     return 0;
        // }
        
        public static function kamata($web_b2c_korpa_id){
            return DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',$web_b2c_korpa_id)->pluck('br_rata');
        }


        public static function bodoviPopustOpseg(){
            $opseg = array();
            $bodovi_popust = DB::table('bodovi_popust')->orderBy('bodovi_popust_id','asc')->get();
            foreach($bodovi_popust as $key => $popust){
                $opseg[] = (object) array(
                    'web_cena_od' => ($key == 0) ? 0 : $bodovi_popust[$key-1]->web_cena,
                    'web_cena' => $popust->web_cena,
                    'popust' => $popust->popust
                );
            }
            return $opseg;
        }
        public static function findBodoviPopust($web_cena,$opseg=null){
            if(is_null($opseg)){
                $opseg = self::bodoviPopustOpseg();
            }
            if(count($opseg) == 0){
                return (object) array('web_cena_od' => 0,'web_cena' => 0,'popust' => 100);
            }
            foreach($opseg as $row){
                if($web_cena < $row->web_cena){
                    return $row;
                    break;
                }
            }
            return $opseg[count($opseg)-1];
        }
        public static function bodoviPopustCenaArtikla($roba_id,$opseg=null){
            $roba = DB::table('roba')->where('roba_id',$roba_id)->first();
            if($roba->bodovi_popust == 0 || All::provera_akcija($roba_id) == 1){
                return 0;
            }
            $popust = self::findBodoviPopust($roba->web_cena,$opseg)->popust;

            return ($roba->web_cena * ($popust/100));
        }
        public static function bodoviPopustCenaKorpa($opseg=null){
            $popustCena = 0;
            foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',self::korpa_id())->get() as $stavka){
                $popustCena += self::bodoviPopustCenaArtikla($stavka->roba_id,$opseg)*$stavka->kolicina;
            }
            return $popustCena;
        }
        public static function bodoviPopustBodoviKorpa($opseg=null){
            if(Options::web_options(314) == 0){
                return 0;
            }
            $bodoviPopustCenaKorpa = self::bodoviPopustCenaKorpa($opseg);
            $vrednostBoda = Options::web_options(317);
            $maksimalniBrojBodova = Options::web_options(315);
            $bodoviPopust = $vrednostBoda > 0 ? intdiv($bodoviPopustCenaKorpa,$vrednostBoda) : 0;

            if($maksimalniBrojBodova > 0 && $maksimalniBrojBodova < $bodoviPopust){
                return $maksimalniBrojBodova;
            }
            return $bodoviPopust;
        }

        public static function bodoviOstvareniOpseg(){
            $opseg = array();
            $bodovi_ostvareni = DB::table('bodovi_ostvareni')->orderBy('bodovi_ostvareni_id','asc')->get();
            foreach($bodovi_ostvareni as $key => $row){
                $opseg[] = (object) array(
                    'web_cena_od' => ($key == 0) ? 0 : $bodovi_ostvareni[$key-1]->web_cena,
                    'web_cena' => $row->web_cena,
                    'broj_bodova' => $row->broj_bodova
                );
            }
            return $opseg;
        }
        public static function findBodoviOstvareni($web_cena,$opseg=null){
            if(is_null($opseg)){
                $opseg = self::bodoviOstvareniOpseg();
            }
            if(count($opseg) == 0){
                return (object) array('web_cena_od' => 0,'web_cena' => 0,'broj_bodova' => 0);
            }
            foreach($opseg as $row){
                if($web_cena < $row->web_cena){
                    return $row;
                    break;
                }
            }
            return $opseg[count($opseg)-1];
        }
        public static function bodoviOstvareniBodoviKorpa($opseg=null){
            if(Options::web_options(314) == 0){
                return 0;
            }
            $bodoviOstvareni = 0;
            foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',self::korpa_id())->get() as $stavka){
                $roba = DB::table('roba')->where('roba_id',$stavka->roba_id)->first();
                if($roba->bodovi_popust == 1 && All::provera_akcija($stavka->roba_id)==0){
                    $bodoviOstvareni += self::findBodoviOstvareni($roba->web_cena,$opseg)->broj_bodova*$stavka->kolicina;
                }
            }
            $maksimalniBrojBodova = Options::web_options(315)-WebKupac::bodovi();

            if($maksimalniBrojBodova > 0 && $maksimalniBrojBodova < $bodoviOstvareni){
                return $maksimalniBrojBodova;
            }
            return $bodoviOstvareni;
        }


        public static function vauceriPopustOpseg(){
            $opseg = array();
            $vauceri_popust = DB::table('vauceri_popust')->orderBy('vauceri_popust_id','asc')->get();
            foreach($vauceri_popust as $key => $popust){
                $opseg[] = (object) array(
                    'web_cena_od' => ($key == 0) ? 0 : $vauceri_popust[$key-1]->web_cena,
                    'web_cena' => $popust->web_cena,
                    'popust' => $popust->popust
                );
            }
            return $opseg;
        }
        public static function findVauceriPopust($web_cena,$opseg=null){
            if(is_null($opseg)){
                $opseg = self::vauceriPopustOpseg();
            }
            if(count($opseg) == 0){
                return (object) array('web_cena_od' => 0,'web_cena' => 0,'popust' => 100);
            }
            foreach($opseg as $row){
                if($web_cena < $row->web_cena){
                    return $row;
                    break;
                }
            }
            return $opseg[count($opseg)-1];
        }
        public static function vauceriPopustCenaArtikla($roba_id,$opseg=null){
            $roba = DB::table('roba')->where('roba_id',$roba_id)->first();
            if($roba->bodovi_popust == 0 || All::provera_akcija($roba_id) == 1){
                return 0;
            }
            $popust = self::findVauceriPopust($roba->web_cena,$opseg)->popust;

            return ($roba->web_cena * ($popust/100));
        }
        public static function vauceriPopustCenaKorpa($opseg=null){
            if(Options::web_options(318) == 0){
                return 0;
            }
            $popustCena = 0;
            foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',self::korpa_id())->get() as $stavka){
                $popustCena += self::vauceriPopustCenaArtikla($stavka->roba_id,$opseg)*$stavka->kolicina;
            }
            return $popustCena;
        }

        public static function currentPriceForCart(stdClass $cartItem){
            $jm_cena = is_null($cartItem->parent_vezani_roba_id) ? Product::get_price($cartItem->roba_id,true,true) : Product::get_price_vezani($cartItem->parent_vezani_roba_id,$cartItem->roba_id);
            $jm_cena = $jm_cena * (1+(DB::table('kupovina_na_rate')->where('br_meseca',$cartItem->br_rata)->pluck('kamata')/100));

            return $jm_cena;
        }

        public static function checkCartAvailableAndPrices($korpa_id){
            $pass = true;
            foreach(DB::select("SELECT * FROM web_b2c_korpa_stavka WHERE web_b2c_korpa_id = ".$korpa_id."") as $cartItem){
                if($cartItem->kolicina <= 0){
                    $pass=false;
                    DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$cartItem->web_b2c_korpa_stavka_id)->delete();
                }
                $available = self::kolicina_lager($cartItem->roba_id);
                if($cartItem->kolicina > $available){
                    $pass=false;
                    if($available > 0){
                        DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$cartItem->web_b2c_korpa_stavka_id)->update(array('kolicina'=>$available));
                    }else{
                        DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$cartItem->web_b2c_korpa_stavka_id)->delete();
                    }
                }

                $currentPriceForCart = self::currentPriceForCart($cartItem);
                if($currentPriceForCart <> $cartItem->jm_cena){
                    $pass=false;
                    DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_stavka_id',$cartItem->web_b2c_korpa_stavka_id)->update(array('jm_cena'=>$currentPriceForCart));
                }
            }

            return $pass;
        }
        public static function getActiveCoupons($korpa_id) {
            $aktive_coupons = DB::table('vaucer_cart')->select('vaucer_id')->where('web_b2c_korpa_id', $korpa_id)->get();
            return $aktive_coupons;
        }
        public static function getCoupons($vaucer_id) {
            $coupons = DB::table('vaucer')->select('vaucer_tip_id', 'iznos', 'vaucer_broj', 'web_nacin_placanja_id')->where('vaucer_id', $vaucer_id)->get();
            return $coupons;
        }
        public static function getCouponType($vaucer_tip_id) {
            $couponType = DB::table('vaucer_tip')->where('vaucer_tip_id', $vaucer_tip_id)->pluck('vaucer_tip');
            return $couponType;
        }
        public static function getCouponPaymentType($vaucer_id) {
            $couponPaymentType_id = DB::table('vaucer')->where('vaucer_id', $vaucer_id)->pluck('web_nacin_placanja_id');
            $couponPaymentType = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id', $couponPaymentType_id)->pluck('naziv');
            return '- '.$couponPaymentType;
        }
} 