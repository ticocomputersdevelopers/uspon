<?php 
class Support {

    public static function regex(){
        return '/(^[A-Za-z0-9\ \!\%\&\(\)\=\*\/\,\.\+\-\_\@\?\:\;\<\>\'\"\č\ć\ž\š\đ\Č\Ć\Ž\Š\Đ\а\б\в\г\д\ђ\е\ж\з\и\ј\к\л\љ\м\н\њ\о\п\р\с\т\ћ\у\ф\х\ц\ч\џ\ш\А\Б\В\Г\Д\Ђ\Е\Ж\З\И\Ј\К\Л\Љ\М\Н\Њ\О\П\Р\С\Т\Ћ\У\Ф\Х\Ц\Ч\Џ\Ш]+$)+/';
    }
    
    public static function manufacturer_categories($proizvodjac_id, $check=null)
    {
        $query=DB::select("SELECT r.grupa_pr_id, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id) as grupa FROM roba r ".Product::checkImage('join').Product::checkCharacteristics('join')." 
        WHERE r.proizvodjac_id = ".$proizvodjac_id." AND r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND r.grupa_pr_id > 0 ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."ORDER BY grupa_pr_id ASC");

        // $query = DB::table('roba')->select('grupa_pr.grupa_pr_id','grupa_pr.grupa')->join('grupa_pr', 'roba.grupa_pr_id', '=', 'grupa_pr.grupa_pr_id')->where('roba.proizvodjac_id', $proizvodjac_id)->where(array('flag_aktivan' => 1,'flag_prikazi_u_cenovniku' => 1))->orderBy('grupa_pr.grupa_pr_id', 'asc')->get();
        $categories=array();

        if($check == 1){
            foreach($query as $row){
                $categories[]=$row->grupa_pr_id;
            }
            return array_unique($categories);           
        }
        
        foreach($query as $row){
            $categories[]=$row->grupa;
        }
        return array_count_values($categories);

    }

    public static function tip_categories($tip_id, $check=null)
    {
        $query=DB::select("SELECT r.grupa_pr_id, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id) as grupa FROM roba r ".Product::checkImage('join').Product::checkCharacteristics('join')." 
        WHERE r.tip_cene = ".$tip_id." AND r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."ORDER BY grupa_pr_id ASC");

        $categories=array();

        if($check == 1){
            foreach($query as $row){
                $categories[]=$row->grupa_pr_id;
            }
            return array_unique($categories);           
        }
        
        foreach($query as $row){
            $categories[]=$row->grupa;
        }
        return array_count_values($categories);

    }

    public static function akcija_categories($check=null)
    {
        $datum = date('Y-m-d');
        $query=DB::select("SELECT distinct r.roba_id, r.grupa_pr_id, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = r.grupa_pr_id) as grupa FROM roba r ".Product::checkLager('join').Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND akcija_flag_primeni = 1 AND CASE WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_od <= '".$datum."' AND datum_akcije_do >= '".$datum."' WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NULL THEN datum_akcije_od <= '".$datum."' WHEN datum_akcije_od IS NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_do >= '".$datum."' ELSE 1 = 1 END ".Product::checkLager().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."ORDER BY grupa_pr_id ASC");

        $categories=array();

        if($check == 1){
            foreach($query as $row){
                $categories[]=$row->grupa_pr_id;
            }
            return array_unique($categories);           
        }
        
        foreach($query as $row){
            $categories[]=$row->grupa;
        }
        return array_count_values($categories);

    }
    public static function theme_path()
    {
        $stil = DB::table('prodavnica_stil')->where('izabrana',1)->first(); 
        return DB::table('prodavnica_tema')->where('prodavnica_tema_id',$stil->prodavnica_tema_id)->pluck('putanja');
    }
    public static function theme_style_path()
    {
        $stil = DB::table('prodavnica_stil')->where('izabrana',1)->first(); 
        return DB::table('prodavnica_tema')->where('prodavnica_tema_id',$stil->prodavnica_tema_id)->pluck('putanja').$stil->putanja;
    }
    // public static function image_theme_path()
    // {
    //     $stil = DB::table('prodavnica_stil')->where('izabrana',1)->first(); 
    //     return 'images/themes/'.DB::table('prodavnica_tema')->where('prodavnica_tema_id',$stil->prodavnica_tema_id)->pluck('putanja').$stil->putanja;
    // }

    // public static function check_admin($sifra=null){
    //     $imenik_id = Session::get('b2c_admin'.AdminOptions::server());
    //     if($imenik_id == 0){
    //         return true;
    //     }
    //     if($sifra == 0){
    //         return false;
    //     }       
    //     $ac_group_id = intval(DB::table('imenik')->where('imenik_id',$imenik_id)->pluck('kvota'));
    //     $ac_module_id = DB::table('ac_module')->where('sifra', $sifra)->pluck('ac_module_id');
    //     $count = DB::table('ac_group_module')->where(array('ac_group_id'=>intval($ac_group_id),'alow'=>1))->where('ac_module_id', $ac_module_id)->count();

    //     if($count > 0){
    //         return true;
    //     }else{
    //         return false;
    //     }
    // }

    public static function checkBrand($roba_id){
        $query = DB::select("SELECT (SELECT brend_prikazi FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) as check_brand FROM roba WHERE roba_id = ".$roba_id."");
        $check = false;
        if(count($query)){
            if($query[0]->check_brand == 1){
                $check = true;
            }
        }
        return $check;
    }

    public static function getBGimg(){
        return DB::table('baneri')->where('tip_prikaza', 3)->pluck('img');
    }

    public static function tip_naziv($tip_id)
    {
        return DB::table('tip_artikla')->where('tip_artikla_id', $tip_id)->pluck('naziv');
    }

    public static function custom_encrypt($string){
        return base64_encode($string);
    }

    public static function custom_decrypt($string){
        return base64_decode($string);
    }

    public static function getFlagCene($all=false)
    {
        if($all){
            return DB::table('roba_flag_cene')->select('roba_flag_cene_id', 'naziv')->where('roba_flag_cene_id', '<>', -1)->orderBy('roba_flag_cene_id','ASC')->get();
        }else{
            return DB::table('roba_flag_cene')->select('roba_flag_cene_id', 'naziv')->where('selected',1)->where('roba_flag_cene_id', '<>', -1)->orderBy('roba_flag_cene_id','ASC')->get();
        }
    }
    public static function getTipovi($active=null)
    {
        if($active){
            return DB::table('tip_artikla')->select('tip_artikla_id','naziv','rbr')->where('tip_artikla_id','>',0)->orderBy('rbr','asc')->get();
        }else{
            return DB::table('tip_artikla')->select('tip_artikla_id','naziv','rbr')->where('active',1)->where('tip_artikla_id','>',0)->orderBy('rbr','asc')->get();
        }  
    } 

    public static function title_akcija(){
        return DB::table('web_b2c_seo')->where('naziv_stranice','akcija')->pluck('seo_title');
    }

    public static function title_blogs(){
        return DB::table('web_b2c_seo')->where('naziv_stranice','blog')->pluck('seo_title');
    }

    public static function title_all_articles(){
        return DB::table('web_b2c_seo')->where('naziv_stranice','pocetna')->pluck('seo_title');
    }
    public static function front_admin_label($id){
        return DB::table('front_admin_labele')->where('front_admin_labele_id',$id)->pluck('labela');
    }
    public static function front_admin_content($id){
        $jezik_id = Language::lang_id();
        return DB::table('front_admin_labele_jezik')->where(array('front_admin_labele_id'=>$id,'jezik_id'=>$jezik_id))->pluck('sadrzaj');
    }
    public static function banca_intesa(){
        return DB::table('web_nacin_placanja')->where('web_nacin_placanja_id', 3 )->pluck('selected') == 1;
    }

    public static function date_convert($date){
        $str_format = date("F j, Y",strtotime($date));
        if(Language::lang() == 'en'){
            return $str_format;
        }else{
            $str_format = str_replace(
                array('January','February','March','May','June','July','August','September','October','November','December'),
                array('Januar','Februar','Mart','Maj','Jun','Jul','Avgust','Septembar','Oktobar','Novembar','Decembar'),
                $str_format);
            return Language::trans($str_format);
        }
    }
    // public static function date_convert_katalog($date){
    //     $str_format = date("j.F.Y",strtotime($date));
    //     if(Language::lang() == 'en'){
    //         return $str_format;
    //     }else{
    //         $str_format = str_replace(
    //             array('January','February','March','Aprile','May','June','July','August','September','October','November','December'),
    //             array('1','2','3','4','5','6','7','8','9','10','11','12'),
    //             $str_format);
    //         return Language::trans($str_format);
    //     }
    // }

    public static function pozadinska_slika(){         
       return DB::table('baneri')->where('tip_prikaza',3)->where('aktivan',1)->get();     
    }
    public static function fileExtension($image_path){
        $extension = 'jpg';
        $slash_parts = explode('/',$image_path);
        $old_name = $slash_parts[count($slash_parts)-1];
        $old_name_arr = explode('.',$old_name);
        $extension = $old_name_arr[count($old_name_arr)-1];
        return $extension;
    }

    public static function anketa($anketa_id){         
       return DB::table('anketa')->where(['anketa_id'=>$anketa_id,'flag_aktivan'=>1])->first();     
    }
    public static function anketaPitanja($anketa_id){         
       return DB::table('anketa_pitanje')->where(['anketa_id'=>$anketa_id,'flag_aktivan'=>1])->orderBy('rbr','asc')->get();     
    }
    public static function anketaOdgovori($anketa_pitanje_id){         
       return DB::table('anketa_odgovor')->where(['anketa_pitanje_id'=>$anketa_pitanje_id,'flag_aktivan'=>1])->orderBy('rbr','asc')->get();     
    }
    // public static function checkOdgovor($anketa_pitanje_id,$webKupacId=0,$narudzbinaId=0){         
    //     $query = DB::table('anketa_odgovor_korisnik')
    //     ->where('anketa_pitanje_id',$anketa_pitanje_id)
    //     ->where(function($q) use ($webKupacId){
    //         $q->where('ip',All::ip_adress())->orWhere('web_kupac_id',!is_null($webKupacId) ? $webKupacId : 0);
    //     });
    //     if($narudzbinaId > 0){
    //         $query = $query->where('web_b2c_narudzbina_id',$narudzbinaId);
    //     }
    //     return $query->first();     
    // }

    public static function checkAnketaKorisnikExists($anketaId,$webKupacId=0,$narudzbinaId=0){         
        $query = DB::table('anketa_odgovor_korisnik')
        ->whereIn('anketa_pitanje_id',array_map('current',DB::table('anketa_pitanje')->select('anketa_pitanje_id')->where('anketa_id',$anketaId)->get()))
        ->where(function($q) use ($webKupacId){
            $q->where('ip',All::ip_adress())->orWhere('web_kupac_id',!is_null($webKupacId) ? $webKupacId : 0);
        });
        if($narudzbinaId > 0){
            $query = $query->where('web_b2c_narudzbina_id',$narudzbinaId);
        }
        return count($query->get()) > 0;     
    }

    // public static function sumaOdgovora($anketa_odgovor_id){
    //     $anketa_odgovor = DB::table('anketa_odgovor')->where('anketa_odgovor_id',$anketa_odgovor_id)->first();

    //     $pitanje_count = DB::table('anketa_odgovor_korisnik')->where('anketa_pitanje_id',$anketa_odgovor->anketa_pitanje_id)->count();
    //     $odgovor_count = DB::table('anketa_odgovor_korisnik')->where('anketa_odgovor_id',$anketa_odgovor_id)->count();

    //     $percent = (100*$odgovor_count) / $pitanje_count;
    //     return (object) array('odgovor_count' => $odgovor_count, 'percent' => $percent);
    // }

    public static function pageSectionContent($page_section_id,$column='sadrzaj'){
        return DB::table('sekcija_stranice_jezik')->where(['sekcija_stranice_id'=>$page_section_id,'jezik_id'=>Language::lang_id()])->pluck($column);
    }
}