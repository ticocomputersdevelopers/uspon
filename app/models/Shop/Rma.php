<?php

class Rma {
    // kupac
    public static function vrsta_kupca($web_b2c_rma_id){
        $vrsta_kupca = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('vrsta_kupca');
        return $vrsta_kupca;
    }
    public static function ime_prezime($web_b2c_rma_id){
        $ime = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('ime');
        $prezime = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('prezime');
        return $ime.' '.$prezime;
    }
    public static function naziv_firme($web_b2c_rma_id){
        $naziv = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('naziv');
        return $naziv;
    }  
    public static function pib_firme($web_b2c_rma_id){
        $naziv = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('pib');
        return $naziv;
    }
    public static function maticni_firme($web_b2c_rma_id){
        $naziv = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('maticni_br');
        return $naziv;
    }
    public static function email($web_b2c_rma_id){
        $email = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('email');
        return $email;
    }
    public static function telefon($web_b2c_rma_id){
        $telefon = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('telefon');
        return $telefon;
    }
    public static function adresa($web_b2c_rma_id){
        $adresa = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('adresa');
        return $adresa;
    }
    public static function mesto($web_b2c_rma_id){
        $mesto = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('mesto');
        return $mesto;
    }
    // kvar
    public static function proizvodjac($web_b2c_rma_id){
        $proizvodjac = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('proizvodjac');
        return $proizvodjac;
    }
    public static function model($web_b2c_rma_id){
        $model = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('model');
        return $model;
    }
    public static function serijski($web_b2c_rma_id){
        $serijski = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('serijski');
        return $serijski;
    }
    public static function opis($web_b2c_rma_id){
        $opis = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('opis');
        return $opis;
    }
    public static function preuzeti($web_b2c_rma_id){
        $preuzeti = DB::table('web_b2c_rma')->where('web_b2c_rma_id',$web_b2c_rma_id)->pluck('preuzeti');
        return $preuzeti;
    }
} 