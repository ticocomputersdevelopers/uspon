<?php 


class Product  {
		
	public static function get_product_id($article_slug){
		
		$artikli = DB::table('roba')->select('roba_id', 'naziv_web')->where(array('flag_prikazi_u_cenovniku'=>1,'flag_aktivan'=>1))->get();
		foreach($artikli as $row){
			if(Url_mod::slugify($row->naziv_web)==$article_slug){
				$roba_id=$row->roba_id;
				break;
			}
		}
		if(!isset($roba_id)){
			$roba_id = 0;
		}
		return $roba_id;
	}

		public static function sifra($roba_id){
		$sifra_d=DB::table('roba')->where('roba_id',$roba_id)->pluck('roba_id');
        return $sifra_d;
	}

	public static function get_sifra($roba_id){        
        foreach(DB::table('roba')->where('roba_id',$roba_id)->get() as $row){            
            return $row->roba_id;
        }
    }
    // public static function id($roba_id){
    // 	$id=DB::table('roba')->where('roba_id',$roba_id)->pluck('roba_id');
    //     return $id;
    // }
    public static function design_id($roba_id){
    	return DB::table('roba')->where('roba_id',$roba_id)->pluck('design_id');
    }
    public static function pitchPrintImageExist($roba_id){
    	$design_id = self::design_id($roba_id);
       	$img_url = 'https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/'.$design_id.'_2.jpg?k=0.4491390433183766';       	
   		if (@getimagesize($img_url)) {
		return TRUE;
		} else {
		return FALSE;
		}	
    }
    // public static function project_id($roba_id){
    // 	return DB::table('web_b2c_korpa_stavka')->where('roba_id',$roba_id)->pluck('project_id');
    // }
    public static function sifra_d($roba_id){
    	$sifra_d=DB::table('roba')->where('roba_id',$roba_id)->pluck('sifra_d');
        return $sifra_d;
    }
    public static function sifra_is($roba_id){
    	$sifra_is=DB::table('roba')->where('roba_id',$roba_id)->pluck('sifra_is');
        return $sifra_is;
    }
    public static function sku($roba_id){
    	$sifra_is=DB::table('roba')->where('roba_id',$roba_id)->pluck('sku');
        return $sifra_is;
    }
	
	public static function seo_description($roba_id){
		
		$product_seo=DB::table('roba')->where('roba_id', $roba_id)->first();
		
		if($product_seo->description!="" ){
			return Seo::grupa_title($product_seo->grupa_pr_id)." ".substr(strip_tags($product_seo->description), 0, 130)." ".Options::company_name();
		}
		elseif($product_seo->web_opis!="" ){
			return Seo::grupa_title($product_seo->grupa_pr_id)." ".substr(strip_tags($product_seo->web_opis), 0, 130)." ".Options::company_name();
		}
		else {
			return Seo::grupa_title($product_seo->grupa_pr_id)." ".strip_tags($product_seo->naziv_web)." ".Options::company_name();
		}
	}
	
	// public static function og_description($roba_id){
		
	// 	$query_seo_desription=DB::table('roba')->where('roba_id', $roba_id)->get();
	
	// 	foreach($query_seo_desription as $row){
		
	// 		$web_description=$row->description;
	// 		$web_opis=$row->web_opis;
	// 		$flag_karakteristike=$row->web_flag_karakteristike;
	// 		$karakteristike=$row->web_karakteristike;
	// 	}
		
		
	// 	if(($web_description=='' or $web_description=="''") and $web_opis!="" ){
	// 		return substr($web_opis, 0, 160);
	// 	}
	// 	else if(($web_description=='' or $web_description=="''") and $web_opis=="" and $flag_karakteristike==0 ) {
	// 		return substr($karakteristike, 0, 160);
	// 	}
	// 	else if(($web_description=='' or $web_description=="''") and $web_opis=="" and $flag_karakteristike==2 ) {
			
	// 		$query_generisane=DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->get();
	// 		foreach($query_generisane as $row){
	// 			$vrednost=$row->vrednost;
	// 			$grupa_pr_naziv_id=$row->grupa_pr_naziv_id;
	// 			$query_karakteristike=DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$grupa_pr_naziv_id)->get();
	// 			foreach ($query_karakteristike as $row2){
					
	// 				return $row2->naziv." ".$vrednost." ";
				
	// 			}
	// 		}
			
			
			
	// 	}
	// 	else if(($web_description=='' or $web_description=="''") and $web_opis=="" and $flag_karakteristike==1 ) {
			
	// 		$query_dobavlajc=DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id',$roba_id)->get();
	// 		foreach($query_dobavlajc as $row){
	// 			$karakteristike_naziv=$row->karakteristika_naziv;
	// 			$karakteristike_vrednost=$row->karakteristika_vrednost;
	// 			return $karakteristike_naziv." ".$karakteristike_vrednost." "; 
	// 		}
			
			
			
	// 	}
	// 	else {
			
	// 		return substr($web_description, 0, 160);
		
	// 	}
	// }
	
	public static function seo_title($roba_id){
		$naziv_web=DB::table('roba')->where('roba_id',$roba_id)->pluck('naziv_web');
		if($naziv_web){
			return str_replace("\"", "''", $naziv_web);
		}
		return '';
	}

	public static function slikabrenda($roba_id){
	return DB::table('proizvodjac')->where('proizvodjac_id', DB::table('roba')->where('roba_id',$roba_id)->pluck('proizvodjac_id'))->pluck('slika');
	}
	public static function stiker_desno($roba_id){
		return DB::table('labela_artikla')->where('labela_artikla_id', DB::table('roba')->where('roba_id',$roba_id)->pluck('labela'))->pluck('slika_desno');
	}
	public static function stiker_levo($roba_id){
		return DB::table('labela_artikla')->where('labela_artikla_id', DB::table('roba')->where('roba_id',$roba_id)->pluck('labela'))->pluck('slika_levo');
	}
	public static function short_title($roba_id,$length=70){
		$query_title=DB::table('roba')->where('roba_id',$roba_id)->get();
		foreach ($query_title as $row){
			if(strlen($row->naziv_web)>$length){
			$title = substr($row->naziv_web,0,($length-3))."...";
			}
			else {
				$title = $row->naziv_web;
			}
		}
		return Language::trans($title);
	}
	public static function get_labela($roba_id){
    	$id=DB::table('roba')->where('roba_id',$roba_id)->pluck('labela');
    	return DB::table('labela_artikla')->where('labela_artikla_id',$id)->pluck('naziv');
    }
	// public static function analitika_title($roba_id){
	// 	$query_title=DB::table('roba')->where('roba_id',$roba_id)->get();

	// 	foreach ($query_title as $row){
	// 		if(strlen($row->naziv_web)>70){
	// 		return str_replace('"','inch',substr($row->naziv_web,0,67))."...";
	// 		}
	// 		else {
	// 			return str_replace('"','inch',$row->naziv_web);
	// 		}
	// 	}
	
	// }	

	public static function get_grupa_title($roba_id){
		$query_grupa = DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id');
		$naziv = DB::table('grupa_pr')->where('grupa_pr_id',$query_grupa)->pluck('grupa');
		return Language::trans($naziv);
	}
	public static function get_grupa($roba_id){

		$grupa_pr_id=DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id');
		$grupa_pr=DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->first();
		return '<a href="'.Options::base_url().self::grupa_link($grupa_pr->parrent_grupa_pr_id,Url_mod::slug_trans($grupa_pr->grupa)).'" itemprop="recipeCategory"> '.Language::trans($grupa_pr->grupa).'</a>';
	}
	// public static function get_grupaB2b($roba_id){

	// 	$query_grupa=DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id');

	// 	if(Groups::grupa_level($query_grupa)==1){

	// 		echo '<a href="'.Options::base_url().'b2b/artikli/'.Url_mod::slug_trans(Seo::grupa_title($query_grupa)).'" itemprop="recipeCategory"> '.Seo::grupa_title($query_grupa).'</a>';
	// 	}
	// 	else if(Groups::grupa_level($query_grupa)==2){
	// 		$query_grupa2=DB::table('grupa_pr')->where('grupa_pr_id',$query_grupa)->pluck('parrent_grupa_pr_id');

	// 		echo '<a  itemprop="recipeCategory" href="'.Options::base_url().'b2b/artikli/'.Url_mod::slug_trans(Seo::grupa_title($query_grupa2)).'/'.Url_mod::slug_trans(Seo::grupa_title($query_grupa)).'"> '.Seo::grupa_title($query_grupa).'</a>';
	// 	}
	// 	else {
	// 		$query_grupa2=DB::table('grupa_pr')->where('grupa_pr_id',$query_grupa)->pluck('parrent_grupa_pr_id');
	// 		$query_grupa3=DB::table('grupa_pr')->where('grupa_pr_id',$query_grupa2)->pluck('parrent_grupa_pr_id');

	// 		echo '<a  itemprop="recipeCategory" href="'.Options::base_url().'b2b/artikli/'.Url_mod::slug_trans(Seo::grupa_title($query_grupa3)).'/'.Url_mod::slug_trans(Seo::grupa_title($query_grupa2)).'/'.Url_mod::slug_trans(Seo::grupa_title($query_grupa)).'"> '.Seo::grupa_title($query_grupa).'</a>';
	// 	}

	// }

	// public static function seo_keywords($roba_id){
	// 	$product_keywords=DB::table('roba')->where('roba_id',$roba_id)->first();
	// 	if($product_keywords->keywords!=""){
	// 		return $product_keywords->keywords;
	// 	}
	// 	else {
	// 		return str_replace(' ',', ',$product_keywords->naziv_web);
	// 	}
	// }
	
    // public static function provera_akcija($roba_id){
        
    //     	$query_roba=DB::table('roba')->where('roba_id',$roba_id);
				
				// foreach ($query_roba->get() as $key) {
							
				// 			$web_vrsta_prikaza=$key->web_vrsta_prikaza;
				// 			$roba_id=$key->roba_id;
				// 				$string = strpos($web_vrsta_prikaza, 'a');
				// 				if($string!==false){
								 
    //                              return 1;
								 
				// 				}
    //                             else {
    //                                 return 0;
    //                             }
								
				// 		}
    // }
	
    
	public static function provera_lagera($roba_id){
		if(Options::vodjenje_lagera()==1){

		$kolicina=DB::table('lager')->where('roba_id',$roba_id)->pluck('kolicina');
		if(isset($kolicina)){
			return $kolicina;
		}else{
			return 0;
		}

		}else {
			return 1;
		}
	}

	public static function ukupna_kolicina_proizvoda($roba_id){
		if(Options::vodjenje_lagera()==1){

			$centralna = DB::table('lager')->where('roba_id', $roba_id)->where('orgj_id', '1')->pluck('kolicina');
			$vp = DB::table('lager')->where('roba_id', $roba_id)->where('orgj_id', '4')->pluck('kolicina');
			if(isset($centralna) && !isset($vp)){
				return $centralna;
			}elseif(isset($vp) && !isset($centralna)){
				return $vp;
			} else {
				return $centralna + $vp;
			}

		}else {
			return 1;
		}
	}

	public static function ukupna_kolicina_proizvoda_magicini($roba_id){
		if(Options::vodjenje_lagera()==1){

			$magacini = DB::table('lager')->where('roba_id', $roba_id)->get();
			$kolicina = 0;
			foreach($magacini as $magacin) {
				$kolicina += $magacin->kolicina;
			}

			return $kolicina;
		}else {
			return 1;
		}
	}
	
	// public static function quantityB2b($roba_id){

	// 		$kolicina=0;
	// 		$rezervisano=0;
	// 		$query_lager=DB::table('lager')->where('roba_id',$roba_id)->get();
	// 		foreach($query_lager as $row){
	// 			$kolicina=$row->kolicina;
	// 			$rezervisano=$row->rezervisano;
	// 		}
	// 		return $kolicina-$rezervisano;

	// }

	// public static function tarifna_grupa($roba_id){
	// 	$tarifna_grupa_id=DB::table('roba')->where('roba_id',$roba_id)->pluck('tarifna_grupa_id');
		
	// 			$tarifna_grupa_query=DB::table('tarifna_grupa')->where('tarifna_grupa_id',$tarifna_grupa_id)->get();
	// 	foreach($tarifna_grupa_query as $row){
	// 		$porez=$row->porez;
		
	// 	}
	// 	return $porez;
		
	// }
	
	public static function web_slika($roba_id){
		$web_slika=DB::select("SELECT putanja,regexp_replace(putanja, '.+/', '') AS slika FROM web_slika where roba_id=".$roba_id." ORDER BY akcija DESC");

		if($web_slika){
			$putanja = 'images/products/big/'.$web_slika[0]->slika;;
		} else {
			$putanja="images/no-image.jpg";
		}
	     
		return $putanja;
	}

	public static function web_slika_second($roba_id){
		$web_slika=DB::select("SELECT putanja,regexp_replace(putanja, '.+/', '') AS slika FROM web_slika where roba_id=".$roba_id." and akcija = 0 ORDER BY web_slika_id DESC");

		if($web_slika){
			$putanja = 'images/products/big/'.$web_slika[0]->slika;;
		} else {
			$putanja=self::web_slika($roba_id);
		}
	     
		return $putanja;
	}
		
	public static function web_slika_big($roba_id,$slika_id=0){
			if($slika_id==0){
				$web_slika=DB::select("SELECT putanja,regexp_replace(putanja, '.+/', '') AS slika FROM web_slika where roba_id=".$roba_id." AND flag_prikazi = 1 ORDER BY akcija DESC");
			}else{
				$web_slika=DB::select("SELECT putanja,regexp_replace(putanja, '.+/', '') AS slika FROM web_slika where roba_id=".$roba_id." AND web_slika_id = ".$slika_id." AND flag_prikazi = 1");
			}

			if(isset($web_slika[0])){
				$putanja = 'images/products/big/'.$web_slika[0]->slika;;
			} else {
				$putanja="images/no-image.jpg";
			}
			return $putanja;
	}

	public static function get_proizvodjac($roba_id){
		return DB::table('proizvodjac')->where('proizvodjac_id',DB::table('roba')->where('roba_id',$roba_id)->pluck('proizvodjac_id'))->pluck('naziv');
	}

	public static function get_energetska_klasa($roba_id){
		return DB::table('roba')->where('roba_id', $roba_id)->pluck('energetska_klasa');
	}
	public static function get_energetska_klasa_old_or_new($roba_id){
		return DB::table('roba')->where('roba_id', $roba_id)->pluck('check_old_class');
	}
	public static function get_energetska_klasa_link($roba_id){
		$check = DB::table('roba')->where('roba_id', $roba_id)->pluck('check_link');
		$pdf = DB::table('roba')->where('roba_id', $roba_id)->pluck('energetska_klasa_pdf');
		$link = DB::table('roba')->where('roba_id', $roba_id)->pluck('energetska_klasa_link');
		if($check == 1) {
			return $link;
		} else {
			return Options::domain().$pdf;
		}
	}
	public static function get_energetska_klasa_tip($roba_id){
		return DB::table('energetska_klasa')->where('klasa', DB::table('roba')->where('roba_id',$roba_id)->pluck('energetska_klasa'))->pluck('energetska_klasa_id');
	}
	public static function get_opis_deklaracija($roba_id){
        return DB::table('roba')->where('roba_id',$roba_id)->pluck('deklaracija');
    }

	// public static function proizvodjaci($roba_id){
	// 	return DB::table('proizvodjac')->where('proizvodjac_id','!=',-1)->where(array('brend_prikazi'=>1))->get();
	// }
    
    // public static function get_model($roba_id){
        
    //     $model=DB::table('roba')->where('roba_id',$roba_id)->pluck('model');
    //     return $model;
    // }
    
	public static function get_price($roba_id,$akcija=true,$pakovanje=false,$kamata=0){
		$date = date('Y-m-d');
		$query = DB::table('roba')->where('roba_id',$roba_id);
		if($query->count() > 0){
			$product = $query->first();
		}else{
			return 0.00;
			
		}
		$datum_do=$product->datum_akcije_do;
		$datum_od=$product->datum_akcije_od;

		$cena = Options::checkCena()=='web_cena'?$product->web_cena:$product->mpcena;
		if($akcija && $product->akcija_flag_primeni == 1
			&& (is_null($datum_od) || ( !is_null($datum_od) && $date >= $datum_od ))
			&& (is_null($datum_do) || ( !is_null($datum_do) && $date <= $datum_do ))){

			$cena = $product->akcijska_cena;
			if(is_null($cena) || $cena == 0){
				$cena = Options::checkCena()=='web_cena'?$product->web_cena:$product->mpcena;
			}
		}
		if($pakovanje && $product->flag_ambalaza == 1){
			$cena = $cena * $product->ambalaza;
		}

		$cena = $cena *(1+$kamata/100);

		// if($cena > 1000 AND $roba_id != 141247124){
		// 	return substr(ceil($cena),0,-2).'90';
		// }
		
		if(AdminOptions::web_options(152)==1){
			return ceil($cena/10)*10;
		}else{
			return round($cena);
		}		
	}

	public static function broj_rata($selected_id=null){
		  foreach( DB::table('kupovina_na_rate')->orderBy('br_meseca','asc')->get() as $row ){
		  	if(!is_null($selected_id) && $selected_id != '' && $selected_id == $row->kamata){
                echo '<option value="'.$row->kamata.'" selected>'.Language::trans($row->br_meseca).' '.Language::trans($row->kamata).'</option> ';
            }else{
                echo '<option value="'.$row->kamata.'">'.Language::trans($row->br_meseca).' '.Language::trans($row->kamata).'</option> ';
            } 
		  }
	}
	
    public static function get_price_vezani($roba_id,$vezani_roba_id){
		$product = DB::table('vezani_artikli')->where(array('roba_id'=>$roba_id,'vezani_roba_id'=>$vezani_roba_id))->first();
		if(!is_null($product)){
			$cena = $product->cena;
		}else{
			return 0.00;
		}

		if(AdminOptions::web_options(152)==1){
			return ceil($cena/10)*10;
		}else{
			return round($cena);
		}            
    }

	public static function get_mpcena($roba_id){
		$mpcena = DB::table('roba')->where('roba_id',$roba_id)->pluck('mpcena');
		$dobavljac_id = DB::table('roba')->where('roba_id',$roba_id)->pluck('dobavljac_id');
				
		if(AdminOptions::web_options(152)==1){			
			return $mpcena_round;
		}elseif($dobavljac_id == 131808 && $mpcena > 1000){	
			$mpcena_round = ceil($mpcena/10)*10;
			$mpcena=substr($mpcena_round,0,-2).'90';			
			return $mpcena;			
		}else{			
			return $mpcena;
		}

	}

	public static function getPopust_akc($roba_id,$cena=null)
	{
		if(is_null($cena)) {
			$popust = self::get_price($roba_id,false) - self::get_price($roba_id);
		} else {
			$popust = $cena - self::get_price($roba_id);
		}
		
		if(AdminOptions::web_options(152)==1){
			return ceil( $popust/10 )*10;
		}else{
			return round( $popust );
		}

	}
	
	public static function old_price($roba_id){
		// $old = DB::table('roba')->where('roba_id',$roba_id)->pluck(Options::checkCena()=='web_cena'?'web_cena':'mpcena');
		$old = DB::table('roba')->where('roba_id',$roba_id)->pluck('mpcena');

		if(AdminOptions::web_options(152)==1){
			return ceil( $old/10 )*10;
		}else{
			return round( $old );
		}
	}
	public static function get_web_roba_karakteristike_short($roba_id){
			echo '<ul>';
			$generisane=DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->limit(4)->orderBy('rbr', 'asc')->get();
			foreach($generisane as $row){
				$grupa_pr_naziv_id=$row->grupa_pr_naziv_id;
				$vrednos=$row->vrednost;
				$naziv = DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$grupa_pr_naziv_id)->pluck('naziv');
				echo "<li>".$naziv.": </li> <li>".$vrednos."</li>";
				
			}
			echo "</ul>";
	}

	public static function get_deklaracija($roba_id) {
		$roba = DB::table('roba')->where('roba_id',$roba_id)->first();
		return $roba->deklaracija;
	}

	public static function get_karakteristike_short($roba_id){
		$query_roba=DB::table('roba')->where('roba_id',$roba_id);
		foreach($query_roba->get() as $row){
			$web_flag_karakteristike=$row->web_flag_karakteristike;
			$web_karakteristike=$row->web_karakteristike;
			
		}
		if($web_flag_karakteristike == 0){
			echo $web_karakteristike;
		}
		else if($web_flag_karakteristike == 1)
		{	echo '<ul class="features-list row">';
			$query_generisane=DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->take(3);
			foreach($query_generisane->get() as $row){
				$grupa_pr_naziv_id=$row->grupa_pr_naziv_id;
				$vrednos=$row->vrednost;
				foreach(DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$grupa_pr_naziv_id)->get() as $row2){
					
					$naziv_grup=$row2->naziv;
				}
			echo "<li class='medium-6 columns'>".$naziv_grup.": </li> <li class='medium-6 columns'>".$vrednos."</li>";
				
			
			}
			echo "</ul>";
		}
		else if($web_flag_karakteristike == 2){
			echo '<ul class="features-list row">';
			foreach(DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id',$roba_id)->distinct()->take(3)->get() as $row){
			$karakteristike_naziv=$row->karakteristika_naziv;
			$karakteristike_vrednost=$row->karakteristika_vrednost;
			echo "<li class='medium-6 columns'>".$karakteristike_naziv.": </li><li class='medium-6 columns'>".$karakteristike_vrednost."</li>";
			}
			echo "</ul>";
		}	
	}
	public static function get_karakteristike_short_print_spec($roba_id){
		$query_roba=DB::table('roba')->where('roba_id',$roba_id);
		foreach($query_roba->get() as $row){
			$web_flag_karakteristike=$row->web_flag_karakteristike;
			$web_karakteristike=$row->web_karakteristike;
			
		}
		if($web_flag_karakteristike == 0){
			echo $web_karakteristike;
		}
		else if($web_flag_karakteristike == 1)
		{	echo '<ul class="genkar">';
			$query_generisane=DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->take(10);
			foreach($query_generisane->get() as $row){
				$grupa_pr_naziv_id=$row->grupa_pr_naziv_id;
				$vrednos=$row->vrednost;
				foreach(DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$grupa_pr_naziv_id)->get() as $row2){ 
					$naziv_grup=$row2->naziv;
				}
			echo "<li>".$naziv_grup.": </li> <li>".$vrednos."</li><br>"; 
			}
			echo "</ul>";
		}
		else if($web_flag_karakteristike == 2){
			echo '<ul class="ul_1">';
			foreach(DB::table('dobavljac_cenovnik_karakteristike')->select('karakteristika_grupa')->where('roba_id',$roba_id)->distinct()->take(10)->get() as $naslov)
			{			 
				$karakteristika_grupa=$naslov->karakteristika_grupa; 
			
			if(isset($karakteristika_grupa)){
				echo '<li class="li_1">'.$karakteristika_grupa.'</li>'; 
		  		echo '<li class="li_2">';
		  		echo '<ul class="ul_2">';
				foreach(DB::table('dobavljac_cenovnik_karakteristike')->where('karakteristika_grupa',$karakteristika_grupa)->where('roba_id',$roba_id)->distinct()->take(3)->get() as $row)
				{
					$karakteristike_naziv=$row->karakteristika_naziv;
					$karakteristike_vrednost=$row->karakteristika_vrednost;		

					echo "<li class='li_3'>".$karakteristike_naziv.": </li><li class='li_3'>".$karakteristike_vrednost."</li><br>";
				} 
				echo '</ul>'; // ul_2
		 		echo '</li><br>'; // li_2 
			}else{
				echo '<li class="li_2">';
		  		echo '<ul class="ul_2">';
				foreach(DB::table('dobavljac_cenovnik_karakteristike')->where('karakteristika_grupa',$karakteristika_grupa)->where('roba_id',$roba_id)->distinct()->take(30)->get() as $row)
				{
					$karakteristike_naziv=$row->karakteristika_naziv;
					$karakteristike_vrednost=$row->karakteristika_vrednost;		

					echo "<li class='li_3'>".$karakteristike_naziv.": </li><li class='li_3'>".$karakteristike_vrednost."</li><br>";
				} 
				echo '</ul>'; // ul_2
		 		echo '</li><br>'; // li_2 
			}
			}
			echo '</ul>';
		}
	}
	public static function get_karakteristike_short_grupe($roba_id){
		$roba=DB::table('roba')->where('roba_id',$roba_id)->first();
		$web_flag_karakteristike=$roba->web_flag_karakteristike;

		if($web_flag_karakteristike == 0){
			if(strlen($roba->web_karakteristike) > 600){
				$string = preg_replace('/\<[\/]?(tr|td)([^\>]*)\>/i', '-', $roba->web_karakteristike);
				$string = strip_tags($string);
				$string = trim(preg_replace('/\s+/', ' ',  $string));
				$string = preg_replace('/- - - -/', '<br>',$string);
				$string = preg_replace('/- -- -/', '<br>',$string);
				$string = preg_replace('/- -/', ': ',$string);
				$string = preg_replace('/---/', '<br>',$string);
				$string = preg_replace('/----/', '<br>',$string);
				$string = preg_replace('/--/', ' ',$string);

				$positions = self::strpos_all($string,"<br>");
				if(isset($positions[4])){
					$string = substr($string, 0, $positions[4]);
				}
				if(strlen($string) > 500){
					$string = substr($string,0,500).' ...';
				}
				echo $string;
			}else{
				return $roba->web_karakteristike;
			}
		}
		else if($web_flag_karakteristike == 1)
		{	
			echo '<ul class="features-list row">';
			$generisane=DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->limit(4)->orderBy('rbr', 'asc')->get();
			foreach($generisane as $row){
				$grupa_pr_naziv_id=$row->grupa_pr_naziv_id;
				$vrednos=$row->vrednost;
				$naziv = DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$grupa_pr_naziv_id)->pluck('naziv');
				echo "<li class='medium-6 columns'>".$naziv.": </li> <li class='medium-6 columns'>".$vrednos."</li>";
				
			}
			echo "</ul>";
		}
		else if($web_flag_karakteristike == 2){
			echo '<ul class="features-list row">';
			foreach(DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id',$roba_id)->distinct()->limit(4)->orderBy('dobavljac_cenovnik_karakteristike_id', 'asc')->get() as $row){
			$karakteristike_naziv=$row->karakteristika_naziv;
			$karakteristike_vrednost=$row->karakteristika_vrednost;
			echo "<li  class='medium-6 columns'>".$karakteristike_naziv."</li> <li  class='medium-6 columns'>".$karakteristike_vrednost."</li>";
				
			}
			echo "</ul>";
		}
	}

	public static function get_karakteristike_table($roba_id){
		$roba=DB::table('roba')->where('roba_id',$roba_id)->first();
		$web_flag_karakteristike=$roba->web_flag_karakteristike;
		$web_karakteristike=$roba->web_karakteristike;
			
		if($web_flag_karakteristike == 0){
			echo str_replace('<br>', ' ', $web_karakteristike);
		}
		else if($web_flag_karakteristike == 1)
		{	echo "<table>";
			$query_generisane=DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->orderBy('rbr', 'asc');
			foreach($query_generisane->get() as $row){
				$grupa_pr_naziv_id=$row->grupa_pr_naziv_id;
				$vrednos=$row->vrednost;
				$naziv = DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$grupa_pr_naziv_id)->pluck('naziv');
			echo "<tr><td>".$naziv." </td><td>".$vrednos."</td></tr>";
				
			
			}
			echo "</table>";
		}
		else if($web_flag_karakteristike == 2){
			if(All::check_ewe($roba_id)==1){
		
				$query_type_ewe=DB::select("select * from dobavljac_cenovnik_karakteristike where roba_id=? and redni_br_grupe>0 order by redni_br_grupe asc",array($roba_id));
				echo "<table>";
				$karakteristika_grupa= '';
				foreach($query_type_ewe as $key => $grupa_kar){
					if($karakteristika_grupa != $grupa_kar->karakteristika_grupa){
						$karakteristika_grupa=$grupa_kar->karakteristika_grupa;

						echo "<tr><td>".$karakteristika_grupa."</td><td><table>";
					}
					
					$karakteristike_naziv=$grupa_kar->karakteristika_naziv;
					$karakteristike_vrednost=$grupa_kar->karakteristika_vrednost;
					echo "<tr><td>".$karakteristike_naziv."</td><td>".$karakteristike_vrednost."</td></tr>";

					if(!isset($query_type_ewe[$key+1]) || (isset($query_type_ewe[$key+1]) && $karakteristika_grupa != $query_type_ewe[$key+1]->karakteristika_grupa)){
						echo "</table></td></tr>";

					}
				
				}
				echo "</table>";
			}
			else {
			echo "<table>";
			foreach(DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id',$roba_id)->distinct()->orderBy('redni_br_grupe', 'asc')->orderBy('dobavljac_cenovnik_karakteristike_id', 'asc')->get() as $row){
			$karakteristike_naziv=$row->karakteristika_naziv;
			$karakteristike_vrednost=$row->karakteristika_vrednost;
			echo "<tr><td>".$karakteristike_naziv."</td><td>".$karakteristike_vrednost."</td></tr>";
				
			}
			echo "</table>";
			
			}
		}
	
	}

	public static function strpos_all($haystack, $needle) {
	    $offset = 0;
	    $allpos = array();
	    while (($pos = strpos($haystack, $needle, $offset)) !== FALSE) {
	        $offset   = $pos + 1;
	        $allpos[] = $pos;
	    }
	    return $allpos;
	}
		
	public static function get_karakteristike($roba_id){
		$roba=DB::table('roba')->where('roba_id',$roba_id)->first();
		$web_flag_karakteristike=$roba->web_flag_karakteristike;
		$web_karakteristike=$roba->web_karakteristike;
			
		if($web_flag_karakteristike == 0){
			$web_flag_karakteristike= str_replace('<br>', ' ', $web_karakteristike);
			echo htmlspecialchars_decode($web_flag_karakteristike);
		}
		else if($web_flag_karakteristike == 1)
		{	echo '<ul class="features-list row">';
			$query_generisane=DB::table('web_roba_karakteristike')->where('roba_id',$roba_id)->orderBy('rbr', 'asc');
			foreach($query_generisane->get() as $row){
				$grupa_pr_naziv_id=$row->grupa_pr_naziv_id;
				$vrednos=$row->vrednost;
				$naziv = DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$grupa_pr_naziv_id)->pluck('naziv');
			echo "<li class='medium-6 columns'>".$naziv.": </li> <li class='medium-6 columns'>".$vrednos."</li>";
				
			
			}
			echo "</ul>";
		}
		else if($web_flag_karakteristike == 2){
			if(All::check_ewe($roba_id)==1){
		
				$query_type_ewe=DB::select("select * from dobavljac_cenovnik_karakteristike where roba_id=? and redni_br_grupe>0 order by redni_br_grupe asc",array($roba_id));

				$karakteristika_grupa= '';
				foreach($query_type_ewe as $key => $grupa_kar){
					if($karakteristika_grupa != $grupa_kar->karakteristika_grupa){
						$karakteristika_grupa=$grupa_kar->karakteristika_grupa;

						echo '<ul class="generated-features-list row">
			                <li class="medium-4 columns features-list-title"><span>'.$karakteristika_grupa.'</span></li>
			                <li class="medium-8 columns features-list-items">';
							echo '<ul class="row">';
					}
					
					$karakteristike_naziv=$grupa_kar->karakteristika_naziv;
					$karakteristike_vrednost=$grupa_kar->karakteristika_vrednost;
					echo "<li class='medium-6 columns'>".$karakteristike_naziv.": </li><li class='medium-6 columns'>".$karakteristike_vrednost."</li>";

					if(!isset($query_type_ewe[$key+1]) || (isset($query_type_ewe[$key+1]) && $karakteristika_grupa != $query_type_ewe[$key+1]->karakteristika_grupa)){
						echo "</ul></li></ul>";

					}
				
				}
			}
			else {
					echo '<ul class="features-list row">';
			foreach(DB::table('dobavljac_cenovnik_karakteristike')->where('roba_id',$roba_id)->distinct()->orderBy('redni_br_grupe', 'asc')->orderBy('dobavljac_cenovnik_karakteristike_id', 'asc')->get() as $row){
			$karakteristike_naziv=$row->karakteristika_naziv;
			$karakteristike_vrednost=$row->karakteristika_vrednost;
			echo "<li  class='medium-6 columns'>".$karakteristike_naziv."</li> <li  class='medium-6 columns'>".$karakteristike_vrednost."</li>";
				
			}
			echo "</ul>";
			
			}
		}
	
	}
	
	// public static function get_tags($roba_id){
	// 	 echo '<ul class="tags row clearfix">';
	// 	foreach(DB::table('roba')->where('roba_id',$roba_id)->get() as $row){
	// 		$tags=$row->tags;
	// 	}
		
	// 	$niz=explode(',',$tags);
		
	// 	foreach($niz as $row2){
	// 		echo "<li>".$row2."</li>";
	// 	}
	// 	echo '</ul>';
	// }
    
    // public static function tags_count($roba_id){
    //     $tags=DB::table('roba')->where('roba_id',$roba_id)->pluck('tags');
        
    //     $niz=explode(',',$tags);
	       
    //     return count($niz);
    // }
    
    public static function get_proizvodjac_name($roba_id){
			$proizvodjac_id=DB::table('roba')->where('roba_id',$roba_id)->pluck('proizvodjac_id');
			return DB::table('proizvodjac')->where('proizvodjac_id',$proizvodjac_id)->pluck('naziv');
	}

    public static function get_barkod($roba_id){
		return DB::table('roba')->where('roba_id',$roba_id)->pluck('barkod');
	}

	public static function get_id_is($roba_id){
		return DB::table('roba')->where('roba_id',$roba_id)->pluck('id_is');
	}
	
	public static function get_list_images($roba_id,$parent=false){

		$query_images_query=DB::table('web_slika')->where(array('roba_id'=>$roba_id, 'flag_prikazi'=>1))->whereNull('parent_id');
		if(!$parent){
			$query_images_query->take(4);
		}
		return $query_images_query->orderBy('akcija','desc')->get();
	}

	public static function get_list_child_images($roba_id,$parent_id){

		$slike=DB::table('web_slika')->where(array('roba_id'=>$roba_id, 'flag_prikazi'=>1))->where('parent_id',$parent_id)->orderBy('akcija','desc')->get();

		$glavna = DB::table('web_slika')->where(array('roba_id'=>$roba_id, 'flag_prikazi'=>1, 'web_slika_id'=>$parent_id))->first();
		if(!is_null($glavna)){
			array_unshift($slike,$glavna);
		}
		return $slike;
	}

    public static function get_opis($roba_id){
        return DB::table('roba')->where('roba_id',$roba_id)->pluck('web_opis');
    }
    public static function get_kratak_opis($roba_id){
        return DB::table('roba')->where('roba_id',$roba_id)->pluck('opis');
    }

    public static function get_dobavljac($roba_id){
    	$id=DB::table('roba')->where('roba_id',$roba_id)->pluck('dobavljac_id');
    	return DB::table('partner')->where('partner_id',$id)->pluck('naziv');
    }
    public static function get_ncena($roba_id){
    	$ncena=DB::table('roba')->where('roba_id',$roba_id)->pluck('racunska_cena_nc');
    	$cena=$ncena*1.2;
    	$cena=number_format($cena, 2, ',', '.');
    	return $cena;
    }
    
    public static function get_related($roba_id, $limit=4){

        $grupa_pr_id=DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id');
        $web_cena=DB::table('roba')->where('roba_id',$roba_id)->pluck(Options::checkCena()=='web_cena'?'web_cena':'mpcena');
        $cena_od = $web_cena * 9/10;
        $cena_do = $web_cena * 6/5;
        
        return DB::select("SELECT DISTINCT r.roba_id, r.naziv_web FROM roba r".Product::checkLager('join').Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.grupa_pr_id = ".$grupa_pr_id." AND r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND r.roba_id <> -1 AND r.roba_id <> ".$roba_id." AND ".Options::checkCena()." > ".$cena_od." AND ".Options::checkCena()." < ".$cena_do." ".Product::checkLager().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()." LIMIT ".$limit."");
        
    }

	public static function product_bredacrumps($grupa_pr_id,$bredacrumps=""){
        $first = DB::table('grupa_pr')->where(array('grupa_pr_id'=>$grupa_pr_id,'web_b2c_prikazi'=>1))->first();
        if(!$first){
        	return $bredacrumps;
        }
        if($first->parrent_grupa_pr_id){
    		$link = self::grupa_link($first->parrent_grupa_pr_id,Url_mod::slug_trans($first->grupa));
        }else{
        	$link = Url_mod::slug_trans($first->grupa);
        }
        $bredacrumps = "<li><a href='".Options::base_url().$link."'>".Seo::grupa_title($first->grupa_pr_id)."</a></li>".$bredacrumps;
        if($first->parrent_grupa_pr_id != 0){
        	$bredacrumps =  self::product_bredacrumps($first->parrent_grupa_pr_id,$bredacrumps);
        }else{
        	$bredacrumps = "<li><a href='".Options::base_url()."'>".All::get_title_page_start()."</a></li>".$bredacrumps;
        }
        return $bredacrumps;

	}
	public static function grupa_link($grupa_pr_id,$link){
        $first = DB::table('grupa_pr')->where(array('grupa_pr_id'=>$grupa_pr_id))->first();

        if(is_null($first)){
        	return '/';
        }

        if($first->grupa_pr_id > 0){
			$link = Url_mod::slug_trans($first->grupa).'/'.$link;
		}

        if($first->parrent_grupa_pr_id > 0){
        	$link = self::grupa_link($first->parrent_grupa_pr_id,$link);
        }
        return $link;
	}

	public static function b2bBasicPrice($roba_id){
		// $product = DB::table('roba')->where('roba_id',$roba_id)->first();
		// return Options::checkCena()=='web_cena'?$product->web_cena:$product->mpcena / 1.2;
		return DB::select("SELECT ".Options::checkCena()." / (1 + (SELECT porez FROM tarifna_grupa WHERE tarifna_grupa_id = roba.tarifna_grupa_id)/100) as basic_price FROM roba WHERE roba_id = ".$roba_id."")[0]->basic_price;
	}

	public static function b2bPartnerGroupRabat($grupa_pr_id, $partner_id){
		$rabat = DB::table('partner_rabat_grupa')->where('partner_id',$partner_id)->where('grupa_pr_id',$grupa_pr_id)->pluck('rabat');
		$prcenat = 0;
		if($rabat != 0 && !is_null($rabat)){
			$prcenat=$rabat;
		}
		return $prcenat;
	}
	public static function b2bRabatProduct($roba_id, $partner_id){
		$b2b_max_rabat = DB::table('roba')->where('roba_id',$roba_id)->pluck('b2b_max_rabat');
		$art_prcenat = 0;
		if($b2b_max_rabat != 0 && !is_null($b2b_max_rabat)){
			$art_prcenat=$b2b_max_rabat;
		}
		return $art_prcenat;
	}
	public static function b2bPartnerRabatProduct($roba_id, $partner_id){
		return self::b2bRabatProduct($roba_id, $partner_id) + self::b2bPartnerGroupRabat(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id'), $partner_id);
	}
	public static function b2bTax($tarifna_grupa_id){
		return DB::table('tarifna_grupa')->where('tarifna_grupa_id',$tarifna_grupa_id)->pluck('porez');
	}

	// public static function b2bPrice($roba_id){
	// 	// $product = DB::table('roba')->where('roba_id',$roba_id)->first();
	// 	// $rabat = DB::table('partner_rabat_grupa')->where('partner_id',Session::get('b2b_user_'.Options::server()))->where('grupa_pr_id',$product->grupa_pr_id)->first();
	// 	// $prcenat = 0;
	// 	// if($rabat){
	// 	// 	$prcenat=$rabat->rabat;
	// 	// }
	// 	// $tg = DB::table('tarifna_grupa')->where('tarifna_grupa_id',$product->tarifna_grupa_id)->first();

	// 	// return All::procenat_p(All::procenat_m(Options::checkCena()=='web_cena'?$product->web_cena:$product->mpcena / 1.2,$prcenat),$tg->porez);
		
	// 	$article = DB::table('roba')->where('roba_id',$roba_id)->first();
 //        $basic_price = Product::b2bBasicPrice($roba_id);
 //        $rab_p = Product::b2bPartnerRabatProduct($roba_id, Session::get('b2b_user_'.Options::server()));
 //        $priceDiscount = All::procenat_m($basic_price,($rab_p));
 //        $tax = Product::b2bTax($article->tarifna_grupa_id);
 //        return All::procenat_p($priceDiscount, $tax);
	// }

	public static function getName($roba_id){
		// $roba = DB::table('roba')->where('roba_id',$roba_id)->first();
		// return $roba->naziv_web;
		return DB::table('roba')->where('roba_id',$roba_id)->pluck('naziv_web');
	}
    
    
	public static function checkPrice(){
		if(Options::gnrl_options(1307) ){
			return '';
		}else{
			return 'AND r.web_cena > 0 ';
		}
	}

	public static function checkImage($join=null){
		if(Options::gnrl_options(1308)){
			return '';
		}else{
			$result = 'AND ws.roba_id IS NOT NULL ';
			if($join == 'join'){
				$result = ' LEFT JOIN web_slika ws ON r.roba_id = ws.roba_id';
			}
			return $result;
		}
	}
	public static function checkLager($join=null){
		if(Options::gnrl_options(3057)){
			return '';
		}else{
			$result = 'AND l.kolicina > 0 ';
			if($join == 'join'){
				$result = ' LEFT JOIN lager l ON r.roba_id = l.roba_id';
			}
			return $result;
		}
	}	
	public static function checkNaStanju($join=null){
		if(Options::gnrl_options(3057)){
			return '';
		}else{
			$result = 'AND r.roba_flag_cene_id = 1 ';
			return $result;
		}
	}
	public static function checkActiveGroup($join=null){

			$result = 'AND g.web_b2c_prikazi = 1 ';
			if($join == 'join'){
				$result = ' LEFT JOIN grupa_pr g ON r.grupa_pr_id = g.grupa_pr_id';
			}
			return $result;	
	}
	public static function checkDescription(){
		if(Options::gnrl_options(1305)){
			return '';
		}else{
			return 'AND r.web_opis IS NOT NULL ';
		}
	}

	public static function checkCharacteristics($join=null){
		if(Options::gnrl_options(1306)){
			return '';
		}else{
			$result = 'AND (r.web_karakteristike IS NOT NULL OR wrk.roba_id IS NOT NULL OR dck.roba_id IS NOT NULL) ';
			if($join == 'join'){
				$result = ' LEFT JOIN web_roba_karakteristike wrk ON r.roba_id = wrk.roba_id LEFT JOIN dobavljac_cenovnik_karakteristike dck ON r.roba_id = dck.roba_id';
			}
			return $result;
		}
	}		
	public static function checkView($roba_id)
	{		
		$query = DB::select("SELECT DISTINCT r.roba_id FROM roba r".Product::checkLager('join').Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.roba_id = ".$roba_id." AND r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 ".Product::checkLager().Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."");
		if(count($query)>0){
			return true;
		}else{
			return false;
		}		
	}
	public static function getPopust($roba_id)
	{
		$mpcena = DB::table('roba')->where('roba_id',$roba_id)->pluck('mpcena');
		$web_cena = DB::table('roba')->where('roba_id',$roba_id)->pluck('web_cena');

		if(AdminOptions::web_options(152)==1){
			$popust = (ceil( $mpcena/10 )*10)-(ceil( $web_cena/10 )*10);		
			return round( $popust );
		}else{
			$popust = $mpcena-$web_cena;		
			return round( $popust );			
		}
		

	}


	public static function getSale($roba_id)
	{
		$result = DB::table('roba')->where('roba_id',$roba_id)->first();

		// $web_cena = Options::checkCena()=='web_cena'?intval($result->web_cena):intval($result->mpcena);
		// $web_cena = intval($result->web_cena);
		$web_cena = intval($result->mpcena);
		$akcijska_cena = intval($result->akcijska_cena);

		if($akcijska_cena>0){
			if($web_cena>0){
				// $result = $web_cena - $akcijska_cena;
				$result = 100*(1-$akcijska_cena/$web_cena);
				return round($result);
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	public static function getPopustProcenat($old_price,$new_price) {
		$popust = (1 - $new_price/$old_price) * 100;
		return number_format(round(floatval($popust),'2'), 2, ',', '.');;
	}
	
    public static function find_flag_cene($roba_flag_cene_id, $column)
    {
        return DB::table('roba_flag_cene')->select($column)->where('roba_flag_cene_id', $roba_flag_cene_id)->pluck($column);
    }
    public static function getStatusArticle($roba_id)
    {
        $roba_flag_cene_id = DB::select("SELECT roba_flag_cene_id FROM roba r WHERE roba_id=".$roba_id."")[0]->roba_flag_cene_id;
        return !is_null(DB::table('roba_flag_cene')->where(array('roba_flag_cene_id'=>$roba_flag_cene_id,'selected'=>1))->first()) ? $roba_flag_cene_id : 1;
    }
    public static function getStatusArticlePrice($roba_id)
    {
        $roba_flag_cene_id=DB::table('roba')->where('roba_id',$roba_id)->pluck('roba_flag_cene_id');
        return !is_null($roba_flag_cene = DB::table('roba_flag_cene')->where(array('roba_flag_cene_id'=>$roba_flag_cene_id,'selected'=>1))->first()) ? $roba_flag_cene->prikaz_cene : 1;
    }
    public static function getExtension($extension_id){
        return DB::select("SELECT ekstenzija FROM vrsta_fajla WHERE vrsta_fajla_id = ".$extension_id."")[0]->ekstenzija;
    }

    // public static function getAverageRating($roba_id){

    //     $averageRating = DB::table('web_b2c_komentari')->where('web_b2c_komentar_id', '!=', -1)->where('roba_id', $roba_id)->where('ocena', '!=', 0)->where('komentar_odobren', 1)->avg('ocena');
    //     $roundAvgRating = round($averageRating, 1);

    //     if($roundAvgRating == 0){
    //         return $result =  '<span>Ocena: Bez ocene</span>';
    //     } elseif ($roundAvgRating > 0 && $roundAvgRating <= 1.5) {
    //         return $result =  '<span>
    //                                 <span>Ocena: ('.$roundAvgRating.') </span>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="far fa-star" aria-hidden="true"></i>
    //                                 <i class="far fa-star" aria-hidden="true"></i>
    //                                 <i class="far fa-star" aria-hidden="true"></i>
    //                                 <i class="far fa-star" aria-hidden="true"></i>
    //                            </span>';
    //     } elseif ($roundAvgRating >= 1.6 && $roundAvgRating <= 2.5) {
    //         return $result =  '<span>
    //                                 <span>Ocena: ('.$roundAvgRating.') </span>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="far fa-star" aria-hidden="true"></i>
    //                                 <i class="far fa-star" aria-hidden="true"></i>
    //                                 <i class="far fa-star" aria-hidden="true"></i>
    //                            </span>';
    //     } elseif ($roundAvgRating >= 2.6 && $roundAvgRating <= 3.5) {
    //         return $result =  '<span>
    //                                 <span>Ocena: ('.$roundAvgRating.') </span>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="far fa-star" aria-hidden="true"></i>
    //                                 <i class="far fa-star" aria-hidden="true"></i>
    //                            </span>';
    //     } elseif ($roundAvgRating >= 3.6 && $roundAvgRating <= 4.5) {
    //         return $result =  '<span>
    //                                 <span>Ocena: ('.$roundAvgRating.') </span>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="far fa-star" aria-hidden="true"></i>
    //                            </span>';
    //     } elseif ($roundAvgRating >= 4.6 && $roundAvgRating <= 5) {
    //         return $result =  '<span>
    //                                 <span>Ocena: ('.$roundAvgRating.') </span>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                                 <i class="fas fa-star" aria-hidden="true"></i>
    //                            </span>';
    //     }
    // }

     public static function getRating($roba_id){

        $averageRating = DB::table('web_b2c_komentari')->where('web_b2c_komentar_id', '!=', -1)->where('roba_id', $roba_id)->where('ocena', '!=', 0)->where('komentar_odobren', 1)->avg('ocena');
        
        $roundAvgRating = round($averageRating, 1);

        if($roundAvgRating == 0){
            return $result =  '     <i class="far fa-star" aria-hidden="true" title="Artikal nije ocenjen"></i>
                                    <i class="far fa-star" aria-hidden="true" title="Artikal nije ocenjen"></i>
                                    <i class="far fa-star" aria-hidden="true" title="Artikal nije ocenjen"></i>
                                    <i class="far fa-star" aria-hidden="true" title="Artikal nije ocenjen"></i>
                                    <i class="far fa-star" aria-hidden="true" title="Artikal nije ocenjen"></i>
            					';
        } elseif ($roundAvgRating > 0 && $roundAvgRating <= 0.5) {
        	return $result =  '     <i class="fas fa-star-half-alt" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
            					';
        } elseif ($roundAvgRating > 0.6 && $roundAvgRating <= 1) {
        	return $result =  '     <i class="fas fa-star"   aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
            					';
        } elseif ($roundAvgRating > 1.1 && $roundAvgRating <= 1.5) {
            return $result =  '     <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star-half-alt" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                               ';

        } elseif ($roundAvgRating >= 1.6 && $roundAvgRating <= 2) {
            return $result =  '     <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                               ';

        } elseif ($roundAvgRating >= 2.1 && $roundAvgRating <= 2.5) {
            return $result =  '     <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star-half-alt" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>                                    
                               ';
        } elseif ($roundAvgRating >= 2.6 && $roundAvgRating <= 3) {
            return $result =  '     <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                               ';
        } elseif ($roundAvgRating >= 3.1 && $roundAvgRating <= 3.5) {
            return $result =  '     <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star-half-alt" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                               ';
        }elseif ($roundAvgRating >= 3.6 && $roundAvgRating <= 4) {
            return $result =  '     <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="far fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                               ';
        }elseif ($roundAvgRating >= 4.1 && $roundAvgRating <= 4.5) {
            return $result =  '     <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star-half-alt" aria-hidden="true" title='.$roundAvgRating.'></i>
                               ';
        }elseif ($roundAvgRating >= 4.6 && $roundAvgRating <= 5) {
            return $result =  '     <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                                    <i class="fas fa-star" aria-hidden="true" title='.$roundAvgRating.'></i>
                               ';
        }

    }

    public static function getRatingStars($rating){

        if($rating == 0){
            return $result =  'Ocena: Bez ocene ';
        } elseif ($rating > 0 && $rating <= 1.5) {
            return $result =  'Ocena: 
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="far fa-star" aria-hidden="true"></i>
                                    <i class="far fa-star" aria-hidden="true"></i>
                                    <i class="far fa-star" aria-hidden="true"></i>
                                    <i class="far fa-star" aria-hidden="true"></i>
                                ';
        } elseif ($rating >= 1.6 && $rating <= 2.5) {
            return $result =  'Ocena: 
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="far fa-star" aria-hidden="true"></i>
                                    <i class="far fa-star" aria-hidden="true"></i>
                                    <i class="far fa-star" aria-hidden="true"></i>
                                ';
        } elseif ($rating >= 2.6 && $rating <= 3.5) {
            return $result =  'Ocena: 
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="far fa-star" aria-hidden="true"></i>
                                    <i class="far fa-star" aria-hidden="true"></i>
                                ';
        } elseif ($rating >= 3.6 && $rating <= 4.5) {
            return $result =  'Ocena: 
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="far fa-star" aria-hidden="true"></i>
                               ';
        } elseif ($rating >= 4.6 && $rating <= 5) {
            return $result =  'Ocena:
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                    <i class="fas fa-star" aria-hidden="true"></i>
                                ';
        }
    }
    
	public static function check_osobine($roba_id){
		$check = false;
		if(DB::table('roba')->where('roba_id',$roba_id)->pluck('osobine')==1){
			if(DB::table('osobina_kombinacija')->where(array('roba_id'=>$roba_id,'aktivna'=>1))->count() > 0){
				$check = true;
			}
		}
		return $check;
	}

	public static function osobine_nazivi($roba_id){
		return array_unique(array_map('current',DB::table('osobina_naziv')->select('osobina_naziv.osobina_naziv_id','osobina_naziv.rbr')->distinct()->join('osobina_vrednost','osobina_vrednost.osobina_naziv_id','=','osobina_naziv.osobina_naziv_id')->join('osobina_kombinacija_vrednost','osobina_kombinacija_vrednost.osobina_vrednost_id','=','osobina_vrednost.osobina_vrednost_id')->join('osobina_kombinacija','osobina_kombinacija.osobina_kombinacija_id','=','osobina_kombinacija_vrednost.osobina_kombinacija_id')->where('roba_id',$roba_id)->where('osobina_kombinacija.aktivna',1)->orderBy('osobina_naziv.rbr','asc')->get()));
	}

	public static function osobine_vrednosti($roba_id,$osobina_naziv_id){
		return array_unique(array_map('current',DB::table('osobina_vrednost')->select('osobina_vrednost.osobina_vrednost_id','osobina_vrednost.rbr')->distinct()->join('osobina_kombinacija_vrednost','osobina_kombinacija_vrednost.osobina_vrednost_id','=','osobina_vrednost.osobina_vrednost_id')->join('osobina_kombinacija','osobina_kombinacija.osobina_kombinacija_id','=','osobina_kombinacija_vrednost.osobina_kombinacija_id')->where('roba_id',$roba_id)->where('osobina_kombinacija.aktivna',1)->where('osobina_vrednost.osobina_naziv_id',$osobina_naziv_id)->orderBy('osobina_vrednost.rbr','asc')->get()));
	}

	public static function find_osobina_naziv($osobina_naziv_id,$column){
		return DB::table('osobina_naziv')->where('osobina_naziv_id',$osobina_naziv_id)->pluck($column);
	}
	public static function find_osobina_vrednost($osobina_vrednost_id,$column){
		return DB::table('osobina_vrednost')->where('osobina_vrednost_id',$osobina_vrednost_id)->pluck($column);
	}
	public static function check_osobina_vrednost($roba_id,$osobina_naziv_id,$osobina_vrednost_id,$oldinput=null){
		if(!isset($oldinput)){
    		return self::osobine_vrednosti($roba_id,$osobina_naziv_id)[0] == $osobina_vrednost_id ? 'checked' : '';
		}else{
			return $oldinput == $osobina_vrednost_id ? 'checked' : '';
		}
	}

	public static function getOsobineStr($roba_id,$osobina_vrednost_ids){
		$str = '';
        if(self::check_osobine($roba_id) AND $osobina_vrednost_ids != null){
        	$vrednosti_ids = array();
        	foreach(DB::table('osobina_vrednost')->where('aktivna',1)->whereIn('osobina_vrednost_id',explode('-',$osobina_vrednost_ids))->whereIn('osobina_naziv_id',array_map('current',DB::table('osobina_naziv')->select('osobina_naziv_id')->where('aktivna',1)->get()))->get() as $row){
        		$vrednosti_ids[] = $row->osobina_vrednost_id;
			}
			if(count($vrednosti_ids)>0){			
	    		$str .= '(';
	            foreach($vrednosti_ids as $osobina_vrednost_id){
	    			$str .= self::find_osobina_naziv(self::find_osobina_vrednost($osobina_vrednost_id,'osobina_naziv_id'),'naziv').': '. self::find_osobina_vrednost($osobina_vrednost_id,'vrednost').', ';
	            }
	            $str = substr($str,0,-2);
				$str .= ')';
			}
        }
        return $str;
	}

	public static function getOsobineStrNonActv($roba_id,$osobina_vrednost_ids){
		$str = '';
        if(self::check_osobine($roba_id) AND $osobina_vrednost_ids != null){
        	$vrednosti_ids = array();
        	foreach(DB::table('osobina_vrednost')->whereIn('osobina_vrednost_id',explode('-',$osobina_vrednost_ids))->whereIn('osobina_naziv_id',array_map('current',DB::table('osobina_naziv')->select('osobina_naziv_id')->get()))->get() as $row){
        		$vrednosti_ids[] = $row->osobina_vrednost_id;
			}
			if(count($vrednosti_ids)>0){			
	    		$str .= '(';
	            foreach($vrednosti_ids as $osobina_vrednost_id){
	    			$str .= self::find_osobina_naziv(self::find_osobina_vrednost($osobina_vrednost_id,'osobina_naziv_id'),'naziv').': '. self::find_osobina_vrednost($osobina_vrednost_id,'vrednost').', ';
	            }
	            $str = substr($str,0,-2);
				$str .= ')';
			}
        }
        return $str;
	}
	
	public static function osobina_vrednost_checked($osobina_naziv_id,$osobina_vrednost_id){
		$vrednost = '&nbsp';
		if(self::find_osobina_naziv($osobina_naziv_id,'prikazi_vrednost') == 1){
			$vrednost = self::find_osobina_vrednost($osobina_vrednost_id,'vrednost');
		}
		return $vrednost;
	}

	public static function jedinica_mere($roba_id){
		$jedinica_mere_id = DB::table('roba')->where('roba_id',$roba_id)->pluck('jedinica_mere_id');
		return DB::table('jedinica_mere')->where('jedinica_mere_id',$jedinica_mere_id)->first();
	}

	public static function tezina_proizvoda($roba_id){
		return round(DB::table('roba')->where('roba_id',$roba_id)->pluck('tezinski_faktor'));
	}

	public static function tags($roba_id) {
		$tags = DB::table('roba')->where('roba_id', $roba_id)->pluck('tags');
		if(!is_null($tags) && trim($tags) != ''){
			$tags = explode(',', $tags);
			$formated = '';
			foreach ($tags as $tag) {
				if(trim($tag) != '') {
					$tag = trim($tag);
					$formated .= "<a href='".Options::base_url().Url_mod::slug_trans('tagovi')."/".Url_mod::slugify($tag)."'>".$tag."</a>";
				}
			}
			return $formated;
		}
		return '';


	}

	public static function article_link($roba_id,$lang=null){
		$link = Options::domain();
		if(Language::multi()){
			if(is_null($lang)){
				$link .= Language::lang().'/';
			}else{
				$link .= $lang.'/';
			}
		}
		return $link .= Url_mod::slug_trans('artikal',$lang).'/'.Url_mod::slugify(Product::seo_title($roba_id),$lang);
	}

	public static function vrsteCena($naziv){
		if(DB::table('vrsta_cena')->where(array('selected'=>1,'naziv'=>$naziv))->count() == 0){
			return false;
		}else{
			return true;
		}
	}
	public static function getSlike($roba_id)
	{
		return DB::table('web_slika')->select('roba_id','web_slika_id','akcija','flag_prikazi','putanja')->where('roba_id', $roba_id)->orderBy('akcija','DESC')->orderBy('web_slika_id','ASC')->get();

	}
    // public static function naziv_dopunski($roba_id){
    // 	$naziv_dopunski=DB::table('roba')->where('roba_id',$roba_id)->pluck('naziv_dopunski');
    //     return $naziv_dopunski;
    // }
	public static function pakovanje($roba_id){
		return DB::table('roba')->where('roba_id',$roba_id)->pluck('flag_ambalaza') == 1;
	}
	
	public static function ambalaza($roba_id){
		return DB::table('roba')->where('roba_id',$roba_id)->pluck('ambalaza');
	}
	
	public static function get_karakteristika_srodni($grupa_pr_vrednost_id){
		$grupa_pr_vrednost = DB::table('grupa_pr_vrednost')->where('grupa_pr_vrednost_id',$grupa_pr_vrednost_id)->first();
		if(!is_null($grupa_pr_vrednost)){
			$grupa_pr_naziv = DB::table('grupa_pr_naziv')->where('grupa_pr_naziv_id',$grupa_pr_vrednost->grupa_pr_naziv_id)->first();
			if(!is_null($grupa_pr_naziv)){
				return $grupa_pr_vrednost->naziv;
			}
		}
		return '';
	}
	public static function get_main_category($roba_id){
		$grupa_pr_id=DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id');
		if($grupa_pr_id > 0){
			return DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->first();
		}
		return null;
	}
	public static function gtArticleDetails($roba_id,$quantity){
		$category = Product::get_main_category($roba_id);

		return [
			'name' => ($seo = Seo::article($roba_id)) ? addslashes(str_replace("'","",$seo->title)) : $roba_id,
			'id' => $roba_id,
			'price' => Product::get_price($roba_id),
			'brand' => addslashes(str_replace("'","",Product::get_proizvodjac_name($roba_id))),
			'category_id' => !is_null($category) ? $category->grupa_pr_id : '',
			'category' => !is_null($category) ? $category->grupa : 'None',
			'currency' => Articles::get_valuta(),
			'quantity' => $quantity
		];
	}
}