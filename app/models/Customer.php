<?php

class Customer extends Eloquent
{
    protected $table = 'web_kupac';

    protected $primaryKey = 'web_kupac_id';
    
    public $timestamps = false;

	protected $hidden = array(
        'web_kupac_id',
        'kod',
        'lozinka',
        'ime',
        'adresa',
        'telefon',
        'flag_potvrda',
        'partner_id',
        'prezime',
        'flag_prima_poruke',
        'flag_vrsta_kupca',
        'telefon_mobilni',
        'naziv',
        'fax',
        'mesto',
        'pib',
        'status_registracije',
        'sifra_connect',
        'kupac_popust',
        'facebook_id',
        'ulica_id',
        'broj',
        'posta_br',
        'bodovi',
        'bodovi_aktivno',
        'maticni_br',
        'ptt_cityexpress',
        'datum_kreiranja',
        'servis',
        'id_is'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['web_kupac_id']) ? $this->attributes['web_kupac_id'] : null;
	}
    public function getFirstNameAttribute()
    {
        return isset($this->attributes['ime']) ? $this->attributes['ime'] : null;
    }
    public function getLastNameAttribute()
    {
        return isset($this->attributes['prezime']) ? $this->attributes['prezime'] : null;
    }
    public function getNameAttribute()
    {
        return isset($this->attributes['naziv']) ? $this->attributes['naziv'] : null;
    }
    public function getTaxIdAttribute()
    {
        return isset($this->attributes['pib']) ? intval(trim($this->attributes['pib'])) : null;
    }
    public function getAddressAttribute()
    {
        return isset($this->attributes['adresa']) ? $this->attributes['adresa'] : null;
    }
    public function getCityAttribute()
    {
        return isset($this->attributes['mesto']) ? $this->attributes['mesto'] : null;
    }
    public function getPhoneAttribute()
    {
        return isset($this->attributes['telefon']) ? $this->attributes['telefon'] : null;
    }
    public function getEmailAttribute()
    {
        return isset($this->attributes['email']) ? $this->attributes['email'] : null;
    }
    public function getExternalCodeAttribute()
    {
        return isset($this->attributes['id_is']) && !empty($this->attributes['id_is']) ? $this->attributes['id_is'] : null;
    }


	protected $appends = array(
    	'id',
    	'first_name',
        'last_name',
        'name',
        'tax_id',
        'address',
        'city',
        'phone',
        'email',
        'external_code'
    	);



}