<?php

class Category extends Eloquent
{
    protected $table = 'grupa_pr';

    protected $primaryKey = 'grupa_pr_id';
    
    public $timestamps = false;

	protected $hidden = array(
		'grupa_pr_id',
    	'grupa',
    	'putanja_slika',
		'opis', 
    	'parrent_grupa_pr_id',
    	'sifra',
    	'web_b2b_prikazi',
    	'web_b2c_prikazi',
    	'prikaz',
    	'sifra_connect',
    	'sablon_opis', 
    	'sablon_karakteristike', 
    	'css_opis', 
    	'css_karakteristike', 
    	'b2c_konf_prikazi',
    	'redni_broj',
    	'keywords', 
    	'rabat', 
    	'sifra_is', 
    	'id_is', 
    	'pozadinska_slika'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['grupa_pr_id']) ? $this->attributes['grupa_pr_id'] : null;
	}
	public function getNameAttribute()
	{
	    return isset($this->attributes['grupa']) ? $this->attributes['grupa'] : null;
	}
	public function getParentIdAttribute()
	{
	    return isset($this->attributes['parrent_grupa_pr_id']) ? $this->attributes['parrent_grupa_pr_id'] : null;
	}
	public function getImagePathAttribute()
	{
	    return isset($this->attributes['putanja_slika']) ? $this->attributes['putanja_slika'] : null;
	}
    public function getSortAttribute()
    {
        return isset($this->attributes['redni_broj']) ? $this->attributes['redni_broj'] : null;
    }
    public function getShowAttribute()
    {
        return isset($this->attributes['web_b2c_prikazi']) ? $this->attributes['web_b2c_prikazi'] : null;
    }
    public function getShowB2bAttribute()
    {
        return isset($this->attributes['web_b2b_prikazi']) ? $this->attributes['web_b2b_prikazi'] : null;
    }
    public function getExternalCodeAttribute()
    {
        return isset($this->attributes['id_is']) && !empty($this->attributes['id_is']) ? $this->attributes['id_is'] : null;
    }      

	protected $appends = array(
    	'id',
    	'name',
    	'parent_id',
        'show',
        'show_b2b',
        'image_path',
        'sort',
        'external_code'
    	);

    public function parent(){
        return $this->belongsTo('Category','parrent_grupa_pr_id');
    }

    public function childs(){
        return $this->hasMany('Category', 'parrent_grupa_pr_id', 'grupa_pr_id');
    }

    public function categoryLangs(){
        return $this->hasMany('CategoryLang', 'grupa_pr_id');
    }

    public function articles(){
        return $this->hasMany('Article', 'grupa_pr_id');
    }


    public function getIdByCode($name,$mapped=[]){
        if(isset($mapped[$name])){
            return $mapped[$name];
        }
        return -1;

        // if(is_null($categoryId = self::where('grupa',$name)->pluck('grupa_pr_id'))){
        //     $nextId = Category::max('grupa_pr_id')+1;

        //     $category = new Category();
        //     $category->grupa_pr_id = $nextId;
        //     $category->sifra = $nextId;
        //     $category->parrent_grupa_pr_id = 0;
        //     $category->grupa = $name;
        //     $category->web_b2c_prikazi = 1;
        //     $category->web_b2b_prikazi = 1;
        //     $category->save();
        //     $categoryId = $category->grupa_pr_id;
        // }

        // return $categoryId;
    }

    public function mappedByExternalCode(){
        $mapped = [];
        foreach(self::select('grupa_pr_id','sifra_is')->whereNotNull('sifra_is')->where('sifra_is','<>','')->get() as $category){
            $mapped[$category->sifra_is] = $category->grupa_pr_id;
        }
        return $mapped;
    }

}