$(document).ready(function(){
	var tiny_settings_full = {
		selector: ".JSInlineFull",
	    //menubar: false,
	    inline: true,
	    plugins: [ 
            "advlist autolink lists link image charmap print preview anchor",    
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table paste",
            "autoresize"
        ],   
	    toolbar: "insertfile undo redo | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",  
	};
    tinymce.init(tiny_settings_full);

	var tiny_settings_short = {
		selector: ".JSInlineShort",
	    menubar: false,
	    inline: true,
	    plugins: [
	      'textcolor',
	      'lists',
	      // 'powerpaste',
	      // 'linkchecker',
	      'contextmenu',
	      'autolink'
	    ],
	    toolbar: "insertfile undo redo"
	};
    tinymce.init(tiny_settings_short);


	var change_data = [];
	// $(document).on('mouseover','.JSInlineShort, .JSInlineFull',function(){
	// 	$(this).css('border', '1px dotted #000');
	// 	$(this).before('<div class="JSEditBar"><div>Edit</div></div>');

	// });
	// $(document).on('mouseout','.JSInlineShort, .JSInlineFull',function(){
	// 	$(this).css('border', '0px');
	// 	$(document).find('.JSEditBar').remove();
	// });


	$(document).on('focusout','.JSInlineShort, .JSInlineFull',function(){
		var target = $(this).data('target');

		var element_data = {
			action: target.action,
			id: target.id,
			content: $(this).html()
		};

		var filter_index = change_data.findIndex(function(element){
			return (element.action == target.action && element.id == target.id);
		});

		if(filter_index > -1){
			change_data[filter_index] = element_data;
		}else{
			change_data.push(element_data);
		}

	});

	$(document).on('click','#JSShortAdminSave',function(){

		if(change_data.length > 0){
	    	$.post('/admin/front-admin-save', {change_data: change_data, lang_id: $('#lang_id').val()}, function (response){
	    		swal('Uspešno ste sačuvali izmene.', "", "success");
				setTimeout(function() { sweetAlert.close(); }, 2000);
	    		setTimeout(function(){ location.reload(true); }, 1000);
	    	});
	    }else{
    		swal('Trenutno nema izmena koje biste sačuvali.', "", "error");
			setTimeout(function() { sweetAlert.close(); }, 2000);
	    }
	});
 
});