$(document).ready(function () {
	$('#JSOrderSubmit').click(function(){
		$(this).attr('disabled',true);
		$('#JSOrderForm').submit();
	});
	$('.JScheck_user_type').click(function () {
		$('.JScheck_user_type').removeClass('active');
		$(this).addClass('active');
		$('.JSwithout-reg-personal').removeClass('active');
		$('.JSwithout-reg-none-personal').removeClass('active');
		if($(this).data('vrsta') == 'personal'){
			$("input[name='flag_vrsta_kupca']").val('0');
			$('.JSwithout-reg-personal').addClass('active');
		}
		else if($(this).data('vrsta') == 'non-personal'){
			$("input[name='flag_vrsta_kupca']").val('1');
			$('.JSwithout-reg-none-personal').addClass('active');
		}
	});
}); 