$(document).ready(function(){
	$('#JSRegToggle').click(function(){
		if($('#JSRegToggleSec').attr('hidden') == 'hidden'){
			$('#JSRegToggleSec').removeAttr('hidden');
		}else{
			$('#JSRegToggleSec').attr('hidden','true');
		}
	});

	$('#JSAddCartSubmit').click(function(){
		$(this).attr('disabled',true);
		$('#JSAddCartForm').submit();
	});
	
	$(document).on('click','.JSadd-to-cart',function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		$.post(base_url+'list-cart-add',{roba_id: roba_id},function(response){
			$('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Artikal je dodat u korpu')+".</p>");

			var results = $.parseJSON(response);
			
			if(parseInt(results.check_available) == 0){
				obj.after('<button class="not-available not-available-style"> <i class="fas fa-shopping-cart cart-me"> </i> <span> '+trans("Nije dostupno")+'</span> </button>');
				obj.remove();
			}
			$('.JSheader-cart-content').html(results.mini_cart_list);
			$('.JScart_num').text(results.broj_cart);			

		});
	});

		$(document).on('click','.JSAddToCartQuickView',function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		var kolicina = $('#JSQuickViewAmount').val();
		if(!(!isNaN(kolicina) && kolicina > 0)){
			$('.JSinfo-popup').fadeIn("fast").delay(1000).fadeOut("fast");
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans("Nije dostupno")+".</p>");
		}else{
			$.post(base_url+'quick-view-cart-add',{roba_id: roba_id, kolicina: kolicina},function(response){
				var results = $.parseJSON(response);
				$('.JSinfo-popup').fadeIn("fast").delay(1000).fadeOut("fast");
				if(results.added){
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Artikal je dodat u korpu.')+"</p>");
					if(parseInt(results.check_available) == 0){
						$('#JSQuickViewAmount').remove();
						obj.after('<button class="not-available button"> <span> '+trans("Nije dostupno")+'</span> </button>');
						obj.remove();

						var obj_list = $('.JSadd-to-cart[data-roba_id="'+roba_id+'"]');
						obj_list.after('<button class="not-available dodavnje not-available-style"> <i class="fas fa-shopping-cart cart-me"> </i> <span> '+trans("Nije dostupno")+'</span> </button>');
						obj_list.remove();
					}
					$('.JSheader-cart-content').html(results.mini_cart_list);
					$('.JScart_num').text(results.broj_cart);		
					
				}else{
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans("Nije dostupno")+".</p>");
				}

			});
		}
	});
	$('.JSclosed').on('click', function(){
		$('.JScart-view').fadeOut(100);
	});

	$('.JSadd-to-cart-similar').click(function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		var vezani_roba_id = obj.data('vezani_roba_id');
		// var kolicina = obj.closest('div').find('.JSkolicina').val();
		var kolicina = 1;

		$.post(base_url+'vezani-cart-add',{roba_id: roba_id, vezani_roba_id: vezani_roba_id, kolicina: kolicina},function(response){
			var results = $.parseJSON(response);
			$('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
			if(results.success){
				$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Artikal je dodat u korpu')+".</p>");					
				if(results.check_available <= 0){
					obj.after('<button class="not-available not-available-style"> <i class="fas fa-shopping-cart cart-me"> </i> <span> '+trans("Nije dostupno")+'</span> </button>');
					obj.remove();			
				}
				$('.JSheader-cart-content').html(results.mini_cart_list);
				$('.JScart_num').text(results.broj_cart);
			}else{
				if(results.exists == 0){
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Tražena količina nije dostupna')+".</p>");
				}else{
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Artikal je već dodat u korpu')+".</p>");
				}
			}

		});
	});

	$('.JScart-less, .JScart-more').click(function(){
		var obj = $(this);
		var stavka_id = obj.data('stavka_id');
		var kolicina_temp = obj.closest('li').find('.JScart-amount').val();
		var kolicina;
		if(obj.attr('class') == 'JScart-less'){
			kolicina = parseInt(kolicina_temp) - 1;
		}
		else if(obj.attr('class') == 'JScart-more'){
			kolicina = parseInt(kolicina_temp) + 1;
		}

		if(kolicina > 0){		
			$.post(base_url+'cart-add-sub',{stavka_id: stavka_id, kolicina: kolicina},function(response){
				var results = $.parseJSON(response);
				if(results.changed){
					obj.closest('li').find('.JScart-amount').val(kolicina);					
					obj.closest('ul').find('.JScart-item-price').html(results.cart_item_ukupno);					

					$('.JSdelivery_total_amount').html(results.cart_ukupno);
					$('.JSUkupno_sa_dostavom').html(results.cart_item_ukupno_dostava);
					$('.JStotal_amount_weight').html(results.cart_item_ukupno_tezina);
					$('.JSheader-cart-content').html(results.mini_cart_list);
					$('.JScart_num').text(results.broj_cart);
					$('#JSAchievePoints').text(results.bodovi_ostvareni_bodovi_korpa);
					$('#JSMaxUsingPoints').text(results.bodovi_popust_bodovi_korpa);
					$('#JSMaxUsingVoucherPrice').html(results.vauceri_popust_cena_korpa);	

					for (var i=0; i < results.vezane_stavke.length; i++) {
						var vezani_id = results.vezane_stavke[i].web_b2c_korpa_stavka_id;
						var vezani_kolicina = parseFloat(results.vezane_stavke[i].kolicina).toFixed(0);
						$('.JScart-amount[data-stavka_id='+vezani_id+']').val(vezani_kolicina);
						$('.JScart-item-price[data-stavka_id='+vezani_id+']').html(results.vezane_stavke[i].vezani_item_cena);
					}

					$('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Količina je promenjena')+".</p>");
				}else{
					$('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Tražena količina nije dostupna')+".</p>");
				}

			});
		}else{
			$('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Količina ne sme biti manja od 1')+"!</p>");
		}
	});

	$('.JScart-amount').keyup(function(){
		var obj = $(this);
		var stavka_id = obj.data('stavka_id');
		var kolicina = $(this).val();

		
		if(kolicina > 0){		
			$.post(base_url+'cart-add-sub',{stavka_id: stavka_id, kolicina: kolicina},function(response){
				var results = $.parseJSON(response);
				if(results.changed){
					obj.closest('li').find('.JScart-amount').val(kolicina);					
					obj.closest('ul').find('.JScart-item-price').html(results.cart_item_ukupno);					

					$('.JSdelivery_total_amount').html(results.cart_ukupno);
					$('.JSUkupno_sa_dostavom').html(results.cart_item_ukupno_dostava);
					$('.JStotal_amount_weight').html(results.cart_item_ukupno_tezina);
					$('.JSheader-cart-content').html(results.mini_cart_list);
					$('.JScart_num').text(results.broj_cart);	
					$('#JSAchievePoints').text(results.bodovi_ostvareni_bodovi_korpa);
					$('#JSMaxUsingPoints').text(results.bodovi_popust_bodovi_korpa);
					$('#JSMaxUsingVoucherPrice').html(results.vauceri_popust_cena_korpa);


					$('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Količina je promenjena')+".</p>");
				}else{
					$('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
					$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Tražena količina nije dostupna')+".</p>");
				}

			});
		}else{
		
			$('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Količina ne sme biti manja od 1')+"!</p>");
		}
	});

	$(document).on('click','.JSdelete_cart_item',function(){
		var stavka_id = $(this).data('stavka_id');

		bootbox.confirm({
            message: "<p>"+trans('Artikal će biti uklonjen iz korpe. Da li ste sigurni')+"?</p>",
            buttons: {
                cancel: {
                    label: trans('Ne')
                },
                confirm: {
                    label: trans('Da')
                }
            },
            callback: function (result) {
                if(result){
					$.post(base_url+'cart-stavka-delete',{stavka_id: stavka_id},function(response){
						location.reload(true);
						$('.JSinfo-popup').fadeIn().delay(1500).fadeOut();
						$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Artikal je uklonjen iz korpe')+".</p>");
					});
                }
            }
        });
	});
	
	$('#JSDeleteCart').click(function(){
		bootbox.confirm({
            message: "<p>"+trans('Da li ste sigurni da želite da ispraznite korpu')+"?</p>",
            buttons: {
                cancel: {
                    label: trans('Ne')
                },
                confirm: {
                    label: trans('Da')
                }
            },
            callback: function (result) {
                if(result){
					$.post(base_url+'cart-delete',{},function(response){
						location.reload(true);
						$('.JSinfo-popup').fadeIn().delay(1500).fadeOut();
						$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Korpa je ispražnjena')+".</p>");
					});
                }
            }
        });
	});

	$(document).on('click','.JSnot_logged',function(){
		bootbox.alert({
			size: "small",
			message: "<p>"+trans('Da biste dodali proizvod na listu želja, morate biti ulogovani')+".</p>"
		});
	});

	$(document).on('click','.JSadd-to-wish',function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		$.post(base_url+'wish-list-add',{roba_id: roba_id},function(response){
			var results = $.parseJSON(response);
			$('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+results.message+"</p>");
			$('.JSbroj_wish').text(results.broj_wish);
		});
	});

	$(document).on('click','.JSukloni',function(){
		var roba_id = $(this).data('roba_id');

		bootbox.confirm({
            message: "<p>"+trans('Artikal će biti uklonjen sa liste želja. Da li ste sigurni')+"?</p>",
            buttons: {
                cancel: {
                    label: trans('Ne')
                },
                confirm: {
                    label: trans('Da')
                }
            },
            callback: function (result) {
                if(result){
					$.post(base_url+'wish-list-delete',{roba_id: roba_id},function(response){
						location.reload(true);
						$('.JSinfo-popup').fadeIn().delay(1000).fadeOut();
						$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Artikal je uklonjen iz liste želja')+".</p>");
					});
                }
            }
        });
	});

	$('.JSdeliveryInput').on('change', function(){
		// vrednost dostave
		var deliv_value = $('.JSexpenses');
		var deliv_value_repl = (deliv_value.text().replace(/[^\d]/g,'')/100);
		// ukupno
		var value = $('.JStotal_amount');
		var val2 = value.text();
		// cena arrtikla
		var art_price =  $('.JSdelivery_total_amount');
		var art_price_repl = (art_price.text().replace(/[^\d]/g,'')/100);
		var dec = art_price.text().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		// cena sa dostavom
		var original = art_price_repl+deliv_value_repl;		
		var original2 = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'rsd' }).format(original);
		var original3 =original2.replace('RSD','rsd.');
		
		if($(this).val() == 2){ 
			$('.JSDelivery').hide();
			value.text(dec); 
		}else{ 
			$('.JSDelivery').show();
			value.text(original3);
		} 
	});

	
	$('.JS_total_price').on('DOMSubtreeModified', function(){
		var price = $(this).text(),
			divs = $(this).parent().prevAll('.JS_price_adds'),
			free_delivery = $(this).nextAll('.JS_free_del'), 
			original_price = $(this).next().data('dostava');
 
	 	if (price > original_price) { 
			$(this).hide();
			divs.hide(); 
			free_delivery.removeAttr('hidden');
	 	}else{
	 		$(this).show();
			divs.show(); 
			free_delivery.attr('hidden', 'hidden'); 
	 	}
		// var ukupno_repl = (ukupno.replace(/[^\d]/g,'')/100);
		// var dostava = $(this).data('dostava');
		// var JS_free_del = $('.JS_free_del'); 
		// var razlika = ukupno_repl-dostava;
		// console.log(dostava) 
		// if(razlika < 0){
		// $('.JSfree_delivery').hide(); 
		// }else{
		// $('.JSfree_delivery').show();  
		// } 
	});
 

	if($('select[name="web_nacin_placanja_id"]').val() == 3){
		$('#JSCaptcha').removeAttr('hidden');
	}
	$(document).on('change','select[name="web_nacin_placanja_id"]',function(){
		if($(this).val() == 3){
			$('#JSCaptcha').removeAttr('hidden');
		}else{
			$('#JSCaptcha').attr('hidden','hidden');
		}
	});

	
    if (window.location.hash) {
        window.location.hash;
    }
});

function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
} 
 