$(document).ready(function () { 
    $(window).on('scroll', function () { 
 
        if ($('#JSfixed_header').hasClass('JSsticky_header')) {
            $('.sticky-element').css('top', $('#JSfixed_header').outerHeight() + 15 );
        } else {
            $('.sticky-element').css('top', '');
        }

    });
  
    $('a[href^="#"]').on('click', function(event) { 
        var target = $( $(this).attr('href') ); 
        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top  
            }, 500);
        } 
    }); 

    // category sidebar toggler 
    $('.JScategory-sidebar__list__toggler').on('click', function(){
        $(this).parent().toggleClass('category-sidebar__list--open');
    }); 

});



 