// URL FRAGMENT HANDLER
if(window.location.hash) {
    history.pushState("", document.title, window.location.pathname + window.location.search);
}
$(document).ready(function () {
// LAZY LOAD IMAGES
	$(function() {
		// change data-src to src to disable plugin
		// 1.product_on_grid
		// 2.product_on_list
		// 3.footer
        $('.JSlazy_load').Lazy({
        	scrollDirection: 'vertical'
        });
    });

	  // IF THERE IS NO IMAGE
	$('img').on('error', function(){ 
		$(this).attr('src', '/images/no-image.jpg');
	});
 
 // if($('body').is('#start-page') || $('body').is('#artical-page')){		// START PAGE ==============
  
	// SLICK SLIDER INCLUDE
	if($('.JSmain-slider')[0]) {  
		if($('#admin_id').val()){
			$('.JSmain-slider').slick({
				autoplay: true,
				autoplaySpeed: 6000, 
				draggable: true,
				dots:true
			});
		}else{
			$('.JSmain-slider').slick({
				autoplay: true,
				autoplaySpeed: 6000,
				dots:true
			});
		} 
	}
 
	
	// IZDVAJAMO TYPE
	$('.JSlist-article').each(function(){
		if($(this).find('.section-title').text().toLowerCase() == 'izdvajamo') {
			var siblings_parent = $(this).parent().prev('div');
				elem = $(this);
			elem.removeClass('parent-container container').addClass('JSseparated'); 
			siblings_parent.find('.JSlist-article').removeClass('parent-container container').addClass('JSseparated'); 

			elem.find('.JSproducts_slick').removeClass().addClass('JSproduct-one-slick');
			siblings_parent.find('.JSproducts_slick').removeClass().addClass('JSproduct-five-slick');

			siblings_parent.addClass('parent-container container clearfix JSwrapp-two-types');
			siblings_parent.append(elem);
		}
	});
	
// PRODUCTS SLICK SLIDER 
	 if ($('.JSproducts_slick')[0]) { 
		$('.JSproducts_slick').slick({ 
			autoplay: true, 
			slidesToShow: 6,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 1600,
				  settings: {
					slidesToShow: 5,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 1200,
				  settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 991,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 550,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		}); 
	 }

	 if ($('.JSproduct-one-slick')[0]) { 
		$('.JSproduct-one-slick').slick({ 
			autoplay: true, 
			slidesToShow: 1,
			slidesToScroll: 1, 
			fade: true,
			responsive: [
				{
				  breakpoint: 1200,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 991,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					fade: false
				  }
				},
				{
				  breakpoint: 550,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					fade: false
				  }
				}
			]
		}); 
	 }
	  if ($('.JSproduct-five-slick')[0]) { 
		$('.JSproduct-five-slick').slick({ 
			autoplay: true, 
			slidesToShow: 5,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 1600,
				  settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 1200,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 991,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 550,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		}); 
	 }	
  
	// if ($('.JSBrandSlider')[0]){
	// 	$('.JSBrandSlider').slick({
	// 		autoplay: true,
	// 	    infinite: true,
	// 	    speed: 600,
	// 	    arrows: true,
	// 		slidesToShow: 5,
	// 		slidesToScroll: 3, 
	// 		responsive: [
	// 			{
	// 			  breakpoint: 1100,
	// 			  settings: {
	// 				slidesToShow: 3,
	// 				slidesToScroll: 3,
	// 				infinite: true,
	// 				dots: false
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 800,
	// 			  settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 2
	// 			  }
	// 			},
	// 			{
	// 			  breakpoint: 550,
	// 			  settings: {
	// 				slidesToShow: 2,
	// 				slidesToScroll: 2
	// 			  }
	// 			}
	// 		]
	// 	});
	// }
	
	if ($('.JSblog-slick').length){
		$('.JSblog-slick').slick({
			autoplay: true,
		    infinite: true,
		    speed: 600, 
			slidesToShow: 5,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 1200,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				  }
				},
				{
				  breakpoint: 991,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				},
				{
				  breakpoint: 550,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		});
	}

// } 	// START PAGE END ==============
 
	// CATEGORIES - MOBILE
	if ($(window).width() < 991 ) {
		$('.JScategories .fa-bars').click(function () { 
	    	$('.JSlevel-1').slideToggle();
		});
	}

	$('.JSlevel-1 li').each(function(i, li){
		if($(li).children('ul').length > 0){
			$(li).append('<span class="JSsubcat-toggle"><i class="fas fa-chevron-down"></i></span>');
		}
	}); 

	$('.JSsubcat-toggle').on('click', function() {
	 	$(this).toggleClass('rotate');
	    $(this).siblings('ul').slideToggle();
		$(this).parent().siblings().children('ul').slideUp();
		$(this).parent().siblings().find('.JSsubcat-toggle').removeClass('rotate');
	}); 
 
 
 // SOCIAL ICONS
	$('.social-icons .facebook').append('<i class="fab fa-facebook-f"></i>');
	$('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
	$('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
	$('.social-icons .skype').append('<i class="fab fa-skype"></i>');
	$('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');
 	$('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
	$('.social-icons .youtube').append('<i class="fab fa-youtube"></i>'); 

// PRODUCT PREVIEW IMAGE
    if ($(".JSproduct-preview-image")[0]){
		jQuery(".JSzoom_03").elevateZoom({
			gallery:'gallery_01', 
			cursor: 'pointer',  
			imageCrossfade: true,  
			zoomType: 'lens',
			lensShape: 'round', 
			lensSize: 200, 
			borderSize: 1, 
			containLensZoom: true
		}); 
		jQuery(".JSzoom_03").bind("click", function(e) { var ez = $('.JSzoom_03').data('elevateZoom');	$.fancybox(ez.getGalleryList()); return false; });
    } 

 // GALLERY SLIDER
	(function(){
 		var img_gallery_href = $('.JSimg-gallery'),
 			img_gallery_img = img_gallery_href.children('img'),
 			main_img = $('.JSmain_img'),
 			modal_img = $('#JSmodal_img'),
 			modal = $('.JSmodal'),

 			img_arr = [], 
			next_prev = 0;

		img_gallery_href.eq(0).addClass('galery_Active');

		img_gallery_href.on('click', function(){
			var img = $(this).children('img');
			main_img.attr({ src: img.attr('src'), alt: img.attr('alt') });
			$(this).addClass('galery_Active').siblings().removeClass('galery_Active');
		});

		

		main_img.on('click', function(){
			var src = $(this).attr('src');
			if (src.toLowerCase().indexOf('no-image') <= 0){
				modal_img.attr('src', src);

				$('.JSmodal').show();

				//IF MAIN PHOTO (WHEN CLICKED ON IT) IS EQUAL TO ANY OF THE THUMBNAILS, FIND HER
				// ORDINAL NUMBER AND THEN ADD THAT NUMBER TO THE NEXT PREV

				img_gallery_img.each(function(i, img){
					img_arr.push($(img).attr('src'));  
	    			if( src ===  $(img).attr('src')) {
						next_prev = img_gallery_img.index(this);
	    			}
					
				});
			} 
		});

		$('.JSclose-modal').on('click', function(){
			modal.hide();
		});

		$(document).on('click', function(){
			if (modal.css('display') == 'block')  {
				$('body').css('overflow', 'hidden');
			} else {
				$('body').css('overflow', '');  
			} 
		});

		modal.on('click', function(e){
			if ( ! $('.JSmodal img, .JSmodal .btn, .JSmodal .btn > *').is(e.target)) {
				modal.hide();
				modal_img.attr('src', '');
			} 
		}); 

		img_gallery_img.each(function(i, img){
			img_arr.push($(img).attr('src'));  
		});


		$(document).on('click','.JSleft_btn',function(){
	        if (next_prev == 0) {
	          next_prev = img_gallery_img.length -1; 
	        } else {
	          next_prev --;
	          modal_img.attr('src', img_arr[next_prev]);
	        } 
	        modal_img.attr('src', img_arr[next_prev]);
	    });  
        
	    $(document).on('click','.JSright_btn',function(){

	        if (next_prev != img_gallery_img.length -1) {
	            next_prev++; 
	          } else {
	            next_prev = 0;
	        } 
	        modal_img.attr('src', img_arr[next_prev]);

	    });  


		if (img_gallery_href.length > 1) {
			$('.modal-cont .btn').show();
		}
 
	}());

// ======== SEARCH ============
 
	var searchTime;
	$('#JSsearch2').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch2').val() != ''){
        	$(".JSsearch-button2").trigger("click");
    	}
    	if($('#elasticsearch').val() == 1){
			timer2();
    	}else{	
			clearTimeout(searchTime);
			searchTime = setTimeout(timer2, 300);
    	}
	});
 
	$('.JSSearchGroup2').change(function(){	timer2(); });

	function timer2(){
		var search_length = 2;
		if($('#elasticsearch').val() == 1){
			search_length = 0;
		}
		var search = $('#JSsearch2').val();
		if (search != '' && search.length > search_length) {
			$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup2').val()}, function (response){
				if(response != ''){
					$('.JSsearch_list').remove();
					$('.JSsearchContent2').append(response);
				}
			});
		} 
	}

	$('html :not(.JSsearch_list)').on("click", function() {
		$('.JSsearch_list').remove();
	});

	$('.warranty_modal_open').on('click', function() {
		$('.warranty_modal').show();
		$('.warranty_modal_overlay').show();
	})

	$('.warranty_modal_close').on('click', function() {
		$('.warranty_modal').hide();
		$('.warranty_modal_overlay').hide();
	})

	$(".JSCheckWarranty").on("click", function(){
	    var serijskiBroj = $("input[name='serijski_broj']").val();
	    $.ajax({
	        type: "POST",
	        url: base_url + "check-warranty", 
	        data: {
	            serijski_broj: serijskiBroj
	        },
	        dataType: 'json', 
	        success: function(response){
	            $(".warranty_msg").html(response).toggleClass('error-text', false);
	        },
	        error: function(xhr, status, error){
	            $(".warranty_msg").text(xhr.status === 404 ? 'Za uneti serijski broj ne postoji garancija' : 'Došlo je do greške')
	                .toggleClass('error-text', true);
	        }
	    });
	});

		 
// RATING STARS
	$('.JSrev-star i').each(function(i, el){
		var val = 0;
		$(el).on('click', function(){    
			$(el).addClass('fas').removeClass('far'); 
		    $(el).prevAll('i').addClass('fas').removeClass('far'); 
			$(el).nextAll('i').removeClass('fas').addClass('far'); 
		 	if (i == 0) { val = 1; }
		 	else if(i == 1){ val = 2; }
		 	else if(i == 2){ val = 3; }
		 	else if(i == 3){ val = 4; }
		 	else if(i == 4){ val = 5; } 
		 	$('#JSreview-number').val(val); 
		}); 
	}); 
 
  
	$('.JStoggle-btn').on('click', function(){
		$(this).closest('div').find('.JStoggle-content').toggleClass('hidden-small'); 
	});
	 
 // RESPONSIVE NAVIGATON
 	$(".resp-nav-btn").on("click", function(){  
		$("#responsive-nav").toggleClass("openMe");		 
	});
	$(".JSclose-nav").on("click", function(){  
		$("#responsive-nav").removeClass("openMe");		 
	});
 
// SCROLL TO TOP 
 	$(window).scroll(function () {
        if ($(this).scrollTop() > 150) {
            $('.JSscroll-top').css('right', '20px');
        } else {
            $('.JSscroll-top').css('right', '-70px');
        }
    });
 
    $('.JSscroll-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    }); 
    
// filters slide down
	$(".JSfilters-slide-toggle").click(function(){
	    $(this).next(".JSfilters-slide-toggle-content").slideToggle('fast');
	});

	if ($('.selected-filters li').length) {
		$('.JShidden-if-no-filters').show();
	} 

	if($(window).width() < 991) { 
	
		if (!$('.filters').find('ul').children().length) {
			$('.JS-mob-o-filters').hide();
		} 

		$('.JS-mob-o-filters').on('click', function() { $('#product-page').addClass('JS-active-filters'); });

		$('.JS-mob-c-filters').on('click', function() { $('#product-page').removeClass('JS-active-filters'); });

		// OPEN IF CLICKED ON INPUT FITLER
		function checkedFilters(){
			var inputs = $('.JSfilters-slide-toggle-content label input'); 

			inputs.on('click', function(){
				sessionStorage.setItem('active-filter', '1')
			}); 
			if( sessionStorage.getItem('active-filter') ) {
				$('#product-page').addClass('JS-active-filters');
			} 
			$('.JS-mob-c-filters, .JSreset-filters-button').on('click', function() {
				sessionStorage.removeItem('active-filter')
			});

			// OPEN CHECKED FILTERS

			inputs.each(function(i, element){
				var input = $(element);

				if (input.is(':checked')) {  
					$(this).closest('div').show();
				} 
			});
		}
		checkedFilters();

 	}
 
 // SELECT ARROW - FIREFOX FIX
	// $('select').wrap('<span class="select-wrapper"></span>');
	
 
 /*======= MAIN CONTENT HEIGHT ========*/
	if ($(window).width() > 1024 ) {
		$('.JScategories').on('mouseover', function(){
			if ($('.d-content').height() < $('.JSlevel-1').height()) { 
				$('.d-content').height($('.JSlevel-1').height());
			}
		}).on('mouseleave', function(){
			$('.d-content').height('');
		});
	} 
 

// POPUP BANNER
	if($('body').is('#start-page') && !(sessionStorage.getItem('popup')) ){   
	 	setTimeout(function(){ 
		 	$('.JSfirst-popup').animate({ top: '50%' }, 700);
		}, 1500);   

		 $(document).on('click', function(e){
		 	var target = $(e.target);
		 	if(!target.is($('.first-popup-inner img'))) { 
		 		$('.JSfirst-popup').hide();
		 	} 
		 }); 
		 if ($('.JSfirst-popup').length) {
	    	sessionStorage.setItem('popup','1');
		 }
	}


	if ($(window).width() > 991 ) {
	// CATEGORIES WRAP
		$('.JSlevel-1 li').each(function () {	
			var subitems = $(this).find(".JSlevel-2 > li");
			for(var i = 0; i < subitems.length; i+=4) {
				subitems.slice(i, i+4).wrapAll("<div class='clearfix'></div>");
			}
		});
	}

	if ($(window).width() > 768 ) {
	// FOOTER COLS CLEARFIX
	  	(function(){
	  		var cols = $('.JSfooter-cols > div');
	  		for(var i = 0; i < cols.length; i+=3){
	  			cols.slice(i, i+3).wrapAll('<div class="clearfix JSfooter-secs"></div>');
	  		}
  		}()); 
  		
  		var third_div = $('.JSfooter-secs').eq(0).children('div').eq(2);
  		if(third_div.length) {
  			$(third_div).append($('.e-trustmark'));
  		} 
	}

 // LEFT & RIGHT BODY LINK
	if ($(window).width() > 1024 ) {  
		var right_link = $('.JSright-body-link'),
			left_link = $('.JSleft-body-link'),
			main = $('.JSmain'); 

		setTimeout(function(){ 
			main.before((left_link).css('top', main.position().top + 'px'));
			main.after((right_link).css('top', main.position().top + 'px'));
			left_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 
			right_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 
		}, 1000); 
  	} 
 

// SEARCH BY GROUP TEXT
	function selectTxt(){    
		var txt = $('.JSSearchGroup2 option:first').text(trans('Sve')); 
		// if ($(window).width() > 1024 ){ 
		// 	txt.text('Pretraga po kategorijama');  
		// }else{
		// 	txt.text('po kategorijama');  
		// }
	}
	selectTxt();


	// GENERIC CAR TITLE
	$('.generic_car li').each(function(i, li){
		if ($(li).text().trim().length > 20) {
			$(li).attr('title', $(li).text().trim());
		} 
	});

	//order poll modal
	$('.JSOrderPoll').click(function() {
		var order_id = $(this).data('id');
		
		$.ajax({
			type: "POST",
			url: base_url + 'poll-content',
			data: { order_id: order_id },
			success: function(response) {
				$('#JSOrderPollContent').html(response);
				$('#JSOrderPollModal').modal('show');
			}
		});
	});
 	
 	// COOKIES
 	$(function(){
		function getCookie(cookieParamName){
			if(document.cookie.split('; ').find(row => row.startsWith(cookieParamName))) {
				return document.cookie
				  .split('; ')
				  .find(row => row.startsWith(cookieParamName))
				  .split('=')[1];
			}else{
				return undefined;
			}
		}
		function setCookie(cookieParamName,cookieParamValue,expireDays){
			var now = new Date();
			var time = now.getTime();
			var expireTime = time + 1000*86400*expireDays;
			now.setTime(expireTime);
			document.cookie = cookieParamName+"="+cookieParamValue+";expires="+now.toUTCString()+"; path=/;";
		}

		$("#alertCookiePolicy").hide()
		if (getCookie('acceptedCookiesPolicy') === undefined){
			//console.log('accepted policy', chk);
			$("#alertCookiePolicy").show(); 
		}
		$('#btnAcceptCookiePolicy').on('click',function(){
			//console.log('btn: accept');
			setCookie('acceptedCookiesPolicy','yes',30);
		});
		$('#btnDeclineCookiePolicy').on('click',function(){
			//console.log('btn: decline');
			setCookie('acceptedCookiesPolicy','no',30);
		});
 	});


 	// COOKIES TEXT
 	(function(){
	 	var cookiesBtn = $('.JScookiesInfo_btn');
		cookiesBtn.text('Prikaži detalje');
		cookiesBtn.on('click', function(){
			cookiesBtn.next().slideToggle('fast');
			cookiesBtn.text(function(i, text){
		   		return text == 'Prikaži detalje' ? 'Sakrij detalje' : 'Prikaži detalje'
			}); 
		});
	}());

 	// SHOW COOKIES
	setTimeout(function(){ 
	 	$('.JScookies-part').animate({ bottom: '0' }, 700);
	}, 1500);   

    // CATEGORY CLICK
	// if($(window).width() > 991) {
	//     $('.categories-title').on('click', function(){				
	//     	$(".JSlevel-1").addClass('active');
	// 		$('body').toggleClass('cat-bg')
	// 	});

	// 	$('.JStoggle-btn').on('click', function(){
	// 		$(this).toggleClass('hidden-small'); 
	// 	});

	// 	$(document).on('click', function(e){
	// 	 	var target = $(e.target);
	// 	 	if(!target.is($('.JSlevel-1, .JSlevel-1 *, .JScategories, .JScategories *, .categories-title, .categories-title *'))) { 
	// 			$(".JSlevel-1").removeClass("openCats");
	// 			$('body').removeClass('cat-bg')
	// 	 	} 
	// 	}); 
	// }

	 // RESPONSIVE NAVIGATON
	 if($(window).width() > 991) {


 	$(".categories-title").on("click", function(){  
	    $(".JSlevel-1").toggleClass('active');
		// $("#responsive-nav").toggleClass("openCats");
		$('body').addClass('cat-bg');

	});
	$(".JSclose-nav").on("click", function(){  
		$("#responsive-nav").removeClass("openCats");	
		$('body').removeClass('cat-bg');
		$('.JSlevel-1').removeClass('active');
		$('.JSlevel-2').hide();

	});

	$(document).on('click', function(e) {
		var target = $(e.target);
	 	if(!target.is($('#responsive-nav *') )) { 
			$("#responsive-nav").removeClass("openCats");		
			$('body').removeClass('cat-bg');
			$('.JSlevel-1').removeClass('active')
			$('.JSlevel-2').hide();
	 	} 
	});
	
	$('.JSlevel-1 > li').on('mouseover', function() {
	    $(this).find('ul').show();
	    $(this).siblings().find('ul').hide();
	    if($(window).width() > 991) {
		    $(this).addClass('mouseovered');
		    $(this).siblings().removeClass('mouseovered');
	    }

	 	// $(this).toggleClass('rotate');
		// $(this).find('ul').siblings('ul').hide();
		// $(this).parent().siblings().find('.JSsubcat-toggle').removeClass('rotate');
	}); 
}


    // MOBILE USER CLICK
   if($(window).width() < 991) {
		$('.JSopen-user').on('click', function(){
			$('.JSmobile-user').toggle();
		});
	}

	// PRODUCT CART AMOUNT 
	$(document).on('click', '.JSadd-num', function(){
		var	input = $('.JSadd-num').siblings('input'); 
		if(!isNaN(input.val())){	
			var curr_val = parseInt(input.val()); 
			input.val(curr_val + 1); 
		}
	});
	$(document).on('click', '.JSsub-num', function(){
		var	input = $('.JSsub-num').siblings('input'); 
		if(!isNaN(input.val())){	
			var curr_val = parseInt(input.val());
			if(curr_val > 1){
				input.val(curr_val - 1); 
				return;
			}
		}
	}); 

	// INSURE JUST ONE CLIK IN CART
	var count = 0;
    $('#JSOrderSubmit').click(function(){
        count++;
        if (count > 1) {
            $(this).prop("disabled", true);
        }
    });
	
	// JSDelivery Input on Change
	if($('.JSdeliveryInput').val() == 2) {
		$('.JSdeliveryMP').show();
	} else {
		$('.JSdeliveryMP').hide();
	}
	$('.JSdeliveryInput').on('change', function() {
		if($(this).val() == 2) {
			$('.JSdeliveryMP').show();
		} else {
			$('.JSdeliveryMP').hide();
		}
	})

	// B2C RMA
	$('#preuzeti').on('change', function() {
		if($('#preuzeti').is(':checked')) {
			$("input[name='preuzeti']").val(1);
		} else {
			$("input[name='preuzeti']").val(0);
		}
	});


	// CART TERMS ACCEPT
		$('.JScall-alert').on('click', function(){
			bootboxDialog({ message: "<p>" + trans('Prihvatite opšte uslove poslovanja') + ".</p>" }); 
			$('.JScheck-term').addClass('JSterm-blink');
		});

		if($('select[name="web_nacin_placanja_id"]').val() == '3') {
			$('.JScheck-term, .JScall-alert').addClass('hidden');
		}
		$('select[name="web_nacin_placanja_id"]').on('change', function(){
			if($(this).val() == '3'){
				$('.JScheck-term, .JScall-alert').addClass('hidden')
			}
			else {
				$('.JScheck-term, .JScall-alert').removeClass('hidden');
			}
		});

		$('.JScheck-term').on('change', function(){
			if(!$('.JScheck-term').find('input').prop('checked')) {
				sessionStorage.removeItem('checkedTerm');
				$('.JScall-alert').show();
			}else {
				sessionStorage.setItem('checkedTerm', '1');
				$('.JScall-alert').hide();
				$('.JScheck-term').removeClass('JSterm-blink');
			}
		});

		if (sessionStorage.getItem('checkedTerm')){ 
			$('.JScheck-term').find('input').prop('checked', 'ckecked');
			$('.JScall-alert').hide();
		} 

		if (! $('.JScheck-term').length) { sessionStorage.removeItem('checkedTerm'); }
		
		// END CART TERMS ACCEPT
		
	// Open modal if user make mistake 
	 if($('#checkState').find('.red-dot-error').text()) {
	 	$('#checkState').modal();
	 }

	$('.JSenquiry').click(function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		$('#JSStateArtID').val(roba_id);
		$('#checkState').modal('show');
		console.log($('#JSStateArtID').val());

	});

	// BOLD SPECIFIC PAGE IN HEADER
	$('.main-menu > li > a').each(function(){
	    if($(this).text().trim().toLowerCase() == "blog" || $(this).text().trim().toLowerCase() == "outlet") {
	        $(this).css('font-weight', '600');
	    } 
	});

	// COMMENTS COUNTER
	if($('.comments').length) {
	    var comment_count = $('.comments').children().length;
	    $('.JScomment-number div').text(comment_count)
	} else {
		$('.JScomment-number div').text('0')
	}
	
}); // DOCUMENT READY END

// CATEGORIES TOP IF ADMIN ON
if($('#admin-menu').length) {
	$(".JSlevel-1").css("top", $('#admin-menu').outerHeight());
 }
 

// ZOOMER 
document.onreadystatechange = function(){
    if (document.readyState === "complete") { 

		if($('body').is('#artical-page')){	
			$('.zoomContainer').css({
				width: $('.zoomWrapper').width(),
				height: $('.zoomWrapper').height()
			}); 
				
			var art_img =  $('#art-img').attr('src');
			if (art_img.toLowerCase().indexOf('no-image') >= 0){
				$('.zoomContainer').hide();
			} 
		// ARTICLE ZOOMER		
			$('.zoomContainer').each(function(i, el){
				if ( i != 0) {
					$(el).remove();
				}
			});
		}  

		$('.product-image').each(function(i, img){
			if ($(img).attr('src') != 'quick_view_loader.gif') {
				$(img).attr('src', '/images/no-image.jpg');
			}
		});  


// OVERLAPING CATEGORIES
		// (function(){
		//   	var categ = $('.JSlevel-1'),
		//   		categ_offset_bottom = categ.offset().top + categ.outerHeight(true); 
 
	 //  		$('.JSproducts_slick').each(function(i, el){
	 //  			if (categ_offset_bottom >= $(el).offset().top) {
	 //  				$(el).closest('.JSaction_offset').css('marginLeft', categ.width() + 'px');
	 //  			}  
	 //  		});

		// }());
 
// SECTION OFFSET
		// if($('body').is('#start-page') && $(window).width() > 991){		
		// 	if($('.JSd_content').length) {
		// 		$('.JSd_content').each(function(i, el){  
		//  			var lvl_1 = $('.JSlevel-1'),
		//  				main_slider = $('.JSmain-slider'),
		//  				parent_cont = $('.parent-container');

		//  			if ($(el).offset().top <= lvl_1.offset().top + lvl_1.outerHeight()) { 

		//  				if((lvl_1).children().length){
		// 					// $(el).css('margin-left', lvl_1.outerWidth()); 
		// 				}	 

		// 				if ($(this).closest(parent_cont).hasClass('container-fluid')) {

		// 					if (!$(el).find(main_slider).length) {
		// 			 			$(this).closest(parent_cont).removeClass('container-fluid no-padding').addClass('container');

		// 			 		} else {
		// 			 			$(el).css('margin-left', ''); 	 
		// 			 		}

		// 		 		}
		// 			}  

	 
		// 			$(el).css('width', 'auto');  
				 
		// 			$('.JSloader').hide();
		// 		}); 
		// 	} 
		// 	else {
		// 		$('.JSloader').hide();
		// 	}
		// }
 
	} 
}
 
 