	base_url = $('#base_url').val();
	in_stock = $('#in_stock').val();
	var flag = 0;

	$('.JScategoryAjax').on('click', function() {
		$(this).addClass('activated');
	});

	$(document).ready(function() {
		if($(window).width() > 991) {
			$(document).on('click', '.categories-title', function() {
				flag = 1;
				$('.JSlevel-1 > li').on('mouseover', function() {
					$(this).find('ul').show();
					$(this).siblings().find('ul').hide();
					if($(window).width() > 991) {
						$(this).addClass('mouseovered');
						$(this).siblings().removeClass('mouseovered');
					}
				});
			})
		} else {
			$(document).on("click", '.JSclose-nav', function(){  
				$("#responsive-nav").removeClass("openMe");		
				$('.JSsubcat-toggle').removeClass('rotate'); 
			});
		}

		$(document).on("click", '.JSclose-nav', function(){  
			$("#responsive-nav").removeClass("openCats");	
			$('body').removeClass('cat-bg');
			$('.JSlevel-1').removeClass('active');
			$('.JSlevel-2').hide();
		});
	})

	$(document).ready(function(){
		$.post(base_url + 'category-ajax', {}, function (response){
			if($('.JScategoryAjax').hasClass('activated')) {
				flag = 1
			}
			$('.JScategoryAjax').replaceWith(response);
			if($(window).width() < 991) {
				$('.JSlevel-1 li').each(function(i, li){
					if($(li).children('ul').length > 0){
						$(li).append('<span class="JSsubcat-toggle"><i class="fas fa-chevron-down"></i></span>');
					}
				}); 

				$('.JSsubcat-toggle').on('click', function() {
					$(this).toggleClass('rotate');
				   $(this).siblings('ul').slideToggle();
				   $(this).parent().siblings().children('ul').slideUp();
				   $(this).parent().siblings().find('.JSsubcat-toggle').removeClass('rotate');
			    }); 
			}
			if(flag == 1) {
				$('.JSlevel-1').toggleClass('active');

				$('.JSlevel-1 > li').on('mouseover', function() {
					$(this).find('ul').show();
					$(this).siblings().find('ul').hide();
					if($(window).width() > 991) {
						$(this).addClass('mouseovered');
						$(this).siblings().removeClass('mouseovered');
					}
				});
			}
		});
	}());



	$(document).ready(function() { 

		// $('.JScomment_add').click(function() { 
		// 	var ocena = $('#JSreview-number').val();
		// 	var ime = $('#JScomment_name').val();
		// 	var pitanje = $('#JScomment_message').val();
		// 	var roba_id = $('#JSproduct_id').val();

		// 	if (ime == '' || ime == ' ') {
		// 		$('#JScomment_name').css("border", "1px solid red").focus().fadeIn(200);
		// 	} else if (pitanje == '' || pitanje == ' ') {
		// 		$('#JScomment_message').css("border", "1px solid red").focus().fadeIn(200);
		// 	} else {
		// 		$.ajax({
		// 			type: "POST",
		// 			url: base_url + 'coment_add',
		// 			data: { ime: ime, pitanje: pitanje, roba_id: roba_id, ocena: ocena },
		// 			success: function(msg) { 

		// 				bootbox.alert({
		// 					size: "small",   
		// 					message: "<p>" + trans('Vaš komentar je poslat') + "</p>",
		// 					className: 'JSauto-close'	 	 
		// 				}); 

		// 				$('#JScomment_name, #JScomment_message, #JSreview-number').val(''); 
		// 			}
		// 		});
		// 	}
		// });
 

	 // SLAJDER ZA CENU  
	if(typeof min_web_cena_init !== 'undefined' && typeof max_web_cena_init !== 'undefined'){

		$( "#JSslider-range" ).slider({
			range: true,
			min: min_web_cena_init,
			max: max_web_cena_init,
			values: [ min_web_cena, max_web_cena ],
			
			slide: function(event, ui) { 
				sliderPrice("#JSamount","#JSslider-range");  
			},
			
			stop: function(event,ui) {
				var url = $("#JSfilters-url").val(),	
				    proizvodjaci = $("#JSmanufacturer_ids").val(),
				    karakteristike = $("#JSmanufacturer_attr").val();
				    grupa = $("#JSGroups_ids").val();

				window.location.href=base_url+url+'?m='+proizvodjaci+'&ch='+karakteristike+'&pr='+ui.values[ 0 ]+'-'+ui.values[ 1 ] +'&gr='+grupa;
		
			}
		}); 
		// var valuta = $("#JSExcange").val();
		// alert(valuta);
		sliderPrice("#JSamount","#JSslider-range");

		function sliderPrice(el, slide_el, valuta){  
		
			$(el).html("Cena od <b>" + Math.round(($(slide_el).slider("values", 0) / kurs)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') +
		"</b> do <b>" + Math.round(($(slide_el).slider("values", 1) / kurs)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + "</b>");
		}
	}


	$('.JSGrupa_id').on('click', function(){
		var url = $("#JSfilters-url").val()
		var proizvodjaci = $("#JSmanufacturer_ids").val()
		var karakteristike = $("#JSmanufacturer_attr").val()

		window.location.href = base_url + url + '?m=' + proizvodjaci + '&ch=' + karakteristike + '&pr=0-0' + '&gr=' + $(this).data('key');
	});
	
		$('.JSreset-filters-button').click(function() {
			var url = $("#JSfilters-url").val();
			window.location.href = base_url + url;
		});


		$('.JSfilter-close').click(function() {
			var tip = $(this).data("type");
			var rb = $(this).data("rb");
			var element = $(this).data("element");
			var url = $("#JSfilters-url").val();
			var niz_proiz = $("#JSmanufacturer_ids").val();
			var niz_kara = $("#JSmanufacturer_attr").val();
			var proizvodjaci = "";
			var karakteristike = "";
			var grupa = "";

			if (tip == 1) {
				karakteristike = niz_kara;
				niz = niz_proiz.split("-");
				if (niz.length > 1) {
					br = 0;
					niz.forEach(function(el) {
						if (el != element) {
							if (br == 0) {
								proizvodjaci += el;
							} else {
								proizvodjaci += "-" + el;
							}
							br = br + 1;
						}
					});
				} else {
					proizvodjaci = 0;
				}
			}
			if (tip == 2) {
				proizvodjaci = niz_proiz;
				niz = niz_kara.split("-");
				if (niz.length > 1) {
					br = 0;
					niz.forEach(function(el) {
						if (!element.toString().split('-').includes(el)) {
							if (br == 0) {
								karakteristike += el;
							} else {
								karakteristike += "-" + el;
							}
							br = br + 1;
						}
					});
					if(karakteristike == ""){
						karakteristike = 0;
					}
				} else {
					karakteristike = 0;
				}
			}
			//alert(base_url+'filter/'+url+'/'+proizvodjaci+'/'+karakteristike);
			window.location.href = base_url + url + '?m=' + proizvodjaci + '&ch=' + karakteristike + '&pr=0-0' + '&gr=0';
		});
		$('.JScompare').click(function() {
			var $this = $(this),
				dataId = $this.data('id');
			if ($(this).hasClass('active')) {
				$.post(base_url + 'clearCompare', { clear_id: dataId }, function(response) {
					if(response.count_compare > 0){
						$('#JScompareArticles').removeClass('show-compered');
						$('#JScompareArticles').addClass('show-compered-active');
					}else{
						$('#JScompareArticles').removeClass('show-compered-active');
						$('#JScompareArticles').addClass('show-compered');
					}
				});
				$this.removeClass('active');
			} else {
				$(this).addClass('active');
				$.post(base_url + 'compareajax', { id: dataId }, function(response) {
					if(response.count_compare > 0){
						$('#JScompareArticles').removeClass('show-compered');
						$('#JScompareArticles').addClass('show-compered-active');
					}else{
						$('#JScompareArticles').removeClass('show-compered-active');
						$('#JScompareArticles').addClass('show-compered');
					}
				});
				$('#JScompareTable').text('');
			}
		});
		function formatNumber(num) {

 		 return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
		}
	
		function currencyFormat(num) {
		  return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')+' rsd.'
		}
		$('.JSinterest').on('change',function() {
			var kamata = $(this).val()/100;
			var cena = $(this).closest('.product-preview-info').find('.JSweb_price').data('cena');
			var akc_cena = $(this).closest('.product-preview-info').find('.JSaction_price').data('action_price');
			
			cena = Math.round(cena);
			cena = cena+(cena*kamata);
			cena = currencyFormat(cena) 
			$(this).closest('.product-preview-info').find('.JSweb_price').text(cena);	 

			akc_cena = Math.round(akc_cena);
			akc_cena = akc_cena+(akc_cena*kamata);
			akc_cena = currencyFormat(akc_cena) 
			$(this).closest('.product-preview-info').find('.JSaction_price').text(akc_cena);	
		});

		$('#JScompareArticles').click(function() {
			$.post(base_url + 'compare', { id: $(this).data('id') }, function(response) {
				$('#JScompareTable').html(response.content);
			});
		});
		$(document).on('click', '.JSclearCompare', function() {
			var $this = $(this),
				clearDataId = $this.data('id');
			$('.JScompare').filter('[data-id="' + clearDataId + '"]').removeClass('active');
			$.post(base_url + 'compare', { clear_id: clearDataId }, function(response) {
				$('#JScompareTable').html(response.content);
				if(response.count_compare > 0){
					$('#JScompareArticles').removeClass('show-compered');
					$('#JScompareArticles').addClass('show-compered-active');
				}else{
					$('#JScompareArticles').removeClass('show-compered-active');
					$('#JScompareArticles').addClass('show-compered');
				}
			});
		});
	});

	//search
 
	function search() {
		search = document.getElementById('JSsearch').value;
		grupa_pr_id = $('.JSSearchGroup').val();
		search = encodeURIComponent(search);
		if(search != ''){
			window.location.href = base_url + "search/" + search + "/" + grupa_pr_id;
		}
	}

	function search2() {
		search = document.getElementById('JSsearch2').value;
		search = search.replace(/\//g, '+')

		grupa_pr_id = $('.JSSearchGroup2').val();
		search = encodeURIComponent(search);
		if(search != ''){
			window.location.href = base_url + "search/" + search + "/" + grupa_pr_id;
		}
	}

	function newsletter() {
		var email = $('#newsletter').val();
		if (isValidEmailAddress(email) == true) {
			/*$.post(base_url+'newsletter',{ emeil:email }, success: function( msg ) {
		alert( msg );
		});*/
			$.ajax({
				type: "POST",
				url: base_url + 'newsletter',
				data: { email: email },
				success: function(msg) {  
		  
					bootboxDialog({ message: "<p>" + msg + "</p>" }, 2200); 
					$('#newsletter').val('');
				}
			});
		} else {
	 
			bootboxDialog({ message: "<p>" + trans('E-mail adresa nije validna') + ".</p>" });

		}
	} 

	$('#JSpassword_login').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSpassword_login').val() != ''){
        	$("#login").trigger("click");
    	}
    });
	$('#JSemail_login').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSemail_login').val() != ''){
        	$("#login").trigger("click");
    	}
    });

	function user_login() {
		email = document.getElementById('JSemail_login').value;
		password = document.getElementById('JSpassword_login').value;
		
			$.ajax({
				type: "POST",
				url: base_url + 'login',
				data: { email_login: email , lozinka_login:password},
				success: function(response) { 
					if(response.success){  
				  
						bootboxDialog({ message: "<p>" + trans('Uspešno ste se prijavili') + "!</p>" }); 

						location.reload();
					}else{
						var error = 'E-mail ili lozinka nisu validni!';
						if(response.errors.email_error){
							error = response.errors.email_error;
						}else if(response.errors.lozinka_error){
							error = response.errors.lozinka_error;
						}  
 
						bootboxDialog({ message: "<p>" + error + "</p>" }, 2200); 
 
					}
				}
			});
	}

	function user_forgot_password() {
		email = document.getElementById('JSemail_login').value;
			$.ajax({
				type: "POST",
				url: base_url + 'zaboravljena-lozinka-ajax',
				data: { email_fg: email },
				success: function(response) {
					if(response.success){
					
						$('#JSForgotSuccess').removeClass('JShidden-msg');
				
					} else { 	
				   
						bootboxDialog({ message: "<p>" + response.email_fg + "</p>" }); 

					}
				}
			});
	}

	function filters_send() {
		var strChoices = "0";
		var proiz = "0";
		var objCBarray2 = document.getElementsByName('proizvodjac[]');
		var objCBarray = document.getElementsByName('vrednost[]');
		var url = $("#JSfilters-url").val();
		var vrednost_arr = new Array();
		var proiz_arr = new Array();
		var p = 0;
		for (i = 0; i < objCBarray.length; i++) {
			if (objCBarray[i].checked) {
				var sub_vals = objCBarray[i].value.split('-');
				for (j = 0; j < sub_vals.length; j++) {
					vrednost_arr.push(sub_vals[j]);
				}
			}
		}
		if (vrednost_arr.length > 0) {
			strChoices = vrednost_arr.join('-');
		}


		for (i = 0; i < objCBarray2.length; i++) {
			if (objCBarray2[i].checked) {
				proiz_arr.push(objCBarray2[i].value);
			}
		}
		if (proiz_arr.length > 0) {
			proiz = proiz_arr.join('-');
		}

		//alert(base_url+'filters/'+url+'/'+proiz+'/'+strChoices);
		window.location.href = base_url + url + '?m=' + proiz + '&ch=' + strChoices + '&pr=0-0';
	}

	function check_fileds(polje) {
		polje2 = document.getElementById(polje).value;
		if (polje == 'email') {
			if (isValidEmailAddress(polje2) == false) {
				$('#email').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#email').css("border", "1px solid #ccc").fadeIn(200);
			}
		} else if (polje == 'JSwithout-reg-email') {
			if (isValidEmailAddress(polje2) == false) {
				$('#JSwithout-reg-email').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#JSwithout-reg-email').css("border", "1px solid #ccc").fadeIn(200);
			}
		} else if (polje == 'JScontact_email') {
			if (isValidEmailAddress(polje2) == false) {
				$('#JScontact_email').css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#JScontact_email').css("border", "1px solid #ccc").fadeIn(200);
			}
		} else {
			if (polje2 == '' || polje2 == '') {
				$('#' + polje).css("border", "1px solid red").focus().fadeIn(200);
			} else {
				$('#' + polje).css("border", "1px solid #ccc").fadeIn(200);
			}
		}
	}

	function meil_send() { 
		name = document.getElementById('JScontact_name').value;
		email = document.getElementById('JScontact_email').value;
		message = document.getElementById('message').value;
		captcha_string = document.getElementById('JSCaptchaValue').value;
		if (name.trim() == '') {
			$('#JScontact_name').css("border", "1px solid red").focus().fadeIn(200);
		} else if (isValidEmailAddress(email) == false) {
			$('#JScontact_email').css("border", "1px solid red").focus().fadeIn(200);
		} else {
			$.ajax({
				type: "POST",
				url: base_url + 'meil_send',
				data: { name: name, message: message, email: email, "captcha-string": captcha_string },
				success: function() {
	  
					bootboxDialog({ message: "<p>" + trans('Vaša poruka je poslata') + ".</p>" }); 

 					document.getElementById('JScontact_name').value = '';
					document.getElementById('JScontact_email').value = '';
					document.getElementById('message').value = '';
					document.getElementById('JSCaptchaValue').value = '';
					location.reload(true);
				}
			});
		}
	}
	//Proverava da li je polje prazno ili sadrzi nedozvoljene karaktere
	function isValidField(fields) {
		var check = true;
		var field = "";
		var script = /["-*<->-(){}`|^]+$/; // Invalid characters:  !"#$%^&*'() <=?> {}`|^ Valid characters: +-_., SPACE a-z A-Z 0-1
		for (i = 0; i < fields.length; i++) {
			var ind = fields[i].value.search(script);
			//For empty fields
			if ((fields[i].value == "") || (ind != -1)) {
				check = false;
				//Fokusira se na polje.Treba napraviti obavestenje za to polje, zvezdica ili nesto slicno
				fields[i].focus(fields[i].id);
				$('#' + fields[i].id).css("border", "1px solid red").next().fadeIn(200);
			} else {
				$('#' + fields[i].id).css("border", "1px solid gray").next().fadeOut(200);
			}
		}
		return check;
	}
	//Proverava da li je validna e-mail adresa
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	};
	//Dozvoljava samo numeric values
	function isNumberKey(charCode) {
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		return true;
	}

// ACTION COUNTDOWN 
var timer = setInterval(function(){
	var elem = $('.JSrok_akcije'),
   		now = new Date(); 
	  
	for(var i = 0; i < elem.length; i++) { 
		var end_date = new Date(elem.eq(i).val()).getTime(),
			t = end_date - now;

		if(t >= 0) { 
			var days = Math.floor(t / (1000 * 60 * 60 * 24)),
			    hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)),
			    mins = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60)),
			    secs = Math.floor((t % (1000 * 60)) / 1000);

			    //days = (days < 10) ? '0' + days : days;
				hours = (hours < 10) ? '0' + hours : hours;
				mins = (mins < 10) ? '0' + mins : mins;
				secs  = (secs < 10) ? '0' + secs : secs;

			var days_elem = elem.eq(i).closest('div').find('.JSdays').text(days + 'd'),
				hours_elem = elem.eq(i).closest('div').find('.JShours').text(hours + 'h'),
				mins_elem = elem.eq(i).closest('div').find('.JSminutes').text(mins + 'm'),
				secs_elem = elem.eq(i).closest('div').find('.JSseconds').text(secs + 's'); 
		}    
	}
	if (t < 0) { clearInterval(timer); } 
}, 1000);


// BOOTBOX PLUGIN DEFAULTS
// bootbox.setDefaults({     
	
// 	backdrop: true,   
// 	closeButton: false,  

// 	// onShown: function(e) {
//  //        setTimeout(() => {
//  //        	if ($('.bootbox').hasClass('JSauto-close')) { 
//  //        		bootbox.hideAll();  
//  //        	}
//  //        }, 1200);
//  //    } 
// });

function bootboxDialog(data, time = 1200){   
    var dialog = bootbox.dialog({ 
		message: data.message || 'The message must not be empty',
		size: data.size || 'small',
		className: data.className || 'JSauto-close', 
		closeButton: data.closeButton || false
    }); 

    setTimeout(function(){ 
        dialog.modal('hide');
    }, time);
}
