
base_url = $('#base_url').val();

function newsletter() {
	email = document.getElementById('newsletter').value;
	if (isValidEmailAddress(email) == true) {

		$.ajax({
			type: "POST",
			url: base_url + 'newsletter',
			data: { email: email },
			success: function(msg) {
				alertSuccess(msg);
			}
		});
	} else {
		alertError(trans("E-mail adresa nije validna")+".");
	}
}

  

function meil_send() { 
	var name = $('#JScontact_name').val(),
		email = $('#JScontact_email').val(),
		message = $('#message').val();

		if (name == '' || (email == '' || (! isValidEmailAddress(email))) || message == '') {
		
		alertError(trans("Niste popunili sva polja")+"."); 

	} else {
		$.ajax({
			type: "POST",
			url: base_url + 'meil_send',
			data: { name: name, message: message, email: email },
			success: function() {
				alertSuccess(trans('Vaša poruka je poslata')+'!');

				setTimeout(function() {
					location.reload();
				}, 1500);
			}
		});
	}
}

// VALIDATE INPUTS 
$('.JSvalidate').on('change', validate_Input);

function validate_Input(input){

	var input = $(this);

		if ( input.attr('name') == 'email') {

		if ( ! isValidEmailAddress(input.val())) {

			alertError(trans("E-mail adresa nije validna")+".");

		} else {

			input.css('border-color', '');
				
		}
	} 
}



function isValidEmailAddress(emailAddress) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(emailAddress).toLowerCase());
};

