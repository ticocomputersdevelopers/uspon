// PRIKAZ SVIH KATEGORIJA U PRODAVNICI = SVE KATEGORIJE 
$(document).ready(function () {

    smoothScroll(250);
    openAllCategoryList();
    
    var $body = $('body');
    var cat_sidebar_list = $('.category-sidebar__list');
    var cat_sidebar_list_Offset = cat_sidebar_list.offset().top;


    if ($body.hasClass('fixHead')){
        var fixedHeader = true;
    }else{
        var fixedHeader = false;
    }

    var sidebarMarginTop = 20;

    if (fixedHeader) {
        sidebarMarginTop = 80;
    }

    $(window).scroll(function () {

        var vScroll = $(window).scrollTop();

        if (fixedHeader) {
            // category sidebar fixed
            if(vScroll > cat_sidebar_list_Offset - sidebarMarginTop) {
                cat_sidebar_list.addClass('category-sidebar__list--fix').css("top","80px");
            }
            else {
                cat_sidebar_list.removeClass('category-sidebar__list--fix').css("top","-80px");
            }    
        } else{
            // category sidebar fixed
            if(vScroll > cat_sidebar_list_Offset - sidebarMarginTop) {
                cat_sidebar_list.addClass('category-sidebar__list--fix');
            }
            else {
                cat_sidebar_list.removeClass('category-sidebar__list--fix');
            }   
        }
    });

    // smoothScroll function is applied from the document ready function
    function smoothScroll (duration) {
        $('a[href^="#"]').on('click', function(event) {

            var target = $( $(this).attr('href') );
            
            if( target.length ) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top - sidebarMarginTop
                }, duration);
            }
            
        });
    }

    // category sidebar toggler
    function openAllCategoryList(){
        $('.category-sidebar__list__toggler').on('click', function(){
            $(this).parent().toggleClass('category-sidebar__list--open');
        });
    }
});



