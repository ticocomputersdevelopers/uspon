$(document).ready(function(){
	  $('.JSOrgjEnable').click(function () {
		var aktivan;
		if($(this).attr('checked')){
			aktivan = 0;
			$(this).removeAttr("checked");
			$(this).closest('li').find('.JSOrgjPrimary').attr("disabled",true);
		}else{
			aktivan = 1;
			$(this).attr('checked', '');
			$(this).closest('li').find('.JSOrgjPrimary').removeAttr("disabled");
		}

		$.post(base_url+'admin/b2b/settings', {action:'magacin_enable', aktivan:aktivan, id:$(this).data('id')}, function (response){});
   });

  $('.JSOrgjPrimary').click(function () {

			$('.JSOrgjEnable').removeAttr("disabled");
			$(this).closest('li').find('.JSOrgjEnable').attr("disabled",true);

		$.post(base_url+'admin/b2b/settings', {action:'magacin_primary', id:$(this).data('id')}, function (response){});
   });

   $('.JSNewMagtacinSave').click(function () {

		var value = $('#JSNewMagtacinVal').val();
		if(value != ''){

			$.post(base_url+'admin/b2b/settings', {action:'magacin_add', value: value}, function (response){
				alertify.success('Uspešno ste dodali magacin!')
				location.reload(true);
			});			
		}
   });

  $('.JSMagtacinDelete').click(function () {

		var orgj_id = $(this).data('id');
		if(orgj_id != ''){
			$.post(base_url+'admin/b2b/settings', {action:'magacin_delete', orgj_id: orgj_id}, function (response){
				alertify.success('Uspešno ste obrisali magacin!')
				location.reload(true);
			});
		}
   });

	$('.JSOptionId').click(function () {
		var option_id = $(this).data('id');
		var value = 0;
		if($(this).is(":checked")){
			value = 1;
		}
		$.post(base_url+'admin/b2b/settings', {action:'switch_option', option_id: option_id, value: value}, function (response){ });
	});

	$('.JSOptionIdStrSave').click(function () {
		var option_id = $(this).data('id');
		var value = $('#JSOptionIdStr-'+option_id).val();
		$.post(base_url+'admin/b2b/settings', {action:'save_str_option', option_id: option_id, value: value}, function (response){
			location.reload(true);
		});
	});

});