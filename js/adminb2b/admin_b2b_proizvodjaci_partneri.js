$(document).ready(function () {

//PARTNER
	$('#parner-change').on("change", function () {
		var partner_id = $(this).val();
		location.href = base_url+'admin/b2b/proizvodjaci/' + partner_id;
	});

// RABAT PROIZVODJACA
	var  lastVal ="";
	var ifEnter = 0;

	$('.JSrabat').focus(function () {
		lastVal = $(this).val();
	});

// $('.JSrabat').keydown(function(e){
// 	if(e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 190 && e.keyCode != 37 && e.keyCode != 39){
// 		e.preventDefault();
// 	}	
// });

	$('.JSrabat').keyup(function(e){ 

		if (e.keyCode == 38) {
			$(this).closest('tr').prev().find('.JSrabat').focus();
		}

		if (e.keyCode == 40) {
			$(this).closest('tr').next().find('.JSrabat').focus();
		}

		if(e.keyCode == 13) {

			if (lastVal != $(this).val()){

				var proizvodjac_id = $(this).data('id');
				var rabat = $(this).val();
				var partner_id = $('#parner-change').val();

				if(rabat != ''){

					ifEnter = 1;
					$(this).blur();

					$.post(
						base_url+'admin/b2b/ajax/rabat_proizvodjac_edit', {
							action: 'rabat_proizvodjac_edit',
							proizvodjac_id: proizvodjac_id,
							rabat: rabat,
							partner_id: partner_id
						}, function (response){
							if(response == 1) {
								alertify.success('Uspešno ste sačuvali rabat.');
							} else {
								alertify.error('Greška prilikom upisivanja u bazu.');
							}
						}
					);
				}

			}
		}
	});

	$("#parner-change").select2({
		placeholder: 'Izaberite partnera'
	});


	$('.JSrabat').blur(function(e){

		if (lastVal != $(this).val() && !(ifEnter)){

			var proizvodjac_id = $(this).data('id');
			var rabat = $(this).val();
			var partner_id = $('#parner-change').val();

			if(rabat != ''){

				$.post(
					base_url+'admin/b2b/ajax/rabat_proizvodjac_edit', {
						action: 'rabat_proizvodjac_edit',
						proizvodjac_id: proizvodjac_id,
						rabat: rabat,
						partner_id: partner_id
					}, function (response){
						if(response == 1) {
							alertify.success('Uspešno ste sačuvali rabat.');
						} else {
							alertify.error('Greška prilikom upisivanja u bazu.');
						}
					}
				);
			}
		} else if(ifEnter){
			ifEnter = 0;
		}
	});

	var partner_ids = new Array();
	var selected_partner_ids;

	$(function() {
		$( "#selectable" ).selectable({
			filter: 'tr',
			stop: function() {
				partner_ids = [];
				$("td").removeClass("ui-selected");
				$("td").find('input').removeClass("ui-selected");

				$( ".ui-selected", this ).each(function() {
					var id = $( this ).data("id");
					partner_ids.push(id);
				});
				selected_partner_ids = partner_ids;
			}
		});
	});

//RABAT PARTNERA
	$('#rabat_partner_edit-btn').click(function(e){
		var rabat = $('.rabat_partner_edit').val();
		var partner_ids = selected_partner_ids;

		if(partner_ids && rabat != '') {

			$.post(
				base_url+'admin/b2b/ajax/rabat_partner_edit', {
					action: 'rabat_partner_edit',
					partner_ids: partner_ids,
					rabat: rabat
				}, function (response){

					alertify.success('Uspešno ste sačuvali rabat.');
					var data = $.parseJSON(response);

					$.each(data, function(index, value) {
						var obj_row = $("#selectable").find('[data-id="'+ index +'"]');
						obj_row.find('.rabat').html(value);
					});

				}
			);
		} else {
			alertify.error('Morate izabrati partnera/e i upisati željeni rabat.');
		}

	});

//KATEGORIJA PARTNERA
	$('#kategorija_partner_edit-btn').click(function(){

		var kategorija = $('.kategorija_partner_edit').val();
		var partner_ids = selected_partner_ids;

		if(partner_ids && kategorija != 'Bez kategorije') {

			$.post(
				base_url+'admin/b2b/ajax/kategorija_partner_edit', {
					action: 'kategorija_partner_edit',
					partner_ids: partner_ids,
					kategorija: kategorija
				}, function (response){

					alertify.success('Uspešno ste sačuvali kategoriju.');

					var data = $.parseJSON(response);

					$.each(data, function(index, value) {
						var obj_row = $("#selectable").find('[data-id="'+ index +'"]');
						obj_row.find('.kategorija').html(value);
					});

				}
			);
		} else if(partner_ids && kategorija == 'Bez kategorije'){

			$.post(
				base_url+'admin/b2b/ajax/kategorija_partner_edit', {
					action: 'kategorija_partner_edit',
					partner_ids: partner_ids,
					kategorija: kategorija
				}, function (response){

					alertify.success('Uspešno ste obrisali kategorije');
					var data = $.parseJSON(response);

					$.each(data, function(index, value) {
						var obj_row = $("#selectable").find('[data-id="'+ index +'"]');
						obj_row.find('.kategorija').html("Bez kategorije");
					});

				}
			);
		} else{
			alertify.error('Morate izabrati partnera/e.');
		}

	});





	$(document).ready(function(){
		$("#myInput").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$("#myTable tr").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});
	});

	$(document).ready(function(){
		$("#myInput2").on("keyup", function() {
			var value = $(this).val().toLowerCase();
			$("#myTable2 tr").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});
	});

	//nivo pristupa proizvodjaci
	$('#access-parner-change').on("change", function () {
		var partner_id = $(this).val();
		location.href = base_url+'admin/b2b/access-list-manufacturer/' + partner_id;
	});

	$('.JSProizvodjac').change(function(){


		var checked = this.checked ? 1 : 0;
		var proizvodjac_id = $(this).data('id');
		var partner_id = $('#access-parner-change').val();

		$.post(
			base_url+'admin/b2b/ajax/edit_permision_manufacturer', {
				action: 'edit_permision_manufacturer',
				proizvodjac_ids: [proizvodjac_id],
				checked: checked,
				partner_id: partner_id,
			}, function (response){

				alertify.success('Uspešno ste sačuvali promene.');

			}
		);
	});

	$('#select-all').click(function (event) {

		var checked = this.checked ? 1 : 0;
		var proizvodjac_ids=[];
		var partner_id = $('#access-parner-change').val();

		$('.JSProizvodjac').each(function (){
			this.checked = checked;
			proizvodjac_ids.push($(this).data('id'));

		});
		$.post(
			base_url+'admin/b2b/ajax/edit_permision_manufacturer', {
				action: 'edit_permision_manufacturer',
				proizvodjac_ids: proizvodjac_ids,
				checked: checked,
				partner_id: partner_id,
			}, function (response){

				alertify.success('Uspešno ste sačuvali promene.');

			}
		);

	});

	$('#access-parner-change').select2({
		placeholder: ('Izaberite partnera')
	});


	// nivo pristupa grupe

	$('#access-groups-parner-change').on("change", function () {
		var partner_id = $(this).val();
		var grupa_pr_id = $(this).data('id');
		location.href = base_url+'admin/b2b/access-list-group-article/' + partner_id + '/' + grupa_pr_id;
	});

	$('#access-groups-parner-change').select2({
		placeholder: ('Izaberite partnera')
	});

	$('.JSGrupa').change(function(){

		var checked = this.checked ? 1 : 0;
		var grupa_pr_id = $(this).data('id');
		var partner_id = $('#access-groups-parner-change').val();


		$.post(
			base_url+'admin/b2b/ajax/edit_permision', {
				action: 'edit_permision_group',
				grupa_pr_ids: [grupa_pr_id],
				checked: checked,
				partner_id: partner_id,
			}, function (response){

				alertify.success('Uspešno ste sačuvali promene.');

			}
		);
	});

	$('#select-all-groups').click(function (event) {

		var checked = this.checked ? 1 : 0;
		var grupa_pr_ids=[];
		var partner_id = $('#access-groups-parner-change').val();

		$('.JSGrupa').each(function (){
			this.checked = checked;
			grupa_pr_ids.push($(this).data('id'));

		});
		$.post(
			base_url+'admin/b2b/ajax/edit_permision', {
				action: 'edit_permision_group',
				grupa_pr_ids: grupa_pr_ids,
				checked: checked,
				partner_id: partner_id,
			}, function (response){

				alertify.success('Uspešno ste sačuvali promene.');

			}
		);

	});

	// nivo pristupa artikli

	$('.JSArtikal').change(function(){

		var checked = this.checked ? 1 : 0;
		var roba_id = $(this).data('id');
		var partner_id = $('#access-groups-parner-change').val();


		$.post(
			base_url+'admin/b2b/ajax/edit_permision', {
				action: 'edit_permision_article',
				roba_ids: [roba_id],
				checked: checked,
				partner_id: partner_id,
			}, function (response){

				alertify.success('Uspešno ste sačuvali promene.');

			}
		);
	});

	$('#select-all-articles').click(function (event) {
		$('#wait').show();
		var checked = this.checked ? 1 : 0;
		var roba_ids=[];
		var partner_id = $('#access-groups-parner-change').val();

		$('.JSArtikal').each(function (){
			this.checked = checked;
			//roba_ids.push($(this).data('id'));

		});
		$.post(
			base_url+'admin/b2b/ajax/edit_permision', {
				action: 'edit_permision_article_all',
				//roba_ids: roba_ids,
				checked: checked,
				partner_id: partner_id,
			}, function (response){
				$('.JSads-loader').show();
				alertify.success('Uspešno ste sačuvali promene.');
				$('#wait').hide();

			}
		);

	});

	//nivo pristupa strane

	$('#access-pages-parner-change').on("change", function () {
		var partner_id = $(this).val();
		var grupa_pr_id = $(this).data('id');
		location.href = base_url+'admin/b2b/access-list-page/' + partner_id;
	});

	$('#access-pages-parner-change').select2({
		placeholder: ('Izaberite partnera')
	});

	$('.JSStrana').change(function(){

		var checked = this.checked ? 1 : 0;
		var web_b2b_seo_id = $(this).data('id');
		var partner_id = $('#access-pages-parner-change').val();


		$.post(
			base_url+'admin/b2b/ajax/edit_permision', {
				action: 'edit_permision_page',
				web_b2b_seo_ids: [web_b2b_seo_id],
				checked: checked,
				partner_id: partner_id,
			}, function (response){

				alertify.success('Uspešno ste sačuvali promene.');

			}
		);
	});

	$('#select-all-pages').click(function (event) {

		var checked = this.checked ? 1 : 0;
		var web_b2b_seo_ids =[];
		var partner_id = $('#access-pages-parner-change').val();

		$('.JSStrana').each(function (){
			this.checked = checked;
			web_b2b_seo_ids.push($(this).data('id'));

		});
		$.post(
			base_url+'admin/b2b/ajax/edit_permision', {
				action: 'edit_permision_page',
				web_b2b_seo_ids: web_b2b_seo_ids,
				checked: checked,
				partner_id: partner_id,
			}, function (response){

				alertify.success('Uspešno ste sačuvali promene.');

			}
		);

	});

	// nivo pristupa vesti

	$('#access-news-parner-change').on("change", function () {
		var partner_id = $(this).val();
		var grupa_pr_id = $(this).data('id');
		location.href = base_url+'admin/b2b/access-list-news/' + partner_id;
	});

	$('#access-news-parner-change').select2({
		placeholder: ('Izaberite partnera')
	});

	$('.JSVest').change(function(){

		var checked = this.checked ? 1 : 0;
		var web_vest_b2b_id = $(this).data('id');
		var partner_id = $('#access-news-parner-change').val();


		$.post(
			base_url+'admin/b2b/ajax/edit_permision', {
				action: 'edit_permision_news',
				web_vest_b2b_ids: [web_vest_b2b_id],
				checked: checked,
				partner_id: partner_id,
			}, function (response){

				alertify.success('Uspešno ste sačuvali promene.');

			}
		);
	});


	$('#select-all-news').click(function (event) {

		var checked = this.checked ? 1 : 0;
		var web_vest_b2b_ids =[];
		var partner_id = $('#access-news-parner-change').val();

		$('.JSVest').each(function (){
			this.checked = checked;
			web_vest_b2b_ids.push($(this).data('id'));

		});
		$.post(
			base_url+'admin/b2b/ajax/edit_permision', {
				action: 'edit_permision_news',
				web_vest_b2b_ids: web_vest_b2b_ids,
				checked: checked,
				partner_id: partner_id,
			}, function (response){

				alertify.success('Uspešno ste sačuvali promene.');

			}
		);

	});

	// nivo pristupa lageri

	$('#access-warehouse-parner-change').on("change", function () {
		var partner_id = $(this).val();
		var grupa_pr_id = $(this).data('id');
		location.href = base_url+'admin/b2b/access-list-warehouse/' + partner_id;
	});

	$('#access-warehouse-parner-change').select2({
		placeholder: ('Izaberite partnera')
	});

	$('.JSLager').change(function(){

		var checked = this.checked ? 1 : 0;
		var orgj_id = $(this).data('id');
		var partner_id = $('#access-warehouse-parner-change').val();


		$.post(
			base_url+'admin/b2b/ajax/edit_permision', {
				action: 'edit_permision_warehouse',
				orgj_ids: [orgj_id],
				checked: checked,
				partner_id: partner_id,
			}, function (response){

				alertify.success('Uspešno ste sačuvali promene.');

			}
		);
	});

	$('#select-all-warehouse').click(function (event) {

		var checked = this.checked ? 1 : 0;
		var orgj_ids =[];
		var partner_id = $('#access-warehouse-parner-change').val();

		$('.JSLager').each(function (){
			this.checked = checked;
			orgj_ids.push($(this).data('id'));

		});
		$.post(
			base_url+'admin/b2b/ajax/edit_permision', {
				action: 'edit_permision_warehouse',
				orgj_ids: orgj_ids,
				checked: checked,
				partner_id: partner_id,
			}, function (response){

				alertify.success('Uspešno ste sačuvali promene.');

			}
		);

	});

	//pretraga

	$('#JSArticalSearch').click(function(){
		var search = $('input[name="search"]').val() == '' ? 'null' : $('input[name="search"]').val();
		var params = getUrlVars();
		delete params['page'];
		params['search'] = search;

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});



});
