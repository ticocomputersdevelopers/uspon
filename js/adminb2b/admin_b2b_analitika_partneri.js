
$(document).ready(function(){

	$(function() {
	$( "#datum_od_analitika, #datum_do_analitika" ).datepicker();
	});
	$('#datum_do_analitika').keydown(false);
	$('#datum_od_analitika').keydown(false);

	$('#datum_od_analitika').change(function() {
	 var datum_od = $('#datum_od_analitika').val();
	 var datum_do = $('#datum_do_analitika').val();


	});

	$('input[name="datum_od_analitika"]').change(function(){
		var params = getUrlVars();
		delete params['page'];
		delete params['export'];
		params['datum_od_analitika'] = $(this).val();

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});	

	$('input[name="datum_do_analitika"]').change(function(){
		var params = getUrlVars();
		delete params['page'];
		delete params['export'];
		params['datum_do_analitika'] = $(this).val();

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});
	
	$('.JSPartnerAnalitikaFilterSelect').change(function(){
		var params = getUrlVars();
		params[$(this).data('name')] = $(this).val();
		delete params['page'];
		delete params['export'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});

	$('.JSSort').click(function(){
		var params = getUrlVars();
		params['sort_column'] = $(this).data('sort_column');
		params['sort_direction'] = $(this).data('sort_direction');
		delete params['page'];
		delete params['export'];

		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});

	$('#JSExportAnalitikaExport').click(function(){
		var params = getUrlVars();
		params['export'] = 1;
		window.location.href = window.location.pathname+(Object.keys(params).length > 0 ? '?'+$.param(params) : '');
	});	

}); 