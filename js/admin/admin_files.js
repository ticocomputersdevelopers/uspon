$(document).ready(function(){

	$(document).on('click', '.JSSelectFile',function(){
        var file = $(this).data('id');

        var string = '<div class="JSSelectedFile column medium-3" data-id="'+file+'"> <span class="img-desc" title="URL:'+file+'" >URL:'+file+'</span><input class="JSLinkName" type="text" placeholder="Naziv linka" value="" /><a class="btn btn-danger margin-bottom-buttons JSDocumentsDelete">'+translate('Skloni')+'</a></div>';

		if($(this).closest('.col-document').hasClass('active')){
			$('.JSSelectedFile[data-id="'+file+'"]').remove();

			if($(".JSSelectedFile").length == 0){
				$("#JSSelectedFiles").html('<span id="JSNotSelectFile" class="options-title-img column medium-12">'+translate('Niste izabrali fajl.')+'</span>');
			}
		}else{
			$("#JSNotSelectFile").remove();
	        $("#JSSelectedFiles").append(string);
	        // $(this).closest('.column').siblings().find('.active').removeClass('active');
		}

		selectedFilesDocument();
	});

	$(document).on('click', '.JSDocumentsDelete',function(){
        $(this).parent().remove();
        selectedFilesDocument();
		if($(".JSSelectedFile").length == 0){
			$("#JSSelectedFiles").html('<span id="JSNotSelectFile" class="options-title-img column medium-12">'+translate('Niste izabrali fajl.')+'</span>');
		}
	});

	$(document).on('click', '.JSFileDelete',function(){
        var file = $(this).data('id');
        var this_element = $(this);

  		$.get(base_url+'admin/upload-document-delete/'+file, {}, function (response){
			this_element.parent().remove();
			$('.JSSelectedFile[data-id="'+file+'"]').parent().remove();

			if($(".JSSelectedFile").length == 0){
				$("#JSSelectedFiles").html('<span id="JSNotSelectFile" class="options-title-img column medium-12">'+translate('Niste izabrali fajl.')+'</span>');
			}
  		});

	});

	$(document).on('change','#JSFileUpload',function(){
		var formData = new FormData();
		
		var slike_element = $(this)[0];

		for(var i = 0; i < slike_element.files.length; i++) {
			var slika_obj = slike_element.files[i];
	    	formData.append('files[]', slika_obj, slika_obj.name);
		}

		upload_file_page = 1;
		$.ajax({
			type: "POST",
			url: base_url + 'admin/upload-document-add',
			data: formData,
			async: false,
			cache: false,
			contentType: false,
			enctype: 'multipart/form-data',
			processData: false,
			success: function(msg) {
				$.ajax({
					type: "POST",
					url: base_url + 'admin/ajax/files-content',
					data:  {page: upload_file_page, search: $('#JSFileSearch').val()},
					success: function(response) {
			     		upload_image_max_page = response.max_page;

						$('#JSFilesModal').find('.JSFileContent').html(response.content);
						selectedFilesDocument();

						alertify.alert(translate('Uspešno ste upload-ovali slike!'));
					}
				});
			}
		});
	});

	var upload_image_max_page = 1;
	var upload_file_page = 1;
	$('#JSFiles').on('click', function(){
		$("#JSSelectedFiles").html('<span id="JSNotSelectFile" class="options-title-img column medium-12">'+translate('Niste izabrali fajl.')+'</span>');

		// if (selected_roba_ids) {
	     	upload_file_page = 1;
			 $.ajax({
			     type: "POST",
			     url: base_url + 'admin/ajax/files-content',
			     data:  {page: upload_file_page},
			     success: function(response) {
			     	upload_image_max_page = response.max_page;
			        $('#JSFilesModal').find('.JSFileContent').html(response.content);
					$('#JSFilesModal').foundation('reveal', 'open');
			     }
			 });
		// }else{
		// 	alertify.alert(translate('Nema selektovanih artikala!'));
		// }
	});
	
	var ajax_load = false;
	$('#JSFilesModal').find('.JSFileContent').on('scroll',function(){
		var scroll_bottom = $(this)[0].scrollHeight - ($(this).scrollTop() + $(this).innerHeight());
		if(!ajax_load && scroll_bottom < 50 && upload_image_max_page >= upload_file_page){
			upload_file_page++;
			ajax_load = true;
			$.ajax({
				type: "POST",
				url: base_url + 'admin/ajax/files-content',
				data:  {page: upload_file_page, search: $('#JSFileSearch').val()},
				success: function(response) {
					ajax_load = false;
			     	upload_image_max_page = response.max_page;
					$('#JSFilesModal').find('.JSFileContent').append(response.content);
					selectedFilesDocument();
				}
			});

		}
	});

	var searchTime;
	$('#JSFileSearch').on('keyup', function(){
		var search = $(this).val();
     	upload_file_page = 1;
		clearTimeout(searchTime);
		searchTime = setTimeout(function(){
				$.ajax({
					type: "POST",
					url: base_url + 'admin/ajax/files-content',
					data:  {page: upload_file_page, search: search},
					success: function(response) {
						upload_image_max_page = response.max_page;
						$('#JSFilesModal').find('.JSFileContent').html(response.content);
						selectedFilesDocument();
					}
				});
			},400);
	});

	$(document).on('click','#JSSelectFilesAssignToTinyEditor',function(){
		var ed = tinymce.activeEditor;
		var content = ed.selection.getContent({'format':'html'});
		new_selection_content = '';
		$(".JSSelectedFile").each(function(index,value){
			new_selection_content += '<a href="'+base_url+'files/documents/'+$(value).data('id')+'">'+$(value).find('.JSLinkName').val()+'</a>';
		});
			
		ed.execCommand('insertHTML', false, new_selection_content);

		$("#JSSelectedFiles").html('<span id="JSNotSelectFile" class="options-title-img column medium-12">'+translate('Niste izabrali fajl.')+'</span>');
		selectedFilesDocument();

	});


});

function selectedFilesDocument(){
	$(".JSSelectFile").closest('.col-document').removeClass('active');

	$(".JSSelectedFile").each(function(index,value){
		$('.JSSelectFile[data-id="'+$(value).data('id')+'"]').closest('.col-document').addClass('active');
	});
}