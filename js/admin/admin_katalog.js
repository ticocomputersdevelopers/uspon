$(document).ready(function(){
	$('#JSKatalogPoleAdd').click(function(){
		var katalog_id = $(this).data('katalog_id');
		var naziv = $('#JSKatalogPoleSelect').val();
		var title = $('#JSKatalogPoleSelect').find('option:selected').attr("title"); 
  		$.post(base_url+'admin/katalog-polje-dodaj', {naziv: naziv, title: title, katalog_id: katalog_id}, function (response){
  			location.reload(true);
  		});			
	});

	$(function() {
		$('.JSFildsSortable').sortable({
			update: function(event, ui) {
				var moved = ui.item[0].id;
				var order = [];
				var katalog_id = $('.JSFildsSortable').data('katalog_id');
				$('.JSFildsSortable li').each( function(e) {
					order.push( $(this).attr('id') );
				});

				if(moved != 0){
					$.ajax({
						type: "POST",
						url: base_url+'admin/katalog-polja-pozicija',
						data: {order:order, katalog_id:katalog_id},
						success: function(msg) {

						}
					});
				}	
			}
		});					   
		$( ".JSFildsSortable").disableSelection();			
	});

	$('#JSKatalogVrsta').change(function(){
		var katalog_vrsta = $(this).val();
		if(katalog_vrsta==3){
			$('#JSKarakteristikeSort').removeAttr('hidden');
		}else{
			$('#JSKarakteristikeSort').attr('hidden','hidden');
		}
	});
	$('#JSGrupaID').change(function(){
		var grupa_pr_id = $(this).val();
		$.ajax({
			type: "POST",
			url: base_url+'admin/ajax/grupa-karakteristike-nazivi',
			data: {grupa_pr_id:grupa_pr_id},
			success: function(response) {
				var result = $.parseJSON(response);
				var render = '';
				for (var i=0;i<result.length;i++) {
					console.log(result[i]);
					render += '<input type="radio" name="grupa_pr_naziv_id" value="'+result[i].grupa_pr_naziv_id+'" '+(i==0?'checked':'')+'>'+result[i].naziv+'<br>';
				}
				$('#JSGrupaNazivID').html(render);
			}
		});
	});
	$('.JSGrupaKarakNazivID').click(function(){
		var data = $(this).data('katalog_grupe_sort');
		$.ajax({
			type: "POST",
			url: base_url+'admin/ajax/grupa-karakteristika-delete',
			data: data,
			success: function(response) {
				location.reload(true);
			}
		});
	});

});