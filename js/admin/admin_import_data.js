
$(document).ready(function () {

	$('#JSDobavljacSelect').change(function(){
		var dobavljac_id = $(this).val();
		window.location.href = base_url+"admin/import-podaci-proizvodjac/"+dobavljac_id+"/0";
	});

	$(document).click(function(event){
		if(
			!$(event.target).hasClass('JSListaProizvodjac') 
			&& $(event.target).attr('name') != 'proizvodjac'
		){
			$('#JSListaProizvodjaca').attr('hidden','hidden');	
		}
		if(
			!$(event.target).hasClass('JSListaGrupa') 
			&& $(event.target).attr('name') != 'grupa'
		){
			$('#JSListaGrupe').attr('hidden','hidden');	
		}
	});
	//proizvodjaci
	$('input[name="proizvodjac"]').click(function(){
		if($('#JSListaProizvodjaca').attr('hidden') == 'hidden'){
			$('#JSListaProizvodjaca').removeAttr('hidden');
		}else{
			$('#JSListaProizvodjaca').attr('hidden','hidden');	
		}	
	});
	$('.JSListaProizvodjac').click(function(){
		var proizvodjac = $(this).data('proizvodjac');
		$('input[name="proizvodjac"]').val(proizvodjac);
		$('#JSListaProizvodjaca').attr('hidden','hidden');	
	});
	$(".search_select").select2({
		width: '90%',
        language: {
            noResults: function () {
            return translate("Nema rezultata");
            }
        }
    });
	//Import podaci grupa
	$('#JSPartneriSelect').change(function(){
		var dobavljac_id = $(this).val();
		window.location.href = base_url+"admin/import_podaci_grupa/"+dobavljac_id+"/0/0";
	});
	//grupe
	$('input[name="grupa"]').click(function(){
		if($('#JSListaGrupe').attr('hidden') == 'hidden'){
			$('#JSListaGrupe').removeAttr('hidden');
		}else{
			$('#JSListaGrupe').attr('hidden','hidden');	
		}	
	});
	$('.JSListaGrupa').click(function(){
		var grupa = $(this).data('grupa');
		$('input[name="grupa"]').val(grupa);
		$('#JSListaGrupe').attr('hidden','hidden');	
	});

	$('#search_grupe').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#search_grupe').val() != ''){
        $("#search_grupe-btn").trigger("click");
    	}
    });

    $('#search_grupe-btn').click(function(){

		var search = $('#search_grupe').val();
		var obj = $(this);
		var partner_id = obj.data('partner_id');
		//console.log(partner_id);		
		if(!search){
			search = '0';
		}
		window.location.href = base_url + "admin/import_podaci_grupa/"+partner_id+'/0/' + search ;
		
		});
    	$('#JSSearchClear').click(function(){
		var obj = $(this);
		var partner_id = obj.data('partner_id');

		window.location.href = base_url+"admin/import_podaci_grupa/"+partner_id+"/0/0";

		});

});