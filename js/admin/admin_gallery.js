$(document).ready(function(){

	$(document).on('click', '.JSSelectGalleryImage',function(){
        var image = $(this).data('id');
        var string = '<div class="column medium-3"> <div class="modal-image-add flex"> <img class="JSSelectedGalleryImage" src="'+base_url+'images/upload_image/'+image+'" data-id="'+image+'"> </div> <span> <label> Alt: </label></span><input class="JSGalleryImageAlt" type="text" value="" /><a class="btn btn-danger JSGallerySelectImageDelete margin-bottom-buttons">'+translate('Skloni')+'</a></div>';

		if($(this).closest('.col-slika').hasClass('active')){
			$('.JSSelectedGalleryImage[data-id="'+image+'"]').closest('.column').remove();

			if($(".JSSelectedGalleryImage").length == 0){
				$("#JSSelectedGalleryImages").html('<span id="JSGalleryNotSelectImage" class="options-title-img column medium-12">'+translate('Niste izabrali sliku.')+'</span>');
			}
		}else{
			$("#JSGalleryNotSelectImage").remove();
	        $("#JSSelectedGalleryImages").append(string);
	        // $(this).closest('.column').siblings().find('.active').removeClass('active');
		}

		selectedGalleryImages();
	});

	$(document).on('click', '.JSGallerySelectImageDelete',function(){
        $(this).parent().remove();
        selectedGalleryImages();
		if($(".JSSelectedGalleryImage").length == 0){
			$("#JSSelectedGalleryImages").html('<span id="JSGalleryNotSelectImage" class="options-title-img column medium-12">'+translate('Niste izabrali sliku.')+'</span>');
		}
	});

	$(document).on('click', '.JSGalleryImageDelete',function(){
        var image = $(this).data('id');
        var this_element = $(this);

  		$.get(base_url+'admin/upload-image-delete/'+image, {}, function (response){
			this_element.parent().remove();
			$('.JSSelectedGalleryImage[data-id="'+image+'"]').closest('.column').remove();

			if($(".JSSelectedGalleryImage").length == 0){
				$("#JSSelectedGalleryImages").html('<span id="JSGalleryNotSelectImage" class="options-title-img column medium-12">'+translate('Niste izabrali sliku.')+'</span>');
			}
  		});

	});

	$(document).on('change','#JSGalleryImageUpload',function(){
		var formData = new FormData();
		
		var slike_element = $(this)[0];
		for(var i = 0; i < slike_element.files.length; i++) {
			var slika_obj = slike_element.files[i];
	    	formData.append('slike[]', slika_obj, slika_obj.name);
		}

		upload_image_page = 1;
		$.ajax({
			type: "POST",
			url: base_url + 'admin/upload-image-add',
			data: formData,
			async: false,
			cache: false,
			contentType: false,
			enctype: 'multipart/form-data',
			processData: false,
			success: function(msg) {
				$.ajax({
					type: "POST",
					url: base_url + 'admin/ajax/gallery-content',
					data:  {page: upload_image_page, search: $('#JSGalleryImageSearch').val()},
					success: function(response) {
			     		upload_image_max_page = response.max_page;

						$('#JSGalleryModal').find('.JSContent').html(response.content);
						selectedGalleryImages();

						alertify.alert(translate('Uspešno ste upload-ovali slike!'));
					}
				});
			}
		});
	});

	var upload_image_max_page = 1;
	var upload_image_page = 1;
	$('#JSGallery').on('click', function(){
			$("#JSSelectedGalleryImages").html('<span id="JSGalleryNotSelectImage" class="options-title-img column medium-12">'+translate('Niste izabrali sliku.')+'</span>');

		// if (selected_roba_ids) {
	     	upload_image_page = 1;
			 $.ajax({
			     type: "POST",
			     url: base_url + 'admin/ajax/gallery-content',
			     data:  {page: upload_image_page},
			     success: function(response) {
			     	upload_image_max_page = response.max_page;
			        $('#JSGalleryModal').find('.JSContent').html(response.content);
					$('#JSGalleryModal').foundation('reveal', 'open');
			     }
			 });
		// }else{
		// 	alertify.alert(translate('Nema selektovanih artikala!'));
		// }
	});
	
	var ajax_load = false;
	$('#JSGalleryModal').find('.JSContent').on('scroll',function(){
		var scroll_bottom = $(this)[0].scrollHeight - ($(this).scrollTop() + $(this).innerHeight());
		if(!ajax_load && scroll_bottom < 50 && upload_image_max_page >= upload_image_page){
			upload_image_page++;
			ajax_load = true;
			$.ajax({
				type: "POST",
				url: base_url + 'admin/ajax/gallery-content',
				data:  {page: upload_image_page, search: $('#JSGalleryImageSearch').val()},
				success: function(response) {
					ajax_load = false;
			     	upload_image_max_page = response.max_page;
					$('#JSGalleryModal').find('.JSContent').append(response.content);
					selectedGalleryImages();
				}
			});

		}
	});

	var searchTime;
	$('#JSGalleryImageSearch').on('keyup', function(){
		var search = $(this).val();
     	upload_image_page = 1;
		clearTimeout(searchTime);
		searchTime = setTimeout(function(){
				$.ajax({
					type: "POST",
					url: base_url + 'admin/ajax/gallery-content',
					data:  {page: upload_image_page, search: search},
					success: function(response) {
						upload_image_max_page = response.max_page;
						$('#JSGalleryModal').find('.JSContent').html(response.content);
						selectedGalleryImages();
					}
				});
			},400);
	});

	$(document).on('click','#JSSelectGalleryImageAssignToTinyEditor',function(){
		var ed = tinymce.activeEditor;
		var content = ed.selection.getContent({'format':'html'});
		new_selection_content = '';
		$(".JSSelectedGalleryImage").each(function(index,value){
			new_selection_content += '<div class="relative inline-block"><img class="wrap-img" src="'+base_url+'images/upload_image/'+$(value).data('id')+'" alt="'+$(value).parent().parent().find('.JSGalleryImageAlt').val()+'"></div>';
		});
		ed.execCommand('insertHTML', false, new_selection_content);

		$("#JSSelectedGalleryImages").html('<span id="JSGalleryNotSelectImage" class="options-title-img column medium-12">'+translate('Niste izabrali sliku.')+'</span>');
		selectedGalleryImages();
	});


});

function selectedGalleryImages(){
	$(".JSSelectGalleryImage").closest('.col-slika').removeClass('active');

	$(".JSSelectedGalleryImage").each(function(index,value){
		$('.JSSelectGalleryImage[data-id="'+$(value).data('id')+'"]').closest('.col-slika').addClass('active');
	});
}