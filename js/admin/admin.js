// GLAVNI ADMIN

$(document).ready(function () {
	   $(document).foundation();
  if ($('.JSLogoUpload').length) {
	$('.JSLogoUploadButton').on('click', function(event) {
	  // event.preventDefault();
	});
	$(document).on('change','.JSLogoUpload' , function(){
	  $('.JSLogoForm').submit();
	});
  }

  if ($('.JSPotpisUpload').length) {
	$('.JSPotpisUploadButton').on('click', function(event) {
	  // event.preventDefault();
	});
	$(document).on('change','.JSPotpisUpload' , function(){
	  $('.JSPotpisForm').submit();
	});
  }
 
  /*==================================
  =            kupci page            =
  ==================================*/
  	if($('.JSKupacCheckbox').is(':checked')){
  		$(".JSPravnoLice").toggleClass('active');
	    $(".JSPrivatnoLice").toggleClass('active');
  	}
  	// $(".JSPravnoLice").hide();  // unchecked
	$('.JSKupacCheckbox').on('change', function() {
		
	    $(".JSPravnoLice").toggleClass('active');
	    $(".JSPrivatnoLice").toggleClass('active');
	});

		// SLIDERS DEVICE FILTER
	$('.JSdeviceFilter').on('change', function() {
		var device = $(this).children(':selected').attr('id');
		$('.banner-list li').each(function() {
			if($(this).data('slajder_device_id') != device) {
					$(this).hide();
			} else {
					$(this).show();
			}
		});
	})

	
$('.JSvaucerRefresh').on('click', function() {
	var vaucer = $(this).parent().find('#vaucer_broj');
	$(vaucer).val('U-' + Math.floor((Math.random() * 100000) + 1));
});

  /*=====  End of kupci page  ======*/

	// $('.JSchoose_file_hack_label').on('click', function(){
	// 	$('.JSchoose_file_hack').click().change(function() {
	// 		filename = this.files[0].name;
	// 		$('.JSchoose_file_hack_text').text(filename);
	// 	}); 
	// });
	// <label class="JSchoose_file_hack_label btn btn-small btn-primary">Izaberi sliku</label>
	// <input type="file" name="bgImg" class="JSchoose_file_hack hide" value="Izaberi sliku">
	// <span class="JSchoose_file_hack_text"></span>
	 
  
//web options
   $('.check_swift').click(function () {
		var aktivan;
		if($(this).attr('checked')){
			aktivan = 0;
			$(this).removeAttr("checked");
			$('.JSMailInput').attr('disabled',true);
		}else{
			aktivan = 1;
			$(this).attr('checked', '');
			$('.JSMailInput').removeAttr('disabled');
		}


		$.post(base_url+'admin/aktivan-modul', {action:'check_swift', aktivan:aktivan}, function (response){});

   });

   	function showPicture() {
  var sourceOfPicture = "http://img.tesco.com/Groceries/pi/118/5000175411118/IDShot_90x90.jpg";
  var img = document.getElementById('JSSlika')
  img.src = sourceOfPicture.replace('90x90', '225x225');
  img.style.display = "block";
}
   $('.options_active').click(function () {
		var aktivan;
		if($(this).attr('checked')){
			aktivan = 0;
			$(this).removeAttr("checked");
		}else{
			aktivan = 1;
			$(this).attr('checked', '');
		}
		$.post(base_url+'admin/aktivan-modul', {action:'options_active', aktivan:aktivan, id:$(this).data('id')}, function (response){});

   });
   $('input[name="JSPortal"]').click(function () {
		var aktivan = $(this).val();

		$.post(base_url+'admin/aktivan-modul', {action:'b2b_active', aktivan:aktivan, id:$(this).data('id')}, function (response){ location.reload(true); });

   });

   $('.nacin_placanja').click(function () {
		var aktivan;
		if($(this).attr('checked')){
			aktivan = 0;
			$(this).removeAttr("checked");
		}else{
			aktivan = 1;
			$(this).attr('checked', '');
		}

		$.post(base_url+'admin/aktivan-modul', {action:'nacin_placanja', aktivan:aktivan, id:$(this).data('id')}, function (response){});

   });

  $('.nacin_isporuke').click(function () {
		var aktivan;
		if($(this).attr('checked')){
			aktivan = 0;
			$(this).removeAttr("checked");
		}else{
			aktivan = 1;
			$(this).attr('checked', '');
		}

		$.post(base_url+'admin/aktivan-modul', {action:'nacin_isporuke', aktivan:aktivan, id:$(this).data('id')}, function (response){});

   });

  $('.JSOrgjEnable').click(function () {
		var aktivan;
		if($(this).attr('checked')){
			aktivan = 0;
			$(this).removeAttr("checked");
			$(this).closest('li').find('.JSOrgjPrimary').attr("disabled",true);
		}else{
			aktivan = 1;
			$(this).attr('checked', '');
			$(this).closest('li').find('.JSOrgjPrimary').removeAttr("disabled");
		}

		$.post(base_url+'admin/aktivan-modul', {action:'magacin_enable', aktivan:aktivan, id:$(this).data('id')}, function (response){});
   });

  $('.JSOrgjPrimary').click(function () {

			$('.JSOrgjEnable').removeAttr("disabled");
			$(this).closest('li').find('.JSOrgjEnable').attr("disabled",true);

		$.post(base_url+'admin/aktivan-modul', {action:'magacin_primary', id:$(this).data('id')}, function (response){});
   });

  $('.JSVrstaCena').change(function () {
	  $.post(base_url+'admin/aktivan-modul', {action:'vrsta_cena', valuta_id:$(this).val(), id:$(this).data('id')}, function (response){});
   });

  $('.JSVrstaCenaSelect').change(function () {
	  var aktivan;
	  if($(this).attr('checked')){
		  aktivan = 0;
		  $(this).removeAttr("checked");
	  }else{
		  aktivan = 1;
		  $(this).attr('checked', '');
	  }
	  $.post(base_url+'admin/aktivan-modul', {action:'vrsta_cena_select', aktivan:aktivan, id:$(this).data('id')}, function (response){});
  });

  $('.JSExportEnable').click(function () {
		var aktivan;
		if($(this).attr('checked')){
			aktivan = 0;
			$(this).removeAttr("checked");
		}else{
			aktivan = 1;
			$(this).attr('checked', '');
		}

		$.post(base_url+'admin/aktivan-modul', {action:'export_enable', aktivan:aktivan, id:$(this).data('id')}, function (response){});
   });
   $('.auto_import_active').click(function () {
		var aktivan;
		if($(this).attr('checked')){
			aktivan = 0;
			$(this).removeAttr("checked");
		}else{
			aktivan = 1;
			$(this).attr('checked', '');
		}

		$.post(base_url+'admin/aktivan-modul', {action:'auto_import_active', aktivan:aktivan, id:$(this).data('id')}, function (response){});

   });
  $('.JSTheme').click(function () {
		$.post(base_url+'admin/aktivan-modul', {action:'theme_select', id:$(this).data('id')}, function (response){ location.reload(true) });
   });
  $('#JSThemeConfirm').click(function () {
  		var check = confirm(translate("Ovom potvrdom izabrana tema postaje konačan izbor!\nDa li ste sigurni?"));
  		if(check == true){		
			$.post(base_url+'admin/aktivan-modul', {action:'theme_confirm'}, function (response){
				location.reload(true);
			});
  		}
   });

  $('.JSJezikActive').click(function () {
		var aktivan;
		if($(this).attr('checked')){
			aktivan = 0;
			$(this).removeAttr("checked");
			$(this).closest('li').find('.JSJezikPrimary').attr("disabled",true);
		}else{
			aktivan = 1;
			$(this).attr('checked', '');
			$(this).closest('li').find('.JSJezikPrimary').removeAttr("disabled");
		}

		$.post(base_url+'admin/aktivan-modul', {action:'jezik_active', aktivan:aktivan, id:$(this).data('id')}, function (response){});
   });

  $('.JSJezikPrimary').click(function () {

			$('.JSJezikActive').removeAttr("disabled");
			$(this).closest('li').find('.JSJezikActive').attr("disabled",true);

		$.post(base_url+'admin/aktivan-modul', {action:'jezik_primary', id:$(this).data('id')}, function (response){});
   });

  // foundation
  $(document).foundation('accordion', 'reflow');

	var base_url= $('#base_url').val();
   // MOBILE MENU  
   $('.main-menu-toggler').click(function () {
		$('.main-menu, .logout').slideToggle();
   });

   // DATEPICKER 
	 
   $(function() {
		$( "#datepicker-from, #datepicker-to, #order-date" ).datepicker();
   }); 
   
   // SELECT FIELD
  
  $('.select-field').click(function (e) {
	  $(this).children('ul').slideToggle('fast');
	$('.select-field ul').not($(this).children('ul')).slideUp('fast');
	e.stopPropagation();
  });
  
  $('body').click(function () {
	  $('.select-field ul').slideUp('fast');
  });
  
  $(".select-field ul").click(function(e) {
		e.stopPropagation();
	});
  
  // SORTABLE PAGES
  
	$(function() {
	 
	$('.JSPagesSortable').sortable({
		update: function(event, ui) {

			var moved = ui.item[0].id;
			var parent_id = $(event.target).attr('parent');
			var order = [];
			$('.JSPagesSortable li').each( function(e) {
				order.push( $(this).attr('id') );
			});

			if(moved != 0){	 
				$.ajax({
					type: "POST",
					url: base_url+'admin/position',
					data: {parent_id:parent_id, order:order, moved:moved},
					success: function(msg) {
					  // console.log('test');
					}
				});
			}	
		}
	});					   
	$( ".JSPagesSortable").disableSelection();

	$('.JSPageSectionsSortable').sortable({
		update: function(event, ui) {
			var web_b2c_seo_id = $(event.target).data('web_b2c_seo_id');
			var order = [];
			$('.JSPageSectionsSortable li').each( function(e) {
				order.push( $(this).data('sekcija_stranice_id') );
			});

			$.ajax({
				type: "POST",
				url: base_url+'admin/page-section-position',
				data: {web_b2c_seo_id: web_b2c_seo_id, order:order},
				success: function(msg) {

				}
			});	
		}
	});					   
	$( ".JSPageSectionsSortable").disableSelection();

	});

	$('#JSPageSectionsAdd').on('change',function(){
		var web_b2c_seo_id = $(this).data('web_b2c_seo_id');
		var sekcija_stranice_id = $(this).val();

		if(sekcija_stranice_id > 0){
			$.ajax({
				type: "POST",
				url: base_url+'admin/page-section-add',
				data: {web_b2c_seo_id: web_b2c_seo_id, sekcija_stranice_id:sekcija_stranice_id},
				success: function(msg) {
					location.reload(true);
				}
			});	
		}
	});
  
  // SEO SETTINGS
  
  // $('.page-edit-seo-button').click(function (event) {
	 //  event.preventDefault();
	 //  $('.seo-settings').slideToggle();
  // });
   
	// POPUP
  
  $('.popup-close').click(function () {
	  $('.popup').removeClass('popup-opened');
	
	$('.popup-inner').attr('style', function(i, style) {
		return style.replace(/height[^;]+;?/g, '');
	});
  });
  
  // MORE POPUP
  
  $('.more-button').click(function () {
  
	$('.more-popup').addClass('popup-opened');
	
	$('.popup-inner').each(function() {
	
	  setTimeout(function () {
	  
	  var popup_wrapper_Height = $('.popup-inner2').height() + 115;
	  var popup_inner_Height = $('.popup-inner2').height() + 60;
	
	  $('.more-popup .popup-wrapper').height(popup_wrapper_Height);
	  $('.more-popup .popup-inner').height(popup_inner_Height);
	  
	  }, 500);
	});
	
  });

  	
});


// (function ($) {
// 	var $body,
// 		$headerWidthToggle,
// 		headerCookie;

// 	function onHeaderWidthToggle() {
// 		if ($window.width() > 767 && $window.width() < 1014) {
// 		  $body.toggleClass('small-width');
// 		  $.post(
// 			  base_url+'admin/ajax', {
// 				  action: 'adminHeaderWidth'
// 			  }, function (response){}
// 		  );
// 		} else if ($window.width() < 767 ) {
// 		  $body.toggleClass('extra-small-width');
// 		} else {
// 		  $body.toggleClass('small-width');
// 		  $.post(
// 			  base_url+'admin/ajax', {
// 				  action: 'adminHeaderWidth'
// 			  }, function (response){}
// 		  );
// 		}
// 	}

// 	function collapseAdminHeader() {
// 	  $body.removeClass('small-width');
// 	  $.post(
// 		  base_url+'admin/ajax', {
// 			  action: 'collapseAdminHeaderWidth'
// 		  }, function (response){}
// 	  );
// 	}

// 	function expendAdminHeader() {
// 	  $body.addClass('small-width');
// 	  $.post(
// 		  base_url+'admin/ajax', {
// 			  action: 'expendAdminHeaderWidth'
// 		  }, function (response){}
// 	  );
// 	}

// 	function init() {
// 	  if($window.width() < 1024) {
// 		expendAdminHeader();
// 	  }
// 	}

// 	function binding () {
// 		$headerWidthToggle.on('click', onHeaderWidthToggle);
// 	}

// 	$(document).ready(function() {

// 		// declere variables
// 		$body = $('body');
// 		$window = $(window);
// 		$adminHeader = $body.find('#admin-header');
// 		$headerWidthToggle = $body.find('.header-width-toggle');

// 		var headerCookie = document.cookie = "headerSmall=true";

// 		binding();
// 		init();
		
// 		$window.resize(function(){
// 		  init();
// 		});
// 	});
// })(jQuery);

// HEADER COLLAPSE
$('.header-width-toggle').on('click', function(){ 
	$('body').toggleClass('small-width');
 	
 	if ($('body').is('.small-width')) {
 		sessionStorage.setItem('header', 'collapsed');
 	} else {
 		sessionStorage.removeItem('header'); 
 	}  
});

if (sessionStorage.getItem('header')) {
	$('body').addClass('small-width');
}

$(window).on('resize', function(){
	if ($(window).width() < 1200) {
		$('body').addClass('small-width'); 
	}
});

if ($(window).width() < 768) {
	$('body').addClass('small-width'); 
}


/* Kategorije session */
	
	$(document).ready(function() {
		$('.categories').show();

		$('.JScat_toggle_btn').on('click', function(){
			sessionStorage.setItem('opened', '1');
			if ($('.JScat_toggle').attr('hidden')) {
				$('.JScat_toggle').removeAttr('hidden');
				$('.JStoggle_product_list').removeClass('medium-12').addClass('medium-10');
			}else {
				$('.JScat_toggle').attr('hidden', 'hidden');
				$('.JStoggle_product_list').removeClass('medium-10').addClass('medium-12');
				sessionStorage.removeItem('opened');
			} 
		});
		if (sessionStorage.getItem('opened')) {
			$('.JScat_toggle').removeAttr('hidden');
			$('.JStoggle_product_list').removeClass('medium-12').addClass('medium-10');
		}
		if ( ! $('.JScat_toggle_btn').length) {
			sessionStorage.removeItem('opened');
		}
		
		// var articlesContent = $('.articles-content');
		// var fixedCatMenu = $('.fixed-cat-menu');
		
		// if($('.categories').is(":visible")) {
		// 	openCat = 1;
		// } else {
		// 	openCat = 0;
		// }
		 
		// fixedCatMenu.on('click', function(){
		// 	if (openCat == 0) {
		// 		openCategories();
		// 	} else if (openCat == 1) {
		// 		collapseCategories();
		// 	}

		// });

		// function collapseCategories() {
		//   $('.categories').hide();
		//   articlesContent.addClass('wide-articles');

		//   openCat = 0;
		//   $.post(
		// 	  base_url+'admin/ajax', {
		// 		  action: 'collapseCategories'
		// 	  }, function (response){}
		//   );
		// }

		// function openCategories() {
		//   articlesContent.removeClass('wide-articles');
		//   $('.categories').show();
		  
		//   openCat = 1;
		//   $.post(
		// 	  base_url+'admin/ajax', {
		// 		  action: 'openCategories'
		// 	  }, function (response){}
		//   );
		// }
	});
		


/*============================================
=            alert - notification            =
============================================*/

$('.toast__close').on('click', function(){
	this.closest('.toast').remove();
});


/*=====  End of alert - notification  ======*/

if($('.JSenergyLinkType').val() == 0) {
	$('.JSenergyLinkType_one').show();
	$('.JSenergyLinkType_two').hide();
} else if($('.JSenergyLinkType').val() == 1) {
	$('.JSenergyLinkType_one').hide();
	$('.JSenergyLinkType_two').show();
} 
$('.JSenergyLinkType').on('change', function() {
	if($('.JSenergyLinkType').val() == 0) {
		$('.JSenergyLinkType_one').show();
		$('.JSenergyLinkType_two').hide();
	} else if($('.JSenergyLinkType').val() == 1) {
		$('.JSenergyLinkType_one').hide();
		$('.JSenergyLinkType_two').show();
	} 
})

/*============================================
=           Admin Kupci Partneri             =
============================================*/

$('.JS-vrsta').on('click', function(){
	var vrsta = $(this).data('id');
	var partner_id = $('#partner_id').val();

	if(vrsta == 117){
		if($(this).attr('checked')){
			$(this).removeAttr('checked');
			$('.JSServiceSetings').attr('hidden',true);
		}else{
			$(this).attr('checked',true);
			$('.JSServiceSetings').removeAttr('hidden');
		}
	}
	
	$.post(base_url+'admin/ajax/vrsta-partnera', {vrsta:vrsta, partner_id:partner_id}, function (response){ });

});

$('#login-pass2').hide();

$("#show-pass").click(function(){
	$('#login-pass2').val($('#login-pass1').val());
	$('#login-pass2').on('change', function(){
		$('#login-pass1').val($('#login-pass2').val());
	});
	$('#login-pass2').toggle();
	$('#login-pass1').toggle();
});

$('#login-api_pass2').hide();

$("#show-api-pass").click(function(){
	$('#login-api_pass2').val($('#login-api_pass1').val());
	$('#login-api_pass2').on('change', function(){
		$('#login-api_pass1').val($('#login-api_pass2').val());
	});
	$('#login-api_pass2').toggle();
	$('#login-api_pass1').toggle();
});

$('.JSbtn-delete').on('click', function(e){
	e.preventDefault();
	var link = $(this).data('link');

  	alertify.confirm(translate("Da li ste sigurni da želite da izvršite akciju brisanja?"),
	function(e){
	    if(e){
	        location.href = link;
	    }
	});
});

$(function() {
    	$( "#datum_od, #datum_do" ).datepicker();
        });
        $('#datum_od').keydown(false);
        $('#datum_do').keydown(false);
        $('#datum_od_delete').click(function(){
            $('#datum_od').val('');
        });
        $('#datum_do_delete').click(function(){
            $('#datum_do').val('');
});

// Osobine
$('#JSselectProperty').on('change', function(){
	var osobina_naziv_id = $(this).find(':selected').data('id');
	
	location.href = base_url+'admin/osobine/'+osobina_naziv_id;
});



	$('#JSsablon').on('click', function(e){
		e.preventDefault();
		var roba_id = $('#roba_id').val();

		$.post(
			base_url+'admin/ajax', {
				action: 'getTemplate',
				roba_id: roba_id
			}, function (response){
				var resp = $.parseJSON(response)
				var template = resp[0]['sablon_opis'];
				if(template !== null) {
					var content = tinyMCE.activeEditor.getContent();
					tinyMCE.activeEditor.setContent(content + template);
				}
				
			}
		);

	});


// IMAGE PATH NAME
	// function showPicName(){
	// 	var img = $('.images_upload ul li img');

	// 	img.each(function(i, img){
	// 		var src = $(img).attr('src'),
	// 			filename = src.substring(src.lastIndexOf('/')+1),
	// 			li = $(this).closest('li');
	// 		li.append('<span>'+filename+'</span>'); 
	// 	});  
	// }
	// showPicName();


// TOOGLE BUTTON & CONTENT
	$('.JStoggle_btn').on('click', function(e){
		var id = $(this).data('section_id');

		$('.JStoggle_content[data-section_id=' + id + ']').slideToggle();  
	});

   
 function translate (string){
 	return string;
 }

 $(document).ready(function(){
 	if ($(window).width() > 1024 ) {    
		$('.tooltipz').on('mouseenter', function(e){    
			var title = $(this).attr('aria-label'),
				pos_left = e.pageX,
				pos_top = $(this).offset().top;

 			if (title != '') {
				$('body').append('<p id="JScustomTooltip">' + title + '</p>');  
			}
			$('#JScustomTooltip').css({
				top: pos_top - $('#JScustomTooltip').outerHeight(true),
				left: pos_left - $('#JScustomTooltip').width() / 2 
			}).show()  
		
		}).on('mouseleave', function(){
			 $('#JScustomTooltip').remove();
		}); 
 	} 
 });

// B2C & B2B ACTION MANAGE
$(document).ready(function(){
	$('.JSakc_tabs .active').css('border-color', '#61c1ce');

	$('.JSakc_tabs a').on('click', function(e){
		e.preventDefault();
	 	var el = $(e.target);

		if (el.attr('href') == 'b2c') {  
			el.parent().nextAll('[data-akc="b2c"]').removeAttr('hidden');
			el.parent().nextAll('[data-akc="b2b"]').attr('hidden', 'hidden');
		}else if(el.attr('href') == 'b2b'){  
			el.parent().nextAll('[data-akc="b2b"]').removeAttr('hidden');
			el.parent().nextAll('[data-akc="b2c"]').attr('hidden', 'hidden');
		} 
		 
		el.addClass('active').css('border-color', '#61c1ce'); 
		el.siblings('a').removeClass('active').removeAttr('style');
		 
	});
});


document.onreadystatechange = function(){
	if (document.readyState === "complete") { 
		$(".JSads-loader").hide();
	}
}