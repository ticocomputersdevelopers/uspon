$(document).ready(function(){
	$('button').click(function(){
		$(this).attr("disabled",true);
	});

	$('#JSPovezi').click(function(){
		$('#JSPovezivanjeGrupa').submit();
	});

	$('#JSObrisi').click(function(){
		var base_url = $('input[name="base_url"]').val();
		var export_id = $('input[name="export_id"]').val();
		var grupa_id = $('#grupa_id').val();
		var key = $('input[name="key"]').val();

		if(grupa_id!=''){		
			$.post(base_url+'export/grupe-razvezi/'+key, {export_id: export_id, grupa_id: grupa_id}, function (response){
				alert(response);
				location.reload(true);
			});		
		}else{
			$('#JSObrisiError').text(translate('Niste izabrali grupu!'));
		}
	});

	$('#JSDodajGrupu').click(function(){
		$('#JSDodavanjeGrupe').submit();
	});
	$('#imp_file').change(function(){
		$('#JSUcitavanjeFajla').submit();
	});
});