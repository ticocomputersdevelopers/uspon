$(document).ready(function(){

	$("input[name='news_img']").on("change", function(){
		if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.news-pic').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    	}
	});

});